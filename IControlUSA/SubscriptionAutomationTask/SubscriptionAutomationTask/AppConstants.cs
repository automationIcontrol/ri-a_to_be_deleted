﻿using System.Configuration;

namespace SubscriptionAutomationTask
{
    /// <summary>
    /// Class AppConstants.
    /// This file will contains all constants,which we will be using in our job.
    /// </summary>
    public partial class AppConstants
    {
        //reports names
        public const string REPORT_DISTRIBUTION_GAP_ANALYSIS = "DISTRIBUTIONGAPANALYSIS";
        public const string REPORT_DIGITAL_ID_CUSTOMER_DASHBOARD = "DIGITALIDCUSTOMERDASHBOARD";
        public const string REPORT_PRODUCT_PURCHASE_MULTIPLES = "PRODUCTPURCHASEMULTIPLES";
        public const string REPORT_DIGITALIDCUSTOMERDASHBOARD = "DIGITALIDCUSTOMERDASHBOARD";
        public const string REPORT_DISTRIBUTION_GAP_SALES_OPPORTUNITY = "DISTRIBUTIONGAPSALES$OPPORTUNITY";
        public const string REPORT_PRICE_EXECUTION_AUDIT = "PRICEEXECUTIONAUDIT";
        public const string REPORT_DIGITAL_ID_CUSTOMER_DEMOGRAPHICS = "DigitalIDCustomerDemographics";
        public const string REPORT_DIGITAL_ID_PERFORMANCE_METRICS = "DIGITALIDPERFORMANCEMETRICS";
        public const string REPORT_DIGITAL_ID_NEW_ITEMS = "DIGITALIDNEWITEMS";
        public const string REPORT_DIGITAL_ID_DISCONTINUED_ITEMS = "DIGITALIDDISCONTINUEDITEMS";
        public const string REPORT_NEW_ITEM_DASHBOARD = "NEWITEMDASHBOARD";
        public const string REPORT_AD_HOC_ADVANCED = "ADHOCADVANCED";
        public const string REPORT_AD_HOC_HOLIDAY_SHIFT = "ADHOCHOLIDAYSHIFT";
        public const string REPORT_SALES_DRIVER_BASKET = "SALESDRIVERBASKET";
        public const string REPORT_BUSINESS_REVIEW_DASHBOARDS = "BUSINESSREVIEWDASHBOARDS";

    }
}
