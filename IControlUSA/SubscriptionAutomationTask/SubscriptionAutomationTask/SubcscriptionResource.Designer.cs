﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SubscriptionAutomationTask {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class SubcscriptionResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal SubcscriptionResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SubscriptionAutomationTask.SubcscriptionResource", typeof(SubcscriptionResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;table&gt;&lt;tr&gt;&lt;td&gt;To unsubscribe from this report, go to Maintain Reports. Settings =&gt; Maintain Reports.&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;br/&gt;.
        /// </summary>
        internal static string EmailBodyFooterContent {
            get {
                return ResourceManager.GetString("EmailBodyFooterContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;tr&gt;&lt;td&gt;Dear @NAME,&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&apos;height:30px;&apos;&gt;&lt;td&gt;The following SVInsights report,  @ReportName, could not be completed. Select new filter options and submit.&lt;/td&gt;&lt;/tr&gt;.
        /// </summary>
        internal static string EmailBodyWithoutAttachment {
            get {
                return ResourceManager.GetString("EmailBodyWithoutAttachment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select SchedulerExecutionTime from SavedReportsSchedule where NewRecur=&apos;@ScheduleType&apos;;.
        /// </summary>
        internal static string GetSchedulerExecutionTime {
            get {
                return ResourceManager.GetString("GetSchedulerExecutionTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;tr&gt;&lt;td&gt;Dear @Name,&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&apos;height:40px;&apos;&gt;&lt;td&gt;Attached is the @ReportFrequency SVInsights report subscription, @OrignalReportName.&lt;/td&gt;&lt;/tr&gt;.
        /// </summary>
        internal static string SubsReportBodyContent {
            get {
                return ResourceManager.GetString("SubsReportBodyContent", resourceCulture);
            }
        }
    }
}
