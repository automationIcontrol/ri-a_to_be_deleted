﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SubscriptionAutomationTask
{
    public class SubscritionTask
    {
        private static string LogPath = ConfigurationManager.AppSettings["LogPath"].ToString();
        private static string strConn = ConfigurationManager.ConnectionStrings["Aspnet_DataManager"].ToString();
        private static string strConnNetezza = ConfigurationManager.ConnectionStrings["DataTrue_EDWGrocery_Netezza"].ToString();


        private static string ReportPath = ConfigurationManager.AppSettings["ReportPath"].ToString();
        private static string ReportPathforAttach = ConfigurationManager.AppSettings["ReportPathforAttach"].ToString();
        private static string BatchFilePath = ConfigurationManager.AppSettings["BatchFilePath"].ToString();
        private static string CommandPrompt = ConfigurationManager.AppSettings["CommandPrompt"].ToString();
        private static string TableauDir = ConfigurationManager.AppSettings["TableauDir"].ToString();
        private static string TableauServer = ConfigurationManager.AppSettings["TableauServer"].ToString();
        private static string SavedReportsPath = ConfigurationManager.AppSettings["SavedReportsPath"].ToString();
        private static string EmailFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();
        private static string EmailSubjectText = ConfigurationManager.AppSettings["EmailSubject"].ToString();

        private static string TableauUname = "";
        private static string TableauPassword = "";
        private static string UserEmailId = "";
        private static string UserProfile = "";
        private static string ErrorStatus = "";
        private static string Name = "";
        private static string ReportFrequency = "";
        private static int ExecutionCount;
        private static string BCCEmailId = Convert.ToString(ConfigurationManager.AppSettings["BCCEmailId"]);
        private static string mailHost = Convert.ToString(ConfigurationManager.AppSettings["mailHost"]);
        private static Int32 mailPort = Convert.ToInt32(ConfigurationManager.AppSettings["mailPort"]);

        private static string SPFailedAlertEmails = Convert.ToString(ConfigurationManager.AppSettings["SPFailedAlertEmails"]);
        private static string TableauHost
        {
            get
            {
                if (ConfigurationManager.AppSettings["TableauServer"].ToString().Trim().ToUpper() == "DEV")
                {
                    return Convert.ToString(ConfigurationManager.AppSettings["TableauHostDev"]);
                }
                else
                {
                    return Convert.ToString(ConfigurationManager.AppSettings["TableauHostProd"]);
                }

            }
        }
        static void Main(string[] args)
        {

            if (!Directory.Exists(ReportPath))
            {
                Directory.CreateDirectory(ReportPath);
            }
            if (!Directory.Exists(BatchFilePath))
            {
                Directory.CreateDirectory(BatchFilePath);
            }
            string scheduleType = "W";

            if (args.Length > 0)
            {
                scheduleType = args[0].ToString();
            }

            if (ExecuteSubscriptions(scheduleType)) //Run subscriptions at specified time
            {
                WriteProcessLogFiles("Process Started (First Attempt)...", true);
                //1st attempt
                GenerateSubscriptionReports(scheduleType);
                UpdateSchedule();

                WriteProcessLogFiles("Process Started (Second Attempt)...", true);
                //2nd attempt
                GenerateSubscriptionReports(scheduleType);

                WriteProcessLogFiles("Process Started (Third Attempt)...", true);
                //3rd attempt
                GenerateSubscriptionReports(scheduleType);

                SendProcessLog();
            }
        }

        /// <summary>
        /// Check Exectue Subscription 
        /// </summary>
        public static bool ExecuteSubscriptions(string schdType)
        {
            bool RunNow = false;
            try
            {
                TimeSpan dtScheduleExecutionTime = new TimeSpan(0, 0, 0);
               
                string str = SubcscriptionResource.GetSchedulerExecutionTime.Replace("@ScheduleType", schdType);
                SqlDataAdapter adp = new SqlDataAdapter(str, strConn);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                    dtScheduleExecutionTime = TimeSpan.Parse(dt.Rows[0]["SchedulerExecutionTime"].ToString());

                if (dtScheduleExecutionTime.Hours == DateTime.Now.Hour)
                {
                    RunNow = true;
                }

            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "ExecuteSubscriptions()");
            }
            return RunNow;
        }

        /// <summary>
        /// Generate Subscription Reports
        /// </summary>
        public static void GenerateSubscriptionReports(string ScheduleType)
        {
            int ReportStatus = 0;
            int ReportCount = 0;
            string SelectedParameters = "";
            try
            {
                DataTable dtReports = GetReportsToSend("S", TableauServer, ScheduleType);

                WriteProcessLogFiles(dtReports.Rows.Count + " Reports to Send...", false);

                foreach (DataRow dr in dtReports.Rows)
                {
                    ReportCount = ReportCount + 1;
                    TableauUname = dr["UserName"].ToString();
                    TableauPassword = dr["TableauPwd"].ToString();
                    UserEmailId = dr["EmailId"].ToString();
                    UserProfile = dr["UserProfile"].ToString();
                    SelectedParameters = "";
                    string EmailSubject = dr["ReportName"].ToString();
                    string ReportName = dr["OriginalReportName"].ToString().Replace("->", "").Replace("%", "") + System.DateTime.Now.ToString("ddMMyyhhmmss");
                    string OrignalReportName = dr["OriginalReportName"].ToString();
                    string FolderName = "";

                    WriteProcessLogFiles("Processing " + EmailSubject + " (" + ReportCount + " of " + dtReports.Rows.Count + ")", false);

                    if (dr["Folder_Name"] != null)
                    {
                        FolderName = dr["Folder_Name"].ToString();
                    }

                    if (dr["ExecutionCount"] != null)
                    {
                        ExecutionCount = Convert.ToInt32(dr["ExecutionCount"]);
                    }

                    if (dr["Name"] != null)
                    {
                        Name = dr["Name"].ToString();
                    }
                    else
                    {
                        Name = "User";
                    }

                    if (dr["ReportFrequency"] != null)
                    {
                        ReportFrequency = dr["ReportFrequency"].ToString();
                    }
                    else
                    {
                        ReportFrequency = "";
                    }

                    string ReportParamName;
                    if (UserProfile.Contains("WithoutVendorNumber"))
                        ReportParamName = dr["ParamName"].ToString().Replace("_{RoleId}", UserProfile);
                    else if (String.IsNullOrEmpty(UserProfile))
                        ReportParamName = dr["ParamName"].ToString().Replace("_{RoleId}", "");
                    else
                        ReportParamName = dr["ParamName"].ToString().Replace("{RoleId}", UserProfile);

                    string reportName = GetReportName(Convert.ToInt32(dr["Menuid"]));

                    string ReportParameters = GetUpdatedParameters(reportName, Convert.ToString(dr["ReportParameters"]), Convert.ToString(dr["DateRange"])) + "\"";
                    SelectedParameters = GetSelectedParameters(Convert.ToString(dr["SelectedFilters"].ToString()), Convert.ToString(dr["DateRange"]));

                    string SPCallForLog = "";
                    string DBConnectionStringForLog = "";
                    bool SPExecutionSuccess = true;

                    try
                    {
                        if (reportName.Trim().ToUpper() == AppConstants.REPORT_BUSINESS_REVIEW_DASHBOARDS || reportName.Trim().ToUpper() == AppConstants.REPORT_SALES_DRIVER_BASKET)
                        {
                            if (dr["SpCall"].ToString().Trim().Length > 0)
                            {
                                string New_EID = Guid.NewGuid().ToString("N");
                                Int64 RowCount = 0;
                                string Spcall = dr["SpCall"].ToString().Replace("%27", "'");
                                string[] Spcall_Rep = Spcall.Split(new string[] { "#~#" }, StringSplitOptions.None);

                                string NewDt = DateTime.Now.ToString("yyyy-MM-dd");

                                string[] RepPar = ReportParameters.Split('&');
                                int j = 0;
                                string e_id = "";
                                string Fromadweek = "";
                                string ToAdweek = "";

                                for (j = 0; j < RepPar.Length; j++)
                                {
                                    if (RepPar[j].ToString().Contains("e_id"))
                                    {
                                        e_id = RepPar[j].ToString();
                                    }

                                    if (RepPar[j].ToString().Contains("FromAdWeek"))
                                    {
                                        string[] getdt = RepPar[j].ToString().Split('=');
                                        if (getdt[0].ToString() == "FromAdWeek")
                                            Fromadweek = getdt[1].ToString();
                                    }

                                    if (RepPar[j].ToString().Contains("ToAdWeek"))
                                    {
                                        string[] getdt = RepPar[j].ToString().Split('=');
                                        if (getdt[0].ToString() == "ToAdWeek")
                                            ToAdweek = getdt[1].ToString();
                                    }
                                }

                                string FinalSpcall = "";

                                // Make SP call for Business Review Report
                                if (reportName.Trim().ToUpper() == AppConstants.REPORT_BUSINESS_REVIEW_DASHBOARDS)
                                {
                                    FinalSpcall = Spcall_Rep[0] + "'" + New_EID + "'" + Spcall_Rep[2] + "'" + NewDt + "'" + Spcall_Rep[4];
                                }
                                // Make SP call for Sales Driver Basket Report

                                if (reportName.Trim().ToUpper() == AppConstants.REPORT_SALES_DRIVER_BASKET)
                                {
                                    string Start_date_Quarterly = GetParamDate(Fromadweek);
                                    string End_date_Quarterly = GetEnddate(ToAdweek);
                                    FinalSpcall = Spcall_Rep[0] + "'" + New_EID + "'," + "'" + Start_date_Quarterly + "'" + Spcall_Rep[3] + "'" + End_date_Quarterly + "'" + Spcall_Rep[5];
                                }

                                SPCallForLog = FinalSpcall;

                                string strConnNetezzaEDWGrocery = dr["DBConnection"].ToString();
                                DBConnectionStringForLog = strConnNetezzaEDWGrocery;

                                using (OleDbConnection Conn = new OleDbConnection(strConnNetezzaEDWGrocery))
                                {

                                    using (OleDbCommand cmd = new OleDbCommand(FinalSpcall, Conn))
                                    {
                                        cmd.CommandType = CommandType.Text;
                                        cmd.CommandTimeout = 5400;
                                        Conn.Open();
                                        WriteProcessLogFiles("SP Execution started: " + FinalSpcall, false);
                                        RowCount = Convert.ToInt64(cmd.ExecuteScalar());
                                    }
                                    WriteProcessLogFiles("SP call ended", false);
                                }
                                string Newid = "e_id=" + New_EID + "";

                                string FinalReportParam = ReportParameters.Replace(e_id, Newid);

                                FinalReportParam = FinalReportParam.TrimEnd('"');

                                ReportParameters = FinalReportParam + "\"";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        SPExecutionSuccess = false;
                        //For logging SP related errors
                        WriteProcessLogFiles("SP Execution Failed with Error : " + ex.Message.ToString(), false);
                        WriteLogFiles("SP Call: " + SPCallForLog + ", " + Environment.NewLine + " Error: " + ex.Message.ToString(), ex.StackTrace.ToString(), "SPExecution");
                        SPExecutionFailedAlert(EmailSubject, OrignalReportName, SPCallForLog, DBConnectionStringForLog, SelectedParameters, ex.Message.ToString());
                    }

                    // FOR SP CHANGES ONLY END

                    if (SPExecutionSuccess == true)
                    {
                        string[] cmdBuilder = new string[5] {
                                            @"cd " + TableauDir,
                                            @"tabcmd login -s " + TableauHost + " -u " + TableauUname + " -p " + TableauPassword,
                                            @"tabcmd get " + "\"" + TableauHost + "/views/" + ReportParamName + ".pdf?" + ReportParameters + " -f  "+ "\"" + ReportPath + ReportName + ".pdf" + "\" --timeout 1200",
                                            @"tabcmd logout",
                                             @"Exit"
                                           };

                        string BatFile = BatchFilePath + System.DateTime.Now.ToString("ddMMyyyyhhmmss") + ".bat";
                        using (StreamWriter sWriter = new StreamWriter(BatFile))
                        {
                            foreach (string cmd in cmdBuilder)
                            {
                                sWriter.WriteLine(cmd);
                            }
                        }

                        try
                        {
                            using (Process p = new Process())
                            {
                                p.StartInfo.CreateNoWindow = true;
                                p.StartInfo.UseShellExecute = false;
                                p.StartInfo.FileName = CommandPrompt;
                                p.StartInfo.Arguments = "/c start /wait " + BatFile + " ";
                                p.StartInfo.RedirectStandardError = true;
                                p.Start();
                                ErrorStatus = "Success";
                                p.StandardError.ReadToEnd();
                                p.WaitForExit();
                            }
                            File.Delete(BatFile);

                            if (File.Exists(ReportPathforAttach + ReportName + ".pdf"))
                            {
                                ReportStatus = 2; // Status 2 = Sucessfully sent

                                SendEmailToUser(UserEmailId, BCCEmailId, ReportPathforAttach + ReportName + ".pdf", ReportName, "S", SelectedParameters, OrignalReportName, Name, ReportFrequency, EmailSubject);

                                if (!String.IsNullOrEmpty(FolderName)) //Save to Folder report
                                {
                                    //Save report path in table column
                                    CopyFile(ReportPathforAttach + ReportName + ".pdf", Path.Combine(SavedReportsPath, ReportName + ".pdf"));
                                    UpdateSaveToFolderReportPath(Convert.ToInt16(dr["ReportId"]), "S", ReportName + ".pdf");
                                }
                            }
                            else
                            {
                                ReportStatus = 3; // Error
                                ErrorStatus = "Error in generating the file.";
                                WriteLogFiles(ErrorStatus, ErrorStatus, "PDF File Error");

                                if (ExecutionCount == 11) // Send email to user on last attempt
                                {
                                    SendEmailToUser(UserEmailId, BCCEmailId, "", ReportName, "S", SelectedParameters, OrignalReportName, Name, ReportFrequency, EmailSubject);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ReportStatus = 3; // Error
                            ErrorStatus = "Error: " + ex.Message.ToString();
                            WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "PDF File Generation");
                        }
                    }

                    UpdateReportStatus(ReportStatus, Convert.ToInt16(dr["ReportId"]), ErrorStatus, Convert.ToString(dr["EmailID"]), "S", ExecutionCount);

                    if (ExecutionCount == 0) //Update next execution date for 1st time only
                        CalculateNextExecDate(Convert.ToInt16(dr["Scheduleid"]));

                    WriteProcessLogFiles("Finished Processing " + EmailSubject + ", Status:" + ErrorStatus, false);
                }
            }

            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GenerateSubscriptionReports()");
            }
        }
        /// <summary>
        /// Sp Execution failed alert method
        /// </summary>
        private static void SPExecutionFailedAlert(string ReportName, string OriginalReportName, string SPExecution, string DBConnectionString, string SelectedParameters, string ErrorMessage)
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {

                    mail.From = new MailAddress(EmailFrom);
                    if(SPFailedAlertEmails!="")
                    mail.To.Add(SPFailedAlertEmails);

                    mail.Subject = "SVInsights Alert: SP Execution Failed for " + ReportName;

                    string Message = "";

                    Message = "<tr><td>Hi All,</td></tr>";
                    Message += "<tr style='height:40px;'><td>The SP Execution failed for subscription " + ReportName + ".</td></tr>";
                    Message += "<tr style='height:40px;'><td>Report Name : " + OriginalReportName + "</td></tr>";
                    Message += "<tr style='height:40px;'><td>SP Call : " + SPExecution + "</td></tr>";
                    Message += "<tr style='height:40px;'><td>Database Connection : " + DBConnectionString + "</td></tr>";
                    Message += "<tr style='height:40px;'><td>Error Message : " + ErrorMessage + "</td></tr>";
                    Message += "<tr style='height:40px;'><td>Report is requested with the following filters.</td></tr>";

                    int i = 0;
                    string strEmail = "";
                    strEmail = "<html><head></head>";
                    strEmail += "<body>  ";

                    strEmail += "<table>" + Message + "</table>";
                    if (!String.IsNullOrEmpty(SelectedParameters))
                    {
                        string[] RepPar = SelectedParameters.Split(new string[] { "~#~" }, StringSplitOptions.None);
                        strEmail += "<table style='border:1px solid;  border-collapse: collapse; text-align:left;' cellpadding='3'>";
                        for (i = 0; i < RepPar.Length; i++)
                        {
                            int j = 0;
                            string[] Columns = RepPar[i].Split(new string[] { "#~#" }, StringSplitOptions.None);

                            strEmail += "<tr style='text-align:left;'>";

                            if (Columns.Length > 0)
                                strEmail += "<th style='border:1px solid; text-align: right;'>" + Columns[0].ToString() + "</th>";

                            string val = "";
                            for (j = 1; j < Columns.Length; j++)
                            {
                                if (j == 1)
                                    val += Columns[j].ToString();
                                else
                                    val += ":" + Columns[j].ToString();
                            }
                            strEmail += "<td style='border:1px solid;min-width:300px; text-align: left;'>" + val + "</td>";

                            strEmail += "</tr>";
                        }
                        strEmail += "</table><br/>";
                    }

                    strEmail += "</body> </html>";
                    mail.Body = strEmail;
                    mail.IsBodyHtml = true;
                    using (SmtpClient smtp = new SmtpClient())
                    {
                        NetworkCredential basicAuthenticationInfo = new NetworkCredential("iControl", "SpojIzF3CBum");
                        smtp.Host = "smtp.socketlabs.com";//"smtp.gmail.com";
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = basicAuthenticationInfo;

                        smtp.EnableSsl = false;
                        smtp.Port = 587; //use 465 or 587
                        smtp.Send(mail);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "SPExecutionFailedAlert()");
            }
        }
       
        //changes in Sp pending
        /// <summary>
        /// Get report list for send mail
        /// </summary>
        private static DataTable GetReportsToSend(string ReportType, string TableauServer, string schdType)
        {
            using (DataTable dtReports = new DataTable())
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(strConn))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = con;
                            cmd.Parameters.Add("@ReportType", SqlDbType.VarChar, 1).Value = ReportType;
                            cmd.Parameters.Add("@TableauServer", SqlDbType.VarChar, 3).Value = TableauServer;
                            cmd.Parameters.Add("@TableauUrl", SqlDbType.VarChar, 200).Value = TableauHost;
                            cmd.Parameters.Add("@ScheduleType", SqlDbType.VarChar, 5).Value = schdType;
                            cmd.CommandText = "usp_GetSubscriptionReports_V3";
                            cmd.CommandType = CommandType.StoredProcedure;
                            using (SqlDataAdapter adap = new SqlDataAdapter(cmd))
                            {
                                adap.Fill(dtReports);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GetReportsToSend()");
                }
                return dtReports;
            }
        }
        /// <summary>
        /// mehtod for copy file from source to destination pATH
        /// </summary>
        private static void CopyFile(string SourceFileName, string DestinationFileName)
        {
            try
            {
                File.Copy(SourceFileName, DestinationFileName);
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "CopyFile()");
            }
        }
        /// <summary>
        /// update save to folder report
        /// </summary>
        private static void UpdateSaveToFolderReportPath(int ReportID, string ReportType, string ReportPath)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(strConn))
                {
                    using (SqlCommand cmd = new SqlCommand("Usp_UpdateSaveToFolderReportPath", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@ReportType", SqlDbType.VarChar, 1).Value = ReportType;
                        cmd.Parameters.Add("@ReportId", SqlDbType.Int).Value = ReportID;
                        cmd.Parameters.Add("@ReportPath", SqlDbType.VarChar).Value = ReportPath;
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "UpdateReportStatus()");
            }
        }

        /// <summary>
        /// send email to user
        /// </summary>
        public static bool SendEmailToUser(string UserEmailId, string BCCEmailId, string FilePath, string ReportName, string ReportType, string SelectedParameters, string OrignalReportName, string Name, string ReportFrequency, string EmailSubject)
        {
            bool EmailStatus = false;
            try
            {
                MailMessage mail = new MailMessage();
                // check for emailids repeatation
                List<string> EmailIds = UserEmailId.Split(',').ToList(); ;
                HashSet<string> uniqueEmailIds = new HashSet<string>(EmailIds);
                string useremailids = String.Join(",", uniqueEmailIds);

                mail.From = new MailAddress(EmailFrom);
                mail.To.Add(useremailids);
                if(BCCEmailId!="")
                mail.Bcc.Add(BCCEmailId);
                mail.Subject = EmailSubjectText + EmailSubject;

                string Message = "";
                if (FilePath != "")
                {
                    Message = SubcscriptionResource.SubsReportBodyContent.Replace("@Name", string.Format("{0}", Name)).
                               Replace("@ReportFrequency", string.Format("'{0}'", @ReportFrequency)).Replace("@OrignalReportName", string.Format("{0}", OrignalReportName));

                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(FilePath);
                    attachment.Name = ReportName + ".pdf";
                    mail.Attachments.Add(attachment);
                }
                else
                {
                    Message = Message = SubcscriptionResource.EmailBodyWithoutAttachment.Replace("@Name", string.Format("{0}", Name)).
                               Replace("@OrignalReportName", string.Format("{0}", OrignalReportName));
                }

                int i = 0;
                string strEmail = "";
                strEmail = "<html><head></head>";
                strEmail += "<body>  ";

                strEmail += "<table>" + Message + "</table>";
                if (SelectedParameters != "")
                {
                    string[] RepPar = SelectedParameters.Split(new string[] { "~#~" }, StringSplitOptions.None);
                    strEmail += "<table style='border:1px solid;  border-collapse: collapse; text-align:left;' cellpadding='3'>";
                    for (i = 0; i < RepPar.Length; i++)
                    {
                        int j = 0;

                        string[] Columns = RepPar[i].Split(new string[] { "#~#" }, StringSplitOptions.None);

                        strEmail += "<tr style='text-align:left;'>";

                        if (Columns.Length > 0)
                            strEmail += "<th style='border:1px solid; text-align: right;'>" + Columns[0].ToString() + "</th>";

                        string val = "";
                        for (j = 1; j < Columns.Length; j++)
                        {
                            if (j == 1)
                                val += Columns[j].ToString();
                            else
                                val += ":" + Columns[j].ToString();
                        }
                        strEmail += "<td style='border:1px solid;min-width:300px; text-align: left;'>" + val + "</td>";

                        strEmail += "</tr>";
                    }
                    strEmail += "</table><br/>";
                }
                if (ReportType == "S")
                {
                    strEmail += SubcscriptionResource.EmailBodyFooterContent;
                }

                strEmail += "</body> </html>";
                mail.Body = strEmail;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                NetworkCredential basicAuthenticationInfo = new NetworkCredential("iControl", "SpojIzF3CBum");
                smtp.Host = mailHost;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = basicAuthenticationInfo;

                smtp.EnableSsl = false;
                smtp.Port = mailPort;
                smtp.Send(mail);

                EmailStatus = true;
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "SendEmailToUser()");
            }
            return EmailStatus;
        }
        /// <summary>
        /// method for write error log in file
        /// </summary>
        private static void WriteLogFiles(string exception, string StackTrace, string MethodName)
        {
            if (!Directory.Exists(LogPath + "ErrorLog\\"))
            {
                Directory.CreateDirectory(LogPath + "ErrorLog\\");
            }
            string ErrorLogFileName = LogPath + "ErrorLog\\" + "ErrorLog.txt";
            if (!File.Exists(ErrorLogFileName))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(ErrorLogFileName))
                {
                }
            }
            using (StreamWriter writer = new StreamWriter(ErrorLogFileName, true))
            {
                writer.WriteLine("Message :" + exception + "<br/>" + Environment.NewLine + "StackTrace :" + StackTrace +
                   "" + Environment.NewLine + "Date :" + DateTime.Now.ToString() +
                   "" + Environment.NewLine + "Method :" + MethodName);
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
        }
        /// <summary>
        /// method for write process log in file
        /// </summary>
        private static void WriteProcessLogFiles(string Message, bool NewLineBefore)
        {
            if (!Directory.Exists(LogPath + "ProcessLog\\"))
            {
                Directory.CreateDirectory(LogPath + "ProcessLog\\");
            }

            string ProcessLogFileName = LogPath + "ProcessLog\\" + "ProcessLog_" + DateTime.Now.ToString("MMddyyyy") + ".txt";

            if (!File.Exists(ProcessLogFileName))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(ProcessLogFileName))
                {
                }
            }
            using (StreamWriter writer = new StreamWriter(ProcessLogFileName, true))
            {
                if (NewLineBefore)
                    writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);

                writer.WriteLine(DateTime.Now.ToString() + " ---> " + Message);
            }
        }
        /// <summary>
        /// send process log
        /// </summary>
        public static void SendProcessLog()
        {
            try
            {
                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(EmailFrom);
                mail.To.Add(BCCEmailId);
                string ProcessLogFileName = LogPath + "ProcessLog\\" + "ProcessLog_" + DateTime.Now.ToString("MMddyyyy") + ".txt";
                if (ProcessLogFileName != "")
                {
                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(ProcessLogFileName);
                    attachment.Name = ProcessLogFileName;
                    mail.Attachments.Add(attachment);
                }

                mail.Subject = EmailSubjectText + "Subscription Process Log";

                string Message = "";
                Message = "<tr><td>Hi All,</td></tr>";
                Message += "<tr style='height:40px;'><td>Attached please find the process log for subscriptions.</td></tr>";
                string strEmail = "";
                strEmail = "<html><head></head>";
                strEmail += "<body>  ";
                strEmail += "<table>" + Message + "</table>";
                strEmail += "</body> </html>";
                mail.Body = strEmail;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                NetworkCredential basicAuthenticationInfo = new NetworkCredential("iControl", "SpojIzF3CBum");
                smtp.Host = mailHost;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = basicAuthenticationInfo;
                smtp.EnableSsl = false;
                smtp.Port = mailPort;
                smtp.Send(mail);

            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "SendProcessLog()");
            }
        }

        /// <summary>
        /// update report satus
        /// </summary>
        private static void UpdateReportStatus(int ReportStatus, int ReportID, string ErrorStatus, string EmailId, string ReportType, int ExecCount)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(strConn))
                {

                    AddSendReportHistory(ReportType, ReportID, ErrorStatus, EmailId, con);

                    UpdateSendReportStatus(ReportID, ReportStatus, con, ExecCount);
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "UpdateReportStatus()");
            }
        }
        /// <summary>
        /// Add send report history
        /// </summary>
        private static void AddSendReportHistory(string ReportType, int ReportID, string ErrorStatus, string EmailId, SqlConnection con)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("usp_AddSendReportHistory", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ReportType", SqlDbType.VarChar, 1).Value = ReportType;
                    cmd.Parameters.Add("@ReportId", SqlDbType.Int).Value = ReportID;
                    cmd.Parameters.Add("@Email", SqlDbType.VarChar, 200).Value = EmailId;
                    cmd.Parameters.Add("@ExecutionStatus", SqlDbType.VarChar, 2000).Value = ErrorStatus;
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "AddSendReportHistory()");
            }
        }
        /// <summary>
        /// update send report status
        /// </summary>
        private static void UpdateSendReportStatus(int ReportID, int ReportStatus, SqlConnection con, int executioncount)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Usp_UpdateSubscriptionReportStatus_V2", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ReportId", SqlDbType.Int).Value = ReportID;

                    if (ReportStatus == 3)
                    {
                        if (executioncount == 0)
                        {
                            cmd.Parameters.Add("@Executioncount", SqlDbType.Int).Value = 10;
                        }
                        else if (executioncount == 10)
                        {
                            cmd.Parameters.Add("@Executioncount", SqlDbType.Int).Value = 11;
                        }
                        else if (executioncount == 11)
                        {
                            cmd.Parameters.Add("@Executioncount", SqlDbType.Int).Value = 0;
                        }
                    }
                    else if (ReportStatus == 2)
                    {
                        cmd.Parameters.Add("@Executioncount", SqlDbType.Int).Value = 0;
                    }
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "UpdateSendReportStatus()");
            }
        }
        /// <summary>
        /// get updated parameter
        /// </summary>
        private static string GetUpdatedParameters(string reportName, string Parameter, string DateRange)
        {
            try
            {
                using (OleDbConnection conNetezza = new OleDbConnection(strConnNetezza))
                {
                    string OldStartDate = "";
                    string OldEndDate = "";
                    string OldFY = "";
                    string OldRC = "";
                    string NewStartDate = "Start_Date=";
                    string NewEndDate = "End_Date=";

                    string FromAdWeek = "FromAdWeek=";
                    string ToAdWeek = "ToAdWeek=";

                    string OldFiscalCalendarWeek = "";
                    string OldHeading = "";
                    string RelativeStartDate = "";
                    string RelativeEndDate = "";

                    // Paramters for Sales Driver Basket Report Start
                    string Param_OldStartDate = "";
                    string Param_OldEndDate = "";

                    string Param_OldStartDate_Ly = "";
                    string Param_OldEndDate_Ly = "";
                    string FromAdweekValue = "";
                    string ToAdweekValue = "";
                    string OldDates = "";
                    // Parameter for Sales Driver Basket Report END

                    string OldFromAdWeek = "";
                    string OldToAdWeek = "";
                    string ForStartEndDate = "";
                    string EmptyStr = "";
                    string Adweek = "";

                    string[] DateFilters;
                    string[] RepPar = Parameter.Split('&');
                    string e_id = "";

                    if (!String.IsNullOrEmpty(DateRange) || (!string.IsNullOrEmpty(reportName) && reportName.Trim().ToUpper() == AppConstants.REPORT_BUSINESS_REVIEW_DASHBOARDS))
                    {
                        DateFilters = DateRange.ToString().Split('-');
                        if (DateFilters[0].ToString() == "W")
                        {
                            NewStartDate += StartDateForSaveFilterForWeek(Convert.ToInt16(DateFilters[1]) * 7).ToString("yyyy-MM-dd");
                            NewEndDate += EndDate().ToString("yyyy-MM-dd");

                            RelativeStartDate = StartDateForSaveFilterForWeek(Convert.ToInt16(DateFilters[1]) * 7).ToString("yyyy-MM-dd");
                            RelativeEndDate = EndDate().ToString("yyyy-MM-dd");
                        }
                        int i = 0;
                        for (i = 0; i < RepPar.Length; i++)
                        {
                            if (RepPar[i].ToString().Contains("Start_Date"))
                            {
                                OldStartDate = RepPar[i].ToString();
                            }
                            if (RepPar[i].ToString().Contains("End_Date"))
                            {
                                EmptyStr = "1";
                                ForStartEndDate = "1";
                                OldEndDate = RepPar[i].ToString();
                            }
                            string OldEndDate_Param = "", OldStartDate_Param = "", OldStart_FQ = "", OldRC_DatePrd = "", OldRC_DateQtr = "";
                            if (RepPar[i].ToString().Contains("START_DATE_PARAM"))
                            {
                                OldStartDate_Param = RepPar[i].ToString();
                            }
                            if (RepPar[i].ToString().Contains("END_DATE_PARAM"))
                            {
                                OldEndDate_Param = RepPar[i].ToString();
                            }
                            // Used IN Saled Driver Basket Report Start
                            if (RepPar[i].ToString().Contains("PARAM_START_DATE"))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (getdt[0].ToString() == "PARAM_START_DATE")
                                    Param_OldStartDate = getdt[1].ToString();
                            }
                            if (RepPar[i].ToString().Contains("PARAM_END_DATE"))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (getdt[0].ToString() == "PARAM_END_DATE")
                                    Param_OldEndDate = getdt[1].ToString();
                            }
                            if (RepPar[i].ToString().Contains("PARAM_START_DATE_LY"))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (getdt[0].ToString() == "PARAM_START_DATE_LY")
                                    Param_OldStartDate_Ly = getdt[1].ToString();
                            }
                            if (RepPar[i].ToString().Contains("PARAM_END_DATE_LY"))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (getdt[0].ToString() == "PARAM_END_DATE_LY")
                                    Param_OldEndDate_Ly = getdt[1].ToString();
                            }
                            if (RepPar[i].ToString().Contains("Dates"))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (getdt[0].ToString() == "Dates")
                                    OldDates = getdt[1].ToString();
                            }
                            if (RepPar[i].ToString().Contains("FY"))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (getdt[0].ToString() == "FY")
                                    OldFY = RepPar[i].ToString();
                            }

                            if (RepPar[i].ToString().Contains("RC"))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (getdt[0].ToString() == "RC")
                                    OldRC = getdt[1].ToString();
                            }
                            if (RepPar[i].ToString().Contains("Start_FQ"))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (getdt[0].ToString() == "Start_FQ")
                                    OldStart_FQ = RepPar[i].ToString();
                            }
                            if (RepPar[i].ToString().Contains("RC_Date%%20Prd"))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (getdt[0].ToString() == "RC_Date%%20Prd")
                                    OldRC_DatePrd = RepPar[i].ToString();
                            }
                            if (RepPar[i].ToString().Contains("RC_Date%%20Qtr"))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (getdt[0].ToString() == "RC_Date%%20Qtr")
                                    OldRC_DateQtr = RepPar[i].ToString();
                            }
                            if (RepPar[i].ToString().Contains("Fiscal%%20Calendar%%20Week"))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (getdt[0].ToString() == "Fiscal%%20Calendar%%20Week")
                                    OldFiscalCalendarWeek = RepPar[i].ToString();
                            }
                            if (RepPar[i].ToString().Contains("Heading"))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (getdt[0].ToString() == "Heading")
                                    OldHeading = RepPar[i].ToString();
                            }

                            // Used IN Saled Driver Basket Report END

                            // For Distribution Gap Sales Opportunity
                            if (!string.IsNullOrEmpty(reportName) && reportName.Trim().ToUpper() == AppConstants.REPORT_DISTRIBUTION_GAP_SALES_OPPORTUNITY)
                            {
                                if (RepPar[i].ToString().Contains("e_id"))
                                {
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    if (getdt[0].ToString() == "e_id")
                                        e_id = getdt[1].ToString();
                                }
                            }

                            if (RepPar[i].ToString().Contains("FromAdWeek"))
                            {
                                OldFromAdWeek = RepPar[i].ToString();
                                if (DateFilters.Length > 1)
                                {
                                    DateTime Enddate = EndDate();
                                    NewStartDate = StartDateForSaveFilterForWeek(Convert.ToInt16(DateFilters[1]) * 7, Enddate).ToString("yyyy-MM-dd");
                                    string Fromdtsql = "SELECT 'FY' || FISCAL_YEAR_NUM || case when Length(FISCAL_WEEK_OF_YEAR_NUM) =1 then  '-W0' ||FISCAL_WEEK_OF_YEAR_NUM  else '-W' || FISCAL_WEEK_OF_YEAR_NUM end as [Param] FROM DIM_WEEKLY_CALENDAR Where WEEK_START_DATE ='" + NewStartDate + "' order by WEEK_START_DATE desc";
                                    using (OleDbDataAdapter adp = new OleDbDataAdapter(Fromdtsql, conNetezza))
                                    {
                                        using (DataTable dt = new DataTable())
                                        {
                                            adp.Fill(dt);

                                            if (dt.Rows.Count > 0)
                                            {
                                                FromAdWeek += dt.Rows[0]["Param"].ToString();
                                                FromAdweekValue = dt.Rows[0]["Param"].ToString();
                                            }
                                        }
                                    }
                                }
                            }

                            if (RepPar[i].ToString().Contains("ToAdWeek"))
                            {
                                OldToAdWeek = RepPar[i].ToString();
                                if (DateFilters.Length > 1)
                                {
                                    string ToDtSql = "SELECT 'FY' || FISCAL_YEAR_NUM || case when Length(FISCAL_WEEK_OF_YEAR_NUM) =1 then  '-W0' ||FISCAL_WEEK_OF_YEAR_NUM  else '-W' || FISCAL_WEEK_OF_YEAR_NUM end as [Param] FROM DIM_WEEKLY_CALENDAR Where FISCAL_WEEK_END_DT ='" + EndDate().ToString("yyyy-MM-dd") + "' order by FISCAL_WEEK_END_DT desc";
                                    using (OleDbDataAdapter adp = new OleDbDataAdapter(ToDtSql, conNetezza))
                                    {
                                        using (DataTable dt = new DataTable())
                                        {
                                            adp.Fill(dt);
                                            if (dt.Rows.Count > 0)
                                            {
                                                EmptyStr = "1";
                                                Adweek = "1";
                                                ToAdWeek += dt.Rows[0]["Param"].ToString();
                                                ToAdweekValue = dt.Rows[0]["Param"].ToString();
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        string FInalRepParam = "";
                        if (!String.IsNullOrEmpty(ForStartEndDate))
                        {
                            if (!String.IsNullOrEmpty(OldStartDate))
                            {
                                FInalRepParam = Parameter.ToString().Replace(OldStartDate, NewStartDate);
                            }
                            if (!String.IsNullOrEmpty(OldEndDate) && String.IsNullOrEmpty(OldStartDate))
                            {
                                FInalRepParam = Parameter.ToString().Replace(OldEndDate, NewEndDate);
                            }
                            if (!String.IsNullOrEmpty(OldEndDate) && !String.IsNullOrEmpty(OldStartDate))
                            {
                                FInalRepParam = FInalRepParam.Replace(OldEndDate, NewEndDate);
                            }
                        }
                        if (!String.IsNullOrEmpty(Adweek))
                        {
                            FInalRepParam = Parameter.ToString().Replace(OldFromAdWeek, FromAdWeek).Replace(OldToAdWeek, ToAdWeek);
                        }

                        if (String.IsNullOrEmpty(EmptyStr))
                        {
                            FInalRepParam = Parameter.ToString();
                        }

                        if (!String.IsNullOrEmpty(OldFiscalCalendarWeek))
                        {
                            string NewFiscalCalendarWeek = "Fiscal%%20Calendar%%20Week=" + GetFiscalDatesW0(RelativeStartDate, RelativeEndDate);
                            FInalRepParam = FInalRepParam.ToString().Replace(OldFiscalCalendarWeek, NewFiscalCalendarWeek);
                        }

                        if (!String.IsNullOrEmpty(OldHeading))
                        {
                            string NewStartDateHeading = Convert.ToDateTime(RelativeStartDate).ToString("MM/dd/yyyy");
                            string NewEndDateHeading = Convert.ToDateTime(RelativeEndDate).ToString("MM/dd/yyyy");

                            string NewHeading = "Heading=From:%%20" + NewStartDateHeading + "%%20To:%%20" + NewEndDateHeading;
                            FInalRepParam = FInalRepParam.ToString().Replace(OldHeading, NewHeading);
                        }

                        Adweek = "";
                        ForStartEndDate = "";
                        EmptyStr = "";

                        if (reportName.Trim().ToUpper() == AppConstants.REPORT_BUSINESS_REVIEW_DASHBOARDS)
                        {
                            string NewFY = "";
                            string NewDt = "Start_Date=" + StartDate().ToString("yyyy-MM-dd");

                            if (OldRC == "true")
                                NewFY = "FY=" + GetClosedFiscalPeriod(DateTime.Now.ToString("yyyy-MM-dd"));
                            else
                                NewFY = "FY=" + GetClosedFiscalQuarter(DateTime.Now.ToString("yyyy-MM-dd"));

                            FInalRepParam = Parameter.ToString().Replace(OldStartDate, NewDt).Replace(OldFY, NewFY);
                        }
                        if (reportName.Trim().ToUpper() == AppConstants.REPORT_SALES_DRIVER_BASKET)
                        {
                            string Param_NewStartDate = GetParamDate(FromAdweekValue);
                            string Param_NewEndDate = GetEnddate(ToAdweekValue);

                            string Param_NewStartDate_Ly = GetParamLYDate(FromAdweekValue, "Start");
                            string Param_NewEndDate_Ly = GetParamLYDate(ToAdweekValue, "End");
                            string Dates = FromAdweekValue + "%%20to%%20" + ToAdweekValue;

                            if (!String.IsNullOrEmpty(Param_OldStartDate))
                                FInalRepParam = FInalRepParam.ToString().Replace(Param_OldStartDate, Param_NewStartDate);

                            if (!String.IsNullOrEmpty(Param_OldEndDate))
                                FInalRepParam = FInalRepParam.ToString().Replace(Param_OldEndDate, Param_NewEndDate);

                            if (!String.IsNullOrEmpty(Param_OldStartDate_Ly))
                                FInalRepParam = FInalRepParam.ToString().Replace(Param_OldStartDate_Ly, Param_NewStartDate_Ly);

                            if (!String.IsNullOrEmpty(Param_OldEndDate_Ly))
                                FInalRepParam = FInalRepParam.ToString().Replace(Param_OldEndDate_Ly, Param_NewEndDate_Ly);

                            if (!String.IsNullOrEmpty(OldDates))
                                FInalRepParam = FInalRepParam.ToString().Replace(OldDates, Dates);

                        }
                        // For Distribution Gap Sales Opportunity Applicable only if Date Range is selected by User.
                        if (reportName.Trim().ToUpper() == AppConstants.REPORT_DISTRIBUTION_GAP_SALES_OPPORTUNITY)
                        {
                            if (!String.IsNullOrEmpty(e_id))
                            {
                                string New_EID = Guid.NewGuid().ToString("N");
                                string stre_id = "";
                                stre_id = "select * from REPORT_DATA_DEFINITION where EXEC_IDENTIFIER='" + e_id + "'";
                                using (OleDbDataAdapter adp_EID = new OleDbDataAdapter(stre_id, conNetezza))
                                {
                                    using (DataSet dsE_id = new DataSet())
                                    {
                                        adp_EID.Fill(dsE_id);
                                        int j = 0;
                                        if (dsE_id.Tables[0].Rows.Count > 0)
                                        {
                                            for (j = 0; j < dsE_id.Tables[0].Rows.Count; j++)
                                            {
                                                if (dsE_id.Tables[0].Rows[j]["Parameter_ID"].ToString() == "1")
                                                {
                                                    NewStartDate = NewStartDate.Replace("Start_Date=", "");
                                                    InsertReportParameters_Call(New_EID, Convert.ToInt32(dsE_id.Tables[0].Rows[j]["Parameter_ID"]), NewStartDate);
                                                }
                                                else if (dsE_id.Tables[0].Rows[j]["Parameter_ID"].ToString() == "2")
                                                {
                                                    NewEndDate = NewEndDate.Replace("End_Date=", "");
                                                    InsertReportParameters_Call(New_EID, Convert.ToInt32(dsE_id.Tables[0].Rows[j]["Parameter_ID"]), NewEndDate);
                                                }
                                                else if (dsE_id.Tables[0].Rows[j]["Parameter_ID"].ToString() == "10004" || dsE_id.Tables[0].Rows[j]["Parameter_ID"].ToString() == "10005")
                                                {
                                                    if (dsE_id.Tables[0].Rows[j]["Parameter_Value"].ToString().Contains("From"))
                                                    {
                                                        NewStartDate = NewStartDate.Replace("Start_Date=", "");
                                                        NewEndDate = NewEndDate.Replace("End_Date=", "");
                                                        string DateParam = "From: " + Convert.ToDateTime(NewStartDate).ToString("MM/dd/yyyy") + " To " + Convert.ToDateTime(NewEndDate).ToString("MM/dd/yyyy");
                                                        InsertReportParameters_Call(New_EID, Convert.ToInt32(dsE_id.Tables[0].Rows[j]["Parameter_ID"]), DateParam);
                                                    }
                                                    else
                                                        InsertReportParameters_Call(New_EID, Convert.ToInt32(dsE_id.Tables[0].Rows[j]["Parameter_ID"]), Convert.ToString(dsE_id.Tables[0].Rows[j]["Parameter_Value"]));
                                                }
                                                else
                                                    InsertReportParameters_Call(New_EID, Convert.ToInt32(dsE_id.Tables[0].Rows[j]["Parameter_ID"]), Convert.ToString(dsE_id.Tables[0].Rows[j]["Parameter_Value"]));
                                            }
                                            InsertParametersforDistributionGap(New_EID);
                                            FInalRepParam = "e_id=" + New_EID;
                                        }
                                    }
                                }
                            }
                        }

                        return FInalRepParam;
                    }
                    else
                    {
                        return Parameter.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GetUpdatedParameters()");
            }
            return Parameter.ToString();
        }
        /// <summary>
        /// Get param LY date
        /// </summary>
        private static string GetParamLYDate(string StartDate, string StartOrEnd)
        {
            string ParamDate = "";
            try
            {
                using (DataTable dt = new DataTable())
                {
                    string strQuery = "SELECT distinct LY_FISCAL_WEEK_START_DT, LY_FISCAL_WEEK_END_DT ";
                    strQuery += " FROM DIM_CALENDAR Where 'FY' || FISCAL_YEAR_NUM || case when Length(FISCAL_WEEK_OF_YEAR_NUM) =1 then  '-W0' || FISCAL_WEEK_OF_YEAR_NUM else '-W' || FISCAL_WEEK_OF_YEAR_NUM end =  '" + StartDate + "'";
                    using (OleDbDataAdapter adp = new OleDbDataAdapter(strQuery, strConnNetezza))
                    {
                        adp.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (StartOrEnd == "Start")
                                ParamDate = Convert.ToDateTime(dr[0].ToString()).ToString("yyyy-MM-dd");
                            else
                                ParamDate = Convert.ToDateTime(dr[1].ToString()).ToString("yyyy-MM-dd");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GetParamLYDate()");
            }
            return ParamDate;
        }
        /// <summary>
        /// Get param date
        /// </summary>
        private static string GetParamDate(string StartDate)
        {
            try
            {
                string ParamDate = "";
                using (DataTable dt = new DataTable())
                {
                    string strQuery = "SELECT distinct FISCAL_WEEK_START_DT, FISCAL_WEEK_END_DT ";
                    strQuery += " FROM DIM_CALENDAR Where 'FY' || FISCAL_YEAR_NUM || case when Length(FISCAL_WEEK_OF_YEAR_NUM) =1 then  '-W0' || FISCAL_WEEK_OF_YEAR_NUM else '-W' || FISCAL_WEEK_OF_YEAR_NUM end =  '" + StartDate + "'";
                    using (OleDbDataAdapter adp = new OleDbDataAdapter(strQuery, strConnNetezza))
                    {
                        adp.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {
                            ParamDate = Convert.ToDateTime(dr[0].ToString()).ToString("yyyy-MM-dd");
                        }
                    }
                }
                return ParamDate;
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GetParamDate()");
                throw ex;
            }
        }
        /// <summary>
        /// Get end date
        /// </summary>
        private static string GetEnddate(string Enddate)
        {
            try
            {
                string ParamDate = "";
                using (DataTable dt = new DataTable())
                {
                    string strQuery = "SELECT distinct FISCAL_WEEK_START_DT, FISCAL_WEEK_END_DT ";
                    strQuery += " FROM DIM_CALENDAR Where 'FY' || FISCAL_YEAR_NUM || case when Length(FISCAL_WEEK_OF_YEAR_NUM) =1 then  '-W0' || FISCAL_WEEK_OF_YEAR_NUM else '-W' || FISCAL_WEEK_OF_YEAR_NUM end =  '" + Enddate + "'";
                    using (OleDbDataAdapter adp = new OleDbDataAdapter(strQuery, strConnNetezza))
                    {
                        adp.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {
                            ParamDate = Convert.ToDateTime(dr[1].ToString()).ToString("yyyy-MM-dd");
                        }
                    }
                }
                return ParamDate;
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GetEnddate()");
                throw ex;
            }
        }
        /// <summary>
        /// Get Fiscal date
        /// </summary>
        private static string GetFiscalDatesW0(string StartDate, string EndDate)
        {
            string FiscalWeeks = "";
            try
            {

                using (DataTable dt = new DataTable())
                {
                    string strQuery = "SELECT 'FY' || FISCAL_YEAR_NUM-2000 || case when Length(FISCAL_WEEK_OF_YEAR_NUM) =1 then  '-W0' || FISCAL_WEEK_OF_YEAR_NUM else '-W' || FISCAL_WEEK_OF_YEAR_NUM end || ',FY' || FISCAL_YEAR_NUM-2000-1 || case when Length(FISCAL_WEEK_OF_YEAR_NUM) =1 then  '-W0' || FISCAL_WEEK_OF_YEAR_NUM else '-W' || FISCAL_WEEK_OF_YEAR_NUM end as [ParamValue] FROM DIM_WEEKLY_CALENDAR Where WEEK_START_DATE between '" + StartDate + "' and '" + EndDate + "' order by WEEK_START_DATE desc";
                    using (OleDbDataAdapter adp = new OleDbDataAdapter(strQuery, strConnNetezza))
                    {
                        adp.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {
                            FiscalWeeks += dr[0].ToString() + ",";
                        }

                        if (!String.IsNullOrEmpty(FiscalWeeks))
                            FiscalWeeks = FiscalWeeks.TrimEnd(',');

                    }
                }

            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GetFiscalDatesW0()");
            }
            return FiscalWeeks;
        }

        #region SPCALL Changes and Distribution Gap Reports
        private static DateTime StartDate()
        {
            DateTime SundayBeforeLast = EndDate().AddDays(-6);
            return SundayBeforeLast;
        }

        public static void InsertReportParameters_Call(string Exec_id, int Parameter_Id, string Filters)
        {
            try
            {
                OleDbParameter[] arParam = new OleDbParameter[3];
                arParam[0] = new OleDbParameter("p_ExecId", SqlDbType.VarChar);
                arParam[0].Value = Exec_id.ToString();

                arParam[1] = new OleDbParameter("p_Parameter_id", SqlDbType.Int);
                arParam[1].Value = Parameter_Id;

                arParam[2] = new OleDbParameter("p_Parameter_Value", SqlDbType.VarChar);
                arParam[2].Value = Filters;

                ExecuteNonQueryNetezza(arParam, "SP_INSERTREPORT_PARAMETERS", strConnNetezza);
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "InsertReportParameters_Call()");
                throw;
            }
        }
        public static void InsertReportParameters_Call_Prod(string Exec_id, int Parameter_Id, string Filters)
        {
            try
            {
                OleDbParameter[] arParam = new OleDbParameter[3];
                arParam[0] = new OleDbParameter("p_ExecId", SqlDbType.VarChar);
                arParam[0].Value = Exec_id.ToString();

                arParam[1] = new OleDbParameter("p_Parameter_id", SqlDbType.Int);
                arParam[1].Value = Parameter_Id;

                arParam[2] = new OleDbParameter("p_Parameter_Value", SqlDbType.VarChar);
                arParam[2].Value = Filters;

                ExecuteNonQueryNetezza(arParam, "SP_INSERTREPORT_PARAMETERS", strConnNetezza);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string GetClosedFiscalPeriod(string CurrDate)
        {
            string RCDatePrd = "";
            try
            {
                string strSql;
                strSql = "select 'FY' || FISCAL_YEAR_NUM || '-P' || FISCAL_PERIOD_OF_YEAR_NUM  as RCP from Dim_Calendar where FISCAL_PERIOD_END_DT = (Select FISCAL_PERIOD_START_DT - interval '1 days' from Dim_Calendar Where DAY_DT= '" + CurrDate + "') limit 1;";
                using (DataTable dt = new DataTable())
                {
                    using (OleDbDataAdapter adp = new OleDbDataAdapter(strSql, strConnNetezza))
                    {
                        adp.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            RCDatePrd = Convert.ToString(dt.Rows[0][0]);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GetClosedFiscalPeriod()");
                throw ex;
            }
            return RCDatePrd;
        }

        private static string GetClosedFiscalQuarter(string CurrDate)
        {
            string RCDatePrd = "";
            try
            {
                string strSql;
                strSql = "select 'FY' || FISCAL_YEAR_NUM || '-Q' || FISCAL_QRTR_OF_YEAR_NUM  as RCQ from Dim_Calendar where FISCAL_QRTR_END_DT = (Select FISCAL_QRTR_START_DT - interval '1 days' from Dim_Calendar Where DAY_DT= '" + CurrDate + "') limit 1;";
                using (DataTable dt = new DataTable())
                {
                    using (OleDbDataAdapter adp = new OleDbDataAdapter(strSql, strConnNetezza))
                    {
                        adp.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            RCDatePrd = Convert.ToString(dt.Rows[0][0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GetClosedFiscalQuarter()");
                throw ex;
            }
            return RCDatePrd;
        }

        private static void ExecuteNonQueryNetezza(OleDbParameter[] arParam, string strProcName, string Conn)
        {
            try
            {
                using (OleDbCommand cmd = new OleDbCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = strProcName;
                    AttachParameters(cmd, arParam);
                    ExecuteCommand(cmd, Conn);
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "ExecuteNonQueryNetezza()");
                throw ex;
            }
        }

        private static void AttachParameters(OleDbCommand command, OleDbParameter[] commandParameters)
        {
            command.Parameters.Clear();

            if (command == null) throw new ArgumentNullException("command");
            if (commandParameters != null)
            {
                foreach (OleDbParameter p in commandParameters)
                {
                    if (p != null)
                    {
                        command.Parameters.Add(p);
                    }
                }
            }
        }

        private static void ExecuteCommand(OleDbCommand cmd, string connNetezza)
        {
            using (OleDbConnection con = new OleDbConnection(connNetezza))
            {
                try
                {
                    con.Open();
                    cmd.CommandTimeout = 1200;
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }
        private static void InsertParametersforDistributionGap(string EXEC_IDENTIFIER)
        {
            try
            {
                Int64 RowCount = 0;
                using (OleDbConnection Conn = new OleDbConnection(strConnNetezza))
                {
                    string qry = "exec P_SALES_OPPORTUNITY(";
                    qry += " '" + EXEC_IDENTIFIER + "'";
                    qry += " );";
                    using (OleDbCommand cmd = new OleDbCommand(qry, Conn))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 1200;
                        Conn.Open();
                        RowCount = Convert.ToInt64(cmd.ExecuteScalar());
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "InsertParametersforDistributionGap()");
            }
        }

        #endregion
        /// <summary>
        /// Get seleceted parameters
        /// </summary>
        private static string GetSelectedParameters(string Parameter, string DateRange)
        {
            try
            {
                using (OleDbConnection conNetezza = new OleDbConnection(strConnNetezza))
                {
                    string OldStartDate = "";
                    string OldEndDate = "";
                    string NewStartDate = "From Date #~#";
                    string NewEndDate = "To Date #~#";

                    string FromAdWeek = "From AdWeek #~#";
                    string ToAdWeek = "To AdWeek #~#";

                    string OldFromAdWeek = "";
                    string OldToAdWeek = "";
                    string ForStartEndDate = "";
                    string EmptyStr = "";
                    string Adweek = "";

                    string[] DateFilters;
                    string[] RepPar = Parameter.Split(new string[] { "~#~" }, StringSplitOptions.None);

                    if (!String.IsNullOrEmpty(DateRange))
                    {
                        DateFilters = DateRange.ToString().Split('-');
                        if (DateFilters[0].ToString() == "W")
                        {
                            NewStartDate += StartDateForSaveFilterForWeek(Convert.ToInt16(DateFilters[1]) * 7).ToString("yyyy-MM-dd");
                            NewEndDate += EndDate().ToString("yyyy-MM-dd");
                        }
                        int i = 0;
                        for (i = 0; i < RepPar.Length; i++)
                        {
                            if (RepPar[i].ToString().Contains("From Date"))
                            {
                                OldStartDate = RepPar[i].ToString();
                            }
                            if (RepPar[i].ToString().Contains("To Date"))
                            {
                                EmptyStr = "1";
                                ForStartEndDate = "1";
                                OldEndDate = RepPar[i].ToString();
                            }

                            if (RepPar[i].ToString().Contains("From AdWeek"))
                            {
                                OldFromAdWeek = RepPar[i].ToString();
                                if (DateFilters.Length > 1)
                                {

                                    DateTime Enddate = EndDate();
                                    NewStartDate = StartDateForSaveFilterForWeek(Convert.ToInt16(DateFilters[1]) * 7, Enddate).ToString("yyyy-MM-dd");
                                    string Fromdtsql = "SELECT 'FY' || FISCAL_YEAR_NUM || case when Length(FISCAL_WEEK_OF_YEAR_NUM) =1 then  '-W0' ||FISCAL_WEEK_OF_YEAR_NUM  else '-W' || FISCAL_WEEK_OF_YEAR_NUM end as [Param] FROM DIM_WEEKLY_CALENDAR Where WEEK_START_DATE ='" + NewStartDate + "' order by WEEK_START_DATE desc";
                                    using (OleDbDataAdapter adp = new OleDbDataAdapter(Fromdtsql, conNetezza))
                                    {
                                        using (DataTable dt = new DataTable())
                                        {
                                            adp.Fill(dt);
                                            if (dt.Rows.Count > 0)
                                            {
                                                FromAdWeek += dt.Rows[0]["Param"].ToString();
                                            }
                                        }
                                    }
                                }
                            }

                            if (RepPar[i].ToString().Contains("To AdWeek"))
                            {
                                OldToAdWeek = RepPar[i].ToString();
                                if (DateFilters.Length > 1)
                                {
                                    string ToDtSql = "SELECT 'FY' || FISCAL_YEAR_NUM || case when Length(FISCAL_WEEK_OF_YEAR_NUM) =1 then  '-W0' ||FISCAL_WEEK_OF_YEAR_NUM  else '-W' || FISCAL_WEEK_OF_YEAR_NUM end as [Param] FROM DIM_WEEKLY_CALENDAR Where FISCAL_WEEK_END_DT ='" + EndDate().ToString("yyyy-MM-dd") + "' order by FISCAL_WEEK_END_DT desc";
                                    using (OleDbDataAdapter adp = new OleDbDataAdapter(ToDtSql, conNetezza))
                                    {
                                        using (DataTable dt = new DataTable())
                                        {
                                            adp.Fill(dt);
                                            if (dt.Rows.Count > 0)
                                            {
                                                EmptyStr = "1";
                                                Adweek = "1";
                                                ToAdWeek += dt.Rows[0]["Param"].ToString();
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        string FInalRepParam = "";
                        if (!String.IsNullOrEmpty(ForStartEndDate))
                        {
                            if (!String.IsNullOrEmpty(OldStartDate))
                            {
                                FInalRepParam = Parameter.ToString().Replace(OldStartDate, NewStartDate);
                            }
                            if (!String.IsNullOrEmpty(OldEndDate) && String.IsNullOrEmpty(OldStartDate))
                            {
                                FInalRepParam = Parameter.ToString().Replace(OldEndDate, NewEndDate);
                            }
                            if (!String.IsNullOrEmpty(OldEndDate) && !String.IsNullOrEmpty(OldStartDate))
                            {
                                FInalRepParam = FInalRepParam.Replace(OldEndDate, NewEndDate);
                            }
                        }
                        if (!String.IsNullOrEmpty(Adweek))
                        {
                            FInalRepParam = Parameter.ToString().Replace(OldFromAdWeek, FromAdWeek).Replace(OldToAdWeek, ToAdWeek);
                        }

                        if (String.IsNullOrEmpty(EmptyStr))
                        {
                            FInalRepParam = Parameter.ToString();
                        }
                        Adweek = "";
                        ForStartEndDate = "";
                        EmptyStr = "";

                        return FInalRepParam;
                    }
                    else
                    {
                        return Parameter.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GetUpdatedParameters()");
            }
            return Parameter.ToString();
        }
        /// <summary>
        /// Get Start Date For Save Filter For Week
        /// </summary>
        private static DateTime StartDateForSaveFilterForWeek(int days, DateTime Enddate)
        {
            DateTime SundayBeforeLast = Enddate.AddDays(-days).AddDays(1);
            return SundayBeforeLast;
        }
        /// <summary>
        /// Get Start Date For Save Filter For Week
        /// </summary>
        private static DateTime StartDateForSaveFilterForWeek(int days)
        {
            DateTime SundayBeforeLast = EndDate().AddDays(-days).AddDays(1);
            return SundayBeforeLast;
        }
        /// <summary>
        /// Get end Date
        /// </summary>
        private static DateTime EndDate()
        {
            DateTime StartOfWeek = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek);
            DateTime LastSaturday = StartOfWeek.AddDays(-1);
            return LastSaturday;
        }
        /// <summary>
        /// Get calculate next exec date
        /// </summary>
        private static void CalculateNextExecDate(Int16 scheduleid)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(strConn))
                {
                    string str = "select * from SavedReportsSchedule where scheduleid='" + scheduleid + "'";
                    using (SqlDataAdapter adp = new SqlDataAdapter(str, con))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            adp.Fill(ds);
                            int calculateweekly = 0;
                            int Getcurrentweekno = getcurrentweekno();
                            string dayno = ds.Tables[0].Rows[0]["Daysforweekandmonth"].ToString().TrimEnd(',');
                            DateTime nextexecdate;
                            string[] SubDays = dayno.Split(',');
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                // For Weekly Calculations

                                if (ds.Tables[0].Rows[0]["NewRecur"].ToString() == "W")
                                {
                                    int nextexecday = 0;
                                    if (SubDays.Length > 0)
                                    {
                                        int i = 0;
                                        for (i = 0; i < SubDays.Length; i++)
                                        {
                                            if (Convert.ToInt16(SubDays[i]) > Getcurrentweekno)
                                            {
                                                int diffdays = Convert.ToInt16(SubDays[i]) - Getcurrentweekno;
                                                nextexecday = Convert.ToInt16(SubDays[i]);
                                                nextexecdate = System.DateTime.Now.AddDays(diffdays);
                                                if (nextexecdate > Convert.ToDateTime(ds.Tables[0].Rows[0]["NextExecDate"]))
                                                {
                                                    calculateweekly = diffdays;
                                                    UpdateNextExecDate(nextexecdate, Convert.ToInt16(ds.Tables[0].Rows[0]["Scheduleid"]));
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                    if (calculateweekly == 0)
                                    {
                                        var sunday = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek);
                                        DateTime dateafterweekadd = sunday.AddDays(Convert.ToInt16(ds.Tables[0].Rows[0]["RecurEvery"]) * 7);
                                        if (SubDays.Length > 0)
                                        {
                                            int i = 0;
                                            for (i = 0; i < SubDays.Length; i++)
                                            {
                                                nextexecday = Convert.ToInt16(SubDays[i]);
                                                nextexecdate = dateafterweekadd.AddDays(nextexecday - 1);
                                                if (nextexecdate > Convert.ToDateTime(ds.Tables[0].Rows[0]["NextExecDate"]))
                                                {
                                                    UpdateNextExecDate(nextexecdate, Convert.ToInt16(ds.Tables[0].Rows[0]["Scheduleid"]));
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }

                                // For Daily Calculations
                                if (ds.Tables[0].Rows[0]["NewRecur"].ToString() == "D")
                                {
                                    nextexecdate = System.DateTime.Now.AddDays(Convert.ToInt16(ds.Tables[0].Rows[0]["RecurEvery"]));
                                    UpdateNextExecDate(nextexecdate, Convert.ToInt16(ds.Tables[0].Rows[0]["Scheduleid"]));
                                    return;
                                }

                                if (ds.Tables[0].Rows[0]["NewRecur"].ToString() == "M")
                                {
                                    DateTime Recur;
                                    if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["weeknoformonth"])))
                                    {
                                        Recur = new DateTime(DateTime.Now.AddMonths(1).Year, DateTime.Now.AddMonths(1).Month, 1);
                                        while (Recur.DayOfWeek != DayOfWeek.Monday)
                                        {
                                            Recur = Recur.AddDays(1);
                                        }
                                        UpdateNextExecDate(Recur, Convert.ToInt16(ds.Tables[0].Rows[0]["Scheduleid"]));
                                        return;
                                    }
                                    else
                                    {
                                        Recur = new DateTime(DateTime.Now.AddMonths(1).Year, DateTime.Now.AddMonths(1).Month, 1);
                                        while (Recur.DayOfWeek != DayOfWeek.Monday)
                                        {
                                            Recur = Recur.AddDays(1);
                                        }
                                        UpdateNextExecDate(Recur, Convert.ToInt16(ds.Tables[0].Rows[0]["Scheduleid"]));
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "CalculateNextExecDate()");
            }
        }
        /// <summary>
        /// update next exec date
        /// </summary>
        private static void UpdateNextExecDate(DateTime NextExecDate, Int16 Scheduleid)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(strConn))
                {
                    using (SqlCommand cmd = new SqlCommand("spUpdateReportSchedule", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@NextExecDate", SqlDbType.DateTime).Value = NextExecDate.ToString("yyyy-MM-dd hh:mm:ss");
                        cmd.Parameters.Add("@LastExecDate", SqlDbType.DateTime).Value = System.DateTime.Now;
                        cmd.Parameters.Add("@Scheduleid", SqlDbType.Int).Value = Scheduleid;
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "UpdateNextExecDate()");
            }
        }
        /// <summary>
        /// Get current week no
        /// </summary>
        private static int getcurrentweekno()
        {
            var currentdayno = 0;
            try
            {
                if ((int)System.DateTime.Now.DayOfWeek == (int)DayOfWeek.Monday)
                    currentdayno = 2;
                if ((int)System.DateTime.Now.DayOfWeek == (int)DayOfWeek.Tuesday)
                    currentdayno = 3;
                if ((int)System.DateTime.Now.DayOfWeek == (int)DayOfWeek.Wednesday)
                    currentdayno = 4;
                if ((int)System.DateTime.Now.DayOfWeek == (int)DayOfWeek.Thursday)
                    currentdayno = 5;
                if ((int)System.DateTime.Now.DayOfWeek == (int)DayOfWeek.Friday)
                    currentdayno = 6;
                if ((int)System.DateTime.Now.DayOfWeek == (int)DayOfWeek.Saturday)
                    currentdayno = 7;
                if ((int)System.DateTime.Now.DayOfWeek == (int)DayOfWeek.Sunday)
                    currentdayno = 1;
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "getcurrentweekno()");

            }
            return currentdayno;
        }


        /// <summary>
        /// update schedule
        /// </summary>
        private static void UpdateSchedule()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(strConn))
                {
                    string str = "select * from savedreportsschedule";
                    using (SqlDataAdapter adp = new SqlDataAdapter(str, con))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            adp.Fill(ds);

                            int i = 0;
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (i = 0; i < ds.Tables[0].Rows.Count; i++)
                                {
                                    CalculateNextExecDate(Convert.ToInt16(ds.Tables[0].Rows[i]["Scheduleid"]));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "UpdateSchedule()");
            }
        }
        /// <summary>
        /// Get report name
        /// </summary>
        private static string GetReportName(int menuId)
        {
            string reportName = "";
            try
            {
                if (menuId > 0)
                {
                    string query = "SELECT RFM.ORDER_NO,CASE WHEN ((@ReportId=1303 OR @ReportId=1304 OR @ReportId=1307) AND RFM.ORDER_NO>6) then 'New Items' else  FG.FILTERGROUP_NAME end as FILTERGROUP_NAME  ,FG.FILTERGROUP_ID," +
                                           " FD.FILTER_NAME,FD.FILTER_LABEL_NAME,CD.CONTROL_NAME AS controlName,RFM.IS_MULTISELECT_DATECONTROL,FD.FILTER_ID, " +
                                             "CASE WHEN FD.DEPENDENCY IN (SELECT FILTER_ID FROM REPORT_FILTER_MAPPING WHERE REPORT_ID = @ReportId) THEN FD.DEPENDENCY " +
                                              "ELSE 0 END AS DEPENDENCY,FD.EVENT_NAME,RFM.IS_REQUIRED,FG2.CSSCLASS_NAME,FD.HIERARCHY_TYPE_ID,FD.PARAMETER_NAME,RD.REPORT_HEADER,RD.REPORT_NAME,RD.BREAD_CRUMB,RD.EXTRA_PARAMETERS " +
                                              "FROM REPORT_FILTER_MAPPING RFM " +
                                            "INNER JOIN CHETU.DYN_FILTER_DETAILS FD ON FD.FILTER_ID = RFM.FILTER_ID " +
                                            "INNER JOIN CHETU.DYN_FILTER_GROUP FG ON FG.FILTERGROUP_ID = FD.FILTER_GROUP_ID " +
                                            "INNER JOIN CHETU.DYN_FILTER_CONTROLS CD ON CD.ID = FD.CONTROL_TYPE_ID " +
                                        "INNER JOIN DYN_REPORT_DETAIL RD ON RFM.REPORT_ID = RD.REPORT_ID " +
                                            "LEFT JOIN CHETU.DYN_FILTER_GROUP FG2 ON FD.HIERARCHY_TYPE_ID = FG2.FILTERGROUP_ID " +
                                                "WHERE RFM.REPORT_ID = @ReportId AND FD.ISACTIVE = '1' " +
                                            "ORDER BY RFM.ORDER_NO";

                    query = query.Replace("@ReportId", string.Format("{0}", menuId));
                    using (OleDbConnection con = new OleDbConnection(strConnNetezza))
                    {
                        using (OleDbDataAdapter adp = new OleDbDataAdapter(query, con))
                        {
                            using (DataTable ds = new DataTable())
                            {
                                adp.Fill(ds);
                                if (ds.Rows.Count > 0)
                                    reportName = ds.Rows[0]["REPORT_NAME"].ToString();
                            }
                        }
                    }


                    return reportName.Replace(" ", "").Replace("-", "");
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GetReportName()");
                return "";
            }
        }

    }
}
