﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BatchingTaskScheduler
{
    public class BatchingTaskScheduler
    {
        private static string LogPath = Convert.ToString(ConfigurationManager.AppSettings["LogPath"]);
        private static string strConn = Convert.ToString(ConfigurationManager.ConnectionStrings["DataTrue_DataManager"]);

        private static string ReportPath = Convert.ToString(ConfigurationManager.AppSettings["ReportPath"]);
        private static string ReportPathforAttach = Convert.ToString(ConfigurationManager.AppSettings["ReportPathforAttach"]);
        private static string BatchFilePath = Convert.ToString(ConfigurationManager.AppSettings["BatchFilePath"]);
        private static string CommandPrompt = Convert.ToString(ConfigurationManager.AppSettings["CommandPrompt"]);
        private static string TableauDir = Convert.ToString(ConfigurationManager.AppSettings["TableauDir"]);
        private static string TableauHost = Convert.ToString(ConfigurationManager.AppSettings["TableauHostDev"]);
        private static string TableauServer = Convert.ToString(ConfigurationManager.AppSettings["TableauHostProd"]);
        private static string SavedReportsPath = Convert.ToString(ConfigurationManager.AppSettings["SavedReportsPath"]);

        private static string TableauUname = "";
        private static string TableauPassword = "";
        private static string UserEmailId = "";
        private static string UserProfile = "";
        private static string ErrorStatus = "";
        private static string Name = "";
        private static string ReportFrequency = "";
        private static int ExecutionCount;
        private static string EmailFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();
        private static string BCCEmailId = Convert.ToString(ConfigurationManager.AppSettings["BCCEmailId"]);
        static void Main(string[] args)
        {

            if (!Directory.Exists(ReportPath))
            {
                Directory.CreateDirectory(ReportPath);
            }
            if (!Directory.Exists(BatchFilePath))
            {
                Directory.CreateDirectory(BatchFilePath);
            }

            GenerateBatchReports();
            GenerateSubscriptionReports();
        }

        #region Subscription Reports
        /// <summary>
        ///Generate batching subscription report
        /// </summary>
        public static void GenerateSubscriptionReports()
        {
            int ReportStatus = 0;
            string SelectedParameters = "";
            try
            {
                DataTable dtReports = GetReportsToSend("S", TableauServer);
                foreach (DataRow dr in dtReports.Rows)
                {
                    TableauUname = dr["UserName"].ToString();
                    TableauPassword = dr["TableauPwd"].ToString();
                    UserEmailId = dr["EmailId"].ToString();
                    UserProfile = dr["UserProfile"].ToString();
                    SelectedParameters = dr["SelectedFilters"].ToString();
                    string EmailSubject = dr["ReportName"].ToString();
                    string ReportName = dr["OriginalReportName"].ToString().Replace("->", "").Replace("%", "") + System.DateTime.Now.ToString("ddMMyyhhmmss");
                    string OrignalReportName = dr["OriginalReportName"].ToString();
                    string FileName = dr["FileName"].ToString();
                    string FolderName = "";
                    if (dr["Folder_Name"] != null)
                    {
                        FolderName = dr["Folder_Name"].ToString();
                    }

                    if (dr["ExecutionCount"] != null)
                    {
                        ExecutionCount = Convert.ToInt32(dr["ExecutionCount"]);
                    }

                    if (dr["Name"] != null)
                    {
                        Name = dr["Name"].ToString();
                    }
                    else
                    {
                        Name = "User";
                    }

                    if (dr["ReportFrequency"] != null)
                    {
                        ReportFrequency = dr["ReportFrequency"].ToString();
                    }
                    else
                    {
                        ReportFrequency = "";
                    }
                    string ReportParamName;
                    if (UserProfile.Contains("WithoutVendorNumber"))
                        ReportParamName = dr["ParamName"].ToString().Replace("_{RoleId}", UserProfile);
                    else if (String.IsNullOrEmpty(UserProfile))
                        ReportParamName = dr["ParamName"].ToString().Replace("_{RoleId}", "");
                    else
                        ReportParamName = dr["ParamName"].ToString().Replace("{RoleId}", UserProfile);

                    string ReportParameters = dr["ReportParameters"].ToString() + "\"";


                    string[] cmdBuilder = new string[5] {
                                        @"cd " + TableauDir,
                                        @"tabcmd login -s " + TableauHost + " -u " + TableauUname + " -p " + TableauPassword,
                                        @"tabcmd get " + "\"" + TableauHost + "/views/" + ReportParamName + ".pdf?" + ReportParameters + " -f  "+ "\"" + ReportPath + ReportName + ".pdf" + "\" --timeout 1200",
                                        @"tabcmd logout",
                                         @"Exit"
                                       };


                    string BatFile = BatchFilePath + System.DateTime.Now.ToString("ddMMyyyyhhmmss") + ".bat";
                    using (StreamWriter sWriter = new StreamWriter(BatFile))
                    {
                        foreach (string cmd in cmdBuilder)
                        {
                            sWriter.WriteLine(cmd);
                        }
                    }
                    try
                    {
                        using (Process p = new Process())
                        {
                            p.StartInfo.CreateNoWindow = true;
                            p.StartInfo.UseShellExecute = false;
                            p.StartInfo.FileName = CommandPrompt;
                            p.StartInfo.Arguments = "/c start /wait " + BatFile + " ";
                            p.StartInfo.RedirectStandardError = true;
                            p.Start();
                            p.StandardError.ReadToEnd();
                            p.WaitForExit();
                        }

                        File.Delete(BatFile);

                        if (File.Exists(ReportPathforAttach + ReportName + ".pdf"))
                        {
                            ReportStatus = 2; // Status 2 = Sucessfully sent
                            if (String.IsNullOrEmpty(FolderName)) //Subscription report
                                SendEmailToUser(UserEmailId, BCCEmailId, ReportPathforAttach + ReportName + ".pdf", ReportName, "S", SelectedParameters, OrignalReportName, Name, ReportFrequency, EmailSubject);
                            else                //Save to Folder report
                            {
                                //Save report path in table column
                                CopyFile(ReportPathforAttach + ReportName + ".pdf", Path.Combine(SavedReportsPath, ReportName + ".pdf"));
                                UpdateSaveToFolderReportPath(Convert.ToInt16(dr["ReportId"]), "S", ReportName + ".pdf");
                                SendEmailToUser_SaveToFolder(UserEmailId, BCCEmailId, FolderName, ReportPathforAttach + ReportName + ".pdf", FileName, SelectedParameters, Name, OrignalReportName);
                            }
                        }
                        else
                        {
                            ReportStatus = 3; // Error
                            ErrorStatus = "Error in generating the file.";
                            WriteLogFiles(ErrorStatus, ErrorStatus, "GenerateSubscriptionReports()");

                            if (ExecutionCount == 2)    // Send email to user on last attempt
                            {
                                SendEmailToUser(UserEmailId, BCCEmailId, "", ReportName, "S", SelectedParameters, OrignalReportName, Name, ReportFrequency, EmailSubject);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ReportStatus = 3; // Error
                        ErrorStatus = "Error: " + ex.Message.ToString();
                        WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GenerateReport()");
                    }
                    UpdateReportStatus(ReportStatus, Convert.ToInt16(dr["ReportId"]), ErrorStatus, Convert.ToString(dr["EmailID"]), Convert.ToInt32(dr["ScheduleId"]), "S", Convert.ToInt32(dr["ExecutionCount"]));
                }
            }

            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GenerateSubscriptionReports()");
            }
        }
        #endregion

        #region Batch Reports
        /// <summary>
        ///Generate batching report
        /// </summary>
        public static void GenerateBatchReports()
        {
            int ReportStatus = 0;
            string SelectedParameters = "";
            try
            {

                DataTable dtReports = GetReportsToSend("B", TableauServer);
                foreach (DataRow dr in dtReports.Rows)
                {
                    try
                    {
                        using (SqlConnection con = new SqlConnection(strConn))
                        {
                            string updateStarttime = BatchResource.UpdateBatchReport.Replace("@ReportId", string.Format("{0}", dr["ReportId"])); //"Update Batchreports Set ProcessStartTime=getdate() Where ReportId='" + dr["ReportId"] + "'";
                            using (SqlCommand cmdupdate = new SqlCommand())
                            {
                                cmdupdate.CommandText = updateStarttime;
                                cmdupdate.Connection = con;
                                con.Open();
                                cmdupdate.ExecuteNonQuery();
                            }
                            TableauUname = dr["UserName"].ToString();
                            TableauPassword = dr["TableauPwd"].ToString();
                            UserEmailId = dr["EmailID"].ToString();
                            UserProfile = dr["UserProfile"].ToString();
                            SelectedParameters = dr["SelectedFilters"].ToString();
                            string ReportName = dr["OriginalReportName"].ToString().Replace("->", "").Replace("%", "") + System.DateTime.Now.ToString("ddMMyyhhmmss");
                            string OrignalReportName = dr["OriginalReportName"].ToString();
                            string FileName = dr["FileName"].ToString();
                            string ReportParamName = "";
                            string FolderName = "";
                            if (dr["Folder_Name"] != null)
                            {
                                FolderName = dr["Folder_Name"].ToString();
                            }
                            if (dr["ExecutionCount"] != null)
                            {
                                ExecutionCount = Convert.ToInt32(dr["ExecutionCount"]);
                            }
                            if (dr["Name"] != null)
                            {
                                Name = dr["Name"].ToString();
                            }
                            else
                            {
                                Name = "User";
                            }
                            if (dr["ReportFrequency"] != null)
                            {
                                ReportFrequency = dr["ReportFrequency"].ToString();
                            }
                            else
                            {
                                ReportFrequency = "";
                            }

                            if (dr["SpCall"].ToString().Trim().Length > 0)
                            {

                                string strConnNetezzaEDWGrocery = "";
                                if (dr["DBConnection"] != null && dr["DBConnection"].ToString().Trim().Length > 0)
                                    strConnNetezzaEDWGrocery = dr["DBConnection"].ToString();
                                else
                                    strConnNetezzaEDWGrocery = ConfigurationManager.ConnectionStrings["DataTrue_EDWGrocery_Netezza"].ConnectionString.ToString();

                                using (OleDbConnection Conn = new OleDbConnection(strConnNetezzaEDWGrocery))
                                {
                                    using (SqlCommand cmdupdate = new SqlCommand())
                                    {
                                        cmdupdate.CommandText = updateStarttime;
                                        cmdupdate.Connection = con;
                                        con.Open();
                                        cmdupdate.ExecuteNonQuery();
                                    }
                                }
                            }

                            if (UserProfile.Contains("WithoutVendorNumber"))
                                ReportParamName = dr["ParamName"].ToString().Replace("_{RoleId}", UserProfile);
                            else if (String.IsNullOrEmpty(UserProfile))
                                ReportParamName = dr["ParamName"].ToString().Replace("_{RoleId}", "");
                            else
                                ReportParamName = dr["ParamName"].ToString().Replace("{RoleId}", UserProfile);


                            string ReportParameters = dr["ReportParameters"].ToString() + "\"";

                            string[] cmdBuilder = new string[5] {
                                        @"cd " + TableauDir,
                                        @"tabcmd login -s " + TableauHost + " -u " + TableauUname + " -p " + TableauPassword,
                                         @"tabcmd get " + "\"" + TableauHost + "/views/" + ReportParamName + ".pdf?" + ReportParameters + " -f  "+ "\"" + ReportPath + ReportName + ".pdf" + "\"",
                                        @"tabcmd logout",
                                        @"Exit"
                                       };

                            string BatFile = BatchFilePath + System.DateTime.Now.ToString("ddMMyyyyhhmmss") + ".bat";
                            using (StreamWriter sWriter = new StreamWriter(BatFile))
                            {
                                foreach (string cmd in cmdBuilder)
                                {
                                    sWriter.WriteLine(cmd);
                                }
                            }
                            try
                            {
                                using (Process p = new Process())
                                {
                                    p.StartInfo.CreateNoWindow = true;
                                    p.StartInfo.UseShellExecute = false;
                                    p.StartInfo.FileName = CommandPrompt;
                                    p.StartInfo.Arguments = "/c start /wait " + BatFile + " ";
                                    p.Start();
                                    ErrorStatus = "Success";
                                    p.WaitForExit();
                                }

                                File.Delete(BatFile);

                                if (File.Exists(ReportPathforAttach + ReportName + ".pdf"))
                                {
                                    ReportStatus = 2; // Status 2 = Sucessfully sent

                                    if (String.IsNullOrEmpty(FolderName)) //Batch report
                                        SendEmailToUser(UserEmailId, BCCEmailId, ReportPathforAttach + ReportName + ".pdf", ReportName, "B", SelectedParameters, OrignalReportName, Name, ReportFrequency, OrignalReportName);
                                    else                //Save to Folder report
                                    {
                                        //Save report path in table column
                                        CopyFile(ReportPathforAttach + ReportName + ".pdf", Path.Combine(SavedReportsPath, ReportName + ".pdf"));
                                        UpdateSaveToFolderReportPath(Convert.ToInt16(dr["ReportId"]), "B", ReportName + ".pdf");
                                        SendEmailToUser_SaveToFolder(UserEmailId, BCCEmailId, FolderName, ReportPathforAttach + ReportName + ".pdf", FileName, SelectedParameters, Name, OrignalReportName);
                                    }
                                }
                                else
                                {
                                    ReportStatus = 3; // Error
                                    ErrorStatus = "Error in generating the file.";
                                    WriteLogFiles(ErrorStatus, ErrorStatus, "GenerateBatchReports()");

                                    if (ExecutionCount == 2)    // Send email to user on last attempt
                                    {
                                        SendEmailToUser(UserEmailId, BCCEmailId, "", ReportName, "B", SelectedParameters, OrignalReportName, Name, ReportFrequency, OrignalReportName);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                ReportStatus = 3; // Error
                                ErrorStatus = "Error: " + ex.Message.ToString();
                                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GenerateReport()");
                            }

                            UpdateReportStatus(ReportStatus, Convert.ToInt16(dr["ReportId"]), ErrorStatus, Convert.ToString(dr["EmailID"]), Convert.ToInt32(dr["ScheduleId"]), "B", Convert.ToInt32(dr["ExecutionCount"]));
                        }
                    }
                    catch (Exception ex)
                    {
                        WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GenerateReport()");
                    }
                }
            }

            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GenerateBatchReports()");
            }
        }
        #endregion

        #region Other methods

        /// <summary>
        /// copy file to destination path
        /// </summary>
        private static void CopyFile(string SourceFileName, string DestinationFileName)
        {
            try
            {
                File.Copy(SourceFileName, DestinationFileName);
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "CopyFile()");
            }
        }
        /// <summary>
        /// send email to user
        /// </summary>
        public static bool SendEmailToUser(string UserEmailId, string BCCEmailId, string FilePath, string ReportName, string ReportType, string SelectedParameters, string OrignalReportName, string Name, string ReportFrequency, string EmailSubject)
        {
            bool EmailStatus = false;
            try
            {
                MailMessage mail = new MailMessage();
                // check for emailids repeatation
                List<string> EmailIds = UserEmailId.Split(',').ToList(); ;
                HashSet<string> uniqueEmailIds = new HashSet<string>(EmailIds);
                string useremailids = String.Join(",", uniqueEmailIds);

                mail.From = new MailAddress(EmailFrom);
                mail.To.Add(useremailids);
                if(BCCEmailId!="")
                mail.Bcc.Add(BCCEmailId);
                mail.Subject = "SVInsights: " + EmailSubject;

                string Message = "";
                if (FilePath != "")
                {
                    if (ReportType == "S")
                    {
                        Message = BatchResource.SubsReportBodyContent.Replace("@Name", string.Format("{0}", Name)).
                                Replace("@ReportFrequency", string.Format("'{0}'", @ReportFrequency)).Replace("@OrignalReportName", string.Format("{0}", OrignalReportName));
                    }
                    else
                    {
                        Message = BatchResource.BatchReportBodyContent.Replace("@Name", string.Format("{0}", Name)).
                                Replace("@OrignalReportName", string.Format("{0}", OrignalReportName));
                    }

                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(FilePath);
                    attachment.Name = ReportName + ".pdf";
                    mail.Attachments.Add(attachment);
                }
                else
                {
                    Message = Message = BatchResource.NoAttachmentBodyContent.Replace("@Name", string.Format("{0}", Name)).
                                Replace("@OrignalReportName", string.Format("{0}", OrignalReportName));
                }

                int i = 0;
                string strEmail = "";
                strEmail = "<html><head></head>";
                strEmail += "<body>  ";

                strEmail += "<table>" + Message + "</table>";
                if (SelectedParameters != "")
                {
                    string[] RepPar = SelectedParameters.Split(new string[] { "~#~" }, StringSplitOptions.None);
                    strEmail += "<table style='border:1px solid;  border-collapse: collapse; text-align:left;' cellpadding='3'>";
                    for (i = 0; i < RepPar.Length; i++)
                    {
                        int j = 0;
                        string[] Columns = RepPar[i].Split(new string[] { "#~#" }, StringSplitOptions.None);

                        strEmail += "<tr style='text-align:left;'>";

                        if (Columns.Length > 0)
                            strEmail += "<th style='border:1px solid; text-align: right;'>" + Columns[0].ToString() + "</th>";

                        string val = "";
                        for (j = 1; j < Columns.Length; j++)
                        {
                            if (j == 1)
                                val += Columns[j].ToString();
                            else
                                val += ":" + Columns[j].ToString();
                        }
                        strEmail += "<td style='border:1px solid;min-width:300px; text-align: left;'>" + val + "</td>";

                        strEmail += "</tr>";
                    }
                    strEmail += "</table><br/>";
                }
                if (ReportType == "S")
                {
                    strEmail += BatchResource.EmailBodyFooterContent;
                }

                strEmail += "</body> </html>";
                mail.Body = strEmail;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                NetworkCredential basicAuthenticationInfo = new NetworkCredential("iControl", "SpojIzF3CBum");
                smtp.Host = Convert.ToString(ConfigurationManager.AppSettings["mailHost"]);
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = basicAuthenticationInfo;

                smtp.EnableSsl = false;
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["mailPort"]);
                smtp.Send(mail);

                EmailStatus = true;
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "SendEmailToUser()");
            }
            return EmailStatus;
        }
        /// <summary>
        /// send email to user
        /// </summary>
        private static bool SendEmailToUser_SaveToFolder(string UserEmailId, string BCCEmailId, string FolderName, string FilePath, string ReportName, string SelectedParameters, string Name, string EmailSubject)
        {
            bool EmailStatus = false;
            try
            {
                MailMessage mail = new MailMessage();
                // check for emailids repeatation
                List<string> EmailIds = UserEmailId.Split(',').ToList(); ;
                HashSet<string> uniqueEmailIds = new HashSet<string>(EmailIds);
                string useremailids = String.Join(",", uniqueEmailIds);

                mail.From = new MailAddress(EmailFrom);
                mail.To.Add(useremailids);
                mail.Bcc.Add(BCCEmailId);
                mail.Subject = "SVInsights: " + EmailSubject;

                string Message = "";
                if (FilePath != "")
                {
                    Message = BatchResource.EmailBodyUnderFolder.Replace("@NAME", string.Format("{0}", Name)).
                                Replace("@ReportName", string.Format("{0}", ReportName)).Replace("@FolderName", string.Format("{0}", FolderName));
                }
                else
                {
                    Message = BatchResource.EmailBodyWithoutFolder.Replace("@NAME", string.Format("{0}", Name)).
                                Replace("@ReportName", string.Format("{0}", ReportName));
                }

                int i = 0;
                string strEmail = "";
                strEmail = "<html><head></head>";
                strEmail += "<body>  ";

                strEmail += "<table>" + Message + "</table>";
                if (SelectedParameters != "")
                {
                    string[] RepPar = SelectedParameters.Split(new string[] { "~#~" }, StringSplitOptions.None);
                    strEmail += "<table style='border:1px solid;  border-collapse: collapse; text-align:left;' cellpadding='3'>";
                    for (i = 0; i < RepPar.Length; i++)
                    {
                        int j = 0;

                        string[] Columns = RepPar[i].Split(new string[] { "#~#" }, StringSplitOptions.None);

                        strEmail += "<tr style='text-align:left;'>";

                        if (Columns.Length > 0)
                            strEmail += "<th style='border:1px solid; text-align: right;'>" + Columns[0].ToString() + "</th>";

                        string val = "";
                        for (j = 1; j < Columns.Length; j++)
                        {
                            if (j == 1)
                                val += Columns[j].ToString();
                            else
                                val += ":" + Columns[j].ToString();
                        }
                        strEmail += "<td style='border:1px solid;min-width:300px; text-align: left;'>" + val + "</td>";

                        strEmail += "</tr>";
                    }
                    strEmail += "</table><br/>";
                }

                strEmail += "</body> </html>";
                mail.Body = strEmail;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                NetworkCredential basicAuthenticationInfo = new NetworkCredential("iControl", "SpojIzF3CBum");
                smtp.Host = Convert.ToString(ConfigurationManager.AppSettings["mailHost"]);
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = basicAuthenticationInfo;

                smtp.EnableSsl = false;
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["mailPort"]);
                smtp.Send(mail);

                EmailStatus = true;
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "SendEmailToUser()");
            }
            return EmailStatus;
        }
        /// <summary>
        /// get list of report
        /// </summary>
        private static DataTable GetReportsToSend(string ReportType, string TableauServer)
        {
            using (DataTable dt = new DataTable())
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(strConn))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = con;
                            cmd.Parameters.Add("@ReportType", SqlDbType.VarChar, 1).Value = ReportType;
                            cmd.Parameters.Add("@TableauServer", SqlDbType.VarChar, 3).Value = TableauServer;
                            cmd.Parameters.Add("@TableauUrl", SqlDbType.VarChar, 200).Value = TableauHost;
                            cmd.CommandText = "usp_GetReportsToSend_V3";
                            cmd.CommandType = CommandType.StoredProcedure;
                            using (SqlDataAdapter adap = new SqlDataAdapter(cmd))
                            {
                                adap.Fill(dt);
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "GetReportsToSend()");
                }
                return dt;
            }
        }
        /// <summary>
        /// write log into a file
        /// </summary>
        private static void WriteLogFiles(string exception, string StackTrace, string MethodName)
        {
            if (!Directory.Exists(LogPath + "ErrorLog\\"))
            {
                Directory.CreateDirectory(LogPath + "ErrorLog\\");
            }
            string ErrorLogFileName = LogPath + "ErrorLog\\" + "ErrorLog.txt";
            if (!File.Exists(ErrorLogFileName))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(ErrorLogFileName))
                {
                }
            }
            using (StreamWriter writer = new StreamWriter(ErrorLogFileName, true))
            {
                writer.WriteLine("Message :" + exception + "<br/>" + Environment.NewLine + "StackTrace :" + StackTrace +
                   "" + Environment.NewLine + "Date :" + DateTime.Now.ToString() +
                   "" + Environment.NewLine + "Method :" + MethodName);
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
        }
        /// <summary>
        /// update status of report
        /// </summary>
        private static void UpdateReportStatus(int ReportStatus, int ReportID, string ErrorStatus, string EmailId, Int32 Scheduleid, string ReportType, int ExecCount)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(strConn))
                {
                    AddSendReportHistory(ReportType, ReportID, ErrorStatus, EmailId, con);
                    UpdateSendReportStatus(ReportType, ReportID, ReportStatus, con, ExecCount);
                    if (ReportType == "S")
                        UpdateSavedReportsSchedule(Scheduleid, con);
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "UpdateReportStatus()");
            }
        }
        /// <summary>
        /// update path of report
        /// </summary>
        private static void UpdateSaveToFolderReportPath(int ReportID, string ReportType, string ReportPath)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(strConn))
                {
                    using (SqlCommand cmd = new SqlCommand("Usp_UpdateSaveToFolderReportPath", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@ReportType", SqlDbType.VarChar, 1).Value = ReportType;
                        cmd.Parameters.Add("@ReportId", SqlDbType.Int).Value = ReportID;
                        cmd.Parameters.Add("@ReportPath", SqlDbType.VarChar).Value = ReportPath;
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "UpdateReportStatus()");
            }
        }
        /// <summary>
        /// insert send report history
        /// </summary>
        private static void AddSendReportHistory(string ReportType, int ReportID, string ErrorStatus, string EmailId, SqlConnection con)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("usp_AddSendReportHistory", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ReportType", SqlDbType.VarChar, 1).Value = ReportType;
                    cmd.Parameters.Add("@ReportId", SqlDbType.Int).Value = ReportID;
                    cmd.Parameters.Add("@Email", SqlDbType.VarChar, 200).Value = EmailId;
                    cmd.Parameters.Add("@ExecutionStatus", SqlDbType.VarChar, 2000).Value = ErrorStatus;
                    con.Open();
                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "AddSendReportHistory()");
            }
        }
        /// <summary>
        /// update send report status
        /// </summary>
        private static void UpdateSendReportStatus(string ReportType, int ReportID, int ReportStatus, SqlConnection con, int executioncount)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Usp_UpdateSendReportStatus_SavedReports_V2", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ReportType", SqlDbType.VarChar, 1).Value = ReportType;
                    cmd.Parameters.Add("@ReportId", SqlDbType.Int).Value = ReportID;
                    if (ReportStatus == 3)
                    {
                        if (executioncount < 2)
                        {
                            cmd.Parameters.Add("@ReportStatus", SqlDbType.Int).Value = 1;
                            cmd.Parameters.Add("@Executioncount", SqlDbType.Int).Value = executioncount + 1;
                        }
                        else if (executioncount == 2)
                        {
                            cmd.Parameters.Add("@ReportStatus", SqlDbType.Int).Value = ReportStatus;
                            cmd.Parameters.Add("@Executioncount", SqlDbType.Int).Value = 0;
                        }
                    }
                    else if (ReportStatus == 2)
                    {
                        cmd.Parameters.Add("@ReportStatus", SqlDbType.Int).Value = ReportStatus;
                        cmd.Parameters.Add("@Executioncount", SqlDbType.Int).Value = 0;
                    }
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "UpdateSendReportStatus()");
            }
        }
        /// <summary>
        ///update schedule of report
        /// </summary>
        private static void UpdateSavedReportsSchedule(int Scheduleid, SqlConnection con)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Usp_UpdateNextExecDate_SavedReportSchedule", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Scheduleid", SqlDbType.Int).Value = Scheduleid;
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                WriteLogFiles(ex.Message.ToString(), ex.StackTrace.ToString(), "UpdateSavedReportsSchedule()");
            }
        }

        #endregion
    }
}
