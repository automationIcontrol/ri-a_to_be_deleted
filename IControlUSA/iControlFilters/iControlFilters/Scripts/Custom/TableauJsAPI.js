﻿var viz;
var workbook;
var activeSheet;
var work;
var obj = {};

function initViz(item) {
    
    var options = JSON.parse(item);
    options["hideTabs"] = true;
    options["hideToolbar"] = true;
    options["onFirstInteractive"] = function () {
        workbook = viz.getWorkbook();
        activeSheet = workbook.getActiveSheet();
    }
    if (viz) { // If a viz object exists, delete it.
        viz.dispose();
    }
    var tableauURL = options["WorkbookUrl"];
    var containerDiv = $("#MainContent_ReportContainer")[0];

    delete options["WorkbookUrl"];
    if (tableauURL) {
        viz = new tableau.Viz(containerDiv, tableauURL, options);
    }
}



