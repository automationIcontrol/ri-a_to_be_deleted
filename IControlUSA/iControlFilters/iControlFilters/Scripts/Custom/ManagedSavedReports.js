﻿//functio for check date
function checkdate() {
    var Repname = document.getElementById("ctl00_MainContent_txtReportName").value;
    if (Repname.trim() == "") {
        alert("Please enter report name");
        return false;
    }
    var value = document.getElementById("ctl00_MainContent_txtemail").value;
    if (value != '') {
        var result = value.split(",");
        for (var i = 0; i < result.length; i++) {
            if (result[i] != '') {
                if (!validateEmail(result[i])) {
                    document.getElementById("ctl00_MainContent_txtemail").focus();
                    alert('Please check ' + result[i] + ' email addresses not valid!');
                    return false;
                }
            }
            else {
                alert('Invalid email address!');
                return false;
            }
        }
    }
    var FromDate = document.getElementById("ctl00_MainContent_Hfdate");
    var ToDate = document.getElementById("ctl00_MainContent_txtrepdate");
    if (ToDate.value == "" || ToDate.value == "__/__/____" || ToDate == null) {
        eval($('.Lnk_eSavedUpdate').attr('href'));
    }
    else {
        if (CompareDates(FromDate, ToDate) == 0) {
            alert("End Date should be greater than current date");
            return false;
        }
        var FromDateres = ToDate.value.substring(0, 2);
        if (FromDateres > 12 || FromDateres == 0) {
            alert('Please select Valid End Date');
            return false;
        }

        var year = ToDate.value.substring(6, 10);

        var FromDays = getDaysInMonth(FromDateres, year);

        if (year == 0) {
            alert('Please select Valid End Date');
            return false;
        }

        var FromDateresForDate = ToDate.value.substring(3, 5);
        if (FromDateresForDate > FromDays) {
            alert('Please select Valid End Date');
            return false;
        }

        if (FromDateresForDate == 0) {
            alert('Please select Valid End Date');
            return false;
        }

        if (CompareDates(FromDate, ToDate) == 0) {
            alert("End Date should be greater than current date");
            return false;
        }
        eval($('.Lnk_eSavedUpdate').attr('href'));
    }
}

//function for get days in month
function getDaysInMonth(m, y) {
    return /8|3|5|10/.test(--m) ? 30 : m == 1 ? (!(y % 4) && y % 100) || !(y % 400) ? 29 : 28 : 31;
}

//function for comparing dates
function CompareDates(FromDate, ToDate) {
    var parts = FromDate.value.split('/');
    var from_date = new Date(parts[2], parts[0] - 1, parts[1]);
    parts = ToDate.value.split('/');
    if (parts.length > 1) {
        var to_date = new Date(parts[2], parts[0] - 1, parts[1]);
    }
    else {
        parts = ToDate.value.split('-');
        var to_date = new Date(parts[2], parts[0] - 1, parts[1]);
    }

    if (from_date > to_date)
        return 0;
    else
        return 1;
}

//function for email validation
function validateEmail(field) {
    var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}$/;
    return (regex.test(field)) ? true : false;
}

//function for validate multiple emails
function validateMultipleEmailsCommaSeparated(emailcntl, seperator) {
    var value = emailcntl.value;
    if (value != '') {
        var result = value.split(seperator);
        for (var i = 0; i < result.length; i++) {
            if (result[i] != '') {
                if (!validateEmail(result[i])) {
                    emailcntl.focus();
                    alert('Please check ' + result[i] + ' email addresses not valid!');
                    return false;
                }
            }
            else {
                alert('Invalid email address!');
                return false;
            }
        }
    }
    return true;
}

function Count(text, long, Controltype) {
    var maxlength = new Number(long); // Change number to your max length.
    if (text.value.length > maxlength) {
        text.value = text.value.substring(0, maxlength);
        alert(Controltype + " must not exceed " + maxlength + " characters.");
    }
}

function CheckValues() {
    var RepName = document.getElementById("ctl00_MainContent_txtSharedReportName");
    if (RepName.value.trim() == "") {
        alert('Please enter report name');
        return false;
    }

    var comboBanners = $find("<%= RdShareUsers.ClientID %>");
    if (comboBanners.get_checkedItems().length == 0) {
        alert('Please select user');
        return false;
    }

    var Msg = document.getElementById("ctl00_MainContent_txtMessage");
    if (Msg.value.trim() == "") {
        alert('Please enter message');
        return false;
    }
    var RdShare = document.getElementById("ctl00_MainContent_HfShareHierarchy").value;

    if (RdShare == "1") {

        var Rdvalue = 0;
        if (document.getElementById('ctl00_MainContent_rdShareHierarchy_1').checked) {
            Rdvalue = 1;
        }
        if (document.getElementById('ctl00_MainContent_rdShareHierarchy_0').checked) {
            Rdvalue = 1;
        }
        if (Rdvalue == 0) {
            alert("Please select an option to share custom hierarchy.")
            return false;
        }
    }
    else if (RdShare == "2") {
        var RdCombo = 0;
        if (document.getElementById('ctl00_MainContent_rdComboStoreHierarchy_1').checked) {
            RdCombo = 1;
        }
        if (document.getElementById('ctl00_MainContent_rdComboStoreHierarchy_0').checked) {
            RdCombo = 1;
        }
        if (RdCombo == 0) {
            alert("Please select an option to share custom store and custom hierarchy.")
            return false;
        }
    }
    else {

        var RdStore = 0;
        if (document.getElementById('ctl00_MainContent_rdShareStore_1').checked) {
            RdStore = 1;
        }
        if (document.getElementById('ctl00_MainContent_rdShareStore_0').checked) {
            RdStore = 1;
        }
        if (RdStore == 0) {
            alert("Please select an option to share custom Store.")
            return false;
        }

    }


}

function CheckEndSubscriDate(sender, args) {
    var txtVal = sender._selectedDate.format(sender._format);
        if (isDate(txtVal)) {
            var endSubscribtionMaxDate = new Date().AddDays(7 * 52);
            var chktxtVal = new Date(txtVal), chkendSubscribtionMaxDate = new Date(endSubscribtionMaxDate);
            if (chktxtVal > chkendSubscribtionMaxDate) {
                $('#MainContent_txtrepdate').val(endSubscribtionMaxDate);
                sender._selectedDate = endSubscribtionMaxDate;
            }
            else {
                sender._selectedDate =txtVal;
                $('#MainContent_txtrepdate').val(txtVal)
            }
              
           }
}
function CheckEndDate(id) {
    $(id).blur(function () {
        var txtVal = $(this).val();
        if ($(this).val() == '__/__/____' || $(this).val().trim().length == 0) {
            $(this).val('__/__/____');
        }
        else {
            if (isDate(txtVal)) {
                var endSubscribtionMaxDate = new Date().AddDays(7 * 52);
                var chktxtVal = new Date(txtVal), chkendSubscribtionMaxDate = new Date(endSubscribtionMaxDate);
                if (chktxtVal > chkendSubscribtionMaxDate) {
                    $(this).val(endSubscribtionMaxDate);
                }
                else
                    $(this).val(txtVal);
            }
            else {
                alert('Invalid Date');
                $(this).val('');
                $(this).focus();
            }
        }
    });

}
// For set date format of mediaList
Date.prototype.AddDays = function (noOfDays) {
    this.setTime(this.getTime() + (noOfDays * (1000 * 60 * 60 * 24)));
    return this;
}

Date.prototype.toString = function () {
    var year = this.getFullYear();

    var month = (1 + this.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = this.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return month + '/' + day + '/' + year;
}


function isDate(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
        return false;

    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}
function SetTextOfTheComboBox() {
    var count = 0;
    var comboBox = $find("<%= RdShareUsers.ClientID %>");
    var items = comboBox.get_items();

    for (var i = 0; i < items.get_count() ; i++) {
        var itemDiv = items.getItem(i).get_element();
        var inputs = itemDiv.getElementsByTagName("input");
        for (var inputIndex = 0; inputIndex < inputs.length; inputIndex++) {
            var input = inputs[inputIndex];
            if (input.type == "checkbox") {
                if (input.checked == true)
                    count++;
            }
        }
    }
    if (count == items.get_count()) {
        comboBox.set_text("All items checked");
    }
    else if (count == 0) {
        comboBox.set_text("Select User");

    }
    else if (count > 1) {
        comboBox.set_text(count + " items checked");
    }
}