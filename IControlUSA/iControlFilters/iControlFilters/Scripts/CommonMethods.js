﻿function showImage() { 
    document.getElementById('ImgStoreSegment').style.display = "inline";
    document.getElementById('ImgStoreSegment').src = '../Images/StoreSegmentFilters.png';
}
function hideImage() { 
    document.getElementById('ImgStoreSegment').style.display = "none";
    document.getElementById('ImgStoreSegment').src = '';
}

function validateCalendar() {
    try {
        var _hiddenFrom = document.getElementById("ctl00_MainContent_dtFromDate_hdnMyDate");
        if (_hiddenFrom != null) {
            if (_hiddenFrom.value == '1') {
                _hiddenFrom.value = 0;
                return true;
            }
        }
        var _hiddenTo = document.getElementById("ctl00_MainContent_dtToDate_hdnToDate");
        if (_hiddenTo != null) {
            if (_hiddenTo.value == '1') {
                _hiddenTo.value = 0;
                return true;
            }
        }
        var _hiddenSt = document.getElementById("ctl00_MainContent_dtToDate_hdnStDate");
        if (_hiddenSt != null) {
            if (_hiddenSt.value == '1') {
                _hiddenSt.value = 0;
                return true;
            }
        }
        //txtrepdate
        var _hiddenSt2 = document.getElementById("ctl00_MainContent_txtrepdate_hdnStDate");
        if (_hiddenSt2 != null) {
            if (_hiddenSt2.value == '1') {
                _hiddenSt2.value = 0;
                return true;
            }
        }

        //dtEndDate
        var _dtEndDate = document.getElementById("ctl00_MainContent_dtEndDate_hdnStDate");
        if (_dtEndDate != null) {
            if (_dtEndDate.value == '1') {
                _dtEndDate.value = 0;
                return true;
            }
        }
        var _dtEndDate_hdnMyDate = document.getElementById("ctl00_MainContent_dtEndDate_hdnMyDate");
        if (_dtEndDate_hdnMyDate != null) {
            if (_dtEndDate_hdnMyDate.value == '1') {
                _dtEndDate_hdnMyDate.value = 0;
                return true;
            }
        }
        var _dtEndDate_hdnToDate = document.getElementById("ctl00_MainContent_dtEndDate_hdnToDate");
        if (_dtEndDate_hdnToDate != null) {
            if (_dtEndDate_hdnToDate.value == '1') {
                _dtEndDate_hdnToDate.value = 0;
                return true;
            }
        }

        //ctl00_MainContent_dtToDate_hdnMyDate
        var _dtToDate_hdnMyDate = document.getElementById("ctl00_MainContent_dtToDate_hdnMyDate");
        if (_dtToDate_hdnMyDate != null) {
            if (_dtToDate_hdnMyDate.value == '1') {
                _dtToDate_hdnMyDate.value = 0;
                return true;
            }
        }
        //dtlaunchDate
        var _dtlaunchDate_hdnMyDate = document.getElementById("ctl00_MainContent_dtlaunchDate_hdnMyDate");
        if (_dtlaunchDate_hdnMyDate != null) {
            if (_dtlaunchDate_hdnMyDate.value == '1') {
                _dtlaunchDate_hdnMyDate.value = 0;
                return true;
            }
        }
        var _dtlaunchDate_hdnStDate = document.getElementById("ctl00_MainContent_dtlaunchDate_hdnStDate");
        if (_dtlaunchDate_hdnStDate != null) {
            if (_dtlaunchDate_hdnStDate.value == '1') {
                _dtlaunchDate_hdnStDate.value = 0;
                return true;
            }
        }
        var _dtlaunchDate_hdnToDate = document.getElementById("ctl00_MainContent_dtlaunchDate_hdnToDate");
        if (_dtlaunchDate_hdnToDate != null) {
            if (_dtlaunchDate_hdnToDate.value == '1') {
                _dtlaunchDate_hdnToDate.value = 0;
                return true;
            }
        }
        //dtStartDate
        var _dtStartDate_hdnMyDate = document.getElementById("ctl00_MainContent_dtStartDate_hdnMyDate");
        if (_dtStartDate_hdnMyDate != null) {
            if (_dtStartDate_hdnMyDate.value == '1') {
                _dtStartDate_hdnMyDate.value = 0;
                return true;
            }
        }
        var _dtStartDate_hdnToDate = document.getElementById("ctl00_MainContent_dtStartDate_hdnToDate");
        if (_dtStartDate_hdnToDate != null) {
            if (_dtStartDate_hdnToDate.value == '1') {
                _dtStartDate_hdnToDate.value = 0;
                return true;
            }
        }
        var _dtStartDate_hdnStDate = document.getElementById("ctl00_MainContent_dtStartDate_hdnStDate");
        if (_dtStartDate_hdnStDate != null) {
            if (_dtStartDate_hdnStDate.value == '1') {
                _dtStartDate_hdnStDate.value = 0;
                return true;
            }
        }
    } catch (err) { }
}