﻿function checkdt(dtformat, sender, eventArgs, dttt) {
    try {
        var re = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;
        var re1 = /^(\d{1,2})-(\d{1,2})-(\d{4})$/;
        if (dtformat.value != '') {

            if (re1.test(dtformat.value)) {
                dtformatDate = new Date(dtformat.value);
                dateNow = new Date();
                var years = dateNow.getFullYear() - dtformatDate.getFullYear();
                var months = dateNow.getMonth() - dtformatDate.getMonth();
                var days = dateNow.getDate() - dtformatDate.getDate();
                if (isNaN(years)) {
                    st = 0;
                }
                else {
                    st = 0;
                    if (months < 0 || (months == 0 && days < 0)) {
                        years = parseInt(years) - 1;
                    }
                    else {
                    }
                }
            }
            else {
                st = 1;
            }
            if (re.test(dtformat.value)) {
                dtformatDate = new Date(dtformat.value);
                dateNow = new Date();
                var years = dateNow.getFullYear() - dtformatDate.getFullYear();
                var months = dateNow.getMonth() - dtformatDate.getMonth();
                var days = dateNow.getDate() - dtformatDate.getDate();
                if (isNaN(years)) {
                    st = 0;
                }
                else {
                    st = 0;
                    if (months < 0 || (months == 0 && days < 0)) {
                        years = parseInt(years) - 1;
                    }
                    else {
                    }
                }
            }
            else {
                st = 1;
            }
            if (st == 1) {
                alert("Invalid  " + dttt + "  format");
                eventArgs.set_cancel(true);
            }
        }
    } 
    catch (err) { }
}


$(document).ready(function () {
    try {
        MaskReset();
    }
    catch (err) { }
});


function MaskReset(textboxid) {
    try {
        var selected = $(textboxid).val();
        if (selected == "" || selected == "__/__/____") 
        {
            $(textboxid).mask("99/99/9999").val("__/__/____");
        }
        else {
            $(textboxid).mask("99/99/9999").val(selected);
        }
        $(textboxid).blur(function () 
        {
            var selectdate = $(textboxid).val();
            if (selectdate == "" || selectdate == "__/__/____") {
                $(textboxid).val("__/__/____");
            }
            else {
                $(textboxid).val(selectdate);
            }
        });
    } 
    catch (err) { }
}


function MaskReset1(_textboxid) {
    try {
        var selected = $(_textboxid).val();
        if (selected == "" || selected == "__/__/____") {
            $(_textboxid).mask("99/99/9999").val("__/__/____");
        }
        else {
            $(_textboxid).mask("99/99/9999").val(selected);
        }
    } 
    catch (err) { }
}

 