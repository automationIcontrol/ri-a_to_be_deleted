﻿using iControlDataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.IO;
using iControlDataLayer.Helper;
using System.Web.UI;
using iControlGenricFilters.ResourceFiles;

namespace iControlGenricFilters.LIB
{
    public class PDFExport
    {
        #region Selected Parameters for Batching
        public static string SaveSelectedFilters(RadComboBox ddlFiscalyear = null, RadComboBox FromAdWeek = null, RadComboBox ToAdWeek = null, TextBox VendorNumber = null, RadComboBox LocBanner = null, RadComboBox LocSubBanner = null, RadComboBox Department = null,
                     RadComboBox Category = null, RadComboBox SubCategory = null, RadComboBox Segment = null, RadComboBox ProductBrand = null, RadComboBox ProProductSizeDesc = null, RadComboBox BrandType = null,
                     RadComboBox RecentTimeframe = null, RadComboBox CustomLevel1 = null, RadComboBox CustomLevel2 = null, RadComboBox CustomLevel3 = null, RadComboBox CustomBrand = null, RadComboBox CustomProductSizeDesc = null,
                     RadComboBox ViewBy = null, TextBox From = null, TextBox To = null, RadComboBox HierarchyName = null, RadComboBox Holiday = null, RadComboBox NumberofWeeksPrior = null, RadComboBox NumberofWeeksPost = null,
                     RadComboBox FromFiscalWeek = null, RadComboBox ToFiscalWeek = null, RadComboBox LocHierSrcBanner = null, RadComboBox LocHierSrcSubBanner = null, RadComboBox SrcStore = null, RadComboBox LocHierDesBanner = null,
                     RadComboBox LocHierDesSubBanner = null, RadComboBox DesStore = null, RadComboBox POG = null, TextBox LaunchDate = null, RadComboBox NumberofWeeks = null, RadComboBox CustomLevel1NewItems = null,
                     RadComboBox CustomLevel1DiscontinuedItems = null, TextBox DiscontinuedDate = null, RadComboBox HierarchyLocation = null, RadComboBox LocationStore = null, RadComboBox LocationCustom1 = null,
                     RadComboBox LocationCustom2 = null, RadComboBox LocationCustom3 = null, RadioButtonList DailyWeekly = null, RadComboBox ProductHierarchy = null, string Exec_Id = "")
        {

            IControlHelper helper = new IControlHelper();
            string orderInfoTable = "";
            try
            {

                //DailyWeekly  
                if (DailyWeekly != null)
                {
                    orderInfoTable += "View #~#";
                    orderInfoTable += DailyWeekly.SelectedItem.Text.ToString().Replace("^@~", ", ") + "~#~";
                }

                //NumberofWeeksPrior  
                if (NumberofWeeksPrior != null)
                {
                    if (NumberofWeeksPrior.CheckedItems.Count > 0 && AppManager.SubBanner(NumberofWeeksPrior) != "empty")
                    {
                        orderInfoTable += "Number of Weeks Prior   #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(NumberofWeeksPrior).Replace("^@~", ", ") + "~#~";
                    }
                    else if (NumberofWeeksPrior.CheckedItems.Count > 0 && NumberofWeeksPrior.CheckedItems.Count == NumberofWeeksPrior.Items.Count)
                    {
                        orderInfoTable += "Number of Weeks Prior    #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }
                //NumberofWeeksPost 
                if (NumberofWeeksPost != null)
                {
                    if (NumberofWeeksPost.CheckedItems.Count > 0 && AppManager.SubBanner(NumberofWeeksPost) != "empty")
                    {
                        orderInfoTable += "Number of Weeks Post #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(NumberofWeeksPost).Replace("^@~", ", ") + "~#~";
                    }
                    else if (NumberofWeeksPost.CheckedItems.Count > 0 && NumberofWeeksPost.CheckedItems.Count == NumberofWeeksPost.Items.Count)
                    {
                        orderInfoTable += "Number of Weeks Post  #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }
                //FromFiscalWeek 
                if (FromFiscalWeek != null)
                {
                    if (FromFiscalWeek.CheckedItems.Count > 0 && AppManager.SubBanner(FromFiscalWeek) != "empty")
                    {
                        orderInfoTable += "From Fiscal Week #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(FromFiscalWeek).Replace("^@~", ", ") + "~#~";
                    }
                    else if (FromFiscalWeek.CheckedItems.Count > 0 && FromFiscalWeek.CheckedItems.Count == FromFiscalWeek.Items.Count)
                    {
                        orderInfoTable += "From Fiscal Week #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }
                //ToFiscalWeek
                if (ToFiscalWeek != null)
                {
                    if (ToFiscalWeek.CheckedItems.Count > 0 && AppManager.SubBanner(ToFiscalWeek) != "empty")
                    {
                        orderInfoTable += "To Fiscal Week #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(ToFiscalWeek).Replace("^@~", ", ") + "~#~";
                    }
                    else if (ToFiscalWeek.CheckedItems.Count > 0 && ToFiscalWeek.CheckedItems.Count == ToFiscalWeek.Items.Count)
                    {
                        orderInfoTable += "To Fiscal Week #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }

                //Discontinued Date
                if (DiscontinuedDate != null && !string.IsNullOrEmpty(DiscontinuedDate.Text))
                {
                    orderInfoTable += "Discontinued Date #~#";
                    orderInfoTable += AppManager.FromDate(DiscontinuedDate.Text).ToString("MM/dd/yyyy") + "~#~";
                }

                //LaunchDate 
                if (LaunchDate != null && !string.IsNullOrEmpty(LaunchDate.Text))
                {
                    orderInfoTable += "LaunchDate #~#";
                    orderInfoTable += AppManager.FromDate(LaunchDate.Text).ToString("MM/dd/yyyy") + "~#~";
                }
                //NumberofWeeks  
                if (NumberofWeeks != null)
                {
                    if (NumberofWeeks.CheckedItems.Count > 0 && AppManager.SubBanner(NumberofWeeks) != "empty")
                    {
                        orderInfoTable += "Number of Weeks #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(NumberofWeeks).Replace("^@~", ", ") + "~#~";
                    }
                    else if (NumberofWeeks.CheckedItems.Count > 0 && NumberofWeeks.CheckedItems.Count == NumberofWeeks.Items.Count)
                    {
                        orderInfoTable += "NumberofWeeks  #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }

                // Fiscal Year
                if (ddlFiscalyear != null)
                {
                    if (ddlFiscalyear.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Fiscal Year #~#";
                        orderInfoTable += AppManager.GetSelectedItems(ddlFiscalyear) + "~#~";
                    }
                    else if (ddlFiscalyear.CheckedItems.Count > 0 && ddlFiscalyear.CheckedItems.Count == ddlFiscalyear.Items.Count)
                    {
                        orderInfoTable += "Fiscal Year #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }
                //Recent time frame
                if (RecentTimeframe != null)
                {
                    orderInfoTable += "Recent Timeframe #~#";
                    orderInfoTable += RecentTimeframe.SelectedItem.Text + "~#~";
                }
                //from
                if (From != null && !string.IsNullOrEmpty(From.Text))
                {
                    orderInfoTable += "Date From #~#";
                    orderInfoTable += AppManager.FromDate(From.Text).ToString("MM/dd/yyyy") + "~#~";
                }

                //To
                if (To != null && !string.IsNullOrEmpty(To.Text))
                {
                    orderInfoTable += "Date To #~#";
                    orderInfoTable += AppManager.FromDate(To.Text).ToString("MM/dd/yyyy") + "~#~";
                }
                if (FromAdWeek != null)
                {
                    orderInfoTable += "From AdWeek #~#";
                    orderInfoTable += AppManager.FromAdWeekValue(FromAdWeek) + "~#~";
                }

                //From ToWeek
                if (ToAdWeek != null)
                {
                    orderInfoTable += "To AdWeek #~#";
                    orderInfoTable += AppManager.ToAdWeekValue(ToAdWeek) + "~#~";
                }

                // For Holiday Reporting 
                if (Holiday != null)
                {
                    if (Holiday.SelectedIndex > 0)
                    {
                        orderInfoTable += "Holiday #~#";
                        orderInfoTable += Holiday.SelectedItem.Text + "~#~";
                    }
                }

                //Vendor Number
                if (VendorNumber != null)
                {
                    if (!String.IsNullOrEmpty(VendorNumber.Text.Trim()))
                    {
                        orderInfoTable += "Vendor #~#";
                        String Vendor = VendorNumber.Text.Trim() + ", " + AppManager.GetVendorName((VendorNumber.Text.Trim()));
                        orderInfoTable += Vendor + "~#~";
                    }
                }

                //ProductHierarchy   
                if (HierarchyLocation != null)
                {
                    orderInfoTable += "Location Hierarchy  #~#";
                    orderInfoTable += HierarchyLocation.SelectedItem.Text.ToString() + "~#~";
                }

                // Banner
                if (LocBanner != null)
                {
                    if (LocBanner.CheckBoxes)
                    {
                        if (LocBanner.CheckedItems.Count > 0)
                        {
                            orderInfoTable += "Banner(s) #~#";
                            orderInfoTable += AppManager.GetSelectedItems(LocBanner).Replace("_", ", ") + "~#~";
                        }
                        else if (LocBanner.CheckedItems.Count > 0 && LocBanner.CheckedItems.Count == LocBanner.Items.Count)
                        {
                            orderInfoTable += "Banner(s) #~#";
                            orderInfoTable += "All" + "~#~";
                        }
                    }
                    else
                    {
                        orderInfoTable += "Banner(s) #~#";
                        orderInfoTable += LocBanner.SelectedItem.Text.ToString() + "~#~";
                    }
                }

                //customStoreGroup
                if (LocationStore != null)
                {

                    if (LocationStore.CheckBoxes)
                    {
                        if (LocationStore.CheckedItems.Count > 0)
                        {
                            orderInfoTable += "Custom Store Group(s) #~#";
                            orderInfoTable += AppManager.GetSelectedItems(LocationStore) + "~#~";
                        }
                        else if (LocationStore.CheckedItems.Count > 0 && LocationStore.CheckedItems.Count == LocationStore.Items.Count)
                        {
                            orderInfoTable += "Custom Store Group(s) #~#";
                            orderInfoTable += "All" + "~#~";
                        }
                    }
                    else   //For single selection of custom store group
                    {
                        orderInfoTable += "Custom Store Group(s) #~#";
                        orderInfoTable += LocationStore.SelectedItem.Text.ToString() + "~#~";

                    }
                }

                //Custom Level 1
                if (LocationCustom1 != null)
                {
                    if (LocationCustom1.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Custom Level 1(s) #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(LocationCustom1).Replace("^@~", ", ") + "~#~";
                    }
                    else if (LocationCustom1.CheckedItems.Count > 0 && LocationCustom1.CheckedItems.Count == LocationCustom1.Items.Count)
                    {
                        orderInfoTable += "Custom Level 1(s) #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }
                //Custom Level 1
                if (LocationCustom1 != null)
                {
                    if (LocationCustom1.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Custom Level 1(s) #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(LocationCustom1).Replace("^@~", ", ") + "~#~";
                    }
                    else if (LocationCustom1.CheckedItems.Count > 0 && LocationCustom1.CheckedItems.Count == LocationCustom1.Items.Count)
                    {
                        orderInfoTable += "Custom Level 1(s) #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }

                //Custom Level 2
                if (LocationCustom2 != null)
                {
                    if (LocationCustom2.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Custom Level 2(s) #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(LocationCustom2).Replace("^@~", ", ") + "~#~";
                    }
                    else if (LocationCustom2.CheckedItems.Count > 0 && LocationCustom2.CheckedItems.Count == LocationCustom2.Items.Count)
                    {
                        orderInfoTable += "Custom Level 2(s) #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }
                //Custom Level 2
                if (LocationCustom3 != null)
                {
                    if (LocationCustom3.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Custom Level 3(s) #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(LocationCustom3).Replace("^@~", ", ") + "~#~";
                    }
                    else if (LocationCustom3.CheckedItems.Count > 0 && LocationCustom3.CheckedItems.Count == LocationCustom3.Items.Count)
                    {
                        orderInfoTable += "Custom Level 3(s) #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }

                //Sub Banner
                if (LocSubBanner != null)
                {
                    if (LocSubBanner.CheckedItems.Count > 0 && AppManager.SubBanner(LocSubBanner) != "empty")
                    {
                        orderInfoTable += "Sub Banner(s) #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(LocSubBanner).Replace("_", ", ") + "~#~";
                    }
                    else if (LocSubBanner.CheckedItems.Count > 0 && LocSubBanner.CheckedItems.Count == LocSubBanner.Items.Count)
                    {
                        orderInfoTable += "Sub Banner(s) #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }

                if (LocHierSrcBanner != null)
                {
                    if (LocHierSrcBanner.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Banner(s) Source #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(LocHierSrcBanner).Replace("_", ", ") + "~#~";
                    }
                    else if (LocHierSrcBanner.CheckedItems.Count > 0 && LocHierSrcBanner.CheckedItems.Count == LocHierSrcBanner.Items.Count)
                    {
                        orderInfoTable += "Banner(s) Source #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }
                //Sub Banner
                if (LocHierSrcSubBanner != null)
                {
                    if (LocHierSrcSubBanner.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Sub Banner(s) Source #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(LocHierSrcSubBanner).Replace("_", ", ") + "~#~";
                    }
                    else if (LocHierSrcSubBanner.CheckedItems.Count > 0 && LocHierSrcSubBanner.CheckedItems.Count == LocHierSrcSubBanner.Items.Count)
                    {
                        orderInfoTable += "Sub Banner(s) Source #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }

                if (SrcStore != null)
                {
                    if (SrcStore.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Store(s) Source #~#";
                        orderInfoTable += AppManager.GetSelectedItems(SrcStore).Replace("_", ", ") + "~#~";
                    }
                    else if (SrcStore.CheckedItems.Count > 0 && SrcStore.CheckedItems.Count == SrcStore.Items.Count)
                    {
                        orderInfoTable += "Store(s) Source #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }
                if (LocHierDesBanner != null)
                {
                    if (LocHierDesBanner.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Banner(s) - Opportunity (Destination) #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(LocHierDesBanner).Replace("_", ", ") + "~#~";
                    }
                    else if (LocHierDesBanner.CheckedItems.Count > 0 && LocHierDesBanner.CheckedItems.Count == LocHierDesBanner.Items.Count)
                    {
                        orderInfoTable += "Banner(s) - Opportunity (Destination) #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }
                //Sub Banner
                if (LocHierDesSubBanner != null)
                {
                    if (LocHierDesSubBanner.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Sub Banner(s) - Opportunity (Destination) #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(LocHierDesSubBanner).Replace("_", ", ") + "~#~";
                    }
                    else if (LocHierDesSubBanner.CheckedItems.Count > 0 && LocHierDesSubBanner.CheckedItems.Count == LocHierDesSubBanner.Items.Count)
                    {
                        orderInfoTable += "Sub Banner(s) - Opportunity (Destination) #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }

                if (DesStore != null)
                {
                    if (DesStore.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Store(s) - Opportunity (Destination) #~#";
                        orderInfoTable += AppManager.GetSelectedItems(DesStore).Replace("_", ", ") + "~#~";
                    }
                    else if (DesStore.CheckedItems.Count > 0 && DesStore.CheckedItems.Count == DesStore.Items.Count)
                    {
                        orderInfoTable += "Store(s) - Opportunity (Destination) #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }

                //ProductHierarchy   
                if (ProductHierarchy != null)
                {
                    orderInfoTable += "Product Hierarchy  #~#";
                    orderInfoTable += ProductHierarchy.SelectedItem.Text.ToString() + "~#~";
                }

                //Department
                if (Department != null)
                {
                    if (Department.CheckedItems.Count >= 20 && Department.CheckedItems.Count != Department.Items.Count)
                    {
                        orderInfoTable += "Department(s) #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(Department).Replace("_", ", ") + "~#~";
                    }
                    else
                    {
                        if (Department.CheckedItems.Count > 0)
                        {
                            orderInfoTable += "Department(s) #~#";
                            orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(Department).Replace("_", ", ") + "~#~";
                        }
                        else if (Department.CheckedItems.Count > 0 && Department.CheckedItems.Count == Department.Items.Count)
                        {
                            orderInfoTable += "Department(s) #~#";
                            orderInfoTable += "All" + "~#~";
                        }
                    }
                }
                //Category
                if (Category != null)
                {
                    if (Category.CheckBoxes)
                    {
                        if (Category.CheckedItems.Count >= 20 && Category.CheckedItems.Count != Category.Items.Count)
                        {
                            orderInfoTable += "Category(s) #~#";
                            orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(Category).Replace("_", ", ") + "~#~";
                        }
                        else
                        {
                            if (Category.CheckedItems.Count > 0 && AppManager.Category(Category) != "empty")
                            {
                                orderInfoTable += "Category(s) #~#";
                                orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(Category).Replace("_", ", ") + "~#~";
                            }
                            else if (Category.CheckedItems.Count > 0 && Category.CheckedItems.Count == Category.Items.Count)
                            {
                                orderInfoTable += "Category(s) #~#";
                                orderInfoTable += "All" + "~#~";
                            }
                        }
                    }
                    else
                    {
                        if (Category.SelectedItem.Text.ToString() != "--Select Category--")
                        {
                            orderInfoTable += "Category(s) #~#";
                            orderInfoTable += Category.SelectedItem.Text.ToString() + "~#~";
                        }
                    }
                }



                //Custom Hierarchy
                if (HierarchyName != null)
                {
                    if (HierarchyName.SelectedIndex > 0)
                    {
                        orderInfoTable += "Hierarchy Name(s) #~#";
                        orderInfoTable += HierarchyName.SelectedItem.Text.ToString() + "~#~";
                    }
                }

                if (CustomLevel1DiscontinuedItems != null)
                {
                    if (CustomLevel1DiscontinuedItems.SelectedIndex > 0)
                    {
                        orderInfoTable += "Custom Level 1(Discontinued Items) #~#";
                        orderInfoTable += CustomLevel1DiscontinuedItems.SelectedItem.Text.ToString() + "~#~";
                    }


                }
                if (CustomLevel1NewItems != null)
                {
                    if (CustomLevel1NewItems.SelectedIndex > 0)
                    {
                        orderInfoTable += "Custom Level 1(New Items) #~#";
                        orderInfoTable += CustomLevel1NewItems.SelectedItem.Text.ToString() + "~#~";
                    }

                }


                if (CustomLevel1 != null)
                {
                    if (CustomLevel1.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Custom Level 1 #~#";
                        //orderInfoTable += DashboardManager.GetSelectedItemsWithUnderScore(ddlcustomlevel1).Replace("_", ", ") + "~#~";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(CustomLevel1).Replace("^@~", ", ") + "~#~";
                    }
                    else if (CustomLevel1.CheckedItems.Count > 0 && CustomLevel1.CheckedItems.Count == CustomLevel1.Items.Count)
                    {
                        orderInfoTable += "Custom Level 1 #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }

                if (CustomLevel2 != null)
                {
                    if (CustomLevel2.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Custom Level 2 #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(CustomLevel2).Replace("^@~", ", ") + "~#~";
                    }
                    else if (CustomLevel2.CheckedItems.Count > 0 && CustomLevel2.CheckedItems.Count == CustomLevel2.Items.Count)
                    {
                        orderInfoTable += "Custom Level 2 #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }

                if (CustomLevel3 != null)
                {
                    if (CustomLevel3.CheckedItems.Count > 0)
                    {
                        orderInfoTable += "Custom Level 3 #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(CustomLevel3).Replace("^@~", ", ") + "~#~";
                    }
                    else if (CustomLevel3.CheckedItems.Count > 0 && CustomLevel3.CheckedItems.Count == CustomLevel3.Items.Count)
                    {
                        orderInfoTable += "Custom Level 3 #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }

                //Sub Category
                if (SubCategory != null)
                {
                    if (SubCategory.CheckedItems.Count >= 20 && SubCategory.CheckedItems.Count != SubCategory.Items.Count)
                    {
                        orderInfoTable += "Sub-Category(s) #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(SubCategory).Replace("_", ", ") + "~#~";
                    }
                    else
                    {
                        if (SubCategory.CheckedItems.Count > 0 && AppManager.SubCategory(SubCategory) != "empty")
                        {
                            orderInfoTable += "Sub-Category(s) #~#";
                            orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(SubCategory).Replace("_", ", ") + "~#~";
                        }
                        else if (SubCategory.CheckedItems.Count > 0 && SubCategory.CheckedItems.Count == SubCategory.Items.Count)
                        {
                            orderInfoTable += "Sub-Category(s) #~#";
                            orderInfoTable += "All" + "~#~";
                        }
                    }
                }

                //Segment
                if (Segment != null)
                {
                    if (Segment.CheckedItems.Count >= 20 && Segment.CheckedItems.Count != Segment.Items.Count)
                    {
                        orderInfoTable += "Segment(s) #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(Segment).Replace("_", ", ") + "~#~";
                    }
                    else
                    {
                        if (Segment.CheckedItems.Count > 0 && AppManager.Segment(Segment) != "empty")
                        {
                            orderInfoTable += "Segment(s) #~#";
                            orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(Segment).Replace("_", ", ") + "~#~";
                        }
                        else if (Segment.CheckedItems.Count > 0 && Segment.CheckedItems.Count == Segment.Items.Count)
                        {
                            orderInfoTable += "Segment(s) #~#";
                            orderInfoTable += "All" + "~#~";
                        }
                    }
                }


                //Brand
                if (CustomBrand != null)
                {
                    if (CustomBrand.CheckedItems.Count >= 20 && CustomBrand.CheckedItems.Count != CustomBrand.Items.Count)
                    {
                        orderInfoTable += "Brand(s) #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(CustomBrand).Replace("^@~", ", ") + "~#~";
                    }
                    else
                    {
                        if (CustomBrand.CheckedItems.Count > 0 && AppManager.Brand(CustomBrand) != "empty")
                        {
                            orderInfoTable += "Brand(s) #~#";
                            orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(CustomBrand).Replace("^@~", ", ") + "~#~";
                        }
                        else if (CustomBrand.CheckedItems.Count > 0 && CustomBrand.CheckedItems.Count == CustomBrand.Items.Count)
                        {
                            orderInfoTable += "Brand(s) #~#";
                            orderInfoTable += "All" + "~#~";
                        }
                    }
                }

                //Brand
                if (ProductBrand != null)
                {
                    if (ProductBrand.CheckedItems.Count >= 20 && ProductBrand.CheckedItems.Count != ProductBrand.Items.Count)
                    {
                        orderInfoTable += "Brand(s) #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(ProductBrand).Replace("^@~", ", ") + "~#~";
                    }
                    else
                    {
                        if (ProductBrand.CheckedItems.Count > 0 && AppManager.Brand(ProductBrand) != "empty")
                        {
                            orderInfoTable += "Brand(s) #~#";
                            orderInfoTable += AppManager.GetSelectedItemsWithSpecialChar(ProductBrand).Replace("^@~", ", ") + "~#~";
                        }
                        else if (ProductBrand.CheckedItems.Count > 0 && ProductBrand.CheckedItems.Count == ProductBrand.Items.Count)
                        {
                            orderInfoTable += "Brand(s) #~#";
                            orderInfoTable += "All" + "~#~";
                        }
                    }
                }


                if (CustomProductSizeDesc != null)
                {
                    if (CustomProductSizeDesc.CheckedItems.Count >= 20 && CustomProductSizeDesc.CheckedItems.Count != CustomProductSizeDesc.Items.Count)
                    {
                        orderInfoTable += "Product Size Description #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(CustomProductSizeDesc).Replace("_", ", ") + "~#~";
                    }
                    else
                    {
                        if (CustomProductSizeDesc.CheckedItems.Count > 0 && AppManager.ProductSizeDesc(CustomProductSizeDesc) != "empty")
                        {
                            orderInfoTable += "Product Size Description #~#";
                            orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(CustomProductSizeDesc).Replace("_", ", ") + "~#~";
                        }
                        else if (CustomProductSizeDesc.CheckedItems.Count > 0 && CustomProductSizeDesc.CheckedItems.Count == CustomProductSizeDesc.Items.Count)
                        {
                            orderInfoTable += "Product Size Description #~#";
                            orderInfoTable += "All" + "~#~";
                        }
                    }
                }

                if (ProProductSizeDesc != null)
                {
                    if (ProProductSizeDesc.CheckedItems.Count >= 20 && ProProductSizeDesc.CheckedItems.Count != ProProductSizeDesc.Items.Count)
                    {
                        orderInfoTable += "Product Size Description #~#";
                        orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(ProProductSizeDesc).Replace("_", ", ") + "~#~";
                    }
                    else
                    {
                        if (ProProductSizeDesc.CheckedItems.Count > 0 && AppManager.ProductSizeDesc(ProProductSizeDesc) != "empty")
                        {
                            orderInfoTable += "Product Size Description #~#";
                            orderInfoTable += AppManager.GetSelectedItemsWithUnderScore(ProProductSizeDesc).Replace("_", ", ") + "~#~";
                        }
                        else if (ProProductSizeDesc.CheckedItems.Count > 0 && ProProductSizeDesc.CheckedItems.Count == ProProductSizeDesc.Items.Count)
                        {
                            orderInfoTable += "Product Size Description #~#";
                            orderInfoTable += "All" + "~#~";
                        }
                    }
                }

                //Brand Type
                if (BrandType != null)
                {
                    if (BrandType.SelectedValue != "0")
                    {
                        orderInfoTable += "Brand Type #~#";
                        orderInfoTable += BrandType.SelectedItem.Text + "~#~";
                    }
                    else
                    {
                        orderInfoTable += "Brand Type #~#";
                        orderInfoTable += "All" + "~#~";
                    }
                }

                //POG
                if (POG != null)
                {
                    orderInfoTable += "POG #~#";
                    orderInfoTable += POG.SelectedItem.Text + "~#~";
                }
                //ViewBy 
                if (ViewBy != null)
                {
                    orderInfoTable += "View By #~#";
                    orderInfoTable += ViewBy.SelectedItem.Text + "~#~";
                }

                if (!String.IsNullOrEmpty(Exec_Id))
                {
                    orderInfoTable += "Exec Identifier #~#";
                    orderInfoTable += Exec_Id + "~#~";
                }


            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "PDFExport.cs > SaveSelectedFilters"));
            }
            orderInfoTable = orderInfoTable.TrimEnd('~', '#', '~');
            return orderInfoTable;


        }
        #endregion

        public static void DownloadAsPDF(string Rname)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                string strHtml = string.Empty;
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                StringReader sr = new StringReader(sw.ToString());
                strHtml = sr.ReadToEnd();
                sr.Close();

                string url = "";
                string CheckUrl = HttpContext.Current.Request.Url.ToString();
                string NewUrl = "";
                int startIndex = CheckUrl.IndexOf(".com");
                if (startIndex > 0)
                {
                    NewUrl = CheckUrl.Substring(0, startIndex + 4);
                }
                else if (CheckUrl.IndexOf(".aspx") > 0)
                {
                    NewUrl = CheckUrl.Substring(0, CheckUrl.IndexOf(".aspx") + 4);

                }
                if (CheckUrl.Contains(NewUrl + ":8200"))
                    url = NewUrl + ":8200/files/" + Rname;
                else
                    url = NewUrl + "/files/" + Rname;
                //code to open pdf in popup
                string script = string.Format("window.open('{0}');", url);
                Page p = (Page)HttpContext.Current.CurrentHandler;
                if (ScriptManager.GetCurrent(p) != null)
                {
                    ScriptManager.RegisterStartupScript(p, typeof(Page), "Message", script, true);
                }
                else
                {
                    p.ClientScript.RegisterStartupScript(typeof(Page), "Message", script, true);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "PDFExport.cs > ExportFilters"));
            }
        }
        /// <summary>
        /// Export Filters
        /// </summary>
        public static void ExportFilters(string reportId, RadComboBox ddlFiscalyear = null, RadComboBox FromAdWeek = null, RadComboBox ToAdWeek = null, TextBox VendorNumber = null, RadComboBox LocBanner = null, RadComboBox LocSubBanner = null, RadComboBox Department = null,
                       RadComboBox Category = null, RadComboBox SubCategory = null, RadComboBox Segment = null, RadComboBox ProductBrand = null, RadComboBox ProProductSizeDesc = null, RadComboBox BrandType = null,
                       RadComboBox RecentTimeframe = null, RadComboBox CustomLevel1 = null, RadComboBox CustomLevel2 = null, RadComboBox CustomLevel3 = null, RadComboBox CustomBrand = null, RadComboBox CustomProductSizeDesc = null,
                       RadComboBox ViewBy = null, TextBox From = null, TextBox To = null, RadComboBox HierarchyName = null, RadComboBox Holiday = null, RadComboBox NumberofWeeksPrior = null, RadComboBox NumberofWeeksPost = null,
                       RadComboBox FromFiscalWeek = null, RadComboBox ToFiscalWeek = null, RadComboBox LocHierSrcBanner = null, RadComboBox LocHierSrcSubBanner = null, RadComboBox SrcStore = null, RadComboBox LocHierDesBanner = null,
                       RadComboBox LocHierDesSubBanner = null, RadComboBox DesStore = null, RadComboBox POG = null, TextBox LaunchDate = null, RadComboBox NumberofWeeks = null, RadComboBox CustomLevel1NewItems = null,
                       RadComboBox CustomLevel1DiscontinuedItems = null, TextBox DiscontinuedDate = null, RadComboBox HierarchyLocation = null, RadComboBox LocationStore = null, RadComboBox LocationCustom1 = null,
                       RadComboBox LocationCustom2 = null, RadComboBox LocationCustom3 = null, RadioButtonList DailyWeekly = null, RadComboBox ProductHierarchy = null)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                string ReportTitle = "";
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string query = "SELECT * from webmenus where Menuid='" + reportId + "'";
                    using (DataTable dt = dataAccessProvider.Select(query))
                    {
                        if (dt.Rows.Count > 0)
                        {
                            ReportTitle = dt.Rows[0]["MenuDescription"].ToString();
                        }
                    }

                }


                string Name = ReportTitle.Replace("%", "");
                string ReportName = Name + "-" + System.DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";

                string pdf_filename = HttpContext.Current.Request.PhysicalApplicationPath + "\\files\\" + ReportName;

                var titleFont = FontFactory.GetFont("Arial", 16, Font.BOLD);
                var boldTableFont = FontFactory.GetFont("Arial", 8, Font.NORMAL);
                var bodyFont = FontFactory.GetFont("Arial", 10, Font.NORMAL);

                var orderInfoTable = new PdfPTable(2);
                orderInfoTable.SpacingBefore = 40;
                orderInfoTable.DefaultCell.Border = 0;
                orderInfoTable.SetWidths(new int[] { 1, 4 });
                orderInfoTable.SplitLate = false;
                orderInfoTable.HorizontalAlignment = Element.ALIGN_LEFT;
                float[] widths = new float[] { 1f, 2f };
                orderInfoTable.SetWidths(widths);
                FileStream fs = new FileStream(pdf_filename, FileMode.Create, FileAccess.Write, FileShare.None);
                Document doc = new Document(PageSize.A4);
                     
                using (PdfWriter writer = PdfWriter.GetInstance(doc, fs))
                {
                    // Step 4: Openning the Document
                    doc.Open();
                    string ChainId = Convert.ToString(HttpContext.Current.Session["AccessValues"]);
                    string LogoName = DataManager.GetFieldValue("ChainLogo Where ChainId=" + ChainId, "LogoImageName");

                    var logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("/Images/ChainLogo/") + LogoName);
                    logo.ScaleAbsolute(120f, 40f);

                    //Tal suggested just have plce holder,we will use logo later
                    // PdfPCell cell = new PdfPCell(logo);
                    PdfPCell cell = new PdfPCell();
                    cell.Border = 0;
                    cell.FixedHeight = 40f;
                    orderInfoTable.AddCell(cell);
                    orderInfoTable.AddCell(new PdfPCell(new Phrase(ReportTitle, titleFont)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER });

                    doc.Add(orderInfoTable);


                    orderInfoTable = new PdfPTable(2);
                    orderInfoTable.HorizontalAlignment = Element.ALIGN_LEFT;
                    orderInfoTable.SetWidths(new float[] { 1f, 2f });
                    orderInfoTable.SpacingBefore = 20;
                    orderInfoTable.SpacingAfter = 20;
                    orderInfoTable.SplitLate = false;

                    if (DailyWeekly != null)
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Select Week View:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(DailyWeekly.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, Rowspan = 1 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 }); ;
                    }

                    if (Holiday != null)
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Holiday:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(Holiday.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                    }

                    if (ddlFiscalyear != null)
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Fiscal Year:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(ddlFiscalyear.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                    }

                    if (NumberofWeeksPrior != null)
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Prior Week(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(NumberofWeeksPrior.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                    }

                    if (NumberofWeeksPost != null)
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Post Week(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(NumberofWeeksPost.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                    }

                    //Recent Time Frame
                    if (RecentTimeframe != null)
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Recent Timeframe:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(RecentTimeframe.SelectedItem.Text, boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                    }
                    //Lauch Date
                    if (LaunchDate != null && !string.IsNullOrEmpty(LaunchDate.Text))
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Launch Date:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.FromDate(LaunchDate.Text).ToString("MM/dd/yyyy"), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                    }
                    //Discontinued Date
                    if (DiscontinuedDate != null && !String.IsNullOrEmpty(DiscontinuedDate.Text))
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Discontinued Date:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.FromDate(DiscontinuedDate.Text).ToString("MM/dd/yyyy"), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                    }
                    if (NumberofWeeks != null)
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Number of Weeks:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(NumberofWeeks.SelectedItem.Text, boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                    }


                    // Fiscal Year
                    if (ddlFiscalyear != null)
                    {

                        if (ddlFiscalyear.CheckedItems.Count > 0)
                        {
                            orderInfoTable.AddCell(new PdfPCell(new Phrase("Fiscal Year:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                            orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItems(ddlFiscalyear), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        }
                        else if (ddlFiscalyear.CheckedItems.Count > 0 && ddlFiscalyear.CheckedItems.Count == ddlFiscalyear.Items.Count)
                        {
                            orderInfoTable.AddCell(new PdfPCell(new Phrase("Fiscal Year:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                            orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        }

                    }


                    //From Date
                    if (From != null && !string.IsNullOrEmpty(From.Text))
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("From Date:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.FromDate(From.Text).ToString("MM/dd/yyyy"), boldTableFont)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                    }

                    //To Date
                    if (To != null && !string.IsNullOrEmpty(To.Text))
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("To Date:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.FromDate(To.Text).ToString("MM/dd/yyyy"), boldTableFont)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                    }

                    //View By
                    if (ViewBy != null)
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("View By:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(ViewBy.SelectedItem.Text, boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                    }

                    //From AdWeek
                    if (FromAdWeek != null)
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("From AdWeek:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.FromAdWeekValue(FromAdWeek), boldTableFont)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                    }

                    //From ToWeek
                    if (ToAdWeek != null)
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("To AdWeek:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.ToAdWeekValue(ToAdWeek), boldTableFont)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                    }
                    //From Fiscal Week
                    if (FromFiscalWeek != null)
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("From Fisacal Week:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.FromFiscalWeekValue(FromFiscalWeek), boldTableFont)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                    }

                    //To Fiscal Week
                    if (ToFiscalWeek != null)
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("To Fiscal Week:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.ToFiscalWeekValue(ToFiscalWeek), boldTableFont)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                    }



                    if (VendorNumber != null && !string.IsNullOrEmpty(VendorNumber.Text))
                    {

                        if (!String.IsNullOrEmpty(VendorNumber.Text.Trim()))
                        {
                            orderInfoTable.AddCell(new PdfPCell(new Phrase("Vendor:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                            String Vendor = VendorNumber.Text + ", " + AppManager.GetVendorName(VendorNumber.Text);
                            //ExportStr += "<tr><td valign='top' style='font-size:10px;'>Vendor:</td><td style='font-size:10px;'>" + txtVendorNumber.Text + ", " + DashboardManager.GetVendorName(txtVendorNumber.Text.Trim()) + "</td></tr><tr style='Height:10px;'></tr>";
                            orderInfoTable.AddCell(new PdfPCell(new Phrase(Vendor, boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        }


                    }


                    //LOCATION SECTION
                    #region LOCATION SECTION
                    if (HierarchyLocation != null)
                    {

                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Location Hierarchy:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(HierarchyLocation.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                        #region LocationHierarchy Exists and CustomStore Group  Selected
                        if (HierarchyLocation.SelectedItem.Text == IcontrolFilterResources.CustomStoreGroupHierarchy)
                        {
                            //customStoreGroup
                            if (LocationStore != null)
                            {
                                if (LocationStore.CheckBoxes)
                                {
                                    if (LocationStore.CheckedItems.Count > 0)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Store Group(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItems(LocationStore), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                                    }
                                    else if (LocationStore.CheckedItems.Count > 0 && LocationStore.CheckedItems.Count == LocationStore.Items.Count)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Store Group(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                }
                                //For single selection of custom store group
                                else
                                {

                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Store Group(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(LocationStore.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }

                            }

                            //Custom Level 1
                            if (LocationCustom1 != null)
                            {

                                if (LocationCustom1.CheckedItems.Count > 0)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 1(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    //orderInfoTable.AddCell(new PdfPCell(new Phrase(DashboardManager.GetSelectedItemsWithUnderScore(ddlAdhocLevel1).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(LocationCustom1).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                                }
                                else if (LocationCustom1.CheckedItems.Count > 0 && LocationCustom1.CheckedItems.Count == LocationCustom1.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 1(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }

                            }
                            // Custom Level 2

                            if (LocationCustom2 != null)
                            {

                                if (LocationCustom2.CheckedItems.Count > 0)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 2(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    //orderInfoTable.AddCell(new PdfPCell(new Phrase(DashboardManager.GetSelectedItemsWithUnderScore(ddladhoclevel2).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(LocationCustom2).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                                }
                                else if (LocationCustom2.CheckedItems.Count > 0 && LocationCustom2.CheckedItems.Count == LocationCustom2.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 2(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }

                            }

                            // Custom Level 3
                            if (LocationCustom3 != null)
                            {

                                if (LocationCustom3.CheckedItems.Count > 0)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 3(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    //orderInfoTable.AddCell(new PdfPCell(new Phrase(DashboardManager.GetSelectedItemsWithUnderScore(ddladhoclevel3).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(LocationCustom3).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                                }
                                else if (LocationCustom3.CheckedItems.Count > 0 && LocationCustom3.CheckedItems.Count == LocationCustom3.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 3(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }

                            }

                            #endregion

                            #region    LocationHierarchy Exists and Corporate Selected
                            else
                            {
                                //Banner
                                if (LocBanner != null)
                                {
                                    if (LocBanner.CheckBoxes)
                                    {
                                        if (LocBanner.CheckedItems.Count > 0 && AppManager.SubBanner(LocBanner) != "empty")
                                        {

                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });


                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                                        }
                                        else if (LocBanner.CheckedItems.Count > 0 && LocBanner.CheckedItems.Count == LocBanner.Items.Count)
                                        {
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        }
                                    }

                                }
                                //Sub Banner
                                if (LocSubBanner != null)
                                {

                                    if (LocSubBanner.CheckedItems.Count >= 20 && LocSubBanner.CheckedItems.Count != LocSubBanner.Items.Count)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocSubBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                    else
                                    {
                                        if (LocSubBanner.CheckedItems.Count > 0)
                                        {
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocSubBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        }
                                        else if (LocSubBanner.CheckedItems.Count > 0 && LocSubBanner.CheckedItems.Count == LocSubBanner.Items.Count)
                                        {
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        }
                                    }
                                }

                            }
                        }
                        else
                        {
                            //Banner
                            if (LocBanner != null)
                            {
                                if (LocBanner.CheckBoxes)
                                {
                                    if (LocBanner.CheckedItems.Count > 0 && AppManager.SubBanner(LocBanner) != "empty")
                                    {

                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });


                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                                    }
                                    else if (LocBanner.CheckedItems.Count > 0 && LocBanner.CheckedItems.Count == LocBanner.Items.Count)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                }

                            }
                            //Sub Banner
                            if (LocSubBanner != null)
                            {

                                if (LocSubBanner.CheckedItems.Count >= 20 && LocSubBanner.CheckedItems.Count != LocSubBanner.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocSubBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else
                                {
                                    if (LocSubBanner.CheckedItems.Count > 0)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocSubBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                    else if (LocSubBanner.CheckedItems.Count > 0 && LocSubBanner.CheckedItems.Count == LocSubBanner.Items.Count)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region if Location Hierarchy Not exist
                    else
                    {
                        //customStoreGroup
                        if (LocationStore != null)
                        {
                            if (LocationStore.CheckBoxes)
                            {
                                if (LocationStore.CheckedItems.Count > 0)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Store Group(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItems(LocationStore), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                                }
                                else if (LocationStore.CheckedItems.Count > 0 && LocationStore.CheckedItems.Count == LocationStore.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Store Group(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                            }
                            //For single selection of custom store group
                            else
                            {

                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Store Group(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(LocationStore.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }

                        }

                        //Custom Level 1
                        if (LocationCustom1 != null)
                        {

                            if (LocationCustom1.CheckedItems.Count > 0)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 1(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                //orderInfoTable.AddCell(new PdfPCell(new Phrase(DashboardManager.GetSelectedItemsWithUnderScore(ddlAdhocLevel1).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(LocationCustom1).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                            }
                            else if (LocationCustom1.CheckedItems.Count > 0 && LocationCustom1.CheckedItems.Count == LocationCustom1.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 1(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }

                        }
                        // Custom Level 2

                        if (LocationCustom2 != null)
                        {

                            if (LocationCustom2.CheckedItems.Count > 0)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 2(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                //orderInfoTable.AddCell(new PdfPCell(new Phrase(DashboardManager.GetSelectedItemsWithUnderScore(ddladhoclevel2).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(LocationCustom2).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                            }
                            else if (LocationCustom2.CheckedItems.Count > 0 && LocationCustom2.CheckedItems.Count == LocationCustom2.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 2(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }

                        }

                        // Custom Level 3
                        if (LocationCustom3 != null)
                        {

                            if (LocationCustom3.CheckedItems.Count > 0)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 3(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                //orderInfoTable.AddCell(new PdfPCell(new Phrase(DashboardManager.GetSelectedItemsWithUnderScore(ddladhoclevel3).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(LocationCustom3).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                            }
                            else if (LocationCustom3.CheckedItems.Count > 0 && LocationCustom3.CheckedItems.Count == LocationCustom3.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 3(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }

                        }
                        if (LocHierSrcBanner != null)
                        {

                            if (LocHierSrcBanner.CheckedItems.Count > 0)
                            {

                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Banner(s) Source:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocHierSrcBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });


                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                            }
                            else if (LocHierSrcBanner.CheckedItems.Count > 0 && LocHierSrcBanner.CheckedItems.Count == LocHierSrcBanner.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Banner(s) Source:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }

                        }
                        //Sub Banner
                        if (LocHierSrcSubBanner != null)
                        {
                            if (LocHierSrcSubBanner.CheckedItems.Count >= 20 && LocHierSrcSubBanner.CheckedItems.Count != LocHierSrcSubBanner.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s) Source:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocHierSrcSubBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }
                            else
                            {
                                if (LocHierSrcSubBanner.CheckedItems.Count > 0)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s) Source:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocHierSrcSubBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else if (LocHierSrcSubBanner.CheckedItems.Count > 0 && LocHierSrcSubBanner.CheckedItems.Count == LocHierSrcSubBanner.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s) Source:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                            }
                        }

                        //Store
                        if (SrcStore != null)
                        {
                            if (SrcStore.CheckedItems.Count > 0)
                            {

                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Store(s) Source:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItems(SrcStore).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });


                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                            }
                            else if (SrcStore.CheckedItems.Count > 0 && SrcStore.CheckedItems.Count == SrcStore.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Store(s) Source:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }
                        }

                        if (LocHierDesBanner != null)
                        {
                            if (LocHierDesBanner.CheckedItems.Count > 0)
                            {

                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Banner(s) - Opportunity (Destination) :", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocHierDesBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });


                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                            }
                            else if (LocHierDesBanner.CheckedItems.Count > 0 && LocHierDesBanner.CheckedItems.Count == LocHierDesBanner.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Banner(s) - Opportunity (Destination) :", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }

                        }
                        //Sub Banner
                        if (LocHierDesSubBanner != null)
                        {


                            if (LocHierDesSubBanner.CheckedItems.Count >= 20 && LocHierDesSubBanner.CheckedItems.Count != LocHierDesSubBanner.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s) - Opportunity (Destination) :", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocHierDesSubBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }
                            else
                            {
                                if (LocHierDesSubBanner.CheckedItems.Count > 0)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s) - Opportunity (Destination) :", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocHierDesSubBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else if (LocHierDesSubBanner.CheckedItems.Count > 0 && LocHierDesSubBanner.CheckedItems.Count == LocHierDesSubBanner.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s) - Opportunity (Destination) :", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                            }
                        }

                        //Store
                        if (DesStore != null)
                        {
                            if (DesStore.CheckedItems.Count > 0)
                            {

                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Store(s) - Opportunity (Destination) :", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItems(DesStore).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                            }
                            else if (DesStore.CheckedItems.Count > 0 && DesStore.CheckedItems.Count == DesStore.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Store(s) - Opportunity (Destination) :", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }
                        }

                        //Banner
                        if (LocBanner != null)
                        {
                            if (LocBanner.CheckBoxes)
                            {
                                if (LocBanner.CheckedItems.Count > 0 && AppManager.SubBanner(LocBanner) != "empty")
                                {

                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });


                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                                }
                                else if (LocBanner.CheckedItems.Count > 0 && LocBanner.CheckedItems.Count == LocBanner.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                            }

                        }
                        //Sub Banner
                        if (LocSubBanner != null)
                        {

                            if (LocSubBanner.CheckedItems.Count >= 20 && LocSubBanner.CheckedItems.Count != LocSubBanner.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocSubBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }
                            else
                            {
                                if (LocSubBanner.CheckedItems.Count > 0)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(LocSubBanner).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else if (LocSubBanner.CheckedItems.Count > 0 && LocSubBanner.CheckedItems.Count == LocSubBanner.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub Banner(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                            }
                        }
                    }


                    #endregion

                    #endregion

                    //PRODUCT SECTION 
                    #region PRODUCT SECTION
                    if (ProductHierarchy != null)
                    {
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Product Hierarchy:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase(ProductHierarchy.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                        #region Product Hierarchy Exists and Custon Selected
                        if (ProductHierarchy.SelectedItem.Text == IcontrolFilterResources.CustomHierarchy)
                        {
                            if (HierarchyName != null)
                            {
                                if (HierarchyName.SelectedIndex > 0)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Hierarchy Name(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(HierarchyName.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                            }

                            if (CustomLevel1 != null)
                            {

                                if (CustomLevel1.CheckedItems.Count > 0)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 1:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(CustomLevel1).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                                }
                                else if (CustomLevel1.CheckedItems.Count > 0 && CustomLevel1.CheckedItems.Count == CustomLevel1.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 1:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }

                            }
                            if (CustomLevel2 != null)
                            {

                                if (CustomLevel2.CheckedItems.Count > 0)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 2:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(CustomLevel2).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                                }
                                else if (CustomLevel2.CheckedItems.Count > 0 && CustomLevel2.CheckedItems.Count == CustomLevel2.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 2:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }

                            }
                            if (CustomLevel3 != null)
                            {

                                if (CustomLevel3.CheckedItems.Count > 0)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 3:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(CustomLevel3).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else if (CustomLevel3.CheckedItems.Count > 0 && CustomLevel3.CheckedItems.Count == CustomLevel3.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 3:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }

                            }

                            //Brand
                            if (CustomBrand != null)
                            {
                                if (CustomBrand.CheckedItems.Count >= 20 && CustomBrand.CheckedItems.Count != CustomBrand.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Brand(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(CustomBrand).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else
                                {
                                    if (CustomBrand.CheckedItems.Count > 0 && AppManager.Brand(CustomBrand) != "empty")
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Brand(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(CustomBrand).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                    else if (CustomBrand.CheckedItems.Count > 0 && CustomBrand.CheckedItems.Count == CustomBrand.Items.Count)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Brand(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }

                                }

                            }

                            if (CustomProductSizeDesc != null)
                            {

                                if (CustomProductSizeDesc.CheckedItems.Count >= 20 && CustomProductSizeDesc.CheckedItems.Count != CustomProductSizeDesc.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Product Size Description:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(CustomProductSizeDesc).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else
                                {
                                    if (CustomProductSizeDesc.CheckedItems.Count > 0 && AppManager.ProductSizeDesc(CustomProductSizeDesc) != "empty")
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Product Size Description:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(CustomProductSizeDesc).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                    else if (CustomProductSizeDesc.CheckedItems.Count > 0 && CustomProductSizeDesc.CheckedItems.Count == CustomProductSizeDesc.Items.Count)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Product Size Description:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }

                                }

                            }
                        }
                        #endregion

                        #region   ProductHierarchy Exists and Corporate  Selected
                        else
                        {
                            //Department
                            if (Department != null)
                            {
                                if (Department.CheckedItems.Count >= 20 && Department.CheckedItems.Count != Department.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Department(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(Department).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                                }
                                else
                                {
                                    if (Department.CheckedItems.Count > 0)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Department(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(Department).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                    else if (Department.CheckedItems.Count > 0 && Department.CheckedItems.Count == Department.Items.Count)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Department(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }

                                }

                            }

                            //Category
                            if (Category != null)
                            {
                                if (Category.CheckBoxes)
                                {
                                    if (Category.CheckedItems.Count >= 20 && Category.CheckedItems.Count != Category.Items.Count)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(Category).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                    else
                                    {
                                        if (Category.CheckedItems.Count > 0 && AppManager.Category(Category) != "empty")
                                        {
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(Category).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        }
                                        else if (Category.CheckedItems.Count > 0 && Category.CheckedItems.Count == Category.Items.Count)
                                        {
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        }

                                    }
                                }
                                else
                                {
                                    // single category

                                    if (Category.SelectedIndex > 0)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(Category.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                                    }


                                }

                            }

                            //Sub Category
                            if (SubCategory != null)
                            {
                                if (SubCategory.CheckedItems.Count >= 20 && SubCategory.CheckedItems.Count != SubCategory.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub-Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(SubCategory).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else
                                {
                                    if (SubCategory.CheckedItems.Count > 0 && AppManager.SubCategory(SubCategory) != "empty")
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub-Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(SubCategory).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                    else if (SubCategory.CheckedItems.Count > 0 && SubCategory.CheckedItems.Count == SubCategory.Items.Count)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub-Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }

                                }

                            }

                            //Segment
                            if (Segment != null)
                            {
                                if (Segment.CheckedItems.Count >= 20 && Segment.CheckedItems.Count != Segment.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Segment(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(Segment).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else
                                {
                                    if (Segment.CheckedItems.Count > 0 && AppManager.Segment(Segment) != "empty")
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Segment(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(Segment).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                    else if (Segment.CheckedItems.Count > 0 && Segment.CheckedItems.Count == Segment.Items.Count)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Segment(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                }

                            }

                            //Brand
                            if (ProductBrand != null)
                            {
                                if (ProductBrand.CheckedItems.Count >= 20 && ProductBrand.CheckedItems.Count != ProductBrand.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Brand(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(ProductBrand).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else
                                {
                                    if (ProductBrand.CheckedItems.Count > 0 && AppManager.Brand(ProductBrand) != "empty")
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Brand(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(ProductBrand).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                    else if (ProductBrand.CheckedItems.Count > 0 && ProductBrand.CheckedItems.Count == ProductBrand.Items.Count)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Brand(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }

                                }

                            }

                            if (ProProductSizeDesc != null)
                            {

                                if (ProProductSizeDesc.CheckedItems.Count >= 20 && ProProductSizeDesc.CheckedItems.Count != ProProductSizeDesc.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Product Size Description:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(ProProductSizeDesc).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else
                                {
                                    if (ProProductSizeDesc.CheckedItems.Count > 0 && AppManager.ProductSizeDesc(ProProductSizeDesc) != "empty")
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Product Size Description:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(ProProductSizeDesc).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                    else if (ProProductSizeDesc.CheckedItems.Count > 0 && ProProductSizeDesc.CheckedItems.Count == ProProductSizeDesc.Items.Count)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Product Size Description:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }

                                }

                            }

                            //Brand Type
                            if (BrandType != null)
                            {
                                if (BrandType.SelectedValue != "0")
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Brand Type:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(BrandType.SelectedItem.Text, boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Brand Type:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }

                            }

                            //POG
                            if (POG != null)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("POG:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(POG.SelectedItem.Text, boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }
                        }
                        #endregion
                    }

                    else
                    {
                        if (Department != null)
                        {
                            if (Department.CheckedItems.Count >= 20 && Department.CheckedItems.Count != Department.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Department(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(Department).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                            }
                            else
                            {
                                if (Department.CheckedItems.Count > 0)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Department(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(Department).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else if (Department.CheckedItems.Count > 0 && Department.CheckedItems.Count == Department.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Department(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }

                            }

                        }

                        //Category
                        if (Category != null)
                        {
                            if (Category.CheckBoxes)
                            {
                                if (Category.CheckedItems.Count >= 20 && Category.CheckedItems.Count != Category.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(Category).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else
                                {
                                    if (Category.CheckedItems.Count > 0 && AppManager.Category(Category) != "empty")
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(Category).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }
                                    else if (Category.CheckedItems.Count > 0 && Category.CheckedItems.Count == Category.Items.Count)
                                    {
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                        orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    }

                                }
                            }
                            else
                            {
                                // single category

                                if (Category.SelectedIndex > 0)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(Category.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                                }
                            }

                        }

                        //Custom Hierarchy
                        if (HierarchyName != null)
                        {
                            if (HierarchyName.SelectedIndex > 0)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Hierarchy Name(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(HierarchyName.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }


                        }

                        if (CustomLevel1 != null)
                        {

                            if (CustomLevel1.CheckedItems.Count > 0)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 1:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                //orderInfoTable.AddCell(new PdfPCell(new Phrase(DashboardManager.GetSelectedItemsWithUnderScore(ddlcustomlevel1).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(CustomLevel1).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                            }
                            else if (CustomLevel1.CheckedItems.Count > 0 && CustomLevel1.CheckedItems.Count == CustomLevel1.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 1:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }

                        }
                        //Custom Level 1 (New Items)

                        if (CustomLevel1NewItems != null)
                        {
                            if (CustomLevel1NewItems.SelectedIndex > 0)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 1(New Items):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(CustomLevel1NewItems.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }

                        }

                        //Custom Level 1 (Discontinued Items)
                        if (CustomLevel1DiscontinuedItems != null)
                        {
                            if (CustomLevel1DiscontinuedItems.SelectedIndex > 0)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 1(Discontinued Items):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(CustomLevel1DiscontinuedItems.SelectedItem.Text.ToString(), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }

                        }
                        if (CustomLevel2 != null)
                        {

                            if (CustomLevel2.CheckedItems.Count > 0)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 2:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                //orderInfoTable.AddCell(new PdfPCell(new Phrase(DashboardManager.GetSelectedItemsWithUnderScore(ddlcustomlevel2).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(CustomLevel2).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                            }
                            else if (CustomLevel2.CheckedItems.Count > 0 && CustomLevel2.CheckedItems.Count == CustomLevel2.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 2:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }

                        }
                        if (CustomLevel3 != null)
                        {

                            if (CustomLevel3.CheckedItems.Count > 0)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 3:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                //orderInfoTable.AddCell(new PdfPCell(new Phrase(DashboardManager.GetSelectedItemsWithUnderScore(ddlcustomlevel3).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(CustomLevel3).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }
                            else if (CustomLevel3.CheckedItems.Count > 0 && CustomLevel3.CheckedItems.Count == CustomLevel3.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Custom Level 3:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }

                        }

                        //Sub Category
                        if (SubCategory != null)
                        {
                            if (SubCategory.CheckedItems.Count >= 20 && SubCategory.CheckedItems.Count != SubCategory.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub-Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(SubCategory).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }
                            else
                            {
                                if (SubCategory.CheckedItems.Count > 0 && AppManager.SubCategory(SubCategory) != "empty")
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub-Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(SubCategory).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else if (SubCategory.CheckedItems.Count > 0 && SubCategory.CheckedItems.Count == SubCategory.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Sub-Category(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }

                            }

                        }

                        //Segment
                        if (Segment != null)
                        {
                            if (Segment.CheckedItems.Count >= 20 && Segment.CheckedItems.Count != Segment.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Segment(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(Segment).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }
                            else
                            {
                                if (Segment.CheckedItems.Count > 0 && AppManager.Segment(Segment) != "empty")
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Segment(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(Segment).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else if (Segment.CheckedItems.Count > 0 && Segment.CheckedItems.Count == Segment.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Segment(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });

                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                            }

                        }

                        //Brand
                        RadComboBox brand = null;

                        if (ProductBrand != null || CustomBrand != null)
                        {
                            brand = ProductBrand != null ? ProductBrand : CustomBrand;
                            if (brand.CheckedItems.Count >= 20 && brand.CheckedItems.Count != brand.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Brand(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(brand).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }
                            else
                            {
                                if (brand.CheckedItems.Count > 0 && AppManager.Brand(brand) != "empty")
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Brand(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithSpecialChar(brand).Replace("^@~", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else if (brand.CheckedItems.Count > 0 && brand.CheckedItems.Count == brand.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Brand(s):", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }

                            }

                        }
                        RadComboBox ProductSizeDesc = null;
                        if (ProProductSizeDesc != null || CustomProductSizeDesc != null)
                        {
                            ProductSizeDesc = ProProductSizeDesc != null ? ProProductSizeDesc : CustomProductSizeDesc;
                            if (ProductSizeDesc.CheckedItems.Count >= 20 && ProductSizeDesc.CheckedItems.Count != ProductSizeDesc.Items.Count)
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Product Size Description:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(ProductSizeDesc).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }
                            else
                            {
                                if (ProductSizeDesc.CheckedItems.Count > 0 && AppManager.ProductSizeDesc(ProductSizeDesc) != "empty")
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Product Size Description:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase(AppManager.GetSelectedItemsWithUnderScore(ProductSizeDesc).Replace("_", ", "), boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }
                                else if (ProductSizeDesc.CheckedItems.Count > 0 && ProductSizeDesc.CheckedItems.Count == ProductSizeDesc.Items.Count)
                                {
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("Product Size Description:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                    orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                }

                            }

                        }

                        //Brand Type
                        if (BrandType != null)
                        {
                            if (BrandType.SelectedValue != "0")
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Brand Type:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase(BrandType.SelectedItem.Text, boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }
                            else
                            {
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("Brand Type:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("All", boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                                orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            }

                        }

                        //POG
                        if (POG != null)
                        {
                            orderInfoTable.AddCell(new PdfPCell(new Phrase("POG:", bodyFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                            orderInfoTable.AddCell(new PdfPCell(new Phrase(POG.SelectedItem.Text, boldTableFont)) { Colspan = 1, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });
                            orderInfoTable.AddCell(new PdfPCell(new Phrase("", bodyFont)) { Border = 0 });

                        }
                    }
                    #endregion

                    doc.Add(orderInfoTable);

                    var logo1 = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/images/logo.png"));
                    logo1.ScaleAbsolute(120, 30);

                    logo1.Alignment = 6;
                    doc.Add(logo1);

                    // Step 6: Closing the Document

                    writer.Flush();
                    doc.Close();
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Receipt-{0}.pdf", ReportName));
                    writer.Close();
                    fs.Close();
                    DownloadAsPDF(ReportName);
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "PDFExport.cs > ExportFilters"));
            }
        }

        /// <summary>
        /// fill saved Filters
        /// </summary>
        public static void FillSavedFilters(string savedReportId, string reportId, string LoginUserName, string LoginUserProfileName, RadComboBox ddlFiscalyear = null, RadComboBox ddlFromAdWeek = null, RadComboBox ddlToAdWeek = null, TextBox txtVendorNumber = null, RadComboBox ddlLocBanner = null, RadComboBox ddlLocSubBanner = null, RadComboBox ddlDepartment = null,
                       RadComboBox ddlCategory = null, RadComboBox ddlSubCategory = null, RadComboBox ddlSegment = null, RadComboBox ddlProductBrand = null, RadComboBox ddlProProductSizeDesc = null, RadComboBox ddlBrandType = null,
                       RadComboBox ddlRecentTimeframe = null, RadComboBox ddlCustomLevel1 = null, RadComboBox ddlCustomLevel2 = null, RadComboBox ddlCustomLevel3 = null, RadComboBox ddlCustomBrand = null, RadComboBox ddlCustomProductSizeDesc = null,
                       RadComboBox ddlViewBy = null, TextBox txtFrom = null, TextBox txtTo = null, RadComboBox ddlHierarchyName = null, RadComboBox ddlHoliday = null, RadComboBox ddlNumberofWeeksPrior = null, RadComboBox ddlNumberofWeeksPost = null,
                       RadComboBox ddlFromFiscalWeek = null, RadComboBox ddlToFiscalWeek = null, RadComboBox ddlLocHierSrcBanner = null, RadComboBox ddlLocHierSrcSubBanner = null, RadComboBox ddlSrcStore = null, RadComboBox ddlLocHierDesBanner = null,
                       RadComboBox ddlLocHierDesSubBanner = null, RadComboBox ddlDesStore = null, RadComboBox ddlPOG = null, TextBox txtLaunchDate = null, RadComboBox ddlNumberofWeeks = null, RadComboBox ddlCustomLevel1NewItems = null,
                       RadComboBox ddlCustomLevel1DiscontinuedItems = null, TextBox txtDiscontinuedDate = null, RadComboBox ddlHierarchyLocation = null, RadComboBox ddlLocationStore = null, RadComboBox ddlLocationCustom1 = null,
                       RadComboBox ddlLocationCustom2 = null, RadComboBox ddlLocationCustom3 = null, RadioButtonList rdoDailyWeekly = null, RadComboBox ddlProductHierarchy = null, DataTable dtBannerAndSubBannerDetails = null, DataTable dtProductDetailsCustomHierarchy = null, DataTable dtLocationDetailsCustomHierarchy = null)
        {
            IControlHelper helper = new IControlHelper();
            DataLayer dataAccessLayer = new DataLayer();
            try
            {
                int i = 0;
                string BannerIds = "0";
                string DesBannerIds = "0";
                string SubBannerIds = "0";
                string DesSubBannerIds = "0";
                string DeptId = "0";
                string DeptIdAll = "0";
                string catId = "0";
                string catIdAll = "0";
                string subcat = "0";
                string segment = "0";
                string Hierarchy = "0";
                string customstore = "0";
                string RecentTimeframe = "0";
                string Holiday = "0";
                using (DataTable dtReportDetails = DataManager.GetSavedReportDetails(savedReportId))
                {
                    string[] RepPar = dtReportDetails.Rows[0]["ReportParameters"].ToString().Split('&');
                    DataTable dtProductDetailsCorporateHierarchy = null;
                    for (i = 0; i < RepPar.Length; i++)
                    {
                        string getvalue = "";

                        if (rdoDailyWeekly != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_WEEKLY_VIEW.Replace(" ", "%20")))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');

                                if (getdt.Length > 0 && !Convert.ToBoolean(getdt[1]))
                                {
                                    rdoDailyWeekly.SelectedValue = "1";
                                }

                            }
                        }
                        if (ddlFromAdWeek != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_FROMADD_WEEK))
                            {
                                string[] DateFilters;
                                DateFilters = dtReportDetails.Rows[0]["DateRange"].ToString().Split('-');
                                if (DateFilters.Length > 1)
                                {
                                    string NewStartDate = AppManager.StartDateForSaveFilterForWeek(Convert.ToInt16(DateFilters[1]) * 7).ToString("yyyy-MM-dd");
                                    string Fromdtsql = "SELECT 'FY' || FISCAL_YEAR_NUM || case when Length(FISCAL_WEEK_OF_YEAR_NUM) =1 then  '-W0' ||FISCAL_WEEK_OF_YEAR_NUM  else '-W' || FISCAL_WEEK_OF_YEAR_NUM end as [Param] FROM DIM_WEEKLY_CALENDAR Where WEEK_START_DATE ='" + NewStartDate + "' order by WEEK_START_DATE desc";
                                    using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                                    {
                                        using (DataTable dt = dataAccessProvider.Select(Fromdtsql))
                                        {
                                            if (dt.Rows.Count > 0)
                                            {
                                                ddlFromAdWeek.SelectedValue = dt.Rows[0]["Param"].ToString();
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    ddlFromAdWeek.SelectedValue = getdt[1];
                                }
                            }
                        }
                        if (ddlToAdWeek != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_TOADD_WEEK))
                            {
                                string[] DateFilters;
                                DateFilters = dtReportDetails.Rows[0]["DateRange"].ToString().Split('-');
                                if (DateFilters.Length > 1)
                                {
                                    string ToDtSql = "SELECT 'FY' || FISCAL_YEAR_NUM || case when Length(FISCAL_WEEK_OF_YEAR_NUM) =1 then  '-W0' ||FISCAL_WEEK_OF_YEAR_NUM  else '-W' || FISCAL_WEEK_OF_YEAR_NUM end as [Param] FROM DIM_WEEKLY_CALENDAR Where FISCAL_WEEK_END_DT ='" + AppManager.EndDate().ToString("yyyy-MM-dd") + "' order by WEEK_START_DATE desc";
                                    using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                                    {
                                        using (DataTable dt = dataAccessProvider.Select(ToDtSql))
                                        {
                                            if (dt.Rows.Count > 0)
                                            {
                                                ddlToAdWeek.SelectedValue = dt.Rows[0]["Param"].ToString();
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    ddlToAdWeek.SelectedValue = getdt[1];
                                }
                            }
                        }
                        if (txtVendorNumber != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_VENDOR_NUMBER.Replace(" ", "%20")))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (getdt[1] != "Select%20Vendor%20(If%20Relevant)")
                                {
                                    txtVendorNumber.Text = getdt[1];
                                }
                            }
                        }
                        if (ddlHierarchyLocation != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_CUSTOM_STORE_ID))
                            {
                                ddlHierarchyLocation.SelectedItem.Text = IcontrolFilterResources.CustomStoreGroupHierarchy;
                            }
                        }

                        #region Location Corporate Hierarchy

                        if (ddlLocBanner != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_BANNER_ID))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (BannerIds == "0")
                                {
                                    getvalue = getdt[1];
                                    if (!String.IsNullOrEmpty(getvalue))
                                    {

                                        DataRow[] BannerDetails = dtBannerAndSubBannerDetails.Select(AppConstants.BANNER_ID_COLUMN + " IN (" + getvalue + ")");
                                        DataTable dtBanners = BannerDetails.CopyToDataTable();
                                        dtBanners = dtBanners.DefaultView.ToTable(true, AppConstants.BANNER_COLUMN);
                                        if (dtBanners != null && dtBanners.Rows.Count > 0)
                                        {
                                            SelectDefaultItem(ddlLocBanner, dtBanners);
                                        }
                                    }
                                    else
                                    {
                                        foreach (RadComboBoxItem item in ddlLocBanner.Items)
                                        {
                                            item.Checked = true;
                                        }
                                    }
                                    BannerIds = AppManager.GetSelectedItemsWithQuotes(ddlLocBanner);
                                    FillSubBanner(BannerIds, ddlLocSubBanner, ddlSrcStore, dtBannerAndSubBannerDetails);
                                }
                            }
                        }
                        if (ddlLocSubBanner != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_SUB_BANNER_ID))
                            {
                                if (BannerIds == "0" || String.IsNullOrEmpty(BannerIds))
                                {

                                    foreach (RadComboBoxItem item in ddlLocBanner.Items)
                                    {
                                        item.Checked = true;
                                    }
                                    BannerIds = AppManager.GetSelectedItemsWithQuotes(ddlLocBanner);
                                    FillSubBanner(BannerIds, ddlLocSubBanner, ddlSrcStore, dtBannerAndSubBannerDetails);
                                }

                                if (SubBannerIds == "0")
                                {
                                    string getvalueSubbanner = "";
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    getvalueSubbanner = getdt[1];
                                    if (!String.IsNullOrEmpty(getvalueSubbanner))
                                    {
                                        DataRow[] SubBannerDetails = dtBannerAndSubBannerDetails.Select(AppConstants.SUBBANNER_ID_COLUMN + " IN (" + getvalueSubbanner + ")");
                                        DataTable dtSubBanners = SubBannerDetails.CopyToDataTable();
                                        dtSubBanners = dtSubBanners.DefaultView.ToTable(true, AppConstants.SUBBANNER_COLUMN);
                                        if (dtSubBanners != null && dtSubBanners.Rows.Count > 0)
                                        {
                                            ddlLocSubBanner.ClearCheckedItems();
                                            SelectDefaultItem(ddlLocSubBanner, dtSubBanners);
                                        }
                                    }
                                    else
                                    {
                                        foreach (RadComboBoxItem item in ddlLocSubBanner.Items)
                                        {
                                            item.Checked = true;
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region Location OppourtunityBanner

                        if (ddlLocHierSrcBanner != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_BANNER_ID))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (BannerIds == "0")
                                {
                                    getvalue = getdt[1];
                                    if (!String.IsNullOrEmpty(getvalue))
                                    {
                                        ddlLocHierSrcBanner.ClearCheckedItems();
                                        DataRow[] BannerDetails = dtBannerAndSubBannerDetails.Select(AppConstants.BANNER_ID_COLUMN + " IN (" + getvalue + ")");
                                        DataTable dtBanners = BannerDetails.CopyToDataTable();
                                        dtBanners = dtBanners.DefaultView.ToTable(true, AppConstants.BANNER_COLUMN);
                                        if (dtBanners != null && dtBanners.Rows.Count > 0)
                                        {
                                            SelectDefaultItem(ddlLocHierSrcBanner, dtBanners);
                                        }
                                    }
                                    else
                                    {
                                        foreach (RadComboBoxItem item in ddlLocHierSrcBanner.Items)
                                        {
                                            item.Checked = true;
                                        }
                                    }
                                    BannerIds = AppManager.GetSelectedItemsWithQuotes(ddlLocHierSrcBanner);
                                    FillSubBanner(BannerIds, ddlLocHierSrcSubBanner, ddlSrcStore, dtBannerAndSubBannerDetails);
                                }
                            }
                        }
                        if (ddlLocHierSrcSubBanner != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_SUB_BANNER_ID))
                            {
                                if (BannerIds == "0" || !String.IsNullOrEmpty(BannerIds))
                                {

                                    foreach (RadComboBoxItem item in ddlLocHierSrcBanner.Items)
                                    {
                                        item.Checked = true;
                                    }
                                    BannerIds = AppManager.GetSelectedItemsWithQuotes(ddlLocHierSrcBanner);
                                    FillSubBanner(BannerIds, ddlLocHierSrcSubBanner, ddlSrcStore, dtBannerAndSubBannerDetails);
                                }

                                if (SubBannerIds == "0")
                                {
                                    string getvalueSubbanner = "";
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    getvalueSubbanner = getdt[1];
                                    if (!String.IsNullOrEmpty(getvalueSubbanner))
                                    {
                                        DataRow[] SubBannerDetails = dtBannerAndSubBannerDetails.Select(AppConstants.SUBBANNER_ID_COLUMN + " IN (" + getvalueSubbanner + ")");
                                        DataTable dtSubBanners = SubBannerDetails.CopyToDataTable();
                                        dtSubBanners = dtSubBanners.DefaultView.ToTable(true, AppConstants.SUBBANNER_COLUMN);
                                        if (dtSubBanners != null && dtSubBanners.Rows.Count > 0)
                                        {
                                            ddlLocHierSrcSubBanner.ClearCheckedItems();
                                            SelectDefaultItem(ddlLocHierSrcSubBanner, dtSubBanners);
                                        }
                                    }
                                    else
                                    {
                                        foreach (RadComboBoxItem item in ddlLocHierSrcSubBanner.Items)
                                        {
                                            item.Checked = true;
                                        }
                                    }
                                }
                            }
                        }

                        if (ddlSrcStore != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_STORE_ID.Replace(" ", "%20")))
                            {
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = getdt[1];
                                if (!String.IsNullOrEmpty(getvalue1))
                                {
                                    DataRow[] SrcStoreDetails = dtBannerAndSubBannerDetails.Select(AppConstants.LOC_STORE_NUMBER_COLUMN + " IN (" + getvalue1 + ")");
                                    DataTable dtSrcStoreDetails = SrcStoreDetails.CopyToDataTable();
                                    dtSrcStoreDetails = dtSrcStoreDetails.DefaultView.ToTable(true, AppConstants.LOC_SOURCE_STORE_COLUMN);
                                    if (dtSrcStoreDetails != null && dtSrcStoreDetails.Rows.Count > 0)
                                    {
                                        ddlSrcStore.ClearCheckedItems();
                                        SelectDefaultItem(ddlSrcStore, dtSrcStoreDetails);
                                    }
                                }
                            }
                        }

                        if (ddlLocHierDesBanner != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_OPP_BANNER_ID))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (DesBannerIds == "0")
                                {
                                    getvalue = getdt[1];
                                    if (!String.IsNullOrEmpty(getvalue))
                                    {

                                        DataRow[] BannerDetails = dtBannerAndSubBannerDetails.Select(AppConstants.BANNER_ID_COLUMN + " IN (" + getvalue + ")");
                                        DataTable dtBanners = BannerDetails.CopyToDataTable();
                                        dtBanners = dtBanners.DefaultView.ToTable(true, AppConstants.BANNER_COLUMN);
                                        if (dtBanners != null && dtBanners.Rows.Count > 0)
                                        {
                                            SelectDefaultItem(ddlLocHierDesBanner, dtBanners);
                                        }
                                    }
                                    else
                                    {
                                        foreach (RadComboBoxItem item in ddlLocHierDesBanner.Items)
                                        {
                                            item.Checked = true;
                                        }
                                    }
                                    DesBannerIds = AppManager.GetSelectedItemsWithQuotes(ddlLocHierDesBanner);
                                    FillSubBanner(DesBannerIds, ddlLocHierDesSubBanner, ddlDesStore, dtBannerAndSubBannerDetails);
                                }
                            }
                        }
                        if (ddlLocHierDesSubBanner != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_OPP_SUB_BANNER_ID))
                            {
                                if (DesBannerIds == "0" || String.IsNullOrEmpty(DesBannerIds))
                                {
                                    foreach (RadComboBoxItem item in ddlLocHierDesBanner.Items)
                                    {
                                        item.Checked = true;
                                    }
                                    DesBannerIds = AppManager.GetSelectedItemsWithQuotes(ddlLocHierDesBanner);
                                    FillSubBanner(BannerIds, ddlLocHierDesSubBanner, ddlDesStore, dtBannerAndSubBannerDetails);
                                }

                                if (DesSubBannerIds == "0")
                                {
                                    string getvalueSubbanner = "";
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    getvalueSubbanner = getdt[1];
                                    if (!String.IsNullOrEmpty(getvalueSubbanner))
                                    {
                                        DataRow[] SubBannerDetails = dtBannerAndSubBannerDetails.Select(AppConstants.SUBBANNER_ID_COLUMN + " IN (" + getvalueSubbanner + ")");
                                        DataTable dtSubBanners = SubBannerDetails.CopyToDataTable();
                                        dtSubBanners = dtSubBanners.DefaultView.ToTable(true, AppConstants.SUBBANNER_COLUMN);
                                        if (dtSubBanners != null && dtSubBanners.Rows.Count > 0)
                                        {
                                            ddlLocHierDesSubBanner.ClearCheckedItems();
                                            SelectDefaultItem(ddlLocHierDesSubBanner, dtSubBanners);
                                        }
                                    }
                                    else
                                    {
                                        foreach (RadComboBoxItem item in ddlLocHierDesSubBanner.Items)
                                        {
                                            item.Checked = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (ddlDesStore != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_OPP_STORE_ID))
                            {
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = getdt[1];
                                if (!String.IsNullOrEmpty(getvalue1))
                                {
                                    DataRow[] DesStoreDetails = dtBannerAndSubBannerDetails.Select(AppConstants.LOC_STORE_NUMBER_COLUMN + " IN (" + getvalue1 + ")");
                                    DataTable dtDesStoreDetails = DesStoreDetails.CopyToDataTable();
                                    dtDesStoreDetails = dtDesStoreDetails.DefaultView.ToTable(true, AppConstants.LOC_DESTINATION_STORE_COLUMN);
                                    if (dtDesStoreDetails != null && dtDesStoreDetails.Rows.Count > 0)
                                    {
                                        ddlDesStore.ClearCheckedItems();
                                        SelectDefaultItem(ddlDesStore, dtDesStoreDetails);
                                    }

                                }
                            }
                        }


                        #endregion

                        #region Location Custom Hierarchy
                        if (ddlLocationStore != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_CUSTOM_STORE_ID))
                            {
                                if (customstore == "0")
                                {
                                    string getvalue1 = "";
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    getvalue1 = getdt[1];
                                    if (!String.IsNullOrEmpty(getvalue1))
                                    {

                                        DataRow[] StoreDetails = dtLocationDetailsCustomHierarchy.Select(AppConstants.CUSTOM_STORE_GROUP_ID_COLUMN + " IN (" + getvalue1 + ")");
                                        DataTable dtStore = StoreDetails.CopyToDataTable();
                                        dtStore = dtStore.DefaultView.ToTable(true, AppConstants.CUSTOM_STORE_NAME_COLUMN);
                                        if (dtStore != null && dtStore.Rows.Count > 0)
                                        {
                                            SelectDefaultItem(ddlLocationStore, dtStore);
                                        }
                                        string selectedStore = AppManager.GetSelectedItemsWithQuotes(ddlLocationStore);
                                        BindLocationCustomLevel1Details(selectedStore, ddlLocationCustom1, ddlLocationCustom2, ddlLocationCustom3, dtLocationDetailsCustomHierarchy);
                                        customstore = selectedStore;
                                    }
                                }
                            }

                        }

                        if (ddlLocationCustom1 != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_LOCCUSTOM_LEVEL1))
                            {
                                ddlLocationCustom1.ClearCheckedItems();
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);
                                string[] dtLocCustomLevel1 = getvalue1.Split(',');
                                for (int j = 0; j < dtLocCustomLevel1.Length; j++)
                                {
                                    foreach (RadComboBoxItem ltm in ddlLocationCustom1.Items)
                                    {
                                        if (ltm.Value.ToString().Replace(",", " ") == dtLocCustomLevel1[j].ToString())
                                        {
                                            ltm.Checked = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (ddlLocationCustom2 != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_LOCCUSTOM_LEVEL2))
                            {
                                ddlLocationCustom2.ClearCheckedItems();
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);
                                string[] dtLocCustomLevel2 = getvalue1.Split(',');
                                for (int j = 0; j < dtLocCustomLevel2.Length; j++)
                                {
                                    foreach (RadComboBoxItem ltm in ddlLocationCustom2.Items)
                                    {
                                        if (ltm.Value.ToString().Replace(",", " ") == dtLocCustomLevel2[j].ToString())
                                        {
                                            ltm.Checked = true;
                                        }
                                    }
                                }
                            }

                        }

                        if (ddlLocationCustom3 != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_LOCCUSTOM_LEVEL3))
                            {
                                ddlLocationCustom3.ClearCheckedItems();
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);
                                string[] dtLocCustomLevel3 = getvalue1.Split(',');
                                for (int j = 0; j < dtLocCustomLevel3.Length; j++)
                                {
                                    foreach (RadComboBoxItem ltm in ddlLocationCustom3.Items)
                                    {
                                        if (ltm.Value.ToString().Replace(",", " ") == dtLocCustomLevel3[j].ToString())
                                        {
                                            ltm.Checked = true;
                                        }
                                    }
                                }
                            }
                        }

                        #endregion
                        if (ddlProductHierarchy != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_HIERARCHY_ID))
                            {
                                ddlProductHierarchy.SelectedItem.Text = IcontrolFilterResources.CustomHierarchy;
                            }
                        }

                        #region product corporate Hieararchy

                        if (ddlDepartment != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_DEPARTMENT_ID))
                            {
                                if (DeptId == "0")
                                {
                                    if (txtVendorNumber != null)
                                    {
                                        dtProductDetailsCorporateHierarchy = dataAccessLayer.GetProductDetailsCorporateHierarchy(Convert.ToInt16(reportId), LoginUserName, LoginUserProfileName, txtVendorNumber.Text.ToString());
                                    }
                                    else
                                    {
                                        dtProductDetailsCorporateHierarchy = dataAccessLayer.GetProductDetailsCorporateHierarchy(Convert.ToInt16(reportId), LoginUserName, LoginUserProfileName);
                                    }
                                    string getvalue1 = "";
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    getvalue1 = getdt[1];
                                    if (!String.IsNullOrEmpty(getvalue1))
                                    {
                                        ddlDepartment.ClearCheckedItems();
                                        DataRow[] DepartmentDetails = dtProductDetailsCorporateHierarchy.Select(AppConstants.DEPARTMENT_ID_COLUMN + " IN (" + getvalue1 + ")");
                                        DataTable dtDepartment = DepartmentDetails.CopyToDataTable();
                                        dtDepartment = dtDepartment.DefaultView.ToTable(true, AppConstants.DEPARTMENT_COLUMN);
                                        if (dtDepartment != null && dtDepartment.Rows.Count > 0)
                                        {
                                            SelectDefaultItem(ddlDepartment, dtDepartment);
                                        }
                                        string selectedDepartments = AppManager.GetSelectedItemsWithQuotes(ddlDepartment);

                                        BindCategoryDetails(selectedDepartments, ddlCategory, ddlSubCategory, ddlSegment, ddlProductBrand, ddlProProductSizeDesc, dtProductDetailsCorporateHierarchy);
                                        DeptId = selectedDepartments;
                                    }
                                    else
                                    {
                                        foreach (RadComboBoxItem item in ddlDepartment.Items)
                                        {
                                            item.Checked = true;
                                        }
                                    }

                                }
                            }
                            else
                            {
                                if (DeptIdAll == "0")
                                {
                                    foreach (RadComboBoxItem item in ddlDepartment.Items)
                                    {
                                        item.Checked = true;
                                    }
                                    string selectedDepartments = AppManager.GetSelectedItemsWithQuotes(ddlDepartment);
                                    BindCategoryDetails(selectedDepartments, ddlCategory, ddlSubCategory, ddlSegment, ddlProductBrand, ddlProProductSizeDesc, dtProductDetailsCorporateHierarchy);
                                    DeptIdAll = selectedDepartments;
                                }
                            }
                        }

                        if (ddlCategory != null)
                        {

                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_CATEGORY_ID))
                            {
                                if (ddlCategory.CheckBoxes)
                                {

                                    if (catId == "0")
                                    {
                                        if (txtVendorNumber != null)
                                        {
                                            dtProductDetailsCorporateHierarchy = dataAccessLayer.GetProductDetailsCorporateHierarchy(Convert.ToInt16(reportId), LoginUserName, LoginUserProfileName, txtVendorNumber.Text.ToString());
                                        }
                                        else
                                        {
                                            dtProductDetailsCorporateHierarchy = dataAccessLayer.GetProductDetailsCorporateHierarchy(Convert.ToInt16(reportId), LoginUserName, LoginUserProfileName);
                                        }

                                        string getvalue1 = "";
                                        string[] getdt = RepPar[i].ToString().Split('=');
                                        getvalue1 = getdt[1];
                                        catId = getvalue1;
                                        if (!String.IsNullOrEmpty(getvalue1))
                                        {
                                            ddlCategory.ClearCheckedItems();
                                            DataRow[] CategoryDetails = dtProductDetailsCorporateHierarchy.Select(AppConstants.CATEGORY_ID_COLUMN + " IN (" + getvalue1 + ")");
                                            DataTable dtCategory = CategoryDetails.CopyToDataTable();
                                            dtCategory = dtCategory.DefaultView.ToTable(true, AppConstants.CATEGORY_COLUMN);
                                            if (dtCategory != null && dtCategory.Rows.Count > 0)
                                            {
                                                SelectDefaultItem(ddlCategory, dtCategory);
                                            }
                                        }
                                        string selectedCategory = AppManager.GetSelectedItemsWithQuotes(ddlCategory);
                                        //binding all subcategories as per category
                                        GetSubCategoryDetailsUsingCategory(null, selectedCategory, ddlSubCategory, ddlSegment, ddlProductBrand, ddlProProductSizeDesc, dtProductDetailsCorporateHierarchy);

                                    }
                                }
                                else
                                {
                                    if (catIdAll == "0")
                                    {
                                        if (txtVendorNumber != null)
                                        {
                                            dtProductDetailsCorporateHierarchy = dataAccessLayer.GetProductDetailsCorporateHierarchy(Convert.ToInt16(reportId), LoginUserName, LoginUserProfileName, txtVendorNumber.Text.ToString());
                                        }
                                        else
                                        {
                                            dtProductDetailsCorporateHierarchy = dataAccessLayer.GetProductDetailsCorporateHierarchy(Convert.ToInt16(reportId), LoginUserName, LoginUserProfileName);
                                        }
                                        dtProductDetailsCorporateHierarchy.DefaultView.Sort = AppConstants.CATEGORY_COLUMN + " ASC";
                                        using (DataTable dtcategory = dtProductDetailsCorporateHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CATEGORY_COLUMN }))
                                        {
                                            ddlCategory.DataSource = dtcategory;
                                            ddlCategory.DataValueField = AppConstants.CATEGORY_COLUMN;
                                            ddlCategory.DataTextField = AppConstants.CATEGORY_COLUMN;
                                            ddlCategory.DataBind();
                                        }


                                        string getvalue1 = "";
                                        string[] getdt = RepPar[i].ToString().Split('=');
                                        getvalue1 = getdt[1];
                                        catIdAll = getvalue1;
                                        if (!String.IsNullOrEmpty(getvalue1))
                                        {
                                            DataRow[] CategoryDetails = dtProductDetailsCorporateHierarchy.Select(AppConstants.CATEGORY_ID_COLUMN + " IN (" + getvalue1 + ")");
                                            ddlCategory.SelectedValue = CategoryDetails[0]["category"].ToString();
                                            catIdAll = ddlCategory.SelectedValue.ToString();
                                        }
                                        //binding all subcategories as per category
                                        GetSubCategoryDetailsUsingCategory(null, AppManager.GetSelectedItemsWithQuotes(ddlCategory), ddlSubCategory, ddlSegment, ddlProductBrand, ddlProProductSizeDesc, dtProductDetailsCorporateHierarchy);

                                    }
                                }
                            }
                        }
                        if (ddlSubCategory != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_SUBCATEGORY_ID))
                            {
                                if (subcat == "0")
                                {
                                    string getvalue1 = "";
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    getvalue1 = getdt[1];
                                    ddlSubCategory.ClearCheckedItems();
                                    DataRow[] SubCategoryDetails = dtProductDetailsCorporateHierarchy.Select(AppConstants.SUBCATEGORY_ID_COLUMN + " IN (" + getvalue1 + ")");
                                    DataTable dtSubCategory = SubCategoryDetails.CopyToDataTable();
                                    dtSubCategory = dtSubCategory.DefaultView.ToTable(true, AppConstants.SUBCATEGORY_COLUMN);
                                    if (dtSubCategory != null && dtSubCategory.Rows.Count > 0)
                                    {
                                        SelectDefaultItem(ddlSubCategory, dtSubCategory);
                                    }
                                    subcat = AppManager.GetSelectedItemsWithQuotes(ddlSubCategory);
                                }
                            }
                        }

                        if (ddlSegment != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_SEGMENT_ID))
                            {
                                if (segment == "0")
                                {

                                    string getvalue1 = "";
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    getvalue1 = getdt[1];
                                    ddlSegment.ClearCheckedItems();
                                    DataRow[] SegmentDetails = dtProductDetailsCorporateHierarchy.Select(AppConstants.SEGMENT_ID_COLUMN + " IN (" + getvalue1 + ")");
                                    DataTable dtSegment = SegmentDetails.CopyToDataTable();
                                    dtSegment = dtSegment.DefaultView.ToTable(true, AppConstants.SEGMENT_COLUMN);
                                    if (dtSegment != null && dtSegment.Rows.Count > 0)
                                    {
                                        SelectDefaultItem(ddlSegment, dtSegment);
                                    }
                                    segment = AppManager.GetSelectedItemsWithQuotes(ddlSegment);
                                }
                            }
                        }
                        if (ddlProductBrand != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_BRAND))
                            {
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = getdt[1];
                                ddlProductBrand.ClearCheckedItems();
                                DataRow[] BrandDetails = dtProductDetailsCorporateHierarchy.Select(AppConstants.BRAND_ID_COLUMN + " IN (" + getvalue1 + ")");
                                DataTable dtBrand = BrandDetails.CopyToDataTable();
                                dtBrand = dtBrand.DefaultView.ToTable(true, AppConstants.BRAND_COLUMN);
                                if (dtBrand != null && dtBrand.Rows.Count > 0)
                                {
                                    SelectDefaultItem(ddlProductBrand, dtBrand);
                                }
                            }
                        }
                        if (ddlProProductSizeDesc != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_PRODUCT_SIZE))
                            {
                                ddlProProductSizeDesc.ClearCheckedItems();
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);
                                string[] dtProductSize = getvalue1.Split(',');
                                for (int j = 0; j < dtProductSize.Length; j++)
                                {
                                    foreach (RadComboBoxItem ltm in ddlProProductSizeDesc.Items)
                                    {
                                        if (ltm.Value.ToString().Replace(",", " ") == dtProductSize[j].ToString())
                                        {
                                            ltm.Checked = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (ddlBrandType != null)
                        {
                            if (!string.IsNullOrEmpty(RepPar[i].ToString()))
                            {
                                if (RepPar[i].ToString().Substring(0, RepPar[i].IndexOf('=')).Contains(AppConstants.PARAM_BRAND_TYPE))
                                {
                                    string getvalue1 = "";
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    getvalue1 = getdt[1];
                                    ddlBrandType.SelectedValue = getvalue1 != "Y" ? "No" : "Yes";
                                }
                            }
                        }
                        #endregion

                        if (ddlPOG != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_POG))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                ddlPOG.SelectedValue = getdt[1];
                            }
                        }

                        if (ddlRecentTimeframe != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.RC))
                            {
                                if (RecentTimeframe == "0")
                                {
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    ddlRecentTimeframe.SelectedValue = getdt[1];
                                    RecentTimeframe = getdt[1];
                                }
                            }
                        }

                        #region Product Custom Hierarchy
                        if (ddlHierarchyName != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_HIERARCHY_ID))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                if (Hierarchy == "0")
                                {
                                    getvalue = getdt[1];
                                    if (!String.IsNullOrEmpty(getvalue))
                                    {
                                        ddlHierarchyName.SelectedValue = getvalue;
                                    }
                                    Hierarchy = getvalue;
                                    BindCustomLevel1Details(AppManager.GetSelectedItemsWithQuotes(ddlHierarchyName), ddlCustomLevel1NewItems, ddlCustomLevel1DiscontinuedItems, ddlCustomLevel1, ddlCustomLevel2, ddlCustomLevel3, ddlCustomBrand, ddlCustomProductSizeDesc, dtProductDetailsCustomHierarchy);
                                }
                            }

                        }
                        if (ddlCustomLevel1NewItems != null && ddlCustomLevel1DiscontinuedItems != null)
                        {
                            if (ddlCustomLevel1NewItems != null)
                            {
                                if (RepPar[i].ToString().Contains(AppConstants.PARAM_CUSTOM_LEVEL1_NEW_ITEM))
                                {
                                    string getvalue1 = "";
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);
                                    ddlCustomLevel1NewItems.SelectedValue = getvalue1;
                                }
                            }
                            else if (ddlCustomLevel1DiscontinuedItems != null)
                            {
                                if (RepPar[i].ToString().Contains(AppConstants.PARAM_CUSTOM_LEVEL1_DISCONTINUED_ITEM))
                                {
                                    string getvalue1 = "";
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);
                                    ddlCustomLevel1DiscontinuedItems.SelectedValue = getvalue1;
                                }
                            }
                        }
                        if (ddlCustomLevel1NewItems != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.NEW_ITEM_VALUE))
                            {
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);
                                ddlCustomLevel1NewItems.SelectedValue = getvalue1;
                            }
                        }
                        if (ddlCustomLevel1DiscontinuedItems != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.DISCO_ITEM_VALUE))
                            {
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);
                                ddlCustomLevel1DiscontinuedItems.SelectedValue = getvalue1;
                            }
                        }
                        if (ddlCustomLevel1 != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_CUSTOM_LEVEL1))
                            {
                                ddlCustomLevel1.ClearCheckedItems();
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);
                                string[] dtCustomLevel1 = getvalue1.Split(',');
                                for (int j = 0; j < dtCustomLevel1.Length; j++)
                                {
                                    foreach (RadComboBoxItem ltm in ddlCustomLevel1.Items)
                                    {
                                        if (ltm.Value.ToString().Replace(",", " ") == dtCustomLevel1[j].ToString())
                                        {
                                            ltm.Checked = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (ddlCustomLevel2 != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_CUSTOM_LEVEL2))
                            {
                                ddlCustomLevel2.ClearCheckedItems();
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);
                                string[] dtCustomLevel2 = getvalue1.Split(',');
                                for (int j = 0; j < dtCustomLevel2.Length; j++)
                                {
                                    foreach (RadComboBoxItem ltm in ddlCustomLevel2.Items)
                                    {
                                        if (ltm.Value.ToString().Replace(",", " ") == dtCustomLevel2[j].ToString())
                                        {
                                            ltm.Checked = true;
                                        }
                                    }
                                }
                            }

                        }

                        if (ddlCustomLevel3 != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_CUSTOM_LEVEL3))
                            {
                                ddlCustomLevel3.ClearCheckedItems();
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);
                                string[] dtCustomLevel3 = getvalue1.Split(',');
                                for (int j = 0; j < dtCustomLevel3.Length; j++)
                                {
                                    foreach (RadComboBoxItem ltm in ddlCustomLevel3.Items)
                                    {
                                        if (ltm.Value.ToString().Replace(",", " ") == dtCustomLevel3[j].ToString())
                                        {
                                            ltm.Checked = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (ddlCustomBrand != null)
                        {

                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_BRAND))
                            {
                                ddlCustomBrand.ClearCheckedItems();
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);
                                string[] dtBrand = getvalue1.Split(',');
                                for (int j = 0; j < dtBrand.Length; j++)
                                {
                                    foreach (RadComboBoxItem ltm in ddlCustomBrand.Items)
                                    {
                                        if (ltm.Value.ToString().Replace(",", " ") == dtBrand[j].ToString())
                                        {
                                            ltm.Checked = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (ddlCustomProductSizeDesc != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_PRODUCT_SIZE))
                            {
                                ddlCustomProductSizeDesc.ClearCheckedItems();
                                string getvalue1 = "";
                                string[] getdt = RepPar[i].ToString().Split('=');
                                getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);
                                string[] dtProductSize = getvalue1.Split(',');
                                for (int j = 0; j < dtProductSize.Length; j++)
                                {
                                    foreach (RadComboBoxItem ltm in ddlCustomProductSizeDesc.Items)
                                    {
                                        if (ltm.Value.ToString().Replace(",", " ") == dtProductSize[j].ToString())
                                        {
                                            ltm.Checked = true;
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        if (ddlViewBy != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_VIEW_BY))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                ddlViewBy.SelectedValue = getdt[1];
                            }
                        }

                        if (txtFrom != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.START_DATE))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                txtFrom.Text = Convert.ToDateTime(getdt[1]).ToString("MM/dd/yyyy");
                            }
                        }
                        if (txtTo != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.END_DATE))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                txtTo.Text = Convert.ToDateTime(getdt[1]).ToString("MM/dd/yyyy");
                            }
                        }
                        if (txtLaunchDate != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.L_DATE))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                txtLaunchDate.Text = Convert.ToDateTime(getdt[1]).ToString("MM/dd/yyyy");
                            }
                        }

                        if (txtDiscontinuedDate != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.DISCODATE))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                txtDiscontinuedDate.Text = Convert.ToDateTime(getdt[1]).ToString("MM/dd/yyyy");
                            }
                        }

                        if (ddlHoliday != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_HOLIDAY_ID))
                            {
                                if (Holiday == "0")
                                {
                                    string[] getdt = RepPar[i].ToString().Split('=');
                                    ddlHoliday.SelectedValue = getdt[1].ToString();
                                    Holiday = getdt[1].ToString();
                                    FillHolidayFiscalyear(ddlFiscalyear, ddlHoliday);
                                }
                            }
                        }
                        if (ddlFiscalyear != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_FISCAL_YEAR.Replace(" ", "%20")))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                ddlFiscalyear.SelectedValue = getdt[1].ToString();
                            }
                        }
                        if (ddlNumberofWeeksPrior != null)
                        {

                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_PRIOR_WEEK))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                ddlNumberofWeeksPrior.SelectedValue = getdt[1].ToString();
                            }
                        }

                        if (ddlNumberofWeeksPost != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_POST_WEEK))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                ddlNumberofWeeksPost.SelectedValue = getdt[1].ToString();
                            }
                        }
                        if (ddlNumberofWeeks != null)
                        {
                            if (RepPar[i].ToString().Contains(AppConstants.PARAM_NO_OFF_WEEK))
                            {
                                string[] getdt = RepPar[i].ToString().Split('=');
                                ddlNumberofWeeks.SelectedValue = getdt[1].ToString();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "PDFExport.cs > FillSavedFilters"));
            }

        }
        /// <summary>
        /// select default item
        /// </summary>
        private static void SelectDefaultItem(RadComboBox comboBox, DataTable Items)
        {
            foreach (RadComboBoxItem item in comboBox.Items)
            {
                foreach (DataRow dr in Items.Rows)
                {
                    if (item.Text == dr[0].ToString())
                    {
                        item.Checked = true;
                    }
                }
            }
        }
        /// <summary>
        /// fill sub banner
        /// </summary>
        private static void FillSubBanner(string selectedBanner, RadComboBox ddlLocSubBanner, RadComboBox ddlStore, DataTable dtBannerAndSubBannerDetails)
        {
            DataRow[] subBannerDetails = AppManager.GetNextLocationFilterItemsList_Corporate(dtBannerAndSubBannerDetails, selectedBanner);
            string selectedSubBanners = string.Empty;
            if (subBannerDetails != null && subBannerDetails.Count() > 0)
            {

                DataTable dtSubBanners = subBannerDetails.CopyToDataTable();
                dtSubBanners.DefaultView.Sort = AppConstants.SUBBANNER_COLUMN + " ASC";

                if (ddlLocSubBanner != null)
                {
                    ddlLocSubBanner.DataSource = dtSubBanners.DefaultView.ToTable(true, AppConstants.SUBBANNER_COLUMN);
                    ddlLocSubBanner.DataTextField = AppConstants.SUBBANNER_COLUMN;
                    ddlLocSubBanner.DataValueField = AppConstants.SUBBANNER_COLUMN;
                    ddlLocSubBanner.Filter = RadComboBoxFilter.StartsWith;
                    ddlLocSubBanner.AllowCustomText = true;
                    ddlLocSubBanner.CheckBoxes = true;
                    ddlLocSubBanner.EnableCheckAllItemsCheckBox = true;
                    ddlLocSubBanner.DataBind();
                    foreach (RadComboBoxItem SubBanner in ddlLocSubBanner.Items)
                    {
                        SubBanner.Checked = true;
                    }
                    selectedSubBanners = AppManager.GetSelectedItemsWithQuotes(ddlLocSubBanner);
                }
            }
            FillBannerStore(selectedBanner, selectedSubBanners, ddlStore, dtBannerAndSubBannerDetails);

        }
        /// <summary>
        /// fill banner store
        /// </summary>
        private static void FillBannerStore(string selectedBanners, string selectedLoctionDestinationSubBanners, RadComboBox ddlBannerStore, DataTable dtBannerAndSubBannerDetails)
        {
            if (dtBannerAndSubBannerDetails != null && dtBannerAndSubBannerDetails.Rows.Count > 0)
            {

                if (!string.IsNullOrEmpty(selectedLoctionDestinationSubBanners))
                {
                    DataRow[] storeDetails = AppManager.GetNextLocationFilterItemsList_Corporate(dtBannerAndSubBannerDetails, selectedBanners, selectedLoctionDestinationSubBanners);
                    if (storeDetails != null && storeDetails.Count() > 0)
                    {
                        DataTable dtDestinationStore = storeDetails.CopyToDataTable();
                        dtDestinationStore.DefaultView.Sort = AppConstants.LOC_DESTINATION_STORE_COLUMN + " ASC";

                        if (ddlBannerStore != null)
                        {
                            ddlBannerStore.DataSource = dtDestinationStore.DefaultView.ToTable(true, AppConstants.LOC_DESTINATION_STORE_COLUMN);
                            ddlBannerStore.DataTextField = AppConstants.LOC_DESTINATION_STORE_COLUMN;
                            ddlBannerStore.DataValueField = AppConstants.LOC_DESTINATION_STORE_COLUMN;
                            ddlBannerStore.DataBind();

                            if (ddlBannerStore.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemLoctionDestinationStore in ddlBannerStore.Items)
                                {
                                    itemLoctionDestinationStore.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        #region Location Details Process Controls Bindings for Custom Hierarchy
        /// <summary>
        /// bind custom level 1 details
        /// </summary>
        private static void BindLocationCustomLevel1Details(string LocationStores, RadComboBox ddlCustomHierarchyLevel1, RadComboBox ddlCustomHierarchyLevel2, RadComboBox ddlCustomHierarchyLevel3, DataTable dtLocationDetailsCustomHierarchy)
        {

            if (dtLocationDetailsCustomHierarchy != null && dtLocationDetailsCustomHierarchy.Rows.Count > 0)
            {
                string selectedCustomeLevel1 = string.Empty;
                if (!string.IsNullOrEmpty(LocationStores))
                {
                    DataRow[] CustomLevel1Details = AppManager.GetNextLocationFilterItemsList_Custom(dtLocationDetailsCustomHierarchy, LocationStores);
                    if (CustomLevel1Details != null && CustomLevel1Details.Count() > 0)
                    {
                        DataTable dtCustomLevel1 = CustomLevel1Details.CopyToDataTable();
                        dtCustomLevel1.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_1_COLUMN + " ASC";

                        if (ddlCustomHierarchyLevel1 != null)
                        {
                            ddlCustomHierarchyLevel1.DataSource = dtCustomLevel1.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_1_COLUMN);
                            ddlCustomHierarchyLevel1.DataTextField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            ddlCustomHierarchyLevel1.DataValueField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            ddlCustomHierarchyLevel1.DataBind();
                            if (ddlCustomHierarchyLevel1.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemCustomHierarchyLevel1 in ddlCustomHierarchyLevel1.Items)
                                {
                                    itemCustomHierarchyLevel1.Checked = true;
                                }
                            }
                            selectedCustomeLevel1 = AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel1);
                        }
                    }
                    GetLocationCustomLevel2DetailsUsingLocationCustomLevel1(LocationStores, selectedCustomeLevel1, ddlCustomHierarchyLevel2, ddlCustomHierarchyLevel3, dtLocationDetailsCustomHierarchy);
                }
            }

        }

        /// <summary>
        /// Gets the custom level2 details using custom level1.
        /// </summary>
        /// <param name="HierarchyId">The hierarchy identifier.</param>
        private static void GetLocationCustomLevel2DetailsUsingLocationCustomLevel1(string LocationStores, string selectedCustomeLevel1Text, RadComboBox ddlCustomHierarchyLevel2, RadComboBox ddlCustomHierarchyLevel3, DataTable dtLocationDetailsCustomHierarchy)
        {
            if (dtLocationDetailsCustomHierarchy != null && dtLocationDetailsCustomHierarchy.Rows.Count > 0)
            {
                string selectedCustomeLevel2 = string.Empty;
                if (!string.IsNullOrEmpty(selectedCustomeLevel1Text))
                {
                    DataRow[] CustomLevel2Details = AppManager.GetNextLocationFilterItemsList_Custom(dtLocationDetailsCustomHierarchy, LocationStores, selectedCustomeLevel1Text);
                    if (CustomLevel2Details != null && CustomLevel2Details.Count() > 0)
                    {
                        DataTable dtCustomLevel2 = CustomLevel2Details.CopyToDataTable();
                        dtCustomLevel2.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_2_COLUMN + " ASC";

                        if (ddlCustomHierarchyLevel2 != null)
                        {
                            ddlCustomHierarchyLevel2.DataSource = dtCustomLevel2.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_2_COLUMN);
                            ddlCustomHierarchyLevel2.DataTextField = AppConstants.CUSTOM_LEVEL_2_COLUMN;
                            ddlCustomHierarchyLevel2.DataValueField = AppConstants.CUSTOM_LEVEL_2_COLUMN;
                            ddlCustomHierarchyLevel2.DataBind();

                            if (ddlCustomHierarchyLevel2.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemCustomHierarchyLevel2 in ddlCustomHierarchyLevel2.Items)
                                {
                                    itemCustomHierarchyLevel2.Checked = true;
                                }
                            }
                            selectedCustomeLevel2 = AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel2);
                        }
                    }
                }
                GetLocationCustomLevel3DetailsUsingLocationCustomLevel2(LocationStores, selectedCustomeLevel1Text, selectedCustomeLevel2, ddlCustomHierarchyLevel3, dtLocationDetailsCustomHierarchy);
            }
        }

        /// <summary>
        /// Gets the custom level3 details using custom level2.
        /// </summary>
        /// <param name="HierarchyId">The hierarchy identifier.</param>
        private static void GetLocationCustomLevel3DetailsUsingLocationCustomLevel2(string LocationStores, string selectedCustomeLevel1Text, string selectedCustomeLevel2Text, RadComboBox ddlCustomHierarchyLevel3, DataTable dtLocationDetailsCustomHierarchy)
        {
            if (dtLocationDetailsCustomHierarchy != null && dtLocationDetailsCustomHierarchy.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(selectedCustomeLevel2Text))
                {
                    DataRow[] CustomLevel3Details = AppManager.GetNextLocationFilterItemsList_Custom(dtLocationDetailsCustomHierarchy, LocationStores, selectedCustomeLevel1Text, selectedCustomeLevel2Text);
                    if (CustomLevel3Details != null && CustomLevel3Details.Count() > 0)
                    {
                        DataTable dtCustomLevel3 = CustomLevel3Details.CopyToDataTable();
                        dtCustomLevel3.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_3_COLUMN + " ASC";

                        if (ddlCustomHierarchyLevel3 != null)
                        {
                            ddlCustomHierarchyLevel3.DataSource = dtCustomLevel3.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_3_COLUMN);
                            ddlCustomHierarchyLevel3.DataTextField = AppConstants.CUSTOM_LEVEL_3_COLUMN;
                            ddlCustomHierarchyLevel3.DataValueField = AppConstants.CUSTOM_LEVEL_3_COLUMN;
                            ddlCustomHierarchyLevel3.DataBind();

                            if (ddlCustomHierarchyLevel3.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemCustomHierarchyLevel3 in ddlCustomHierarchyLevel3.Items)
                                {
                                    itemCustomHierarchyLevel3.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
        }


        #endregion

        #region Product Details Process Controls Bindings for corporate Hierarchy

        /// <summary>
        /// Binds the category details.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <param name="departmentId">The department identifier.</param>
        private static void BindCategoryDetails(string departments, RadComboBox ddlCategory, RadComboBox ddlSubCategory, RadComboBox ddlsegment, RadComboBox ddlProductBrand, RadComboBox ddlProProductSizeDesc, DataTable dtProductDetailsCorporateHierarchy)
        {

            if (dtProductDetailsCorporateHierarchy != null && dtProductDetailsCorporateHierarchy.Rows.Count > 0)
            {
                string selectedCategory = string.Empty;
                if (!string.IsNullOrEmpty(departments))
                {
                    DataRow[] categoryDetails = null;
                    categoryDetails = AppManager.GetCategoryDetails(dtProductDetailsCorporateHierarchy, departments);
                    if (categoryDetails != null && categoryDetails.Count() > 0)
                    {
                        DataTable dtCategory = categoryDetails.CopyToDataTable();
                        dtCategory.DefaultView.Sort = AppConstants.CATEGORY_COLUMN + " ASC";
                        ddlCategory.DataSource = dtCategory.DefaultView.ToTable(true, AppConstants.CATEGORY_COLUMN);
                        ddlCategory.DataTextField = AppConstants.CATEGORY_COLUMN;
                        ddlCategory.DataValueField = AppConstants.CATEGORY_COLUMN;
                        ddlCategory.DataBind();

                        if (ddlCategory.Items.Count > 0 && ddlCategory.CheckBoxes)
                        {
                            foreach (RadComboBoxItem itemCategory in ddlCategory.Items)
                            {
                                itemCategory.Checked = true;
                            }

                        }

                        selectedCategory = AppManager.GetSelectedItemsWithQuotes(ddlCategory);
                    }

                }

                //binding all subcategories as per category
                GetSubCategoryDetailsUsingCategory(departments, selectedCategory, ddlSubCategory, ddlsegment, ddlProductBrand, ddlProProductSizeDesc, dtProductDetailsCorporateHierarchy);
            }

        }

        /// <summary>
        /// Gets the sub category details using category.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        private static void GetSubCategoryDetailsUsingCategory(string departments, string category, RadComboBox ddlSubCategory, RadComboBox ddlsegment, RadComboBox ddlProductBrand, RadComboBox ddlProProductSizeDesc, DataTable dtProductDetailsCorporateHierarchy)
        {
            if (dtProductDetailsCorporateHierarchy != null && dtProductDetailsCorporateHierarchy.Rows.Count > 0)
            {
                string selectedSubCategories = string.Empty;
                if (!string.IsNullOrEmpty(category))
                {
                    DataRow[] subCategoryDetails = AppManager.GetSubCategoryDetails(dtProductDetailsCorporateHierarchy, departments, category);
                    if (subCategoryDetails != null && subCategoryDetails.Count() > 0)
                    {
                        DataTable dtSubCategory = subCategoryDetails.CopyToDataTable();
                        dtSubCategory.DefaultView.Sort = AppConstants.SUBCATEGORY_COLUMN + " ASC";
                        if (ddlSubCategory != null)
                        {
                            ddlSubCategory.DataSource = dtSubCategory.DefaultView.ToTable(true, AppConstants.SUBCATEGORY_COLUMN);
                            ddlSubCategory.DataTextField = AppConstants.SUBCATEGORY_COLUMN;
                            ddlSubCategory.DataValueField = AppConstants.SUBCATEGORY_COLUMN;
                            ddlSubCategory.DataBind();
                            if (ddlSubCategory.Items.Count > 0 && ddlSubCategory.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemSubCategory in ddlSubCategory.Items)
                                {
                                    itemSubCategory.Checked = true;
                                }
                            }
                            selectedSubCategories = AppManager.GetSelectedItemsWithQuotes(ddlSubCategory);
                        }
                    }
                }
                GetSegmentDetailsUsingSubCategory(departments, category, selectedSubCategories, ddlsegment, ddlProductBrand, ddlProProductSizeDesc, dtProductDetailsCorporateHierarchy);
            }
        }

        /// <summary>
        /// Gets the segment details using sub category.
        /// </summary>
        /// <param name="subCategoryId">The sub category identifier.</param>
        private static void GetSegmentDetailsUsingSubCategory(string departments, string categories, string subCategories, RadComboBox ddlsegment, RadComboBox ddlProductBrand, RadComboBox ddlProProductSizeDesc, DataTable dtProductDetailsCorporateHierarchy)
        {
            if (dtProductDetailsCorporateHierarchy != null && dtProductDetailsCorporateHierarchy.Rows.Count > 0)
            {
                string selectedsegments = string.Empty;
                if (!string.IsNullOrEmpty(subCategories))
                {
                    DataRow[] segmentDetails = AppManager.GetSegmentDetails(dtProductDetailsCorporateHierarchy, departments, categories, subCategories);
                    if (segmentDetails != null && segmentDetails.Count() > 0)
                    {
                        DataTable dtSegment = segmentDetails.CopyToDataTable();
                        dtSegment.DefaultView.Sort = AppConstants.SEGMENT_COLUMN + " ASC";
                        if (ddlsegment != null)
                        {
                            ddlsegment.DataSource = dtSegment.DefaultView.ToTable(true, AppConstants.SEGMENT_COLUMN);
                            ddlsegment.DataTextField = AppConstants.SEGMENT_COLUMN;
                            ddlsegment.DataValueField = AppConstants.SEGMENT_COLUMN;
                            ddlsegment.DataBind();

                            if (ddlsegment.Items.Count > 0 && ddlsegment.CheckBoxes)
                            {
                                foreach (RadComboBoxItem segments in ddlsegment.Items)
                                {
                                    segments.Checked = true;
                                }
                            }
                            selectedsegments = AppManager.GetSelectedItemsWithQuotes(ddlsegment);
                        }
                    }
                }
                GetBrandDetailsUsingSegment(departments, categories, subCategories, selectedsegments, ddlProductBrand, ddlProProductSizeDesc, dtProductDetailsCorporateHierarchy);
            }

        }

        /// <summary>
        /// Gets the brand details using segment.
        /// </summary>
        /// <param name="segmentId">The segment identifier.</param>
        private static void GetBrandDetailsUsingSegment(string departments, string categories, string subCategories, string segments, RadComboBox ddlProductBrand, RadComboBox ddlProProductSizeDesc, DataTable dtProductDetailsCorporateHierarchy)
        {
            if (dtProductDetailsCorporateHierarchy != null && dtProductDetailsCorporateHierarchy.Rows.Count > 0)
            {
                string selectedbrand = string.Empty;
                if (!string.IsNullOrEmpty(segments))
                {
                    DataRow[] brandDetails = AppManager.GetBrandDetails(dtProductDetailsCorporateHierarchy, departments, categories, subCategories, segments);
                    if (brandDetails != null && brandDetails.Count() > 0)
                    {
                        DataTable dtBrand = brandDetails.CopyToDataTable();
                        dtBrand.DefaultView.Sort = AppConstants.BRAND_COLUMN + " ASC";
                        if (ddlProductBrand != null)
                        {
                            ddlProductBrand.DataSource = dtBrand.DefaultView.ToTable(true, AppConstants.BRAND_COLUMN);
                            ddlProductBrand.DataTextField = AppConstants.BRAND_COLUMN;
                            ddlProductBrand.DataValueField = AppConstants.BRAND_COLUMN;
                            ddlProductBrand.DataBind();
                            if (ddlProductBrand.Items.Count > 0 && ddlProductBrand.CheckBoxes)
                            {
                                foreach (RadComboBoxItem brand in ddlProductBrand.Items)
                                {
                                    brand.Checked = true;
                                }
                            }
                            selectedbrand = AppManager.GetSelectedItemsWithQuotes(ddlProductBrand);
                        }
                    }
                }
                GetProductSizeDetailsUsingBrand(departments, categories, subCategories, segments, selectedbrand, ddlProProductSizeDesc, dtProductDetailsCorporateHierarchy);
            }
        }

        /// <summary>
        /// Gets the product size details using brand.
        /// </summary>
        /// <param name="BrandId">The brand identifier.</param>
        private static void GetProductSizeDetailsUsingBrand(string departments, string categories, string subCategories, string segments, string brands, RadComboBox ddlProProductSizeDesc, DataTable dtProductDetailsCorporateHierarchy)
        {
            if (dtProductDetailsCorporateHierarchy != null && dtProductDetailsCorporateHierarchy.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(brands))
                {
                    DataRow[] ProductSizeDetails = AppManager.GetProductSizeDescDetails(dtProductDetailsCorporateHierarchy, departments, categories, subCategories, segments, brands);
                    if (ProductSizeDetails != null && ProductSizeDetails.Count() > 0)
                    {
                        DataTable dtProductSize = ProductSizeDetails.CopyToDataTable();
                        dtProductSize.DefaultView.Sort = AppConstants.PRODUCT_SIZE_COLUMN + " ASC";
                        if (ddlProProductSizeDesc != null)
                        {
                            ddlProProductSizeDesc.DataSource = dtProductSize.DefaultView.ToTable(true, AppConstants.PRODUCT_SIZE_COLUMN);
                            ddlProProductSizeDesc.DataTextField = AppConstants.PRODUCT_SIZE_COLUMN;
                            ddlProProductSizeDesc.DataValueField = AppConstants.PRODUCT_SIZE_COLUMN;
                            ddlProProductSizeDesc.DataBind();
                            if (ddlProProductSizeDesc.Items.Count > 0 && ddlProProductSizeDesc.CheckBoxes)
                            {
                                foreach (RadComboBoxItem ProductSize in ddlProProductSizeDesc.Items)
                                {
                                    ProductSize.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Product Details Process Controls Bindings for Custom Hierarchy
        private static void BindCustomLevel1Details(string hierarchyItem, RadComboBox ddlCustomHierarchyLevel1_NewItem, RadComboBox ddlCustomHierarchyLevel1_DiscontinuedItem, RadComboBox ddlCustomHierarchyLevel1, RadComboBox ddlCustomHierarchyLevel2, RadComboBox ddlCustomHierarchyLevel3, RadComboBox ddlBrand, RadComboBox ddlProductSize, DataTable dtProductDetailsCustomHierarchy)
        {
            if (dtProductDetailsCustomHierarchy != null && dtProductDetailsCustomHierarchy.Rows.Count > 0)
            {
                string selectedCustomeLevel1Text = string.Empty;
                if (!string.IsNullOrEmpty(hierarchyItem))
                {
                    DataRow[] customLevel1Details = dtProductDetailsCustomHierarchy.Select(AppConstants.CUSTOM_HIERARCHY_COLUMN + "=" + hierarchyItem);
                    DataTable dtCustomLevel1 = customLevel1Details.CopyToDataTable();
                    if (customLevel1Details != null && customLevel1Details.Count() > 0)
                    {

                        dtCustomLevel1.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_1_COLUMN + " ASC";
                        if (ddlCustomHierarchyLevel1_NewItem != null)
                        {
                            ddlCustomHierarchyLevel1_NewItem.DataSource = dtCustomLevel1.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_1_COLUMN);
                            ddlCustomHierarchyLevel1_NewItem.DataTextField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            ddlCustomHierarchyLevel1_NewItem.DataValueField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            ddlCustomHierarchyLevel1_NewItem.DataBind();
                            ddlCustomHierarchyLevel1_NewItem.Items.Insert(0, new RadComboBoxItem { Text = IcontrolFilterResources.ddl_Select, Value = IcontrolFilterResources.ddl_Select });
                            if (ddlCustomHierarchyLevel1_NewItem.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemCustomHierarchyLevel1New in ddlCustomHierarchyLevel1_NewItem.Items)
                                {
                                    itemCustomHierarchyLevel1New.Checked = true;
                                }
                            }
                            else
                            {
                                ddlCustomHierarchyLevel1_NewItem.SelectedIndex = 1;
                            }

                            selectedCustomeLevel1Text = AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel1_NewItem);

                        }
                        if (ddlCustomHierarchyLevel1_DiscontinuedItem != null)
                        {
                            ddlCustomHierarchyLevel1_DiscontinuedItem.DataSource = dtCustomLevel1.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_1_COLUMN);
                            ddlCustomHierarchyLevel1_DiscontinuedItem.DataTextField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            ddlCustomHierarchyLevel1_DiscontinuedItem.DataValueField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            ddlCustomHierarchyLevel1_DiscontinuedItem.DataBind();
                            ddlCustomHierarchyLevel1_DiscontinuedItem.Items.Insert(0, new RadComboBoxItem { Text = IcontrolFilterResources.ddl_Select, Value = IcontrolFilterResources.ddl_Select });
                            if (ddlCustomHierarchyLevel1_DiscontinuedItem.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemCustomHierarchyLevel1dis in ddlCustomHierarchyLevel1_DiscontinuedItem.Items)
                                {
                                    itemCustomHierarchyLevel1dis.Checked = true;
                                }
                            }
                            else
                            {
                                ddlCustomHierarchyLevel1_DiscontinuedItem.SelectedIndex = 1;
                            }

                            selectedCustomeLevel1Text = AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel1_DiscontinuedItem);



                        }
                        if (ddlCustomHierarchyLevel1 != null)
                        {
                            ddlCustomHierarchyLevel1.DataSource = dtCustomLevel1.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_1_COLUMN);
                            ddlCustomHierarchyLevel1.DataTextField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            ddlCustomHierarchyLevel1.DataValueField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            ddlCustomHierarchyLevel1.DataBind();

                            if (ddlCustomHierarchyLevel1.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemCustomHierarchyLevel1 in ddlCustomHierarchyLevel1.Items)
                                {
                                    itemCustomHierarchyLevel1.Checked = true;
                                }
                            }
                            else
                            {
                                ddlCustomHierarchyLevel1.SelectedIndex = 1;
                            }
                            selectedCustomeLevel1Text = AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel1);
                        }
                    }
                }
                GetCustomLevel2DetailsUsingCustomLevel1(hierarchyItem, selectedCustomeLevel1Text, ddlCustomHierarchyLevel2, ddlCustomHierarchyLevel3, ddlBrand, ddlProductSize, dtProductDetailsCustomHierarchy);
            }
        }

        /// <summary>
        /// Gets the custom level2 details using custom level1.
        /// </summary>
        /// <param name="HierarchyId">The hierarchy identifier.</param>
        private static void GetCustomLevel2DetailsUsingCustomLevel1(string hierarchyItem, string selectedCustomeLevel1Text, RadComboBox ddlCustomHierarchyLevel2, RadComboBox ddlCustomHierarchyLevel3, RadComboBox ddlBrand, RadComboBox ddlProductSize, DataTable dtProductDetailsCustomHierarchy)
        {
            string selectedCustomeLevel2Text = string.Empty;
            if (!string.IsNullOrEmpty(selectedCustomeLevel1Text))
            {
                DataRow[] customLevel2Details = AppManager.GetNextProductFilterItemsList_Custom(dtProductDetailsCustomHierarchy, hierarchyItem, selectedCustomeLevel1Text);

                if (customLevel2Details != null && customLevel2Details.Count() > 0)
                {
                    DataTable dtCustomLevel2 = customLevel2Details.CopyToDataTable();
                    dtCustomLevel2.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_2_COLUMN + " ASC";
                    if (ddlCustomHierarchyLevel2 != null)
                    {
                        ddlCustomHierarchyLevel2.DataSource = dtCustomLevel2.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_2_COLUMN);
                        ddlCustomHierarchyLevel2.DataTextField = AppConstants.CUSTOM_LEVEL_2_COLUMN;
                        ddlCustomHierarchyLevel2.DataValueField = AppConstants.CUSTOM_LEVEL_2_COLUMN;
                        ddlCustomHierarchyLevel2.DataBind();

                        if (ddlCustomHierarchyLevel2.CheckBoxes)
                        {
                            foreach (RadComboBoxItem itemCustomHierarchyLevel2 in ddlCustomHierarchyLevel2.Items)
                            {
                                itemCustomHierarchyLevel2.Checked = true;
                            }

                        }
                        selectedCustomeLevel2Text = AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel2, true);

                    }

                }
            }
            GetCustomLevel3DetailsUsingCustomLevel2(hierarchyItem, selectedCustomeLevel1Text, selectedCustomeLevel2Text, ddlCustomHierarchyLevel3, ddlBrand, ddlProductSize, dtProductDetailsCustomHierarchy);
        }

        /// <summary>
        /// Gets the custom level3 details using custom level2.
        /// </summary>
        /// <param name="HierarchyId">The hierarchy identifier.</param>
        private static void GetCustomLevel3DetailsUsingCustomLevel2(string hierarchyItem, string selectedCustomeLevel1Text, string customLevel2Text, RadComboBox ddlCustomHierarchyLevel3, RadComboBox ddlBrand, RadComboBox ddlProductSize, DataTable dtProductDetailsCustomHierarchy)
        {
            string customLevel3Text = string.Empty;
            if (!string.IsNullOrEmpty(customLevel2Text))
            {
                DataRow[] CustomLevel3Details = AppManager.GetNextProductFilterItemsList_Custom(dtProductDetailsCustomHierarchy, hierarchyItem, selectedCustomeLevel1Text, customLevel2Text);

                if (CustomLevel3Details != null && CustomLevel3Details.Count() > 0)
                {
                    DataTable dtCustomLevel3 = CustomLevel3Details.CopyToDataTable();
                    dtCustomLevel3.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_3_COLUMN + " ASC";

                    if (ddlCustomHierarchyLevel3 != null)
                    {
                        ddlCustomHierarchyLevel3.DataSource = dtCustomLevel3.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_3_COLUMN); ;
                        ddlCustomHierarchyLevel3.DataTextField = AppConstants.CUSTOM_LEVEL_3_COLUMN;
                        ddlCustomHierarchyLevel3.DataValueField = AppConstants.CUSTOM_LEVEL_3_COLUMN;
                        ddlCustomHierarchyLevel3.DataBind();

                        if (ddlCustomHierarchyLevel3.CheckBoxes)
                        {
                            foreach (RadComboBoxItem itemCustomHierarchyLevel3 in ddlCustomHierarchyLevel3.Items)
                            {
                                itemCustomHierarchyLevel3.Checked = true;
                            }
                        }

                        customLevel3Text = AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel3);

                    }
                }
            }
            GetBrandDetailsUsingCustomLevel3(hierarchyItem, selectedCustomeLevel1Text, customLevel2Text, customLevel3Text, ddlBrand, ddlProductSize, dtProductDetailsCustomHierarchy);
        }

        /// <summary>
        /// Gets the brand details using segment.
        /// </summary>
        /// <param name="segmentId">The segment identifier.</param>
        private static void GetBrandDetailsUsingCustomLevel3(string hierarchyItem, string selectedCustomeLevel1Text, string customLevel2Text, string customLevel3Text, RadComboBox ddlBrand, RadComboBox ddlProductSize, DataTable dtProductDetailsCustomHierarchy)
        {
            string selectedBrandText = string.Empty;
            if (!string.IsNullOrEmpty(customLevel3Text))
            {
                DataRow[] brandDetails = AppManager.GetNextProductFilterItemsList_Custom(dtProductDetailsCustomHierarchy, hierarchyItem, selectedCustomeLevel1Text, customLevel2Text, customLevel3Text);
                if (brandDetails != null && brandDetails.Count() > 0)
                {
                    DataTable dtBrand = brandDetails.CopyToDataTable();
                    dtBrand.DefaultView.Sort = AppConstants.CUSTOM_BRAND_COLUMN + " ASC";

                    if (ddlBrand != null)
                    {
                        ddlBrand.DataSource = dtBrand.DefaultView.ToTable(true, AppConstants.CUSTOM_BRAND_COLUMN);
                        ddlBrand.DataTextField = AppConstants.CUSTOM_BRAND_COLUMN;
                        ddlBrand.DataValueField = AppConstants.CUSTOM_BRAND_COLUMN;
                        ddlBrand.DataBind();

                        if (ddlBrand.CheckBoxes)
                        {
                            foreach (RadComboBoxItem brand in ddlBrand.Items)
                            {
                                brand.Checked = true;
                            }

                        }
                        selectedBrandText = AppManager.GetSelectedItemsWithQuotes(ddlBrand);

                    }

                }
            }
            GetProductSizeDetailsUsingCustomBrand(hierarchyItem, selectedCustomeLevel1Text, customLevel2Text, customLevel3Text, selectedBrandText, ddlProductSize, dtProductDetailsCustomHierarchy);
        }

        /// <summary>
        /// Gets the product size details using brand.
        /// </summary>
        /// <param name="BrandId">The brand identifier.</param>
        private static void GetProductSizeDetailsUsingCustomBrand(string hierarchyItem, string selectedCustomeLevel1Text, string customLevel2Text, string customLevel3Text, string selectedBrandText, RadComboBox ddlProductSize, DataTable dtProductDetailsCustomHierarchy)
        {
            if (!string.IsNullOrEmpty(selectedBrandText))
            {
                DataRow[] productSizeDetails = AppManager.GetNextProductFilterItemsList_Custom(dtProductDetailsCustomHierarchy, hierarchyItem, selectedCustomeLevel1Text, customLevel2Text, customLevel3Text, selectedBrandText);

                if (productSizeDetails != null && productSizeDetails.Count() > 0)
                {
                    DataTable dtProductSize = productSizeDetails.CopyToDataTable();
                    dtProductSize.DefaultView.Sort = AppConstants.CUSTOM_PRODUCT_SIZE_COLUMN + " ASC";
                    if (ddlProductSize != null)
                    {
                        ddlProductSize.DataSource = dtProductSize.DefaultView.ToTable(true, AppConstants.CUSTOM_PRODUCT_SIZE_COLUMN);
                        ddlProductSize.DataTextField = AppConstants.CUSTOM_PRODUCT_SIZE_COLUMN;
                        ddlProductSize.DataValueField = AppConstants.CUSTOM_PRODUCT_SIZE_COLUMN;
                        ddlProductSize.DataBind();

                        if (ddlProductSize != null && ddlProductSize.CheckBoxes)
                        {
                            foreach (RadComboBoxItem ProductSize in ddlProductSize.Items)
                            {
                                ProductSize.Checked = true;
                            }
                        }
                    }
                }
            }
        }
        #endregion
        /// <summary>
        /// fill holiday fiscal year
        /// </summary>
        private static void FillHolidayFiscalyear(RadComboBox ddlFiscalyear, RadComboBox ddlHoliday)
        {
            DataLayer dataAccessLayer = new DataLayer();
            DataTable dtHolidayAndYearDetails = null;
            dtHolidayAndYearDetails = dataAccessLayer.GetHolidayAndYearDetails();
            DataRow[] YearDetails = dtHolidayAndYearDetails.Select(AppConstants.HOLIDAY_ID + "=" + Convert.ToInt16(ddlHoliday.SelectedValue));
            DataTable dtYearDetails = YearDetails.CopyToDataTable();
            dtYearDetails.DefaultView.Sort = AppConstants.HOLIDAY_YEAR_COLUMN + " ASC";
            dtYearDetails = dtYearDetails.DefaultView.ToTable(true, new string[] { AppConstants.HOLIDAY_YEAR_COLUMN, AppConstants.HOLIDAY_ID });
            if (ddlFiscalyear != null && dtYearDetails != null && dtYearDetails.Rows.Count > 0)
            {
                ddlFiscalyear.DataSource = dtYearDetails;
                ddlFiscalyear.DataTextField = AppConstants.HOLIDAY_YEAR_COLUMN;
                ddlFiscalyear.DataValueField = AppConstants.HOLIDAY_ID;
                ddlFiscalyear.DataBind();
                ddlFiscalyear.Items.Insert(0, new RadComboBoxItem
                {
                    Text = IcontrolFilterResources.ddl_Select,
                    Value = IcontrolFilterResources.ddl_Select
                });
            }
        }
    }
}