﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using iControlDataLayer;
using iControlDataLayer.Helper;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using ICSharpCode.SharpZipLib.Checksums;
using ICSharpCode.SharpZipLib.Zip;
using System.Configuration;
using System.Threading;
using System.Collections;
using iControlGenricFilters.ResourceFiles;
using System.Data.OleDb;

namespace iControlGenricFilters.LIB
{
    public class AppManager
    {
        public static bool _AlcoholLogin;

        public static bool AlcoholLogin
        {
            get { return _AlcoholLogin; }
            set { _AlcoholLogin = value; }
        }
        /// <summary>
        /// Get Application Url
        /// </summary>
        private static string ApplicationUrl
        {
            get
            {
                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["AppUrl"]))
                {
                    return Convert.ToString(ConfigurationManager.AppSettings["AppUrl"]);
                }
                else
                {
                    //this is just in case we forget too add in webconfig
                    return "http://icontrolusa.com:8200";
                }

            }
        }

        #region Products DropDown Fill

        /// <summary>
        /// Get Category details by Departments
        /// </summary>
        /// <param name="dtProductDetails"></param>
        /// <param name="departments"></param>
        /// <returns></returns>
        public static DataRow[] GetCategoryDetails(DataTable dtProductDetails, string departments)
        {
            DataRow[] categoryDetails = null;

            string strQuery = string.Empty;
            strQuery = GetNextProductFilterItemsList_Corporate(departments);

            categoryDetails = !string.IsNullOrEmpty(strQuery) ? dtProductDetails.Select(strQuery).Distinct().ToArray() : dtProductDetails.Select().Distinct().ToArray();

            return categoryDetails;
        }

        /// <summary>
        /// Get Sub Category details by department and category
        /// </summary>
        /// <param name="dtProductDetails"></param>
        /// <param name="departments"></param>
        /// <param name="categories"></param>
        /// <returns></returns>
        public static DataRow[] GetSubCategoryDetails(DataTable dtProductDetails, string departments, string categories)
        {
            DataRow[] subCategoryDetails = null;

            string strQuery = string.Empty;
            strQuery = GetNextProductFilterItemsList_Corporate(departments, categories);

            subCategoryDetails = !string.IsNullOrEmpty(strQuery) ? dtProductDetails.Select(strQuery).Distinct().ToArray() : dtProductDetails.Select().Distinct().ToArray();

            return subCategoryDetails;
        }

        /// <summary>
        /// Get Segment details by department,Category and sub Category
        /// </summary>
        /// <param name="dtProductDetails"></param>
        /// <param name="departments"></param>
        /// <param name="categories"></param>
        /// <param name="subCategories"></param>
        /// <returns></returns>
        public static DataRow[] GetSegmentDetails(DataTable dtProductDetails, string departments, string categories, string subCategories)
        {
            DataRow[] segmentDetails = null;

            string strQuery = string.Empty;
            strQuery = GetNextProductFilterItemsList_Corporate(departments, categories, subCategories);

            segmentDetails = !string.IsNullOrEmpty(strQuery) ? dtProductDetails.Select(strQuery).Distinct().ToArray() : dtProductDetails.Select().Distinct().ToArray();

            return segmentDetails;
        }

        /// <summary>
        /// Get Brand details by department,category,sub category and segment
        /// </summary>
        /// <param name="dtProductDetails"></param>
        /// <param name="departments"></param>
        /// <param name="categories"></param>
        /// <param name="subCategories"></param>
        /// <param name="segments"></param>
        /// <returns></returns>
        public static DataRow[] GetBrandDetails(DataTable dtProductDetails, string departments, string categories, string subCategories, string segments)
        {
            DataRow[] brandDetails = null;

            string strQuery = string.Empty;
            strQuery = GetNextProductFilterItemsList_Corporate(departments, categories, subCategories, segments);

            brandDetails = !string.IsNullOrEmpty(strQuery) ? dtProductDetails.Select(strQuery).Distinct().ToArray() : dtProductDetails.Select().Distinct().ToArray();

            return brandDetails;
        }

        /// <summary>
        /// Get Product size details by department,category,subcategory,segement and brand
        /// </summary>
        /// <param name="dtProductDetails"></param>
        /// <param name="departments"></param>
        /// <param name="categories"></param>
        /// <param name="subCategories"></param>
        /// <param name="segments"></param>
        /// <param name="brands"></param>
        /// <returns></returns>
        public static DataRow[] GetProductSizeDescDetails(DataTable dtProductDetails, string departments, string categories, string subCategories, string segments, string brands)
        {
            DataRow[] brandDetails = null;

            string strQuery = string.Empty;
            strQuery = GetNextProductFilterItemsList_Corporate(departments, categories, subCategories, segments, brands);
            brandDetails = !string.IsNullOrEmpty(strQuery) ? dtProductDetails.Select(strQuery).Distinct().ToArray() : dtProductDetails.Select().Distinct().ToArray();

            return brandDetails;
        }

        /// <summary>
        /// Fill next filter product corporate hierarchy for category,sub category,segment,brand and product size
        /// </summary>
        /// <param name="departments"></param>
        /// <param name="categories"></param>
        /// <param name="subCategories"></param>
        /// <param name="segments"></param>
        /// <param name="brands"></param>
        /// <returns></returns>
        private static string GetNextProductFilterItemsList_Corporate(string departments = "", string categories = "", string subCategories = "", string segments = "", string brands = "")
        {
            string strQuery = string.Empty;

            if (!string.IsNullOrEmpty(departments) && departments != "-1")
            {
                strQuery += AppConstants.DEPARTMENT_COLUMN + " IN (" + departments + ")";
            }

            if (!string.IsNullOrEmpty(departments) && !string.IsNullOrEmpty(categories) && categories != "-1")
            {
                strQuery += AppendQuery(strQuery) + AppConstants.CATEGORY_COLUMN + " IN (" + categories + ")";
            }
            else if (string.IsNullOrEmpty(departments) && !string.IsNullOrEmpty(categories) && categories != "-1")
            {
                strQuery += AppendQuery(strQuery) + AppConstants.CATEGORY_COLUMN + " IN (" + categories + ")";
            }

            if (!string.IsNullOrEmpty(subCategories) && subCategories != "-1")
            {
                strQuery += AppendQuery(strQuery) + AppConstants.SUBCATEGORY_COLUMN + " IN (" + subCategories + ")";
            }

            if (!string.IsNullOrEmpty(segments) && segments != "-1")
            {
                strQuery += AppendQuery(strQuery) + AppConstants.SEGMENT_COLUMN + " IN (" + segments + ")";
            }

            if (!string.IsNullOrEmpty(brands) && brands != "-1")
            {
                strQuery += AppendQuery(strQuery) + AppConstants.BRAND_COLUMN + " IN (" + brands + ")";
            }
            return strQuery;
        }

        /// <summary>
        /// Method for append query
        /// </summary>
        /// <param name="strQuery"></param>
        /// <returns></returns>
        private static string AppendQuery(string strQuery)
        {
            return !string.IsNullOrEmpty(strQuery) ? " AND " : strQuery;
        }

        /// <summary>
        /// Fill next filter product custom hieararchy
        /// </summary>
        /// <param name="dtProductCustomDetails"></param>
        /// <param name="hierarchyItem"></param>
        /// <param name="selectedCustomeLevel1Text"></param>
        /// <param name="customLevel2Text"></param>
        /// <param name="customLevel3Text"></param>
        /// <param name="selectedBrandText"></param>
        /// <returns></returns>
        public static DataRow[] GetNextProductFilterItemsList_Custom(DataTable dtProductCustomDetails, string hierarchyItem = "", string selectedCustomeLevel1Text = "", string customLevel2Text = "", string customLevel3Text = "", string selectedBrandText = "")
        {
            string strQuery = string.Empty;
            DataRow[] filterItemsList = null;

            if (!string.IsNullOrEmpty(hierarchyItem) && hierarchyItem != "-1")
            {
                strQuery += AppConstants.CUSTOM_HIERARCHY_COLUMN + " IN (" + hierarchyItem + ")";
            }

            if (!string.IsNullOrEmpty(selectedCustomeLevel1Text) && selectedCustomeLevel1Text != "-1")
            {
                strQuery += AppendQuery(strQuery) + AppConstants.CUSTOM_LEVEL_1_COLUMN + " IN (" + selectedCustomeLevel1Text + ")";
            }
            if (!string.IsNullOrEmpty(customLevel2Text) && customLevel2Text != "-1")
            {
                strQuery += AppendQuery(strQuery) + AppConstants.CUSTOM_LEVEL_2_COLUMN + " IN (" + customLevel2Text + ")";
            }
            if (!string.IsNullOrEmpty(customLevel3Text) && customLevel3Text != "-1")
            {
                strQuery += AppendQuery(strQuery) + AppConstants.CUSTOM_LEVEL_3_COLUMN + " IN (" + customLevel3Text + ")";
            }
            if (!string.IsNullOrEmpty(selectedBrandText) && selectedBrandText != "-1")
            {
                strQuery += AppendQuery(strQuery) + AppConstants.CUSTOM_BRAND_COLUMN + " IN (" + selectedBrandText + ")";
            }

            filterItemsList = !string.IsNullOrEmpty(strQuery) ? dtProductCustomDetails.Select(strQuery).Distinct().ToArray() : dtProductCustomDetails.Select().Distinct().ToArray();
            return filterItemsList;
        }
        #endregion

        #region Location Dropdown Fill

        /// <summary>
        /// Fill next filter for product corporate 
        /// </summary>
        /// <param name="dtLocationCorporateDetails"></param>
        /// <param name="banners"></param>
        /// <param name="subBanners"></param>
        /// <param name="store"></param>
        /// <returns></returns>
        public static DataRow[] GetNextLocationFilterItemsList_Corporate(DataTable dtLocationCorporateDetails, string banners = "", string subBanners = "", string store = "")
        {
            string strQuery = string.Empty;

            DataRow[] filterItemsList = null;
            if (!string.IsNullOrEmpty(banners) && banners != "-1")
            {
                strQuery += AppConstants.BANNER_COLUMN + " IN (" + banners + ")";
            }

            if (!string.IsNullOrEmpty(subBanners) && subBanners != "-1")
            {
                strQuery += AppendQuery(strQuery) + AppConstants.SUBBANNER_COLUMN + " IN (" + subBanners + ")";
            }
            if (!string.IsNullOrEmpty(store) && store != "-1")
            {
                strQuery += AppendQuery(strQuery) + AppConstants.LOC_SOURCE_STORE_COLUMN + " IN (" + store + ")";
            }

            filterItemsList = !string.IsNullOrEmpty(strQuery) ? dtLocationCorporateDetails.Select(strQuery).Distinct().ToArray() : dtLocationCorporateDetails.Select().Distinct().ToArray();
            return filterItemsList;
        }

        /// <summary>
        /// Fill next filter for location custom
        /// </summary>
        /// <param name="dtLocationCustomDetails"></param>
        /// <param name="CustomStore"></param>
        /// <param name="selectedCustomeLevel1Text"></param>
        /// <param name="customLevel2Text"></param>
        /// <returns></returns>
        public static DataRow[] GetNextLocationFilterItemsList_Custom(DataTable dtLocationCustomDetails, string CustomStore = "", string selectedCustomeLevel1Text = "", string customLevel2Text = "")
        {
            string strQuery = string.Empty;
            DataRow[] filterItemsList = null;

            if (!string.IsNullOrEmpty(CustomStore) && CustomStore != "-1")
            {
                strQuery += AppConstants.CUSTOM_STORE_NAME_COLUMN + " IN (" + CustomStore + ")";
            }

            if (!string.IsNullOrEmpty(selectedCustomeLevel1Text) && selectedCustomeLevel1Text != "-1")
            {
                strQuery += AppendQuery(strQuery) + AppConstants.CUSTOM_LEVEL_1_COLUMN + " IN (" + selectedCustomeLevel1Text + ")";
            }
            if (!string.IsNullOrEmpty(customLevel2Text) && customLevel2Text != "-1")
            {
                strQuery += AppendQuery(strQuery) + AppConstants.CUSTOM_LEVEL_2_COLUMN + " IN (" + customLevel2Text + ")";
            }
            filterItemsList = !string.IsNullOrEmpty(strQuery) ? dtLocationCustomDetails.Select(strQuery).Distinct().ToArray() : dtLocationCustomDetails.Select().Distinct().ToArray();
            return filterItemsList;
        }

        #endregion

        /// <summary>
        /// Get selected items with quotes
        /// </summary>
        /// <param name="ddlCombo"></param>
        /// <param name="IsText"></param>
        /// <returns></returns>
        public static string GetSelectedItemsWithQuotes(RadComboBox ddlCombo, bool IsText = true)
        {
            IControlHelper helper = new IControlHelper();

            string strSelected = "";
            try
            {
                if (ddlCombo != null && ddlCombo.CheckBoxes)
                {
                    if (ddlCombo.CheckedItems.Count == ddlCombo.Items.Count)
                    {
                        strSelected = "-1";
                    }
                    else
                    {

                        foreach (RadComboBoxItem ltm in ddlCombo.Items)
                        {
                            if (ltm.Checked && !String.IsNullOrEmpty(ltm.Value.ToString()))
                            {
                                if (IsText == false)
                                    strSelected += "'" + ltm.Value.ToString().Replace("'", "''") + "'" + ",";
                                else
                                    strSelected += "'" + ltm.Text.ToString().Replace("'", "''") + "'" + ",";
                            }
                        }
                        strSelected = strSelected.TrimEnd(',');
                    }
                }
                else
                {

                    strSelected = "'" + ddlCombo.SelectedItem.Text + "'";
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "GetSelectedItemsWithQuotes: "));
            }
            return strSelected;
        }

        /// <summary>
        /// Get selected Items without quotes
        /// </summary>
        /// <param name="ddlCombo"></param>
        /// <param name="IsText"></param>
        /// <returns></returns>
        public static string GetSelectedItemsWithOutQuotes(RadComboBox ddlCombo, bool IsText = true)
        {
            IControlHelper helper = new IControlHelper();

            string strSelected = "";
            try
            {
                if (ddlCombo != null && ddlCombo.CheckBoxes)
                {

                    foreach (RadComboBoxItem ltm in ddlCombo.Items)
                    {
                        if (ltm.Checked && !String.IsNullOrEmpty(ltm.Value.ToString()))
                        {
                            if (IsText == false)
                                strSelected += ltm.Value.ToString() + ",";
                            else
                                strSelected += ltm.Text.ToString() + ",";
                        }
                    }
                    strSelected = strSelected.TrimEnd(',');
                }
                else
                {

                    strSelected = ddlCombo.SelectedItem.Text;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > GetSelectedItemsWithOutQuotes: "));
            }
            return strSelected;
        }

        /// <summary>
        /// Get contol values by text
        /// </summary>
        /// <param name="ddlCombo">Rad combo box</param>
        /// <param name="dataSource"></param>
        /// <param name="toPass">Which is used to pass value</param>
        /// <param name="toGet">Which is get value</param>
        /// <returns></returns>
        public static string GetControlValuesByText(RadComboBox ddlCombo, DataTable dataSource, string toPass, string toGet)
        {
            IControlHelper helper = new IControlHelper();
            string strSelected = string.Empty;
            var itemList = new List<dynamic>();
            try
            {
                string[] selectedTexts;
                if (ddlCombo.CheckBoxes == true)
                {
                    selectedTexts = ddlCombo
                                            .Items.Where(i => i.Checked)
                                            .Select(i => i.Text)
                                            .ToArray();
                }
                else
                {
                    selectedTexts = ddlCombo
                                          .Items.Where(i => i.Selected)
                                          .Select(i => i.Text)
                                          .ToArray();
                }
                foreach (var item in selectedTexts)
                {

                    List<dynamic> result = (from myRow in dataSource.AsEnumerable()
                                            where myRow.Field<dynamic>(toPass) == item.ToString()
                                            select myRow.Field<dynamic>(toGet)).Distinct().ToList();
                    itemList.AddRange(result.ToArray());
                }
                strSelected = string.Join(",", itemList);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, " AppManager.cs > GetControlValuesByText: "));
            }
            return strSelected;
        }

        #region SalesDriverBasket

        /// <summary>
        /// Method for create dynamic sql for sales driver basket Report
        /// </summary>
        /// <param name="UserType"></param>
        /// <param name="ProfileName"></param>
        /// <param name="userName"></param>
        /// <param name="SelectedFilters"></param>
        /// <returns></returns>
        public static string CreateDynamicSql_ReportSalesDriverBasket(string UserType, string ProfileName, string userName, Dictionary<string, dynamic> SelectedFilters)
        {
            dynamic VendorNumber = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_VENDOR_NUMBER, out VendorNumber);

            dynamic BannerIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_BANNER_ID, out BannerIds);

            dynamic SubBannerIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_SUB_BANNER_ID, out SubBannerIds);

            dynamic CatIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_CATEGORY_ID, out CatIds);

            dynamic SubCatIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_SUBCATEGORY_ID, out SubCatIds);

            dynamic SegmentIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_SUBCATEGORY_ID, out SegmentIds);

            dynamic BrandIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_BRAND, out BrandIds);

            dynamic ProductSize = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_PRODUCT_SIZE, out ProductSize);

            dynamic BrandType = "";//Product Lable
            SelectedFilters.TryGetValue(AppConstants.PARAM_BRAND_TYPE, out BrandType);

            dynamic Hierarchy = "";
            SelectedFilters.TryGetValue(AppConstants.CUSTOM_HIERARCHY_NAME, out Hierarchy);

            dynamic CustomLevel1 = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_CUSTOM_LEVEL1, out CustomLevel1);

            dynamic CustomLevel2 = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_CUSTOM_LEVEL2, out CustomLevel2);

            dynamic CustomLevel3 = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_CUSTOM_LEVEL3, out CustomLevel3);

            dynamic StartDate = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_START_DATE, out StartDate);

            dynamic EndDate = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_END_DATE, out EndDate);

            dynamic SalesBy = "";
            //  SelectedFilters.TryGetValue(AppConstants.END_DATE, out EndDate);


            string Exec_Identifier = "";
            Exec_Identifier = InsertDynamicSql_ReportSalesDriverBasket(UserType, ProfileName, userName, SalesBy, StartDate, EndDate, BannerIds, SubBannerIds, CatIds, SubCatIds, SegmentIds, BrandIds, ProductSize, BrandType, Hierarchy, CustomLevel1, CustomLevel2, CustomLevel3, VendorNumber);
            return Exec_Identifier;
        }

        /// <summary>
        /// Method for inserting data for sales driver basket report
        /// </summary>
        /// <param name="UserType"></param>
        /// <param name="ProfileName"></param>
        /// <param name="UserName"></param>
        /// <param name="SalesBy"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="BannerIds"></param>
        /// <param name="SubBannerIds"></param>
        /// <param name="CatIds"></param>
        /// <param name="SubCatIds"></param>
        /// <param name="SegmentIds"></param>
        /// <param name="BrandIds"></param>
        /// <param name="ProductSize"></param>
        /// <param name="BrandType"></param>
        /// <param name="Hierarchy"></param>
        /// <param name="CustomLevel1"></param>
        /// <param name="CustomLevel2"></param>
        /// <param name="CustomLevel3"></param>
        /// <param name="VendorNumber"></param>
        /// <returns></returns>
        public static string InsertDynamicSql_ReportSalesDriverBasket(string UserType, string ProfileName, string UserName, string SalesBy, string StartDate, string EndDate, string BannerIds, string SubBannerIds, string CatIds, string SubCatIds, string SegmentIds, string BrandIds, string ProductSize, string BrandType, string Hierarchy, string CustomLevel1, string CustomLevel2, string CustomLevel3, string VendorNumber)
        {
            IControlHelper helper = new IControlHelper();
            DataLayer dataLayer = new DataLayer();
            string Exec_Identifier = Guid.NewGuid().ToString("N");
            string SpExec = "";
            string qry = "";
            int IsBurst = 0;
            //if (chkBurst.Checked == true)
            //    IsBurst = 1;
            //else
            //    IsBurst = 0;
            try
            {
                SpExec += " '" + Exec_Identifier + "',";
                SpExec += " '" + StartDate + "',";
                SpExec += " '" + EndDate + "',";

                if (BannerIds != null)
                    qry += " '" + BannerIds + "',";
                else
                    qry += " NULL,";

                if (SubBannerIds != null)
                    qry += " '" + SubBannerIds + "',";
                else
                    qry += " NULL,";

                if (CatIds != null)
                    qry += " '" + CatIds + "',";
                else
                    qry += " NULL,";

                if (SubCatIds != null)
                    qry += " '" + SubCatIds + "',";
                else
                    qry += " NULL,";

                if (SegmentIds != null)
                    qry += " '" + SegmentIds + "',";
                else
                    qry += " NULL,";

                if (BrandIds != null)
                    qry += " '" + BrandIds + "',";
                else
                    qry += " NULL,";

                if (ProductSize != null)
                    qry += " '" + ProductSize + "',";
                else
                    qry += " NULL,";

                if (BrandType != null)
                    qry += " '" + BrandType + "',";
                else
                    qry += " NULL,";

                qry += " NULL,";//for upc list 

                if (Hierarchy != null)
                    qry += " '" + Hierarchy.Replace("'", "''") + "',";
                else
                    qry += " NULL,";

                if (CustomLevel1 != null)
                    qry += " '" + CustomLevel1 + "',";
                else
                    qry += " NULL,";

                if (CustomLevel2 != null)
                    qry += " '" + CustomLevel2 + "',";
                else
                    qry += " NULL,";

                if (CustomLevel3 != null)
                    qry += " '" + CustomLevel3 + "',";
                else
                    qry += " NULL,";


                if (UserType != null)
                    qry += " '" + UserType + "',";
                else
                    qry += " NULL,";

                if (VendorNumber != null)
                    qry += " '" + VendorNumber + "',";
                else
                    qry += " NULL,";

                if (UserName != null)
                    qry += " '" + UserName + "',";
                else
                    qry += " NULL,";

                if (!String.IsNullOrEmpty(ProfileName))
                    qry += " '" + ProfileName + "',";
                else
                    qry += " NULL,";

                if (!String.IsNullOrEmpty(SalesBy))
                    qry += " " + SalesBy + ",";
                else
                    qry += " NULL,";

                qry += IsBurst;

                SpExec += qry;

                dataLayer.InsertDynamicSql_ReportSalesDriverBasket(SpExec);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs >GenerateSpCallForBatch:"));
            }
            return Exec_Identifier;
        }
        #endregion

        #region DistributionGapSalesOopportunity

        /// <summary>
        /// Method for create dynamic sql for distribution gap sales opportunity Report
        /// </summary>
        /// <param name="ReportId"></param>
        /// <param name="UserType"></param>
        /// <param name="ProfileName"></param>
        /// <param name="userName"></param>
        /// <param name="SelectedFilters"></param>
        /// <returns></returns>
        public static string CreateDynamicSql_ReportDistributionGapSalesOopportunity(string ReportId, string UserType, string ProfileName, string userName, Dictionary<string, dynamic> SelectedFilters)
        {

            dynamic VendorNumber = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_VENDOR_NUMBER, out VendorNumber);

            dynamic BannerIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_BANNER_ID, out BannerIds);

            dynamic SubBannerIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_SUB_BANNER_ID, out SubBannerIds);

            dynamic DeptIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_DEPARTMENT_ID, out DeptIds);

            dynamic CatIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_CATEGORY_ID, out CatIds);

            dynamic SubCatIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_SUBCATEGORY_ID, out SubCatIds);

            dynamic SegmentIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_SUBCATEGORY_ID, out SegmentIds);

            dynamic BrandIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_BRAND, out BrandIds);

            dynamic ProductSize = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_PRODUCT_SIZE, out ProductSize);

            dynamic BrandType = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_BRAND_TYPE, out BrandType);

            dynamic StoreIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_STORE_ID, out StoreIds);

            dynamic OppSubBannerIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_OPP_SUB_BANNER_ID, out OppSubBannerIds);

            dynamic OppBannerIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_OPP_BANNER_ID, out OppBannerIds);

            dynamic OppStoreIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_OPP_STORE_ID, out OppStoreIds);

            dynamic PogFlag = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_POG_FLAG, out PogFlag);


            dynamic HeaderDepartments = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_HEADER_DEPARTMENT, out HeaderDepartments);

            dynamic HeaderCategories = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_HEADER_CATEGORY, out HeaderCategories);

            dynamic HeaderOppBanners = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_HEADER_BANNERS, out HeaderOppBanners);


            dynamic HeaderOppSubBanners = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_HEADER_SUB_BANNERS, out HeaderOppSubBanners);

            dynamic StartDate = "";
            SelectedFilters.TryGetValue(AppConstants.START_DATE, out StartDate);

            dynamic EndDate = "";
            SelectedFilters.TryGetValue(AppConstants.END_DATE, out EndDate);

            string HeaderParamforDeptAndCat = "";
            string HeaderParamforBannerAndSubBanner = "";
            string VendHeaderParam = "";
            HeaderParamforDeptAndCat = "Department: " + HeaderDepartments + "; Category: " + HeaderCategories;
            HeaderParamforBannerAndSubBanner = "Banner: " + HeaderOppBanners + "; Sub-Banner: " + HeaderOppSubBanners;
            string DateParam = "From: " + FromDate(StartDate).ToString("MM/dd/yyyy") + " To " + FromDate(EndDate).ToString("MM/dd/yyyy");
            if (VendorNumber != "")
                VendHeaderParam = "Vendor: " + VendorNumber + ", " + DataManager.GetVendorNameByVendorNo(VendorNumber);


            string Exec_Identifier = InsertDynamicSql_ReportDistributionGapSalesOopportunity(ReportId, UserType, ProfileName, BannerIds, SubBannerIds, StoreIds, OppBannerIds,
                OppSubBannerIds, OppStoreIds, DeptIds, CatIds, SubCatIds, SegmentIds, BrandIds, ProductSize, BrandType, PogFlag,
                VendorNumber, userName, HeaderParamforDeptAndCat, HeaderParamforBannerAndSubBanner, VendHeaderParam, DateParam, StartDate, EndDate);
            return Exec_Identifier;
        }


        /// <summary>
        /// Method for inserting data for distribution gap sales opportunity report
        /// </summary>
        /// <param name="ReportId"></param>
        /// <param name="UserType"></param>
        /// <param name="ProfileName"></param>
        /// <param name="BannerIds"></param>
        /// <param name="SubBannerIds"></param>
        /// <param name="StoreIds"></param>
        /// <param name="OppBannerIds"></param>
        /// <param name="OppSubBannerIds"></param>
        /// <param name="OppStoreIds"></param>
        /// <param name="DeptIds"></param>
        /// <param name="CatIds"></param>
        /// <param name="SubCatIds"></param>
        /// <param name="SegmentIds"></param>
        /// <param name="BrandIds"></param>
        /// <param name="ProductSize"></param>
        /// <param name="BrandType"></param>
        /// <param name="PogFlag"></param>
        /// <param name="txtVendorNumber"></param>
        /// <param name="userName"></param>
        /// <param name="HeaderParamforDeptAndCat"></param>
        /// <param name="HeaderParamforBannerAndSubBanner"></param>
        /// <param name="VendHeaderParam"></param>
        /// <param name="DateParam"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public static string InsertDynamicSql_ReportDistributionGapSalesOopportunity(string ReportId, string UserType, string ProfileName, string BannerIds, string SubBannerIds, string StoreIds,
            string OppBannerIds, string OppSubBannerIds, string OppStoreIds, string DeptIds, string CatIds, string SubCatIds, string SegmentIds, string BrandIds,
            string ProductSize, string BrandType, string PogFlag, string txtVendorNumber, string userName, string HeaderParamforDeptAndCat, string HeaderParamforBannerAndSubBanner, string VendHeaderParam, string DateParam, string StartDate, string EndDate)
        {
            IControlHelper helper = new IControlHelper();
            DataLayer dataLayer = new DataLayer();
            string Exec_Identifier = "";
            try
            {
                Exec_Identifier = Guid.NewGuid().ToString("N");
                DataTable dt1 = dataLayer.GetMenuName(ReportId);
                if (dt1.Rows.Count > 0)
                    InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.REPORTNAME, dt1.Rows[0]["MenuName"].ToString());

                if (DataManager.IsCorporateUser(userName))
                {
                    if (UserType == "CORP_NO_VND")
                    {
                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_1, HeaderParamforBannerAndSubBanner, "N");
                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_2, HeaderParamforDeptAndCat, "N");
                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_3, DateParam);
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(txtVendorNumber))
                            InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_1, VendHeaderParam, "N");

                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_2, HeaderParamforBannerAndSubBanner, "N");
                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_3, HeaderParamforDeptAndCat, "N");
                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_4, DateParam);
                    }
                }
                else
                {
                    InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_2, HeaderParamforBannerAndSubBanner, "N");
                    InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_3, HeaderParamforDeptAndCat, "N");
                    InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_4, DateParam);

                    if (UserType == "VND")
                    {
                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_1, VendHeaderParam, "N");
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(ProfileName))
                        {
                            InsertReportParameters_Ids(Exec_Identifier, (int)ReportParameters.PROFILENAME, ProfileName);
                            InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_1, "Profile: " + ProfileName);
                        }
                    }
                }
                InsertReportParameters_Ids(Exec_Identifier, (int)ReportParameters.USER_TYPE, UserType);
                InsertReportParameters_Ids(Exec_Identifier, (int)ReportParameters.USERNAME, userName);
                InsertReportParameters_Ids(Exec_Identifier, (int)DateRange.START_DATE, FromDate(StartDate).ToString("yyyy-MM-dd"));
                InsertReportParameters_Ids(Exec_Identifier, (int)DateRange.END_DATE, FromDate(EndDate).ToString("yyyy-MM-dd"));

                if (!String.IsNullOrEmpty(txtVendorNumber) && txtVendorNumber != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ReportParameters.VENDOR_NUMBER, txtVendorNumber.Trim());

                if (!String.IsNullOrEmpty(BannerIds) && BannerIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)LocationHierarchy.SOURCE_TOP_BANNER_ID, BannerIds);

                if (!String.IsNullOrEmpty(SubBannerIds) && SubBannerIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)LocationHierarchy.SOURCE_BANNER_ID, SubBannerIds);

                if (!String.IsNullOrEmpty(StoreIds) && StoreIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)LocationHierarchy.SOURCE_STORE_ID, StoreIds);

                if (!String.IsNullOrEmpty(OppBannerIds) && OppBannerIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)LocationHierarchy.DEST_TOP_BANNER_ID, OppBannerIds);

                if (!String.IsNullOrEmpty(OppSubBannerIds) && OppSubBannerIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)LocationHierarchy.DEST_BANNER_ID, OppSubBannerIds);

                if (!String.IsNullOrEmpty(OppStoreIds) && OppStoreIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)LocationHierarchy.DEST_STORE_ID, OppStoreIds);

                if (!String.IsNullOrEmpty(DeptIds) && DeptIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.DEPARTMENT_ID, DeptIds);

                if (!String.IsNullOrEmpty(CatIds) && CatIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.CATEGORY_ID, CatIds);

                if (!String.IsNullOrEmpty(SubCatIds) && SubCatIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.SUB_CATEGORY_ID, SubCatIds);

                if (!String.IsNullOrEmpty(SegmentIds) && SegmentIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.SEGMENT_ID, SegmentIds);

                if (!String.IsNullOrEmpty(BrandIds) && BrandIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.BRAND_ID, BrandIds);

                if (!String.IsNullOrEmpty(ProductSize) && ProductSize != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.PRODUCT_SIZE, ProductSize);

                if (!String.IsNullOrEmpty(BrandType) && BrandType != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.PRIVATE_LABEL, BrandType);

                InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.POG_FLAG, PogFlag);

                dataLayer.InsertParametersDistributionGapAnalysis(Exec_Identifier);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs >InsertDynamicSql_ReportDistributionGapSalesOopportunity:"));
            }
            return Exec_Identifier;
        }
        #endregion

        /// <summary>
        /// Method for Insert report parameters breaking comma seprated ids
        /// </summary>
        /// <param name="Exec_id"></param>
        /// <param name="Parameter_Id"></param>
        /// <param name="Filters"></param>
        /// <param name="SplitReq"></param>
        public static void InsertReportParameters_Ids(string Exec_id, int Parameter_Id, string Filters, string SplitReq = "Y")
        {
            IControlHelper helper = new IControlHelper();
            try
            {

                DataLayer dataLayer = new DataLayer();
                if (SplitReq == "Y")
                {
                    string[] AllFilters = Filters.Split(',');
                    if (AllFilters.Length > 0)
                    {
                        int i = 0;
                        for (i = 0; i < AllFilters.Length; i++)
                        {
                            dataLayer.InsertReportParameters_Call(Exec_id, Parameter_Id, AllFilters[i].ToString());
                        }
                    }
                    else
                    {
                        dataLayer.InsertReportParameters_Call(Exec_id, Parameter_Id, Filters.ToString());
                    }
                }
                else
                {
                    dataLayer.InsertReportParameters_Call(Exec_id, Parameter_Id, Filters.ToString());
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs >InsertReportParameters_Ids:"));
            }
        }

        #region ProductPurchase Multiple

        /// <summary>
        /// Method for create dynamic sql for Product Purchase Multiple Report
        /// </summary>
        /// <param name="ReportId"></param>
        /// <param name="UserType"></param>
        /// <param name="ProfileName"></param>
        /// <param name="userName"></param>
        /// <param name="SelectedFilters"></param>
        /// <returns></returns>
        public static string CreateDynamicSql_ReportProductPurchaseMultiple(string ReportId, string UserType, string ProfileName, string userName, Dictionary<string, dynamic> SelectedFilters)
        {

            dynamic VendorNumber = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_VENDOR_NUMBER, out VendorNumber);

            dynamic BannerIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_BANNER_ID, out BannerIds);

            dynamic SubBannerIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_SUB_BANNER_ID, out SubBannerIds);

            dynamic CatIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_CATEGORY_ID, out CatIds);

            dynamic SubCatIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_SUBCATEGORY_ID, out SubCatIds);

            dynamic SegmentIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_SUBCATEGORY_ID, out SegmentIds);

            dynamic BrandIds = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_BRAND, out BrandIds);

            dynamic ProductSize = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_PRODUCT_SIZE, out ProductSize);

            dynamic BrandType = "";//Product Lable
            SelectedFilters.TryGetValue(AppConstants.PARAM_BRAND_TYPE, out BrandType);

            dynamic Hierarchy = "";
            SelectedFilters.TryGetValue(AppConstants.CUSTOM_HIERARCHY_NAME, out Hierarchy);

            dynamic CustomLevel1 = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_CUSTOM_LEVEL1, out CustomLevel1);

            dynamic CustomLevel2 = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_CUSTOM_LEVEL2, out CustomLevel2);

            dynamic CustomLevel3 = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_CUSTOM_LEVEL3, out CustomLevel3);


            dynamic HeaderCategories = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_HEADER_CATEGORY, out HeaderCategories);

            dynamic HeaderBanners = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_HEADER_BANNERS, out HeaderBanners);

            dynamic HeaderSubBanners = "";
            SelectedFilters.TryGetValue(AppConstants.PARAM_HEADER_SUB_BANNERS, out HeaderSubBanners);

            dynamic StartDate = "";
            SelectedFilters.TryGetValue(AppConstants.START_DATE, out StartDate);

            dynamic EndDate = "";
            SelectedFilters.TryGetValue(AppConstants.END_DATE, out EndDate);

            string HeaderParamforDept_Cat_Hierarchy = "";
            string HeaderParamforBannerAndSubBanner = "";
            string VendHeaderParam = "";
            if (!String.IsNullOrEmpty(HeaderCategories))
            {
                HeaderParamforDept_Cat_Hierarchy = "Category: " + HeaderCategories;
            }
            else
            {
                HeaderParamforDept_Cat_Hierarchy = "Custom Hierarchy: " + Hierarchy;
            }
            HeaderParamforBannerAndSubBanner = "Banner: " + HeaderBanners + "; Sub-Banner: " + HeaderSubBanners;
            string DateParam = "From: " + FromDate(StartDate).ToString("MM/dd/yyyy") + " To " + FromDate(EndDate).ToString("MM/dd/yyyy");
            if (VendorNumber != "")
                VendHeaderParam = "Vendor: " + VendorNumber + ", " + DataManager.GetVendorNameByVendorNo(VendorNumber);


            string Exec_Identifier = InsertDynamicSql_ReportProductPurchaseMultiple(ReportId, UserType, ProfileName, BannerIds, SubBannerIds, CatIds, SubCatIds, SegmentIds, BrandIds, ProductSize, Hierarchy, CustomLevel1, CustomLevel2, CustomLevel3,
                VendorNumber, userName, HeaderParamforDept_Cat_Hierarchy, HeaderParamforBannerAndSubBanner, VendHeaderParam, DateParam, StartDate, EndDate);
            return Exec_Identifier;
        }

        /// <summary>
        ///  Method for inserting data for Product Purchase Multiple report
        /// </summary>
        /// <param name="ReportId"></param>
        /// <param name="UserType"></param>
        /// <param name="ProfileName"></param>
        /// <param name="BannerIds"></param>
        /// <param name="SubBannerIds"></param>
        /// <param name="CatIds"></param>
        /// <param name="SubCatIds"></param>
        /// <param name="SegmentIds"></param>
        /// <param name="BrandIds"></param>
        /// <param name="ProductSize"></param>
        /// <param name="Hierarchy"></param>
        /// <param name="CustomLevel1"></param>
        /// <param name="CustomLevel2"></param>
        /// <param name="CustomLevel3"></param>
        /// <param name="txtVendorNumber"></param>
        /// <param name="userName"></param>
        /// <param name="HeaderParamforDept_Cat_Hierarchy"></param>
        /// <param name="HeaderParamforBannerAndSubBanner"></param>
        /// <param name="VendHeaderParam"></param>
        /// <param name="DateParam"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public static string InsertDynamicSql_ReportProductPurchaseMultiple(string ReportId, string UserType, string ProfileName, string BannerIds, string SubBannerIds, string CatIds, string SubCatIds, string SegmentIds, string BrandIds,
            string ProductSize, string Hierarchy, string CustomLevel1, string CustomLevel2, string CustomLevel3, string txtVendorNumber, string userName, string HeaderParamforDept_Cat_Hierarchy, string HeaderParamforBannerAndSubBanner, string VendHeaderParam, string DateParam, string StartDate, string EndDate)
        {
            IControlHelper helper = new IControlHelper();
            DataLayer dataLayer = new DataLayer();
            string Exec_Identifier = "";
            try
            {
                Exec_Identifier = Guid.NewGuid().ToString("N");
                DataTable dt1 = dataLayer.GetMenuName(ReportId);
                if (dt1.Rows.Count > 0)
                    InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.REPORTNAME, dt1.Rows[0]["MenuName"].ToString());

                if (DataManager.IsCorporateUser(userName))
                {
                    if (UserType == "CORP_NO_VND")
                    {
                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_1, HeaderParamforBannerAndSubBanner, "N");
                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_2, HeaderParamforDept_Cat_Hierarchy, "N");
                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_3, DateParam);
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(txtVendorNumber))
                            InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_1, VendHeaderParam, "N");

                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_2, HeaderParamforBannerAndSubBanner, "N");
                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_3, HeaderParamforDept_Cat_Hierarchy, "N");
                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_4, DateParam);
                    }
                }
                else
                {
                    InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_2, HeaderParamforBannerAndSubBanner, "N");
                    InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_3, HeaderParamforDept_Cat_Hierarchy, "N");
                    InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_4, DateParam);

                    if (UserType == "VND")
                    {
                        InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_1, VendHeaderParam, "N");
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(ProfileName))
                        {
                            InsertReportParameters_Ids(Exec_Identifier, (int)ReportParameters.PROFILENAME, ProfileName);
                            InsertReportParameters_Ids(Exec_Identifier, (int)HeaderTitle.HEADER_TITLE_1, "Profile: " + ProfileName);
                        }
                    }
                }

                InsertReportParameters_Ids(Exec_Identifier, (int)ReportParameters.USER_TYPE, UserType);
                InsertReportParameters_Ids(Exec_Identifier, (int)ReportParameters.USERNAME, userName);
                InsertReportParameters_Ids(Exec_Identifier, (int)DateRange.START_DATE, FromDate(StartDate).ToString("yyyy-MM-dd"));
                InsertReportParameters_Ids(Exec_Identifier, (int)DateRange.END_DATE, FromDate(EndDate).ToString("yyyy-MM-dd"));

                if (!String.IsNullOrEmpty(txtVendorNumber) && txtVendorNumber != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ReportParameters.VENDOR_NUMBER, txtVendorNumber.Trim());

                if (!String.IsNullOrEmpty(BannerIds) && BannerIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)LocationHierarchy.SOURCE_TOP_BANNER_ID, BannerIds);

                if (!String.IsNullOrEmpty(SubBannerIds) && SubBannerIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)LocationHierarchy.SOURCE_BANNER_ID, SubBannerIds);

                if (!String.IsNullOrEmpty(CatIds) && CatIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.CATEGORY_ID, CatIds);

                if (!String.IsNullOrEmpty(SubCatIds) && SubCatIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.SUB_CATEGORY_ID, SubCatIds);

                if (!String.IsNullOrEmpty(SegmentIds) && SegmentIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.SEGMENT_ID, SegmentIds);

                if (!String.IsNullOrEmpty(BrandIds) && BrandIds != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.BRAND_ID, BrandIds);

                if (!String.IsNullOrEmpty(ProductSize) && ProductSize != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.PRODUCT_SIZE, ProductSize);

                if (!String.IsNullOrEmpty(Hierarchy) && Hierarchy != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.PRODUCT_HIERARCHY_NAME, Hierarchy);

                if (!String.IsNullOrEmpty(CustomLevel1) && CustomLevel1 != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.PRODUCT_LEVEL_1, CustomLevel1);

                if (!String.IsNullOrEmpty(CustomLevel2) && CustomLevel2 != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.PRODUCT_LEVEL_2, CustomLevel2);

                if (!String.IsNullOrEmpty(CustomLevel3) && CustomLevel3 != null)
                    InsertReportParameters_Ids(Exec_Identifier, (int)ProductHierarchy.PRODUCT_LEVEL_3, CustomLevel3);

                dataLayer.InsertParametersProductPurchaseMultiple(Exec_Identifier);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > InsertDynamicSql_ReportProductPurchaseMultiple: "));
            }
            return Exec_Identifier;
        }
        #endregion


        /// <summary>
        /// Enum for Header title
        /// </summary>
        enum HeaderTitle : int
        {
            REPORTNAME = 10001,
            HEADER_TITLE_1 = 10002,
            HEADER_TITLE_2 = 10003,
            HEADER_TITLE_3 = 10004,
            HEADER_TITLE_4 = 10005,
            HEADER_TITLE_5 = 10006
        };

        /// <summary>
        /// Enum for Report parameter
        /// </summary>
        enum ReportParameters : int
        {
            USER_TYPE = 22,
            VENDOR_NUMBER = 23,
            USERNAME = 24,
            PROFILENAME = 25,
            ID_SALES = 32,
            BURST_FLAG = 33,
            UPC = 31

        };

        /// <summary>
        /// Enum for Date Range
        /// </summary>
        enum DateRange : int
        {
            START_DATE = 1,
            END_DATE = 2,
        };

        /// <summary>
        /// Enum for Location Hierarchy
        /// </summary>
        enum LocationHierarchy : int
        {
            TOP_BANNER_ID = 3, // Banner
            BANNER_ID = 4,  // Sub Banner
            SOURCE_TOP_BANNER_ID = 5,
            SOURCE_BANNER_ID = 6,
            SOURCE_STORE_ID = 7,
            DEST_TOP_BANNER_ID = 8,
            DEST_BANNER_ID = 9,
            DEST_STORE_ID = 10,
            CUSTOMSTORE_GROUP = 27,
            CUSTOMSTORE_LEVEL1 = 28,
            CUSTOMSTORE_LEVEL2 = 29,
            CUSTOMSTORE_LEVEL3 = 30,
        };

        /// <summary>
        /// Enum for Product Hierarchy
        /// </summary>
        enum ProductHierarchy : int
        {
            DEPARTMENT_ID = 11,
            CATEGORY_ID = 12,
            SUB_CATEGORY_ID = 13,
            SEGMENT_ID = 14,
            BRAND_ID = 15,
            PRODUCT_SIZE = 16,
            PRIVATE_LABEL = 17,

            PRODUCT_HIERARCHY_NAME = 18,
            PRODUCT_LEVEL_1 = 19,
            PRODUCT_LEVEL_2 = 20,
            PRODUCT_LEVEL_3 = 21,
            POG_FLAG = 26,
        };

        /// <summary>
        /// from date in datetime format
        /// </summary>
        /// <param name="FromDate"></param>
        /// <returns></returns>
        public static DateTime FromDate(string FromDate)
        {
            return Convert.ToDateTime(FromDate.Trim());
        }

        /// <summary>
        /// return selected value from  ddlFromAdWeek
        /// </summary>
        /// <param name="ddlFromAdWeek"></param>
        /// <returns></returns>
        public static string FromAdWeekValue(RadComboBox ddlFromAdWeek)
        {
            return ddlFromAdWeek.SelectedValue.ToString();
        }

        /// <summary>
        /// return selected value from  ddlToAdWeek
        /// </summary>
        /// <param name="ddlToAdWeek"></param>
        /// <returns></returns>
        public static string ToAdWeekValue(RadComboBox ddlToAdWeek)
        {
            return ddlToAdWeek.SelectedValue.ToString();
        }

        /// <summary>
        /// return selected value from  ddlFromFiscalWeek
        /// </summary>
        /// <param name="ddlFromFiscalWeek"></param>
        /// <returns></returns>
        public static string FromFiscalWeekValue(RadComboBox ddlFromFiscalWeek)
        {
            return ddlFromFiscalWeek.SelectedValue.ToString();
        }

        /// <summary>
        /// return selected value from  ddlToFiscalWeek
        /// </summary>
        /// <param name="ddlToFiscalWeek"></param>
        /// <returns></returns>
        public static string ToFiscalWeekValue(RadComboBox ddlToFiscalWeek)
        {
            return ddlToFiscalWeek.SelectedValue.ToString();
        }

        /// <summary>
        /// Return the selected item text with UnderScore
        /// </summary>
        /// <param name="ddlCombo"></param>
        /// <returns></returns>
        public static string GetSelectedItemsWithUnderScore(RadComboBox ddlCombo)
        {
            string strSelected = "";
            IControlHelper helper = new IControlHelper();
            try
            {
                foreach (RadComboBoxItem ltm in ddlCombo.Items)
                {
                    if (ltm.Checked && !String.IsNullOrEmpty(ltm.Value.ToString()))
                    {
                        strSelected += ltm.Value.ToString().Replace("'", "''") + "_";
                    }
                }
                strSelected = strSelected.TrimEnd('_');
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > GetSelectedItemsWithUnderScore"));
            }
            return strSelected;
        }

        /// <summary>
        /// Return the selected item with special character 
        /// </summary>
        /// <param name="ddlCombo"></param>
        /// <returns></returns>
        public static string GetSelectedItemsWithSpecialChar(RadComboBox ddlCombo)
        {
            string strSelected = "";
            IControlHelper helper = new IControlHelper();
            try
            {
                foreach (RadComboBoxItem ltm in ddlCombo.Items)
                {
                    if (ltm.Checked && !String.IsNullOrEmpty(ltm.Value.ToString()))
                    {
                        strSelected += ltm.Value.ToString().Replace("'", "''") + "^@~";
                    }
                }

                strSelected = strSelected.Length > 3 ? strSelected.Remove(strSelected.Length - 3) : strSelected;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs => GetSelectedItemsWithSpecialChar"));

            }
            return strSelected;
        }

        /// <summary>
        /// Get selected items text with quotes
        /// </summary>
        /// <param name="ddlCombo"></param>
        /// <returns></returns>
        public static string GetSelectedItems(RadComboBox ddlCombo)
        {
            string strSelected = "";
            IControlHelper helper = new IControlHelper();
            try
            {
                foreach (RadComboBoxItem ltm in ddlCombo.Items)
                {
                    if (ltm.Checked && !String.IsNullOrEmpty(ltm.Text.ToString()))
                    {
                        strSelected += ltm.Text.ToString() + ",";
                    }
                }
                strSelected = strSelected.TrimEnd(',');
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs => GetSelectedItems"));
            }
            return strSelected;
        }

        /// <summary>
        /// Get sub banner using sub banner text
        /// </summary>
        /// <param name="ddlSubBanners"></param>
        /// <returns></returns>
        public static string SubBanner(RadComboBox ddlSubBanners)
        {
            var CheckedItems = ddlSubBanners.CheckedItems;
            if (CheckedItems.Count == ddlSubBanners.Items.Count)
            {
                return "empty";
            }
            else
            {
                return GetSubBannerIds(ddlSubBanners);
            }
        }

        /// <summary>
        /// Get sub banner ids using sub banner text
        /// </summary>
        /// <param name="ddlSubBanners"></param>
        /// <returns></returns>
        public static string GetSubBannerIds(RadComboBox ddlSubBanners)
        {
            string SubBannerIds = "";
            IControlHelper helper = new IControlHelper();
            try
            {
                string SubBanner = GetSelectedItemsWithQuotes(ddlSubBanners);
                if (!String.IsNullOrEmpty(SubBanner))
                {
                    string sql = "select distinct Banner_Id from Dim_Stores where Banner in (" + SubBanner + ")";
                    SubBannerIds = GetCommaSeparatedIds(sql);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > GetSubBannerIds"));

            }
            return SubBannerIds;
        }

        /// <summary>
        /// Get comma seprated Ids 
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static string GetCommaSeparatedIds(string sql)
        {
            string Ids = "";
            IControlHelper helper = new IControlHelper();
            try
            {

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {

                    using (DataTable dt = dataAccessProvider.Select(sql))
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (!String.IsNullOrEmpty(dt.Rows[i][0].ToString()))
                                Ids += dt.Rows[i][0].ToString() + ",";
                        }
                        if (!String.IsNullOrEmpty(Ids))
                            Ids = Ids.TrimEnd(',');
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > GetCommaSeparatedIds"));

            }
            return Ids;
        }

        /// <summary>
        /// Get sub category ids by sub category combo text
        /// </summary>
        /// <param name="ddlSubCategories"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public static string SubCategory(RadComboBox ddlSubCategories, string TableName = "Dim_Products")
        {
            var CheckedItems = ddlSubCategories.CheckedItems;
            if (CheckedItems.Count == ddlSubCategories.Items.Count)
            {
                return "empty";
            }
            else
            {
                return GetSubCategoryIds(ddlSubCategories, TableName);
            }
        }

        /// <summary>
        /// Get segment ids by segment combo text
        /// </summary>
        /// <param name="ddlSegments"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public static string Segment(RadComboBox ddlSegments, string TableName = "Dim_Products")
        {
            var CheckedItems = ddlSegments.CheckedItems;
            if (CheckedItems.Count == ddlSegments.Items.Count)
            {
                return "empty";
            }
            else
            {
                return GetSegmentIds(ddlSegments, TableName);
            }
        }

        /// <summary>
        /// Get selected brand with encoding special character
        /// </summary>
        /// <param name="ddlBrands"></param>
        /// <returns></returns>
        public static string Brand(RadComboBox ddlBrands)
        {
            DataLayer DAL = new DataLayer();
            string strSelectedBrands = "";

            var CheckedItems = ddlBrands.CheckedItems;
            if (CheckedItems.Count == ddlBrands.Items.Count)
            {
                strSelectedBrands = "empty";
            }
            else
            {
                strSelectedBrands = GetSelectedItems(ddlBrands);
            }

            return DAL.EncodeSpecialCharacters(strSelectedBrands, true);
        }

        /// <summary>
        /// Get sub category ids by sub category text
        /// </summary>
        /// <param name="ddlSubCategories"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public static string GetSubCategoryIds(RadComboBox ddlSubCategories, string TableName = "Dim_Products")
        {
            string SubCategoryIds = "";
            IControlHelper helper = new IControlHelper();
            try
            {
                string SubCategories = GetSelectedItemsWithQuotes(ddlSubCategories);
                if (!String.IsNullOrEmpty(SubCategories))
                {
                    string sql = "select distinct Sub_Category_Id from " + TableName + " where Sub_Category in (" + SubCategories + ")";
                    SubCategoryIds = GetCommaSeparatedIds(sql);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > GetSubCategoryIds"));
            }
            return SubCategoryIds;
        }

        /// <summary>
        /// Get segment ids by segment text
        /// </summary>
        /// <param name="ddlSegments"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public static string GetSegmentIds(RadComboBox ddlSegments, string TableName = "Dim_Products")
        {
            string SegmentIds = "";
            IControlHelper helper = new IControlHelper();
            try
            {
                string Segments = GetSelectedItemsWithQuotes(ddlSegments);
                if (!String.IsNullOrEmpty(Segments))
                {
                    string sql = "select distinct Segment_Id from " + TableName + " where Segment in (" + Segments + ")";
                    SegmentIds = GetCommaSeparatedIds(sql);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > GetSegmentIds"));

            }
            return SegmentIds;
        }

        /// <summary>
        /// Get category ids by category combo text
        /// </summary>
        /// <param name="ddlCategories"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public static string Category(RadComboBox ddlCategories, string TableName = "Dim_Products")
        {
            var CheckedItems = ddlCategories.CheckedItems;
            if (CheckedItems.Count == ddlCategories.Items.Count)
            {
                return "empty";
            }
            else
            {
                return GetCategoryIds(ddlCategories, TableName);
            }
        }

        /// <summary>
        /// Get category ids by category text
        /// </summary>
        /// <param name="ddlCategories"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public static string GetCategoryIds(RadComboBox ddlCategories, string TableName = "Dim_Products")
        {
            IControlHelper helper = new IControlHelper();
            string CategoryIds = "";
            try
            {
                string Categories = GetSelectedItemsWithQuotes(ddlCategories);
                if (!String.IsNullOrEmpty(Categories))
                {
                    string sql = "select distinct Category_Id from " + TableName + " where Category in (" + Categories + ")";
                    CategoryIds = GetCommaSeparatedIds(sql);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > GetCategoryIds"));

            }
            return CategoryIds;
        }

        /// <summary>
        /// Get Vendor Name by Vendor Number
        /// </summary>
        /// <param name="VendorNumber"></param>
        /// <returns></returns>
        public static string GetVendorName(string VendorNumber)
        {
            IControlHelper helper = new IControlHelper();
            string VendorName = "";
            try
            {
                VendorName = DataManager.GetFieldValueNetezza("DIM_VENDORS WHERE VENDOR_NUMBER=" + VendorNumber, "VENDOR_NAME");
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > GetVendorName"));
            }
            return VendorName;
        }

        /// <summary>
        /// get selected product size by ddlProductSizeDesc combo 
        /// </summary>
        /// <param name="ddlProductSizeDesc"></param>
        /// <returns></returns>
        public static string ProductSizeDesc(RadComboBox ddlProductSizeDesc)
        {
            DataLayer dal = new DataLayer();
            string strSelectedProductSize = "";

            var CheckedItems = ddlProductSizeDesc.CheckedItems;

            if (CheckedItems.Count == ddlProductSizeDesc.Items.Count)
            {
                strSelectedProductSize = "empty";
            }
            else
            {
                strSelectedProductSize = GetSelectedItems(ddlProductSizeDesc);
            }

            return dal.EncodeSpecialCharacters(strSelectedProductSize, true);
        }

        /// <summary>
        /// Fill the relative data list for weeks
        /// </summary>
        /// <param name="DrpDateVal"></param>
        /// <param name="weeks"></param>
        public static void FillRelativeDateListforWeeks(DropDownList DrpDateVal, int weeks = 0)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                int weekshow = 0;
                if (weeks == 0)
                    weekshow = 52;
                else
                    weekshow = weeks;

                int i = 1;
                using (DataTable dt = new DataTable())
                {
                    dt.Columns.Add("TextName");
                    dt.Columns.Add("TextValue");
                    for (i = 1; i <= weekshow; i++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["TextName"] = i;
                        dr["TextValue"] = i;
                        dt.Rows.Add(dr);
                    }
                    DrpDateVal.DataSource = dt;
                    DrpDateVal.DataTextField = "TextName";
                    DrpDateVal.DataValueField = "TextValue";
                    DrpDateVal.DataBind();
                    DrpDateVal.SelectedValue = "1";
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > FillRelativeDateListforWeeks"));
            }
        }

        /// <summary>
        /// Fill Schedule Frequency
        /// </summary>
        /// <param name="DrpReportTime"></param>
        public static void FillSchedule(DropDownList DrpReportTime)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                string str = "";
                str = "select * from SavedReportsSchedule order by Scheduleid";
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    using (DataTable dt = dataAccessProvider.Select(str))
                    {
                        DrpReportTime.DataSource = dt;
                        DrpReportTime.DataTextField = "ReportFrequency";
                        DrpReportTime.DataValueField = "ScheduleId";
                        DrpReportTime.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > FillSchedule"));
            }
        }

        /// <summary>
        /// Method for send mail
        /// </summary>
        /// <param name="emailtext">Text of Email</param>
        /// <param name="strSubject">Subject of Email</param>
        /// <param name="strFrom">From</param>
        /// <param name="strTo">To</param>
        /// <param name="filePath">File Path</param>
        /// <param name="strCC">CC</param>
        /// <returns></returns>
        public static bool SendEmail(string emailtext, string strSubject, string strFrom, string strTo, string filePath, string strCC = "")
        {
            IControlHelper helper = new IControlHelper();
            try
            {

                //Reader reading header and footer ----- Start
                TextReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~") + "\\Styles\\Header.txt");
                string HeaderHTML = reader.ReadToEnd().Replace("APPURL", ApplicationUrl);
                TextReader reader1 = new StreamReader(HttpContext.Current.Server.MapPath("~") + "\\Styles\\Footer.txt");
                string FooterHTML = reader1.ReadToEnd().Replace("APPURL", ApplicationUrl); ;
                //Reader reading header and footer ----- End

                MailMessage mm = new MailMessage(strFrom, strTo);

                if (!String.IsNullOrEmpty(strCC))
                    mm.CC.Add(strCC);

                mm.Subject = strSubject;

                emailtext = emailtext.Replace("<body>", "<body>" + HeaderHTML);
                emailtext = emailtext.Replace("</body>", FooterHTML + "</body>");
                mm.Body = emailtext;
                mm.IsBodyHtml = true;
                if (filePath.Length > 0)
                {
                    Attachment attach = new Attachment(filePath);
                    mm.Attachments.Add(attach);
                }
                SmtpClient smtp = new SmtpClient();
                NetworkCredential basicAuthenticationInfo = new NetworkCredential("iControl", "SpojIzF3CBum");
                smtp.Host = Convert.ToString(ConfigurationManager.AppSettings["mailHost"]);
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = basicAuthenticationInfo;
                smtp.EnableSsl = false;
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["mailPort"]);
                smtp.Send(mm);

                return true;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > SendEmail"));
            }
            return false;

        }

        /// <summary>
        /// Export data into csv 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strFilePath"></param>
        /// <param name="fileName"></param>
        /// <param name="blnWriteHeader"></param>
        /// <param name="strExportType"></param>
        /// <returns></returns>
        public static string ExportToCSV(DataTable dt, string strFilePath, string fileName, bool blnWriteHeader, string strExportType)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                var sw = new StreamWriter(strFilePath + fileName, false);

                // Write the headers.
                int iColCount = dt.Columns.Count;
                if (blnWriteHeader)
                {
                    for (int i = 0; i < iColCount; i++)
                    {
                        sw.Write(dt.Columns[i]);
                        if (i < iColCount - 1) sw.Write(",");
                    }
                    sw.Write(sw.NewLine);
                }
                foreach (System.Data.DataColumn col in dt.Columns) col.ReadOnly = false;
                // Write rows.
                foreach (DataRow dr in dt.Rows)
                {
                    for (int i = 0; i < iColCount; i++)
                    {
                        if (!Convert.IsDBNull(dr[i]) && dr[i].ToString().Trim().Length > 0)
                        {

                            dr[i] = dr[i].ToString().Replace("\r", " ").Replace("\n", " ");

                            if (IsInteger(dr[i].ToString().Trim()))
                            {
                                //use Replace for My Report page
                                sw.Write(@"=""" + dr[i].ToString().Replace("\"", "''") + @"""");
                            }
                            else if (dr[i].ToString().Trim().Substring(0, 1) == "-")
                            {
                                //use Replace for My Report page
                                sw.Write(@"=""" + dr[i].ToString().Replace("\"", "''") + @"""");
                            }
                            else if (dr[i].ToString().Length > 8 && (dr[i].ToString().Contains("/") || dr[i].ToString().Contains("-")) && !dr[i].ToString().Contains(","))
                            {
                                //use Replace for My Report page
                                sw.Write(SetDateFormat(dr[i].ToString().Replace("\"", "''")));
                            }
                            else
                            {
                                //use Replace for My Report page
                                sw.Write(dr[i].ToString().Replace(',', ';').Replace("\"", "''"));
                            }
                        }

                        if (i < iColCount - 1) sw.Write(",");
                    }
                    sw.Write(sw.NewLine);
                }

                sw.Close();

                if (strExportType != "zip")
                    return strFilePath + fileName;
                else
                    return WriteZipFile(strFilePath + fileName, strFilePath + fileName.Replace(".csv", ".zip"));
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > ExportToCSV"));
                throw ex;
            }
        }

        /// <summary>
        /// Return Value IsInteger
        /// </summary>
        /// <param name="theValue"></param>
        /// <returns></returns>
        public static bool IsInteger(string theValue)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                if (theValue.Substring(0, 1) == "0" && theValue.Contains(".") == false)
                {
                    return true;
                }
                else if (theValue.Length > 11)
                {
                    Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
                    return regex.IsMatch(theValue);
                }

                return false;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > IsInteger"));
                return false;
            }

        }

        /// <summary>
        /// Set Date format
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static string SetDateFormat(string strDate)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                strDate = DateTime.Parse(strDate).ToShortDateString();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > SetDateFormat"));
            }
            return strDate;
        }

        /// <summary>
        /// Method for write zip file
        /// </summary>
        /// <param name="files"></param>
        /// <param name="writeToFilePath"></param>
        /// <returns></returns>
        private static string WriteZipFile(string files, string writeToFilePath)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                Crc32 crc = new Crc32();
                ZipOutputStream s = new ZipOutputStream(File.Create(writeToFilePath));
                s.SetLevel(9);




                ZipEntry entry = new ZipEntry(Path.GetFileName(files));
                entry.DateTime = DateTime.Now;

                // Read in the 
                using (FileStream fs = File.OpenRead(files))
                {

                    byte[] buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, buffer.Length);

                    // set Size and the crc, because the information
                    // about the size and crc should be stored in the header
                    // if it is not set it is automatically written in the footer.
                    // (in this case size == crc == -1 in the header)
                    // Some ZIP programs have problems with zip files that don't store
                    // the size and crc in the header.
                    entry.Size = fs.Length;
                    fs.Close();

                    crc.Reset();
                    crc.Update(buffer);
                    entry.Crc = crc.Value;
                    s.PutNextEntry(entry);
                    s.Write(buffer, 0, buffer.Length);
                }


                s.Finish();
                s.Close();

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > WriteZipFile"));

            }

            return writeToFilePath;

        }

        /// <summary>
        /// Start date for saved filer for week
        /// </summary>
        /// <param name="days"></param>
        /// <returns></returns>
        public static DateTime StartDateForSaveFilterForWeek(int days)
        {
            DateTime SundayBeforeLast = EndDate().AddDays(-days).AddDays(1);
            return SundayBeforeLast;
        }

        /// <summary>
        /// Get last week saturday date time
        /// </summary>
        /// <returns></returns>
        public static DateTime EndDate()
        {
            DateTime StartOfWeek = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek);
            DateTime LastSaturday = StartOfWeek.AddDays(-1);
            return LastSaturday;
        }

        /// <summary>
        /// Method for decoding special characters
        /// </summary>
        /// <param name="strOriginal"></param>
        /// <returns></returns>
        public static string DecodeSpecialCharacters(string strOriginal)
        {
            string strEncodedString = strOriginal;


            strEncodedString = strEncodedString.Replace("%20", " ");

            // Replacing  %09 with Tab 
            strEncodedString = strEncodedString.Replace("%09", "\t");

            // Replacing ! with %21

            strEncodedString = strEncodedString.Replace("%21", "!");

            // Replacing # with %23

            strEncodedString = strEncodedString.Replace("%23", "#");

            // Replacing $ with %24

            strEncodedString = strEncodedString.Replace("%24", "$");

            // Replacing % with %25

            strEncodedString = strEncodedString.Replace("%25", "%");

            // Replacing & with %26

            strEncodedString = strEncodedString.Replace("%26", "&");

            // Replacing ' with %27

            strEncodedString = strEncodedString.Replace("%27", "'");

            // Replacing * with %2A

            strEncodedString = strEncodedString.Replace("%2A", "*");

            // Replacing , with %2C

            strEncodedString = strEncodedString.Replace("%2C", ",");

            // Replacing - with %2D

            strEncodedString = strEncodedString.Replace("%2D", "-");

            // Replacing . with %2E

            strEncodedString = strEncodedString.Replace("%2E", ".");

            // Replacing / with %2F

            strEncodedString = strEncodedString.Replace("%2F", "/");

            // Replacing : with %3A

            strEncodedString = strEncodedString.Replace("%3A", ":");

            // Replacing ; with %3B

            strEncodedString = strEncodedString.Replace("%3B", ";");

            // Replacing < with %3C

            strEncodedString = strEncodedString.Replace("%3C", "<");

            // Replacing = with %3D

            strEncodedString = strEncodedString.Replace("%3D", "=");

            // Replacing > with %3E

            strEncodedString = strEncodedString.Replace("%3E", ">");

            // Replacing ? with %3F

            strEncodedString = strEncodedString.Replace("%3F", "?");

            // Replacing @ with %40

            strEncodedString = strEncodedString.Replace("%40", "@");

            // Replacing [ with %5B

            strEncodedString = strEncodedString.Replace("%5B", "[");

            // Replacing \ with %5C

            strEncodedString = strEncodedString.Replace("%5C", @"\");

            // Replacing ] with %5D

            strEncodedString = strEncodedString.Replace("%5D", "]");

            // Replacing ^ with %5E

            strEncodedString = strEncodedString.Replace("%5E", "^");

            // Replacing _ with %5F

            strEncodedString = strEncodedString.Replace("%5F", "_");

            // Replacing ` with %60

            strEncodedString = strEncodedString.Replace("%60", "`");

            // Replacing { with %7B

            strEncodedString = strEncodedString.Replace("%7B", "{");

            // Replacing | with %7C

            strEncodedString = strEncodedString.Replace("%7C", "|");

            // Replacing } with %7D

            strEncodedString = strEncodedString.Replace("%7D", "}");

            // Replacing ~ with %7E

            strEncodedString = strEncodedString.Replace("%7E", "~");

            // Replacing ( with %28

            strEncodedString = strEncodedString.Replace("%28", "(");

            // Replacing ) with %29

            strEncodedString = strEncodedString.Replace("%29", ")");

            // Replacing + with %2B

            strEncodedString = strEncodedString.Replace("%2B", "+");

            // Replacing " with %22

            strEncodedString = strEncodedString.Replace("%22", "\"");

            return strEncodedString;
        }

        /// <summary>
        /// Get frame content
        /// </summary>
        /// <param name="ChartPath">Chart Path</param>
        /// <returns></returns>
        public static string GetFrameContent(string ChartPath)
        {
            ChartPath = ChartPath.Replace(" ", "%20").Replace("+", "%2B").Replace("(", "%28").Replace(")", "%29");
            return "<iframe id='Iframechart' src=" + ChartPath + "  width='100%' height='800' frameborder=1></iframe>";
        }

        /// <summary>
        /// Get Parameter Date
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="StartOrEnd"></param>
        /// <returns></returns>
        public static string GetParamDate(string StartDate, string StartOrEnd = "Start")
        {
            IControlHelper helper = new IControlHelper(); string ParamDate = "";
            try
            {

                DataTable dt = new DataTable();
                string strQuery = "SELECT distinct FISCAL_WEEK_START_DT, FISCAL_WEEK_END_DT ";
                strQuery += " FROM DIM_CALENDAR Where 'FY' || FISCAL_YEAR_NUM || case when Length(FISCAL_WEEK_OF_YEAR_NUM) =1 then  '-W0' || FISCAL_WEEK_OF_YEAR_NUM else '-W' || FISCAL_WEEK_OF_YEAR_NUM end =  '" + StartDate + "'";
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    dt = dataAccessProvider.Select(strQuery);
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (StartOrEnd == "Start")
                            ParamDate = Convert.ToDateTime(dr[0].ToString()).ToString("yyyy-MM-dd");
                        else
                            ParamDate = Convert.ToDateTime(dr[1].ToString()).ToString("yyyy-MM-dd");
                    }
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > GetParamDate"));

            }
            return ParamDate;
        }

        /// <summary>
        /// Get Selected items with double quotes
        /// </summary>
        /// <param name="ddlCombo"></param>
        /// <param name="IsText"></param>
        /// <returns></returns>
        public static string GetSelectedItemsWithDoubleQuotes(RadComboBox ddlCombo, bool IsText = false)
        {
            string strSelected = "";
            IControlHelper helper = new IControlHelper();
            try
            {
                foreach (RadComboBoxItem ltm in ddlCombo.Items)
                {
                    if (ltm.Checked && !String.IsNullOrEmpty(ltm.Value.ToString()))
                    {
                        if (IsText == false)
                            strSelected += "'" + ltm.Value.ToString().Replace("'", "''''") + "'" + ",";
                        else
                            strSelected += "''" + ltm.Text.ToString().Replace("'", "''''") + "''" + ",";
                    }
                }
                strSelected = strSelected.TrimEnd(',');
                //}
            }
            catch (Exception ex)
            {

                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > GetSelectedItemsWithDoubleQuotes"));
            }
            strSelected += "";
            return strSelected;
        }

        /// <summary>
        /// Get Custom level 1 text with encoding special character 
        /// </summary>
        /// <param name="ddlcustomelevel1"></param>
        /// <returns></returns>
        public static string CustomLevel1(RadComboBox ddlcustomelevel1)
        {
            var CheckedItems = ddlcustomelevel1.CheckedItems;
            if (CheckedItems.Count == ddlcustomelevel1.Items.Count)
            {
                return "empty";
            }
            else
            {
                return EncodeSpecialCharacters(GetCustomLevel1(ddlcustomelevel1));
            }
        }

        /// <summary>
        ///  Get Custom level 2 text with encoding special character 
        /// </summary>
        /// <param name="ddlcustomelevel2"></param>
        /// <returns></returns>
        public static string CustomLevel2(RadComboBox ddlcustomelevel2)
        {
            var CheckedItems = ddlcustomelevel2.CheckedItems;
            if (CheckedItems.Count == ddlcustomelevel2.Items.Count)
            {
                return "empty";
            }
            else
            {
                return EncodeSpecialCharacters(GetCustomLevel2(ddlcustomelevel2));
            }
        }

        /// <summary>
        ///  Get Custom level 3 text with encoding special character 
        /// </summary>
        /// <param name="ddlcustomelevel3"></param>
        /// <returns></returns>
        public static string CustomLevel3(RadComboBox ddlcustomelevel3)
        {
            var CheckedItems = ddlcustomelevel3.CheckedItems;
            if (CheckedItems.Count == ddlcustomelevel3.Items.Count)
            {
                return "empty";
            }
            else
            {
                return EncodeSpecialCharacters(GetCustomLevel3(ddlcustomelevel3));
            }
        }

        /// <summary>
        /// Method for Encoding special character
        /// </summary>
        /// <param name="strOriginal"></param>
        /// <returns></returns>
        public static string EncodeSpecialCharacters(string strOriginal)
        {
            string strEncodedString = strOriginal;

            // Replacing % with %25

            strEncodedString = strEncodedString.Replace("%", "%25");

            // Replacing Space with %20
            strEncodedString = strEncodedString.Replace(" ", "%20");

            // Replacing Tab with %09
            strEncodedString = strEncodedString.Replace("\t", "%09");

            // Replacing ! with %21

            strEncodedString = strEncodedString.Replace("!", "%21");

            // Replacing # with %23

            strEncodedString = strEncodedString.Replace("#", "%23");

            // Replacing $ with %24

            strEncodedString = strEncodedString.Replace("$", "%24");

            // Replacing & with %26

            strEncodedString = strEncodedString.Replace("&", "%26");

            // Replacing ' with %27

            strEncodedString = strEncodedString.Replace("'", "%27");

            // Replacing * with %2A

            strEncodedString = strEncodedString.Replace("*", "%2A");

            // Replacing , with %2C

            strEncodedString = strEncodedString.Replace(",", "%2C");

            // Replacing - with %2D

            strEncodedString = strEncodedString.Replace("-", "%2D");

            // Replacing . with %2E

            strEncodedString = strEncodedString.Replace(".", "%2E");

            // Replacing / with %2F

            strEncodedString = strEncodedString.Replace("/", "%2F");

            // Replacing : with %3A

            strEncodedString = strEncodedString.Replace(":", "%3A");

            // Replacing ; with %3B

            strEncodedString = strEncodedString.Replace(";", "%3B");

            // Replacing < with %3C

            strEncodedString = strEncodedString.Replace("<", "%3C");

            // Replacing = with %3D

            strEncodedString = strEncodedString.Replace("=", "%3D");

            // Replacing > with %3E

            strEncodedString = strEncodedString.Replace(">", "%3E");

            // Replacing ? with %3F

            strEncodedString = strEncodedString.Replace("?", "%3F");

            // Replacing @ with %40

            strEncodedString = strEncodedString.Replace("@", "%40");

            // Replacing [ with %5B

            strEncodedString = strEncodedString.Replace("[", "%5B");

            // Replacing \ with %5C

            strEncodedString = strEncodedString.Replace(@"\", "%5C");

            // Replacing ] with %5D

            strEncodedString = strEncodedString.Replace("]", "%5D");

            // Replacing ^ with %5E

            strEncodedString = strEncodedString.Replace("^", "%5E");

            // Replacing _ with %5F

            strEncodedString = strEncodedString.Replace("_", "%5F");

            // Replacing ` with %60

            strEncodedString = strEncodedString.Replace("`", "%60");

            // Replacing { with %7B

            strEncodedString = strEncodedString.Replace("{", "%7B");

            // Replacing | with %7C

            strEncodedString = strEncodedString.Replace("|", "%7C");

            // Replacing } with %7D

            strEncodedString = strEncodedString.Replace("}", "%7D");

            // Replacing ~ with %7E

            strEncodedString = strEncodedString.Replace("~", "%7E");

            // Replacing ( with %28

            strEncodedString = strEncodedString.Replace("(", "%28");

            // Replacing ) with %29

            strEncodedString = strEncodedString.Replace(")", "%29");

            // Replacing + with %2B

            strEncodedString = strEncodedString.Replace("+", "%2B");

            // Replacing " with %22

            strEncodedString = strEncodedString.Replace("\"", "%22");

            return strEncodedString;
        }

        /// <summary>
        /// Get Custom level 1 comma separated ids
        /// </summary>
        /// <param name="ddlcustomlevel1"></param>
        /// <returns></returns>
        public static string GetCustomLevel1(RadComboBox ddlcustomlevel1)
        {
            string customlevel1 = "";
            IControlHelper helper = new IControlHelper();
            try
            {
                string customlevel1_val = GetSelectedItemsWithQuotes(ddlcustomlevel1);
                if (!String.IsNullOrEmpty(customlevel1_val))
                {
                    string sql = "select distinct CUSTOM_LEVEL_1 from Dim_Custom_Hierarchy where CUSTOM_LEVEL_1  in (" + customlevel1_val + ")";
                    customlevel1 = GetCommaSeparatedIds(sql);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomLevel1()"));
            }
            return customlevel1;
        }

        /// <summary>
        /// Get Custom level 2 comma separated ids
        /// </summary>
        /// <param name="ddlcustomlevel2"></param>
        /// <returns></returns>
        public static string GetCustomLevel2(RadComboBox ddlcustomlevel2)
        {
            string customlevel2 = ""; IControlHelper helper = new IControlHelper();
            try
            {
                string customlevel2_val = GetSelectedItemsWithQuotes(ddlcustomlevel2);
                if (!String.IsNullOrEmpty(customlevel2_val))
                {
                    string sql = "select distinct CUSTOM_LEVEL_2 from Dim_Custom_Hierarchy where CUSTOM_LEVEL_2  in (" + customlevel2_val + ")";
                    customlevel2 = GetCommaSeparatedIds(sql);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomLevel2()"));
            }
            return customlevel2;
        }

        /// <summary>
        /// Get Custom level 3 comma separated ids
        /// </summary>
        /// <param name="ddlcustomlevel3"></param>
        /// <returns></returns>
        public static string GetCustomLevel3(RadComboBox ddlcustomlevel3)
        {
            string customlevel3 = ""; IControlHelper helper = new IControlHelper();
            try
            {
                string customlevel3_val = GetSelectedItemsWithQuotes(ddlcustomlevel3);
                if (!String.IsNullOrEmpty(customlevel3_val))
                {
                    string sql = "select distinct CUSTOM_LEVEL_3 from Dim_Custom_Hierarchy where CUSTOM_LEVEL_3  in (" + customlevel3_val + ")";
                    customlevel3 = GetCommaSeparatedIds(sql);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomLevel3()"));
            }
            return customlevel3;
        }

        /// <summary>
        /// Get Banner comma separated ids
        /// </summary>
        /// <param name="ddlBanners"></param>
        /// <returns></returns>
        public static string GetBannerIds(RadComboBox ddlBanners)
        {
            string BannerIds = ""; IControlHelper helper = new IControlHelper();
            try
            {
                string Banners = GetSelectedItemsWithQuotes(ddlBanners);
                if (!String.IsNullOrEmpty(Banners))
                {
                    string sql = "select distinct Top_Banner_Id from Dim_Stores where Top_Banner in (" + Banners + ")";
                    BannerIds = GetCommaSeparatedIds(sql);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetBannerIds()"));
            }
            return BannerIds;
        }

        /// <summary>
        /// Get banner ids by banner combo
        /// </summary>
        /// <param name="Banners"></param>
        /// <returns></returns>
        public static string Banner(RadComboBox Banners)
        {
            var CheckedItems = Banners.CheckedItems;
            if (CheckedItems.Count == Banners.Items.Count)
            {
                return "";
            }
            else
            {
                return GetBannerIds(Banners);
            }
        }

        /// <summary>
        /// Get Brand ids by brand combo
        /// </summary>
        /// <param name="ddlBrands"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public static string GetBrandIds(RadComboBox ddlBrands, string TableName = "Dim_Products")
        {
            string BrandIds = ""; IControlHelper helper = new IControlHelper();
            try
            {
                string BrandName = GetSelectedItemsWithQuotes(ddlBrands);
                if (!String.IsNullOrEmpty(BrandName))
                {
                    string sql = "select distinct MFR_BRAND_ID from " + TableName + " where BRAND_NAME in (" + BrandName + ")";
                    BrandIds = GetCommaSeparatedIds(sql);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetBrandIds()"));
            }
            return BrandIds;
        }

        /// <summary>
        /// Get single category ids by Category combo
        /// </summary>
        /// <param name="ddlCategories"></param>
        /// <returns></returns>
        public static string SingleCategory(RadComboBox ddlCategories)
        {
            return GetSingleCategoryId(ddlCategories);
        }

        /// <summary>
        /// Get Single Category ids by category
        /// </summary>
        /// <param name="ddlCategories"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public static string GetSingleCategoryId(RadComboBox ddlCategories, string TableName = "Dim_Products")
        {
            string CategoryIds = ""; IControlHelper helper = new IControlHelper();
            try
            {
                if (ddlCategories.SelectedItem.ToString() != "--Select Category--")
                {
                    string sql = "select distinct Category_Id from " + TableName + " where Category in ('" + ddlCategories.SelectedValue.ToString() + "')";
                    CategoryIds = GetCommaSeparatedIds(sql);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetSingleCategoryId()"));
            }
            return CategoryIds;
        }

        #region Share

        /// <summary>
        /// Fill shared Users
        /// </summary>
        /// <param name="RdShareUsers"></param>
        /// <param name="userType"></param>
        /// <param name="userName"></param>
        public static void FillShareUsers(RadComboBox RdShareUsers, bool isCorporate, string UserName)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                string str = "";
                if (isCorporate)
                {
                    str = helper.GetResourceString(AppConstants.GET_SHARE_USERS_FOR_CORPORATE).Replace("@UserName", UserName);
                }
                else
                {
                    str = helper.GetResourceString(AppConstants.GET_SHARE_USERS_FOR_NONCORPORATE).Replace("@UserName", UserName);
                }
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    using (DataTable dt = dataAccessProvider.Select(str))
                    {
                        RdShareUsers.Items.Clear();
                        RdShareUsers.DataSource = dt;
                        RdShareUsers.DataTextField = "NAME";
                        RdShareUsers.DataValueField = "USERNAME";
                        RdShareUsers.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs > FillShareUsers"));
            }

        }

        #endregion

        #region MenuAssignment
        public static void FillRadDropDownList(string strSQL, string strFldName, string fldId, RadComboBox ddlList)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                ddlList.Items.Clear();
                if (strSQL.ToLower().Contains("order by") == false)
                    strSQL = strSQL + " order by " + strFldName;
                DataTable Dt = new DataTable();
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    Dt = dataAccessProvider.Select(strSQL);

                }

                if (Dt.Rows.Count > 0)
                {
                    ddlList.DataSource = Dt;
                    ddlList.DataTextField = strFldName;
                    ddlList.DataValueField = fldId;
                    ddlList.DataBind();
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>FillRadDropDownList"));
            }

        }
        public static void FillRadDropDownListNetezza(DataTable dt, string strFldName, string strFldValue, RadComboBox ddlList, string strDefaultValue, Boolean isSelectOne = false)
        {
            try
            {
                ddlList.Items.Clear();

                if (dt.Rows.Count > 0)
                {
                    DataView dv = dt.DefaultView;
                    dv.Sort = strFldName + " asc";
                    DataTable sortedDT = dv.ToTable();
                    ddlList.DataSource = sortedDT;
                    ddlList.DataTextField = strFldName;
                    ddlList.DataValueField = strFldValue;
                    ddlList.ClearSelection();
                    ddlList.DataBind();

                    if (strDefaultValue != "" && ddlList.Items.Count >= 1)
                    {
                        ddlList.Items.Insert(0, new RadComboBoxItem(strDefaultValue, "-1"));
                    }

                    if (isSelectOne == true)
                    {
                        ddlList.Items.Insert(0, new RadComboBoxItem("--Please Select One--", "-2"));
                    }
                    else
                        ddlList.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static void FillRadDropDownList(string strSQL, string strFldName, string fldId, RadComboBox ddlList, string strDefaultValue, Boolean isSelectOne = false)
        {
            IControlHelper helper = new IControlHelper();
            try
            {

                ddlList.Items.Clear();
                if (strSQL.ToLower().Contains("order by") == false)
                    strSQL = strSQL + " order by " + strFldName;
                DataTable Dt = new DataTable();
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    Dt = dataAccessProvider.Select(strSQL);

                }
                if (Dt.Rows.Count > 0)
                {
                    ddlList.DataSource = Dt;
                    ddlList.DataTextField = strFldName;
                    ddlList.DataValueField = fldId;
                    ddlList.DataBind();

                    if (strDefaultValue != "" && ddlList.Items.Count >= 1)
                    {
                        ddlList.Items.Insert(0, new RadComboBoxItem(strDefaultValue, "-1"));
                    }

                    if (isSelectOne == true)
                    {
                        ddlList.Items.Insert(0, new RadComboBoxItem("--Please Select One--", "-2"));
                    }
                    else
                        ddlList.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>FillRadDropDownList"));
            }

        }
        public static void SetIdInRadDropDown(RadComboBox ddlList, Int32 intId)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                for (int cnt = 0; cnt < ddlList.Items.Count; cnt++)
                {
                    if (ddlList.Items[cnt].Value == intId.ToString())
                    {
                        ddlList.SelectedIndex = cnt;
                    }
                }
            }

            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>SetIdInRadDropDown"));
            }
        }
        public static void SetTextInRadDropDown(RadComboBox ddlList, string strText)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                for (int cnt = 0; cnt < ddlList.Items.Count; cnt++)
                {
                    if (ddlList.Items[cnt].Text == strText)
                    {
                        ddlList.SelectedIndex = cnt;
                    }
                }
            }

            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>SetTextInRadDropDown"));
            }
        }
        public static void SetValueInRadDropDown(RadComboBox ddlList, string strValue)
        {
            IControlHelper helper = new IControlHelper();

            try
            {
                for (int cnt = 0; cnt < ddlList.Items.Count; cnt++)
                {
                    if (ddlList.Items[cnt].Value == strValue)
                    {
                        ddlList.SelectedIndex = cnt;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>SetValueInRadDropDown"));
            }
        }
        /// <summary>
        /// Get Owner Users
        /// </summary>
        /// <param name="strQry"></param>
        /// /// <param name="UserName"></param>
        /// <returns></returns>
        public static DataTable GetOwnerUsers(string UserName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_OWNER_USERS).Replace("@UserName", UserName);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetOwnerUsers"));
            }
            return dtResult;
        }
        public static DataTable GetCustomStoresShared(string Query, Int64 CustomStoreGroupID)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(Query).Replace("@CustomStoreGroupID", CustomStoreGroupID.ToString());
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomStoresShared"));
            }
            return dtResult;
        }
        public static DataTable GetCustomStoresSharedByStoreName(string UserName, string StoreName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_CUSTOM_STORES_MASTER_ON_STORENAME).Replace("@UserName", UserName).Replace("@StoreName", StoreName);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomStoresSharedByStoreName"));
            }
            return dtResult;
        }
        public static DataTable GetStoreNumber(string StoreNumber)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_STORE_NUMBER_FROM_STORES).Replace("@StoreNumber", StoreNumber);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetStoreNumber"));
            }
            return dtResult;
        }
        public static DataTable GetNextValueForSEQ_CUSTOM_HIERARCHIES()
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                string Query = helper.GetResourceString(AppConstants.GET_NEXT_VALUE_FOR_SEQ_CUSTOM_HIERARCHIES_MASTER);
                dtResult = DataManager.ExecuteQueryForNetezza(Query);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetNextValueForSEQ_CUSTOM_HIERARCHIES"));
            }
            return dtResult;
        }
        public static DataTable GetNameFromUserDataAccess(string UserName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_NAME_FROM_USERDATAACCESS).Replace("@UserName", UserName);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetNextValueForSEQ_CUSTOM_HIERARCHIES"));
            }
            return dtResult;
        }
        public static DataTable GetCustomStoresSharedOnUsername(string UserName, Int64 CustomStoreGroupID)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_CUSTOM_STORES_SHARED_ON_USERNAME).Replace("@UserName", UserName).Replace("@CustomStoreGroupID", CustomStoreGroupID.ToString());
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetNextValueForSEQ_CUSTOM_HIERARCHIES"));
            }
            return dtResult;
        }
        public static int DeleteCustomStoresShared(Int64 CustomStoreGroupID, string UserName)
        {
            IControlHelper helper = new IControlHelper();
            int RowsAffected = 0;
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.DELETE_CUSTOM_STORES_SHARED).Replace("@CustomStoreGroupID", CustomStoreGroupID.ToString()).Replace("@UserName", UserName);
                RowsAffected = DataManager.ExecuteDeleteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>DeleteCustomStoresShared"));
            }
            return RowsAffected;
        }
        public static int InsertIntoCustomStoresShared(Int64 CustomStoreId, string UserName, string CurrentDate)
        {
            IControlHelper helper = new IControlHelper();
            int RowsAffected = 0;
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.INSERT_INTO_CUSTOM_STORES_SHARED).Replace("@CustomStoreId", CustomStoreId.ToString()).Replace("@UserName", UserName).Replace("@CurrentDate", CurrentDate);
                RowsAffected = DataManager.ExecuteInsertQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>DeleteCustomStoresShared"));
            }
            return RowsAffected;
        }
        public static DataTable GetCustomStoresAndUserDataAccess(string CustomStoreGroupID)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_CUSTOM_STORES_AND_USERDATAACCESS).Replace("@CustomStoreGroupID", CustomStoreGroupID);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomStoresAndUserDataAccess"));
            }
            return dtResult;
        }
        public static DataTable GetUserDataAccessForEmailLNotNull(string UserName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_USERDATAACCESS_FOR_EMAIL_NOT_NULL).Replace("@UserName", UserName);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetUserDataAccessForEmailLNotNull"));
            }
            return dtResult;
        }
        public static bool DeleteCustomStoreGroup(Int64 CustomGroupID)
        {
            IControlHelper helper = new IControlHelper();
            DataLayer DAL = new DataLayer();
            bool Result = false;
            try
            {
                Result = DAL.SP_DELETECUSTOMSTOREGROUP_NEW(CustomGroupID);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>DeleteCustomStoresShared"));
            }
            return Result;
        }
        public static int InsertDuplicateCustomHierarchy(long Custom_Hierarchy_Id, long New_Custom_Hierarchy_Id, string UserName, string New_HierarchyName, string New_HierarchyDesc)
        {
            IControlHelper helper = new IControlHelper();
            DataLayer DAL = new DataLayer();
            int RowsAffected = 0;
            try
            {
                RowsAffected = DAL.SP_INSERT_DUPLICATECUSTOMHIERARCHY(Custom_Hierarchy_Id, New_Custom_Hierarchy_Id, UserName,New_HierarchyName, New_HierarchyDesc);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>InsertDuplicateCustomHierarchy"));
            }
            return RowsAffected;
        }
        public static DataTable GetSearchResult(string strQuery)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetSearchResult"));
            }
            return dtResult;
        }
        public static int GetNextValueForCustomStoreMaster()
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            int Result = 0;
            try
            {
                dtResult = DataManager.ExecuteQueryForNetezza(helper.GetResourceString(AppConstants.GET_NEXT_VALUE_FOR_SEQ_CUSTOM_HIERARCHIES_MASTER));
                Result = Convert.ToInt32(dtResult.Rows[0]["NEXTVAL"]);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetNextValueForCustomStoreMaster"));
            }
            return Result;
        }
        public static DataTable GetAllFromStores(string StoreNumber)
        {
            IControlHelper helper = new IControlHelper();
            DataTable Result = new DataTable();
            try
            {
                string strQuery = helper.GetResourceString(AppConstants.GET_ALL_FROM_STORES).Replace("@StoreNumber", StoreNumber);
                Result = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetAllFromStores"));
            }
            return Result;
        }
        public static int UpdateCustomStoreDetails(string StoreName,string StoreGroupDesc,Int64 CustomStoreGroupID,string CustomLevel1,string CustomLevel2,string CustomLevel3,string StoreNo,string StoreID)
        {
            IControlHelper helper = new IControlHelper();
            int Result = 0;
            string strQuery = string.Empty;
            try
            {
                strQuery = helper.GetResourceString(AppConstants.UPDATE_CUSTOM_STORES_DETAILS).Replace("@STORE_NUMBER", StoreName).Replace("@CUSTOM_LEVEL_1", CustomLevel1).Replace("@CUSTOM_LEVEL_2", CustomLevel2).Replace("@CUSTOM_LEVEL_3", CustomLevel3).Replace("@CustomStoreGroupID", CustomStoreGroupID.ToString()).Replace("@StoreID", StoreID);
                Result = DataManager.ExecuteUpdateQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>UpdateCustomStore"));
            }
            return Result;
        }
        public static int UpdateCustomStoreMaster(string StoreName,string StoreGroupDesc,Int64 CustomStoreGroupID)
        {
            IControlHelper helper = new IControlHelper();
            int Result = 0;
            string strQuery = string.Empty;
            try
            {
                strQuery = helper.GetResourceString(AppConstants.UPDATE_CUSTOM_STORES_MASTER).Replace("@CUSTOM_STORE_NAME", StoreName).Replace("@CUSTOM_STORE_DESCRIPTION", StoreGroupDesc).Replace("@CustomGroupID", CustomStoreGroupID.ToString());
                Result = DataManager.ExecuteUpdateQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>UpdateCustomStore"));
            }
            return Result;
        }
        public static string SpecialChars_Message()
        {
            string message = "Note: & , < > ' \\ + % ^ characters are not allowed.";
            return message;
        }
        public static String SpecialCharactersValidationforAlert_New(String _stringToValidate, string controlname)
        {
            IControlHelper helper = new IControlHelper();
            String _message = String.Empty;
            try
            {
                //string[] stringToValidate = Regex.Split(_stringToValidate, string.Empty);
                string[] _RestrictedCharacters = { "&", ",", "<", ">", "'", "\\", "+", "%", "^" };
                string InvalidChars = "";
                foreach (string invalid in _RestrictedCharacters)
                {
                    if (_stringToValidate.Contains(invalid))
                    {
                        if (invalid == "'")
                        {
                            InvalidChars += "&apos;";
                        }
                        else if (invalid == "\\")
                        {
                            InvalidChars += " &#92;";
                        }
                        else
                        {
                            InvalidChars += invalid;
                        }
                    }
                }
                //cannot include the following characters: & , < >  &apos; &#92; + % ^"
                _message = InvalidChars == string.Empty ? String.Empty : "Note: " + controlname + " cannot include the following character(s): " + InvalidChars;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>SpecialCharactersValidationforAlert"));
            }
            return _message;
        }
        public static int InvalidCharactersCount(string status)
        {
            IControlHelper helper = new IControlHelper();
            int _InvalidCharCount = 0;
            try
            {
                char[] _RestrictedCharacters = { '&', ',', '<', '>', '\'', '\\', '+', '%', '^' };
                foreach (char invalid in _RestrictedCharacters)
                {
                    _InvalidCharCount += status.Split(invalid).Length - 1;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>InvalidCharactersCount"));
            }
            return _InvalidCharCount;
        }
        public static string ExportFileNew(string DownloadedFileName, string PageName, DataTable dtErrorLog, bool isEditHierarchy = false, bool isCustomStore = false)
        {
            IControlHelper helper = new IControlHelper();
            string ExcelLogFileName = "";
            string strUPCStore = "UPC";

            //For Custom Store Validations
            if (isCustomStore)
                strUPCStore = "Store Number";

            try
            {
                DataTable dtExport = new DataTable();
                dtExport.Columns.Add("RowNumber");
                dtExport.Columns.Add("Status");
                dtExport.Columns.Add(strUPCStore);
                int invalidUPC_Count = 0;
                int duplicateUPC_Count = 0;
                int invalidChar_Count = 0;
                int existUPC_Count = 0;
                int missingInformation_Count = 0;
                int columnHeaders_Count = 0;
                List<string> duplicateUPCsList = new List<string>();
                List<string> invalidUPCsList = new List<string>();
                List<string> existUPCsList = new List<string>();
                List<string> missingInformationList = new List<string>();
                List<string> columnHeadersList = new List<string>();

                for (int i = 0; i < dtErrorLog.Rows.Count; i++)
                {
                    DataRow dr = dtExport.NewRow();

                    if (Convert.ToString(dtErrorLog.Rows[i]["Row No"]) == "Header")
                        dr[0] = "Header";
                    else
                        dr[0] = Convert.ToInt16(dtErrorLog.Rows[i]["Row No"]) + 1;

                    dr[1] = dtErrorLog.Rows[i]["status"].ToString();
                    dr[2] = dtErrorLog.Rows[i][strUPCStore].ToString();
                    dtExport.Rows.Add(dr);
                    //invalid upc count
                    if (dtErrorLog.Rows[i]["status"].ToString() == "Invalid " + strUPCStore)
                        invalidUPCsList.Add(dtErrorLog.Rows[i][strUPCStore].ToString());

                    //duplicate upc count
                    if (dtErrorLog.Rows[i]["status"].ToString().Contains("Duplicate"))
                        duplicateUPCsList.Add(dtErrorLog.Rows[i][strUPCStore].ToString());

                    //exist upc count
                    if (dtErrorLog.Rows[i]["status"].ToString().Contains(strUPCStore + " already exists"))
                        existUPCsList.Add(dtErrorLog.Rows[i][strUPCStore].ToString());

                    //exist upc count
                    if (dtErrorLog.Rows[i]["status"].ToString().Contains("cannot be blank"))
                        missingInformationList.Add(dtErrorLog.Rows[i]["status"].ToString());

                    //incorrect column headers
                    if (dtErrorLog.Rows[i]["status"].ToString().Contains("Column Header"))
                        columnHeadersList.Add(dtErrorLog.Rows[i]["status"].ToString());

                    //invalid characters count
                    if (!dtErrorLog.Rows[i]["status"].ToString().Contains("Column Header"))
                        invalidChar_Count += InvalidCharactersCount(dtErrorLog.Rows[i]["status"].ToString());
                }

                duplicateUPC_Count = duplicateUPCsList.Distinct().ToList().Count;
                invalidUPC_Count = invalidUPCsList.ToList().Count;
                existUPC_Count = existUPCsList.ToList().Count;
                missingInformation_Count = missingInformationList.ToList().Count;
                columnHeaders_Count = columnHeadersList.ToList().Count;

                string filePath = HttpContext.Current.Server.MapPath("~") + "Exports\\";
                string _Filename = DownloadedFileName + "_" + DateTime.Now.ToString("MMddyyHHmmss");
                string filename = filePath + _Filename + ".xlsx";

                Microsoft.Office.Interop.Excel.Application xlApp;
                Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;
                xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();

                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                xlWorkSheet.Name = DownloadedFileName;

                Microsoft.Office.Interop.Excel.Range chartRange;
                xlApp.StandardFont = "Calibri"; //Arial Unicode MS  //Calibri
                xlApp.StandardFontSize = 11;

                string DistinctChars = " ";
                string AllSelectedChars = "";
                if (HttpContext.Current.Session["SpecialChars"] != null)
                {
                    AllSelectedChars = Convert.ToString(HttpContext.Current.Session["SpecialChars"]);
                    var chararr = AllSelectedChars.ToCharArray();
                    for (int i = 0; i < chararr.Length; i++)
                    {
                        if (!DistinctChars.Contains(chararr[i]))
                        {
                            DistinctChars += chararr[i] + " ";
                        }
                    }
                }

                //Add line on top of error log
                chartRange = xlWorkSheet.get_Range("A1", "B1");
                chartRange.Merge();
                chartRange.Font.Name = "Calibri";
                chartRange.Font.Size = 11;
                chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                chartRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignTop;
                chartRange.WrapText = true;
                chartRange.Font.Bold = true;
                chartRange.Value = "The following errors were found in your " + PageName + " and need to be removed or replaced";
                chartRange.RowHeight = 28;

                chartRange = xlWorkSheet.get_Range("A2");
                chartRange.ColumnWidth = 25;
                chartRange.Font.Size = 11;
                chartRange.Value = "Special Characters: " + DistinctChars;
                //
                chartRange = xlWorkSheet.get_Range("B2");
                chartRange.ColumnWidth = 80;
                chartRange.Font.Size = 11;
                chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                chartRange.Value = invalidChar_Count;

                //**Duplicate UPCs
                chartRange = xlWorkSheet.get_Range("A3");
                chartRange.ColumnWidth = 25;
                chartRange.Font.Size = 11;
                chartRange.Value = "Duplicate " + strUPCStore + "s";
                //
                chartRange = xlWorkSheet.get_Range("B3");
                chartRange.ColumnWidth = 80;
                chartRange.Font.Size = 11;
                chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                chartRange.Value = duplicateUPC_Count;

                //**Invalid UPCs
                chartRange = xlWorkSheet.get_Range("A4");
                chartRange.ColumnWidth = 25;
                chartRange.Font.Size = 11;
                chartRange.Value = "Invalid " + strUPCStore + "s";
                //
                chartRange = xlWorkSheet.get_Range("B4");
                chartRange.ColumnWidth = 80;
                chartRange.Font.Size = 11;
                chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                chartRange.Value = invalidUPC_Count;

                //**Missing information
                chartRange = xlWorkSheet.get_Range("A5");
                chartRange.ColumnWidth = 25;
                chartRange.Font.Size = 11;
                chartRange.Value = "Missing Information";
                //
                chartRange = xlWorkSheet.get_Range("B5");
                chartRange.ColumnWidth = 80;
                chartRange.Font.Size = 11;
                chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                chartRange.Value = missingInformation_Count;

                //**Incorrect Column Headers
                chartRange = xlWorkSheet.get_Range("A6");
                chartRange.ColumnWidth = 25;
                chartRange.Font.Size = 11;
                chartRange.Value = "Incorrect Column Headers";
                //
                chartRange = xlWorkSheet.get_Range("B6");
                chartRange.ColumnWidth = 80;
                chartRange.Font.Size = 11;
                chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                chartRange.Value = columnHeaders_Count;

                if (isEditHierarchy)
                {
                    //**UPC already exists
                    chartRange = xlWorkSheet.get_Range("A7");
                    chartRange.ColumnWidth = 25;
                    chartRange.Font.Size = 11;
                    chartRange.Value = strUPCStore + "'s already in hierarchy";
                    //
                    chartRange = xlWorkSheet.get_Range("B7");
                    chartRange.ColumnWidth = 80;
                    chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                    chartRange.Value = existUPC_Count;
                }

                //** Columns Name properties
                chartRange = xlWorkSheet.get_Range("A9", "D9");
                chartRange.Font.Name = "Calibri";
                chartRange.Font.Size = 11;
                chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                chartRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignTop;
                chartRange.WrapText = true;
                chartRange.Font.Bold = true;

                //** Columns Name
                chartRange = xlWorkSheet.get_Range("A9");
                chartRange.ColumnWidth = 25;
                chartRange.Value = "Row Number";

                chartRange = xlWorkSheet.get_Range("B9");
                chartRange.ColumnWidth = 80;
                chartRange.Value = "Status";

                chartRange = xlWorkSheet.get_Range("C9");
                chartRange.ColumnWidth = 14;
                chartRange.Value = strUPCStore;

                //** Report Data Columns
                int rowCount = 10;
                xlApp.StandardFont = "Calibri";
                if (dtExport.Rows.Count != 0)
                {
                    //dtExport.DefaultView.Sort = "UPC desc";
                    dtExport = dtExport.DefaultView.ToTable();
                    foreach (DataRow dr in dtExport.Rows)
                    {
                        chartRange = xlWorkSheet.get_Range("A" + rowCount);
                        chartRange.WrapText = true;
                        chartRange.Font.Size = 11;
                        chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
                        chartRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignTop;
                        chartRange.Value = dr["RowNumber"].ToString();

                        chartRange = xlWorkSheet.get_Range("B" + rowCount);
                        chartRange.WrapText = true;
                        chartRange.Font.Size = 11;
                        chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                        chartRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignTop;
                        chartRange.Value = dr["Status"].ToString();

                        chartRange = xlWorkSheet.get_Range("C" + rowCount);
                        chartRange.WrapText = true;
                        chartRange.Font.Size = 11;
                        chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
                        chartRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignTop;
                        chartRange.NumberFormat = "@";
                        chartRange.Value = dr[strUPCStore].ToString();

                        rowCount++;
                    }
                }
                try
                {
                    xlApp.DisplayAlerts = false;
                    xlWorkBook.SaveAs(filename, misValue, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, misValue, misValue, misValue, misValue);
                    xlWorkBook.Close(true, misValue, misValue);
                    releaseObject(xlApp);
                    releaseObject(xlWorkBook);
                    releaseObject(xlWorkSheet);
                }
                catch (Exception ex)
                {
                    xlWorkBook.Close(true, misValue, misValue);
                    releaseObject(xlApp);
                    releaseObject(xlWorkBook);
                    releaseObject(xlWorkSheet);
                    //throw ex;
                }
                //string path = MapPath(filename);

                ExcelLogFileName = _Filename + ".xlsx";

                if (!isEditHierarchy)
                {
                    HttpContext.Current.Session["SpecialChars"] = null;
                    byte[] bts = System.IO.File.ReadAllBytes(filename);
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ClearHeaders();
                    HttpContext.Current.Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    HttpContext.Current.Response.AddHeader("Content-Length", bts.Length.ToString());
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + _Filename + ".xlsx");
                    HttpContext.Current.Response.BinaryWrite(bts);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                }
            }
            catch (ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>ExportFileNew"));
            }
            return ExcelLogFileName;
        }
        private static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                //MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        public static DataTable SearchStoreNumber(string Query,string StoreNumber)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = Query.Replace("@StoreNumber", StoreNumber);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetStoreNumber"));
            }
            return dtResult;
        }
        public static DataTable GetCustomHierarchyProducts(string UPC)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_CUSTOM_HIERARCHY_PRODUCTS).Replace("@UPC", UPC);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomHierarchyProducts"));
            }
            return dtResult;
        }
        public static DataTable GetCustomHierarchySearchProducts(string UPC)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_CUSTOM_HIERARCHY_SEARCH_PRODUCTS).Replace("@UPC", UPC);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomHierarchySearchProducts"));
            }
            return dtResult;
        }
        public static DataTable GetCustomHierarchyData(string Query)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                dtResult = DataManager.ExecuteQueryForNetezza(Query);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomHierarchyData"));
            }
            return dtResult;
        }
        public static int UpdateCustomHierarchyMaster(string HierarchyName,string HierarchyDesc,Int64 CustomHierarchyId)
        {
            IControlHelper helper = new IControlHelper();
            int Result = 0;
            string strQuery = string.Empty;
            try
            {
                strQuery = helper.GetResourceString(AppConstants.UPDATE_CUSTOM_HIERARCHY_MASTER).Replace("@HierarchyName", HierarchyName).Replace("@HierarchyDesc", HierarchyDesc).Replace("@CustomHierarchyId", CustomHierarchyId.ToString());
                Result = DataManager.ExecuteUpdateQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>UpdateCustomHierarchyMaster"));
            }
            return Result;
        }
        public static int UpdateCustomHierarchyProducts(string CustomLevel_1,string CustomLevel_2,string CustomLevel_3,string UPC,Int64 ProductID)
        {
            IControlHelper helper = new IControlHelper();
            int Result = 0;
            string strQuery = string.Empty;
            try
            {
                strQuery = helper.GetResourceString(AppConstants.UPDATE_CUSTOM_HIERARCHY_PRODUCTS).Replace("@CustomLevel_1", CustomLevel_1).Replace("@CustomLevel_2", CustomLevel_2).Replace("@CustomLevel_3", CustomLevel_3).Replace("@UPC", UPC).Replace("@ProductID", ProductID.ToString()) ;
                Result = DataManager.ExecuteUpdateQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>UpdateCustomHierarchyMaster"));
            }
            return Result;
        }
        public static DataTable GetOwnerUsersForCustomHierarchyMaster(string UserName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_OWNER_USER_CUSTOM_HIERARCHY_MASTER).Replace("@UserName", UserName);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetOwnerUsersForCustomHierarchyMaster"));
            }
            return dtResult;
        }
        public static DataTable GetCustomHierarchyDataOnCustomHierarchyID(string Query,Int64 CustomHierarchyID)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = Query.Replace("@CustomHierarchyID", CustomHierarchyID.ToString());
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomHierarchyData"));
            }
            return dtResult;
        }
        public static DataTable GetUPCFromProducts(string UPC)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_UPC_FROM_PRODUCTS).Replace("@UPC", UPC);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetUPCFromProducts"));
            }
            return dtResult;
        }
        public static DataTable GetCustomHierarchyMaster(string HierarchyName,string UserName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_CUSTOM_HIERARCHY_MASTER_ON_HIERARCHYNAME).Replace("@HierarchyName", HierarchyName).Replace("@UserName", UserName);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomHierarchyData"));
            }
            return dtResult;
        }
        public static DataTable GetCustomHierarchySharedOnUsername(Int64 CustomHierarchyID, string UserName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_CUSTOM_HIERARCHY_SHARED_ON_USERNAME).Replace("@CustomHierarchyID", CustomHierarchyID.ToString()).Replace("@UserName", UserName);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomHierarchySharedOnUsername"));
            }
            return dtResult;
        }
        public static DataTable GetTotalUPC(string HierarchyName,string ProductsUserName,string VendorName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_CUSTOM_HIERARCHY_TOTAL_UPC).Replace("@UPC", HierarchyName).Replace("@ProductsUserName", ProductsUserName).Replace("@VendorUserName",VendorName);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetTotalUPC"));
            }
            return dtResult;
        }
        public static DataTable GetCustomHierarchySearchData(string Query)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                dtResult = DataManager.ExecuteQueryForNetezza(Query);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomHierarchySearchData"));
            }
            return dtResult;
        }
        public static int DeleteCustomHierarchyMaster(Int64 CustomHierarchyID, string HierarchyDesc)
        {
            IControlHelper helper = new IControlHelper();
            int Result = 0;
            string strQuery = string.Empty;
            try
            {
                strQuery = helper.GetResourceString(AppConstants.DELETE_FROM_CUSTOM_HIERARCHY_SHARED).Replace("@CustomHierarchyID", CustomHierarchyID.ToString()).Replace("@UserName", HierarchyDesc);
                Result = DataManager.ExecuteDeleteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>DeleteCustomHierarchyMaster"));
            }
            return Result;
        }
        public static int InsertCustomHierarchyShared(Int64 CustomHierarchyID, string UserName, string DateShared)
        {
            IControlHelper helper = new IControlHelper();
            int Result = 0;
            string strQuery = string.Empty;
            try
            {
                strQuery = helper.GetResourceString(AppConstants.INSERT_CUSTOM_HIERARCHY_SHARED).Replace("@CustomHierarchyID", CustomHierarchyID.ToString()).Replace("@UserName", UserName).Replace("@DateShared", DateShared);
                Result = DataManager.ExecuteInsertQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>InsertCustomHierarchyShared"));
            }
            return Result;
        }
        public static DataTable GetCustomPromotions(Int64 CustomHierarchyID, string UserName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_CUSTOM_PROMOTIONS_ON_CUSTOMHIERARCHYID).Replace("@CustomHierarchyID", CustomHierarchyID.ToString()).Replace("@UserName", UserName);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetCustomPromotions"));
            }
            return dtResult;
        }
        public static DataTable SearchUPC(string UPC)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.SEARCH_FOR_UPC).Replace("@UPC", UPC);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>SearchUPC"));
            }
            return dtResult;
        }
        public static bool ValidExtension(string ext)
        {
            if (ext == ".xlsx" || ext == ".xls")
                return true;
            else
                return false;
        }
        public static string SaveFile(FileUpload flu, string PageName)
        {
            IControlHelper helper = new IControlHelper();
            string FileName = "";
            try
            {
                string savePath = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/");
                string fileType = PageName;

                string ext = Path.GetExtension(flu.FileName);
                string fName = flu.FileName;

                int length = flu.FileName.Replace(ext, string.Empty).Length;

                if (length > 50)
                    fName = flu.FileName.Replace(ext, string.Empty).Substring(0, 50) + ext;

                // Get the name of the file to upload.
                string fileName = DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + fileType + "_" + fName;
                //filename = filename.Replace(' ', '_');

                // Create the path and file name to check for duplicates.
                string pathToCheck = savePath + fileName;

                // Append the name of the file to upload to the path.
                savePath += fileName;

                // Call the SaveAs method to save the uploaded file to the specified directory.
                flu.SaveAs(savePath);
                FileName = savePath;
                //MessageBox.Show(status);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>SaveFile"));
            }
            return FileName;
        }
        public static DataTable GetUserAccessProducts(string UPC,string UserName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                String strQuery = helper.GetResourceString(AppConstants.GET_USER_ACCESS_PRODUCTS).Replace("@UPC", UPC).Replace("@UserName", UserName);
                dtResult = DataManager.ExecuteQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetUserAccessProducts"));
            }
            return dtResult;
        }
        public static DataTable StripEmptyRows(DataTable dt)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                List<int> rowIndexesToBeDeleted = new List<int>();
                int indexCount = 0;
                foreach (var row in dt.Rows)
                {
                    var r = (DataRow)row;
                    int emptyCount = 0;
                    int itemArrayCount = r.ItemArray.Length;
                    foreach (var i in r.ItemArray) if (string.IsNullOrEmpty(i.ToString().Trim())) emptyCount++;

                    if (emptyCount == itemArrayCount) rowIndexesToBeDeleted.Add(indexCount);

                    indexCount++;
                }

                int count = 0;
                foreach (var i in rowIndexesToBeDeleted)
                {
                    dt.Rows.RemoveAt(i - count);
                    count++;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>StripEmptyRows"));
            }
            return dt;
        }
        public static String SpecialCharactersValidation_NewValidation(String _stringToValidate, string controlname)
        {
            IControlHelper helper = new IControlHelper();
            String _message = String.Empty;
            try
            {
                //string[] stringToValidate = Regex.Split(_stringToValidate, string.Empty);
                string InvalidChars = "";
                string[] _RestrictedCharacters = { "&", ",", "<", ">", "'", "\\", "+", "%", "^" };
                foreach (string invalid in _RestrictedCharacters)
                {
                    if (_stringToValidate.Contains(invalid))
                    {
                        //_message += _message.Contains(invalid) ? "" : invalid + " ";
                        InvalidChars += invalid;
                    }
                }

                //_message = _message == string.Empty ? String.Empty : " & , < >  \' \\ + % ^ special characters are not allowed";
                _message = InvalidChars == string.Empty ? String.Empty : controlname + " cannot include the following character(s): " + InvalidChars;

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>SpecialCharactersValidation_NewValidation"));
            }
            return _message;
        }
        public static string GetSpecialChar(string stringToValidate)
        {
            IControlHelper helper = new IControlHelper();
            string InvalidChars = "";
            try
            {

                string[] _RestrictedCharacters = { "&", ",", "<", ">", "'", "\\", "+", "%", "^" };
                foreach (string invalid in _RestrictedCharacters)
                {
                    if (stringToValidate.Contains(invalid))
                    {
                        InvalidChars += invalid;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetSpecialChar"));
            }
            return InvalidChars;
        }
        public static DataTable ValidateExcelData_SpecialChars_Combined(DataTable dtExcelDataWithEmpty, CheckBox ChkUploadBrand = null, CheckBox ChkUpdateProduct = null, bool isProductHier = false, bool isEditHierarchy = false, DataTable dtHierarchyUPCs = null, bool isCustomStore = false)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtErrorLog = new DataTable();
            bool IsValid = false;
            string strUPCStore = "UPC";

            //For Custom Store Validations
            if (isCustomStore)
                strUPCStore = "Store Number";

            try
            {
                //Table to store errors

                dtErrorLog.Columns.Add("Row No", typeof(System.String));
                dtErrorLog.Columns.Add("Status", typeof(System.String));
                dtErrorLog.Columns.Add(strUPCStore, typeof(System.String));

                string AllInvalidChars = "";
                string CharReturn = "";

                DataTable dt = new DataTable();
                dt = dtExcelDataWithEmpty;
                bool CheckEmptyColumns = false;
                string strColumn1 = ""; //upc //level1
                string strColumn2 = ""; //brand //level2
                string strColumn3 = ""; //product size //level3
                string UPC = "";
                string prevUPC = "";
                bool upcInt = true;
                string invalidChar = "";
                DataTable dtDuplicateUPCs = new DataTable();
                dtDuplicateUPCs.Columns.Add("Row", typeof(System.Int32));
                dtDuplicateUPCs.Columns.Add("Status", typeof(System.String));
                dtDuplicateUPCs.Columns.Add(strUPCStore, typeof(System.String));
                ArrayList arrDuplicateUPCs = new ArrayList();
                Dictionary<int, string> listDuplicateUPCs = new Dictionary<int, string>();

                //Header validations ICINS-2195 
                int CounterRows = 0;
                string HeaderPosition = "";
                string HeaderValue = "";

                foreach (DataColumn dc in dt.Columns)
                {
                    if ((isProductHier && dc.Ordinal <= 2) || (!isProductHier && dc.Ordinal <= 3))
                    {
                        string cName = dc.Caption.ToString();

                        if (isProductHier)
                        {
                            switch (dc.Ordinal)
                            {
                                case 0:
                                    HeaderPosition = "A";
                                    HeaderValue = "UPC";
                                    break;
                                case 1:
                                    HeaderPosition = "B";
                                    HeaderValue = "Brand";
                                    break;
                                case 2:
                                    HeaderPosition = "C";
                                    HeaderValue = "Product Size Desc";
                                    break;
                            }
                        }
                        else
                        {
                            switch (dc.Ordinal)
                            {
                                case 0:
                                    HeaderPosition = "A";
                                    HeaderValue = "Custom Level 1";
                                    break;
                                case 1:
                                    HeaderPosition = "B";
                                    HeaderValue = "Custom Level 2";
                                    break;
                                case 2:
                                    HeaderPosition = "C";
                                    HeaderValue = "Custom Level 3";
                                    break;
                                case 3:
                                    HeaderPosition = "D";
                                    if (isCustomStore)
                                        HeaderValue = "Store Number";
                                    else
                                        HeaderValue = "UPC";
                                    break;
                            }
                        }

                        if (cName.Contains("F" + (dc.Ordinal + 1)) || cName.Trim() != HeaderValue)
                        {
                            DataRow dr = dtErrorLog.NewRow();
                            dr["Row No"] = "Header";
                            dr["Status"] = "Column Header " + HeaderPosition + " is incorrect, it must say \"" + HeaderValue + "\"";
                            dr[strUPCStore] = "";
                            dtErrorLog.Rows.Add(dr);
                            CounterRows += 1;
                        }
                    }
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (isProductHier)
                    {
                        strColumn1 = dt.Rows[i][0].ToString().Trim();
                        strColumn2 = dt.Rows[i][1].ToString().Trim();
                        strColumn3 = dt.Rows[i][2].ToString().Trim();

                        UPC = strColumn1.Trim();
                        upcInt = UPC.All(char.IsDigit);

                        if ((strColumn1 == "" || strColumn1 == null) && (strColumn2 == "" || strColumn2 == null) && (strColumn3 == "" || strColumn3 == null))
                        {
                            CheckEmptyColumns = true;
                        }
                    }
                    else
                    {
                        strColumn1 = dt.Rows[i][0].ToString();
                        strColumn2 = dt.Rows[i][1].ToString();
                        strColumn3 = dt.Rows[i][2].ToString();

                        UPC = dt.Rows[i][3].ToString().Trim();
                        upcInt = UPC.All(char.IsDigit);

                        if ((strColumn1 == "" || strColumn1 == null) && (strColumn2 == "" || strColumn2 == null) && (strColumn3 == "" || strColumn3 == null) && (UPC == "" || UPC == null))
                        {
                            CheckEmptyColumns = true;
                        }
                    }

                    if (CheckEmptyColumns)
                    {
                        CheckEmptyColumns = false;
                    }
                    else
                    {
                        // valide for null value
                        if ((strColumn1 == "" || strColumn1 == null) && isProductHier == false)
                        {
                            DataRow dr = dtErrorLog.NewRow();
                            dr["Row No"] = i + 1;
                            dr["Status"] = IcontrolFilterResources.Custom1LevelRequired;
                            dr[strUPCStore] = "";
                            dtErrorLog.Rows.Add(dr);
                        }
                        else if (strColumn1 != "" && isProductHier == false)
                        {
                            invalidChar = SpecialCharactersValidation_NewValidation(strColumn1, "Custom Level 1");

                            CharReturn = GetSpecialChar(strColumn1);
                            if (CharReturn != "")
                            {
                                if (!AllInvalidChars.Contains(CharReturn))
                                {
                                    AllInvalidChars += CharReturn;
                                }
                            }

                            if (invalidChar != "")
                            {
                                DataRow dr = dtErrorLog.NewRow();
                                dr["Row No"] = i + 1;
                                dr["Status"] = invalidChar;
                                dr[strUPCStore] = "";
                                dtErrorLog.Rows.Add(dr);
                            }
                        }

                        if ((strColumn2 == "" || strColumn2 == null) && isProductHier == false)
                        {
                            DataRow dr = dtErrorLog.NewRow();
                            dr["Row No"] = i + 1;
                            dr["Status"] = IcontrolFilterResources.Custom2LevelRequired;
                            dr[strUPCStore] = "";
                            dtErrorLog.Rows.Add(dr);
                        }
                        else if (strColumn2 != "" && isProductHier == false)
                        {
                            invalidChar = SpecialCharactersValidation_NewValidation(strColumn2, "Custom Level 2");
                            CharReturn = GetSpecialChar(strColumn2);
                            if (CharReturn != "")
                            {
                                if (!AllInvalidChars.Contains(CharReturn))
                                {
                                    AllInvalidChars += CharReturn;
                                }
                            }
                            if (invalidChar != "")
                            {
                                DataRow dr = dtErrorLog.NewRow();
                                dr["Row No"] = i + 1;
                                dr["Status"] = invalidChar;
                                dr[strUPCStore] = "";
                                dtErrorLog.Rows.Add(dr);
                            }
                        }

                        if ((strColumn3 == "" || strColumn3 == null) && isProductHier == false)
                        {
                            DataRow dr = dtErrorLog.NewRow();
                            dr["Row No"] = i + 1;
                            dr["Status"] = IcontrolFilterResources.Custom3LevelRequired;
                            dr[strUPCStore] = "";
                            dtErrorLog.Rows.Add(dr);
                        }
                        else if (strColumn3 != "" && isProductHier == false)
                        {
                            invalidChar = SpecialCharactersValidation_NewValidation(strColumn3, "Custom Level 3");
                            CharReturn = GetSpecialChar(strColumn3);
                            if (CharReturn != "")
                            {
                                if (!AllInvalidChars.Contains(CharReturn))
                                {
                                    AllInvalidChars += CharReturn;
                                }
                            }
                            if (invalidChar != "")
                            {
                                DataRow dr = dtErrorLog.NewRow();
                                dr["Row No"] = i + 1;
                                dr["Status"] = invalidChar;
                                dr[strUPCStore] = "";
                                dtErrorLog.Rows.Add(dr);
                            }
                        }

                        if (UPC == "" || UPC == null)
                        {
                            DataRow dr = dtErrorLog.NewRow();
                            dr["Row No"] = i + 1;
                            dr["Status"] = strUPCStore + " is a required field and cannot be blank";
                            dr[strUPCStore] = "";
                            dtErrorLog.Rows.Add(dr);
                        }
                        else if (!upcInt)
                        {
                            DataRow dr = dtErrorLog.NewRow();
                            dr["Row No"] = i + 1;
                            dr["Status"] = "Invalid " + strUPCStore;
                            dr[strUPCStore] = UPC;
                            dtErrorLog.Rows.Add(dr);
                        }
                        else
                        {
                            // validate UPC value
                            string RecordID = "", sql;

                            if (isCustomStore)
                            {
                                sql = helper.GetResourceString(AppConstants.GET_STORE_NUMBER).Replace("@StoreNumber", UPC.Trim().ToUpper().Replace("'", "''"));
                                RecordID = DataManager.ExecuteQueryForNetezza(sql).Rows[0][0].ToString();
                            }
                            else
                            {
                                if (DataManager.IsCorporateUser(HttpContext.Current.Session["UserName"].ToString()))
                                {
                                    RecordID = GetUPCFromProducts(UPC.Trim().ToUpper().Replace("'", "''")).Rows[0][0].ToString(); 
                                }
                                else
                                {
                                    sql = helper.GetResourceString(AppConstants.GET_FROM_USER_ACCESS_PRODUCTS).Replace("@UserName", HttpContext.Current.Session["UserName"].ToString()).Replace("@UPC", UPC.Trim().Replace("'", "''"));
                                    RecordID = DataManager.ExecuteQueryForNetezza(sql).Rows[0][0].ToString(); 
                                }
                            }

                            if (RecordID == "")
                            {
                                DataRow dr = dtErrorLog.NewRow();
                                dr["Row No"] = i + 1;
                                dr["Status"] = "Invalid " + strUPCStore;
                                dr[strUPCStore] = UPC;
                                dtErrorLog.Rows.Add(dr);
                            }
                        }

                        if (isProductHier == true)
                        {
                            if ((strColumn2 == "" || strColumn2 == null) && ChkUploadBrand.Checked == true && isProductHier == true)
                            {
                                DataRow dr = dtErrorLog.NewRow();
                                dr["Row No"] = i + 1;
                                dr["Status"] = IcontrolFilterResources.BrandRequired;
                                dr[strUPCStore] = "";
                                dtErrorLog.Rows.Add(dr);
                            }
                            else if (strColumn2 != "" && ChkUploadBrand.Checked == true && isProductHier == true)
                            {
                                invalidChar = SpecialCharactersValidation_NewValidation(strColumn2, "Brand");
                                CharReturn = GetSpecialChar(strColumn2);
                                if (CharReturn != "")
                                {
                                    if (!AllInvalidChars.Contains(CharReturn))
                                    {
                                        AllInvalidChars += CharReturn;
                                    }
                                }

                                if (invalidChar != "")
                                {
                                    DataRow dr = dtErrorLog.NewRow();
                                    dr["Row No"] = i + 1;
                                    dr["Status"] = invalidChar;
                                    dr[strUPCStore] = "";
                                    dtErrorLog.Rows.Add(dr);
                                }
                            }

                            if ((strColumn3 == "" || strColumn3 == null) && ChkUpdateProduct.Checked == true && isProductHier == true)
                            {
                                DataRow dr = dtErrorLog.NewRow();
                                dr["Row No"] = i + 1;
                                dr["Status"] = IcontrolFilterResources.ProductSizeRequired;
                                dr[strUPCStore] = "";
                                dtErrorLog.Rows.Add(dr);
                            }
                            else if (strColumn3 != "" && ChkUploadBrand.Checked == true && isProductHier == true)
                            {
                                invalidChar = SpecialCharactersValidation_NewValidation(strColumn3, "Product Size Desc");

                                CharReturn = GetSpecialChar(strColumn3);
                                if (CharReturn != "")
                                {
                                    if (!AllInvalidChars.Contains(CharReturn))
                                    {
                                        AllInvalidChars += CharReturn;
                                    }
                                }

                                if (invalidChar != "")
                                {
                                    DataRow dr = dtErrorLog.NewRow();
                                    dr["Row No"] = i + 1;
                                    dr["Status"] = invalidChar;
                                    dr[strUPCStore] = "";
                                    dtErrorLog.Rows.Add(dr);
                                }
                            }
                        }
                        // Check Duplicate UPC Start
                        if (UPC != "")
                        {
                            int columnNumber;

                            if (isProductHier)
                                columnNumber = 0;
                            else
                                columnNumber = 3;

                            var rows = from row in dt.AsEnumerable()
                                       where row.Field<string>(columnNumber) == Convert.ToString(UPC)
                                       select row;

                            int count = rows.Count<DataRow>();
                            if (count > 1 && listDuplicateUPCs.ContainsValue(UPC) == false)
                            {
                                DataRow dr1 = dtErrorLog.NewRow();

                                dr1["Row No"] = i + 1;
                                dr1["Status"] = "Duplicate " + strUPCStore + " in Excel";
                                dr1[strUPCStore] = UPC;
                                dtErrorLog.Rows.Add(dr1);
                            }

                            if (listDuplicateUPCs.ContainsValue(UPC))
                            {
                                DataRow dr = dtErrorLog.NewRow();
                                dr["Row No"] = i + 1;
                                dr["Status"] = "Duplicate " + strUPCStore + " in Excel";
                                dr[strUPCStore] = UPC;
                                dtErrorLog.Rows.Add(dr);
                            }
                            else
                            {
                                listDuplicateUPCs.Add(i + 1, UPC);
                            }
                            prevUPC = UPC;
                        }
                        else
                        {
                            listDuplicateUPCs.Add(i + 1, UPC);
                        }
                        // Check Duplicate UPC End

                        //Check already existing UPCs in case of Edit Hierarchy
                        if (isEditHierarchy && dtHierarchyUPCs != null)
                        {
                            if (isCustomStore)
                            {
                                DataRow[] upcExist = dtHierarchyUPCs.Select("STORE_NUMBER='" + UPC + "' and Status <> 'D' ");
                                if (upcExist.Length > 0)
                                {
                                    DataRow dr = dtErrorLog.NewRow();
                                    dr["Row No"] = i + 1;
                                    dr["Status"] = IcontrolFilterResources.StoreNumberExist;
                                    dr["Store Number"] = UPC;
                                    dtErrorLog.Rows.Add(dr);
                                }
                            }
                            else
                            {
                                DataRow[] upcExist = dtHierarchyUPCs.Select("UPC='" + UPC + "' and Status <> 'D' ");
                                if (upcExist.Length > 0)
                                {
                                    DataRow dr = dtErrorLog.NewRow();
                                    dr["Row No"] = i + 1;
                                    dr["Status"] = IcontrolFilterResources.UPCAlreadyExists;
                                    dr["UPC"] = UPC;
                                    dtErrorLog.Rows.Add(dr);
                                }
                            }
                        }
                    }
                }
                if (AllInvalidChars != "")
                    HttpContext.Current.Session["SpecialChars"] = AllInvalidChars;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>ValidateExcelData_SpecialChars_Combined"));
            }
            return dtErrorLog;
        }
        public static string GetExcelData(string FileName, string SheetName, string strPageName, string tablewithEmptyName, string tmpExcelTable, bool IsProductHierarchy = false)
        {
            string strResult = "";
            try
            {
                string connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";";
                connectionstring += "Extended Properties=Excel 12.0;";

                bool Upload = false;

                if (SheetName == "Fail")
                {
                    //return "No file found.";
                }

                DataSet ds = new DataSet();
                using (OleDbConnection con = new System.Data.OleDb.OleDbConnection(connectionstring))
                {
                    con.Open();
                    OleDbDataAdapter cmd = new System.Data.OleDb.OleDbDataAdapter("select * from [" + SheetName + "$" + "]", con);
                    cmd.Fill(ds);
                    con.Close();
                }


                foreach (DataColumn col in ds.Tables[0].Columns)
                    col.ColumnName = col.ColumnName.Trim();


                DataTable dtCloned = ds.Tables[0].Clone();
                dtCloned.Columns[0].DataType = typeof(string);
                dtCloned.Columns[1].DataType = typeof(string);
                dtCloned.Columns[2].DataType = typeof(string);

                if (!IsProductHierarchy)
                {
                    dtCloned.Columns[3].DataType = typeof(string);
                }

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    dtCloned.ImportRow(row);
                }


                HttpContext.Current.Session[tablewithEmptyName] = dtCloned;


                DataTable copyDataTable = dtCloned.Copy();
                DataTable dtWithoutEmptyRows = AppManager.StripEmptyRows(copyDataTable);
                if (dtWithoutEmptyRows.Rows.Count == 0)
                {
                    strResult = "Fail";
                    MessageBox.Show(IcontrolFilterResources.NoRecordFound);
                }

                HttpContext.Current.Session[tmpExcelTable] = dtWithoutEmptyRows;
            }
            catch (Exception ex)
            {
                MessageBox.Show(IcontrolFilterResources.FileUploadError);
                strResult = "Fail";
            }
            return strResult;
        }
        public static int UpdateSavedReports(string ReportName, string ReportDescription, string DateRange, string EmailID, string ScheduleID, DateTime? EndDate, string ReportID)
        {
            IControlHelper helper = new IControlHelper();
            int Result = 0;
            string strQuery = string.Empty;
            try
            {
                strQuery = helper.GetResourceString(AppConstants.UPDATE_SAVED_REPORTS).Replace("@ReportName", ReportName).Replace("@ReportDescription", ReportDescription).Replace("@DateRange", DateRange).Replace("@EmailID", EmailID).Replace("@ScheduleID", ScheduleID).Replace("@EndDate", EndDate.ToString()).Replace("@ReportID", ReportID);
                Result = DataManager.ExecuteUpdateQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>UpdateSavedReports"));
            }
            return Result;
        }
        public static int InserSavedReports(string ReportName, string ReportDescription, string Username, string MenuId, string TableauURL, string ReportParameters, string DateRange,string OriginalReportName,string UserProfile,string shareby,string ShareMessage)
        {
            IControlHelper helper = new IControlHelper();
            int Result = 0;
            string strQuery = string.Empty;
            try
            {
                strQuery = helper.GetResourceString(AppConstants.INSERT_SAVED_REPORTS).Replace("@ReportName", ReportName).Replace("@ReportDescription", ReportDescription).Replace("@Username", Username).Replace("@MenuId", MenuId).Replace("@TableauURL", TableauURL).Replace("@ReportParameters", ReportParameters.ToString()).Replace("@DateRange", DateRange).Replace("@OriginalReportName", OriginalReportName).Replace("@UserProfile", UserProfile).Replace("@shareby", shareby).Replace("@ShareMessage", ShareMessage);
                Result = DataManager.ExecuteInsertQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>InserSavedReports"));
            }
            return Result;
        }
        public static DataTable GetMarketBasketRequest(string Query)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                dtResult = DataManager.ExecuteQueryForNetezza(Query);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetMarketBasketRequest"));
            }
            return dtResult;
        }
        public static DataTable GetMarketBasketOnBasketID(string BasketID)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                string str = helper.GetResourceString(AppConstants.GET_MARKET_BASKET_REQUEST_ON_BASKETID).Replace("@BasketID", BasketID);
                dtResult = DataManager.ExecuteQueryForNetezza(str);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>GetMarketBasketRequest"));
            }
            return dtResult;
        }
        public static int InsertIntoMarketBasketRequest(string p_DRIVER_LEVEL, string p_DRIVER_VALUE_LIST, string p_OUTPUT_LEVEL, string p_BANNER_LIST, string p_VENDOR_LIST, string p_FROM_DATE, string p_TO_DATE, string p_MIN_BASKET_SIZE, string p_MAX_BASKET_SIZE, string p_EXEC_IDENTIFIER, string p_SUBMISSION_DATETIME,string p_USERNAME,string p_BASKET_DESCRIPTION,string p_REWARD_TYPE,string p_CENSUS_FILTER,bool p_All_UPC)
        {
            IControlHelper helper = new IControlHelper();
            int Result = 0;
            string strQuery = string.Empty;
            try
            {
                strQuery = helper.GetResourceString(AppConstants.INSERT_INTO_MARKET_BASKET_REQUESTS).Replace("@p_DRIVER_LEVEL", p_DRIVER_LEVEL).Replace("@p_DRIVER_VALUE_LIST", p_DRIVER_VALUE_LIST).Replace("@p_OUTPUT_LEVEL", p_OUTPUT_LEVEL).Replace("@p_BANNER_LIST", p_BANNER_LIST).Replace("@p_VENDOR_LIST", p_VENDOR_LIST).Replace("@p_FROM_DATE", p_FROM_DATE.ToString()).Replace("@p_TO_DATE", p_TO_DATE.ToString()).Replace("@p_MIN_BASKET_SIZE", p_MIN_BASKET_SIZE).Replace("@p_MAX_BASKET_SIZE", p_MAX_BASKET_SIZE).Replace("@p_EXEC_IDENTIFIER", p_EXEC_IDENTIFIER).Replace("@p_SUBMISSION_DATETIME", p_SUBMISSION_DATETIME).Replace("@p_USERNAME", p_USERNAME).Replace("@p_BASKET_DESCRIPTION", p_BASKET_DESCRIPTION).Replace("@p_REWARD_TYPE", p_REWARD_TYPE).Replace("@p_CENSUS_FILTER", p_CENSUS_FILTER).Replace("@p_All_UPC", p_All_UPC.ToString());
                Result = DataManager.ExecuteInsertQueryForNetezza(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "AppManager.cs=>InsertIntoMarketBasketRequest"));
            }
            return Result;
        }
        #endregion
    }
}