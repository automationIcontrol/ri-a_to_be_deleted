﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnderMaintenance.aspx.cs" Inherits="iControlGenricFilters.UnderMaintenance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="stylesheets/all.css" rel="stylesheet" type="text/css" />
    <style>
        #NewsContent {
            float: left;
            width: 90%;
            padding: 20px;
        }

        .post {
            margin-bottom: 10px;
        }

        .post-alt {
        }

        .post .title {
            height: 30px;
            padding: 12px 0 0 0px;
            font-size: 36px;
            color: #0078CA;
        }

            .post .title a {
                border: none;
                color: #0078CA;
            }

        .post .meta {
            margin-bottom: 30px;
            padding: 5px 0px 0px 0px;
            text-align: left;
            font-weight: normal;
        }

            .post .meta .date {
                float: left;
            }

            .post .meta .posted {
                float: right;
            }

            .post .meta a {
                color: #0078CA;
            }

        .post .entry {
            text-align: justify;
            font-size: 20px;
            line-height: 20px;
        }

        .links {
            display: inline-block;
            font-size: 14px;
            font-weight: normal;
            color: #1C1C1C;
            margin-bottom: 10px;
        }

        .button {
            padding: 5px 15px;
            background: #0084C4;
            border-radius: 5px;
            letter-spacing: 1px;
            font-size: 14px;
            color: #FFFFFF;
        }

        .PostedBy {
            color: #0078ca;
        }

        #Logos {
            width: 98%;
            padding: 10px;
            float: left;
            margin: 10px 0 10px 0;
        }

        .LeftLogo {
            width: 200px;
            height: 66px;
            float: left;
        }

        .RightLogo {
            width: 200px;
            height: 66px;
            float: right;
        }

        .date {
            font-size: 16px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content1" id="content" style="top: 0;">
            <div id="pageContent" class="container">
                <div id="layout" style="float: left; border: 0;">
                    <div class="container beige-gradient" style="top: 0px;">
                        <div id="Logos">
                            <img id="logoLeft" alt="Logo" src="Images/NewsUpdates_left_logo.png" class="LeftLogo" />
                            <img id="logoRight" alt="Logo" src="Images/NewsUpdates_right_logo.png" class="RightLogo" />
                        </div>
                        <div id="NewsContent">
                            <div class="post">
                                <h2 class="title">
                                    <span class="title"><%=Title%></span>
                                </h2>
                                <div class="entry">
                                    <p><span><%=ContentPart1%></span></p>
                                    <p><span><%=ContentPart2%></span></p>
                                    <p><%=ContentPart3%></p>
                                    <p><%=ThankYou%></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>

