﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using System.Configuration;
using System.Threading;
using System.Data.SqlClient;
using iControlDataLayer;
using iControlGenricFilters.ResourceFiles;

namespace iControlGenricFilters
{
    public partial class Login : System.Web.UI.Page
    {
        String strPageName = "Login.aspx";
        public static string defaultPage = ConfigurationManager.AppSettings["DefaultPageURL"].ToString();
        IControlHelper helper = new IControlHelper();
        public string HtmlContent_AreYouSure = IcontrolFilterResources.AreYouSureWantToContinue;
        public string HtmlContent_PopupContent = IcontrolFilterResources.LoginPopupContents;

        public dynamic SessionUser
        {
            get
            {
                return (iControlGenricFilters.Login)this.Session["User"];
            }
            set
            {
                this.Session["User"] = value;
            }
        }
        public string LoginUserName
        {
            get
            {
                return Convert.ToString(this.Session["UserName"]);
            }
            set
            {
                this.Session["UserName"] = value;
            }
        }

        public string GUID
        {
            get
            {
                return Convert.ToString(this.Session["GUID"]);
            }
            set
            {
                this.Session["GUID"] = value;
            }
        }

        private string ApplicationMode
        {
            get
            {
                return ConfigurationManager.AppSettings["ApplicationMode"].ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Session.RemoveAll();
                Session.Abandon();

                RedirectToHTTPs();

                //Check if request failed while loggin from Harmony
                if (Request.QueryString["msg"] != null)
                {
                    string msg = Convert.ToString(Request.QueryString["msg"]);
                    Lbl_error.Text = msg;
                }
            }
            // Check if site is Down or not
            if (!DataManager.IsSiteDown())
            {
                string ApplicationFor = ConfigurationManager.AppSettings["ApplicationFor"].ToString();
                if (ApplicationFor == "Sysco")
                {
                    imgLogo.ImageUrl = "~/themeimages/Logo_Sysco.png";
                }
                else if (ApplicationFor == "SV")
                {
                    imgLogo.ImageUrl = "~/themeimages/Logo_SV_v2.png";
                }
            }
            else
            {
                //need to change in web config file for default page for site down
                Response.Redirect(defaultPage);
            }
        }
        /// <summary>
        /// redirect to login page
        /// </summary>
        protected void RedirectToHTTPs()
        {
            try
            {
                string CheckUrl = HttpContext.Current.Request.Url.ToString();
                if (CheckUrl.Contains("172.16.100.83"))
                    Response.Redirect("https://ridev.icucsolutions.com/Login.aspx");
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Login.aspx.cs > RedirectToHTTPs:"));
            }

        }
        protected bool UnderMaintenance()
        {
            bool rslt = false;
            string SiteUnderMaintenance = ConfigurationManager.AppSettings["SiteUnderMaintenance"].ToString();
            if (SiteUnderMaintenance == "Yes")
            {
                rslt = !DataManager.CheckUnderMaintenanceUsers(this.LoginUserName);
            }
            return rslt;
        }
        /// <summary>
        /// check login by username and password
        /// </summary>
        protected void CheckLogin(string strUserName, string strPassword)
        {
            try
            {


                if (DataManager.ValidateUser(strUserName, strPassword))
                {
                    if (DataManager.IsPortalExistForUser(strUserName))
                    {
                        Session.Add("UserName", strUserName.Replace("'", "''").ToLower());
                        Session.Add("Pwd", strPassword.Replace("'", "''"));
                        Session.Add("StartPage", strPageName);

                        // add user log information
                        DataManager.SaveUserActivity(strUserName.ToString(), "0", "Login", Session.SessionID);

                        Session.Add("LockStatus", "0");

                        FormsAuthentication.SetAuthCookie(strUserName, true);

                        if (UnderMaintenance())
                        {
                            Response.Redirect("UnderMaintenance.aspx");
                        }
                        else
                        {
                            string Homepageurl = GetHomeUrl(strUserName);
                            if (Homepageurl.Contains("?"))
                                Homepageurl += "&ShowSplash=true";
                            else
                                Homepageurl += "?ShowSplash=true";
                            Response.Redirect(Homepageurl);

                        }
                    }
                    else
                    {
                        Session.Add("LockStatus", "0");
                        Lbl_error.Text = IcontrolFilterResources.InvaliCredentials_Msg;
                    }
                }
                else
                {
                    if (DataManager.IsUserLocked(strUserName, strPassword))
                    {
                        Session.Add("LockStatus", "1");
                        Lbl_error.Text = IcontrolFilterResources.AccountLocked_Msg;
                    }
                    else
                    {
                        Session.Add("LockStatus", "0");
                        Lbl_error.Text = IcontrolFilterResources.InvaliCredentials_Msg;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Login.aspx.cs > CheckLogin"));
            }
        }
        /// <summary>
        /// get home page url
        /// </summary>
        protected string GetHomeUrl(string UserName)
        {
            string url = "";
            try
            {
                this.LoginUserName = UserName.ToLower();
                url = DataManager.GetHomeUrl_Login(this.LoginUserName, defaultPage, Context.Request.ApplicationPath);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Login.aspx.cs  > GetHomeUrl:"));
            }
            return url;
        }
        /// <summary>
        /// click event of btnLogin
        /// </summary>
        protected void btnLogin_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (chkAccept.Checked == false)
                {
                    Lbl_error.Text = IcontrolFilterResources.TearmAndConditionsText;
                }

                else if (!string.IsNullOrEmpty(this.ApplicationMode) && (this.ApplicationMode.Contains("Beta") || this.ApplicationMode.Contains("Live")))
                {
                    CheckLogin(txtusname.Text, txtpass.Text);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Login.aspx.cs > btnLogin_Click:"));
            }
        }
        /// <summary>
        /// click event of btnContinue
        /// </summary>
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            try
            {
                mpeConfirm.Hide();

                string strUserName = txtusname.Text.Trim();

                //update the lst entry
                DataManager.UpdateUserLogInActivity(strUserName.ToString(), "", 1);

                //add a new entry
                this.GUID = DataManager.SaveUserLogInActivity(strUserName.ToString());

                // add user log information
                DataManager.SaveUserActivity(strUserName.ToString(), "0", "Login", Session.SessionID);

                Session.Add("LockStatus", "0");

                FormsAuthentication.SetAuthCookie(strUserName, true);
                string Homepageurl = GetHomeUrl(strUserName);
                if (Homepageurl.Contains("?"))
                    Homepageurl += "&ShowSplash=true";
                else
                    Homepageurl += "?ShowSplash=true";

                Response.Redirect(Homepageurl);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Login.aspx.cs > btnContinue_Click:"));
            }
        }

    }
}