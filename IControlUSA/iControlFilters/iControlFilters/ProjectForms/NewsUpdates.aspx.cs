﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using iControlDataLayer;

namespace iControlGenricFilters.ProjectForms
{
    public partial class NewsUpdates : System.Web.UI.Page
    {
        IControlHelper helper = new IControlHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                topHeader.InnerText = DataManager.GetPageTitle(Request.QueryString["mid"] != null ? Request.QueryString["mid"].ToString() : "", Session["ActiveMenuId"] != null ? Session["ActiveMenuId"].ToString() : "");

                BindNewsUpdates();
                BindQuickLinks();
            }
        }

        #region Bind Methods
        /// <summary>
        /// Bind news updates
        /// </summary>
        protected void BindNewsUpdates()
        {
          try
            {
                using (DataTable dt = DataManager.GetSearchResults())
                {
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            rptNewsUpdates.DataSource = dt;
                            rptNewsUpdates.DataBind();
                            lblMsg.Visible = false;
                            rptNewsUpdates.Visible = true;
                        }
                        else
                        {
                            lblMsg.Visible = true;
                            rptNewsUpdates.Visible = false;
                        }
                    }
                }

              
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "NewsUpdates.aspx.cs > BindNewsUpdates: "));
            }
        }
        /// <summary>
        /// Bind Quick links
        /// </summary>
        protected void BindQuickLinks()
        {
            try
            {
                using (DataTable dt = DataManager.GetSearchResultsQuickLinks())
                {

                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            string strHTML = "<ul>";
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                strHTML += "<li><a href=\"" + Convert.ToString(dt.Rows[i]["TargetUrl"]) + "\" target=\"" +
                                    Convert.ToString(dt.Rows[i]["TargetWindow"]) + "\">" + Convert.ToString(dt.Rows[i]["Title"]) + "</a></li>";
                            }

                            strHTML += "</ul>";

                            divQuickLinks.InnerHtml = strHTML;
                            divQuickLinks.Visible = true;
                        }
                        else
                        {
                            divQuickLinks.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "NewsUpdates.aspx.cs > BindQuickLinks: "));
            }
        }

        #endregion

        #region Fill Methods


        #endregion

        #region Cick Methods
        /// <summary>
        /// click of link video
        /// </summary>
        protected void lnkVideo_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["mid"] != null)
                {
                    string strMID = Request.QueryString["mid"].ToString();
                    DataManager.SaveUserActivity(Convert.ToString(Session["UserName"]), strMID, "SVInsights January 2017 Release Training", Session.SessionID);
                }
                mpeViewVideo.Show();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "NewsUpdates.aspx.cs > lnkVideo_Click: "));
            }
        }

        #endregion

        #region Repeater Events
        /// <summary>
        /// Iemdatabound event click of rptNewsUpdates
        /// </summary>
        protected void rptNewsUpdates__ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnIsSummaryDescSame = (HiddenField)e.Item.FindControl("hdnIsSummaryDescSame");
                    HyperLink btnReadMore = (HyperLink)e.Item.FindControl("btnReadMore");
                    if (hdnIsSummaryDescSame.Value == "1")
                        btnReadMore.Visible = false;
                    else
                        btnReadMore.Visible = true;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "NewsUpdates.aspx.cs > rptNewsUpdates__ItemDataBound: "));
            }
        }
        #endregion
    }
}