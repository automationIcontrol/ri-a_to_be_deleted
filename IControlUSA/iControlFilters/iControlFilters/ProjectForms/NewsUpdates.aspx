﻿<%@ Page Title="News Updates" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="NewsUpdates.aspx.cs" Inherits="iControlGenricFilters.ProjectForms.NewsUpdates" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/MyDate.ascx" TagName="MyDate" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
        <script type="text/javascript">

        $('.lnkVideo').live("click", function () {

            eval($('.myLink').attr('href')); //for 2017 traning video
        });

        $(document).ready(function () {
            $('#TrainingVideo').load(function () {
                $('#divVideo').removeClass('holds-the-iframe'); //display loading while iframe loads
            });
        });
        function cancelClick() {
            $('iframe').removeAttr('src'); //remove video when close popup
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="divMain" runat="server">
        <div id="contentHeader">
            <h1 id="topHeader" runat="server">
            </h1>
        </div>
        <asp:Panel ID="Panel1" runat="Server" GroupingText="" Width="100%" BackColor="transparent">
            <div id="layout" style="float: left;">
                <div class="beige-gradient" style="top: 0px;">
                    <div id="Logos">
                        <img id="logoLeft" alt="" src="../Images/NewsUpdates_left_logo.png" class="LeftLogo" />
                        <img id="logoRight" alt="" src="../Images/NewsUpdates_right_logo_v2.png" class="RightLogo" />
                    </div>
                    <div class="col-sm-9" id="NewsContent">
                        <asp:Repeater ID="rptNewsUpdates" runat="server" OnItemDataBound="rptNewsUpdates__ItemDataBound">
                            <ItemTemplate>
                                <div class="post">
                                    <h2 class="title">
                                        <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("News Title") %>' CssClass="title"></asp:Label>
                                        <span class="date">(<asp:Label ID="Label2" runat="server" Text='<%#Eval("Posted On") %>'
                                            Font-Size="16px"></asp:Label>
                                            )</span>
                                    </h2>
                                    <div class="clear"></div>
                                    <div class="entry">
                                        <asp:Label ID="lblNewsDescription" runat="server" Text='<%#Eval("News Summary") %>'></asp:Label>
                                        <p>
                                        </p>
                                        <p class="links">
                                            <asp:HyperLink ID="btnReadMore" runat="server" CssClass="button" Text="Read More"
                                                NavigateUrl='<%#"NewsUpdatesDetails.aspx?NewsId=" + Eval("NewsId") %>' />
                                        </p>
                                        <asp:HiddenField ID="hdnIsSummaryDescSame" runat="server" Value='<%#Eval("IsSummaryDescSame") %>' />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:Label ID="lblMsg" runat="server" Text="Currently there are no news/updates to display."
                            Visible="false"></asp:Label>
                        <br />
                        <!--cheesy button for the modal popups target control for 2017 traning video-->
                        <asp:Button ID="btnViewVideo" runat="Server" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender ID="mpeViewVideo" runat="server" TargetControlID="btnViewVideo"
                            PopupControlID="pnlViewVideo" BackgroundCssClass="fade" Drag="false" CancelControlID="closePopup"
                            OnCancelScript="cancelClick();">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel Style="display: none; width: 731px; height: 500px;" CssClass="modalPopup modalzindex"
                            ID="pnlViewVideo" runat="server">
                            <div class="changeclienttitleReports" width="100%" style="background: #515151 !important;
                                z-index: 99999 !important;">
                                <span id="Span4">SVInsights May 2017 Release Training</span>
                                <asp:HyperLink ID="closePopup" runat="server" CssClass="ClosePopupCls"><img src="../Images/cross.png" /></asp:HyperLink>
                            </div>
                            <div class="holds-the-iframe" id="divVideo">
                                <iframe id="TrainingVideo" width="100%" height="460px" src="//www.youtube.com/embed/4qGBGmGi9Gc"
                                    frameborder="0" allowfullscreen></iframe>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="col-sm-3" id="quicklinks">
                        <ul>
                            <li>
                                <h2>
                                    Quick Links</h2>
                                <div id="divQuickLinks" runat="server">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:LinkButton ID="lnk" runat="server" OnClick="lnkVideo_Click" CssClass="myLink"></asp:LinkButton>
    </div>
</asp:Content>
