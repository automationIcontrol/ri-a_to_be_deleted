﻿using iControlA;
using iControlDataLayer;
using iControlGenricFilters.LIB;
using iControlGenricFilters.ResourceFiles;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace iControlGenricFilters.ProjectForms
{
    public partial class EditCustomStoreGroup : System.Web.UI.Page
    {
        IControlHelper helper = new IControlHelper();

        public static string editStore = "Edit Store";
        public static string commandEdit = "EditStore";
        public static string commandDelete = "DeleteStore";
        public static string sessStoreAvaliable = "StoreAvailable";
        public static string sessStoreUsers = "CustomStoreUser";
        public static string sessDateCreated = "DATETIMECREATED";
        public static string sessTempTable = "tempdt";
        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    grdMembers.Visible = false;
                    grdMembers.Rebind();
                    hfstorenumber.Value = "0";
                    lblSpecialChars_Message.Text = AppManager.SpecialChars_Message();
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "EditCustomStoreGroup.cs > Page_Load"));
            }
        }
        #endregion

        #region Get Data
        public DataTable GetSearchResults()
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                dtResult = AppManager.GetCustomStoresShared(AppConstants.GET_SEARCH_RESULT_FOR_EDIT_CUSTOM_STORE_GROUP, Convert.ToInt32(Request.QueryString["id"]));
                if (dtResult.Rows.Count > 0)
                {
                    Session["CustomStoreUser"] = dtResult.Rows[0]["USERNAME"].ToString();
                    Session["DATETIMECREATED"] = dtResult.Rows[0]["DATETIMECREATED"];
                    if (!string.IsNullOrEmpty(dtResult.Rows[0]["CUSTOM_STORE_NAME"].ToString()))
                    {
                        topHeader.InnerText = dtResult.Rows[0]["CUSTOM_STORE_NAME"].ToString();
                        txtGroupName.Text = dtResult.Rows[0]["CUSTOM_STORE_NAME"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dtResult.Rows[0]["CUSTOM_STORE_DESCRIPTION"].ToString()))
                    {
                        txtStoreDescription.Text = dtResult.Rows[0]["CUSTOM_STORE_DESCRIPTION"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Timeout expired"))
                    iControlA.MessageBox.Show(IcontrolFilterResources.SearchTimeOut);
                else
                helper.WriteLog(string.Format("{0}" + ex.Message, "EditCustomStoreGroup.cs > GetSearchResults"));
            }
            return dtResult;
        }

        #endregion
        #region Grid Methods
        protected void grdMembers_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = GetSearchResults();
                if (dt != null)
                {
                    if (dt.Columns.Count > 0)
                    {
                        if (Session["tempdt"] == null)
                        {
                            Session["tempdt"] = dt;
                        }
                        else
                        {
                            dt = Session["tempdt"] as DataTable;
                        }

                        grdMembers.DataSource = dt.Select("Status <>'D' ");
                        grdMembers.Visible = true;
                    }
                    else
                    {
                        grdMembers.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "EditCustomStoreGroup.cs > grdMembers_NeedDataSource"));
            }
        }
        protected void grdMembers_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == commandEdit)
                {
                    Session["StoreAvailable"] = "Yes";
                    Btnupdate.Text = "Update";
                    lbleditMessage.Text = editStore;
                    mpeRequest.Show();
                    string StoreId = e.CommandArgument.ToString();
                    hfstoreid.Value = e.CommandArgument.ToString();
                    DataTable dt = new DataTable();
                    dt = Session["tempdt"] as DataTable;

                    DataRow[] drr = dt.Select("STORE_NUMBER=' " + hfstoreid.Value + " ' ");
                    for (int i = 0; i < drr.Length; i++)
                    {
                        hfstorenumber.Value = drr[i]["STORE_NUMBER"].ToString();
                        HfNewStoreid.Value = drr[i]["Old_StoreId"].ToString();
                        if (!string.IsNullOrEmpty(drr[i]["CUSTOM_LEVEL_1"].ToString()))
                        {
                            txtLevel1.Text = drr[i]["CUSTOM_LEVEL_1"].ToString();
                        }

                        if (!string.IsNullOrEmpty(drr[i]["CUSTOM_LEVEL_2"].ToString()))
                        {
                            txtLevel2.Text = drr[i]["CUSTOM_LEVEL_2"].ToString();
                        }

                        if (!string.IsNullOrEmpty(drr[i]["CUSTOM_LEVEL_3"].ToString()))
                        {
                            txtlevel3.Text = drr[i]["CUSTOM_LEVEL_3"].ToString();
                        }

                        if (!string.IsNullOrEmpty(drr[i]["STORE_NUMBER"].ToString()))
                        {
                            txtstore.Text = drr[i]["STORE_NUMBER"].ToString();
                        }
                        if (!string.IsNullOrEmpty(drr[i]["STORE_NAME"].ToString()))
                        {
                            lblStoreName.Text = drr[i]["STORE_NAME"].ToString();
                        }
                        else
                        {
                            lblStoreName.Text = "";
                        }
                        if (!string.IsNullOrEmpty(drr[i]["STORE_LEGACY_NUMBER"].ToString()))
                        {
                            LblLegacyNo.Text = drr[i]["STORE_LEGACY_NUMBER"].ToString();
                        }
                        else
                        {
                            LblLegacyNo.Text = "";
                        }
                    }
                }
                if (e.CommandName == commandDelete)
                {
                    int Store_Number = Convert.ToInt32(e.CommandArgument);
                    GridDataItem item = e.Item as GridDataItem;
                    HiddenField Hf = item.FindControl("Hf") as HiddenField;
                    DataTable dt = new DataTable();
                    dt = Session["tempdt"] as DataTable;
                    DataRow[] drr = dt.Select("STORE_NUMBER=' " + Store_Number + " ' ");
                    for (int i = 0; i < drr.Length; i++)
                    {
                        drr[i]["Status"] = "D";
                    }
                    dt.AcceptChanges();
                    grdMembers.Rebind();
                }
            }
            catch (ThreadAbortException ex1)
            {
                // do nothing
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "EditCustomStoreGroup.cs > grdMembers_ItemCommand"));
            }
        }

        protected void DeleteCustomStore(Int32 StoreId, Int32 StoreGroupId)
        {
            DataLayer DAL = new DataLayer();
            try
            {
                DAL.SP_DELETECUSTOMSTORES(StoreId, StoreGroupId);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "EditCustomStoreGroup.cs > SP_DeleteCustomStore"));
            }
        }

        #endregion

        #region Web Method

        [System.Web.Services.WebMethod]
        public static string[] SearchStoreNumber(string prefixText, int count)
        {
            DataTable dt = AppManager.SearchStoreNumber(AppConstants.SEARCH_STORE_NUMBER, prefixText.Replace("'", "''"));
            List<string> customers = new List<string>();
            int i = 0;
            for (i = 0; i < dt.Rows.Count; i++)
            {
                customers.Add(dt.Rows[i]["STORE_NUMBER"].ToString());
            }
            return customers.ToArray();
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public static string[] GetStoreDetail(string StoreNumber)
        {
            DataTable dtResult = AppManager.GetAllFromStores(StoreNumber);
            string[] StoreDetail;
            if (dtResult.Rows.Count > 0)
            {
                HttpContext.Current.Session["StoreAvailable"] = "Yes";
                StoreDetail = new string[] { Convert.ToString(dtResult.Rows[0]["STORE_NAME"]), Convert.ToString(dtResult.Rows[0]["STORE_LEGACY_NUMBER"]) };
            }
            else
            {
                HttpContext.Current.Session["StoreAvailable"] = "No";
                StoreDetail = new string[] { "Not Available", "Not Available" };
            }
            return StoreDetail;
        }

        #endregion

        #region Button Click

        public Boolean CheckExistStoreId(string Storeid)
        {
            Boolean Storestatus = true;
            if (hfstoreid.Value != txtstore.Text)
            {
                DataTable dt = new DataTable();
                dt = Session["tempdt"] as DataTable;
                DataRow[] drr = dt.Select("STORE_NUMBER='" + txtstore.Text + "' and Status <>'D'");
                if (drr.Length > 0)
                    Storestatus = false;
                else
                    Storestatus = true;
            }
            return Storestatus;
        }
        protected void Btnupdate_click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckExistStoreId(hfstoreid.Value))
                {
                    iControlA.MessageBox.Show(IcontrolFilterResources.StoreExists);
                    mpeRequest.Show();
                    return;
                }

                DataTable dtResult1 = AppManager.GetAllFromStores(txtstore.Text.Replace("'", "''"));
                if (dtResult1.Rows.Count > 0)
                {
                    if (Convert.ToString(Session["StoreAvailable"]) == "Yes")
                    {
                        DataTable dt = new DataTable();
                        dt = Session["tempdt"] as DataTable;
                        ViewState["Action"] = "N";
                        string ChkUpdt = "0";
                        try
                        {
                            foreach (DataRow dr in dt.Select("STORE_NUMBER='" + hfstorenumber.Value + "' and Status <>'D'"))
                            {
                                if (dr["STORE_NUMBER"].ToString() == txtstore.Text)
                                {

                                    if (txtLevel1.Text.Replace("'", "''") != dr["CUSTOM_LEVEL_1"].ToString())
                                    {
                                        ChkUpdt = "1";
                                        dr["CUSTOM_LEVEL_1"] = txtLevel1.Text.Replace("'", "''");
                                    }

                                    if (txtLevel2.Text.Replace("'", "''") != dr["CUSTOM_LEVEL_2"].ToString())
                                    {
                                        ChkUpdt = "1";
                                        dr["CUSTOM_LEVEL_2"] = txtLevel2.Text.Replace("'", "''");
                                    }

                                    if (txtlevel3.Text.Replace("'", "''") != dr["CUSTOM_LEVEL_3"].ToString())
                                    {
                                        ChkUpdt = "1";
                                        dr["CUSTOM_LEVEL_3"] = txtlevel3.Text.Replace("'", "''");
                                    }

                                    if (ChkUpdt == "1")
                                    {
                                        if (dr["Status"] != "X")
                                        {
                                            dr["Status"] = "U";
                                        }
                                        dt.AcceptChanges();
                                    }
                                    Session["tempdt"] = dt;
                                    ViewState["Action"] = "Y";
                                    hfstorenumber.Value = "0";
                                }
                            }
                            if (ViewState["Action"] == "N")
                            {
                                DataRow dradd = dt.NewRow();
                                dradd["STORE_NAME"] = dtResult1.Rows[0]["STORE_NAME"];
                                dradd["STORE_LEGACY_NUMBER"] = dtResult1.Rows[0]["STORE_LEGACY_NUMBER"];
                                dradd["STORE_NUMBER"] = txtstore.Text.Replace("'", "''");
                                if (hfstorenumber.Value != "0")
                                {
                                    DataRow[] drr = dt.Select("STORE_NUMBER='" + hfstorenumber.Value + "'");
                                    for (int i = 0; i < drr.Length; i++)
                                    {
                                        // drr[i].Delete();
                                        drr[i]["Status"] = "D";
                                        dt.AcceptChanges();
                                    }
                                    dradd["Old_StoreNumber"] = hfstorenumber.Value;
                                    dradd["Old_STOREID"] = HfNewStoreid.Value;
                                }
                                else
                                {
                                    dradd["Old_StoreNumber"] = txtstore.Text.Replace("'", "''");
                                    dradd["Old_STOREID"] = dtResult1.Rows[0]["STORE_ID"];
                                }
                                dradd["CUSTOM_LEVEL_1"] = txtLevel1.Text.Replace("'", "''");
                                dradd["CUSTOM_LEVEL_2"] = txtLevel2.Text.Replace("'", "''");
                                dradd["CUSTOM_LEVEL_3"] = txtlevel3.Text.Replace("'", "''");
                                dradd["STORE_ID"] = dtResult1.Rows[0]["STORE_ID"];
                                dradd["Status"] = "X";
                                hfstorenumber.Value = "0";
                                dt.Rows.Add(dradd);
                                Session["tempdt"] = dt;
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                        grdMembers.Rebind();
                    }
                    else
                    {
                        iControlA.MessageBox.Show(IcontrolFilterResources.InvalidStore);
                        lblStoreName.Text = "";
                        LblLegacyNo.Text = "";
                        mpeRequest.Show();
                    }
                }
                else
                {
                    iControlA.MessageBox.Show(IcontrolFilterResources.InvalidStore);
                    lblStoreName.Text = "";
                    LblLegacyNo.Text = "";
                    mpeRequest.Show();
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "EditCustomStoreGroup.cs > btnAddNew_Click"));
            }
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            lbleditMessage.Text = "Add New Store";
            LblLegacyNo.Text = "";
            lblStoreName.Text = "";
            Btnupdate.Text = "Save";
            txtstore.Text = "";
            txtLevel1.Text = "";
            txtLevel2.Text = "";
            txtlevel3.Text = "";
            mpeRequest.Show();
        }
        protected void BtnCancelSave_Click(object sender, EventArgs e)
        {
            Session["tempdt"] = null;
            Response.Redirect("ManageCustomStoreGroup.aspx");
        }
        protected void btnsave_Click(object sender, EventArgs e)
        {
            DataLayer DAL = new DataLayer();
            try
            {
                string UserName, StoreGroupName, StoreGroupDesc, CustomLevel1, CustomLevel2, CustomLevel3, StoreNo, Old_StoreID;
                UserName = Session["UserName"].ToString();
                StoreGroupName = txtGroupName.Text.Trim();
                StoreGroupDesc = txtStoreDescription.Text.Trim();
                DataTable dt = new DataTable();
                dt = ((DataTable)Session["tempdt"]);
                long CustomStoreGroupId = 0;
                dt.DefaultView.Sort = "[Status] ASC";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CustomLevel1 = dt.Rows[i]["CUSTOM_LEVEL_1"].ToString();
                    CustomLevel2 = dt.Rows[i]["CUSTOM_LEVEL_2"].ToString();
                    CustomLevel3 = dt.Rows[i]["CUSTOM_LEVEL_3"].ToString();
                    StoreNo = dt.Rows[i]["Old_StoreNumber"].ToString();
                    Old_StoreID = dt.Rows[i]["Old_STOREID"].ToString();
                    Session["tempdt"] = null;
                    string New_StoreNumber = dt.Rows[i]["STORE_NUMBER"].ToString();

                    if (dt.Rows[i]["Status"].ToString() == "D")
                    {
                        DeleteCustomStore(Convert.ToInt32(Old_StoreID), Convert.ToInt32(Request.QueryString["id"].ToString()));
                    }
                    else if (dt.Rows[i]["Status"].ToString() == "U")
                    {
                        SaveData(Convert.ToInt64(Request.QueryString["id"].ToString()), Session["CustomStoreUser"].ToString(), StoreGroupName, StoreGroupDesc, CustomLevel1, CustomLevel2, CustomLevel3, New_StoreNumber, "Update", StoreNo, Convert.ToDateTime(Session["DATETIMECREATED"]), Old_StoreID);
                    }

                    else if (dt.Rows[i]["Status"].ToString() == "X")
                    {
                        CustomStoreGroupId = Convert.ToInt64(Request.QueryString["id"]);
                        SaveData(Convert.ToInt64(CustomStoreGroupId), Session["CustomStoreUser"].ToString(), StoreGroupName, StoreGroupDesc, CustomLevel1, CustomLevel2, CustomLevel3, New_StoreNumber, "Insert", StoreNo, Convert.ToDateTime(Session["DATETIMECREATED"]), Old_StoreID);
                    }
                    else
                    {
                        AppManager.UpdateCustomStoreMaster(StoreGroupName.Replace("'", "''"), StoreGroupDesc.Replace("'", "''"), Convert.ToInt64(Request.QueryString["id"]));
                    }
                }
                DAL.P_CUSTOM_STORE_EXTENDED_UPDATE(CustomStoreGroupId.ToString());
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "EditCustomStoreGroup.cs > btnsave_Click"));
            }
            Response.Redirect("ManageCustomStoreGroup.aspx");
        }

        protected void BtnCancelStore_click(object sender, EventArgs e)
        {
            hfstorenumber.Value = "0";
            hfstoreid.Value = "0";
            mpeRequest.Hide();
        }
        protected void SaveData(long CustomStoreGroupId, string UserName, string StoreGroupName, string StoreGroupDesc, string CustomLevel1, string CustomLevel2, string CustomLevel3, string StoreNo, string Action, String Old_StoreNo, DateTime Datetimecreated, string Old_StoreID)
        {
            DataLayer DAL = new DataLayer();
            try
            {
                if (Action == "Insert")
                {
                    DAL.SP_INSERT_CUSTOMSTOREGROUP_NEW(UserName, StoreGroupName, StoreGroupDesc, CustomLevel1, CustomLevel2, CustomLevel3, StoreNo, CustomStoreGroupId, Datetimecreated);
                }
                else
                {
                    AppManager.UpdateCustomStoreMaster(StoreGroupName, StoreGroupDesc, CustomStoreGroupId);
                    AppManager.UpdateCustomStoreDetails(StoreGroupName, StoreGroupDesc, CustomStoreGroupId, CustomLevel1, CustomLevel2, CustomLevel3, StoreNo, Old_StoreID);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "EditCustomStoreGroup.cs > SaveData"));
            }
        }
        #endregion
    }
}