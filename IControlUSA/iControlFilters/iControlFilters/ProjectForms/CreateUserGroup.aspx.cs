﻿using iControlDataLayer;
using iControlGenricFilters.LIB;
using iControlGenricFilters.ResourceFiles;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace iControlGenricFilters.ProjectForms
{
    public partial class CreateUserGroup : System.Web.UI.Page
    {
        #region Global Variables
        String strSortExpression = "", strSortDirection = "";
        protected bool _EditRight = false;
        IControlHelper helper = new IControlHelper();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SetEditRights();
                if (!Page.IsPostBack)
                {
                    SetPageDefaults();
                    if (Request.QueryString["id"] != null)
                    {
                        SetActiveTab(Request.QueryString["id"]);
                        Request.QueryString["id"] = null;
                    }
                    else
                    {
                        if (Session["AttributesSetting"] != null)
                        {
                            if (Convert.ToBoolean(Session["AttributesSetting"]))
                            {
                                SetActiveTab("2");
                            }
                        }
                        else
                        {
                            SetActiveTab("0");
                        }

                    }
                    Session["AttributesSetting"] = false;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > Page_Load"));
            }
        }

        protected void SetEditRights()
        {
            try
            {
                if (Session["EditRights"].ToString().Contains("EDIT"))
                {
                    _EditRight = true;
                }
                else
                {
                    _EditRight = false;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > SetEditRights"));
            }
        }

        public String GridViewSortDirection
        {
            get
            {
                if (ViewState["GridViewSortDirection"] == null)
                {
                    return "DESC";
                }
                else
                {
                    return ViewState["GridViewSortDirection"].ToString();
                }
            }

            set
            {
                ViewState["GridViewSortDirection"] = value;
            }

        }

        String GetSortDirection()
        {
            String GridViewSortDirectionNew;

            switch (GridViewSortDirection)
            {
                case "DESC":
                    GridViewSortDirectionNew = "ASC";
                    break;
                case "ASC":
                    GridViewSortDirectionNew = "DESC";
                    break;
                default:
                    GridViewSortDirectionNew = "DESC";
                    break;

            }
            GridViewSortDirection = GridViewSortDirectionNew;
            return GridViewSortDirectionNew;

        }

        protected void SetPageDefaults()
        {
            FillGroupList();
            FillVerticalList();
            FillLoginList();
            FillGroupListEDW();
            FillGroupListIDW();
            FillGroupListGeneral();
        }

        protected void SetDefault()
        {
            ddlGroup.SelectedIndex = 0;
            ddlEmail.SelectedIndex = 0;
            ddlGroupNameListEDW.SelectedIndex = 0;
            ddlGroupNameListIDW.SelectedIndex = 0;
            ddlGroupNameListGeneral.SelectedIndex = 0;
        }

        protected void SetActiveTab(string TabId)
        {
            lnkCreateGroup.CssClass = "";
            lnkAssignGroups.CssClass = "";
            lnkAssignAttributes.CssClass = "";
            strSortExpression = "";
            strSortDirection = "";

            ViewState["GridViewSortDirection"] = "";
            ViewState["GridViewSortExpression"] = "";

            multiTabs.ActiveViewIndex = Convert.ToInt16(TabId);

            if (TabId == "0")
            {
                lnkCreateGroup.CssClass = "active";

            }

            else if (TabId == "1")
                lnkAssignGroups.CssClass = "active";


            else if (TabId == "2")
                lnkAssignAttributes.CssClass = "active";
        }

        # region LinkButton Click

        protected void lnkCreateGroup_Click(object sender, EventArgs e)
        {
            SetActiveTab("0");
            grdGroupName.Rebind();
        }

        protected void lnkAssignGroups_Click(object sender, EventArgs e)
        {
            SetActiveTab("1");
            FillGroupList();
            FillVerticalList();
        }

        protected void lnkAssignAttributes_Click(object sender, EventArgs e)
        {
            SetActiveTab("2");
        }
        #endregion

        #region Create Group Tab

        protected void FillUserGroup(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            try
            {
                DataView dv = new DataView();
                DataTable dt = new DataTable();
                dt = DataManager.FillUserGroups();
                dt.TableName = "UserGroups";
                dv.Table = dt;
                if (ViewState["GridViewSortDirection"] != null)
                {
                    strSortDirection = ViewState["GridViewSortDirection"].ToString();
                    strSortExpression = ViewState["GridViewSortExpression"].ToString();
                }

                if (strSortExpression != "" && strSortDirection != "")
                {
                    dv.Sort = "[" + strSortExpression + "] " + strSortDirection;
                }

                grdGroupName.DataSource = dv;


                if (Session["AccessLevel"].ToString() == "Supplier")
                {
                    grdGroupName.Columns[5].Visible = false;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > FillUserGroup"));
            }
        }

        protected void grdGroupName_RowCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "EditGroupName")
            {
                Session["_GroupId"] = e.CommandArgument.ToString();
                Response.Redirect("AddUserGroup.aspx");
            }
        }

        protected void btnAddGroup_Click(object sender, EventArgs e)
        {
            Session["_GroupId"] = "0";
            Response.Redirect("AddUserGroup.aspx", false);
        }

        #endregion

        #region Assign Menus To Group Tab

        protected void FillGroupList()
        {
            try
            {
                string strQuery = helper.GetResourceString(AppConstants.GET_GROUPLIST, DataSourceConnection.SQLServerConnection.ToString()).Replace("@PortalName", Convert.ToString(ddlPortalName.SelectedItem.Text));

                AppManager.FillRadDropDownList(strQuery, "GroupName", "GroupId", ddlGroup, "--Select one--");
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > FillGroupList"));
            }
        }

        protected void FillVerticalList()
        {
            try
            {
                string strQuery;
                if (ddlPortalName.SelectedItem.Text != "General")
                    strQuery = helper.GetResourceString(AppConstants.GET_VERTICALLIST_OTHER, DataSourceConnection.SQLServerConnection.ToString()).Replace("@PortalName", Convert.ToString(ddlPortalName.SelectedItem.Text));
                else
                    strQuery = helper.GetResourceString(AppConstants.GET_VERTICALLIST_GENERAL, DataSourceConnection.SQLServerConnection.ToString());

                AppManager.FillRadDropDownList(strQuery, "VerticalName", "VerticalId", ddlVerticals, "--Select one--");
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > FillVerticalList"));

            }
        }

        protected void ddlPortalName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGroupList();
                FillVerticalList();
                bindMenuList();
                bindAssignMenuList();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > ddlPortalName_SelectedIndexChanged"));
            }
        }

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillVerticalList();
                bindMenuList();
                bindAssignMenuList();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > ddlGroup_SelectedIndexChanged"));
            }
        }

        protected void ddlVerticals_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bindMenuList();
                bindAssignMenuList();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > ddlVerticals_SelectedIndexChanged"));
            }
        }

        void bindMenuList()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = DataManager.MenuList(ddlGroup.SelectedValue.ToString(), ddlVerticals.SelectedValue.ToString());

                if (dt.Rows.Count > 0)
                {
                    DataTable dtFinal = ReArrangeSourceList(dt);
                    lstSource.DataSource = dtFinal;
                }
                else
                {
                    lstSource.DataSource = dt;
                }

                lstSource.DataTextField = "MenuName";
                lstSource.DataValueField = "MenuID";
                lstSource.DataBind();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > bindMenuList"));
            }
        }

        void bindAssignMenuList()
        {
            try
            {
                DataTable dtResult = new DataTable();
                dtResult = DataManager.AssignMenuList(ddlGroup.SelectedValue.ToString(), ddlVerticals.SelectedValue.ToString());
                lstTarget.DataSource = dtResult;

                lstTarget.DataTextField = "MenuName";
                lstTarget.DataValueField = "MenuID";
                lstTarget.DataBind();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > bindAssignMenuList"));
            }
        }

        protected void ImageMoveRight_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (lstSource.SelectedItem != null)
                {
                    foreach (ListItem item in lstSource.Items)
                    {
                        if (item.Selected)
                        {
                            if (lstTarget.Items.Contains(item) == false)
                            {
                                int idx = GetIndexInList(Convert.ToInt16(item.Value), lstTarget);
                                InsertItemInTargetList(idx, item);
                            }
                        }
                    }
                    for (int i = lstSource.Items.Count - 1; i >= 0; i--)
                    {
                        if (lstSource.Items[i].Selected)
                        {
                            if (lstSource.Items[i].Text.Contains("-->"))
                                lstSource.Items.Remove(lstSource.Items[i]);
                        }
                    }

                    lstSource.ClearSelection();
                    lstTarget.ClearSelection();

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > ImageMoveRight_Click"));
            }
        }

        protected void ImageMoveleft_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (lstTarget.SelectedItem != null)
                {

                    foreach (ListItem item in lstTarget.Items)
                    {
                        if (item.Selected)
                        {
                            if (lstSource.Items.Contains(item) == false)
                            {
                                int idx = GetIndexInList(Convert.ToInt16(item.Value), lstSource);
                                lstSource.Items.Insert(idx, item);
                            }
                        }
                    }
                    for (int i = lstTarget.Items.Count - 1; i >= 0; i--)
                    {
                        if (lstTarget.Items[i].Selected)
                        {
                            lstTarget.Items.Remove(lstTarget.Items[i]);
                        }
                    }

                    lstSource.ClearSelection();
                    lstTarget.ClearSelection();

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > ImageMoveleft_Click"));
            }
        }

        #region Arrange ListItems

        protected DataTable ReArrangeSourceList(DataTable dt)
        {
            //create a new data table and rearrange the child menus with their parent ID
            DataTable dtFinal = new DataTable();
            try
            {
                dtFinal.Clear();
                dtFinal.Columns.Add("MenuID");
                dtFinal.Columns.Add("MenuName");
                DataRow _dtNewRow;
                //variable for all parent ID
                string strAll_PId = "";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ParentMenuId"].ToString() == "")
                    {
                        strAll_PId += dt.Rows[i]["MenuID"].ToString() + ",";
                        // add parent row to table
                        _dtNewRow = dtFinal.NewRow();
                        string strP_ID = dt.Rows[i]["MenuID"].ToString();
                        _dtNewRow["MenuID"] = dt.Rows[i]["MenuID"];
                        _dtNewRow["MenuName"] = dt.Rows[i]["MenuName"];
                        dtFinal.Rows.Add(_dtNewRow);

                        // add all chaild rows of parent id
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["ParentMenuId"].ToString() == strP_ID)
                            {
                                _dtNewRow = dtFinal.NewRow();
                                _dtNewRow["MenuID"] = dr["MenuID"];
                                _dtNewRow["MenuName"] = dr["MenuName"];
                                dtFinal.Rows.Add(_dtNewRow);
                            }
                        }
                    }
                }
                // find the rows which parent id not in table and added them in the end 
                strAll_PId = strAll_PId.TrimEnd(',');
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["ParentMenuId"].ToString() != "")
                    {
                        if (!strAll_PId.Split(',').Contains(dr["ParentMenuId"].ToString()))
                        {
                            _dtNewRow = dtFinal.NewRow();
                            _dtNewRow["MenuID"] = dr["MenuID"];
                            _dtNewRow["MenuName"] = dr["MenuName"];
                            dtFinal.Rows.Add(_dtNewRow);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > ReArrangeSourceList"));
            }
            return dtFinal;
        }

        protected Int32 GetIndexInList(int findValue, ListBox lst)
        {
            int idx = 0;
            try
            {
                int ParentID = 0;
                //get parent value
                ParentID = DataManager.GetParentID(findValue);

                // is Parent menu exist in list
                bool IsParentId = false;
                int IsparentIdx = 0;
                foreach (ListItem itm in lst.Items)
                {
                    if (Convert.ToInt16(itm.Value) == ParentID)
                    {
                        IsParentId = true;
                        IsparentIdx = idx;
                        break;
                    }
                    idx = idx + 1;
                }
                // if parent menu exist get its index
                if (IsParentId == true)
                {
                    ViewState["IsParentExist"] = "true";
                    //get the last index of the list
                    int lastIndex = lst.Items.Count - 1;

                    foreach (ListItem itm in lst.Items)
                    {
                        // if current index equal to last index then insert on current+1 index
                        if (lastIndex == IsparentIdx)
                        {
                            return idx = IsparentIdx + 1;
                        }
                        else
                        {
                            string strVal = lst.Items[IsparentIdx + 1].ToString();
                            if (strVal.Contains("-->"))
                            {
                                int ItemParentID = DataManager.GetParentID(Convert.ToInt32(lst.Items[IsparentIdx + 1].Value));
                                if (ItemParentID == ParentID)
                                {
                                    IsparentIdx = IsparentIdx + 1;
                                }
                                else
                                {
                                    return idx = IsparentIdx + 1;
                                }
                                //IsparentIdx = IsparentIdx + 1;
                            }
                            else
                            {
                                if (strVal == "")
                                    return idx = IsparentIdx + 1;
                                else
                                    return idx = IsparentIdx + 1;
                            }
                        }
                    }
                }
                // else add menu at the end
                else
                {
                    ViewState["IsParentExist"] = "false";
                    return idx;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > GetIndexInList"));
            }
            return idx;
        }

        protected ListItem GetParentItem(ListItem findValue)
        {
            //get the parent menu name from source list
            ListItem P_Value = new ListItem();
            ViewState["IsParentMenu"] = "false";
            int P_Id = 0;
            P_Id = DataManager.GetParentID(Convert.ToInt32(findValue.Value));
            if (P_Id == 0 && Convert.ToInt32(findValue.Value) != 0)
            {
                ViewState["IsParentMenu"] = "true";
                P_Value = findValue;
            }
            else if (P_Id != 0 && Convert.ToInt32(findValue.Value) != 0)
            {
                foreach (ListItem itm in lstSource.Items)
                {
                    if (Convert.ToInt16(itm.Value) == P_Id)
                    {
                        P_Value = itm;
                        break;
                    }
                }
            }
            else
            {
                return P_Value;
            }

            return P_Value;
        }

        protected void InsertItemInTargetList(int idx, ListItem item)
        {
            try
            {
                //if parent exist then insert the item under the parent id
                if (ViewState["IsParentExist"].ToString() == "true")
                {
                    lstTarget.Items.Insert(idx, item);
                }
                else
                {
                    //if parent not in target list then get parent item 
                    ListItem strItem = GetParentItem(item);

                    if (strItem.Text != "")
                    {
                        //find if any child of parent menu already exist in target list
                        int newIdx = 0;
                        int IsChildtIdx = 0;
                        string IsExist = "";
                        foreach (ListItem itm in lstTarget.Items)
                        {
                            int P_Id = DataManager.GetParentID(Convert.ToInt16(itm.Value));
                            if (P_Id == Convert.ToInt16(strItem.Value))
                            {
                                IsExist = "Exist";
                                newIdx = IsChildtIdx;
                                break;
                            }
                            IsChildtIdx = IsChildtIdx + 1;
                        }
                        if (IsExist == "Exist")
                        {
                            if (ViewState["IsParentMenu"].ToString() != "true")
                            {
                                lstTarget.Items.Insert(newIdx, strItem);
                                lstTarget.Items.Insert(newIdx + 1, item);
                            }
                            else
                            {
                                lstTarget.Items.Insert(newIdx, strItem);
                            }
                        }
                        else
                        {
                            if (ViewState["IsParentMenu"].ToString() != "true")
                            {
                                lstTarget.Items.Insert(idx, strItem);// first insert the parent menu
                                lstTarget.Items.Insert(idx + 1, item);//then insert child menu under that parent
                            }
                            else
                            {
                                lstTarget.Items.Insert(idx, strItem);
                            }
                        }
                    }
                    else
                    {
                        lstTarget.Items.Insert(idx, item);//if parent not found in the souce list then insert the child at last index
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > InsertItemInTargetList"));
            }
        }

        #endregion

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            if (lstTarget.SelectedIndex <= lstTarget.Items.Count - 1 && lstTarget.SelectedIndex > 0)
            {

                ListItem li = new ListItem();
                li = lstTarget.SelectedItem;
                int index = lstTarget.SelectedIndex;
                lstTarget.Items.Remove(li);
                lstTarget.Items.Insert(index - 1, li);
            }

        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            if (lstTarget.SelectedIndex < lstTarget.Items.Count - 1)
            {

                ListItem li = lstTarget.Items[lstTarget.SelectedIndex];

                int index = lstTarget.SelectedIndex;
                lstTarget.Items.Remove(li);
                lstTarget.Items.Insert(index + 1, li);
            }
        }

        protected void btnExtremeUp_Click(object sender, ImageClickEventArgs e)
        {
            ListItem li = lstTarget.Items[lstTarget.SelectedIndex];
            lstTarget.Items.Remove(li);
            lstTarget.Items.Insert(0, li);
        }

        protected void btnExtremeDown_Click(object sender, ImageClickEventArgs e)
        {
            ListItem li = lstTarget.Items[lstTarget.SelectedIndex];

            lstTarget.Items.Remove(li);
            lstTarget.Items.Add(li);
        }

        protected void btnSaveGroups_Click(object sender, EventArgs e)
        {
            string CurrentParentID = "0", NewParentId = "";
            try
            {
                if (lstTarget.Items.Count == 0)
                {
                    DataManager.DeleteListItem(ddlGroup.SelectedValue.ToString(), ddlVerticals.SelectedValue.ToString());
                }
                int count = 0;
                for (int i = 0; i < lstTarget.Items.Count; i++)
                {
                    bool exist = false;
                    exist = DataManager.CheckAlreadyAssignMenusToGroup(Convert.ToInt16(ddlGroup.SelectedValue.ToString()), Convert.ToInt16(lstTarget.Items[i].Value));
                    if (!exist)
                    {
                        NewParentId = DataManager.GetParentID(Convert.ToInt32(lstTarget.Items[i].Value)).ToString();

                        if (NewParentId == "" || NewParentId == "0")
                        {
                            DataManager.AssignMenusToGroup(Convert.ToInt16(ddlGroup.SelectedValue.ToString()), Convert.ToInt16(lstTarget.Items[i].Value), 0, i);
                            CurrentParentID = lstTarget.Items[i].Value;
                            count++;
                        }
                        else
                        {
                            DataManager.AssignMenusToGroup(Convert.ToInt16(ddlGroup.SelectedValue.ToString()), Convert.ToInt16(lstTarget.Items[i].Value), Convert.ToInt32(CurrentParentID), i);
                            count++;
                        }
                    }
                }

                if (lstTarget.Items.Count > 0 && count > 0)
                {
                    MessageBox.Show("Congratulations! You have successfully added the Menus to Group!");
                }
                else if (lstTarget.Items.Count == 0)
                {
                    MessageBox.Show("Congratulations! You have successfully Updated the Menus to Group!");
                }
                else
                {
                    MessageBox.Show("Menu for this group already exist !!");

                }
                bindMenuList();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > btnSaveGroups_Click"));
            }
        }
        protected void btnCancelGroups_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreateUserGroup.aspx");
        }

        #endregion

        #region Attribute Assignment

        protected void FillLoginList()
        {
            try
            {
                string strQuery = helper.GetResourceString(AppConstants.GET_LOGIN_EMAILS, DataSourceConnection.SQLServerConnection.ToString());

                AppManager.FillRadDropDownList(strQuery, "Login", "OwnerEntityId", ddlEmail, "--Please select--");
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > FillLoginList"));
            }
        }



        protected void FillGroupListEDW()
        {
            try
            {
                string strGroups = string.Empty;
                strGroups = helper.GetResourceString(AppConstants.GET_GROUP_NAME_FOR_EDW_IDW_GENERAL, DataSourceConnection.SQLServerConnection.ToString()).Replace("@PortalName", "EDW");

                AppManager.FillRadDropDownList(strGroups, "GroupName", "GroupId", ddlGroupNameListEDW, "--Select one--");
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > FillGroupListEDW"));
            }
        }

        protected void FillGroupListIDW()
        {
            try
            {
                string strGroups = string.Empty;
                strGroups = helper.GetResourceString(AppConstants.GET_GROUP_NAME_FOR_EDW_IDW_GENERAL, DataSourceConnection.SQLServerConnection.ToString()).Replace("@PortalName", "IDW");

                AppManager.FillRadDropDownList(strGroups, "GroupName", "GroupId", ddlGroupNameListIDW, "--Select one--");
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > FillGroupListIDW"));
            }
        }

        protected void FillGroupListGeneral()
        {
            try
            {
                string strGroups = string.Empty;
                strGroups = helper.GetResourceString(AppConstants.GET_GROUP_NAME_FOR_EDW_IDW_GENERAL, DataSourceConnection.SQLServerConnection.ToString()).Replace("@PortalName", "EDW");

                AppManager.FillRadDropDownList(strGroups, "GroupName", "GroupId", ddlGroupNameListGeneral, "--Select one--");
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > FillGroupListGeneral"));
            }
        }


        #endregion

        protected void btnCancelAttributes_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreateUserGroup.aspx");
        }

        protected void btnSaveAttributes_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateGroups();
                ddlEmail.SelectedIndex = 0;
                if (ddlGroupNameListEDW.Items.Count > 0)
                    ddlGroupNameListEDW.SelectedIndex = 0;
                if (ddlGroupNameListIDW.Items.Count > 0)
                    ddlGroupNameListIDW.SelectedIndex = 0;
                if (ddlGroupNameListGeneral.Items.Count > 0)
                    ddlGroupNameListGeneral.SelectedIndex = 0;

                Session["AttributesSetting"] = true;
                Response.Redirect("CreateUserGroup.aspx?vid=6&mid=65");


            }
            catch (ThreadAbortException ex1)
            {
                //do nothing
            }
            catch (Exception ex)
            {
                MessageBox.Show("Update Failed.");
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > btnSaveAttributes_Click"));
            }
        }


        protected void UpdateGroups()
        {
            try
            {
                String strSql = helper.GetResourceString(AppConstants.DELETE_ASSIGN_USER_GROUP_BY_USERID, DataSourceConnection.SQLServerConnection.ToString()).Replace("@UserId", ddlEmail.SelectedValue.ToString());
                DataManager.ExecuteNonQuery(strSql);
                if (ddlGroupNameListEDW.SelectedIndex > 0)
                    DataManager.AssignMultipleUserGroups(ddlEmail.SelectedValue.ToString(), Convert.ToInt32(ddlGroupNameListEDW.SelectedValue.ToString()));
                if (ddlGroupNameListIDW.SelectedIndex > 0)
                    DataManager.AssignMultipleUserGroups(ddlEmail.SelectedValue.ToString(), Convert.ToInt32(ddlGroupNameListIDW.SelectedValue.ToString()));
                if (ddlGroupNameListGeneral.SelectedIndex > 0)
                    DataManager.AssignMultipleUserGroups(ddlEmail.SelectedValue.ToString(), Convert.ToInt32(ddlGroupNameListGeneral.SelectedValue.ToString()));
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateUserGroup.cs > UpdateGroups"));
            }
        }

    }
}