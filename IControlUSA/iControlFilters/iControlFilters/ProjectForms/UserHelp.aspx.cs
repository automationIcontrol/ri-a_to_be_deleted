﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using iControlDataLayer;

namespace iControlGenricFilters.ProjectForms
{
    public partial class UserHelp : System.Web.UI.Page
    {

        protected bool _EditRight = false;
        IControlHelper helper = new IControlHelper();


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                SetEditRights();
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["id"] != null)
                    {
                        SetActiveTab(Request.QueryString["id"]);
                        Request.QueryString["id"] = null;
                    }
                    else
                    {
                        if (Session["AttributesSetting"] != null)
                        {
                            if (Convert.ToBoolean(Session["AttributesSetting"]))
                            {
                                SetActiveTab("5");
                            }
                        }
                        else
                        {
                            SetActiveTab("0");
                        }
                    }
                    Session["AttributesSetting"] = false;
                }
                SetPageDefaults();

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Userhelp.aspx.cs > Page_Load"));
            }
        }
        /// <summary>
        /// Set Edit Rights
        /// </summary>
        protected void SetEditRights()
        {
            try
            {
                if (Session["EditRights"].ToString().Contains("EDIT"))
                {
                    _EditRight = true;
                }
                else
                {
                    _EditRight = false;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Userhelp.aspx.cs > SetEditRights"));
            }
        }
       
        /// <summary>
        /// bind list of userguide and trainning videos
        /// </summary>
        protected void SetPageDefaults()
        {
            //User Help
            bindGridUserGuide();
            bindGridTraining();
        }

        #region LinkButton Click
        /// <summary>
        /// set user guide tab active
        /// </summary>
        protected void lnkUserGuide_Click(object sender, EventArgs e)
        {
            SetActiveTab("0");
        }
        /// <summary>
        /// set user training video tab active
        /// </summary>
        protected void lnkTrainingVideos_Click(object sender, EventArgs e)
        {
            SetActiveTab("1");
        }

        /// <summary>
        /// se tab active
        /// </summary>
        /// <param name="TabId"> TabId</param>
        protected void SetActiveTab(string TabId)
        {
            lnkUserGuide.CssClass = "";
            lnkTrainingVideos.CssClass = "";
            ViewState["GridViewSortDirection"] = "";
            ViewState["GridViewSortExpression"] = "";

            multiTabs.ActiveViewIndex = Convert.ToInt16(TabId);

            if (TabId == "0")
            {
                lnkUserGuide.CssClass = "active";
            }

            else if (TabId == "1")
            {
                lnkTrainingVideos.CssClass = "active";
            }
            else
            {
                lnkUserGuide.CssClass = "active";
            }
        }
        #endregion

        #region user guide Grid Events
        /// <summary>
        ///bind grid of userguide
        /// </summary>
        protected void bindGridUserGuide()
        {
            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("~/HelpDocs"));
                FileInfo[] fileInfo = dirInfo.GetFiles("*.*", SearchOption.AllDirectories);
                gridUserGuide.DataSource = fileInfo;
                gridUserGuide.DataBind();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Userhelp.aspx.cs > bindGridUserGuide"));
            }
        }
        /// <summary>
        ///set delete button visible according user type
        /// </summary>
        protected void gridUserGuide_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
                if (DataManager.CheckCurrentUserAccount(Session["PersonId"].ToString()))
                {
                    lnkDelete.Visible = true;
                }
                else
                {
                    lnkDelete.Visible = false;
                }
            }
        }

        protected void gridUserGuide_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {

        }
        /// <summary>
        ///gridUserGuide ItemCommand functionality
        /// </summary>
        protected void gridUserGuide_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DownloadFile")
                {
                    string filename = e.CommandArgument.ToString();
                    if (!String.IsNullOrEmpty(filename))
                    {
                        string filePath1 = ((System.IO.FileInfo)((e.Item).DataItem)).DirectoryName.ToString();//Server.MapPath("~/HelpDocs");
                        string filePath = Path.Combine(filePath1, filename);
                        DownloadFile(filePath);
                    }
                }
                else if (e.CommandName == "DeleteFile")
                {
                    string filePath = ((System.IO.FileInfo)((e.Item).DataItem)).DirectoryName.ToString(); //Server.MapPath("~/HelpDocs");
                    string fileName = Path.Combine(filePath, e.CommandArgument.ToString());
                    if (System.IO.File.Exists(fileName))
                    {
                        System.IO.File.Delete(fileName);
                    }
                    bindGridUserGuide();
                }
            }
            catch (Exception ex)
            {

                helper.WriteLog(string.Format("{0}" + ex.Message, "Userhelp.aspx.cs > gridUserGuide_ItemCommand"));
            }
        }

        #endregion

        #region Download File
        /// <summary>
        ///download file
        /// </summary>
        public void DownloadFile(string filePath)
        {
            try
            {
                if (File.Exists((filePath)))
                {
                    string strFileName = Path.GetFileName(filePath).Replace(" ", "%20");
                    var fileInfo = new System.IO.FileInfo(filePath);
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", strFileName));
                    Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                    Response.WriteFile(filePath);
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Userhelp.aspx.cs > DownloadFile"));
            }
        }

        #endregion

        #region Training Videos Grid Events
        /// <summary>
        /// bind trainning video list in grid
        /// </summary>
        protected void bindGridTraining()
        {
            try
            {
               using (DataTable dt = DataManager.GetTrainningVideoList())
                {
                    if (dt.Rows.Count > 0)
                    {
                        gridTrainingVideos.DataSource = dt;
                        gridTrainingVideos.DataBind();
                        string VideoName = GetVideoName(dt.Rows[0]["VideoUrl"].ToString());
                        videomain.Attributes.Add("src", "//www.youtube.com/embed/" + VideoName);
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Userhelp.aspx.cs > bindGridTraining"));
            }
        }
        public string ReplaceSpecialChar(string name)
        {
            string VideoName = GetVideoName(name);
            return VideoName;
        }
        /// <summary>
        /// gridTrainingVideos RowCommand functionality
        /// </summary>
        protected void gridTrainingVideos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DownloadFile")
            {
                try
                {

                    using (DataTable dt = DataManager.GetTrainningVideoDetail(e.CommandArgument.ToString()))
                    {
                        if (dt.Rows.Count > 0)
                        {
                            string VideoName = GetVideoName(dt.Rows[0]["VideoUrl"].ToString());
                            videomain.Attributes.Add("src", "//www.youtube.com/embed/" + VideoName);
                        }
                    }
                }
                catch (Exception ex)
                {
                    helper.WriteLog(string.Format("{0}" + ex.Message, "Userhelp.aspx.cs > gridTrainingVideos_RowCommand"));
                }
            }
        }
        /// <summary>
        /// get video name
        /// </summary>
        public string GetVideoName(string name)
        {
            try
            {
                string[] words = name.Split('=');
                string[] valu = words[1].Split('&');
                return valu[0].ToString();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Userhelp.aspx.cs > GetVideoName"));
                return name;
            }
        }
        #endregion
    }
}