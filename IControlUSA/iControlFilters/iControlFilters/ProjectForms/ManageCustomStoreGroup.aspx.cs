﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using Telerik.Web.UI;
using System.Threading;
using System.Data.OleDb;
using iControlDataLayer;
using iControlGenricFilters.LIB;
using iControlGenricFilters.ResourceFiles;
using System.Text.RegularExpressions;
using System.IO;

namespace iControlGenricFilters.ProjectForms
{
    public partial class ManageCustomStoreGroup : System.Web.UI.Page
    {
        #region Global Variables
        string UserName, NewStoreName, NewStoreDesc;
        public static string headerPageName = "Maintain Custom Store Group";
        public static string commandEdit = "EditCustomStore";
        public static string commandDelete = "DeleteCustomStore";
        public static string shareCommandName = "ShareStore";
        public static string dupCommandName = "DuplicateStore";
        public static string SharedUserCommandName = "SharedUser";
        public static string UnShareCommandName = "UnShareRep";
        public static string viewCommandName = "ViewRep";
        IControlHelper helper = new IControlHelper();
        Int32 _ItemGridCount = 0;
        #endregion

        #region PageLoad

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                topHeader.InnerText = headerPageName;
                grdMembers.MasterTableView.GetColumn("USERNAME").Display = false;

                if (!IsPostBack)
                {
                    Session["tempdt"] = null;
                    divSearchResult.Visible = false;
                    grdMembers.Visible = false;
                    Session["dtCustomStoreIDs"] = null;
                    Session["CustomStoreGridData"] = null;
                    grdMembers.Rebind();
                    FillOwnerUsers();
                    lblSpecialChars_Message.Text = AppManager.SpecialChars_Message();
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > Page_Load"));
            }
        }

        #endregion

        #region Get Data Method

        protected void FillOwnerUsers()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = AppManager.GetOwnerUsers(Session["UserName"].ToString());
                if (dt.Rows.Count > 0)
                {
                    RdOwnerBy.DataSource = dt;
                    RdOwnerBy.DataTextField = "Name";
                    RdOwnerBy.DataValueField = "Username";
                    RdOwnerBy.DataBind();
                }

                RdOwnerBy.Items.Insert(0, new RadComboBoxItem("Select User ----", "-1"));
                RdOwnerBy.Items.Insert(1, new RadComboBoxItem("Me", Session["UserName"].ToString()));
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > FillShareUsers"));
            }
        }

        protected void FillShareUsers()
        {
            try
            {
                bool isCorporate = false;
                if (DataManager.IsCorporateUser(Session["UserName"].ToString()))
                {
                    isCorporate = true;
                }
                else
                {
                    isCorporate = false;
                }
                AppManager.FillShareUsers(RdShareUsers, isCorporate, Session["UserName"].ToString());
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > FillShareUsers"));
            }
        }

        public DataTable GetSearchResults()
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                string strSql = "";
                if (RdOwnerBy.SelectedIndex > 1)
                {
                    strSql += helper.GetResourceString(AppConstants.GET_CUSTOM_STORE_MASTER_FOR_RDOWNEDBY).Replace("@rdOwnerBy", RdOwnerBy.SelectedValue.ToString()).Replace("@UserName", Session["UserName"].ToString());
                }
                else
                {
                    strSql += helper.GetResourceString(AppConstants.GET_CUSTOM_STORE_MASTER_FOR_NON_RDOWNEDBY).Replace("@UserName", Session["UserName"].ToString());
                }

                if (txtSearchText.Text != "")
                {
                    strSql += helper.GetResourceString(AppConstants.GET_CUSTOM_STORE_MASTER_FOR_SEARCHTEXT).Replace("@SearchText", txtSearchText.Text.ToString().Replace("'", "''"));
                }

                if (dtToDate.Text == "__/__/____")
                {
                    dtToDate.Text = "";
                }
                if (dtFromDate.Text == "__/__/____")
                {
                    dtFromDate.Text = "";
                }

                if (dtToDate.Text != "" && dtFromDate.Text != "")
                {
                    string fromDate = AppManager.FromDate(dtFromDate.Text).ToString("yyyy-MM-dd");
                    string toDate = AppManager.FromDate(dtToDate.Text).ToString("yyyy-MM-dd");
                    strSql += helper.GetResourceString(AppConstants.GET_CUSTOM_STORE_MASTER_FOR_DATEBETWEEN).Replace("@FromDate", fromDate.ToString()).Replace("@ToDate", toDate.ToString());
                }
                else if (dtToDate.Text == "" && dtFromDate.Text != "")
                {
                    string fromDate = AppManager.FromDate(dtFromDate.Text).ToString("yyyy-MM-dd");
                    strSql += helper.GetResourceString(AppConstants.GET_CUSTOM_STORE_MASTER_FOR_FROMDATE).Replace("@FromDate", fromDate.ToString());
                }
                else if (dtToDate.Text != "" && dtFromDate.Text == "")
                {
                    string toDate = AppManager.FromDate(dtToDate.Text).ToString("yyyy-MM-dd");
                    strSql += helper.GetResourceString(AppConstants.GET_CUSTOM_STORE_MASTER_FOR_TODATE).Replace("@ToDate", toDate.ToString());
                }
                if (RdOwnerBy.SelectedValue.ToString().ToLower() == Session["UserName"].ToString().ToLower())
                { 
                    strSql += helper.GetResourceString(AppConstants.GET_CUSTOM_STORE_MASTER_FOR_USERNAME).Replace("@UserName", RdOwnerBy.SelectedValue.ToString());
                }

                if (RdOwnerBy.SelectedIndex > 1)
                {
                    strSql += helper.GetResourceString(AppConstants.GET_CUSTOM_STORE_MASTER_GROUPBY_CLAUSE);
                }

                strSql += helper.GetResourceString(AppConstants.GET_CUSTOM_STORE_MASTER_ORDERBY_CLAUSE);

                dtResult = AppManager.GetSearchResult(strSql);
            } 
            catch (Exception ex)
            {
               helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > GetSearchResults"));
            }
            return dtResult;
        }

        #endregion

        #region Grid Events

        protected void grdMembers_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = GetSearchResults();
                if (dt != null)
                {
                    if (dt.Columns.Count > 0)
                    {
                        grdMembers.DataSource = dt;
                        divSearchResult.Visible = true;
                        grdMembers.Visible = true;
                    }
                    else
                    {
                        divSearchResult.Visible = false;
                        grdMembers.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > grdMembers_NeedDataSource"));
            }
        }

        protected void grdMembers_ItemCommand(object sender, GridCommandEventArgs e)
        {
            HfShare.Value = Convert.ToString(e.CommandArgument);
            try
            {
                if (e.CommandName == commandEdit)
                {
                    string StoreId = e.CommandArgument.ToString();
                    Response.Redirect("EditCustomStoreGroup.aspx?id=" + StoreId);
                }

                if (e.CommandName == viewCommandName)
                {
                    string StoreId = e.CommandArgument.ToString();
                    Response.Redirect("ViewCustomStoreGroup.aspx?id=" + StoreId);
                }

                if (e.CommandName == commandDelete)
                {
                    int StoreGroupId = Convert.ToInt32(e.CommandArgument);
                    GridDataItem item = e.Item as GridDataItem;

                    DeleteCustomStoreGroup(Convert.ToInt32(StoreGroupId));

                    divSearchResult.Visible = true;
                    grdMembers.Visible = true;
                    grdMembers.Rebind();
                }

                if (e.CommandName == shareCommandName)
                {
                    FillShareUsers();
                    
                    RdShareUsers.ClearCheckedItems();
                    DataTable dtgetShared=AppManager.GetCustomStoresShared(AppConstants.GET_CUSTOM_STORES_SHARED, Convert.ToInt32(HfShare.Value));
                    if (dtgetShared.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtgetShared.Rows.Count; i++)
                        {
                            RdShareUsers.SelectedValue = dtgetShared.Rows[i]["USERNAME"].ToString();
                            RdShareUsers.SelectedItem.Checked = true;
                        }
                    }
                    mpeShare.Show();
                }

                if (e.CommandName == dupCommandName)
                {
                    DataTable dt = AppManager.GetCustomStoresShared(AppConstants.GET_CUSTOM_STORES_SHARED_MASTER, Convert.ToInt32(HfShare.Value));
                    HfCustomStoreId.Value = Convert.ToString(e.CommandArgument);
                    txtDupliStoreName.Text = dt.Rows[0]["CUSTOM_STORE_NAME"] + "_copy";
                    txtStoreDescr.Text = dt.Rows[0]["CUSTOM_STORE_DESCRIPTION"] + String.Empty;
                    upRequest.Update();
                    mpeDuplicate.Show();
                }
                if (e.CommandName == SharedUserCommandName)
                {
                    DataTable dtgetShared = AppManager.GetCustomStoresShared(AppConstants.GET_CUSTOM_STORES_SHARED_ORDERBY_USERNAME, Convert.ToInt32(HfShare.Value));
                    if (dtgetShared.Rows.Count > 0)
                    {
                        lstUser.Items.Clear();
                        for (int i = 0; i < dtgetShared.Rows.Count; i++)
                        {
                            lstUser.Items.Add((i + 1).ToString() + ". " + Convert.ToString(dtgetShared.Rows[i]["NAME"]) + " (" + Convert.ToString(dtgetShared.Rows[i]["USERNAME"]) + ")");

                        }
                    }
                    mpeSHaredWith.Show();
                }
                if (e.CommandName == UnShareCommandName)
                {
                    hdnCustmId.Value = e.CommandArgument.ToString();
                    DataTable dtgetShared = AppManager.GetCustomStoresShared(AppConstants.GET_CUSTOM_STORES_UNSHARED_ORDERBY_USERNAME, Convert.ToInt32(HfShare.Value.ToString()));
                    if (dtgetShared.Rows.Count > 0)
                    {
                        chkUserList.Items.Clear();
                        chkUserList.DataSource = dtgetShared;
                        chkUserList.DataTextField = "NAME";
                        chkUserList.DataValueField = "USERNAME";
                        chkUserList.DataBind();

                    }
                    mpeUnshare.Show();
                }
            }
            catch (ThreadAbortException)
            {
                // do nothing
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > grdMembers_ItemCommand"));
            }
        }

        protected void grdMembers_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            //Is it a GridDataItem
            if (e.Item.ItemType == GridItemType.AlternatingItem || e.Item.ItemType == GridItemType.Item)
            {
                try
                {
                    HiddenField hfIsShared = e.Item.FindControl("hfIsShared") as HiddenField;
                    HiddenField hfStoreID = e.Item.FindControl("HfStoreID") as HiddenField;
                    ImageButton img = e.Item.FindControl("imgshare") as ImageButton;
                    LinkButton lnkShare = e.Item.FindControl("Lnkshare") as LinkButton;
                    LinkButton lnkDel = e.Item.FindControl("btnDelete") as LinkButton;
                    LinkButton lnkEdit = e.Item.FindControl("btnEdit") as LinkButton;
                    Label LblSharedBy = e.Item.FindControl("LblSharedBy") as Label;
                    LinkButton LblSharedWith = e.Item.FindControl("lnkSahreWith") as LinkButton;
                    LinkButton lnkUnshare = e.Item.FindControl("lnkUnshare") as LinkButton;
                    LinkButton lnkView = e.Item.FindControl("LnkView") as LinkButton;

                    DataTable dtgetShared = AppManager.GetCustomStoresShared(AppConstants.GET_CUSTOM_STORES_SHARED, Convert.ToInt32(hfStoreID.Value.ToString()));

                    DataTable dtSharedStores = AppManager.GetCustomStoresShared(AppConstants.GET_CUSTOM_STORES_SHARED_MASTER, Convert.ToInt32(hfStoreID.Value.ToString()));

                    if (dtgetShared.Rows.Count > 0)
                    {
                        LblSharedWith.Visible = true;
                        if (dtgetShared.Rows.Count > 1)
                        {
                            LblSharedWith.Text = Convert.ToString(dtgetShared.Rows.Count) + " Users";
                        }
                        else
                        {
                            LblSharedWith.Text = Convert.ToString(dtgetShared.Rows.Count) + " User";
                        }
                        lnkDel.Attributes["onClick"] =
                        string.Format("return confirm('"+IcontrolFilterResources.Manage_Custom_Store_On_GridBind_Confirmation + "')");
                        string.Format("rowConfirmDelete(" + hfStoreID.Value + ", "+ IcontrolFilterResources.Manage_Custom_Store_On_GridBind_Confirmation + "); return false;");
                    }
                    else
                    {
                        lnkUnshare.Visible = false;                                                 
                        lnkDel.Attributes["onClick"] = string.Format("rowConfirmDelete(" + hfStoreID.Value + "," + IcontrolFilterResources.Manage_Custom_Store_On_Group_Delete_Confirmation + "); return false;");
                    }

                    if (hfIsShared.Value == "Yes")
                    {
                        LblSharedBy.Text = Convert.ToString(dtSharedStores.Rows[0]["NAME"]) + "<br /> (" + Convert.ToString(dtSharedStores.Rows[0]["USERNAME"]) + ")";
                        LblSharedWith.Visible = false;
                        img.Visible = true;
                        lnkDel.Visible = false;
                        lnkEdit.Visible = false;
                        lnkShare.Visible = false;
                        lnkUnshare.Visible = false;
                        lnkView.Visible = true;
                    }
                    else
                    {
                        LblSharedBy.Text = "Me";
                        img.Visible = false;
                        lnkDel.Visible = true;
                        lnkEdit.Visible = true;
                        lnkShare.Visible = true;
                        lnkView.Visible = false;
                    }

                    //checkbox changes
                    CheckBox chk = (CheckBox)e.Item.FindControl("chk");
                    GridHeaderItem headerItem = (GridHeaderItem)grdMembers.MasterTableView.GetItems(GridItemType.Header)[0];
                    CheckBox chkAll = headerItem.FindControl("chkSelectAll") as CheckBox;
                    DataTable dtChk = Session["dtCustomStoreIDs"] as DataTable;
                    if (dtChk != null)
                    {
                        if (dtChk.Rows.Count > 0)
                            dtChk = (Session["dtCustomStoreIDs"] as DataTable).DefaultView.ToTable(true);
                        if (ViewState["Pager"] != null && ViewState["Pager"].ToString().Trim() == "PagingClicked".Trim() || ViewState["maxRows"] != null || ViewState["ItemSort"] != null)
                        {
                            Int32 StoreGroupId = Convert.ToInt32(((HiddenField)e.Item.FindControl("HfStoreID")).Value);
                            if (Session["dtCustomStoreIDs"] != null)
                            {
                                int x = 0;
                                for (x = 0; x < dtChk.Rows.Count; x++)
                                {
                                    if (Convert.ToInt32(dtChk.Rows[x]["StoreGroupId"]) == StoreGroupId)
                                    {
                                        chk.Checked = true;
                                        btnDeleteMultiple.Visible = true;
                                        _ItemGridCount += 1;
                                    }
                                }
                            }
                        }
                    }
                    if (ViewState["IsSelectAll"] != null && ViewState["IsSelectAll"].ToString() == "true")
                    {
                        chkAll.Checked = true;
                    }
                    else if (ViewState["ItemSort"] != null)
                    {
                        if (_ItemGridCount == Convert.ToInt32(ViewState["ItemSort"]))
                            chkAll.Checked = true;
                    }
                }
                catch (Exception ex)
                {
                    helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > grdMembers_ItemCommand"));
                }
            }
        }

        protected void grdMembers_ItemSorting(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                //checkbox changes
                ViewState["IsSelectAll"] = null;
                ViewState["ItemSort"] = null;
                Int32 _checkedCount = 0;
                Int32 _noSharedCount = 0;
                Int32 _gridCount = grdMembers.MasterTableView.Items.Count;
                DataTable dtChk = Session["dtCustomStoreIDs"] as DataTable;
                if (dtChk != null)
                {
                    if (dtChk.Rows.Count > 0)
                        dtChk = (Session["dtCustomStoreIDs"] as DataTable).DefaultView.ToTable(true);
                    foreach (GridDataItem dataItem in grdMembers.MasterTableView.Items)
                    {
                        Int32 StoreGroupId = Convert.ToInt32(((HiddenField)dataItem.FindControl("HfStoreID")).Value);
                        string _isShared = ((HiddenField)dataItem.FindControl("hfIsShared")).Value;
                        if (Session["dtCustomStoreIDs"] != null)
                        {
                            for (Int32 j = 0; j < dtChk.Rows.Count; j++)
                            {
                                if (Convert.ToInt32(dtChk.Rows[j]["StoreGroupId"]) == StoreGroupId)
                                {
                                    (dataItem.FindControl("chk") as CheckBox).Checked = true;
                                    dataItem.Selected = true;
                                    btnDeleteMultiple.Visible = true;
                                    _checkedCount += 1;
                                }
                            }
                            if (_isShared.ToLower() == "yes")
                                _noSharedCount += 1;
                        }
                    }
                    ViewState["ItemSort"] = _gridCount - _noSharedCount;

                    if (Session["dtCustomStoreIDs"] != null)
                    {
                        if (dtChk.Rows.Count == 0)
                            btnDeleteMultiple.Visible = false;
                        upRequest.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > grdMembers_ItemSorting"));
            }
        }

        protected void grdMembers_paging(object source, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                ViewState["paging"] = "1";
                ViewState["maxRows"] = e.NewPageSize;
                ViewState["IsSelectAll"] = null;
                ViewState["ItemSort"] = null;
                grdMembers.Rebind();
                //checkbox changes
                Int32 _checkedCount = 0;
                Int32 _noSharedCount = 0;
                Int32 _gridCount = grdMembers.MasterTableView.Items.Count;
                DataTable dtChk = Session["dtCustomStoreIDs"] as DataTable;
                if (dtChk != null)
                {
                    if (dtChk.Rows.Count > 0)
                        dtChk = (Session["dtCustomStoreIDs"] as DataTable).DefaultView.ToTable(true);
                    foreach (GridDataItem dataItem in grdMembers.MasterTableView.Items)
                    {
                        Int32 StoreGroupId = Convert.ToInt32(((HiddenField)dataItem.FindControl("HfStoreID")).Value);
                        string _isShared = ((HiddenField)dataItem.FindControl("hfIsShared")).Value;
                        if (Session["dtCustomStoreIDs"] != null)
                        {
                            int j = 0;
                            for (j = 0; j < dtChk.Rows.Count; j++)
                            {
                                if (Convert.ToInt32(dtChk.Rows[j]["StoreGroupId"]) == StoreGroupId)
                                {
                                    (dataItem.FindControl("chk") as CheckBox).Checked = true;
                                    dataItem.Selected = true;
                                    btnDeleteMultiple.Visible = true;
                                    _checkedCount += 1;
                                }
                            }
                            if (_isShared.ToLower() == "yes")
                                _noSharedCount += 1;
                        }
                    }
                    _checkedCount += _noSharedCount;
                    if (_checkedCount == _gridCount)
                    {
                        ViewState["IsSelectAll"] = "true";
                    }

                    if (Session["dtCustomStoreIDs"] != null)
                    {
                        if (dtChk.Rows.Count > 0)
                        {
                            //btnDeleteMultiple.Visible = true;
                        }
                        else
                            btnDeleteMultiple.Visible = false;
                        upRequest.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > grdMembers_paging"));
            }
        }

        protected void grdMembers_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                ViewState["Pager"] = "PagingClicked";
                ViewState["IsSelectAll"] = null;
                ViewState["ItemSort"] = null;
                grdMembers.CurrentPageIndex = e.NewPageIndex;
                grdMembers.Rebind();

                Int32 _checkedCount = 0;
                Int32 _noSharedCount = 0;
                Int32 _gridCount = grdMembers.MasterTableView.Items.Count;
                DataTable dtChk = Session["dtCustomStoreIDs"] as DataTable;
                if (dtChk != null)
                {
                    if (dtChk.Rows.Count > 0)
                        dtChk = (Session["dtCustomStoreIDs"] as DataTable).DefaultView.ToTable(true);
                    foreach (GridDataItem dataItem in grdMembers.MasterTableView.Items)
                    {
                        Int32 StoreGroupId = Convert.ToInt32(((HiddenField)dataItem.FindControl("HfStoreID")).Value);
                        string _isShared = ((HiddenField)dataItem.FindControl("hfIsShared")).Value;
                        if (Session["dtCustomStoreIDs"] != null)
                        {
                            for (Int32 j = 0; j < dtChk.Rows.Count; j++)
                            {
                                if (Convert.ToInt32(dtChk.Rows[j]["StoreGroupId"]) == StoreGroupId)
                                {
                                    (dataItem.FindControl("chk") as CheckBox).Checked = true;
                                    dataItem.Selected = true;
                                    btnDeleteMultiple.Visible = true;
                                    _checkedCount += 1;
                                }
                            }
                            if (_isShared.ToLower() == "yes")
                                _noSharedCount += 1;
                        }
                    }
                    _checkedCount += _noSharedCount;
                    if (_checkedCount == _gridCount)
                    {
                        ViewState["IsSelectAll"] = "true";
                    }

                    if (Session["dtCustomStoreIDs"] != null)
                    {
                        if (dtChk != null)
                        {
                            if (dtChk.Rows.Count > 0)
                            {
                                //btnDeleteMultiple.Visible = true;
                            }
                            else
                                btnDeleteMultiple.Visible = false;
                        }
                        upRequest.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > grdMembers_PageIndexChanged"));
            }
        }

        #endregion

        #region Button Click

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                grdMembers.Rebind();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > btnSearch_Click"));
            }
        }
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ProjectForms/CustomStoreGroup.aspx?Case=1", false);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > btnAddNew_Click"));
            }
        }
        protected void btnsaveShare_click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                if (RdShareUsers.CheckedItems.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                                         "Confirmation Message",
                                                         "alert('" + IcontrolFilterResources.SelectUser + "');",
                                                         true);
                    mpeShare.Show();
                    return;
                }

                DataTable dtErrors = ValidateStore();
                if (dtErrors.Rows.Count == 0)
                {
                    CheckAlreadySharedUser();
                }
                else
                {
                    string user = "";
                    for (int i = 0; i < dtErrors.Rows.Count; i++)
                    {
                        if (dtErrors.Rows.Count > 1)
                        {
                            user += i + 1 + ") " + dtErrors.Rows[i]["USERID"].ToString() + " (" + dtErrors.Rows[i]["Username"].ToString() + ") " + "  :  " + dtErrors.Rows[i]["TotalStoreNo"].ToString() + " Store(s) " + "\\n";
                        }
                        else
                        {
                            user += i + 1 + ") " + dtErrors.Rows[i]["USERID"].ToString() + " (" + dtErrors.Rows[i]["Username"].ToString() + ") " + "  :  " + dtErrors.Rows[i]["TotalStoreNo"].ToString() + " Store(s) ";
                        }
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                                  "Confirmation Message",
                                                  "alert('" + IcontrolFilterResources.Manage_Custom_Store_UserAuthorization + "" + user + "');",
                                                  true);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > btnsaveShare_click"));
            }
        }
        protected void btnsaveDuplicate_click(object sender, EventArgs e)
        {
            try
            {
                DataLayer DAL = new DataLayer();
                UserName = Session["UserName"].ToString();
                NewStoreDesc = txtStoreDescr.Text.Trim();
                string StoreName = txtDupliStoreName.Text.Trim();

                String _StoreName = AppManager.SpecialCharactersValidationforAlert_New(txtDupliStoreName.Text.Trim(), "Store Group Name");
                if (_StoreName != String.Empty)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('" + _StoreName + "');", true);
                    mpeDuplicate.Show();
                    return;
                }
                String _StoreDesc = AppManager.SpecialCharactersValidationforAlert_New(txtStoreDescr.Text.Trim(), "Store Description");
                if (_StoreDesc != String.Empty)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('" + _StoreDesc + "');", true);
                    mpeDuplicate.Show();
                    return;
                }

                if (txtDupliStoreName.Text.Trim().ToString() == "")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                    "Confirmation Message",
                                      "alert('" + IcontrolFilterResources.NewStoreName + "');",
                                      true);
                    mpeShare.Show();
                    return;
                }
                else
                {
                    NewStoreName = txtDupliStoreName.Text.ToString();
                }
                if (NewStoreName.Length > 100)
                {
                    mpeDuplicate.Show();
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                               "Confirmation Message",
                                               "alert('"+ IcontrolFilterResources.StoreNameLength + "');",
                                               true);
                }
                else
                {
                    int CustomStoreId = Convert.ToInt32(HfCustomStoreId.Value);
                    DataTable dt = new DataTable();
                    dt = AppManager.GetCustomStoresSharedByStoreName(Session["UserName"].ToString(), StoreName.Replace("'", "''"));
                    if (dt.Rows.Count > 0)
                    {
                        mpeDuplicate.Show();
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                                          "err_msg",
                                                          "alert('" + IcontrolFilterResources.StoreNameExists + "');",
                                                          true);
                    }
                    else
                    {
                        DataTable dtSeq = new DataTable();
                        dtSeq = AppManager.GetNextValueForSEQ_CUSTOM_HIERARCHIES();
                        int NewCustomStoreId = Convert.ToInt32(dtSeq.Rows[0][0]);
                        AppManager.InsertDuplicateCustomHierarchy(CustomStoreId, NewCustomStoreId, UserName, NewStoreName, NewStoreDesc);
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                                       "Confirmation Message",
                                                       "alert('" + IcontrolFilterResources.CustomStoreCloned + "');",
                                                       true);
                        grdMembers.Rebind();
                        uncheckAllCheckbox();

                        DAL.P_CUSTOM_STORE_EXTENDED_UPDATE(NewCustomStoreId.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > btnsaveDuplicate_click"));
            }
        }
        protected void btnUnshare_Click(object sender, EventArgs e)
        {
            DataLayer DAL = new DataLayer();
            IControlHelper helper = new IControlHelper();
            try
            {
                bool isSelected = false;
                for (int i = 0; i < chkUserList.Items.Count; i++)
                {
                    if (chkUserList.Items[i].Selected)
                    {
                        isSelected = true;
                        int RowsAffected = AppManager.DeleteCustomStoresShared(Convert.ToInt32(hdnCustmId.Value), chkUserList.Items[i].Value);
                    }

                    //CUSTOM_STORE_EXTENDED_UPDATE
                    DAL.P_CUSTOM_STORE_EXTENDED_UPDATE(hdnCustmId.Value.ToString());
                }
                if (isSelected)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                                        "Confirmation Message",
                                                        "alert('" + IcontrolFilterResources.CustomStoreAdded + "');",
                                                        true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                                            "Confirmation Message",
                                                            "alert('" + IcontrolFilterResources.SelectUserForUnshare + "');",
                                                            true);
                    mpeUnshare.Show();
                }
                grdMembers.Rebind();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > btnUnshare_Click"));
            }
        }
        protected void lnkExport_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 _CustomStoreId = Convert.ToInt32(hiddenStor_Id.Value);
                CustomStore_ExportFile(_CustomStoreId);
            }
            catch (ThreadAbortException ex1)
            {
                // do nothing
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > lnkExport_Click"));
            }
        }
        protected void btnDeleteMultipleStores_Click(object sender, System.EventArgs e)
        {
            try
            {
                Int32 _delMultiples = 0;
                DataTable dtDelete_CustomStore = (Session["dtCustomStoreIDs"] as DataTable).DefaultView.ToTable(true);
                if (dtDelete_CustomStore != null)
                {
                    for (int i = 0; i < dtDelete_CustomStore.Rows.Count; i++)
                    {
                        Int32 StoreGroupId = Convert.ToInt32(dtDelete_CustomStore.Rows[i]["StoreGroupId"]);
                        string isShared = Convert.ToString(dtDelete_CustomStore.Rows[i]["isShared"]);
                        if (isShared.ToLower() == "no")
                        {
                            ViewState["_DeleteCustomStore"] = "1";
                            DeleteCustomStoreGroup(Convert.ToInt32(StoreGroupId));
                            _delMultiples += 1;
                            ViewState["_DeleteCustomStore"] = null;
                        }
                    }
                }

                if (_delMultiples > 1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                           "success_msg", "alert('" + IcontrolFilterResources.CustomStoreDeleted + "')", true);
                }
                else if (_delMultiples == 1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                               "success_msg", "alert('" + IcontrolFilterResources.CustomStoreDeleted + "')", true);
                }
                divSearchResult.Visible = true;
                grdMembers.Visible = true;
                grdMembers.Rebind();
                uncheckAllCheckbox();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > btnDeleteMultipleStores_Click"));
            }
        }
        #endregion


        #region Checkbox Events

        protected void chk_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
                if (!(sender as CheckBox).Checked)
                {
                    foreach (GridHeaderItem headerItem in grdMembers.MasterTableView.GetItems(GridItemType.Header))
                    {
                        CheckBox chkall = (CheckBox)headerItem.FindControl("chkSelectAll");
                        chkall.Checked = false;
                        ViewState["IsSelectAll"] = "false";
                    }
                }

                DataTable dt;
                if (Session["dtCustomStoreIDs"] == null)
                {
                    dt = new DataTable();
                    dt.Columns.Add("StoreGroupId");
                    dt.Columns.Add("isShared");
                }
                else
                {
                    dt = (Session["dtCustomStoreIDs"] as DataTable).DefaultView.ToTable(true);
                }

                GridDataItem currentrow = (sender as CheckBox).NamingContainer as GridDataItem;
                Int32 StoreGroupId = Convert.ToInt32(((HiddenField)currentrow.FindControl("HfStoreID")).Value);
                string isShared = ((HiddenField)currentrow.FindControl("hfIsShared")).Value;

                ((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
                CheckBox chk = (CheckBox)currentrow.FindControl("chk");

                if (chk.Checked && isShared.ToLower() == "no")
                {
                    btnDeleteMultiple.Visible = true;
                    if (dt.Select("StoreGroupId = '" + Convert.ToString(StoreGroupId) + "'").Length == 0)
                    {
                        DataRow dr = dt.NewRow();
                        dr["StoreGroupId"] = StoreGroupId;
                        dr["isShared"] = isShared;
                        dt.Rows.Add(dr);
                    }
                    Session["dtCustomStoreIDs"] = dt;
                    upRequest.Update();
                }
                else
                {
                    if (dt != null)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataRow dr = dt.Rows[i];
                            if (Convert.ToInt32(dr["StoreGroupId"]) == StoreGroupId)
                            {
                                dt.Rows.Remove(dr);
                            }
                        }
                        dt.AcceptChanges();
                        Session["dtCustomStoreIDs"] = dt;
                        if (dt.Rows.Count == 0)
                        {
                            btnDeleteMultiple.Visible = false;
                            upRequest.Update();
                        }
                    }
                }
                Int32 _checkedCount = 0;
                Int32 _noSharedCount = 0;
                Int32 _gridCount = grdMembers.MasterTableView.Items.Count;

                foreach (GridItem dataItem in grdMembers.MasterTableView.Items)
                {
                    if (dataItem is GridDataItem)
                    {
                        CheckBox chkBox = (CheckBox)dataItem.FindControl("chk");
                        string _isShared = ((HiddenField)dataItem.FindControl("hfIsShared")).Value;
                        if (chkBox.Checked && _isShared.ToLower() == "no")
                            _checkedCount += 1;
                        if (_isShared.ToLower() == "yes")
                            _noSharedCount += 1;
                    }
                }
                _checkedCount += _noSharedCount;
                if (_checkedCount == _gridCount)
                {
                    foreach (GridHeaderItem headerItem in grdMembers.MasterTableView.GetItems(GridItemType.Header))
                    {
                        CheckBox chkall = (CheckBox)headerItem.FindControl("chkSelectAll");
                        chkall.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > chk_CheckedChanged"));
            }
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //Session["dtCustomStoreIDs"] = null;
                CheckBox headerCheckBox = (sender as CheckBox);
                foreach (GridDataItem dataItem in grdMembers.MasterTableView.Items)
                {
                    (dataItem.FindControl("chk") as CheckBox).Checked = headerCheckBox.Checked;
                    dataItem.Selected = headerCheckBox.Checked;
                }

                DataTable dt;
                if (Session["dtCustomStoreIDs"] == null)
                {
                    dt = new DataTable();
                    dt.Columns.Add("StoreGroupId");
                    dt.Columns.Add("isShared");
                }
                else
                {
                    dt = (Session["dtCustomStoreIDs"] as DataTable).DefaultView.ToTable(true);
                }

                if (headerCheckBox.Checked == true)
                {
                    btnDeleteMultiple.Visible = true;

                    foreach (GridItem item in grdMembers.MasterTableView.Items)
                    {
                        if (item is GridDataItem)
                        {
                            CheckBox chkBox = (CheckBox)item.FindControl("chk");
                            string isShared = ((HiddenField)item.FindControl("hfIsShared")).Value;
                            if (chkBox.Checked && isShared.Trim().ToLower() == "no")
                            {
                                Int32 StoreGroupId = Convert.ToInt32(((HiddenField)item.FindControl("HfStoreID")).Value);

                                if (dt.Select("StoreGroupId = '" + Convert.ToString(StoreGroupId) + "'").Length == 0)
                                {
                                    DataRow dr = dt.NewRow();
                                    dr["StoreGroupId"] = StoreGroupId;
                                    dr["isShared"] = isShared;
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                    }
                    if (dt.Rows.Count > 0)
                    {
                        Session["dtCustomStoreIDs"] = dt;
                    }
                }
                else
                {
                    if (dt != null)
                    {
                        foreach (GridItem item in grdMembers.MasterTableView.Items)
                        {
                            if (item is GridDataItem)
                            {
                                Int32 _StoreGroupId = Convert.ToInt32(((HiddenField)item.FindControl("HfStoreID")).Value);
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    DataRow dr = dt.Rows[i];
                                    if (Convert.ToInt32(dr["StoreGroupId"]) == _StoreGroupId)
                                    {
                                        dt.Rows.Remove(dr);
                                    }
                                }
                                dt.AcceptChanges();
                                Session["dtCustomStoreIDs"] = dt;
                            }
                        }
                    }
                }
                if (dt.Rows.Count == 0)
                    btnDeleteMultiple.Visible = false;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > chkSelectAll_CheckedChanged"));
            }
        }
        #endregion

        #region Other Methods      
        protected DataTable ValidateStore()
        {
            DataTable dtErrorLog = new DataTable();
            try
            {
                dtErrorLog.Columns.Add("Row No", typeof(System.Int32));
                dtErrorLog.Columns.Add("UserID", typeof(System.String));
                dtErrorLog.Columns.Add("Username", typeof(System.String));
                dtErrorLog.Columns.Add("TotalStoreNo", typeof(System.String));

                int numRows = 0;
                foreach (RadComboBoxItem checkeditem in RdShareUsers.CheckedItems)
                {
                    int totalUnauthorizedUPC = 0;
                    // validate UPC value
                    DataTable dt = new DataTable();
                    dt = AppManager.GetCustomStoresShared(AppConstants.GET_STORE_NUMBER_FROM_STORE_DETAILS, Convert.ToInt32(HfShare.Value));
                    string RecordID = "";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataTable Data = new DataTable();
                        string storeNo = dt.Rows[i]["STORE_NUMBER"].ToString();
                        Data = AppManager.GetStoreNumber(storeNo.Trim().ToUpper().Replace("'", "''"));
                        RecordID = Data.Rows[0]["STORE_NUMBER"].ToString();
                        if (RecordID == "")
                        {
                            totalUnauthorizedUPC += 1;
                        }
                    }
                    if (totalUnauthorizedUPC > 0)
                    {
                        DataTable dt1 = new DataTable();
                        dt1 = AppManager.GetNameFromUserDataAccess(checkeditem.Value.ToString());
                        DataRow dr = dtErrorLog.NewRow();
                        dr["Row No"] = numRows + 1;
                        dr["UserID"] = checkeditem.Value.ToString();
                        dr["Username"] = dt1.Rows[0]["NAME"].ToString();
                        dr["TotalStoreNo"] = totalUnauthorizedUPC;
                        dtErrorLog.Rows.Add(dr);
                        numRows++;
                    }
                }
            }

            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > ValidateUPC"));
            }
            return dtErrorLog;
        }
        private void SharedUsersEmail(string chkeduser)
        {
            try
            {
                DataTable dtStores = AppManager.GetCustomStoresAndUserDataAccess(HfShare.Value.ToString());

                DataTable dtUserDataAccess = AppManager.GetUserDataAccessForEmailLNotNull(chkeduser.ToString());

                string toEmail = dtUserDataAccess.Rows[0]["Email"].ToString().Trim();
                string sharedBy = dtStores.Rows[0]["NAME"].ToString();
                string sharedTo = dtUserDataAccess.Rows[0]["Name"].ToString();
                string sharedStoreName = dtStores.Rows[0]["CUSTOM_STORE_NAME"].ToString();
                string storeDes = dtStores.Rows[0]["CUSTOM_STORE_DESCRIPTION"].ToString();

                if (toEmail.Length > 0)
                {
                    SendShareEmail(toEmail, sharedBy, sharedTo, sharedStoreName, storeDes);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > SharedUsersEmail"));
            }
        }
        protected void CustomStore_ExportFile(Int32 CustomStoreId_)
        {
            try
            {
                string strfileName = "";
                DataTable dtExport = AppManager.GetCustomStoresShared(AppConstants.GET_CUSTOM_STORE_DETAILS, CustomStoreId_);
                if (dtExport.Rows.Count > 0)
                {
                    string CustomStore_Name = dtExport.Rows[0]["CUSTOM_STORE_NAME"].ToString();
                    try
                    {
                        //Remove illegal characters from path or filenames
                        string illegalCharacters = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                        Regex rgx = new Regex(string.Format("[{0}]", Regex.Escape(illegalCharacters)));
                        CustomStore_Name = rgx.Replace(CustomStore_Name, "");
                    }
                    catch (Exception e) { }
                    strfileName = CustomStore_Name + "_" + DateTime.Now.ToString("MMddyyHHmmss");
                }
                else
                    strfileName = "No_Records" + "_" + DateTime.Now.ToString("MMddyyHHmmss");

                DataView viewData = new DataView(dtExport);
                dtExport = viewData.ToTable("dtExport", false, "CUSTOM_LEVEL_1", "CUSTOM_LEVEL_2", "CUSTOM_LEVEL_3", "STORE_NUMBER");

                string filePath = Server.MapPath("~") + "Exports\\";
                string filename = filePath + strfileName.Replace(" ", "") + ".xlsx";

                Microsoft.Office.Interop.Excel.Application xlApp;
                Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;
                xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();

                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                xlWorkSheet.Name = "Custom Store";

                Microsoft.Office.Interop.Excel.Range chartRange;
                xlApp.StandardFont = "Arial"; //Arial Unicode MS  //Calibri
                xlApp.StandardFontSize = 10;

                //** Columns Name properties
                chartRange = xlWorkSheet.get_Range("A1", "D1");
                chartRange.Font.Name = "Arial";
                chartRange.Font.Size = 10;
                chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                chartRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignTop;
                chartRange.WrapText = true;
                chartRange.Font.Bold = true;

                //** Columns Name
                chartRange = xlWorkSheet.get_Range("A1");
                chartRange.ColumnWidth = 30;
                chartRange.Value = "Custom Level 1";

                chartRange = xlWorkSheet.get_Range("B1");
                chartRange.ColumnWidth = 30;
                chartRange.Value = "Custom Level 2";

                chartRange = xlWorkSheet.get_Range("C1");
                chartRange.ColumnWidth = 30;
                chartRange.Value = "Custom Level 3";

                chartRange = xlWorkSheet.get_Range("D1");
                chartRange.ColumnWidth = 30;
                chartRange.Value = "Store Number";

                //** Report Data Columns
                int rowCount = 2;
                xlApp.StandardFont = "Calibri";
                if (dtExport.Rows.Count != 0)
                {
                    //dtExport.DefaultView.Sort = "UPC desc";
                    dtExport = dtExport.DefaultView.ToTable();
                    foreach (DataRow dr in dtExport.Rows)
                    {
                        chartRange = xlWorkSheet.get_Range("A" + rowCount);
                        chartRange.WrapText = true;
                        chartRange.ColumnWidth = 30;
                        chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                        chartRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignTop;
                        chartRange.Value = dr["CUSTOM_LEVEL_1"].ToString();

                        chartRange = xlWorkSheet.get_Range("B" + rowCount);
                        chartRange.WrapText = true;
                        chartRange.ColumnWidth = 30;
                        chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                        chartRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignTop;
                        chartRange.Value = dr["CUSTOM_LEVEL_2"].ToString();

                        chartRange = xlWorkSheet.get_Range("C" + rowCount);
                        chartRange.ColumnWidth = 30;
                        chartRange.WrapText = true;
                        chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                        chartRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignTop;
                        chartRange.Value = dr["CUSTOM_LEVEL_3"].ToString();

                        chartRange = xlWorkSheet.get_Range("D" + rowCount);
                        chartRange.ColumnWidth = 20;
                        chartRange.WrapText = true;
                        chartRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                        chartRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignTop;
                        chartRange.NumberFormat = "@";
                        chartRange.Value = dr["STORE_NUMBER"].ToString();

                        rowCount++;
                    }
                }
                try
                {
                    xlApp.DisplayAlerts = false;
                    xlWorkBook.SaveAs(filename, misValue, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, misValue, misValue, misValue, misValue);
                    xlWorkBook.Close(true, misValue, misValue);
                    releaseObject(xlApp);
                    releaseObject(xlWorkBook);
                    releaseObject(xlWorkSheet);
                }
                catch (Exception ex)
                {
                    xlWorkBook.Close(true, misValue, misValue);
                    releaseObject(xlApp);
                    releaseObject(xlWorkBook);
                    releaseObject(xlWorkSheet);
                    //throw ex;
                }
                //string path = MapPath(filename);
                byte[] bts = System.IO.File.ReadAllBytes(filename);
                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                Response.AddHeader("Content-Length", bts.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strfileName.Replace(" ", "") + ".xlsx");
                Response.BinaryWrite(bts);
                Response.Flush();
                HttpContext.Current.Response.End();

            }
            catch (ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > CustomStore_ExportFile"));
            }
        }
        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                //MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        protected void uncheckAllCheckbox()
        {
            try
            {
                Session["dtCustomStoreIDs"] = null;
                //uncheck all 
                foreach (GridHeaderItem headerItem in grdMembers.MasterTableView.GetItems(GridItemType.Header))
                {
                    CheckBox chkall = (CheckBox)headerItem.FindControl("chkSelectAll");
                    chkall.Checked = false;
                    ViewState["IsSelectAll"] = "false";
                }

                foreach (GridDataItem dataItem in grdMembers.MasterTableView.Items)
                {
                    (dataItem.FindControl("chk") as CheckBox).Checked = false;
                    dataItem.Selected = false;
                }

                btnDeleteMultiple.Visible = false;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > uncheckAllCheckbox"));
            }
        }
        private void ShareDeleteEmail(Int64 CustomStoreId, DataTable dtgetShared)
        {
            try
            {
                for (int i = 0; i < dtgetShared.Rows.Count; i++)
                {
                    DataTable dtUserDataAccess = AppManager.GetUserDataAccessForEmailLNotNull(dtgetShared.Rows[i]["USERNAME"].ToString());

                    DataTable dtHierarchies = AppManager.GetCustomStoresShared(AppConstants.GET_CUSTOM_STORES_SHARED_MASTER, CustomStoreId);

                    string toEmail = dtUserDataAccess.Rows[0]["Email"].ToString().Trim();
                    string deletedBy = Session["UserName"].ToString();
                    string sharedWith = dtUserDataAccess.Rows[0]["Name"].ToString();
                    string sharedStoreName = dtHierarchies.Rows[0]["CUSTOM_STORE_NAME"].ToString();
                    string StoreDes = dtHierarchies.Rows[0]["CUSTOM_STORE_DESCRIPTION"].ToString();

                    if (toEmail.Length > 0)
                    {
                        SendDeleteEmail(toEmail, deletedBy, sharedWith, sharedStoreName, StoreDes);
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > ShareDeleteEmail"));
            }
        }
        protected void DeleteCustomStoreGroup(Int32 StoreGroupId)
        {
            try
            {
                DataTable dtgetShared = AppManager.GetCustomStoresShared(AppConstants.GET_CUSTOM_STORES_SHARED, StoreGroupId);

                if (dtgetShared.Rows.Count > 0)
                {
                    ShareDeleteEmail(StoreGroupId, dtgetShared);
                }

                AppManager.DeleteCustomStoreGroup(StoreGroupId);
                grdMembers.Rebind();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                                      "Information",
                                                      "alert('"+ IcontrolFilterResources.CustomStoreDeleted + "');",
                                                       true);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > DeleteCustomStoreGroup"));
            }
        }
        private void CheckAlreadySharedUser()
        {
            try
            {
                string shareUsers = "";
                int selected = 0;
                DataTable dt = new DataTable();
                foreach (RadComboBoxItem checkeditem in RdShareUsers.CheckedItems)
                {
                    int CustomStoreId = Convert.ToInt32(HfShare.Value);
                    String CurrDate = DateTime.Now.ToString();

                    dt = AppManager.GetCustomStoresSharedOnUsername(checkeditem.Value, CustomStoreId);
                    if (dt.Rows.Count > 0)
                    {
                        shareUsers += "- " + checkeditem.Value + " \\n";
                    }
                    else
                    {
                        SharedUsersEmail(checkeditem.Value);
                        AppManager.InsertIntoCustomStoresShared(CustomStoreId, checkeditem.Value, CurrDate);
                        selected = 1;
                    }
                }
                if (selected == 1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                                   "Confirmation Message",
                                                   "alert('"+IcontrolFilterResources.CustomStoreShared + "');",
                                                   true);
                }
                else
                {
                    mpeShare.Show();
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                                  "err_msg",
                                                  "alert('"+IcontrolFilterResources.CustomStoreAlreadyShared + "" + shareUsers + "');",
                                                 true);
                }
                grdMembers.Rebind();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > CheckAlreadySharedUser"));
            }
        }

        #endregion

        #region SendEmail

        public void SendShareEmail(string ToAddress, string SharedBy, string ShareTo, string StoreName, string StoreDes)
        {
            try
            {
                String _EmailSubject = SharedBy + " has shared a Custom Store with you.";
                string sbMessage = "<body>";
                sbMessage += "<table style='width: 100%;'>";
                sbMessage += "<tr>";
                sbMessage += "<td>Dear " + ShareTo + ",</td>";
                sbMessage += "</tr>";
                sbMessage += "<span></span><tr style='padding-top:20px;'>";
                sbMessage += "<td colspan='2'>" + SharedBy + " has shared the following Custom Store Group with you :</td>";
                sbMessage += "</tr>";
                sbMessage += "<tr><td colspan='2'>&nbsp;</td></tr>";
                sbMessage += "<tr style='padding-top:10px;width: 24%;'><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Store Name:</td><td style='word-wrap: break-word;'>" + StoreName + "</td></tr>";
                sbMessage += "<tr><td style='width: 25%;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Store Description:</td><td style='word-wrap: break-word;'>&nbsp;&nbsp;" + StoreDes + "</td></tr>";
                sbMessage += "<tr><td colspan='2'>&nbsp;</td></tr>";
                sbMessage += "<tr style='padding-top:20px;'>";
                sbMessage += "<td> Thanks</td>";
                sbMessage += "</tr>";
                sbMessage += "</table> </body>";
                AppManager.SendEmail(sbMessage.ToString(), _EmailSubject, "info@icontroldsd.com", ToAddress, "", IcontrolFilterResources.MailCC);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > SendShareEmail"));
            }
        }

        public void SendDeleteEmail(string ToAddress, string DeletedBy, string ShareTo, string StoreName, string StoreDes)
        {
            try
            {
                String _EmailSubject = DeletedBy + " has deleted a Custom Store.";
                string sbMessage = "<body>";
                sbMessage += "<table cellpadding='0' cellspacing='0' border='0'>";
                sbMessage += "<tr>";
                sbMessage += "<td>Dear " + ShareTo + ",</td>";
                sbMessage += "</tr>";
                sbMessage += "<span></span><tr style='padding-top:20px;'>";
                sbMessage += "<td colspan='2'>" + DeletedBy + " has deleted the following Custom Store Group :</td>";
                sbMessage += "</tr>";
                sbMessage += "<tr><td colspan='2'>&nbsp;</td></tr>";
                sbMessage += "<tr style='padding-top:10px;width: 24%;'><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Store Name:</td><td style='word-wrap: break-word;'>" + StoreName + "</td></tr>";
                sbMessage += "<tr><td style='width: 25%;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Store Description:</td><td style='word-wrap: break-word;'>&nbsp;&nbsp;" + StoreDes + "</td></tr>";
                sbMessage += "<tr><td colspan='2'>&nbsp;</td></tr>";
                sbMessage += "<tr style='padding-top:20px;'>";
                sbMessage += "<td  colspan='2'>This Custom Store is no longer available.</td>";
                sbMessage += "</tr>";
                sbMessage += "<tr><td colspan='2'>&nbsp;</td></tr>";
                sbMessage += "<tr style='padding-top:20px;'>";
                sbMessage += "<td> Thanks</td>";
                sbMessage += "</tr>";
                sbMessage += "</table> </body>";
                AppManager.SendEmail(sbMessage.ToString(), _EmailSubject, "info@icontroldsd.com", ToAddress, "", IcontrolFilterResources.MailCC);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageCustomStoreGroup.cs > SendDeleteEmail"));
            }
        }

        #endregion
    }
}