﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserHelp.aspx.cs" Inherits="iControlGenricFilters.ProjectForms.UserHelp" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style>
        #ctl00_MainContent_grdReortsFilter .rgHeader > a, #ctl00_MainContent_grdReortsFilter .rgCurrentPage {
            color: #333333 !important;
        }

        .video-sec {
            background: #fff;
            border: 1px solid #ddd;
            width: 15%;
            float: left;
        }

        .img-sec {
            width: 250px;
            height: 250px;
            background-size: cover;
            filter: alpha(opacity=30);
            -moz-opacity: 0.3;
            -khtml-opacity: 0.3;
            opacity: 0.3;
        }

        #content .container, .content1 .container, div .layout.modalPopup, div#layout.modalPopup {
            background: none;
        }

        .RadGrid_Default .rgRow a, .RadGrid_Default .rgAltRow a {
            color: Blue !important;
            text-decoration: underline;
        }

            .RadGrid_Default .rgRow a[disabled], .RadGrid_Default .rgAltRow a[disabled] {
                color: black !important;
                text-decoration: none;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="divMain" runat="server">

        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Transparent" />
        <div id="tabs6">
            <ul>
                <li>
                    <asp:LinkButton ID="lnkUserGuide" CssClass="active" runat="server" OnClick="lnkUserGuide_Click"><span>User Guide</span></asp:LinkButton></li>
                <li>
                    <asp:LinkButton ID="lnkTrainingVideos" runat="server" OnClick="lnkTrainingVideos_Click"><span>Training Videos</span></asp:LinkButton></li>
            </ul>
        </div>
        <br />
        <asp:MultiView ID="multiTabs" ActiveViewIndex="0" runat="server">
            <asp:View ID="viewUserGuide" runat="server">
                <div class="layout" id="divUserGuide" runat="server" style="outline: none; border: 1px solid #828282;">
                    <div style="clear: both; min-height: 20px; overflow: hidden;">
                    </div>
                    <telerik:RadGrid ID="gridUserGuide" runat="server" PageSize="25" AllowPaging="true"
                        Style="outline: none; border: 1px solid #828282;" AutoGenerateColumns="false"
                        GridLines="Both" AllowSorting="true" BorderStyle="None" CssClass="layout rgRowRad rgAltRow" OnNeedDataSource="gridUserGuide_NeedDataSource"
                        OnItemCommand="gridUserGuide_ItemCommand" OnItemDataBound="gridUserGuide_ItemDataBound">
                        <ClientSettings AllowDragToGroup="true">
                            <Scrolling AllowScroll="True" UseStaticHeaders="true" SaveScrollPosition="true"></Scrolling>
                        </ClientSettings>
                        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False"></GroupingSettings>
                        <MasterTableView PageSize="50" PagerStyle-AlwaysVisible="true" AllowPaging="true"
                            AllowSorting="true">
                            <HeaderStyle Wrap="true" Font-Bold="true" />
                            <ItemStyle Wrap="true" />
                            <AlternatingItemStyle Wrap="true" />
                            <PagerStyle Mode="NextPrevAndNumeric" PageSizeLabelText="Records Per Page :" PageSizes="{50, 100, 200, 250}" />
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="Category" DataField="Directory.Name" HeaderStyle-Width="10%">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Document Name" DataField="Name" HeaderStyle-Width="30%">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Document Type" DataField="Extension" HeaderStyle-Width="10%">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Document Size" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <%# string.Format("{0:#,### KB}", Math.Round(Convert.ToDecimal(Eval("Length")) / Convert.ToDecimal(1024), 2))%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderStyle-Width="10%" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDownload" runat="server" CommandArgument='<%# Eval("Name") %>'
                                            CommandName="DownloadFile" Text="DOWNLOAD"></asp:LinkButton>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderStyle-Width="10%" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("Name") %>'
                                            CommandName="DeleteFile" Text="DELETE" OnClientClick="return confirm('Are you sure want to delete this file?')"></asp:LinkButton>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                            <NoRecordsTemplate>
                                <div class="search-noresults-container" style="text-align: center; padding: 50px 0;">
                                    <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/themeimages/search_icon_grid.png" /><br />
                                    <br />
                                    <div class="inner-content">
                                        <h5 style="margin: 0 0 1.75em;">
                                            <strong>No Results Found</strong></h5>
                                        <p class="small-text">
                                            Please verify the search criteria and try again
                                        </p>
                                    </div>
                                </div>
                            </NoRecordsTemplate>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <div style="clear: both; min-height: 20px; overflow: hidden;">
                    </div>
                </div>
            </asp:View>
            <asp:View ID="viewTrainingVideos" runat="server">
                <div id="divTrainingVideos" class="layout" style="outline: none; border: 1px solid #828282;" runat="server">
                    <div style="clear: both; min-height: 10px; overflow: hidden;">
                    </div>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center">
                                <table width="99%">
                                    <tr>
                                        <td align="left" style="vertical-align: top; border: 1px solid gray; background-color: Black;"
                                            align="center">
                                            <table width="100%" style="height: 100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left">
                                                        <iframe width="100%" height="720" frameborder="0" allowfullscreen="" id="videomain"
                                                            runat="server"></iframe>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="30%" valign="top" style="padding-left: 10px;">
                                            <div style="height: 720px; overflow: auto; border: 1px solid #ccc !important; background-color: White;">
                                                <asp:GridView ID="gridTrainingVideos" runat="server" AutoGenerateColumns="false"
                                                    GridLines="None" Style="outline: none;" CssClass="layout"
                                                    ShowFooter="false" ShowHeader="false" OnRowCommand="gridTrainingVideos_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td valign="top" style="border-bottom: 1px solid #ccc !important; vertical-align: top;">
                                                                            <table width="100%" border="0" style="height: 90px !important;">
                                                                                <tr>
                                                                                    <td valign="top" style="vertical-align: top; padding: 7px;" width="120px">
                                                                                        <img src="http://i.ytimg.com/vi/<%# ReplaceSpecialChar(Eval("VideoUrl").ToString())%>/default.jpg" />
                                                                                    </td>
                                                                                    <td valign="top" style="vertical-align: top; padding: 7px;">
                                                                                        <h2 style="opacity: 0.9 !important;">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("VideoId")%>'
                                                                                                Text='<%#Eval("VideoName")%>' Style="color: #444; font-weight: bold;" Font-Size="16px"
                                                                                                CommandName="DownloadFile"> 
                                                                                            </asp:LinkButton>
                                                                                        </h2>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>
                    <div style="clear: both; min-height: 10px; overflow: hidden;">
                    </div>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>
