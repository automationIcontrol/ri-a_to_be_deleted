﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageSavedReports.aspx.cs" Inherits="iControlGenricFilters.ManageSavedReports" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="~/Controls/MyDate.ascx" TagName="MyDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/Stdate.ascx" TagName="MystartDate" TagPrefix="stdate1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery.maskedinput-1.3.js" type="text/javascript"></script>
    <script src="Scripts/Custom/ManagedSavedReports.js" type="text/javascript"></script>
    <style type="text/css">
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="divMain" runat="server">
        <div id="contentHeader">
            <h1 id="topHeader" runat="server">
                Manage Saved Reports
            </h1>
        </div>
        <asp:UpdatePanel ID="upRequest" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlSearhFilters" runat="server" DefaultButton="btnSearch">
                    <div class="layout">
                        <div class="beige-gradient" style="top: 0px; ">


                           
                            <table class="form uniformForm" width="100%" style="padding: 0 10px 10px 10px;">
                                <tr style="height: 25px;">
                                    <td style="font-weight: bold; font-size: 15px;">
                                        Search Existing Reports
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="row ">
                                             
                                                <label class="col-custom-1 pd-r-0  line-h-30 col-xs-12">Saved Name :  </label>
                                            <div class="col-sm-3 col-xs-12">
                                                <telerik:RadTextBox ID="txtSearchText" runat="server">
                                                </telerik:RadTextBox>
                                                </div>
                                                 <div class="col-sm-3 col-xs-12">
                                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btns btn-small btn-blue"
                                                        OnClick="btnSearch_Click" />
                                                     </div>
                                               
                                           
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:Panel>
                <div class="ribbon-bar-gradient top">
                    <asp:ImageButton ID="btnZip" ImageUrl="~/images/zip.png" runat="server" AlternateText="Export ZIP"
                        OnClick="btnExportZip_Click" />
                    <asp:ImageButton ID="btnExport" ImageUrl="~/Images/excel.png" ToolTip="Click to export"
                        runat="server" AlternateText="Export CSV" OnClick="btnExport_Click" />
                    <div style="text-align: center;">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <div style="text-align: center">
                                    <img src="../Images/loader.gif" alt="loader" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                </div>
                <div class="layout" id="divSearchResult" runat="server">
                    <telerik:RadGrid ID="grdMembers" runat="server" PageSize="25" AllowPaging="true"
                        AutoGenerateColumns="false" GridLines="Both" AllowSorting="true" BorderStyle="None"
                        CssClass="rgMasterTable rgClipCells" OnNeedDataSource="grdMembers_NeedDataSource"
                        OnItemCommand="grdMembers_ItemCommand" OnItemDataBound="grdMembers_ItemDataBound">
                        <ClientSettings AllowDragToGroup="true">
                            <Scrolling AllowScroll="True" UseStaticHeaders="true" SaveScrollPosition="true">
                            </Scrolling>
                        </ClientSettings>
                        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False"></GroupingSettings>
                        <MasterTableView PageSize="50" PagerStyle-AlwaysVisible="true" AllowPaging="true"
                            EditMode="InPlace" AllowSorting="true">
                            <HeaderStyle Wrap="true" />
                            <ItemStyle Wrap="true" />
                            <AlternatingItemStyle Wrap="true" />
                            <PagerStyle Mode="NextPrevAndNumeric" PageSizeLabelText="Records Per Page :" PageSizes="{25, 50, 100, 200,250}" />
                            <Columns>
                                <telerik:GridTemplateColumn>
                                    <ItemStyle HorizontalAlign="Left" Width="2%" Wrap="true" />
                                    <HeaderStyle HorizontalAlign="Left" Width="2%" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgshare" runat="server" Style="cursor: default;" Enabled="false" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn>
                                    <ItemStyle HorizontalAlign="Left" Width="3%" />
                                    <HeaderStyle HorizontalAlign="Left" Width="3%" />
                                    <ItemTemplate>
                                        <asp:Image ID="imgsubscribe" runat="server" ImageUrl="~/Images/timer.png" Visible="false"
                                            Height="15px" Width="15px" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Saved Name" SortExpression="ReportName">
                                    <ItemStyle HorizontalAlign="Left" Width="20%" Wrap="true" />
                                    <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnScheduleId" runat="server" Value='<%#Eval("ScheduleId") %>' />
                                        <asp:Label ID="lblNewsTitle" runat="server" Text='<%#Eval("ReportName") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Report Name" SortExpression="MenuDescription">
                                    <ItemStyle HorizontalAlign="Left" Width="25%" />
                                    <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblReportName" runat="server" Text='<%#Eval("MenuDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Description" SortExpression="ReportDescription">
                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblPostedBy" runat="server" Text='<%#Eval("ReportDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Email Id" SortExpression="Emailid" UniqueName="Emailid">
                                    <ItemStyle HorizontalAlign="Left" Width="30%" Wrap="false" />
                                    <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                    <ItemTemplate>
                                        <asp:Label ID="LblEmail" runat="server" Text='<%#Eval("Emailid") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Date Selection" HeaderStyle-Font-Bold="true"
                                    HeaderStyle-Font-Size="14px">
                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                    <ItemTemplate>
                                        <%#GetDatename(Eval("DateRange").ToString()) %>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Report Frequency" SortExpression="ReportFrequency">
                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblReportFrequency" runat="server" Text='<%#Eval("ReportFrequency") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Shared By" SortExpression="Shareby">
                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblshare" runat="server" Text='<%#Eval("ShareBy") %>'></asp:Label>
                                        <asp:HiddenField ID="hfmsg" runat="server" Value='<%#Eval("ShareMessage") %>' />
                                        <asp:HiddenField ID="hfsharedby" runat="server" Value='<%#Eval("Shareby") %>' />
                                        <asp:HiddenField ID="HFID" runat="server" Value='<%#Eval("ReportID") %>' />
                                        <asp:HiddenField ID="HfReadStatus" runat="server" Value='<%#Eval("ReadStatus") %>' />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn>
                                    <ItemStyle HorizontalAlign="Left" Width="15%" CssClass="MessageIcon" />
                                    <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                    <ItemTemplate>
                                        <%-- <asp:ImageButton ID="btnmessage" runat="server" Width="23px" CommandArgument='<%#Eval("ReportId") %>'
                                            CommandName="ViewMsg" />--%>
                                        <asp:HiddenField ID="hdnDeleteStatus" runat="server" Value='<%#Eval("Is_Deleted") %>' />
                                        <asp:LinkButton ID="btnmessage" runat="server" CommandArgument='<%#Eval("ReportId") %>'
                                            CommandName="ViewMsg"></asp:LinkButton>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Saved On" SortExpression="DateTimeUpdated">
                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                    <ItemTemplate>
                                        <%#GetDateFormat(Convert.ToString(Eval("DateTimeUpdated")))%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn>
                                    <ItemStyle HorizontalAlign="left" Width="20%" />
                                    <HeaderStyle HorizontalAlign="left" Width="20%" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" CommandName="Edit" CommandArgument='<%#Eval("ReportId") %>'
                                            Style="color: blue !important; text-decoration: underline !important; font-size: 14px !important;
                                            text-transform: uppercase !important;" runat="server">Edit</asp:LinkButton>&nbsp;
                                        <asp:LinkButton ID="btnDelete" CommandName="Delete" CommandArgument='<%#Eval("ReportId") %>'
                                            Style="color: blue !important; text-decoration: underline !important; font-size: 14px !important;
                                            text-transform: uppercase !important;" runat="server" Text="Delete" OnClientClick="return confirm ('Are you sure you want to Delete this Report ?');"> </asp:LinkButton>&nbsp;
                                        <asp:LinkButton ID="BtnView" CommandName="ViewRep" CommandArgument='<%#Eval("ReportId") %>'
                                            Style="color: blue !important; text-decoration: underline !important; font-size: 14px !important;
                                            text-transform: uppercase !important;" runat="server" Text="View"> </asp:LinkButton>&nbsp;
                                        <asp:HiddenField ID="hfparameters" runat="server" Value='<%#Eval("reportparameters") %>' />
                                        <asp:LinkButton ID="Lnkshare" CommandName="ShareRep" CommandArgument='<%#Eval("ReportId") %>'
                                            Style="color: blue !important; text-decoration: underline !important; font-size: 14px !important;
                                            text-transform: uppercase !important;" runat="server" Text="Share"> </asp:LinkButton>&nbsp;
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                            <NoRecordsTemplate>
                                <div class="search-noresults-container">
                                    <asp:Image ID="imgSearchIcon" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/themeimages/search_icon.png" /><br />
                                    <br />
                                    <div class="inner-content">
                                        <p>
                                            <strong>No Results Found</strong></p>
                                        <p class="small-text">
                                            Please verify the search criteria and try again</p>
                                    </div>
                                </div>
                            </NoRecordsTemplate>
                        </MasterTableView>
                    </telerik:RadGrid>
                </div>
                <!--cheesy button for the modal popups target control-->
                <telerik:RadWindowManager ID="rdWindow" runat="server">
                </telerik:RadWindowManager>
                <asp:Button ID="btnHiddenRequest" runat="Server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender ID="mpeRequest" runat="server" TargetControlID="btnHiddenRequest"
                    PopupControlID="pnlRequest" BackgroundCssClass="fade"
                    Drag="false">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel Style="display: none; " CssClass="modalPopup" ID="pnlRequest"
                    DefaultButton="BtnUpdate" runat="server">
                    <div class="changeclienttitleReports"  style="width:100%;background: #515151 !important;">
                        <span id="ctl00_Label3">Update Report</span>
                    </div>
                    <table width="100%">
                        <tr>
                            <td style="width: 35%; padding: 5px 10px;">
                                Report Name:
                            </td>
                            <td class="input" style="width: 65%; padding: 5px 10px;">
                                <asp:TextBox ID="txtReportName" runat="server" MaxLength="205" onKeyUp="Count(this,200,'Report Name')"
                                    onChange="Count(this,200,'Report Name')"></asp:TextBox><br />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 35%; padding: 5px 10px;">
                                Description:
                            </td>
                            <td class="input" style="width: 65%; padding: 5px 10px;">
                                <asp:TextBox ID="txtReportDescr" runat="server"  TextMode="MultiLine"
                                    onKeyUp="Count(this,1000,'Description')" onChange="Count(this,1000,'Description')"
                                    MaxLength="1000" Rows="2"></asp:TextBox><br />
                                <span style="font-size: 11px; ">(Description must not exceed 1000
                                    characters)</span>
                            </td>
                        </tr>
                        <asp:Panel ID="Pnlschedule" runat="server">
                         
                            <asp:Panel ID="pnldates" runat="server">
                                <tr>
                                    <td style="width: 35%; padding: 5px 10px;">
                                        Relative Date In Last
                                    </td>
                                    <td class="input" style="width: 65%; padding: 5px 10px;">
                                        Last&nbsp;
                                        <asp:DropDownList ID="DrpDateVal" Width="50px" runat="server">
                                        </asp:DropDownList>
                                        Week(s)
                                    </td>
                                </tr>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlscheduleoption" runat="server">
                            <tr>
                                <td style="width: 35%; padding: 5px 10px;">
                                    Subscribe to?
                                </td>
                                <td class="input" style="padding: 0 !important;" align="left">
                                    <asp:RadioButtonList ID="rdSchedule" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                        AutoPostBack="true" OnSelectedIndexChanged="rdSchedule_SelectedIndexChanged">
                                        <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                        <asp:ListItem Text="No" Selected="True" Value="N"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="PnlReportTime" runat="server" Visible="false">
                            <tr>
                                <td style="width: 35%; padding: 5px 10px;">
                                    Mailing list:
                                </td>
                                <td class="input" style="width: 65%; padding: 5px 10px;" align="left">
                                    <asp:TextBox ID="txtemail" runat="server" ></asp:TextBox><br />
                                    <span style="font-size: 11px; ">(Seperate multiple emails addresses with a comma(,) )</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 35%; padding: 5px 10px;">
                                   Delivery Frequency:
                                </td>
                                <td class="input" align="left" style="padding: 5px 10px;">
                                    <asp:DropDownList ID="DrpReportTime" runat="server" Width="180px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 35%; padding: 5px 10px;">
                                    End Date:
                                </td>
                                <td align="left" style="padding: 0 !important; padding: 5px 10px; padding-left: 10px !important;
                                    vertical-align: middle;">
                                   
                                    <asp:TextBox ID="txtrepdate" CssClass="democls" runat="server" onblur="CheckEndDate(this)"
                                        onfocus="CheckEndDate(this)" Enabled="false" Text="__/__/____"></asp:TextBox>
                                    <asp:Image ID="imgCalendar1" runat="server" ImageUrl="~/Images/Calendar.png" Style="cursor: pointer;"
                                        ToolTip="Calendar" />&nbsp;<asp:RequiredFieldValidator Display="Dynamic" InitialValue="__/__/____"
                                            runat="server" ID="rfvdate" Enabled="false" Font-Bold="true" Font-Size="14px"
                                            ControlToValidate="txtrepdate"></asp:RequiredFieldValidator>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender11" OnClientDateSelectionChanged="CheckEndSubscriDate"  CssClass="myCalendar"  runat="server"
                                        Format="MM/dd/yyyy" PopupButtonID="imgCalendar1" TargetControlID="txtrepdate"
                                        Enabled="false">
                                    </ajaxToolkit:CalendarExtender>
                                </td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                                    <ProgressTemplate>
                                        <div style="text-align: center">
                                            <img src="../Images/loader.gif" alt="loader" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="input">
                                <asp:Button ID="BtnUpdate" runat="server" Text="Update" CssClass="btn btn-small btn-blue bule_up_c"
                                    OnClientClick="return checkdate();" OnClick="BtnUpdate_click" />
                                &nbsp;&nbsp;
                                <asp:Button ID="btnCancel" runat="server" OnClientClick="return confirm('Are you sure you want to cancel the update report?');"  CausesValidation="false" Text="Cancel"
                                    CssClass="btn btn-small btn-blue bule_up_c" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button ID="Button2" runat="Server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender ID="MPMessage" runat="server" PopupControlID="pnlmessage"
                    TargetControlID="Button2" BackgroundCssClass="fade" Drag="false">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel Style="display: none; height: auto !important; " CssClass="modalPopup"
                    ID="pnlmessage" runat="server">
                    <div class="changeclienttitleReports" style="width:100%;background: #515151 !important;">
                        <span id="Span1">Message</span>
                        <div style="margin-top: -23px; padding-right: 5px; text-align: right;">
                            <asp:ImageButton ID="btnClose" runat="server" ImageUrl="~/Images/cancel.png" ToolTip="Close"
                                AlternateText="Close" OnClick="btnClose_click" />
                        </div>
                    </div>
                    <table class="myForm">
                        <tr>
                            <td class="input" style="width: 100%">
                                <asp:HiddenField ID="hdnReportId" runat="server" />
                                <asp:Label ID="lblmessage" runat="server"></asp:Label>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Button ID="btnSaveMessage" runat="server" CausesValidation="false" Text="Save"
                                    CssClass="btns btn-small btn-blue" OnClick="btnSaveMessage_Click" />
                                <asp:Button ID="btnDeleteMessage" runat="server" CausesValidation="false" Text="Delete"
                                    CssClass="btns btn-small btn-blue" OnClientClick="return confirm('Are you sure want to delete Message?');"
                                    OnClick="btnDeleteMessage_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <telerik:RadWindowManager ID="rdShare" runat="server">
                </telerik:RadWindowManager>
                <asp:Button ID="btnShare" runat="Server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender ID="mpeShare" runat="server" TargetControlID="btnShare"
                    PopupControlID="pnlShare" CancelControlID="btnCancelShare" BackgroundCssClass="fade"
                    Drag="false">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel Style="display: none; " CssClass="modalPopup xyz"  ID="pnlShare"
                    DefaultButton="btnsaveShare" runat="server">
                    <div class="changeclienttitleReports" style="width:100%;background: #515151 !important;">
                        <span id="Span2">Share Report</span>
                    </div>
                    <table class="myForm">
                        <tr>
                            <%--<td style="width: 25%">--%>
                            <td style="width: 25%; padding-top: 25px !important; padding-left: 30px">
                                Report Name:
                            </td>
                            <%--<td class="input" style="width: 75%">--%>
                            <td class="input" style="width: 75%; padding-top: 25px !important; padding-left: 30px">
                                <asp:TextBox ID="txtSharedReportName" runat="server" MaxLength="205"
                                    onKeyUp="Count(this,200,'Report Name')" onChange="Count(this,200,'Report Name')"></asp:TextBox><br />
                            </td>
                        </tr>
                        <tr>
                            <%--<td>--%>
                            <td style="width: 25%; padding-left: 30px">
                                Share With:
                            </td>
                            <%--<td class="input" style="width: 75%">--%>
                            <td class="input" style="width: 70%; padding-left: 30px">
                                <telerik:RadComboBox ID="RdShareUsers" CssClass="rdshareManged" DataValueField="USERNAME" DataTextField="NAME"
                                    Filter="StartsWith" DropDownWidth="300px"  MaxHeight="200px" runat="server"
                                    EmptyMessage="Select User" style="border-width: 0px; border-collapse: collapse; " CheckBoxes="true" OnClientDropDownClosed="SetTextOfTheComboBox">
                                </telerik:RadComboBox>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <%-- <td style="width: 25%">--%>
                            <td style="width: 25%; padding-left: 30px">
                                Message:
                            </td>
                            <%-- <td class="input" style="width: 75%">--%>
                            <td class="input" style="width: 75%; padding-left: 30px">
                                <asp:TextBox ID="txtMessage" runat="server" Width="300px" TextMode="MultiLine" Rows="3"
                                    onKeyUp="Count(this,500,'Message')" onChange="Count(this,500,'Message')"></asp:TextBox><br />
                                <span style="font-size: 11px; ">(Message must not exceed 500 characters)</span>
                            </td>
                        </tr>
                        <asp:Panel ID="PnlHierarchy" runat="server" Visible="false">
                            <tr id="trSharemsg" runat="server" style="width: 100%;">
                                <td colspan="2" style="">
                                    <span style="color: Red">The users will not be able to view the report without sharing
                                        the Custom Hierarchy.</span>
                                </td>
                            </tr>
                            <tr id="trSHare" runat="server" style="width: 100%;">
                                <td colspan="2">
                                    <asp:RadioButtonList ID="rdShareHierarchy" runat="server">
                                        <asp:ListItem Text="*Share Custom Hierarchy with the users who cannot access it."
                                            Value="1"></asp:ListItem>
                                        <asp:ListItem Text="*Share Report ONLY with the users who have access to the Custom Hierarchy."
                                            Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="PnlCustomStore" runat="server" Visible="false">
                            <tr id="trStore" runat="server" style="width: 100%;">
                                <td colspan="2" style="">
                                    <span style="color: Red">The users will not be able to view the report without sharing
                                        the Custom Store.</span>
                                </td>
                            </tr>
                            <tr id="trshareStore" runat="server" style="width: 100%;">
                                <td colspan="2">
                                    <asp:RadioButtonList ID="rdShareStore" runat="server">
                                        <asp:ListItem Text="*Share Custom Store with the users who cannot access it." Value="1"></asp:ListItem>
                                        <asp:ListItem Text="*Share Report ONLY with the users who have access to the Custom Store."
                                            Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="PnlComboBoth" runat="server" Visible="false">
                            <tr id="tr1" runat="server" style="width: 100%;">
                                <td colspan="2" style="">
                                    <span style="color: Red">The users will not be able to view the report without sharing
                                        the Custom Hierarchy and Custom Store.</span>
                                </td>
                            </tr>
                            <tr id="tr2" runat="server" style="width: 100%;">
                                <td colspan="2">
                                    <asp:RadioButtonList ID="rdComboStoreHierarchy" runat="server">
                                        <asp:ListItem Text="*Share Custom Hierarchy and Custom Store with the users who cannot access it." Value="1"></asp:ListItem>
                                        <asp:ListItem Text="*Share Report ONLY with the users who have access to the Custom Hierarchy and Custom Store."
                                            Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:UpdateProgress ID="UpdateProgress11" runat="server">
                                    <ProgressTemplate>
                                        <div style="text-align: center">
                                            <img src="../Images/loader.gif" alt="loader" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                        <tr>
                        
                            <%--<td class="input">--%>
                            <td class="input" colspan="2" style="text-align:center">
                                 <asp:Button ID="btnsaveShare" runat="server" Text="Share" CssClass="btn
    btn-small btn-blue" OnClick="btnsaveShare_click" OnClientClick="return CheckValues();" />
                               
                                <asp:Button ID="btnCancelShare" runat="server" CausesValidation="false" Text="Cancel"
                                    CssClass="btn btn-small btn-blue" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:HiddenField ID="HfShare" runat="server" />
                <asp:HiddenField ID="Hfdate" runat="server" />
                <asp:HiddenField ID="HfReportID" runat="server" />
                <asp:HiddenField ID="HfShareHierarchy" runat="server" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnZip" />
<asp:PostBackTrigger ControlID="btnExport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnClose"></asp:PostBackTrigger>
            </Triggers>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExport" />
                <asp:PostBackTrigger ControlID="btnClose" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
