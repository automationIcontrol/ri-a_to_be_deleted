﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportSearch.aspx.cs" Inherits="iControlGenricFilters.ReportSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <div id="divMain" runat="server">
        <div id="contentHeader">
            <h1 id="topHeader" runat="server">
                Search Results
            </h1>
        </div>
        <div id="divReportSearchList" class="container">
            <div class="beige-gradient">
                <div class="layout" style="outline: none; border: 1px solid #828282;">
                    <div class="result">
                        <asp:Repeater ID="rptSearchList" runat="server" OnItemDataBound="rptSearchList_ItemDataBound">
                            <ItemTemplate>
                                <a href='<%# "/"+Eval("PageURL") %>' id='link' class="ReportSearchLinks" runat="server">
                                    <%# Eval("ParentMenu") + " => " + Eval("MenuName")%></a>
                                <br />
                            </ItemTemplate>
                        </asp:Repeater>
                        <div id="divEmpty" runat="server" visible="false">
                            <div class="search-noresults-container" style="text-align: center; padding: 50px 0;">
                                <asp:Image ID="imgSearchIcon" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/themeimages/search_icon_grid.png" /><br />
                                <br />
                                <div class="inner-content">
                                    <h5 style="margin: 0 0 1.75em;">
                                        <strong>No Results Found</strong></h5>
                                    <p class="small-text">
                                        Please verify the search criteria and try again</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
