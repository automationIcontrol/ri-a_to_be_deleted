﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;


namespace iControlA.Controls
{
    public partial class Stdate : System.Web.UI.UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            //this.Text = DateTime.Now.ToString("dd/MM/yyyy");
            rfvdate.ValidationGroup = ValidationGroup;
            rfvdate.ErrorMessage = DateErrorMsg;
            base.OnInit(e);
        }
        public string Text
        {
            get { return txtCalendar1.Text; }
            set { txtCalendar1.Text = value; }
        }

        public DateTime StartDate
        {
            set { CalendarExtender11.StartDate = value; }
        }

        public DateTime SubDays(int n)
        {
            DateTime dt = DateTime.Parse(txtCalendar1.Text);
            return dt.AddDays(-n);
        }
        public bool EnabledTextBox
        {
            get { return txtCalendar1.Enabled; }
            set { txtCalendar1.Enabled = value; }
        }
        public bool EnabledCalendar
        {
            get { return CalendarExtender11.Enabled; }
            set { CalendarExtender11.Enabled = value; }
        }
        public string ValidationGroup
        {
            get
            {
                return rfvdate.ValidationGroup;
            }
            set
            {
                rfvdate.ValidationGroup = value;
            }
        }
        public string DateErrorMsg
        {
            get
            {
                return rfvdate.ErrorMessage;
            }
            set
            {
                rfvdate.ErrorMessage = value;
            }
        }

        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(false)]
        public bool SetFocusonError
        {
            get
            {
                return this.rfvdate.SetFocusOnError;

            }
            set
            {
                this.rfvdate.SetFocusOnError = value;
            }
        }

        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(false)]
        public bool EnableDisable
        {
            get
            {
                return this.rfvdate.Enabled;

            }
            set
            {
                this.rfvdate.Enabled = value;
            }
        }
    }
}