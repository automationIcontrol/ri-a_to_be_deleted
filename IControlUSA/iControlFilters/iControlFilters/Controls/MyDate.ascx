﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyDate.ascx.cs" Inherits="iControlGenricFilters.Controls.MyDate" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<style type="text/css">
    .watermarked
    {
        color: #C0C0C0;
        font-style: italic;
    }
     .ajax__calendar_container 
    {
        width : 175px !important;
    }
    .ajax__calendar_body
    {
        font-family : calibri; 
    }
    .ajax__calendar_today 
    {
        padding-top : 0px !important;
    }
    .ajax__calendar_footer {
        height : 22px !important;
        font-family: calibri;
    }
    .ajax__calendar_container TABLE {
        width: 150px !important;
    }
    .myCalendar .ajax__calendar_active
    {
        border-color: #0066cc;
        background-color: #3E92E0;
        color: #fff;
    }
    
</style>

<telerik:RadScriptBlock runat="server" ID="RadScriptBlockMydate">
<%--<script src="../Scripts/jquery-1.4.2.min.js" type="text/javascript"></script>--%>
    <script src="../Scripts/jquery1.8.3.min.js" type="text/javascript"></script>
<script src="../Scripts/jquery.maskedinput-1.3.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(function ($) {
        var selected = $("#<%=txtCalendar.ClientID %>").val();
        if (selected == "" || selected == "__/__/____") {
            $("#<%=txtCalendar.ClientID %>").mask("99/99/9999").val("__/__/____");
        }
        else {
            $("#<%=txtCalendar.ClientID %>").mask("99/99/9999").val(selected);
        }
        $("#<%=txtCalendar.ClientID %>").blur(function () {
            var selectdate = $("#<%=txtCalendar.ClientID %>").val();
            if (selectdate == "" || selectdate == "__/__/____") {
                $("#<%=txtCalendar.ClientID %>").val("__/__/____");
            }
            else {
                $("#<%=txtCalendar.ClientID %>").val(selectdate);
            }
        });
    });

    $(document).ready(function () {
      
        $("#<%=txtCalendar.ClientID %>").on('focusin', function () {
            $("#<%=txtCalendar.ClientID %>").on("focusout", validateDateOnFocusout);
        }).on('keypress', function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                var selected = $("#<%=txtCalendar.ClientID %>").val();
                var _valMyDate = isValidDate(selected.substr(selected.length - 4), getMonthFromStringMyDate(selected.substring(0, 2)), selected.substring(3, 5));
                if (_valMyDate == '-1' || _valMyDate == '-2' || _valMyDate == '-3' || _valMyDate == '-4') {
                    $(this).off('focusout');
                    validateDateOnFocusout();
                    return false;
                }
            }
        });

        function validateDateOnFocusout() {
            try {
                var _hdnMydate = document.getElementById('<%= hdnMyDate.ClientID %>');
                var selected = $("#<%=txtCalendar.ClientID %>").val();
                if (selected == "" || selected == "__/__/____") {
                    $("#<%=txtCalendar.ClientID %>").mask("99/99/9999").val("__/__/____");
                }
                else if (selected.indexOf('_') > -1) {
                    _hdnMydate.value = 1;
                    $find("MyDate_BehaviorID").hide();
                    alert('Please enter a valid date');
                    $("#<%=txtCalendar.ClientID %>").val("__/__/____");
                }
                else if (selected == "00/00/0000") {
                    _hdnMydate.value = 1;
                    $find("MyDate_BehaviorID").hide();
                    alert('Please enter a valid date');
                    $("#<%=txtCalendar.ClientID %>").val("__/__/____");
                }
                else if (isValidDate(selected.substr(selected.length - 4), getMonthFromStringMyDate(selected.substring(0, 2)), selected.substring(3, 5)) == '-1') {
                    _hdnMydate.value = 1;
                    $find("MyDate_BehaviorID").hide();
                    alert('Please enter valid days');
                    $("#<%=txtCalendar.ClientID %>").val("__/__/____");
                }
                else if (isValidDate(selected.substr(selected.length - 4), getMonthFromStringMyDate(selected.substring(0, 2)), selected.substring(3, 5)) == '-2') {
                    _hdnMydate.value = 1;
                    $find("MyDate_BehaviorID").hide();
                    alert('Please enter a valid year');
                    $("#<%=txtCalendar.ClientID %>").val("__/__/____");
                }
                else if (isValidDate(selected.substr(selected.length - 4), getMonthFromStringMyDate(selected.substring(0, 2)), selected.substring(3, 5)) == '-3') {
                    _hdnMydate.value = 1;
                    $find("MyDate_BehaviorID").hide();
                    alert('Please enter a valid month');
                    $("#<%=txtCalendar.ClientID %>").val("__/__/____");
                }
                else if (isValidDate(selected.substr(selected.length - 4), getMonthFromStringMyDate(selected.substring(0, 2)), selected.substring(3, 5)) == '-4') {
                    _hdnMydate.value = 1;
                    $find("MyDate_BehaviorID").hide();
                    alert('Please enter valid days');
                    $("#<%=txtCalendar.ClientID %>").val("__/__/____");
                }
                else {
                    _hdnMydate.value = 0;
                }
            }
            catch (err) { _hdnMydate.value = 0; }
            finally {
                _hdnMydate.value = 0;
                try {
                    if (window.getSelection) {
                        if (window.getSelection().empty) {  // Chrome
                            window.getSelection().empty();
                        } else if (window.getSelection().removeAllRanges) {  // Firefox
                            window.getSelection().removeAllRanges();
                        }
                    } else if (document.selection) {  // IE?
                        document.selection.empty();
                    }
                } catch (err) { }
            }
        }
    });

    //To get month number from month string 
    function getMonthFromStringMyDate(monthName) {
        var month = new Date(Date.parse(monthName + " 1, 2012")).getMonth() + 1;
        if (isNaN(month)) {
            month = monthName;
        }        
        return ('0' + month).slice(-2);
    };

    function validateDate(year, month, days) {
        var totalDays = new Date(year, month, 1, -1).getDate();
        if (days > totalDays)
            return month + '/' + totalDays + '/' + year
        else
            return month + '/' + days + '/' + year
    };

    function isValidDate(year, month, days) {
        //alert(month + '/' + days + '/' + year);
        var totalDays = new Date(year, month, 1, -1).getDate();
        
        if (month <= 12 && month > 0) { }
        else return '-3';

        if (days > totalDays)
            return '-1';
        else {
            if (days <= 0)
                return '-4';
            else {
                if (year > 1000 && year <= 9999) { }
                else return '-2';
                return month + '/' + days + '/' + year;   //new Date(year, month, days); // 
            }
        }
    };    
    
    function OnClientShownEventMyDate(sender, args) {
        var __hdnMydate = document.getElementById('<%= hdnMyDate.ClientID %>');
        if (__hdnMydate.value == '1') {
            __hdnMydate.value = 0;
            $("#<%=txtCalendar.ClientID %>").val("__/__/____");
        }

        var selected = $("#<%=txtCalendar.ClientID %>").val();
        if (selected == "" || selected == "__/__/____") {
            $("#<%=txtCalendar.ClientID %>").mask("99/99/9999").val("__/__/____");
        }
        else if (selected.indexOf('_') > -1) {
            $find("MyDate_BehaviorID").hide();
            alert('Please enter a valid date');
            $("#<%=txtCalendar.ClientID %>").val("__/__/____");
        }
        else if (selected == "00/00/0000") {
            $find("MyDate_BehaviorID").hide();
            alert('Please enter a valid date');
            $("#<%=txtCalendar.ClientID %>").val("__/__/____");
        }
        else if (isValidDate(selected.substr(selected.length - 4), getMonthFromStringMyDate(selected.substring(0, 2)), selected.substring(3, 5)) == '-1') {
            $find("MyDate_BehaviorID").hide();
            alert('Please enter valid days');
            $("#<%=txtCalendar.ClientID %>").val("__/__/____");
        }
        else if (isValidDate(selected.substr(selected.length - 4), getMonthFromStringMyDate(selected.substring(0, 2)), selected.substring(3, 5)) == '-2') {
            $find("MyDate_BehaviorID").hide();
            alert('Please enter a valid year');
            $("#<%=txtCalendar.ClientID %>").val("__/__/____");            
        }
        else if (isValidDate(selected.substr(selected.length - 4), getMonthFromStringMyDate(selected.substring(0, 2)), selected.substring(3, 5)) == '-3') {
            $find("MyDate_BehaviorID").hide();
            alert('Please enter a valid month');
            $("#<%=txtCalendar.ClientID %>").val("__/__/____");
        }
        else if (isValidDate(selected.substr(selected.length - 4), getMonthFromStringMyDate(selected.substring(0, 2)), selected.substring(3, 5)) == '-4') {
            $find("MyDate_BehaviorID").hide();
            alert('Please enter valid days');
            $("#<%=txtCalendar.ClientID %>").val("__/__/____");
        }
        else {

            try {
                var dateMy_ = new Date($("#<%=txtCalendar.ClientID %>").val());
                sender.set_selectedDate(dateMy_);                
            } catch (err) { }

            $("#MyDate_BehaviorID_nextArrow").click(function () {
                try {
                    selected = $("#<%=txtCalendar.ClientID %>").val();
                    var selectedYear = selected.substr(selected.length - 4);
                    var selectedDays = selected.substring(3, 5);
                    var nextValue = $("#MyDate_BehaviorID_title").text();
                    var nextYear = nextValue.substr(nextValue.length - 4);

                    if (selected == "" || selected == "__/__/____") {
                        $("#<%=txtCalendar.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else {
                        if (nextValue.indexOf(',') > -1) {
                            var month = nextValue.split(',')[0];
                            var DateChanged = validateDate(nextYear, getMonthFromStringMyDate(month), selectedDays);
                            $("#<%=txtCalendar.ClientID %>").val(DateChanged);
                        }
                        else if (nextValue.indexOf('-') > -1) {
                            $("#<%=txtCalendar.ClientID %>").val(selected);
                        }
                        else {
                            var DateChanged = validateDate(nextYear, getMonthFromStringMyDate(selected.substring(0, 2)), selectedDays)
                            $("#<%=txtCalendar.ClientID %>").val(DateChanged);
                        }
                    }
                }
                catch (err) { }
                finally {
                    if (selected == "" || selected == "__/__/____" || selected == "NaN/NaN/NaN") {
                        $("#<%=txtCalendar.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else
                        sender.set_selectedDate(new Date($("#<%=txtCalendar.ClientID %>").val()));
                }
            });

            $("#MyDate_BehaviorID_prevArrow").click(function () {
                try {
                    selected = $("#<%=txtCalendar.ClientID %>").val();
                    var selectedYear = selected.substr(selected.length - 4);
                    var selectedDays = selected.substring(3, 5);
                    var prevValue = $("#MyDate_BehaviorID_title").text();
                    var prevYear = prevValue.substr(prevValue.length - 4);

                    if (selected == "" || selected == "__/__/____") {
                        $("#<%=txtCalendar.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else {
                        if (prevValue.indexOf(',') > -1) {
                            var month = prevValue.split(',')[0];
                            var DateChanged = validateDate(prevYear, getMonthFromStringMyDate(month), selectedDays);
                            $("#<%=txtCalendar.ClientID %>").val(DateChanged);
                        }
                        else if (prevValue.indexOf('-') > -1) {
                            $("#<%=txtCalendar.ClientID %>").val(selected);
                        }
                        else {
                            var DateChanged = validateDate(prevYear, getMonthFromStringMyDate(selected.substring(0, 2)), selectedDays);
                            $("#<%=txtCalendar.ClientID %>").val(DateChanged);
                        }
                    }
                } 
                catch (err) { }
                finally {
                    if (selected == "" || selected == "__/__/____" || selected == "NaN/NaN/NaN") {
                        $("#<%=txtCalendar.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else
                        sender.set_selectedDate(new Date($("#<%=txtCalendar.ClientID %>").val()));
                }
            });

            $("#MyDate_BehaviorID_yearsBody .ajax__calendar_year").click(function () {
                try {
                    selected = $("#<%=txtCalendar.ClientID %>").val();
                    var selectedYear = selected.substr(selected.length - 4);
                    var selectedDays = selected.substring(3, 5);
                    var newYear = $(this).text();

                    if (selected == "" || selected == "__/__/____") {
                        $("#<%=txtCalendar.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else {
                        var DateChanged = validateDate(newYear, getMonthFromStringMyDate(selected.substring(0, 2)), selectedDays);
                        $("#<%=txtCalendar.ClientID %>").val(DateChanged);
                    }
                }
                catch (err) { }
                finally {
                    if (selected == "" || selected == "__/__/____" || selected == "NaN/NaN/NaN") {
                        $("#<%=txtCalendar.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else
                        sender.set_selectedDate(new Date($("#<%=txtCalendar.ClientID %>").val()));
                }
            });

            $("#MyDate_BehaviorID_monthsBody .ajax__calendar_month").click(function () {
                try {
                    selected = $("#<%=txtCalendar.ClientID %>").val();
                    var selectedYear = selected.substr(selected.length - 4);
                    var selectedDays = selected.substring(3, 5);
                    var currentMonth = getMonthFromStringMyDate($(this).text());
                    var calValue = $("#MyDate_BehaviorID_title").text();
                    var calYear = calValue.substr(calValue.length - 4);
                    if (selected == "" || selected == "__/__/____") {
                        $("#<%=txtCalendar.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else {
                        var DateChanged = validateDate(calYear, currentMonth, selectedDays);
                        $("#<%=txtCalendar.ClientID %>").val(DateChanged);
                    }
                }
                catch (err) { }
                finally {
                    if (selected == "" || selected == "__/__/____" || selected == "NaN/NaN/NaN") {
                        $("#<%=txtCalendar.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else
                        sender.set_selectedDate(new Date($("#<%=txtCalendar.ClientID %>").val()));
                }
            });  
        }
    }
  
</script>
</telerik:RadScriptBlock>
<%--<telerik:RadMaskedTextBox ID="txtCalendar" runat="server" Mask="##/##/####" HideOnBlur="false">
</telerik:RadMaskedTextBox>--%>
<asp:TextBox ID="txtCalendar" class="txtMyDate date-picker" runat="server" onblur="MaskReset(this);" onfocus="MaskReset1(this);" CssClass="CntrlWithApplyBtn"></asp:TextBox>&nbsp;

<asp:Image ID="imgCalendar" runat="server" ImageUrl="~/Images/Calendar.png" Style="cursor: pointer !important;"
    ToolTip="Calendar" />&nbsp;<asp:RequiredFieldValidator Display="Dynamic" InitialValue="__/__/____" runat="server"
    ID="rfvdate" Enabled="false" Font-Bold="true" Font-Size="14px" ControlToValidate="txtCalendar"></asp:RequiredFieldValidator>
<ajaxToolKit:CalendarExtender ID="CalendarExtender1" CssClass="myCalendar" runat="server" OnClientShown ="OnClientShownEventMyDate"
    Format="dd/MM/yyyy" PopupButtonID="imgCalendar" TargetControlID="txtCalendar" BehaviorID = "MyDate_BehaviorID">
</ajaxToolKit:CalendarExtender>
<%--<ajaxToolKit:TextBoxWatermarkExtender ID="WatermarkExtender1" runat="server" TargetControlID="txtCalendar"
    WatermarkCssClass="watermarked" WatermarkText="Enter the Date of Birth (mm/dd/yyyy)" />
<ajaxToolKit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtCalendar"
    ClearMaskOnLostFocus="false" Mask="99/99/9999" MaskType="Date" AutoComplete="true">
</ajaxToolKit:MaskedEditExtender>--%>
<asp:HiddenField ID="hdnMyDate" Value="0" runat="server" />

