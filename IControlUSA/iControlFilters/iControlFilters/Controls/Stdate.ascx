﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Stdate.ascx.cs" Inherits="iControlA.Controls.Stdate" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<style type="text/css">
    .watermarked
    {
        color: #C0C0C0;
        font-style: italic;
    }
     .ajax__calendar_container 
    {
        width : 175px !important;
    }
    .ajax__calendar_body
    {
        font-family : calibri; 
    }
    .ajax__calendar_today 
    {
        padding-top : 0px !important;
    }
    .ajax__calendar_footer {
        height : 22px !important;
        font-family: calibri;
    }
    .ajax__calendar_container TABLE {
        width: 150px !important;
    }
    .myCalendar .ajax__calendar_active
    {
        border-color: #0066cc;
        background-color: #3E92E0;
        color: #fff;
    }
</style>
<%--<script src="../Scripts/jquery-1.4.2.min.js" type="text/javascript"></script>--%>
<telerik:RadScriptBlock runat="server" ID="RadScriptBlockStDate">
<script src="../Scripts/jquery.maskedinput-1.3.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(function ($) {
        var selected = $("#<%=txtCalendar1.ClientID %>").val();
        if (selected == "" || selected == "__/__/____") {
            $("#<%=txtCalendar1.ClientID %>").mask("99/99/9999").val("__/__/____");
        }

        else {
            $("#<%=txtCalendar1.ClientID %>").mask("99/99/9999").val(selected);
        }
        $("#<%=txtCalendar1.ClientID %>").blur(function () {
            var selectdate = $("#<%=txtCalendar1.ClientID %>").val();
            if (selectdate == "" || selectdate == "__/__/____") {
                $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
            }
            else {
                $("#<%=txtCalendar1.ClientID %>").val(selectdate);
            }
        });
    });

    $(document).ready(function () {
        
        $("#<%=txtCalendar1.ClientID %>").on('focusin', function () {
            $("#<%=txtCalendar1.ClientID %>").on("focusout", validateDateOnFocusout);
        }).on('keypress', function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                var selected = $("#<%=txtCalendar1.ClientID %>").val();
                var _valStDate = isValidDate(selected.substr(selected.length - 4), getMonthFromString(selected.substring(0, 2)), selected.substring(3, 5));
                if (_valStDate == '-1' || _valStDate == '-2' || _valStDate == '-3' || _valStDate == '-4') {
                    $(this).off('focusout');
                    validateDateOnFocusout();
                    return false;
                }
            }
        });

        function validateDateOnFocusout() {
            try {
                var _hdnStDate = document.getElementById('<%= hdnStDate.ClientID %>');
                var selected = $("#<%=txtCalendar1.ClientID %>").val();
                if (selected == "" || selected == "__/__/____") {
                    $("#<%=txtCalendar1.ClientID %>").mask("99/99/9999").val("__/__/____");
                }
                else if (selected.indexOf('_') > -1) {
                    _hdnStDate.value = 1;
                    $find("StDate_BehaviorID").hide();
                    alert('Please enter a valid date');
                    $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
                }
                else if (selected == "00/00/0000") {
                    _hdnStDate.value = 1;
                    $find("StDate_BehaviorID").hide();
                    alert('Please enter a valid date');
                    $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
                }
                else if (isValidDate(selected.substr(selected.length - 4), getMonthFromString(selected.substring(0, 2)), selected.substring(3, 5)) == '-1') {
                    _hdnStDate.value = 1;
                    $find("StDate_BehaviorID").hide();
                    alert('Please enter valid days');
                    $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
                }
                else if (isValidDate(selected.substr(selected.length - 4), getMonthFromString(selected.substring(0, 2)), selected.substring(3, 5)) == '-2') {
                    _hdnStDate.value = 1;
                    $find("StDate_BehaviorID").hide();
                    alert('Please enter a valid year');
                    $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
                }
                else if (isValidDate(selected.substr(selected.length - 4), getMonthFromString(selected.substring(0, 2)), selected.substring(3, 5)) == '-3') {
                    _hdnStDate.value = 1;
                    $find("StDate_BehaviorID").hide();
                    alert('Please enter a valid month');
                    $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
                }
                else if (isValidDate(selected.substr(selected.length - 4), getMonthFromString(selected.substring(0, 2)), selected.substring(3, 5)) == '-4') {
                    _hdnStDate.value = 1;
                    $find("StDate_BehaviorID").hide();
                    alert('Please enter valid days');
                    $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
                }
                else { _hdnStDate.value = 0; }
            }
            catch (err) { _hdnStDate.value = 0; }
            finally {
                _hdnStDate.value = 0;
                try {
                    if (window.getSelection) {
                        if (window.getSelection().empty) {  // Chrome
                            window.getSelection().empty();
                        } else if (window.getSelection().removeAllRanges) {  // Firefox
                            window.getSelection().removeAllRanges();
                        }
                    } else if (document.selection) {  // IE?
                        document.selection.empty();
                    }
                } catch (err) { }
            }
        }
    });

    function getMonthFromString(monthName) {
        var month = new Date(Date.parse(monthName + " 1, 2012")).getMonth() + 1;
        if (isNaN(month)) {
            month = monthName;
        }
        return ('0' + month).slice(-2);
    };

    function validateDateStDate(year, month, days) {
        var totalDays = new Date(year, month, 1, -1).getDate();
        if (days > totalDays)
            return month + '/' + totalDays + '/' + year
        else
            return month + '/' + days + '/' + year
    };

    function isValidDate(year, month, days) {
        //alert(month + '/' + days + '/' + year);
        var totalDays = new Date(year, month, 1, -1).getDate();

        if (month <= 12 && month > 0) { }
        else return '-3';

        if (days > totalDays)
            return '-1';
        else {
            if (days <= 0)
                return '-4';
            else {
                if (year > 1000 && year <= 9999) { }
                else return '-2';
                return month + '/' + days + '/' + year;   //new Date(year, month, days); // 
            }
        }
    };

    function OnClientShownEventStDate(sender, args) {
        var __hdnStDate = document.getElementById('<%= hdnStDate.ClientID %>');
        if (__hdnStDate.value == '1') {
            __hdnStDate.value = 0;
            $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
        }

        var selected = $("#<%=txtCalendar1.ClientID %>").val();
        if (selected == "" || selected == "__/__/____") {
            $("#<%=txtCalendar1.ClientID %>").mask("99/99/9999").val("__/__/____");
        }
        else if (selected.indexOf('_') > -1) {
            $find("StDate_BehaviorID").hide();
            alert('Please enter a valid date');
            $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
        }
        else if (selected == "00/00/0000") {
            $find("StDate_BehaviorID").hide();
            alert('Please enter a valid date');
            $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
        }
        else if (isValidDate(selected.substr(selected.length - 4), getMonthFromString(selected.substring(0, 2)), selected.substring(3, 5)) == '-1') {
            $find("StDate_BehaviorID").hide();
            alert('Please enter valid days');
            $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
        }
        else if (isValidDate(selected.substr(selected.length - 4), getMonthFromString(selected.substring(0, 2)), selected.substring(3, 5)) == '-2') {
            $find("StDate_BehaviorID").hide();
            alert('Please enter a valid year');
            $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
        }
        else if (isValidDate(selected.substr(selected.length - 4), getMonthFromString(selected.substring(0, 2)), selected.substring(3, 5)) == '-3') {
            $find("StDate_BehaviorID").hide();
            alert('Please enter a valid month');
            $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
        }
        else if (isValidDate(selected.substr(selected.length - 4), getMonthFromString(selected.substring(0, 2)), selected.substring(3, 5)) == '-4') {
            $find("StDate_BehaviorID").hide();
            alert('Please enter valid days');
            $("#<%=txtCalendar1.ClientID %>").val("__/__/____");
        }
        else {

            try {
                var dateSt_ = new Date($("#<%=txtCalendar1.ClientID %>").val());
                sender.set_selectedDate(dateSt_);
            } catch (err) { }

            $("#StDate_BehaviorID_nextArrow").click(function () {
                try {
                    selected = $("#<%=txtCalendar1.ClientID %>").val();
                    var selectedYear = selected.substr(selected.length - 4);
                    var selectedDays = selected.substring(3, 5); //
                    var nextValue = $("#StDate_BehaviorID_title").text();
                    var nextYear = nextValue.substr(nextValue.length - 4);

                    if (selected == "" || selected == "__/__/____") {
                        $("#<%=txtCalendar1.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else {
                        if (nextValue.indexOf(',') > -1) {
                            var month = nextValue.split(',')[0];
                            var DateChanged = validateDateStDate(nextYear, getMonthFromString(month), selectedDays);
                            $("#<%=txtCalendar1.ClientID %>").val(DateChanged);
                        }
                        else if (nextValue.indexOf('-') > -1) {
                            $("#<%=txtCalendar1.ClientID %>").val(selected);
                        }
                        else {
                            var DateChanged = validateDateStDate(nextYear, getMonthFromString(selected.substring(0, 2)), selectedDays)
                            $("#<%=txtCalendar1.ClientID %>").val(DateChanged);
                        }
                    }
                }
                catch (err) { }
                finally {
                    if (selected == "" || selected == "__/__/____" || selected == "NaN/NaN/NaN") {
                        $("#<%=txtCalendar1.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else
                        sender.set_selectedDate(new Date($("#<%=txtCalendar1.ClientID %>").val()));
                }
            });

            $("#StDate_BehaviorID_prevArrow").click(function () {
                try {
                    selected = $("#<%=txtCalendar1.ClientID %>").val();
                    var selectedYear = selected.substr(selected.length - 4);
                    var selectedDays = selected.substring(3, 5);
                    var prevValue = $("#StDate_BehaviorID_title").text();
                    var prevYear = prevValue.substr(prevValue.length - 4);

                    if (selected == "" || selected == "__/__/____") {
                        $("#<%=txtCalendar1.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else {
                        if (prevValue.indexOf(',') > -1) {
                            var month = prevValue.split(',')[0];
                            var DateChanged = validateDateStDate(prevYear, getMonthFromString(month), selectedDays);
                            $("#<%=txtCalendar1.ClientID %>").val(DateChanged);
                        }
                        else if (prevValue.indexOf('-') > -1) {
                            $("#<%=txtCalendar1.ClientID %>").val(selected);
                        }
                        else {
                            var DateChanged = validateDateStDate(prevYear, getMonthFromString(selected.substring(0, 2)), selectedDays);
                            $("#<%=txtCalendar1.ClientID %>").val(DateChanged);
                        }
                    }
                }
                catch (err) { }
                finally {
                    if (selected == "" || selected == "__/__/____" || selected == "NaN/NaN/NaN") {
                        $("#<%=txtCalendar1.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else
                        sender.set_selectedDate(new Date($("#<%=txtCalendar1.ClientID %>").val()));
                }
            });

            $("#StDate_BehaviorID_yearsBody .ajax__calendar_year").click(function () {
                try {
                    selected = $("#<%=txtCalendar1.ClientID %>").val();
                    var selectedYear = selected.substr(selected.length - 4);
                    var selectedDays = selected.substring(3, 5);
                    var newYear = $(this).text();

                    if (selected == "" || selected == "__/__/____") {
                        $("#<%=txtCalendar1.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else {
                        var DateChanged = validateDateStDate(newYear, getMonthFromString(selected.substring(0, 2)), selectedDays);
                        $("#<%=txtCalendar1.ClientID %>").val(DateChanged);
                    }
                }
                catch (err) { }
                finally {
                    if (selected == "" || selected == "__/__/____" || selected == "NaN/NaN/NaN") {
                        $("#<%=txtCalendar1.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else
                        sender.set_selectedDate(new Date($("#<%=txtCalendar1.ClientID %>").val()));
                }
            });

            $("#StDate_BehaviorID_monthsBody .ajax__calendar_month").click(function () {
                try {
                    selected = $("#<%=txtCalendar1.ClientID %>").val();
                    var selectedYear = selected.substr(selected.length - 4);
                    var selectedDays = selected.substring(3, 5);
                    var currentMonth = getMonthFromString($(this).text());
                    var calValue = $("#StDate_BehaviorID_title").text();
                    var calYear = calValue.substr(calValue.length - 4);
                    var changedMonth = getMonthFromString(calValue.split(',')[0]); //split text before first comma

                    if (selected == "" || selected == "__/__/____") {
                        $("#<%=txtCalendar1.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else {
                        var DateChanged = validateDate(calYear, currentMonth, selectedDays);
                        $("#<%=txtCalendar1.ClientID %>").val(DateChanged);
                    }
                }
                catch (err) { }
                finally {
                    if (selected == "" || selected == "__/__/____" || selected == "NaN/NaN/NaN") {
                        $("#<%=txtCalendar1.ClientID %>").mask("99/99/9999").val("__/__/____");
                    }
                    else
                        sender.set_selectedDate(new Date($("#<%=txtCalendar1.ClientID %>").val()));
                }
            });
        }
    }
      
</script>
</telerik:RadScriptBlock>
<%--<telerik:RadMaskedTextBox ID="txtCalendar" runat="server" Mask="##/##/####" HideOnBlur="false">
</telerik:RadMaskedTextBox>--%>
<asp:TextBox ID="txtCalendar1" class="txtStDate" runat="server" Width="55%" onblur="MaskReset(this);" onfocus="MaskReset1(this);" Text="__/__/____"></asp:TextBox>&nbsp;

<asp:Image ID="imgCalendar1" runat="server" ImageUrl="~/Images/Calendar.png" Style="cursor: hand !important;"
    ToolTip="Calendar" />&nbsp;
<asp:RequiredFieldValidator Display="Dynamic" InitialValue="__/__/____" runat="server"
    ID="rfvdate" Enabled="false" Font-Bold="true" Font-Size="14px" ControlToValidate="txtCalendar1"></asp:RequiredFieldValidator>
<ajaxToolKit:CalendarExtender ID="CalendarExtender11" CssClass="myCalendar" runat="server"  OnClientShown ="OnClientShownEventStDate"
BehaviorID = "StDate_BehaviorID" Format="MM/dd/yyyy" PopupButtonID="imgCalendar1" TargetControlID="txtCalendar1">
</ajaxToolKit:CalendarExtender>
<%--<ajaxToolKit:TextBoxWatermarkExtender ID="WatermarkExtender1" runat="server" TargetControlID="txtCalendar"
    WatermarkCssClass="watermarked" WatermarkText="Enter the Date of Birth (mm/dd/yyyy)" />
<ajaxToolKit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtCalendar"
    ClearMaskOnLostFocus="false" Mask="99/99/9999" MaskType="Date" AutoComplete="true">
</ajaxToolKit:MaskedEditExtender>--%>
<asp:HiddenField ID="hdnStDate" Value="0" runat="server" />
