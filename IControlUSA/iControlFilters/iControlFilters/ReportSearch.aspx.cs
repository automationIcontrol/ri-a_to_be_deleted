﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Threading;
using iControlDataLayer;

namespace iControlGenricFilters
{
    public partial class ReportSearch : System.Web.UI.Page
    {
        string strReturnURL = "/ProjectForms/NewsUpdates.aspx?";

        protected void Page_Load(object sender, EventArgs e)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                if (!Page.IsPostBack)
                {
                    DataTable dtSearchResult = GetSearchResult();
                    rptSearchList.DataSource = dtSearchResult;
                    rptSearchList.DataBind();
                }
            }

            catch (Exception ex)
            {
                Response.Redirect(strReturnURL);
                helper.WriteLog(string.Format("{0}" + ex.Message, "RepoertSearch.aspx.cs > Page_Load"));
            }

        }
        /// <summary>
        /// get search list 
        /// </summary>
        /// <returns>DataTable.</returns>
        private DataTable GetSearchResult()
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                string personid = HttpContext.Current.Session["PersonId"].ToString();
                string appGroup = ConfigurationManager.AppSettings["ApplicationGroup"] != null ? ConfigurationManager.AppSettings["ApplicationGroup"].ToString() : "General";
                string searchValue = HttpContext.Current.Request.QueryString["SearchText"].ToString();
                DataTable dtResult = DataManager.GetAppReportSearch(personid, appGroup, searchValue);
                if (dtResult.Rows.Count == 0)
                    divEmpty.Visible = true;

                return dtResult;
            }
            catch (Exception ex)
            {
                Response.Redirect(strReturnURL);
                helper.WriteLog(string.Format("{0}" + ex.Message, "RepoertSearch.aspx.cs > GetSearchResult"));
                return null;
            }
        }
        /// <summary>
        /// highlight searched text at time of grid bind 
        /// </summary>
        public void rptSearchList_ItemDataBound(object source, RepeaterItemEventArgs e)
        {
            IControlHelper helper = new IControlHelper();
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                try
                {
                    HtmlAnchor Searchedlink = (HtmlAnchor)e.Item.FindControl("link");
                    string searchText = Request.QueryString["SearchText"].ToString();
                    int index = Searchedlink.InnerHtml.ToLower().IndexOf(searchText.ToLower());
                    string TextToReplace = Searchedlink.InnerHtml.Substring(index, searchText.Length);
                    Searchedlink.InnerHtml = Searchedlink.InnerHtml.Replace(TextToReplace, "<mark>" + TextToReplace + "</mark>");
                }
                catch (Exception ex)
                {
                    Response.Redirect(strReturnURL);
                    helper.WriteLog(string.Format("{0}" + ex.Message, "RepoertSearch.aspx.cs > rptSearchList_ItemDataBound"));
                }
            }
        }

    }
}
