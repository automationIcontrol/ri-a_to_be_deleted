(function(){
	var options = $('.option_panel'),
	button = options.find('button[class^="fixed_"]'),
	menu = $('.menu_container'),
	color = $('.colors_options>li>a');
	$('.option_panel>button:first-child').on('click',function(){
		if(options.hasClass('close')){
			options.animate({ 'left': '0px'});
			options.removeClass('close').addClass('open');
			$(this).text('-');
		}
		else if(options.hasClass('open')){
			options.animate({ 'left': '-220px'});
			options.removeClass('open').addClass('close');
			$(this).text('+');
		}
	});
	
	button.on('click',function(){
		$(this).addClass('button_clicked');
		$(this).siblings().removeClass('button_clicked');
		if($(this).data('dir') === 'top'){
			menu.addClass('fixed_top');
			menu.removeClass('fixed_bottom').removeClass('fixed_left').removeClass('fixed_right');
		}
		else if($(this).data('dir') === 'bottom'){
			menu.addClass('fixed_bottom');
			menu.removeClass('fixed_top').removeClass('fixed_left').removeClass('fixed_right');
		}
		else if($(this).data('dir') === 'right'){
			menu.addClass('fixed_right');
			menu.removeClass('fixed_top').removeClass('fixed_left').removeClass('fixed_bottom');
		}
		else if($(this).data('dir') === 'left'){
			menu.addClass('fixed_left');
			menu.removeClass('fixed_top').removeClass('fixed_bottom').removeClass('fixed_right');
		}
		else if($(this).data('dir') === 'default'){
			menu.removeClass('fixed_top').removeClass('fixed_bottom').removeClass('fixed_right').removeClass('fixed_left');
		}
	
	});
	
	color.on('click',function(){
		var colorValue = $(this).attr('title');
		
		$('body').removeClass().addClass(colorValue);
		$(this).addClass('active_color');
		$(this).parent('li').siblings().children('a').removeClass('active_color');
		
		if(colorValue === "red"){
			menu.addClass('red').removeClass('green').removeClass('blue').removeClass('yellow').removeClass('brown').removeClass('violet');
		}
		else if(colorValue === "green"){
			menu.addClass('green').removeClass('red').removeClass('blue').removeClass('yellow').removeClass('brown').removeClass('violet');
		}
		else if(colorValue === "blue"){
			menu.addClass('blue').removeClass('red').removeClass('green').removeClass('yellow').removeClass('brown').removeClass('violet');
		}
		else if(colorValue === "yellow"){
			menu.addClass('yellow').removeClass('red').removeClass('green').removeClass('blue').removeClass('brown').removeClass('violet');
		}
		else if(colorValue === "brown"){
			menu.addClass('brown').removeClass('red').removeClass('green').removeClass('blue').removeClass('yellow').removeClass('violet');
		}
		else if(colorValue === "violet"){
			menu.addClass('violet').removeClass('red').removeClass('green').removeClass('blue').removeClass('yellow').removeClass('brown');
		}
		
	});
	
}());