﻿var TelrikControlPrefix = "ctl00_MainContent_";
var AspxControlPrefix = "MainContent_";
var CustomHierarchy = "Custom Hierarchy";
var CorporateHierarchy = "Corporate Hierarchy";
var CustomStoreGroup = "Custom Store Group";
var AdditionalFilterText = "";
$(document).ready(function () {
    if (('#DailyWeekly')) {
        ShowDateRangeValues();

    }
});

//On change event for Dropdown Hierarchy
function Init() {
    $('#' + TelrikControlPrefix + 'Hierarchy').change(function () {
        LoadDynamicControles();
    });
    $('#' + TelrikControlPrefix + 'HierarchyLocation').change(function () {
        LoadDynamicControles();
    });

    //expand-coapse feature
    $("#additionalFilter").click(function () {
        var ProductHierarchyselectedValue = $('#' + TelrikControlPrefix + 'Hierarchy').val();
        if (ProductHierarchyselectedValue) {
            if (ProductHierarchyselectedValue == CustomHierarchy) {
                $(".CorporateItemAnchorCss").hide();
                $(".CustomItemAnchorCss").toggle();
            }
            else if (ProductHierarchyselectedValue == CorporateHierarchy) {
                $(".CorporateItemAnchorCss").toggle();
                $(".CustomItemAnchorCss").hide();
            }
        }
        else {
            $(".CorporateItemAnchorCss").toggle();
            $(".CustomItemAnchorCss").toggle();
            $(".AnchorCss").toggle();
        }
        var $this = $(this);
        if ($this.text() == 'Additional Filters') {
            $this.text('Less Filters');
            AdditionalFilterText = 'Less Filters';
        } else {
            $this.text('Additional Filters');
            AdditionalFilterText = 'Additional Filters';

        }
    });

    if (('#DailyWeekly')) {
        $('#DailyWeekly').on('click', function () {
            ShowDateRangeValues();
        });
    };
};

//Get Query String Parameters
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
// To Close the div after showing message.
function MessageClose() {
    $('#' + AspxControlPrefix + 'divMessage').css("display", "None");
}

//Fill Date Sunday to Saturday in from and To Date
function ShowDateRangeValues() {
    if ($('#' + AspxControlPrefix + 'From').val() == '' && $('#' + AspxControlPrefix + 'To').val() == '') {
        var val = "";
        var curr = new Date; // get current date
        var LastSat = addDays(curr, -(curr.getDay() + 1));
        var LastSunday = addDays(LastSat, -6);
        $('#' + AspxControlPrefix + 'From').val(getFormattedDate(LastSunday));
        $('#' + AspxControlPrefix + 'To').val(getFormattedDate(LastSat));
    }
}

function addDays(theDate, days) {
    return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
}
//Check Date validation
function CheckDateValidations() {
    var RepType = getParameterByName('RepType');
    if (RepType != null)
    {
      $('#' + AspxControlPrefix + 'ReportContainer').html('');
    }
    var DateFrom = $('#' + AspxControlPrefix + 'From').val();
    var DateTo = $('#' + AspxControlPrefix + 'To').val();
    if (DateTo != null) {
        if (Date.parse(DateFrom) > Date.parse(DateTo)) {
            alert("From Date should be less than To date.");
            return false;
        } else if ($('#DailyWeekly').find('input:checked').val() == "2") {
            var fromDate = new Date(DateFrom); //Converting string to date
            var toDate = new Date(DateTo);
            if (!(fromDate.getDay() == 0) || !(toDate.getDay() == 6)) {
                alert("From Date should be Sunday and To Date should be Saturday,unless you are selecting Daily.");
                return false;
            }
        }
    }
    var DiscontinuedDate = $('#' + AspxControlPrefix + 'DiscontinuedDate').val();
    if (DiscontinuedDate) {
        if (Date.parse(DiscontinuedDate) > new Date()) {
            alert("[Discontinued Date] should be less than Today’s date.");
            return false;
        }
    }
    var result = CheckFiscalWeekAndAddWeekValidations();
    if (result == true) {
        return true;
    }
    return false;
}

//Check fiscal week validation and fiscal week validation
function CheckFiscalWeekAndAddWeekValidations() {
    var FromAdWeekValue = $('#' + TelrikControlPrefix + 'FromFiscalWeek').val();
    var ToAdWeekValue = $('#' + TelrikControlPrefix + 'ToFiscalWeek').val();
    if (FromAdWeekValue && ToAdWeekValue) {
        var YearFromAdWeek = FromAdWeekValue.split('-')[0];
        var WeekFromAdWeek = FromAdWeekValue.split('-')[1];

        var YearToAdWeek = ToAdWeekValue.split('-')[0];
        var WeekToAdWeek = ToAdWeekValue.split('-')[1];

        if (YearToAdWeek >= YearFromAdWeek) {
            if (YearToAdWeek == YearFromAdWeek) {
                if (WeekToAdWeek < WeekFromAdWeek) {
                    alert('[To Fiscal Week] should be greater than [From Fiscal Week]');
                    return false;
                }
            }
        }
        else {
            alert('[To Fiscal Week] should be greater than [From Fiscal Week]');
            return false;
        }
    } else {
        var FromAdWeekValue = $('#' + TelrikControlPrefix + 'FromAdWeek').val();
        var ToAdWeekValue = $('#' + TelrikControlPrefix + 'ToAdWeek').val();

        if (FromAdWeekValue && ToAdWeekValue) {
            var YearFromAdWeek = FromAdWeekValue.split('-')[0];
            var WeekFromAdWeek = FromAdWeekValue.split('-')[1];

            var YearToAdWeek = ToAdWeekValue.split('-')[0];
            var WeekToAdWeek = ToAdWeekValue.split('-')[1];

            if (YearToAdWeek >= YearFromAdWeek) {
                if (YearToAdWeek == YearFromAdWeek) {
                    if (WeekToAdWeek < WeekFromAdWeek) {
                        alert('[To Ad Week] should be greater than [From Ad Week]');
                        return false;
                    }
                }
            }
            else {
                alert('[To Ad Week] should be greater than [From Ad Week]');
                return false;
            }
        }
    }
    return true;
}

//Vendor No change Event
function VendorNoChangeEvent() {
    if ($('#' + AspxControlPrefix + 'VendorNumber')) {
        //      $('#' + AspxControlPrefix + 'VendorNumber').ForceNumericOnly();
        $('#' + AspxControlPrefix + 'VendorNumber').change(function () {
            if ($('#' + AspxControlPrefix + 'VendorNumber').val().length >= 6 || $('#' + AspxControlPrefix + 'VendorNumber').val().length == 0) {
                document.getElementById("BtnFilterForVendorNumber").click();
            }
        });
    }
}

//On page load function
function LoadDynamicControles() {
    if ($('#' + TelrikControlPrefix + 'Hierarchy')) {
        var ProductHierarchyselectedValue = $('#' + TelrikControlPrefix + 'Hierarchy').val();
        if (ProductHierarchyselectedValue) {
            if (ProductHierarchyselectedValue == CustomHierarchy) {
                if (AdditionalFilterText == 'Additional Filters') {
                    $(".CorporateItem").hide();
                    $(".CorporateItemAnchorCss").hide();
                    $(".CustomItem").show();
                    $(".CustomItemAnchorCss").hide();
                    $("#additionalFilter").text('Additional Filters');
                    AdditionalFilterText = 'Additional Filters';

                }
                else {
                    $(".CorporateItem").hide();
                    $(".CorporateItemAnchorCss").hide();
                    $(".CustomItem").show();
                    $(".CustomItemAnchorCss").show();
                    $("#additionalFilter").text('Less Filters');
                    AdditionalFilterText = 'Less Filters';
                }
            }

            else if (ProductHierarchyselectedValue == CorporateHierarchy) {
                if (AdditionalFilterText != '') {
                    if (AdditionalFilterText == 'Additional Filters') {
                        $(".CustomItem").hide();
                        $(".CustomItemAnchorCss").hide();
                        $(".CorporateItem").show();
                        $(".CorporateItemAnchorCss").hide();
                        $("#additionalFilter").text('Additional Filters');
                        AdditionalFilterText = 'Additional Filters';
                    }
                    else {
                        $(".CustomItem").hide();
                        $(".CustomItemAnchorCss").hide();
                        $(".CorporateItem").show();
                        $(".CorporateItemAnchorCss").show();
                        $("#additionalFilter").text('Less Filters');
                        AdditionalFilterText = 'Less Filters';
                    }
                }
                else {
                    AdditionalFilterText = 'Additional Filters';
                    if (AdditionalFilterText == 'Additional Filters') {

                        $(".CustomItem").hide();
                        $(".CustomItemAnchorCss").hide();
                        $(".CorporateItem").show();
                        $(".CorporateItemAnchorCss").hide();
                        $("#additionalFilter").text('Additional Filters');
                        AdditionalFilterText = 'Additional Filters';

                    }
                    else {
                        $(".CustomItem").hide();
                        $(".CustomItemAnchorCss").hide();
                        $(".CorporateItem").show();
                        $(".CorporateItemAnchorCss").show();
                        $("#additionalFilter").text('Less Filters');
                        AdditionalFilterText = 'Less Filters';
                    }
                }
            }
        }
        else {
            if (AdditionalFilterText != '') {
                if ($('#' + TelrikControlPrefix + 'HierarchyName').val() != undefined) {
                    if (AdditionalFilterText == 'Additional Filters') {
                        $(".CustomItem").show();
                        $(".CustomItemAnchorCss").hide();
                        $("#additionalFilter").text('Additional Filters');
                        AdditionalFilterText = 'Additional Filters';
                    }
                    else {
                        $(".CustomItem").show();
                        $(".CustomItemAnchorCss").show();
                        $("#additionalFilter").text('Less Filters');
                        AdditionalFilterText = 'Less Filters';
                    }
                }
                else {
                    if (AdditionalFilterText == 'Additional Filters') {
                        $(".CorporateItem").show();
                        $(".CorporateItemAnchorCss").hide();
                        $(".AnchorCss").hide();
                        $("#additionalFilter").text('Additional Filters');
                        AdditionalFilterText = 'Additional Filters';
                    }
                    else {
                        $(".CorporateItem").show();
                        $(".CorporateItemAnchorCss").show();
                        $(".AnchorCss").show();
                        $("#additionalFilter").text('Less Filters');
                        AdditionalFilterText = 'Less Filters';
                    }
                }
            }
            else {
                AdditionalFilterText = 'Additional Filters';
                if (AdditionalFilterText == 'Additional Filters') {
                    if ($('#' + TelrikControlPrefix + 'HierarchyName').val() != undefined) {
                        $(".CustomItem").show();
                        $(".CustomItemAnchorCss").hide();
                        $("#additionalFilter").text('Additional Filters');
                        AdditionalFilterText = 'Additional Filters';
                    }
                    else {
                        $(".CorporateItem").show();
                        $(".CorporateItemAnchorCss").hide();
                        $(".AnchorCss").hide();
                        $("#additionalFilter").text('Additional Filters');
                    }
                }

            }

        }
        if ($('#' + TelrikControlPrefix + 'HierarchyLocation')) {
            var LocationHierarchyselectedValue = $('#' + TelrikControlPrefix + 'HierarchyLocation').val();
            if (LocationHierarchyselectedValue) {
                if (LocationHierarchyselectedValue == CustomStoreGroup) {
                    $(".LocationCorporateItem").hide();
                    $(".LocationCustomItem").show();
                }
                else if (LocationHierarchyselectedValue == CorporateHierarchy) {
                    $(".LocationCustomItem").hide();
                    $(".LocationCorporateItem").show();
                }
            }
        }
    }
}
//On page load event
function pageLoad() {
    LoadDynamicControles();
    VendorNoChangeEvent();
    Init();
};


//Function for numric value only 
$.fn.ForceNumericOnly =
function () {
    return this.each(function () {
        $(this).keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 ||
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};

// function for check subscription end date by event
function CheckEndSubscriDate(sender, args) {
    var txtVal = sender._selectedDate.format(sender._format);
    if (isDate(txtVal)) {
        var endSubscribtionMaxDate = new Date().AddDays(7 * 52);
        var chktxtVal = new Date(txtVal), chkendSubscribtionMaxDate = new Date(endSubscribtionMaxDate);
        if (chktxtVal > chkendSubscribtionMaxDate) {
            $('#MainContent_txtrepdate').val(endSubscribtionMaxDate);
            sender._selectedDate = endSubscribtionMaxDate;
        }
        else {
            sender._selectedDate = txtVal;
            $('#MainContent_txtrepdate').val(txtVal)
        }
    }
}

//function for check end subscription date
function CheckEndSubscrDate(id) {
    $(id).blur(function () {
        var txtVal = $(this).val();
        if ($(this).val() == '__/__/____' || $(this).val().trim().length == 0) {
            $(this).val('__/__/____');
        }
        else {
            if (isDate(txtVal)) {
                var endSubscribtionMaxDate = new Date().AddDays(7 * 52);
                var chktxtVal = new Date(txtVal), chkendSubscribtionMaxDate = new Date(endSubscribtionMaxDate);
                if (chktxtVal > chkendSubscribtionMaxDate) {
                    $(this).val(endSubscribtionMaxDate);
                }
                else
                    $(this).val(txtVal);
            }
            else {
                alert('Invalid Date');
                $(this).val('');
                $(this).focus();
            }
        }
    });

}

//Function for date check valid or not
function CheckEndDate(id) {
    $(id).blur(function () {
        var txtVal = $(this).val();
        if ($(this).val() == '__/__/____' || $(this).val().trim().length == 0) {
            $(this).val('');
        }
        else {
            if (isDate(txtVal)) {
                $(this).val(txtVal);
            }
            else {
                alert('Invalid Date');
                $(this).val('');
                $(this).focus();
            }
        }
    });

}

// For set date format of mediaList
Date.prototype.AddDays = function (noOfDays) {
    this.setTime(this.getTime() + (noOfDays * (1000 * 60 * 60 * 24)));
    return this;
}

Date.prototype.toString = function () {
    var year = this.getFullYear();

    var month = (1 + this.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = this.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return month + '/' + day + '/' + year;
}

//Function for date check
function isDate(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
        return false;

    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}

//format for MM/dd/yyyy
function getFormattedDate(date) {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return month + '/' + day + '/' + year;
}

//function for export filters
function Exportfilters() {
    var FromDate = document.getElementById("ctl00_MainContent_dtFromDate_txtCalendar");
    if (FromDate.value == "" || FromDate.value == "__/__/____") {
        alert('Please select From Date');
        return false;
    }

    var ToDate = document.getElementById("ctl00_MainContent_dtToDate_txtCalendar");
    if (ToDate.value == "" || ToDate.value == "__/__/____") {
        alert('Please select To Date');
        return false;
    }

    if (CompareDates(FromDate, ToDate) == 0) {
        alert('The From Date should be less than the To date.');
        return false;
    }

    var StartDt = "02/23/2014";
    if (StartDateCheck(StartDt, FromDate) == 0) {
        alert("The From Date  must be FY2015 (2/23/14) or later.");
        return false;
    }
    jQuery.noConflict();
    var comboBanners = $find("<%= ddlBanners.ClientID %>");
    if (comboBanners.get_checkedItems().length == 0) {
        alert('Please select Banner');
        return false;
    }
    var comboddlCustomHierarchy = $find("<%= ddlCustomHierarchy.ClientID %>");
    if (comboddlCustomHierarchy.get_selectedIndex() == 0) {
        alert('Please select Custom Hierarchy');
        return false;
    }
    if (comboddlCustomHierarchy.get_selectedIndex() == null) {
        alert('Please select Custom Hierarchy');
        return false;
    }
    eval($('.lnkexpo').attr('href'));
}


//for Share Functionality
function Sharereports() {
    $('#' + AspxControlPrefix + 'txtSharedReportName').value = "";
    $('#' + AspxControlPrefix + 'txtMessage').value = "";
    var comboUsers = $find("ctl00_MainContent_RdShareUsers");
    comboUsers.trackChanges();
    for (var i = 0; i < comboUsers.get_items().get_count() ; i++) {
        comboUsers.get_items().getItem(i).set_checked(false);
    }
    comboUsers.commitChanges();
    comboUsers.clearSelection();
    jQuery.noConflict();
    $('#' + AspxControlPrefix + 'PnlshareButtons').css("display", "inline-block");
    $('#' + AspxControlPrefix + 'PnlshareButtons').css("text-align", "center");

}

//function for lenth count of text
function Count(text, long, Controltype) {
    var maxlength = new Number(long); // Change number to your max length.
    if (text.value.length > maxlength) {
        text.value = text.value.substring(0, maxlength);
        alert(Controltype + " must not exceed " + maxlength + " characters.");
    }
}

//function for clear shared control
function ClearSharedControls() {
    $('#' + AspxControlPrefix + 'txtMessage').value = "";
    $('input:radio[id=' + AspxControlPrefix + 'rdShareHierarchy_0]').attr('checked', false);
    $('input:radio[id=' + AspxControlPrefix + 'rdShareHierarchy_1]').attr('checked', false);
}

//function for check values
function CheckValues() {
    var RepName = $('#' + AspxControlPrefix + 'txtSharedReportName').val();
    if (RepName.trim() == "") {
        alert('Please enter report name');
        return false;
    }


    var Msg = $('#' + AspxControlPrefix + 'txtMessage').val();
    if (Msg.trim() == "") {
        alert('Please enter message');
        return false;
    }
    if ($('#' + AspxControlPrefix + 'PnlHierarchy').is(':visible') || $('#' + AspxControlPrefix + 'PnlCustomStore').is(':visible') || $('#' + AspxControlPrefix + 'PnlComboBoth').is(':visible')) {
        var Rdvalue = 0;
        if ($('#' + AspxControlPrefix + 'rdShareHierarchy_1').checked) {
            Rdvalue = 1;
        }
        if ($('#' + AspxControlPrefix + 'rdShareHierarchy_0').checked) {
            Rdvalue = 1;
        }
        if ($('#' + AspxControlPrefix + 'rdShareStore_1').checked) {
            Rdvalue = 1;
        }
        if ($('#' + AspxControlPrefix + 'rdShareStore_0').checked) {
            Rdvalue = 1;
        }
        if ($('#' + AspxControlPrefix + 'rdComboStoreHierarchy_1').checked) {
            Rdvalue = 1;
        }
        if ($('#' + AspxControlPrefix + 'rdComboStoreHierarchy_0').checked) {
            Rdvalue = 1;
        }
        if (Rdvalue == 0) {
            alert("Please select an option to share custom hierarchy.")
            return false;
        }
    }
}














