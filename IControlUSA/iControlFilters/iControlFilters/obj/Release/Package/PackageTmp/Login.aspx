﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="iControlGenricFilters.Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/admin.css" rel="stylesheet" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="Content/login.css" rel="stylesheet" />
    <link href="Content/ModelDialog.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="~/Images/favicon.ico" type="image/x-icon" />
    <link href='https://fonts.googleapis.com/css?family=Lato:300,700' rel='stylesheet'
        type='text/css' />
    <style>
       
    </style>
</head>
<body class="clsBody">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div id="wrapper">
            <div class="loginbg1" id="loginbg">
                <div class="login_contr">
                    <div class="logi-rt1 mT10">
                        <table style="width: 100%; height: 100%;">
                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <asp:Image ID="imgLogo" runat="server" AlternateText="Logo" />
                                </td>
                            </tr>
                            <tr style="height: 75px; text-align: center">
                                <td align="center " class="customerCustomtext">Customer Access
                                </td>
                            </tr>
                            <tr style="height: 50px">
                                <td align="center">
                                    <table style="border-collapse: collapse; border-spacing: 0;">
                                        <tr>
                                            <td style="vertical-align: top; padding: 0;">
                                                <img src="themeimages/Username.png" style="width: 40px; height: 40px; margin-right: -2px;" />
                                            </td>
                                            <td style="vertical-align: top; padding: 0;">
                                                <asp:TextBox ID="txtusname" runat="server" CssClass="login_textfield1" required></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table style="border-collapse: collapse; border-spacing: 0;">
                                        <tr>
                                            <td style="vertical-align: top; padding: 0;">
                                                <img src="themeimages/Password.png" style="width: 40px; height: 40px; margin-right: -2px;" />
                                            </td>
                                            <td style="vertical-align: top; padding: 0;">
                                                <asp:TextBox ID="txtpass" runat="server" TextMode="Password" CssClass="login_textfield1" required></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family: 'Lato' !important;">
                                    <asp:CheckBox ID="chkAccept" runat="server" CssClass="checker" />
                                    <asp:Label ID="lblAccept" Text="I accept the " class="login_lable1" runat="server" />
                                    <asp:HyperLink ID="lnkTerms" runat="server" class="login_lable1" Font-Bold="true"
                                        Text="Terms & Conditions" NavigateUrl="~/images/iControl_terms_conditions.pdf"
                                        ForeColor="Black" Target="_blank"></asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family: 'Lato' !important;">
                                    <asp:Label ID="Lbl_error" runat="server" ForeColor="#923F3F" Font-Size="14px" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a href="#">
                                        <asp:ImageButton ID="btnLogin" runat="server" ImageUrl="~/themeimages/LoginButton.png"
                                            ValidationGroup="a" OnClick="btnLogin_Click" Width="150px" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <asp:Button ID="btnHdnRequest" runat="Server" Style="display: none;" />
            <ajaxToolkit:ModalPopupExtender ID="mpeConfirm" runat="server" TargetControlID="btnHdnRequest"
                PopupControlID="pnlRqst" CancelControlID="btnCancelRequest" BackgroundCssClass="fade"
                Drag="false">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel Style="display: none; width: 413px !important; height: 115px; padding: 10px; border-color: #3b86bf;"
                CssClass="modalPopup" ID="pnlRqst" runat="server">
                <div>
                    <table style="height: 110px">
                        <tr>
                            <td style="vertical-align: top">
                                <asp:Image ID="imgIcon" runat="server" ImageUrl="~/Images/I_mark.png" />
                            </td>
                            <td style="vertical-align: top">
                                <%=HtmlContent_PopupContent %>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                            <td>
                                <%=HtmlContent_AreYouSure %>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center">
                                <asp:Button ID="btnContinue" runat="server" Text="Continue" OnClick="btnContinue_Click"
                                    CssClass="btn btn-small btn-blue" />
                                <asp:Button ID="btnCancelRequest" runat="server" Text="Cancel" CssClass="btn btn-small btn-blue" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
