﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DyanamicFilters.aspx.cs" Inherits="iControlGenricFilters.DyanamicFilters" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" EnableViewState="true" runat="server">
    <link href="Content/DynamicFilter.css" rel="stylesheet" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"> </script>
    <script type="text/javascript" src="https://public.tableau.com/javascripts/api/tableau-2.min.js"></script>
    <script src="Scripts/Custom/DynamicFilter.js" type="text/javascript"></script>
    <script src="Scripts/Custom/TableauJsAPI.js" type="text/javascript"></script>
   
    <asp:UpdateProgress ID="UpdateProgressFilters" AssociatedUpdatePanelID="UpdatePanelFilters" runat="server">
        <ProgressTemplate>
            <div class="raDiv">
                <img alt="Loading..." src="Images/loader-s.png" style="width:50px;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


    <div class="update-panel-filer">
        <div>
            <div class="Breadcrumbs">
                <asp:Label ID="lblBreadCrumbs" runat="server"></asp:Label>
            </div>
        </div>
        <div>
            <div class="filter-section">
                <div class="white-section">
                    <asp:UpdatePanel ID="UpdatePanelFilters" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="left-form">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="ControlsParent" ViewStateMode="Enabled" ClientIDMode="Static" runat="server">
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="BtnShowReport" runat="server" ClientIDMode="Static" CssClass="btns btn-primary btn-block" Text="Show Report" OnClientClick="return CheckDateValidations();" OnClick="BtnShowReport_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="right-section">
                        <table>
                            <tr>
                                <td>
                                    <div id="DivPnlToolbar" runat="server">
                                        <asp:UpdateProgress ID="UpdateProgressToolbar" AssociatedUpdatePanelID="UpdatePanelToolbar" runat="server">
                                            <ProgressTemplate>
                                                <div class="raToolbar">
                                                    <img alt="Loading..." src="Images/loader.gif" style="width: 50px;margin-top:6px;" />
                                                </div>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                        <asp:UpdatePanel ID="UpdatePanelToolbar" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div id="PnlshareButtons" runat="server"  visible="false" style="text-align: center; width: 100%; border-bottom: solid 1px #5a5a5a;">
                                                    <asp:Panel ID="pnlToolbar" runat="server">
                                                        <div style="width: 100%; text-align: center;">
                                                            <asp:Button ID="BtnsaveRep" runat="server" Text="Save " OnClick="BtnsaveRep_click"
                                                                CssClass="buttonsave" Visible="true" />
                                                            <asp:Button ID="Btnsharerep" runat="server" Text="Share" OnClick="Btnsharerep_Click"
                                                                OnClientClick="return Sharereports();" CssClass="buttonShare" Visible="true" />
                                                            <asp:Button ID="btnExport" runat="server" Text="Export Filters" OnClick="btnExport_click" OnClientClick="return ActiveToolBarSave()"
                                                                CssClass="buttonExport" />
                                                        </div>
                                                    </asp:Panel>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div id="divMessage" runat="server" class="ShareMessage">
                                        <div class="ShareHeader">
                                            Message from &nbsp;<asp:Literal ID="ltrSharedBy" runat="server"></asp:Literal>:
                                        <a href="#" class="show_hide" style="font: 2.3em Arial; text-decoration: none; float: right; color: Gray !important; margin-top: 1px; margin-right: 2px;"
                                            id="Close">
                                            <img id="imgCancel" src="../Images/Cancel-icon.png" onclick="MessageClose();" alt="close" /></a>
                                            <a href="#" class="show_hide" style="font: 2.3em Arial; text-decoration: none; float: right; color: Gray !important; margin-top: 1px; margin-right: 2px;"
                                                id="plus">
                                                </a>
                                        </div>
                                        <div id="divInnerMessage" class="slidingDiv" style="display: block; padding: 8px;">
                                            <asp:Literal ID="ltrMessage" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="ReportContainer" runat="server">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="upRequest" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <telerik:RadWindowManager ID="rdWindow" runat="server">
            </telerik:RadWindowManager>
            <asp:Button ID="btnHiddenRequest" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mpeRequest" runat="server" TargetControlID="btnHiddenRequest"
                PopupControlID="pnlRequest" BackgroundCssClass="fade"
                Drag="false">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel Style="display: none;" CssClass="modalPopup" ID="pnlRequest"
                DefaultButton="btnsave" runat="server">
                <div class="changeclienttitleReports"  style="width:100%;background: #515151 !important;">
                    <span id="Span1">Save/Subscribe</span>
                </div>
                <table width="100%">
                    <tr>
                        <td style="width: 35%; padding: 5px 10px;">Report Name:
                        </td>
                        <td class="input" style="width: 65%; padding: 5px 10px 5px 0;">
                            <asp:TextBox ID="txtReportName" runat="server"  MaxLength="205" onKeyUp="Count(this,200,'Report Name')"
                                onChange="Count(this,200,'Report Name')"></asp:TextBox>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 35%; padding: 5px 10px;">Description:
                        </td>
                        <td class="input" style="padding: 5px 10px 5px 0;">
                            <asp:TextBox ID="txtReportDescr" runat="server" TextMode="MultiLine"
                                onKeyUp="Count(this,1000,'Description')" onChange="Count(this,1000,'Description')"
                                Rows="2"></asp:TextBox>
                            <br />
                            <span style="font-size: 11px; ">(Description must not exceed 1000
                                                                characters)</span> </td>
                    </tr>
                    <asp:Panel ID="pnldates" runat="server">
                        <tr>
                            <td style="width: 35%; padding: 5px 10px;">Relative Date In:
                            </td>
                            <td class="input" style="padding: 5px 10px 5px 0;">Last&nbsp;
                                                               
                               

                                <asp:DropDownList ID="DrpDateVal" Width="50px" runat="server">
                                </asp:DropDownList>
                                Week(s)
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td style="width: 35%; padding: 5px 10px;">Subscribe to?
                        </td>
                        <td class="input" style="padding: 0 !important;" align="left">
                            <asp:RadioButtonList ID="rdSchedule" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                AutoPostBack="true" OnSelectedIndexChanged="rdSchedule_SelectedIndexChanged" Style="margin-left: 0;">
                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                <asp:ListItem Text="No" Selected="True" Value="N"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <asp:Panel ID="PnlReportTime" runat="server" Visible="false">
                        <tr>
                            <td style="width: 35%; padding: 5px 10px;">Mailing list:
                            </td>
                            <td class="input" style="width: 65%; padding: 5px 10px;" align="left">
                                <asp:TextBox ID="txtemail" runat="server" Width="300px"></asp:TextBox>
                                <br />
                                <span style="font-size: 11px; ">(Seperate multiple emails addresses with a comma(,)
                                                                    )</span> </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px 10px; width: 35%;">Delivery Frequency:
                            </td>
                            <td class="input" align="left" style="padding: 5px 10px;">
                                <asp:DropDownList ID="DrpReportTime" runat="server" Width="180px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px 10px; width: 35%;">End Subscription On:
                            </td>
                            <td align="left" style="padding: 0 !important; padding-left: 10px !important; padding: 5px 10px;">
                                <asp:TextBox ID="txtrepdate" CssClass="democls" runat="server" onblur="CheckEndSubsOnDate(this)"
                                    onfocus="CheckEndSubscrDate(this)" Enabled="false" Text="__/__/____"></asp:TextBox>
                                <asp:Image ID="imgCalendar1" runat="server" ImageUrl="~/Images/Calendar.png" Style="cursor: pointer;"
                                    ToolTip="Calendar" />
                                &nbsp;<asp:RequiredFieldValidator Display="Dynamic" InitialValue="__/__/____"
                                    runat="server" ID="rfvdate" Enabled="false" Font-Bold="true" Font-Size="14px"
                                    ControlToValidate="txtrepdate"></asp:RequiredFieldValidator>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender11" OnClientDateSelectionChanged="CheckEndSubscriDate" CssClass="myCalendar Calcss"
                                    runat="server" Format="MM/dd/yyyy" PopupButtonID="imgCalendar1" TargetControlID="txtrepdate"
                                    Enabled="false"></ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                                <ProgressTemplate>
                                    <div style="text-align: center">
                                        <img src="../Images/loader.gif" alt="loader" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" class="input ft-btn">
                            <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btns
                                                                  btn-small btn-blue"
                                OnClick="btnsave_click" OnClientClick="return checkdate();" />
                            <asp:Button ID="btnsend" runat="server" Text="Send Now" CssClass="btns btn-small btn-blue"
                                OnClick="btnsSend_click" Visible="false" OnClientClick="return checkdateforsend();" />
                            <asp:Button ID="btnsendandsave" runat="server" Text="Save & Send Now" CssClass="btns btn-small btn-blue"
                                OnClick="btnsSavSend_click" Visible="false" OnClientClick="return checkdateforsend();" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="false" Text="Cancel"
                                CssClass="btns btn-small btn-blue" OnClientClick="return confirm('Are you sure you want to cancel the save report?');" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upShare" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <telerik:RadWindowManager ID="rdShare" runat="server">
            </telerik:RadWindowManager>
            <asp:Button ID="btnShare" runat="Server" Style="display: none" />
            <asp:Button ID="btnshowsharemsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mpeShare" runat="server" TargetControlID="btnShare"
                PopupControlID="pnlShare" CancelControlID="btnShareCancel" BackgroundCssClass="fade"
                Drag="false">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel Style="display: none; " CssClass="modalPopup" ID="pnlShare"
                DefaultButton="btnsaveShare" runat="server">
                <div class="changeclienttitleReports"  style="width:100%;background: #515151 !important;">
                    <span>Share Report</span>
                </div>
                <table class="myForm">
                    <tr>
                        <td style="width: 25%; padding-top: 25px !important; padding-left: 30px">Report Name:
                        </td>
                        <td class="input" style="width: 75%; padding-top: 25px !important; padding-left: 30px">
                            <asp:TextBox ID="txtSharedReportName" runat="server"  MaxLength="205"
                                onKeyUp="Count(this,200,'Report Name')" onChange="Count(this,200,'Report Name')"></asp:TextBox>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%; padding-left: 30px">Share With:
                        </td>
                        <td class="input" style="width: 75%; padding-left: 30px">
                            <telerik:RadComboBox ID="RdShareUsers" CssClass="rdsharedynamic" DataValueField="USERNAME" DataTextField="NAME"
                                AllowCustomText="true" Filter="StartsWith" DropDownWidth="307px" 
                                MaxHeight="200px" runat="server" EmptyMessage="Select User" CheckBoxes="true">
                            </telerik:RadComboBox>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%; padding-left: 30px">Message:
                        </td>
                        <td class="input" style="width: 75%; padding-left: 30px">
                            <asp:TextBox ID="txtMessage" runat="server"  TextMode="MultiLine" Rows="3"
                                onKeyUp="Count(this,500,'Message')" onChange="Count(this,500,'Message')"></asp:TextBox>
                            <br />
                            <span style="font-size: 11px; ">(Message must not exceed 500 characters)</span> </td>
                    </tr>
                    <asp:Panel ID="PnlHierarchy" runat="server" Visible="false">
                        <tr id="trSharemsg" runat="server" style="width: 100%;">
                            <td colspan="2" style="padding-left: 30px">
                                <span style="color: Red">The users will not be able to view the report without sharing
                                        the Custom Hierarchy.</span>
                            </td>
                        </tr>
                        <tr id="trSHare" runat="server" style="width: 100%;">
                            <td colspan="2">
                                <asp:RadioButtonList ID="rdShareHierarchy" runat="server">
                                    <asp:ListItem Text="*Share Custom Hierarchy with the users who cannot access it."
                                        Value="1"></asp:ListItem>
                                    <asp:ListItem Text="*Share Report ONLY with the users who have access to the Custom Hierarchy."
                                        Value="0"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="PnlCustomStore" runat="server" Visible="false">
                        <tr id="trStore" runat="server" style="width: 100%;">
                            <td colspan="2" style="">
                                <span style="color: Red">The users will not be able to view the report without sharing
                                        the Custom Store.</span>
                            </td>
                        </tr>
                        <tr id="trshareStore" runat="server" style="width: 100%;">
                            <td colspan="2">
                                <asp:RadioButtonList ID="rdShareStore" runat="server">
                                    <asp:ListItem Text="*Share Custom Store with the users who cannot access it." Value="1"></asp:ListItem>
                                    <asp:ListItem Text="*Share Report ONLY with the users who have access to the Custom Store."
                                        Value="0"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="PnlComboBoth" runat="server" Visible="false">
                        <tr id="tr1" runat="server" style="width: 100%;">
                            <td colspan="2" style="">
                                <span style="color: Red">The users will not be able to view the report without sharing
                                        the Custom Hierarchy and Custom Store.</span>
                            </td>
                        </tr>
                        <tr id="tr2" runat="server" style="width: 100%;">
                            <td colspan="2">
                                <asp:RadioButtonList ID="rdComboStoreHierarchy" runat="server">
                                    <asp:ListItem Text="*Share Custom Hierarchy and Custom Store with the users who cannot access it." Value="1"></asp:ListItem>
                                    <asp:ListItem Text="*Share Report ONLY with the users who have access to the Custom Hierarchy and Custom Store."
                                        Value="0"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:UpdateProgress ID="UpdateProgress11" runat="server">
                                <ProgressTemplate>
                                    <div style="text-align: center">
                                        <img src="../Images/loader.gif" alt="loader" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="input" style="text-align: center">
                            <asp:Button ID="btnsaveShare" runat="server" Text="Share" CssClass="btns btn-small btn-blue" OnClick="btnsaveShare_Click" OnClientClick="return CheckValues();" />
                            &nbsp;&nbsp;
                           
                            <asp:Button ID="btnShareCancel" runat="server" CausesValidation="false" Text="Cancel"
                                OnClientClick="ClearSharedControls();" CssClass="btns btn-small btn-blue" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

