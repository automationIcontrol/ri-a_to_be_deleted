﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="iControlGenricFilters.About" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RoomCategoryID">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ControlsParent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ClientIDMode="Static" ID="RadAjaxLoadingPanel1" IsSticky="true" runat="server" Height="75px" Width="75px" Transparency="25">
        <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>' style="border: 0; position: relative; top: 50%; left: 50%;" />
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadComboBox ID="RoomCategoryID"  runat="server" Height="200px" Width="200px" EmptyMessage="Select Category" CheckBoxes="true"
        EnableLoadOnDemand="True" MarkFirstMatch="True" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="RoomCategoryID_SelectedIndexChanged"
        ShowMoreResultsBox="True" EnableVirtualScrolling="True">
        <Items>
            <telerik:RadComboBoxItem Text="item1" />
            <telerik:RadComboBoxItem Text="item1" />
            <telerik:RadComboBoxItem Text="item1" />
            <telerik:RadComboBoxItem Text="item1" />

        </Items>
    </telerik:RadComboBox>

    <table>
        <tr>
            <td>
                <asp:Panel ID="ControlsParent" ViewStateMode="Enabled" ClientIDMode="Static" runat="server">
<div>
    abc
</div>
                </asp:Panel>
            </td>
        </tr>
    </table>
   
</asp:Content>
