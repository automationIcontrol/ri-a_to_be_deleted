﻿using iControlGenricFilters.ResourceFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iControlGenricFilters
{
    public partial class UnderMaintenance : System.Web.UI.Page
    {
        public new string Title
        {

            get
            {

                return IcontrolFilterResources.UnderMaintenance_Title;

            }
        }

        public static string ContentPart1
        {

            get
            {

                return IcontrolFilterResources.UnderMaintenance_Content1;

            }
        }

        public string ContentPart2
        {

            get
            {

                return IcontrolFilterResources.UnderMaintenance_Content2;

            }
        }


        public string ContentPart3
        {

            get
            {

                return IcontrolFilterResources.UnderMaintenance_Content3;

            }
        }

        public string ThankYou
        {

            get
            {

                return IcontrolFilterResources.ThankYou;

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
    }
}