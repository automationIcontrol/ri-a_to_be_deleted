﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(iControlGenricFilters.Startup))]
namespace iControlGenricFilters
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
