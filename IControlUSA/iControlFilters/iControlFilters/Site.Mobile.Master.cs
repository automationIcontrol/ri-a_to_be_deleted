using iControlDataLayer;
using iControlGenricFilters.LIB;
using iControlGenricFilters.ResourceFiles;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iControlGenricFilters
{
    public partial class Site_Mobile : System.Web.UI.MasterPage
    {

        public string LoginUserName
        {
            get
            {
                return Convert.ToString(this.Session["UserName"]);
            }
            set
            {
                this.Session["UserName"] = value;
            }
        }

        public string LoginExpiryMsg = IcontrolFilterResources.LoginExpiryMessage;
        public string PageTitle = IcontrolFilterResources.SiteMaster_Title;

        #region VariableDeclarations
        IControlHelper helper = new IControlHelper();
        private string applicationfolderpath = "";
        protected string _ApplicationFolder;
        protected string _Target = "";
        protected bool _SharedShrinkUser;
        protected string _OpenLinksInNewTab
        {
            get
            {
                return Session["IsNewTab"].ToString() == "Enable" ? "Disable" : "Enable";
            }
            set
            {
                Session["IsNewTab"] = (String.IsNullOrEmpty(value) ? "Disable" : value);

                if (Session["IsNewTab"].ToString() == "Enable")
                {
                    _Target = "target='_blank'";
                }
            }
        }

        protected string strPageName = "SiteMaster.cs";
        protected string defaultPage = "Default.aspx";
        protected string ApplicationFolderPath
        {
            get { return applicationfolderpath; }
        }

        public int iWarningTimeoutInMilliseconds;
        public int iSessionTimeoutInMilliseconds;
        public string sTargetURLForSessionTimeout;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.LoginUserName))
                {
                    string strScript = "alert('" + IcontrolFilterResources.Session_Expired + "');window.location.href='" + GetHomePageURL() + "';";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionOut", strScript, true);
                    return;
                }
                if (Session["IDPLogin"] != null)
                    divChangePwdandLogout.Visible = false;
                else
                    divChangePwdandLogout.Visible = true;

                if (Convert.ToString(ConfigurationManager.AppSettings["ApplicationGroup"]) == "CVS")
                    toTop.Visible = true;
                else
                    toTop.Visible = false;

                string sTopNav = "";
                if (Request.QueryString["logout"] != null)
                {
                    logout();
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "onLoad", "DisplaySessionTimeout();", true);

                //In a real app, stuff these values into web.config.
                sTargetURLForSessionTimeout = GetHomePageURL();//"Error.aspx?Reason=Timeout";
                int iNumberOfMinutesBeforeSessionTimeoutToWarnUser = 1;

                //Get the sessionState timeout (from web.config).
                //If not set there, the default is 20 minutes.
                int iSessionTimeoutInMinutes = Session.Timeout; // Session.Timeout;

                //Compute our timeout values, one for
                //our warning, one for session termination.
                int iWarningTimeoutInMinutes = iSessionTimeoutInMinutes -
                    iNumberOfMinutesBeforeSessionTimeoutToWarnUser;

                iWarningTimeoutInMilliseconds = iWarningTimeoutInMinutes * 60 * 1000;

                iSessionTimeoutInMilliseconds = iSessionTimeoutInMinutes * 60 * 1000;
                if (Page.IsPostBack == true)
                {
                    string eventTarget = (this.Request["__EVENTTARGET"] == null) ? string.Empty : this.Request["__EVENTTARGET"];

                    if (eventTarget == "SetLogout")
                    {
                        logout(true);
                    }
                    if (eventTarget == "ResetSession")
                    {
                        //Session.Timeout = 3;
                        System.Web.Configuration.SessionStateSection sessionStateSection = (System.Web.Configuration.SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
                        Session.Timeout = Convert.ToInt16(sessionStateSection.Timeout.Minutes);
                    }

                    if (Context.Request.Url.ToString().Contains("ManagePOSettings") == false && Context.Request.Url.ToString().Contains("MaintainUserInformation") == false && Context.Request.Url.ToString().Contains("AlcoholMain") == false && Context.Request.Url.ToString().Contains("ShowPaymentDetails") == false)
                    {
                        if (HttpContext.Current.Session["PersonAttributes"] != null)
                        {
                            if (HttpContext.Current.Session["PersonAttributes"].ToString().Contains("-")) //Menu_Top_Admin"))
                            {
                                if (GetBrowser() == "IE")
                                {
                                    dropdown_menu.Visible = false;
                                    dropdown_menu_IE7.Visible = true;
                                    dropdown_menu_IE7.InnerHtml = Session["MyMenuIE"].ToString();
                                }
                                else
                                {
                                    dropdown_menu.Visible = true;
                                    dropdown_menu_IE7.Visible = false;
                                    dropdown_menu.InnerHtml = Session["MyMenu"].ToString();
                                }

                                if (Session["ActiveVerticalId"] != null)
                                {

                                    if (Session["RoleName"].ToString() == "Vendor Portal Users" || Session["RoleName"].ToString() == "Mega Suppliers" || Session["RoleName"].ToString() == "SuperValu Vendors" || Session["RoleName"].ToString() == "SVU Corporate")
                                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Call my function", "ShowHideMenu('" + Session["ActiveVerticalId"].ToString() + "','','1');", true);
                                    else
                                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Call my function", "ShowHideMenu('" + Session["ActiveVerticalId"].ToString() + "','');", true);
                                }
                            }
                        }
                        return;
                    }
                }
                else
                {
                    //add user log information
                    string strMID = "";
                    string strusername = Context.User.Identity.Name;
                    string strUserId = DataManager.GetFieldValue("Logins where login like '%" + strusername + "%'", "ownerentityid");
                    string strpage = Context.Request.Url.LocalPath.ToString();
                    char[] arr = new char[] { '/', ' ' }; //Trim these characters
                    strpage = strpage.TrimStart(arr);

                    if (Request.QueryString["mid"] != null)
                    {
                        strMID = Request.QueryString["mid"].ToString();
                        DataManager.SaveUserActivity(strUserId.ToString(), strMID, strpage, Session.SessionID);
                    }
                    else
                    {
                        strpage = "Home";
                        DataManager.SaveUserActivity(strUserId.ToString(), "0", strpage, Session.SessionID);
                    }
                    BindNotifications();
                }

                string username = "";
                string personattributes = "";
                int accesslevelvaluestart;
                int accesslevelvaluelength;
                string accessvalues = "";
                Boolean userisauthenticated = false;


                if (Session["UpdateMessage"] != null)
                {
                    MessageBox.Show(Session["UpdateMessage"].ToString());
                    Session["UpdateMessage"] = null;
                }

                if (Session["LockStatus"] != null && Session["LockStatus"].ToString() == "1")
                {
                    string strScriptAlert = "alert('" + IcontrolFilterResources.AccountLocked_PleaseSendMailToUnlock_Msg + "');window.location.href='" + GetHomePageURL() + "';";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "LockAlert", strScriptAlert, true);
                }
                else
                {
                    string OpenInNewTab = Request.QueryString["OpenInNewTab"] != null ? Request.QueryString["OpenInNewTab"].ToString() : "";
                    if (String.IsNullOrEmpty(OpenInNewTab))
                    {
                        if (Session["IsNewTab"] == null)
                            _OpenLinksInNewTab = OpenInNewTab;
                        else
                            _OpenLinksInNewTab = Session["IsNewTab"].ToString();
                    }
                    else
                    {
                        _OpenLinksInNewTab = OpenInNewTab;
                    }
                    userisauthenticated = Context.User.Identity.IsAuthenticated;

                    if (userisauthenticated == false)
                    {
                        try
                        {
                            string HomePage = GetHomePageURL();
                            Response.Redirect(HomePage);
                        }
                        catch (Exception ex)
                        {
                            helper.WriteLog(string.Format("{0}" + ex.Message, "site.master.cs > Page_Load"));
                        }
                    }
                    username = Context.User.Identity.Name;

                    if (HttpContext.Current.Session["PersonAttributes"] == null)
                    {
                        HttpContext.Current.Session.Add("AccessLevel", "");
                        HttpContext.Current.Session.Add("AccessValues", "");
                        HttpContext.Current.Session.Add("SupplierAccessId", "");
                        HttpContext.Current.Session.Add("ChainAccessId", "");
                        HttpContext.Current.Session.Add("ManufacturerAccessId", "");
                        HttpContext.Current.Session.Add("BannerAccess", "");
                        HttpContext.Current.Session.Add("EditRights", "VIEW");
                        HttpContext.Current.Session.Add("AlcoholMainPage", "");

                        HttpContext.Current.Session.Add("PersonId", "");
                        personattributes = DataManager.GetUserAttributesWithLoginID(userisauthenticated, username);
                        if (personattributes.Length > 0)
                        {
                            HttpContext.Current.Session.Add("PersonAttributes", personattributes.ToString());

                            if (personattributes.ToString().Contains("UserEntityID"))
                            {
                                accesslevelvaluestart = personattributes.IndexOf("UserEntityID-", 0) + 13;
                                accesslevelvaluelength = personattributes.IndexOf("|", accesslevelvaluestart);
                                accessvalues = personattributes.Substring(accesslevelvaluestart, accesslevelvaluelength - accesslevelvaluestart).ToString();
                                Session["PersonId"] = accessvalues.ToString();
                            }

                            if (personattributes.ToString().Contains("AdminAccess"))
                            {
                                Session["AccessLevel"] = "Admin";
                                accesslevelvaluestart = personattributes.IndexOf("AdminAccess-", 0) + 12;
                                accesslevelvaluelength = personattributes.IndexOf("|", accesslevelvaluestart);
                                accessvalues = personattributes.Substring(accesslevelvaluestart, accesslevelvaluelength - accesslevelvaluestart).ToString();
                                Session["AccessValues"] = accessvalues.ToString();
                                Session["AlcoholMainPage"] = "MaintainUserInformation.aspx";
                            }

                            if (personattributes.ToString().Contains("ChainAccess"))
                            {
                                Session["AccessLevel"] = "Chain";
                                accesslevelvaluestart = personattributes.IndexOf("ChainAccess-", 0) + 12;
                                accesslevelvaluelength = personattributes.IndexOf("|", accesslevelvaluestart);
                                accessvalues = personattributes.Substring(accesslevelvaluestart, accesslevelvaluelength - accesslevelvaluestart).ToString();
                                Session["AccessValues"] = accessvalues.ToString();
                                Session["AlcoholMainPage"] = "MaintainUserInformation.aspx";
                                Session.Add("AttributeId", 17);
                            }

                            if (personattributes.ToString().Contains("BannerAccess"))
                            {
                                accesslevelvaluestart = personattributes.IndexOf("BannerAccess-", 0) + 13;
                                accesslevelvaluelength = personattributes.IndexOf("|", accesslevelvaluestart);
                                accessvalues = personattributes.Substring(accesslevelvaluestart, accesslevelvaluelength - accesslevelvaluestart).ToString();
                                Session["BannerAccess"] = accessvalues.ToString();
                            }

                            if (personattributes.ToString().Contains("EditRights"))
                            {
                                accesslevelvaluestart = personattributes.IndexOf("EditRights-", 0) + 11;
                                accesslevelvaluelength = personattributes.IndexOf("|", accesslevelvaluestart);
                                accessvalues = personattributes.Substring(accesslevelvaluestart, accesslevelvaluelength - accesslevelvaluestart).ToString();
                                Session["EditRights"] = accessvalues.ToString();
                            }
                        }
                    }

                    if (Request.QueryString["vid"] != null)
                    {
                        Session.Add("ActiveVerticalId", Request.QueryString["vid"].ToString());
                    }

                    if (Request.QueryString["mid"] != null)
                    {
                        Session.Add("ActiveMenuId", Request.QueryString["mid"].ToString());
                    }

                    if (Session["WelcomeMsg"] == null)
                    {
                        SetWelcomeMessage();
                        if (Convert.ToString(ConfigurationManager.AppSettings["ApplicationGroup"]) == "EDW")
                        {
                            ShowAdminMessage();
                        }
                    }
                    if (Session["WelcomeMsg"] != null)
                        lblWelcome.Text = Session["WelcomeMsg"].ToString();

                    if (Session["AccessLevel"].ToString() == "Supplier")
                    {
                        _SharedShrinkUser = DataManager.IsSharedShrinkUser(Session["AccessValues"].ToString());
                    }

                    _ApplicationFolder = Context.Request.ApplicationPath + applicationfolderpath;

                    if (HttpContext.Current.Session["PersonAttributes"] != null)
                    {
                        if (HttpContext.Current.Session["PersonAttributes"].ToString().Contains("-")) //Menu_Top_Admin"))
                        {
                            String RoleName = DataManager.GetUserRoleName(username);
                            Session["RoleName"] = RoleName;

                            if (GetBrowser() == "IE")
                            {
                                sTopNav = SetTopNavVendorPortalIE7();
                            }
                            else
                            {
                                sTopNav = SetTopNavVendorPortal();
                            }

                            if (GetBrowser() == "IE")
                            {
                                Session["MyMenuIE"] = sTopNav;
                            }
                            else
                            {
                                Session["MyMenu"] = sTopNav;
                            }
                        }
                    }

                    if (GetBrowser() == "IE")
                    {
                        dropdown_menu.Visible = false;
                        dropdown_menu_IE7.Visible = true;
                        dropdown_menu_IE7.InnerHtml = sTopNav.ToString();
                    }
                    else
                    {
                        dropdown_menu.Visible = true;
                        dropdown_menu_IE7.Visible = false;
                        dropdown_menu.InnerHtml = sTopNav.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > Page_Load"));
            }

            #region Show Maintenance Message


            GetUpcomingMessage();
            GetDuringMessage();
            GetDuringSiteActiveMessage();

            #endregion
        }
        /// <summary>
        /// Bind Notifications
        /// </summary>
        public void BindNotifications()
        {
            try
            {
                DataTable dt = DataManager.GetNotifications(this.LoginUserName);
                if (dt.Rows.Count > 0)
                {
                    if (Convert.ToInt16(dt.Rows[0]["counter"]) > 0)
                    {
                        divnotification.Visible = true;
                        lblcount.Text = Convert.ToString(dt.Rows[0]["counter"]);
                    }
                    else
                    {
                        divnotification.Visible = false;
                        divProfileName.Style["right"] = "108px";
                    }
                }
                else
                {
                    divnotification.Visible = false;
                    divProfileName.Style["right"] = "108px";
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > BindNotifications"));
            }
        }
        /// <summary>
        /// Get Browser desc
        /// </summary>
        protected string GetBrowser()
        {
            string rtnvalue = "";
            try
            {
                string browser = Request.Browser.Browser;
                string version = Request.Browser.Version;
                if (browser.ToLower() == "ie" || browser.ToLower() == "internet explorer")
                {
                    if (Convert.ToDouble(version) < Convert.ToDouble(8))
                    {
                        rtnvalue = "IE";
                    }
                    else
                    {
                        rtnvalue = "Not IE";
                    }
                }
                else
                {
                    rtnvalue = "Not IE";
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > GetBrowser"));
            }
            return rtnvalue;
        }

        #region Method To Get Upcoming Maintenance Message
        /// <summary>
        /// Get upcoming message
        /// </summary>
        private int GetUpcomingMessage()
        {
            try
            {
                DataTable dtUpcoming = DataManager.GetUpcomingMessage();
                if (dtUpcoming.Rows.Count > 0)
                {
                    string UpcomingMessage = Convert.ToString(dtUpcoming.Rows[0]["UpComingMessage"]);
                    lblUpcomingMessage.Text = UpcomingMessage;
                    footer.Visible = true;
                    return 1;
                }
                else
                {
                    lblUpcomingMessage.Text = "";
                    footer.Visible = false;
                    return 0;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > GetUpcomingMessage"));
                return 0;
            }
        }
        /// <summary>
        /// Get during message
        /// </summary>
        private void GetDuringMessage()
        {
            try
            {
                DataTable dtUpcoming = DataManager.GetDuringMessage();
                if (dtUpcoming.Rows.Count > 0)
                {
                    string Duringmessage = Convert.ToString(dtUpcoming.Rows[0]["DuringMessage"]);
                    pnlConstruction.Visible = true;
                    pnlMain.Visible = false;
                    lblMEssage.Text = Duringmessage;
                }
                else
                {
                    lblMEssage.Text = "";
                    pnlConstruction.Visible = false;
                    pnlMain.Visible = true;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > GetDuringMessage"));
            }
        }
        /// <summary>
        /// Get during site active message
        /// </summary>
        private void GetDuringSiteActiveMessage()
        {
            try
            {
                DataTable dtUpcoming = DataManager.GetDuringSiteActiveMessage();
                if (dtUpcoming.Rows.Count > 0)
                {
                    string Duringmessage = Convert.ToString(dtUpcoming.Rows[0]["DuringMessage"]);
                    lblUpcomingMessage.Text = Duringmessage;
                    footer.Visible = true;
                }
                else
                {
                    if (GetUpcomingMessage() == 0)
                    {
                        lblUpcomingMessage.Text = "";
                        footer.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > GetDuringSiteActiveMessage"));
            }
        }

        #endregion

        /// <summary>
        /// click event of btnContinueWorking
        /// </summary>
        protected void btnContinueWorking_Click(object sender, EventArgs e)
        {
            //Do nothing. But the Session will be refreshed as a result of 
            //this method being called, which is its entire purpose.
        }

        #region WelcomeMessageSessionAndLogout
        /// <summary>
        /// Get home page url
        /// </summary>
        protected string GetHomePageURL()
        {
            string HomePageURL = Context.Request.Url.GetLeftPart(UriPartial.Authority).ToString();
            try
            {
                if (ConfigurationManager.AppSettings["ApplicationGroup"] == "CVS")
                    HomePageURL = "http://cvs.icucsolutions.com/login.aspx";
                else if (ConfigurationManager.AppSettings["ApplicationMode"].ToString().Contains("Demo"))
                    HomePageURL += "/remotepost.aspx";
                else if (ConfigurationManager.AppSettings["ApplicationMode"].ToString().Contains("Beta"))
                    HomePageURL += "/Login.aspx";
                else if (ConfigurationManager.AppSettings["ApplicationMode"].ToString().Contains("Live"))
                    HomePageURL = "http://www.icucsolutions.com";
            }

            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > GetHomePageURL"));
            }
            return HomePageURL;
        }
        /// <summary>
        /// set welcome message
        /// </summary>
        protected void SetWelcomeMessage()
        {
            try
            {
                if (Session["PersonId"] == null || String.IsNullOrEmpty(Convert.ToString(Session["PersonId"])))
                    return;

                string strEntityName = "";
                string strWelcome = DataManager.GetFieldValue("Persons where PersonId=" + Session["PersonId"], "FirstName + ' ' + LastName");

                if (Session["AccessLevel"].ToString() == "Chain")
                {
                    strEntityName = DataManager.GetFieldValue("AttributeValues where OwnerEntityId=" + Session["PersonId"] + " and AttributeId=18", "AttributeValue");

                    if (!String.IsNullOrEmpty(strEntityName))
                    {
                        strEntityName = "iControl - ";
                    }

                    strEntityName += DataManager.GetFieldValue("Chains where ChainId=" + Session["AccessValues"], "ChainName");

                    Session["ChainAccessId"] = Session["AccessValues"];
                }

                Session.Add("WelcomeMsg", "Welcome " + strWelcome + " (" + strEntityName + ")");
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > SetWelcomeMessage"));
            }

        }
        /// <summary>
        /// show admin message
        /// </summary>
        protected void ShowAdminMessage()
        {
            try
            {
                if (Session["PersonId"] == null || String.IsNullOrEmpty(Convert.ToString(Session["PersonId"])))
                    return;

                string AdminMessageId, AdminMessage;
                using (DataTable dt = DataManager.GetAdminMessageDetails(Session["PersonId"].ToString()))
                {
                    if (dt.Rows.Count > 0)
                    {
                        AdminMessageId = dt.Rows[0]["Message_Id"].ToString();
                        AdminMessage = dt.Rows[0]["Message"].ToString();
                        if (!String.IsNullOrEmpty(AdminMessage))
                        {
                            DataManager.SaveAdminMessageActivityDetails(Session["PersonId"].ToString(), AdminMessageId);

                            mpeAdminMsg.Show();
                            lblAdminMsg.Text = AdminMessage;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > ShowAdminMessage"));
            }
        }
        /// <summary>
        /// log out
        /// </summary>
        protected void logout(bool IsSessionOut = false)
        {
            try
            {
                string strLoginId = Session["PersonId"] != null ? Session["PersonId"].ToString() : DataManager.GetFieldValue("Logins where Login='" + Context.User.Identity.Name + "'", "OwnerEntityId");
                if (IsSessionOut == true)
                {
                    // user activity log
                    DataManager.SaveUserActivity(strLoginId, "0", "Session Out", Session.SessionID);
                }
                else
                {
                    // user activity log
                    DataManager.SaveUserActivity(strLoginId, "0", "Logout", Session.SessionID);
                    //update the lst entry
                    DataManager.UpdateUserLogInActivity(strLoginId, Convert.ToString(Session["GUID"]), 0);
                }

                if (Session["IDPLogin"] != null)
                {
                    // Logout locally.
                    FormsAuthentication.SignOut();

                    // Request logout at the identity provider.(CHETU HAS LEAVED IT AS COMMENTED AS PER TAL SUGGESTION)
                    //string partnerIdP = WebConfigurationManager.AppSettings[DemoSP.Login.AppSettings.PartnerIdP];
                    //  string partnerIdP = ConfigurationManager.AppSettings["PartnerIdP"].ToString();
                    // SAMLServiceProvider.InitiateSLO(Response, null, partnerIdP);
                }
                else
                {
                    Session.RemoveAll();
                    Session.Abandon();
                    FormsAuthentication.SignOut();
                    Response.Redirect(GetHomePageURL());
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > logout"));
            }
        }

        #endregion

        #region SetMenusDynamic
        /// <summary>
        /// set top navigation in vendor portal
        /// </summary>
        protected string SetTopNavVendorPortal()
        {
            string sMenu = "";
            try
            {
                using (DataTable DtAll = DataManager.GetUserMenues(Session["PersonId"].ToString()))
                {
                    using (DataTable Dt = DataManager.GetParentMenues(Session["PersonId"].ToString()))
                    {

                        foreach (DataRow row in Dt.Rows)
                        {
                            if (Convert.ToInt32(row["VerticalId"]) == 11)   // special case vertical- HOME
                            {
                                sMenu = sMenu + "<li class=\"active\"><a href=\"" + GetHomeUrl() + "\"> Home </a></li>";
                            }
                            else
                            {
                                if (ConfigurationManager.AppSettings["ApplicationGroup"] == "CVS")
                                {
                                    if (Convert.ToInt32(row["VerticalId"]) == 54)
                                    {
                                        foreach (DataRow rowMenu in DtAll.Select("verticalid=" + Convert.ToInt32(row["VerticalId"])))
                                        {
                                            sMenu += "<li><a href=\"" + rowMenu["PageURL"].ToString() + "?vid=" + rowMenu["verticalid"] + "&mid=" + rowMenu["MenuID"] + "\"" + ">" + rowMenu["MenuName"] + "</a></li>";
                                        }
                                    }
                                    else
                                    {
                                        if (MenuCount(Convert.ToInt16(row["VerticalID"]), DtAll) == 0)
                                            sMenu += "<li><a href=\"#\">" + row["VerticalName"] + "</a>";
                                        else
                                            sMenu += "<li><a href=\"#\">" + row["VerticalName"] + "</a>";

                                        sMenu += "<ul class=\"dropdown_menu\">" + SetMenusVendorPortal(Convert.ToInt16(row["VerticalID"]), DtAll) + "</ul>";
                                        sMenu += "</li>";
                                    }
                                }
                                else
                                {
                                    if (MenuCount(Convert.ToInt16(row["VerticalID"]), DtAll) == 0)
                                        sMenu += "<li><a href=\"#\">" + row["VerticalName"] + "</a>";
                                    else
                                        sMenu += "<li class=\"has_dropdown\"><a href=\"#\">" + row["VerticalName"] + "</a>";


                                    sMenu += "<ul class=\"dropdown_menu\">" + SetMenusVendorPortal(Convert.ToInt16(row["VerticalID"]), DtAll) + "</ul>";

                                    sMenu += "</li>";
                                }
                            }

                        }
                    }
                }

                SettingsUserPanel();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > SetTopNavVendorPortal"));
            }

            return sMenu;
        }
        /// <summary>
        /// Get home url
        /// </summary>
        protected string GetHomeUrl()
        {
            string url = "";
            try
            {
                url = DataManager.GetHomeUrl_SiteMaster(Session["PersonId"].ToString(), defaultPage, _ApplicationFolder);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > GetHomeUrl"));
            }
            return url;
        }
        /// <summary>
        /// set top navigation for vendor portal IE7
        /// </summary>
        protected string SetTopNavVendorPortalIE7()
        {
            string sMenu = "";
            try
            {
                using (DataTable DtAll = DataManager.GetUserMenues(Session["PersonId"].ToString()))
                {
                    using (DataTable Dt = DataManager.GetParentMenues(Session["PersonId"].ToString()))
                    {
                        foreach (DataRow row in Dt.Rows)
                        {
                            if (Convert.ToInt32(row["VerticalId"]) == 11)   // special case vertical- HOME
                            {
                                sMenu = sMenu + "<li><a href=\"" + GetHomeUrl() + "\"> Home </a></li>";
                            }
                            else
                            {
                                if (MenuCount(Convert.ToInt16(row["VerticalID"]), DtAll) == 0)
                                    sMenu += "<li><a href=\"#\">" + row["VerticalName"] + "</a>";
                                else
                                    sMenu += "<li><a href=\"#\">" + row["VerticalName"] + "</a>";


                                sMenu += "<ul class=\"dropdown_menu\">" + SetMenusVendorPortalIE7(Convert.ToInt16(row["VerticalID"]), DtAll) + "</ul>";

                                sMenu += "</li>";
                            }

                        }
                    }
                }

                SettingsUserPanel();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > logout"));
            }

            return sMenu;
        }
        /// <summary>
        /// set menu for vendor portal
        /// </summary>
        protected string SetMenusVendorPortal(int vertialID, DataTable Dt)
        {
            string sMenu = "";
            try
            {
                foreach (DataRow row in Dt.Select("verticalid=" + vertialID))
                {
                    sMenu += "<li><a href=\"" + "../" + row["PageURL"].ToString() + "?vid=" + vertialID + "&mid=" + row["MenuID"] + "\">" + row["MenuName"] + "</a></li>";
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > SetMenusVendorPortal"));
            }
            return sMenu;
        }
        /// <summary>
        /// set menu for vendor portal IE7
        /// </summary>
        protected string SetMenusVendorPortalIE7(int vertialID, DataTable Dt)
        {
            string sMenu = "";
            try
            {
                foreach (DataRow row in Dt.Select("verticalid=" + vertialID))
                {
                    sMenu += "<li><a href=\"" + "../" + row["PageURL"].ToString() + "?vid=" + vertialID + "&mid=" + row["MenuID"] + "\">" + row["MenuName"] + "</a></li>";
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > SetMenusVendorPortalIE7"));
            }
            return sMenu;
        }
        /// <summary>
        /// Get menu count
        /// </summary>
        protected int MenuCount(int VertcalId, DataTable dt)
        {
            int cnt = 0;
            try
            {
                string lastitm = "";
                foreach (DataRow row in dt.Select("verticalid=" + VertcalId))
                {
                    if (lastitm != row["MenuName"].ToString())
                        cnt += 1;

                    lastitm = row["MenuName"].ToString();
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > MenuCount"));
            }
            return cnt;
        }

        #endregion

        #region SetMenusHardCoded

        protected void SettingsUserPanel()
        {
            try
            {
                //Change User Profile
                if (!DataManager.IsCorporateUser(LoginUserName))
                {
                    divChangeUserProfile.Style["display"] = "inline";
                    divProfileName.Style["display"] = "inline";

                    if (Session["UserProfile"] == null && Session["VendorNumber"] == null)
                        GetDefaultUserProfile();

                    FillProfileValues();
                }
                else
                {
                    Session["UserRoleId"] = "0";
                }
            }

            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > SettingsUserPanel"));
            }
        }

        #region ChangeUserProfile

        protected void GetDefaultUserProfile()
        {
            try
            {
                string ProfileName = "";
                string[] arrValues = new string[2];
                arrValues = DataManager.GetUserProfileAndVendorNumber(this.LoginUserName);

                if (arrValues != null && arrValues.Length > 0)
                {
                    Session["UserProfile"] = arrValues[0].Trim();
                    Session["VendorNumber"] = arrValues[1].Trim();
                    ProfileName = arrValues[0].Trim();
                    if (ProfileName.ToUpper() == "VENDOR")
                    {
                        LblProfileName.Text = "Current Profile: " + DataManager.GetVendorNameByVendorNo(Session["VendorNumber"].ToString()) + " #" + Session["VendorNumber"].ToString();
                    }
                    else
                    {
                        LblProfileName.Text = "Current Profile: " + ProfileName;
                    }
                }

                Session["UserRoleId"] = DataManager.GetUserRoleIdByUserNameAndProfileName(Session["UserName"].ToString(), ProfileName);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > GetDefaultUserProfile"));
            }
        }

        protected void FillProfileValues()
        {
            try
            {
                FillUserProfileList();
                if (Session["UserProfile"] != null)
                {
                    ListItem match = ddlUserProfileList.Items.FindByText(Session["UserProfile"].ToString());
                    if (match != null)
                        match.Selected = true;
                    if (ddlUserProfileList.SelectedItem.Text != "VENDOR")
                    {
                        LblProfileName.Text = "Current Profile: " + ddlUserProfileList.SelectedItem.Text;
                    }
                }
                else
                {
                    Session["UserProfile"] = ddlUserProfileList.SelectedItem.Text;
                }

                FillVendorNumberList();
                if (Session["VendorNumber"] != null)
                {
                    ListItem match = ddlVendorNumberList.Items.FindByText(Session["VendorNumber"].ToString());
                    if (match != null)
                        match.Selected = true;
                    if (ddlVendorNumberList.SelectedItem.Text.ToUpper() == "Select Vendor (If Relevant)".ToUpper())
                    {

                        LblProfileName.Text = "Current Profile: " + ddlUserProfileList.SelectedItem.Text;
                    }
                    else
                    {
                        LblProfileName.Text = "Current Profile: " + DataManager.GetVendorNameByVendorNo(ddlVendorNumberList.SelectedItem.Text) + " #" + ddlVendorNumberList.SelectedItem.Text;
                    }
                }
                else
                {
                    Session["VendorNumber"] = ddlVendorNumberList.SelectedItem.Text;

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > FillProfileValues"));
            }
        }

        protected void FillUserProfileList()
        {
            try
            {
                using (DataTable dtUserProfiles = DataManager.GetUserProfileList(this.LoginUserName))
                {

                    if (dtUserProfiles.Rows.Count > 0)
                    {
                        ddlUserProfileList.DataSource = dtUserProfiles;
                        ddlUserProfileList.DataTextField = "ProfileName";
                        ddlUserProfileList.DataValueField = "ProfileName";
                        ddlUserProfileList.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > FillUserProfileList"));
            }
        }

        protected void FillVendorNumberList()
        {
            try
            {
                if (ddlUserProfileList.Items.Count > 0)
                {

                    using (DataTable dtVendors = DataManager.GetVendorNumberList(this.LoginUserName, ddlUserProfileList.SelectedItem.Text))
                    {

                        if (dtVendors.Rows.Count > 0)
                        {
                            ddlVendorNumberList.DataSource = dtVendors;
                            ddlVendorNumberList.DataTextField = "Vendor_Num";
                            ddlVendorNumberList.DataValueField = "Vendor_Num";
                            ddlVendorNumberList.DataBind();
                        }
                        else
                            ddlVendorNumberList.Items.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > FillVendorNumberList"));
            }
        }

        protected void ddlUserProfileList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillVendorNumberList();
                mpeChangeUserProfile.Show();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > ddlUserProfileList_SelectedIndexChanged"));
            }
        }

        protected void btnSaveUserProfile_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlUserProfileList.SelectedValue.ToString() != "-1")
                {

                    DataManager.DeleteUserDefaultProfile_ByUserName(this.LoginUserName);

                    DataManager.InsertUserDefaultProfileVendor(this.LoginUserName, ddlUserProfileList.SelectedItem.Text.Replace("'", "''"), ddlVendorNumberList.SelectedItem.Text.Replace("'", "''"));

                    Session["UpdateMessage"] = IcontrolFilterResources.ProfileChangedSuccessfully;
                    Session["UserProfile"] = ddlUserProfileList.SelectedItem.Text.ToString();
                    Session["VendorNumber"] = ddlVendorNumberList.SelectedItem.Text.ToString();
                    Session["UserRoleId"] = DataManager.GetUserRoleIdByUserNameAndProfileName(Session["UserName"].ToString(), Session["UserProfile"].ToString());

                    mpeChangeUserProfile.Hide();
                    string defaultPage = "";

                    using (DataTable dt = DataManager.GetVerticalIdAndDefaultPageUrl())
                    {
                        if (dt.Rows.Count > 0)
                        {
                            string applicationfolderpath = Context.Request.ApplicationPath;
                            string url = applicationfolderpath + dt.Rows[0]["url"].ToString();
                            defaultPage = url + "?vid=" + dt.Rows[0]["VerticalID"].ToString() + "&mid=379";
                        }
                    }
                    Response.Redirect(defaultPage);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > btnSaveUserProfile_Click"));
            }
        }

        protected void btnCloseUserProfile_Click(object sender, EventArgs e)
        {
            try
            {
                mpeChangeUserProfile.Hide();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > btnCloseUserProfile_Click"));
            }
        }

        #endregion

        #endregion
        /// <summary>
        /// log out click
        /// </summary>
        protected void imgLogout_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                logout();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > imgLogout_Click"));
            }

        }
        /// <summary>
        /// Menu textbox search text change event
        /// </summary>
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtSearch.Text.Trim()))
                {
                    Response.Redirect("/ReportSearch.aspx?SearchText=" + txtSearch.Text.ToString());
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > txtSearch_TextChanged"));
            }
        }
        /// <summary>
        /// Menu help icon click event
        /// </summary>
        protected void imgHelp_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Response.Redirect("/ProjectForms/UserHelp.aspx", false);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Site.Master.cs > imgHelp_Click"));
            }
        }
    }
}