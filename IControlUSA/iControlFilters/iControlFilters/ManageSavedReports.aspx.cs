﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using iControlGenricFilters.ResourceFiles;
using iControlDataLayer;
using AjaxControlToolkit;
using Telerik.Web.UI;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Script.Serialization;
using iControlGenricFilters.LIB;
using iControlDataLayer.Helper;
using System.Configuration;
using System.Web;
using System.Data.SqlClient;

namespace iControlGenricFilters
{
    public partial class ManageSavedReports : System.Web.UI.Page
    {
        IControlHelper helper = new IControlHelper();

        //Get report Id from Query string
        private string ReportId
        {
            get
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Id"]))
                {
                    return Convert.ToString(Request.QueryString["Id"]);
                }
                else if (!String.IsNullOrEmpty(Request.QueryString["mid"]))
                {
                    return Convert.ToString(Request.QueryString["mid"]);
                }
                else
                {
                    return "";
                }

            } 
        } 

        //Get Login User Name from session
        public string LoginUserName
        {
            get
            {
                return Convert.ToString(this.Session["UserName"]);
            }
        }

        //get Email from 
        public static string EmailFrom
        {
            get
            {
                return !string.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailFrom"]) ? ConfigurationManager.AppSettings["EmailFrom"] : "info@icontroldsd.com";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    ViewState["WeekValue"] = "52";
                    Hfdate.Value = System.DateTime.Now.ToString("MM/dd/yyyy");

                    if (Request.QueryString["mid"] == null)
                        topHeader.InnerText = "My Saved Reports";
                    else
                        topHeader.InnerText = DataManager.GetPageTitle(ReportId, Session["ActiveMenuId"] != null ? Session["ActiveMenuId"].ToString() : "");

                    FillShareUsers();
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > RadDates_SelectedIndexChanged:"));
            }
        }

        #region Button Click
        /// <summary>
        /// btnSearch click event
        /// </summary>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                grdMembers.Rebind();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > btnSearch_Click:"));
            }
        }
        /// <summary>
        /// btnSaveMessage click event
        /// </summary>
        protected void btnSaveMessage_Click(object sender, EventArgs e)
        {
            try
            {
                DataManager.SaveMessage(hdnReportId.Value);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                 "err_msg",
                 "alert('Message has been saved successfully.');",
                 true);

                grdMembers.Rebind();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > btnSaveMessage_Click:"));
            }
        }
        /// <summary>
        /// btnDeleteMessage click event
        /// </summary>
        protected void btnDeleteMessage_Click(object sender, EventArgs e)
        {
            try
            {
                DataManager.SaveMessage(hdnReportId.Value);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Message has been deleted successfully.');  window.location ='ManageSavedReports.aspx';", true);
                grdMembers.Rebind();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > btnDeleteMessage_Click:"));
            }
        }
        /// <summary>
        /// btnExport click event
        /// </summary>
        protected void btnExport_Click(object sender, ImageClickEventArgs e)
        {
            ExportFile("csv");
        }
        /// <summary>
        /// btnExportZip click event
        /// </summary>
        protected void btnExportZip_Click(object sender, ImageClickEventArgs e)
        {
            ExportFile("zip");
        }
        /// <summary>
        /// BtnUpdate click event
        /// </summary>
        protected void BtnUpdate_click(object sender, EventArgs e)
        {
            try
            {
                string dateVal = null;
                if (txtReportName.Text.Length > 0)
                {
                    DataTable ds = DataManager.GetReportInfo(this.ReportId, HfReportID.Value, this.LoginUserName);
                    if (ds.Rows.Count > 0)
                    {
                        mpeRequest.Show();
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                  "err_msg",
                  "alert('Report Name Already Exist');",
                  true);
                    }
                    else
                    {
                        DateTime? enddate = null;
                        if (txtrepdate.Text == "__/__/____" || String.IsNullOrEmpty(txtrepdate.Text))
                        {

                        }
                        else
                        {
                            enddate = Convert.ToDateTime(txtrepdate.Text);
                        }
                        string Emailid = "";
                        string ScheduleId = "";
                        if (rdSchedule.SelectedValue == "Y")
                        {
                            if (String.IsNullOrEmpty(txtemail.Text.Trim().Replace("'", "''")))
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                               "err_msg",
                               "alert('Please enter Email Id');",
                               true);
                                mpeRequest.Show();
                                grdMembers.Rebind();
                                return;
                            }
                            else
                            {
                                Emailid = txtemail.Text.Replace("'", "''");
                                ScheduleId = DrpReportTime.SelectedValue;

                            }
                        }
                        else
                        {
                            Emailid = "";
                            ScheduleId = "";
                            enddate = null;
                        }

                        string str = "";
                        if (enddate == null)
                            str = "update SavedReports set ReportName='" + txtReportName.Text.Replace("'", "''").Trim() + "',ReportDescription='" + txtReportDescr.Text.Replace("'", "''").Trim() + "',DateRange='" + dateVal + "',EmailId='" + Emailid + "',ScheduleId='" + ScheduleId + "',EndDate=null where  ReportID='" + HfReportID.Value + "'";
                        else
                            str = "update SavedReports set ReportName='" + txtReportName.Text.Replace("'", "''").Trim() + "',ReportDescription='" + txtReportDescr.Text.Replace("'", "''").Trim() + "',DateRange='" + dateVal + "',EmailId='" + Emailid + "',ScheduleId='" + ScheduleId + "',EndDate='" + enddate + "' where  ReportID='" + HfReportID.Value + "'";


                        using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                        {
                            dataAccessProvider.ExecuteNonQuery(str);

                            txtReportDescr.Text = "";
                            txtReportName.Text = "";
                            //RadDates.ClearSelection();
                            // DrpDate.ClearSelection();
                            DrpDateVal.ClearSelection();
                            txtemail.Text = "";
                            txtrepdate.Text = "";
                            grdMembers.Rebind();
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                         "Confirmation Message",
                         "alert('Report has been updated successfully');",
                         true);
                        }


                    }
                    grdMembers.Rebind();
                }
                else
                {
                    mpeRequest.Show();
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
              "err_msg",
              "alert('Please enter report name');",
              true);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > BtnUpdate_click:"));
            }
        }
        /// <summary>
        /// btnsaveShare click event
        /// </summary>
        protected void btnsaveShare_click(object sender, EventArgs e)
        {
            try
            {

                string reportExistedFor = string.Empty;

                if (RdShareUsers.CheckedItems.Count < 1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                                              "err_msg", "alert('Please select user');", true);
                    return;
                }
                foreach (RadComboBoxItem itm in RdShareUsers.CheckedItems)
                {
                    bool isReportExisted = DataManager.SharedReportNameExists(txtSharedReportName.Text.Trim(), itm.Value.ToString());
                    if (isReportExisted)
                        reportExistedFor += itm.Value.ToString() + ",";
                }
                if (!string.IsNullOrEmpty(reportExistedFor))  // (reportExistedFor != string.Empty)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                          "err_msg", "alert('Report Name Already Exist.');", true);
                    mpeShare.Show();
                    grdMembers.Rebind();
                    return;
                }

                string str = "";
                string UserProfile = "";
                if (DataManager.IsCorporateUser(this.LoginUserName))
                {
                    UserProfile = DataManager.GetUserRoleName(this.LoginUserName).ToLower();
                }
                else
                {
                    string ProfileName, VendorNumber;
                    if (Session["UserProfile"] == null || Session["VendorNumber"] == null)
                    {
                        string[] arrValues = new string[2];
                        arrValues = DataManager.GetUserProfileAndVendorNumber(this.LoginUserName);
                        ProfileName = arrValues[0].Trim();
                        VendorNumber = arrValues[1].Trim();
                    }
                    else
                    {
                        ProfileName = Session["UserProfile"].ToString().Trim();
                        VendorNumber = Session["VendorNumber"].ToString().Trim();
                    }
                    if (VendorNumber.ToLower().Contains("Select Vendor (If Relevant)".ToLower()))
                    {
                        if (DataManager.GetUserRoleBasedOnProfile(Session["UserName"].ToString(), ProfileName) == "3")
                            UserProfile = "vendors";
                        else
                            UserProfile = "products";
                    }
                    else
                    {
                        UserProfile = "vendors";
                    }
                }
                int i = 0;
                string Hierarchy = "";
                string Store = "";
                bool combo = false;
                string SharedReportName = "";
                using (DataTable dsrep = DataManager.GetReportInfoByReportId(HfShare.Value))
                {
                    if (dsrep.Rows.Count > 0)
                    {
                        string[] RepPar = dsrep.Rows[0]["ReportParameters"].ToString().Split('&');
                        for (i = 0; i < RepPar.Length; i++)
                        {
                            if (RepPar[i].ToString().Contains("SharedHierarchy"))
                            {
                                Hierarchy = "1";
                            }
                            if (RepPar[i].ToString().Contains("PnlCustomStore"))
                            {
                                Store = "1";
                            }
                            if (RepPar[i].ToString().Contains("share_Combo"))
                            {
                                combo = true;
                            }
                        }
                        if (!String.IsNullOrEmpty(Hierarchy))
                        {
                            string strMessage = ShareReport();
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Confirmation Message", "alert('" + strMessage + "');", true);
                        }
                        else if (!String.IsNullOrEmpty(Store))
                        {
                            string storemessage = ShareReportforStore();
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Confirmation Message", "alert('" + storemessage + "');", true);
                        }
                        else if (combo)
                        {
                            string message = ShareComboReport();
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Confirmation Message", "alert('" + message + "');", true);
                        }
                        else
                        {
                            if (dsrep.Rows.Count > 0)
                            {
                                if (String.IsNullOrEmpty(txtSharedReportName.Text.Trim()))
                                    SharedReportName = dsrep.Rows[0]["MenuName"].ToString();
                                else
                                    SharedReportName = txtSharedReportName.Text.Trim();

                                foreach (RadComboBoxItem ltm in RdShareUsers.Items)
                                {
                                    bool result = false;
                                    if (ltm.Checked && !String.IsNullOrEmpty(ltm.Value.ToString()))
                                    {

                                        str = "Insert into SavedReports(ReportName,ReportDescription,Username,MenuId,TableauURL,ReportParameters,DateRange,OriginalReportName,SendReportNow,UserProfile,shareby,ShareMessage)values('" + SharedReportName.Replace("'", "''").Trim() + "','" + dsrep.Rows[0]["ReportDescription"].ToString().Replace("'", "''") + "','" + ltm.Value + "','" + dsrep.Rows[0]["MenuId"].ToString() + "','" + dsrep.Rows[0]["TableauURL"].ToString() + "','" + dsrep.Rows[0]["ReportParameters"].ToString() + "','" + dsrep.Rows[0]["DateRange"].ToString() + "','" + dsrep.Rows[0]["OriginalReportName"].ToString() + "','0','" + UserProfile + "','" + Session["UserName"] + "','" + txtMessage.Text.Replace("'", "''").Trim() + "')";
                                        using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                                        {
                                            dataAccessProvider.ExecuteNonQuery(str);
                                            result = true;
                                        }
                                        if (result)
                                        {
                                            string getemail = "Select * from SETUP_USERDATAACCESS where username = '" + ltm.Value.ToString() + "' and EMAIL is not null limit 1";
                                            using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                                            {
                                                DataTable dtuser = dataAccessProvider.Select(getemail);
                                                if (dtuser.Rows[0]["Email"].ToString().Trim().Length > 0)
                                                    SendEmail(dtuser.Rows[0]["Email"].ToString(), this.LoginUserName, dtuser.Rows[0]["Name"].ToString(), SharedReportName, txtMessage.Text.Replace("'", "''"));
                                            }

                                        }

                                    }
                                }
                            }
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                 "Confirmation Message",
                                 "alert('Report has been shared successfully');",
                                 true);

                        }
                        grdMembers.Rebind();

                    }
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > btnsaveShare_click:"));
            }
        }
        /// <summary>
        /// share report
        /// </summary>
        protected string ShareReport()
        {
            string Message = "";
            try
            {
                string str = "";
                string UserProfile = "";
                if (DataManager.IsCorporateUser(this.LoginUserName))
                {
                    UserProfile = DataManager.GetUserRoleName(this.LoginUserName).ToLower();
                }
                else
                {
                    string ProfileName, VendorNumber;
                    if (Session["UserProfile"] == null || Session["VendorNumber"] == null)
                    {
                        string[] arrValues = new string[2];
                        arrValues = DataManager.GetUserProfileAndVendorNumber(this.LoginUserName);
                        ProfileName = arrValues[0].Trim();
                        VendorNumber = arrValues[1].Trim();
                    }
                    else
                    {
                        ProfileName = Session["UserProfile"].ToString().Trim();
                        VendorNumber = Session["VendorNumber"].ToString().Trim();
                    }
                    if (VendorNumber.ToLower().Contains("Select Vendor (If Relevant)".ToLower()))
                    {
                        if (DataManager.GetUserRoleBasedOnProfile(Session["UserName"].ToString(), ProfileName) == "3")
                            UserProfile = "vendors";
                        else
                            UserProfile = "products";
                    }
                    else
                    {
                        UserProfile = "vendors";
                    }
                }
                int i = 0;
                int UserCount = 0;
                int ShareCount = 0;
                string getvalue1 = "";
                string SharedReportName = "";
                DataTable dsrep = DataManager.GetReportInfoByReportId(HfShare.Value);
                string[] RepPar = dsrep.Rows[0]["ReportParameters"].ToString().Split('&');
                for (i = 0; i < RepPar.Length; i++)
                {
                    if (RepPar[i].ToString().Contains("CUSTOM_HIERARCHY_ID"))
                    {
                        string[] getdt = RepPar[i].ToString().Split('=');
                        getvalue1 = getdt[1];
                    }
                    if (RepPar[i].ToString().Contains("ch_id"))
                    {
                        string[] getdt = RepPar[i].ToString().Split('=');
                        getvalue1 = getdt[1];
                    }
                }

                if (dsrep.Rows.Count > 0)
                {
                    if (String.IsNullOrEmpty(txtSharedReportName.Text.Trim()))
                        SharedReportName = dsrep.Rows[0]["MenuName"].ToString();
                    else
                        SharedReportName = txtSharedReportName.Text.Trim();
                    foreach (RadComboBoxItem ltm in RdShareUsers.CheckedItems)
                    {
                        if (ltm.Checked && !String.IsNullOrEmpty(ltm.Value.ToString()))
                        {
                            UserCount += 1;
                            if (rdShareHierarchy.SelectedValue == "1")
                                if (DataManager.IsHierarchyShared(getvalue1, ltm.Value.ToString()) == false)
                                {
                                    DataManager.ShareHierarchy_Owned(getvalue1, ltm.Value.ToString());
                                }
                            if (DataManager.IsHierarchyShared(getvalue1, ltm.Value.ToString()))
                            {

                                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                                {
                                    List<IDataParameter> arParam = new List<IDataParameter>();
                                    str = "insert into SavedReports(ReportName,ReportDescription,Username,MenuId,TableauURL,ReportParameters,DateRange,OriginalReportName,SendReportNow,UserProfile,shareby,ShareMessage) values(@ReportName,@ReportDescription, @Username,@MenuId,@TableauURL,@ReportParameters,@DateRange,@OriginalReportName,@SendReportNow,@UserProfile,@shareby,@ShareMessage)";
                                    arParam.Add(new SqlParameter("@ReportName", SharedReportName.Replace("'", "''")));
                                    arParam.Add(new SqlParameter("@ReportDescription", dsrep.Rows[0]["ReportDescription"].ToString().Replace("'", "''")));
                                    arParam.Add(new SqlParameter("@Username", ltm.Value));
                                    arParam.Add(new SqlParameter("@MenuId", dsrep.Rows[0]["MenuId"].ToString()));
                                    arParam.Add(new SqlParameter("@TableauURL", dsrep.Rows[0]["TableauURL"].ToString()));
                                    arParam.Add(new SqlParameter("@ReportParameters", dsrep.Rows[0]["ReportParameters"].ToString()));
                                    arParam.Add(new SqlParameter("@DateRange", dsrep.Rows[0]["DateRange"].ToString()));
                                    arParam.Add(new SqlParameter("@OriginalReportName", dsrep.Rows[0]["OriginalReportName"].ToString()));
                                    arParam.Add(new SqlParameter("@SendReportNow", "0"));
                                    arParam.Add(new SqlParameter("@UserProfile", UserProfile));
                                    arParam.Add(new SqlParameter("@shareby", Session["UserName"]));
                                    arParam.Add(new SqlParameter("@ShareMessage", txtMessage.Text.Replace("'", "''").Trim()));

                                    dataAccessProvider.ExecuteNonQuery(str, arParam, false);

                                    string getemail = "Select *  from SETUP_USERDATAACCESS where username='" + ltm.Value.ToString() + "' and EMAIL is not null limit 1";
                                    using (DataTable dt = dataAccessProvider.Select(getemail))
                                    {
                                        ShareCount += 1;
                                        if (dt.Rows[0]["Email"].ToString().Trim().Length > 0)
                                            SendEmail(dt.Rows[0]["Email"].ToString(), Session["UserName"].ToString(), dt.Rows[0]["Name"].ToString(), SharedReportName, txtMessage.Text.Replace("'", "''"));
                                    }

                                }

                            }

                        }
                    }
                    if (ShareCount == UserCount)
                        if (RdShareUsers.CheckedItems.Count > 1)
                        {
                            Message = "Report has been shared with all the selected users.";
                        }
                        else
                        {
                            Message = "Report has been shared with the selected user.";
                        }
                    else if (ShareCount > 0)
                        Message = "Report has been shared with " + ShareCount + " user(s) out of " + UserCount;
                    else
                        Message = "Report could not be shared with any user(s) as hierarchy is not shared.";

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > RadDates_SelectedIndexChanged:"));
            }
            return Message;
        }
        /// <summary>
        /// share report for store
        /// </summary>
        protected string ShareReportforStore()
        {
            string Message = "";
            try
            {
                string str = "";
                string UserProfile = "";
                if (DataManager.IsCorporateUser(this.LoginUserName))
                {
                    UserProfile = DataManager.GetUserRoleName(this.LoginUserName).ToLower();
                }
                else
                {
                    string ProfileName, VendorNumber;
                    if (Session["UserProfile"] == null || Session["VendorNumber"] == null)
                    {
                        string[] arrValues = new string[2];
                        arrValues = DataManager.GetUserProfileAndVendorNumber(Session["UserName"].ToString());
                        ProfileName = arrValues[0].Trim();
                        VendorNumber = arrValues[1].Trim();
                    }
                    else
                    {
                        ProfileName = Session["UserProfile"].ToString().Trim();
                        VendorNumber = Session["VendorNumber"].ToString().Trim();
                    }
                    if (VendorNumber.ToLower().Contains("Select Vendor (If Relevant)".ToLower()))
                    {
                        if (DataManager.GetUserRoleBasedOnProfile(Session["UserName"].ToString(), ProfileName) == "3")
                            UserProfile = "vendors";
                        else
                            UserProfile = "products";
                    }
                    else
                    {
                        UserProfile = "vendors";
                    }
                }
                int i = 0;
                int UserCount = 0;
                int ShareCount = 0;
                int SharedStore = 0;
                string getvalue1 = "";
                string SharedReportName = "";
                DataTable dsrep = DataManager.GetReportInfoByReportId(HfShare.Value);

                string[] RepPar = dsrep.Rows[0]["ReportParameters"].ToString().Split('&');
                for (i = 0; i < RepPar.Length; i++)
                {
                    if (RepPar[i].ToString().Contains("cg_id"))
                    {
                        string[] getdt = RepPar[i].ToString().Split('=');
                        getvalue1 = AppManager.DecodeSpecialCharacters(getdt[1]);


                    }
                }

                if (dsrep.Rows.Count > 0)
                {
                    if (String.IsNullOrEmpty(txtSharedReportName.Text.Trim()))
                        SharedReportName = dsrep.Rows[0]["MenuName"].ToString();
                    else
                        SharedReportName = txtSharedReportName.Text.Trim();
                    foreach (RadComboBoxItem ltm in RdShareUsers.CheckedItems)
                    {
                        if (ltm.Checked && !String.IsNullOrEmpty(ltm.Value.ToString()))
                        {
                            UserCount += 1;
                            string[] getdt1 = getvalue1.Split(',');
                            int j = 0;
                            for (j = 0; j < getdt1.Length; j++)
                            {
                                if (rdShareStore.SelectedValue == "1")
                                {

                                    if (DataManager.IsStoreShared(getdt1[j], ltm.Value.ToString()) == false)
                                    {
                                        DataManager.SharedCustomStore_Owned(getdt1[j], ltm.Value);
                                        SharedStore += 1;
                                    }
                                }
                                if (DataManager.IsStoreShared(getdt1[j], ltm.Value.ToString()))
                                {
                                    SharedStore += 1;
                                }
                                else
                                {
                                    //SharedStore = 0 ;
                                }

                            }

                            if (SharedStore > 0)
                            {
                                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                                {
                                    List<IDataParameter> arParam = new List<IDataParameter>();
                                    str = "insert into SavedReports(ReportName,ReportDescription,Username,MenuId,TableauURL,ReportParameters,DateRange,OriginalReportName,SendReportNow,UserProfile,shareby,ShareMessage) values(@ReportName,@ReportDescription, @Username,@MenuId,@TableauURL,@ReportParameters,@DateRange,@OriginalReportName,@SendReportNow,@UserProfile,@shareby,@ShareMessage)";
                                    arParam.Add(new SqlParameter("@ReportName", SharedReportName.Replace("'", "''")));
                                    arParam.Add(new SqlParameter("@ReportDescription", dsrep.Rows[0]["ReportDescription"].ToString().Replace("'", "''")));
                                    arParam.Add(new SqlParameter("@Username", ltm.Value));
                                    arParam.Add(new SqlParameter("@MenuId", dsrep.Rows[0]["MenuId"].ToString()));
                                    arParam.Add(new SqlParameter("@TableauURL", dsrep.Rows[0]["TableauURL"].ToString()));
                                    arParam.Add(new SqlParameter("@ReportParameters", dsrep.Rows[0]["ReportParameters"].ToString()));
                                    arParam.Add(new SqlParameter("@DateRange", dsrep.Rows[0]["DateRange"].ToString()));
                                    arParam.Add(new SqlParameter("@OriginalReportName", dsrep.Rows[0]["OriginalReportName"].ToString()));
                                    arParam.Add(new SqlParameter("@SendReportNow", "0"));
                                    arParam.Add(new SqlParameter("@UserProfile", UserProfile));
                                    arParam.Add(new SqlParameter("@shareby", Session["UserName"]));
                                    arParam.Add(new SqlParameter("@ShareMessage", txtMessage.Text.Replace("'", "''").Trim()));

                                    dataAccessProvider.ExecuteNonQuery(str, arParam, false);

                                    string getemail = "Select *  from SETUP_USERDATAACCESS where username='" + ltm.Value.ToString() + "' and EMAIL is not null limit 1";
                                    using (DataTable dt = dataAccessProvider.Select(getemail))
                                    {
                                        ShareCount += 1;
                                        if (dt.Rows[0]["Email"].ToString().Trim().Length > 0)
                                            SendEmail(dt.Rows[0]["Email"].ToString(), Session["UserName"].ToString(), dt.Rows[0]["Name"].ToString(), SharedReportName, txtMessage.Text.Replace("'", "''"));
                                    }
                                }
                            }
                        }
                    }
                    if (ShareCount == UserCount)
                        if (RdShareUsers.CheckedItems.Count > 1)
                        {
                            Message = "Report has been shared with all the selected users.";
                        }
                        else
                        {
                            Message = "Report has been shared with the selected user.";
                        }
                    else if (ShareCount > 0)
                        Message = "Report has been shared with " + ShareCount + " user(s) out of " + UserCount;
                    else
                        Message = "Report could not be shared with any user(s) as Store is not shared.";

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > ShareReportforStore:"));
            }
            return Message;
        }
        /// <summary>
        /// share combo report
        /// </summary>
        protected string ShareComboReport()
        {
            string Message = "";
            try
            {
                string str = "";
                string UserProfile = "";

                if (DataManager.IsCorporateUser(this.LoginUserName))
                {
                    UserProfile = DataManager.GetUserRoleName(this.LoginUserName).ToLower();
                }
                else
                {
                    string ProfileName, VendorNumber;
                    if (Session["UserProfile"] == null || Session["VendorNumber"] == null)
                    {
                        string[] arrValues = new string[2];
                        arrValues = DataManager.GetUserProfileAndVendorNumber(Session["UserName"].ToString());
                        ProfileName = arrValues[0].Trim();
                        VendorNumber = arrValues[1].Trim();
                    }
                    else
                    {
                        ProfileName = Session["UserProfile"].ToString().Trim();
                        VendorNumber = Session["VendorNumber"].ToString().Trim();
                    }
                    if (VendorNumber.ToLower().Contains("Select Vendor (If Relevant)".ToLower()))
                    {
                        if (DataManager.GetUserRoleBasedOnProfile(Session["UserName"].ToString(), ProfileName) == "3")
                            UserProfile = "vendors";
                        else
                            UserProfile = "products";
                    }
                    else
                    {
                        UserProfile = "vendors";
                    }
                }

                int i = 0;
                int UserCount = 0;
                int ShareCount = 0;
                int SharedStore = 0;
                string getValueStore = "";
                string getValueHierarchy = "";
                string SharedReportName = "";
                DataTable dsrep = DataManager.GetReportInfoByReportId(HfShare.Value);
                string[] RepPar = dsrep.Rows[0]["ReportParameters"].ToString().Split('&');
                for (i = 0; i < RepPar.Length; i++)
                {
                    if (RepPar[i].ToString().Contains("cg_id"))
                    {
                        string[] getdt = RepPar[i].ToString().Split('=');
                        getValueStore = AppManager.DecodeSpecialCharacters(getdt[1]);
                    }
                    if (RepPar[i].ToString().Contains("CUSTOM_HIERARCHY_ID"))
                    {
                        string[] getdt = RepPar[i].ToString().Split('=');
                        getValueHierarchy = getdt[1];
                    }
                    if (RepPar[i].ToString().Contains("ch_id"))
                    {
                        string[] getdt = RepPar[i].ToString().Split('=');
                        getValueHierarchy = getdt[1];
                    }
                }

                if (dsrep.Rows.Count > 0)
                {
                    if (String.IsNullOrEmpty(txtSharedReportName.Text.Trim()))
                        SharedReportName = dsrep.Rows[0]["MenuName"].ToString();
                    else
                        SharedReportName = txtSharedReportName.Text.Trim();

                    foreach (RadComboBoxItem ltm in RdShareUsers.CheckedItems)
                    {
                        int StoreShared = 0;
                        bool isHierarchyShared = false;
                        if (ltm.Checked && !String.IsNullOrEmpty(ltm.Value.ToString()))
                        {
                            UserCount += 1;
                            string[] getdt1 = getValueStore.Split(',');
                            int j = 0;
                            for (j = 0; j < getdt1.Length; j++)
                            {
                                if (rdComboStoreHierarchy.SelectedValue == "1")
                                {
                                    if (DataManager.IsStoreShared(getdt1[j], ltm.Value.ToString()) == false)
                                    {
                                        DataManager.SharedCustomStore_Owned(getdt1[j], ltm.Value);
                                        SharedStore++;
                                    }
                                }
                                //for 2nd option
                                if (DataManager.IsStoreShared(getdt1[j], ltm.Value.ToString()))
                                    StoreShared++;
                            }

                            if (rdComboStoreHierarchy.SelectedValue == "1")
                            {
                                if (DataManager.IsHierarchyShared(getValueHierarchy, ltm.Value.ToString()) == false)
                                {
                                    DataManager.ShareHierarchy_Owned(getValueHierarchy, ltm.Value.ToString());
                                    isHierarchyShared = true;
                                }
                            }

                            //for 2nd option
                            if (DataManager.IsHierarchyShared(getValueHierarchy, ltm.Value.ToString()))
                                isHierarchyShared = true;
                            else
                                isHierarchyShared = false;

                            if (isHierarchyShared && StoreShared > 0)
                            {
                                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                                {
                                    List<IDataParameter> arParam = new List<IDataParameter>();
                                    str = "insert into SavedReports(ReportName,ReportDescription,Username,MenuId,TableauURL,ReportParameters,DateRange,OriginalReportName,SendReportNow,UserProfile,shareby,ShareMessage) values(@ReportName,@ReportDescription, @Username,@MenuId,@TableauURL,@ReportParameters,@DateRange,@OriginalReportName,@SendReportNow,@UserProfile,@shareby,@ShareMessage)";
                                    arParam.Add(new SqlParameter("@ReportName", SharedReportName.Replace("'", "''")));
                                    arParam.Add(new SqlParameter("@ReportDescription", dsrep.Rows[0]["ReportDescription"].ToString().Replace("'", "''")));
                                    arParam.Add(new SqlParameter("@Username", ltm.Value));
                                    arParam.Add(new SqlParameter("@MenuId", dsrep.Rows[0]["MenuId"].ToString()));
                                    arParam.Add(new SqlParameter("@TableauURL", dsrep.Rows[0]["TableauURL"].ToString()));
                                    arParam.Add(new SqlParameter("@ReportParameters", dsrep.Rows[0]["ReportParameters"].ToString()));
                                    arParam.Add(new SqlParameter("@DateRange", dsrep.Rows[0]["DateRange"].ToString()));
                                    arParam.Add(new SqlParameter("@OriginalReportName", dsrep.Rows[0]["OriginalReportName"].ToString()));
                                    arParam.Add(new SqlParameter("@SendReportNow", "0"));
                                    arParam.Add(new SqlParameter("@UserProfile", UserProfile));
                                    arParam.Add(new SqlParameter("@shareby", Session["UserName"]));
                                    arParam.Add(new SqlParameter("@ShareMessage", txtMessage.Text.Replace("'", "''").Trim()));

                                    dataAccessProvider.ExecuteNonQuery(str, arParam, false);

                                    string getemail = "Select *  from SETUP_USERDATAACCESS where username='" + ltm.Value.ToString() + "' and EMAIL is not null limit 1";
                                    using (DataTable dt = dataAccessProvider.Select(getemail))
                                    {
                                        ShareCount += 1;
                                        if (dt.Rows[0]["Email"].ToString().Trim().Length > 0)
                                            SendEmail(dt.Rows[0]["Email"].ToString(), Session["UserName"].ToString(), dt.Rows[0]["Name"].ToString(), SharedReportName, txtMessage.Text.Replace("'", "''"));
                                    }

                                }

                            }
                        }
                    }

                    if (ShareCount == UserCount)
                    {
                        if (RdShareUsers.CheckedItems.Count > 1)
                            Message = "Report has been shared with all the selected users.";
                        else
                            Message = "Report has been shared with the selected user.";
                    }
                    else if (ShareCount > 0)
                        Message = "Report has been shared with " + ShareCount + " user(s) out of " + UserCount;
                    else
                        Message = "Report could not be shared with any user(s) as either Hierarchy or Store is not shared.";
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > RadDates_SelectedIndexChanged:"));
            }

            return Message;
        }
        /// <summary>
        /// btnClose click event
        /// </summary>
        protected void btnClose_click(object sender, EventArgs e)
        {
            try
            {
                //Response.Redirect("ManageSavedReports.aspx");
                MPMessage.Hide();
                grdMembers.Rebind();
                BindNotifications();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > RadDates_SelectedIndexChanged:"));
            }
        }
        /// <summary>
        /// bind notifications
        /// </summary>
        public void BindNotifications()
        {
            try
            {
                string str = "select count(*) as counter from SavedReports where username='" + this.LoginUserName + "' and ReadStatus='0' and Shareby is not null";
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    using (DataTable ds = dataAccessProvider.Select(str))
                    {
                        if (ds.Rows.Count > 0)
                        {
                            if (Convert.ToInt16(ds.Rows[0]["counter"]) > 0)
                            {

                                this.Master.FindControl("divnotification").Visible = true;
                                ((Label)Master.FindControl("lblcount")).Text = Convert.ToString(ds.Rows[0]["counter"]);
                            }
                            else
                            {
                                this.Master.FindControl("divnotification").Visible = false;
                            }
                        }
                        else
                        {
                            this.Master.FindControl("divnotification").Visible = false;
                        }
                    }

                }


            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > RadDates_SelectedIndexChanged:"));
            }
        }

        #endregion 

        #region Grid Events
        /// <summary>
        /// grid event of grdmembers
        /// </summary>
        protected void grdMembers_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            try
            {
                using (DataTable dt = GetSearchResults())
                {
                    if (dt != null)
                    {
                        if (dt.Columns.Count > 0)
                        {
                            grdMembers.DataSource = dt;
                            divSearchResult.Visible = true;
                            grdMembers.Visible = true;
                            if (dt.Rows.Count > 0)
                            {
                                btnExport.Visible = true;
                                btnZip.Visible = true;
                            }
                        }
                        else
                        {
                            divSearchResult.Visible = false;
                            grdMembers.Visible = false;
                            btnExport.Visible = false;
                            btnZip.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > RadDates_SelectedIndexChanged:"));
            }
        }
        /// <summary>
        /// Gate date name
        /// </summary>
        public string GetDatename(string DateRange)
        {
            string FinalDateRange = "";
            try
            {
                string[] StrReports = DateRange.Split('-');
                if (StrReports[0] == "W")
                {
                    FinalDateRange = "Last " + StrReports[1] + " Week(s)";
                }
                if (StrReports[0] == "M")
                {
                    FinalDateRange = "Last " + StrReports[1] + " Month(s)";
                }
                if (StrReports[0] == "Q")
                {
                    FinalDateRange = "Last " + StrReports[1] + " Quarter(s)";
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > RadDates_SelectedIndexChanged:"));
            }
            return FinalDateRange;
        }
        /// <summary>
        /// Itemcommand event of grdmembers
        /// </summary>
        protected void grdMembers_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    ViewState["EditMode"] = "1";
                    txtReportDescr.Text = "";
                    txtReportName.Text = "";

                    DrpDateVal.ClearSelection();
                    txtemail.Text = "";

                    DataTable dsSchedule = DataManager.GetReportSchedule();
                    DrpReportTime.DataSource = dsSchedule;
                    DrpReportTime.DataTextField = "ReportFrequency";
                    DrpReportTime.DataValueField = "ScheduleId";
                    DrpReportTime.DataBind();
                    string ReportID = e.CommandArgument.ToString();
                    HfReportID.Value = e.CommandArgument.ToString();
                    DataTable ds = DataManager.GetReportInfoByReportId(ReportID);
                    txtReportName.Text = ds.Rows[0]["ReportName"] + "";
                    txtReportDescr.Text = ds.Rows[0]["ReportDescription"] + "";

                    if (!String.IsNullOrEmpty(Convert.ToString(ds.Rows[0]["Emailid"])))
                    {

                        DataTable ds1 = DataManager.GetReportSchedule();
                        DrpReportTime.DataSource = ds1;
                        DrpReportTime.DataTextField = "ReportFrequency";
                        DrpReportTime.DataValueField = "ScheduleId";
                        DrpReportTime.DataBind();

                        PnlReportTime.Visible = true;
                        rdSchedule.SelectedValue = "Y";
                        if (!string.IsNullOrEmpty(Convert.ToString(ds.Rows[0]["ScheduleId"])))
                        {
                            if (DrpReportTime.Items.FindByValue(Convert.ToString(ds.Rows[0]["ScheduleId"])) != null)
                            {
                                DrpReportTime.SelectedValue = Convert.ToString(ds.Rows[0]["ScheduleId"]);
                            }

                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ds.Rows[0]["EmailID"])))
                        {
                            txtemail.Text = Convert.ToString(ds.Rows[0]["EmailID"]);
                        }
                        else
                        {
                            string strEmail = "";
                            strEmail = "Select Email from SETUP_USERDATAACCESS where isnull(IsDelete, false) = false and (Email is not null) and Email<>'' and lower(Username)=lower('" + Session["UserName"].ToString() + "') Limit 1";
                            using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                            {
                                using (DataTable dt = dataAccessProvider.Select(strEmail))
                                {
                                    if (dt.Rows.Count > 0)
                                    {
                                        if (!String.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["EMAIL"])))
                                        {
                                            txtemail.Text = dt.Rows[0]["EMAIL"].ToString();
                                        }
                                    }
                                }
                            }
                        }
                        if (ds.Rows[0]["EndDate"] != null && ds.Rows[0]["EndDate"].ToString() != "1/1/1900 12:00:00 AM" && ds.Rows[0]["EndDate"] != DBNull.Value && ds.Rows[0]["EndDate"].ToString().Length > 8)
                        {
                            txtrepdate.Text = Convert.ToDateTime(ds.Rows[0]["EndDate"]).ToString("MM/dd/yyyy");
                            CalendarExtender11.Enabled = true;
                            txtrepdate.Enabled = true;
                        }
                        else
                        {
                            txtrepdate.Text = "__/__/____";
                            CalendarExtender11.Enabled = false;
                            txtrepdate.Enabled = false;
                        }
                    }
                    else
                    {
                        rdSchedule.SelectedValue = "N";
                        txtrepdate.Text = "__/__/____";
                        CalendarExtender11.Enabled = false;
                        txtrepdate.Enabled = false;
                        PnlReportTime.Visible = false;
                        string strEmail = "";
                        strEmail = "Select Email from SETUP_USERDATAACCESS where isnull(IsDelete, false) = false and (Email is not null) and Email<>'' and lower(Username)=lower('" + Session["UserName"].ToString() + "') Limit 1";
                        using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                        {
                            using (DataTable dt = dataAccessProvider.Select(strEmail))
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["EMAIL"])))
                                    {
                                        txtemail.Text = dt.Rows[0]["EMAIL"].ToString();
                                    }
                                }
                            }
                        }
                    }

                    Session["CheckFilter"] = ds.Rows[0]["ReportParameters"].ToString();
                    string[] RepPar = ds.Rows[0]["ReportParameters"].ToString().Split('&');
                    int j = 0;
                    string fiscal = "";
                    string lastweeks = "";
                    string hidedateselection = "";
                    string startdateexist = "";
                    string enddateexist = "";
                    string ShowReportnameanddesc = "";
                    string fromadweek = "";
                    string HolidayReports = "";
                    string ShoeRelativedate = "";
                    string PnlSchdule = "";
                    string PnlVisibility = "";
                    string Newitem = "";
                    string ShowSubscription = "";
                    string startdateexistTy = "";
                    string SetWeekValue = "";
                    string HolidayWithYear = "";
                    for (j = 0; j < RepPar.Length; j++)
                    {
                        if (RepPar[j].ToString().Contains("FC%20Filter"))
                        {
                            fiscal = "1";
                        }

                        if (RepPar[j].ToString().Contains("Fiscal%20Calendar%20Week"))
                        {
                            fiscal = "1";
                        }
                        if (RepPar[j].ToString().Contains("PnlSchedule"))
                        {
                            PnlSchdule = "1";
                        }

                        if (RepPar[j].ToString().Contains("Heading"))
                        {
                            fiscal = "";
                        }
                        if (RepPar[j].ToString().Contains("Last%20Weeks"))
                        {
                            string getvalue = "";
                            string[] getdt = RepPar[j].ToString().Split('=');
                            getvalue = getdt[1];
                            lastweeks = "Last " + getdt[1] + " Weeks";
                        }
                        if (RepPar[j].ToString().Contains("Lastweek"))
                        {
                            string getvalue = "";
                            string[] getdt = RepPar[j].ToString().Split('=');
                            getvalue = getdt[1];
                            lastweeks = "Last " + getdt[1] + " Weeks";
                        }

                        if (RepPar[j].ToString().Contains("launch_date"))
                        {
                            hidedateselection = "1";
                        }
                        if (RepPar[j].ToString().Contains("Start_Date"))
                        {
                            startdateexist = "1";
                        }
                        if (RepPar[j].ToString().Contains("End_Date"))
                        {
                            enddateexist = "1";
                        }

                        if (RepPar[j].ToString().Contains("FY"))
                        {
                            string getvalue = "";
                            string[] getdt = RepPar[j].ToString().Split('=');
                            getvalue = getdt[0];
                            if (getvalue == "FY")
                            {
                                fiscal = "1";
                            }
                        }
                        if (RepPar[j].ToString().Contains("CUSTOM_PROMO_ID"))
                        {
                            ShowReportnameanddesc = "1";
                        }
                        if (RepPar[j].ToString().Contains("HideSubscription"))
                        {
                            ShowReportnameanddesc = "1";
                        }

                        if (RepPar[j].ToString().Contains("RC"))
                        {
                            string getvalue = "";
                            string[] getdt = RepPar[j].ToString().Split('=');
                            getvalue = getdt[0];
                            if (getvalue == "RC")
                            {
                                hidedateselection = "1";
                            }
                        }
                        if (RepPar[j].ToString().Contains("RCY"))
                        {
                            string getvalue = "";
                            string[] getdt = RepPar[j].ToString().Split('=');
                            getvalue = getdt[0];
                            if (getvalue == "RCY")
                            {
                                hidedateselection = "1";
                            }
                        }
                        if (RepPar[j].ToString().Contains("FromAdWeek"))
                        {
                            string getvalue = "";
                            string[] getdt = RepPar[j].ToString().Split('=');
                            getvalue = getdt[0];
                            if (getvalue == "FromAdWeek")
                            {
                                fromadweek = "1";
                            }
                        }

                        if (RepPar[j].ToString().Contains("e_id"))
                        {
                            string getvalue = "";
                            string[] getdt = RepPar[j].ToString().Split('=');
                            getvalue = getdt[0];
                            if (getvalue == "e_id")
                            {
                                ShowReportnameanddesc = "1";
                            }
                        }
                        if (RepPar[j].ToString().Contains("ddlHolidayYear"))
                        {
                            string getvalue = "";
                            string[] getdt = RepPar[j].ToString().Split('=');
                            getvalue = getdt[0];
                            HolidayReports = "1";
                        }
                        if (RepPar[j].ToString().Contains("OpporBanner_id"))
                        {
                            string getvalue = "";
                            string[] getdt = RepPar[j].ToString().Split('=');
                            getvalue = getdt[0];
                            getvalue = getdt[0];
                            if (getvalue == "OpporBanner_id")
                            {
                                ShoeRelativedate = "1";
                            }
                        }

                        if (RepPar[j].ToString().Contains("WeekSupported"))
                        {
                            string getvalue = "";
                            string[] getdt = RepPar[j].ToString().Split('=');
                            getvalue = getdt[0];
                            if (getvalue == "WeekSupported")
                            {
                                SetWeekValue = Convert.ToString(getdt[1]);
                            }
                        }

                        if (RepPar[j].ToString().Contains("SalesDriverBasketProducthierarchy"))
                        {
                            ShoeRelativedate = "1";
                            ShowReportnameanddesc = "";
                        }
                        if (RepPar[j].ToString().Contains("QuarterlyshoppersMetrics"))
                        {
                            fiscal = "1";
                            ShowReportnameanddesc = "";
                        }
                        if (RepPar[j].ToString().Contains("BusinessReviewDashboard"))
                        {
                            ShowReportnameanddesc = "";
                        }
                        if (RepPar[j].ToString().Contains("pnlvisibile"))
                        {
                            PnlVisibility = "1";
                        }
                        if (RepPar[j].ToString().Contains("DiscoDate"))
                        {
                            Newitem = "1";
                        }
                        if (RepPar[j].ToString().Contains("NoSubscribe"))
                        {
                            ShowSubscription = "1";
                        }
                        if (RepPar[j].ToString().Contains("SDateTY"))
                        {
                            startdateexistTy = "1";
                        }

                        if (RepPar[j].ToString().Contains("HolidayWithYear"))
                        {
                            HolidayWithYear = "1";
                        }
                    }

                    if (!String.IsNullOrEmpty(fiscal))
                    {
                        pnldates.Visible = false;
                        Pnlschedule.Visible = true;
                        pnlscheduleoption.Visible = true;
                    }

                    if (!String.IsNullOrEmpty(lastweeks))
                    {
                        pnldates.Visible = false;
                        Pnlschedule.Visible = true;
                        pnlscheduleoption.Visible = true;
                    }

                    if (String.IsNullOrEmpty(fiscal) && String.IsNullOrEmpty(lastweeks))
                    {
                        pnldates.Visible = true;
                        Pnlschedule.Visible = true;
                        pnlscheduleoption.Visible = true;
                    }

                    if (!String.IsNullOrEmpty(hidedateselection))
                    {
                        Pnlschedule.Visible = false;
                        pnlscheduleoption.Visible = true;
                    }

                    if (String.IsNullOrEmpty(startdateexist) && !String.IsNullOrEmpty(enddateexist))
                    {
                        Pnlschedule.Visible = false;
                        pnlscheduleoption.Visible = true;
                    }

                    if (!String.IsNullOrEmpty(startdateexist) && String.IsNullOrEmpty(enddateexist))
                    {
                        Pnlschedule.Visible = false;
                        pnlscheduleoption.Visible = true;
                    }

                    if (String.IsNullOrEmpty(fiscal) && String.IsNullOrEmpty(lastweeks) && String.IsNullOrEmpty(hidedateselection) && String.IsNullOrEmpty(startdateexist) && String.IsNullOrEmpty(enddateexist) && String.IsNullOrEmpty(ShowReportnameanddesc))
                    {
                        Pnlschedule.Visible = false;
                        pnlscheduleoption.Visible = true;
                    }
                    if (!String.IsNullOrEmpty(fromadweek))
                    {
                        pnldates.Visible = true;
                        Pnlschedule.Visible = true;
                        pnlscheduleoption.Visible = true;
                    }

                    if (ShowReportnameanddesc == "1")
                    {
                        Pnlschedule.Visible = false;
                        pnlscheduleoption.Visible = false;
                    }
                    if (ShoeRelativedate == "1")
                    {
                        Pnlschedule.Visible = true;
                        pnlscheduleoption.Visible = true;
                    }
                    if (!String.IsNullOrEmpty(HolidayReports))
                    {
                        pnldates.Visible = false;
                        Pnlschedule.Visible = true;
                        pnlscheduleoption.Visible = true;
                    }
                    if (!String.IsNullOrEmpty(PnlVisibility))
                    {
                        pnldates.Visible = true;
                        Pnlschedule.Visible = true;
                        pnlscheduleoption.Visible = true;
                    }

                    if (!String.IsNullOrEmpty(Newitem))
                    {
                        pnldates.Visible = false;
                        Pnlschedule.Visible = false;
                        pnlscheduleoption.Visible = true;
                    }
                    if (!String.IsNullOrEmpty(PnlSchdule))
                    {
                        pnlscheduleoption.Visible = false;
                    }
                    if (!String.IsNullOrEmpty(ShowSubscription))
                    {
                        Pnlschedule.Visible = true;
                        pnlscheduleoption.Visible = false;
                    }
                    if (!String.IsNullOrEmpty(startdateexistTy))
                    {
                        pnldates.Visible = true;
                        Pnlschedule.Visible = true;
                        pnlscheduleoption.Visible = true;
                    }
                    if (!String.IsNullOrEmpty(HolidayWithYear))
                    {
                        pnldates.Visible = false;
                        Pnlschedule.Visible = false;
                        pnlscheduleoption.Visible = true;
                    }


                    if (!String.IsNullOrEmpty(SetWeekValue))
                        ViewState["WeekValue"] = Convert.ToInt16(SetWeekValue);

                    if (!String.IsNullOrEmpty(Convert.ToString(ds.Rows[0]["DateRange"])))
                    {
                        pnldates.Visible = true;
                        DrpDateVal.Enabled = true;
                        string[] StrReports = ds.Rows[0]["DateRange"].ToString().Split('-');
                        int i = 1;
                        using (DataTable dt = new DataTable())
                        {
                            dt.Columns.Add("TextName");
                            dt.Columns.Add("TextValue");
                            if (StrReports[0] == "W")
                            {
                                for (i = 1; i <= Convert.ToInt16(ViewState["WeekValue"]); i++)
                                {
                                    DataRow dr = dt.NewRow();
                                    dr["TextName"] = i;
                                    dr["TextValue"] = i;
                                    dt.Rows.Add(dr);
                                }
                            }

                            DrpDateVal.DataSource = dt;
                            DrpDateVal.DataTextField = "TextName";
                            DrpDateVal.DataValueField = "TextValue";
                            DrpDateVal.DataBind();
                            DrpDateVal.SelectedValue = StrReports[1];
                        }
                    }
                    else
                    {
                        int i = 1;
                        using (DataTable dt = new DataTable())
                        {
                            dt.Columns.Add("TextName");
                            dt.Columns.Add("TextValue");
                            for (i = 1; i <= Convert.ToInt16(ViewState["WeekValue"]); i++)
                            {
                                DataRow dr = dt.NewRow();
                                dr["TextName"] = i;
                                dr["TextValue"] = i;
                                dt.Rows.Add(dr);
                            }
                            DrpDateVal.DataSource = dt;
                            DrpDateVal.DataTextField = "TextName";
                            DrpDateVal.DataValueField = "TextValue";
                            DrpDateVal.DataBind();
                            DrpDateVal.Enabled = true;
                        }
                    }

                    upRequest.Update();
                    mpeRequest.Show();
                }
                if (e.CommandName == "Delete")
                {
                    int ReportID = Convert.ToInt32(e.CommandArgument);
                    DataManager.DeleteReports(ReportID);
                    divSearchResult.Visible = true;
                    grdMembers.Visible = true;
                    grdMembers.Rebind();
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Confirmation Message", "alert('Report has been deleted successfully'); window.location ='ManageSavedReports.aspx';", true);
                }

                if (e.CommandName == "ViewMsg")
                {
                    DataTable ds = DataManager.GetReportInfoByReportId(e.CommandArgument.ToString());

                    lblmessage.Text = ds.Rows[0]["ShareMessage"] + "";
                    if (ds.Rows[0]["ReadStatus"].ToString() == "0")
                    {
                        using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                        {
                            string sql = "update SavedReports set ReadStatus=1 where reportid=" + e.CommandArgument;
                            dataAccessProvider.ExecuteNonQuery(sql);
                        }
                    }
                    hdnReportId.Value = e.CommandArgument.ToString();
                    MPMessage.Show();
                }

                if (e.CommandName == "ShareRep")
                {
                    int i = 0;
                    string Hierarchy = "";
                    string Store = "";
                    bool Combo = false;
                    string[] RepPar;
                    using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                    {
                        string str = "select * from SavedReports where reportid=" + e.CommandArgument;
                        using (DataTable dt = dataAccessProvider.Select(str))
                        {
                            txtSharedReportName.Text = dt.Rows[0]["ReportName"] + "";
                            HfShare.Value = Convert.ToString(e.CommandArgument);
                            txtMessage.Text = "";
                            RdShareUsers.ClearCheckedItems();
                            rdShareHierarchy.SelectedIndex = -1;
                            rdShareStore.SelectedIndex = -1;
                            rdComboStoreHierarchy.SelectedIndex = -1;
                            grdMembers.Rebind();
                            RepPar = dt.Rows[0]["ReportParameters"].ToString().Split('&');
                        }

                    }

                    for (i = 0; i < RepPar.Length; i++)
                    {
                        if (RepPar[i].ToString().Contains("SharedHierarchy"))
                        {
                            Hierarchy = "1";
                            HfShareHierarchy.Value = "1";
                        }
                        if (RepPar[i].ToString().Contains("PnlCustomStore"))
                        {
                            Store = "1";
                            HfShareHierarchy.Value = "0";
                        }
                        if (RepPar[i].ToString().Contains("share_Combo"))
                        {
                            Combo = true;
                            HfShareHierarchy.Value = "2";
                        }
                    }
                    if (Hierarchy == "1")
                    {
                        PnlHierarchy.Visible = true;
                    }
                    else
                    {
                        PnlHierarchy.Visible = false;

                    }
                    if (Store == "1")
                    {
                        PnlCustomStore.Visible = true;
                    }
                    else
                    {
                        PnlCustomStore.Visible = false;
                    }

                    if (Combo)
                        PnlComboBoth.Visible = true;
                    else
                        PnlComboBoth.Visible = false;

                    mpeShare.Show();
                }

                if (e.CommandName == "ViewRep")
                {
                    try
                    {


                        using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                        {
                            string str = "select * from webmenus a , SavedReports b where a.MenuId=b.MenuId and Reportid=" + e.CommandArgument;
                            using (DataTable ds = dataAccessProvider.Select(str))
                            {
                                if (ds.Rows.Count > 0)
                                {
                                    string OldStartDate = "";
                                    string OldEndDate = "";
                                    string NewStartDate = "Start_Date=";
                                    string NewEndDate = "End_Date=";
                                    string OldStartDateTY = "";
                                    string OldEndDateLY = "";
                                    string NewStartDateTY = "Start_Date%20TY=";
                                    string NewEndDateLY = "End_Date%20TY=";
                                    Session["DateRange"] = ds.Rows[0]["DateRange"].ToString();
                                    string[] DateFilters;
                                    string[] RepPar = ds.Rows[0]["ReportParameters"].ToString().Split('&');
                                    Session["CheckBusinessUserProfile"] = ds.Rows[0]["ReportParameters"].ToString();
                                    if (!String.IsNullOrEmpty(Convert.ToString(ds.Rows[0]["DateRange"])))
                                    {
                                        DateFilters = ds.Rows[0]["DateRange"].ToString().Split('-');
                                        if (DateFilters[0].ToString() == "W")
                                        {
                                            NewStartDate += AppManager.StartDateForSaveFilterForWeek(Convert.ToInt16(DateFilters[1]) * 7).ToString("yyyy-MM-dd");
                                            NewEndDate += AppManager.EndDate().ToString("yyyy-MM-dd");
                                            NewStartDateTY += AppManager.StartDateForSaveFilterForWeek(Convert.ToInt16(DateFilters[1]) * 7).ToString("yyyy-MM-dd");
                                            NewEndDateLY += AppManager.EndDate().ToString("yyyy-MM-dd");
                                        }
                                        int i = 0;
                                        for (i = 0; i < RepPar.Length; i++)
                                        {
                                            if (RepPar[i].ToString().Contains("Start_Date"))
                                            {
                                                OldStartDate = RepPar[i].ToString();
                                            }

                                            if (RepPar[i].ToString().Contains("Start_Date%20TY"))
                                            {
                                                OldStartDateTY = RepPar[i].ToString();
                                                ViewState["StartDateTY"] = "1";
                                            }

                                            if (RepPar[i].ToString().Contains("End_Date"))
                                            {
                                                ViewState["ForStartEndDate"] = "1";
                                                OldEndDate = RepPar[i].ToString();
                                            }
                                            if (RepPar[i].ToString() == "End_Date%20TY")
                                            {
                                                OldEndDateLY = RepPar[i].ToString();
                                            }
                                        }
                                        string FInalRepParam = "";
                                        if (ViewState["ForStartEndDate"] == null)
                                            FInalRepParam = ds.Rows[0]["ReportParameters"].ToString();
                                        else
                                        {
                                            if (Convert.ToString(ViewState["StartDateTY"]) != "1")
                                            {
                                                if (!String.IsNullOrEmpty(OldStartDate))
                                                {
                                                    FInalRepParam = ds.Rows[0]["ReportParameters"].ToString().Replace(OldStartDate, NewStartDate);
                                                }
                                                if (!String.IsNullOrEmpty(OldEndDate) && !String.IsNullOrEmpty(OldStartDate))
                                                {
                                                    FInalRepParam = ds.Rows[0]["ReportParameters"].ToString().Replace(OldEndDate, NewEndDate);
                                                }
                                                if (!String.IsNullOrEmpty(OldEndDate) && !String.IsNullOrEmpty(OldStartDate))
                                                {
                                                    FInalRepParam = FInalRepParam.Replace(OldEndDate, NewEndDate);
                                                }
                                            }
                                            else
                                            {
                                                if (!String.IsNullOrEmpty(OldStartDateTY))
                                                {
                                                    FInalRepParam = ds.Rows[0]["ReportParameters"].ToString().Replace(OldStartDateTY, NewStartDateTY);
                                                }
                                                if (!String.IsNullOrEmpty(OldEndDateLY) && !String.IsNullOrEmpty(OldStartDateTY))
                                                {
                                                    FInalRepParam = ds.Rows[0]["ReportParameters"].ToString().Replace(OldEndDateLY, NewEndDateLY);
                                                }
                                                if (!String.IsNullOrEmpty(OldEndDateLY) && !String.IsNullOrEmpty(OldStartDateTY))
                                                {
                                                    FInalRepParam = FInalRepParam.Replace(OldEndDateLY, NewEndDateLY);
                                                }
                                            }
                                        }

                                        Session["RepParam"] = FInalRepParam;
                                    }
                                    else
                                    {
                                        Session["RepParam"] = ds.Rows[0]["ReportParameters"].ToString();
                                    }

                                    Response.Redirect("/" + ds.Rows[0]["SupplierPageUrl"].ToString() + "?vid=" + ds.Rows[0]["VerticalID"].ToString() + "&mid=" + ds.Rows[0]["MenuId"].ToString() + "&repid=" + e.CommandArgument + "&RepType=S");
                                }
                            }

                        }


                    }
                    catch (Exception ex)
                    {
                        helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > RadDates_SelectedIndexChanged:"));
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > RadDates_SelectedIndexChanged:"));
            }
        }
        /// <summary>
        /// ItemDataBound event of grdmembers
        /// </summary>
        protected void grdMembers_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            //Is it a GridDataItem
            if (e.Item is GridDataItem)
            {
                try
                {
                    HiddenField hfparameters = e.Item.FindControl("hfparameters") as HiddenField;
                    HiddenField HFMessage = e.Item.FindControl("hfmsg") as HiddenField;
                    HiddenField HfShareBy = e.Item.FindControl("hfsharedby") as HiddenField;
                    HiddenField hfreadstatus = e.Item.FindControl("HfReadStatus") as HiddenField;
                    HiddenField hdnScheduleId = e.Item.FindControl("hdnScheduleId") as HiddenField;
                    Image imgsubscribe = e.Item.FindControl("imgsubscribe") as Image;
                    HiddenField hdnDeleteStatus = e.Item.FindControl("hdnDeleteStatus") as HiddenField;
                    Label LblEmail = e.Item.FindControl("LblEmail") as Label;

                    /*wrapping email ids, new emailid in next line*/
                    if (LblEmail.Text.Contains(","))
                    {
                        LblEmail.Text = LblEmail.Text.Replace(",", ",<br/>");
                    }

                    LinkButton BtnMsg = e.Item.FindControl("btnmessage") as LinkButton;
                    LinkButton Lnkshare = e.Item.FindControl("Lnkshare") as LinkButton;
                    ImageButton img = e.Item.FindControl("imgshare") as ImageButton;
                    img.Enabled = false;
                    if (!String.IsNullOrEmpty(HFMessage.Value))
                    {
                        BtnMsg.Visible = true;
                        if (hfreadstatus.Value == "0")
                            BtnMsg.Controls.Add(new Image { ImageUrl = "~/Images/message.png" });
                        else
                            BtnMsg.Controls.Add(new Image { ImageUrl = "~/Images/ReadMessage.png" });
                    }
                    else
                    {
                        BtnMsg.Visible = false;
                    }
                    if (hdnDeleteStatus.Value == "True")
                    {
                        BtnMsg.Visible = false;
                    }
                    else if (hdnDeleteStatus.Value == "False")
                    {
                        BtnMsg.Visible = true;
                    }
                    if (hdnScheduleId.Value == "0" || hdnScheduleId.Value == null || String.IsNullOrEmpty(hdnScheduleId.Value))
                    {
                        imgsubscribe.Visible = false;
                    }
                    else
                    {
                        imgsubscribe.Visible = true;
                    }


                    if (!String.IsNullOrEmpty(HfShareBy.Value))
                        img.ImageUrl = "../Images/Sharebg.png";
                    else
                        img.ImageUrl = "../Images/blank.png";


                    string[] RepPar = hfparameters.Value.ToString().Split('&');
                    int j = 0;

                    string ShowReportnameanddesc = "";
                    string ShowSubscription = string.Empty;
                    for (j = 0; j < RepPar.Length; j++)
                    {
                        if (RepPar[j].ToString().Contains("CUSTOM_PROMO_ID"))
                        {
                            ShowReportnameanddesc = "1";
                            ShowSubscription = "1";
                        }
                        if (RepPar[j].ToString().Contains("ch_id"))
                        {
                            string[] RepPar1 = RepPar[j].ToString().Split('=');
                            if (RepPar1[0].ToString() == "ch_id")
                            {
                                ShowReportnameanddesc = "1";
                            }
                        }
                        // For Custom Hierarchy reports
                        if (RepPar[j].ToString().Contains("CUSTOM_HIERARCHY_ID"))
                        {
                            ShowReportnameanddesc = "1";
                        }
                        if (RepPar[j].ToString().Contains("launch_date"))
                        {
                            ShowReportnameanddesc = "1";
                        }
                        if (RepPar[j].ToString().Contains("Share"))
                        {
                            ShowReportnameanddesc = "";
                        }
                        if (RepPar[j].ToString().Contains("NoSubscribe"))
                        {
                            ShowSubscription = "1";
                        }
                    }

                    if (ShowReportnameanddesc == "1")
                    {
                        Lnkshare.Visible = false;
                    }
                    else
                        Lnkshare.Visible = true;


                    if (ShowSubscription == "1")
                    {
                        Lnkshare.Visible = false;
                    }
                    else
                    {
                        Lnkshare.Visible = true;
                    }

                }
                catch (Exception ex)
                {
                    helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > grdMembers_ItemDataBound:"));

                }
            }
        }
       
        #endregion

        #region DropDown Selected Index Changed

        public void ChangeRelativedate()
        {
            try
            {
                string[] RepPar = Session["CheckFilter"].ToString().Split('&');
                int j = 0;
                string fiscal = "";
                string lastweeks = "";
                string hidedateselection = "";
                string startdateexist = "";
                string enddateexist = "";
                string ShowReportnameanddesc = "";
                string fromadweek = "";
                string HolidayReports = "";
                string ShoeRelativedate = "";
                string PnlVisibility = "";
                string Newitem = "";
                string HolidayWithYear = "";
                string startdateexistTy = "";
                for (j = 0; j < RepPar.Length; j++)
                {
                    if (RepPar[j].ToString().Contains("FC%20Filter"))
                    {
                        fiscal = "1";
                    }
                    if (RepPar[j].ToString().Contains("Fiscal%20Calendar%20Week"))
                    {
                        fiscal = "1";
                    }
                    if (RepPar[j].ToString().Contains("Heading"))
                    {
                        fiscal = "";
                    }
                    if (RepPar[j].ToString().Contains("Last%20Weeks"))
                    {
                        string getvalue = "";
                        string[] getdt = RepPar[j].ToString().Split('=');
                        getvalue = getdt[1];
                        lastweeks = "Last " + getdt[1] + " Weeks";
                    }

                    if (RepPar[j].ToString().Contains("Lastweek"))
                    {
                        string getvalue = "";
                        string[] getdt = RepPar[j].ToString().Split('=');
                        getvalue = getdt[1];
                        lastweeks = "Last " + getdt[1] + " Weeks";
                    }

                    if (RepPar[j].ToString().Contains("launch_date"))
                    {
                        hidedateselection = "1";
                    }
                    if (RepPar[j].ToString().Contains("Start_Date"))
                    {
                        startdateexist = "1";
                    }
                    if (RepPar[j].ToString().Contains("End_Date"))
                    {
                        enddateexist = "1";
                    }
                    if (RepPar[j].ToString().Contains("pnlvisibile"))
                    {
                        PnlVisibility = "1";
                    }

                    if (RepPar[j].ToString().Contains("FY"))
                    {
                        string getvalue = "";
                        string[] getdt = RepPar[j].ToString().Split('=');
                        getvalue = getdt[0];
                        if (getvalue == "FY")
                        {
                            fiscal = "1";
                        }
                    }

                    if (RepPar[j].ToString().Contains("RC"))
                    {
                        string getvalue = "";
                        string[] getdt = RepPar[j].ToString().Split('=');
                        getvalue = getdt[0];
                        if (getvalue == "RC")
                        {
                            hidedateselection = "1";
                        }
                    }
                    if (RepPar[j].ToString().Contains("RCY"))
                    {
                        string getvalue = "";
                        string[] getdt = RepPar[j].ToString().Split('=');
                        getvalue = getdt[0];
                        if (getvalue == "RCY")
                        {
                            hidedateselection = "1";
                        }
                    }
                    if (RepPar[j].ToString().Contains("CUSTOM_PROMO_ID"))
                    {
                        ShowReportnameanddesc = "1";
                    }
                    if (RepPar[j].ToString().Contains("HideSubscription"))
                    {
                        ShowReportnameanddesc = "1";
                    }

                    if (RepPar[j].ToString().Contains("FromAdWeek"))
                    {
                        string getvalue = "";
                        string[] getdt = RepPar[j].ToString().Split('=');
                        getvalue = getdt[0];
                        if (getvalue == "FromAdWeek")
                        {
                            fromadweek = "1";
                        }
                    }
                    if (RepPar[j].ToString().Contains("e_id"))
                    {
                        string getvalue = "";
                        string[] getdt = RepPar[j].ToString().Split('=');
                        getvalue = getdt[0];
                        if (getvalue == "e_id")
                        {
                            ShowReportnameanddesc = "1";
                        }
                    }
                    if (RepPar[j].ToString().Contains("ddlHolidayYear"))
                    {
                        string getvalue = "";
                        string[] getdt = RepPar[j].ToString().Split('=');
                        getvalue = getdt[0];
                        HolidayReports = "1";
                    }
                    if (RepPar[j].ToString().Contains("OpporBanner_id"))
                    {
                        string getvalue = "";
                        string[] getdt = RepPar[j].ToString().Split('=');
                        getvalue = getdt[0];
                        getvalue = getdt[0];
                        if (getvalue == "OpporBanner_id")
                        {
                            ShoeRelativedate = "1";
                        }
                    }
                    if (RepPar[j].ToString().Contains("SalesDriverBasketProducthierarchy"))
                    {
                        ShoeRelativedate = "1";
                        ShowReportnameanddesc = "";
                    }
                    if (RepPar[j].ToString().Contains("QuarterlyshoppersMetrics"))
                    {
                        fiscal = "1";
                        ShowReportnameanddesc = "";
                    }
                    if (RepPar[j].ToString().Contains("BusinessReviewDashboard"))
                    {
                        ShowReportnameanddesc = "";
                    }
                    if (RepPar[j].ToString().Contains("DiscoDate"))
                    {
                        Newitem = "1";
                    }

                    if (RepPar[j].ToString().Contains("SDateTY"))
                    {
                        startdateexistTy = "1";
                    }
                    if (RepPar[j].ToString().Contains("HolidayWithYear"))
                    {
                        HolidayWithYear = "1";
                    }


                }

                if (!String.IsNullOrEmpty(fiscal))
                {
                    pnldates.Visible = false;
                    //RadDates.Visible = true;
                    //RadDates.Items[1].Attributes.Add("class", "hidden");
                    //lblweeks.Visible = false;
                    Pnlschedule.Visible = true;
                    pnlscheduleoption.Visible = true;
                }

                if (!String.IsNullOrEmpty(lastweeks))
                {
                    pnldates.Visible = false;
                    //RadDates.Visible = false;
                    //lblweeks.Visible = true;
                    //lblweeks.Text = lastweeks;
                    Pnlschedule.Visible = true;
                    pnlscheduleoption.Visible = true;
                }

                if (String.IsNullOrEmpty(fiscal) && String.IsNullOrEmpty(lastweeks))
                {
                    //RadDates.Items[1].Attributes.Add("class", "display");
                    //RadDates.Visible = true;
                    //lblweeks.Visible = false;
                    pnldates.Visible = true;
                    Pnlschedule.Visible = true;
                    pnlscheduleoption.Visible = true;
                }

                if (!String.IsNullOrEmpty(hidedateselection))
                {
                    Pnlschedule.Visible = false;
                    pnlscheduleoption.Visible = true;
                }

                if (String.IsNullOrEmpty(startdateexist) && !String.IsNullOrEmpty(enddateexist))
                {
                    Pnlschedule.Visible = false;
                    pnlscheduleoption.Visible = true;
                }
                if (!String.IsNullOrEmpty(startdateexist) && !String.IsNullOrEmpty(enddateexist))
                {
                    // RadDates.Items[1].Attributes.Add("class", "display");
                }
                if (!String.IsNullOrEmpty(startdateexist) && String.IsNullOrEmpty(enddateexist))
                {
                    Pnlschedule.Visible = false;
                    pnlscheduleoption.Visible = true;
                }

                if (String.IsNullOrEmpty(fiscal) && String.IsNullOrEmpty(lastweeks) && String.IsNullOrEmpty(hidedateselection) && String.IsNullOrEmpty(startdateexist) && String.IsNullOrEmpty(enddateexist) && String.IsNullOrEmpty(ShowReportnameanddesc) && String.IsNullOrEmpty(fromadweek))
                {
                    Pnlschedule.Visible = false;
                    pnlscheduleoption.Visible = true;
                }
                if (!String.IsNullOrEmpty(fromadweek))
                {
                    //RadDates.Items[1].Attributes.Add("class", "display");
                    //RadDates.Visible = true;
                    //lblweeks.Visible = false;
                    pnldates.Visible = true;
                    Pnlschedule.Visible = true;
                    pnlscheduleoption.Visible = true;
                }
                if (ShowReportnameanddesc == "1")
                {
                    Pnlschedule.Visible = false;
                    pnlscheduleoption.Visible = false;
                }
                if (ShoeRelativedate == "1")
                {
                    Pnlschedule.Visible = true;
                    pnlscheduleoption.Visible = true;
                }
                if (!String.IsNullOrEmpty(HolidayReports))
                {
                    pnldates.Visible = false;
                    //RadDates.Visible = true;
                    //RadDates.Items[1].Attributes.Add("class", "hidden");
                    //lblweeks.Visible = false;
                    Pnlschedule.Visible = true;
                    pnlscheduleoption.Visible = true;
                }

                if (!String.IsNullOrEmpty(PnlVisibility))
                {
                    //RadDates.Items[1].Attributes.Add("class", "display");
                    //RadDates.Visible = true;
                    //lblweeks.Visible = false;
                    pnldates.Visible = true;
                    Pnlschedule.Visible = true;
                    pnlscheduleoption.Visible = true;
                }
                if (!String.IsNullOrEmpty(Newitem))
                {
                    //    RadDates.Items[1].Attributes.Add("class", "hidden");
                    //    RadDates.Visible = false;
                    //    lblweeks.Visible = false;
                    pnldates.Visible = false;
                    Pnlschedule.Visible = false;
                    pnlscheduleoption.Visible = true;
                }
                if (!String.IsNullOrEmpty(startdateexistTy))
                {
                    //RadDates.Items[1].Attributes.Add("class", "display");
                    //RadDates.Visible = true;
                    //lblweeks.Visible = false;
                    pnldates.Visible = true;
                    Pnlschedule.Visible = true;
                    pnlscheduleoption.Visible = true;
                }
                if (!String.IsNullOrEmpty(HolidayWithYear))
                {
                    //RadDates.Items[1].Attributes.Add("class", "hidden");
                    //RadDates.Visible = false;
                    //lblweeks.Visible = false;
                    pnldates.Visible = false;
                    Pnlschedule.Visible = false;
                    pnlscheduleoption.Visible = true;
                }
                upRequest.Update();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > ChangeRelativedate:"));
            }

        }

        /// <summary>
        /// radcombo rdSchedule SelectedIndexChanged event
        /// </summary>
        protected void rdSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdSchedule.SelectedValue == "Y")
                {
                    if (ViewState["EditMode"] == null)
                    {
                        PnlReportTime.Visible = true;
                        // chk.Checked = false;
                        txtrepdate.Text = "__/__/____";
                        CalendarExtender11.Enabled = false;
                        txtrepdate.Enabled = false;
                        DataTable ds = DataManager.GetReportSchedule();
                        DrpReportTime.DataSource = ds;
                        DrpReportTime.DataTextField = "ReportFrequency";
                        DrpReportTime.DataValueField = "ScheduleId";
                        DrpReportTime.DataBind();
                    }
                    else
                    {
                        PnlReportTime.Visible = true;
                        txtrepdate.Text = DateTime.UtcNow.AddDays(7 * 52).ToString("MM/dd/yyyy");
                        CalendarExtender11.Enabled = true;
                        txtrepdate.Enabled = true;
                    }
                }
                else
                {
                    if (txtrepdate.Text == "__/__/____")
                    {
                        if (ViewState["EditMode"] != null)
                        {
                            // chk.Checked = false;
                            txtrepdate.Text = "__/__/____";
                            CalendarExtender11.Enabled = false;
                            txtrepdate.Enabled = false;
                        }
                    }
                    else
                        //  chk.Checked = true;

                        PnlReportTime.Visible = false;
                    //chk.Checked = false;
                    txtrepdate.Text = "__/__/____";
                    txtrepdate.Enabled = false;
                    CalendarExtender11.Enabled = false;
                }

                ChangeRelativedate();
                mpeRequest.Show();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > rdSchedule_SelectedIndexChanged:"));
            }
        }
        /// <summary>
        /// radcombo DrpDate SelectedIndexChanged event
        /// </summary>
        protected void DrpDate__SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int i = 1;
                using (DataTable dt = new DataTable())
                {
                    dt.Columns.Add("TextName");
                    dt.Columns.Add("TextValue");
                    for (i = 1; i <= Convert.ToInt16(ViewState["WeekValue"]); i++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["TextName"] = i;
                        dr["TextValue"] = i;
                        dt.Rows.Add(dr);
                    }
                    mpeRequest.Show();
                    DrpDateVal.DataSource = dt;
                    DrpDateVal.DataTextField = "TextName";
                    DrpDateVal.DataValueField = "TextValue";
                    DrpDateVal.DataBind();
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > DrpDate__SelectedIndexChanged:"));
            }
        }
        #endregion

        #region #region Get Data & Export Method's
        /// <summary>
        /// Fill share users
        /// </summary>
        protected void FillShareUsers()
        {
            try
            {
                DataTable dt;
                // string str = "Select distinct UserName,(Name || ' (' || UserName || ')') As Name from SETUP_USERDATAACCESS where isnull(IsDelete, false) = false and lower(username) <> lower('" + Session["UserName"].ToString() + "')  order by Name";
                string str = "";
                if (DataManager.IsCorporateUser(HttpContext.Current.Session["UserName"].ToString()))
                {
                    str = "Select distinct UserName, (Name || ' (' || UserName || ')') As Name from SETUP_USERDATAACCESS where isnull(IsDelete, false) = false and lower(username) <> lower('" + HttpContext.Current.Session["UserName"].ToString() + "') and role='Corporate' order by Name";
                }
                else
                {
                    str = "Select distinct UserName, (Name || ' (' || UserName || ')') As Name from SETUP_USERDATAACCESS where isnull(IsDelete, false) = false and lower(username) <> lower('" + HttpContext.Current.Session["UserName"].ToString() + "') and role<>'Corporate' order by Name";
                }


                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    dt = dataAccessProvider.Select(str);

                }
                RdShareUsers.Items.Clear();
                RdShareUsers.DataSource = dt;
                RdShareUsers.DataTextField = "NAME";
                RdShareUsers.DataValueField = "USERNAME";
                RdShareUsers.DataBind();

                dt.Dispose();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > FillShareUsers:"));
            }

        }
        /// <summary>
        /// Get search result
        /// </summary>
        public DataTable GetSearchResults()
        {

            DataTable dtResult = new DataTable();
            try
            {
                string str = " select a.* , replace(replace(replace(a.ReportName,' ','<>'),'><',''),'<>',' ') as Report_Name , ";
                str += " w.MenuDescription, (select ReportFrequency from SavedReportsSchedule where Scheduleid=a.scheduleid ) ReportFrequency  from SavedReports a inner join webmenus w on a.MenuId=w.MenuId Where 1=1 ";


                str += " and lower(a.USERNAME)=lower('" + Session["UserName"].ToString() + "')";

                if (!String.IsNullOrEmpty(txtSearchText.Text))
                {
                    str = str + " and lower((replace(replace(replace(a.ReportName,' ','<>'),'><',''),'<>',' '))) like lower('%" + txtSearchText.Text.Trim().ToString() + "%')";

                }

                str += " and isnull(IsActive,1) = 1 Order by ReportName";

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dtResult = dataAccessProvider.Select(str);

                }
                if (dtResult.Rows.Count > 0)
                {
                    btnExport.Visible = true;
                    btnZip.Visible = true;
                }
                else
                {
                    btnExport.Visible = false;
                    btnZip.Visible = false;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Timeout expired"))
                    MessageBox.Show("Timeout expired. Please try to Search using some different criteria");
                else
                    helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > GetSearchResults:"));
            }
            return dtResult;
        }
        /// <summary>
        /// Get date format
        /// </summary>
        public string GetDateFormat(string Enddate)
        {
            string ReturnEndDate = "";
            if (!String.IsNullOrEmpty(Enddate) && Enddate != "1/1/1900 12:00:00 AM")
            {
                ReturnEndDate = Convert.ToDateTime(Enddate).ToString("MM/dd/yyyy");
            }
            return ReturnEndDate;
        }
        /// <summary>
        /// Export file
        /// </summary>
        /// <param name="strExportType">Export type</param>
        protected void ExportFile(string strExportType)
        {
            try
            {

                string filePath = Server.MapPath("~") + "\\Exports\\";
                string strfileName = "SavedReports_" + Session["AccessValues"] + "_" + DateTime.Now.ToString("MMddyyHHmmss");
                filePath = AppManager.ExportToCSV(GetSearchResults(), filePath, strfileName + ".csv", true, strExportType);
                var fileInfo = new System.IO.FileInfo(filePath);
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", strfileName + "." + strExportType));
                Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                Response.WriteFile(filePath);
                Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > ExportFile:"));
            }
        }

        #endregion

        #region SendEmail
        /// <summary>
        /// send Email
        /// </summary>
        /// <param name="ToAddress"></param>
        /// <param name="SharedBy"></param>
        /// <param name="ShareTo"></param>
        /// <param name="ReportName"></param>
        /// <param name="ReportMessage"></param>
        public void SendEmail(string ToAddress, string SharedBy, string ShareTo, string ReportName, string ReportMessage)
        {
            try
            {
                String _EmailSubject = SharedBy + " has shared a report with you.";
                string sbMessage = "<body>  <br/><br/> ";
                sbMessage += " <br/>";
                sbMessage += "<table>";

                sbMessage += "<tr>";
                sbMessage += "<td>Dear " + ShareTo + ",</td><td></td>";
                sbMessage += "</tr>";
                sbMessage += "<tr style='padding-top:20px;'>";
                sbMessage += "<td colspan='2'>" + SharedBy + " has shared a report with you with the following details.</td>";
                sbMessage += "</tr>";
                sbMessage += "<tr style='padding-top:10px;'><td>Report Name:</td><td> " + ReportName + "</td></tr>";
                sbMessage += "<tr><td>Message:</td><td>" + ReportMessage + "</td></tr>";
                sbMessage += "<tr style='padding-top:20px;'>";
                sbMessage += "<td> Thanks</td><td></td>";
                sbMessage += "</tr>";
                sbMessage += "<tr>";
                sbMessage += "<td>" + SharedBy + "</td><td></td>";
                sbMessage += "</tr>";
                sbMessage += "</table> </body>";
                AppManager.SendEmail(sbMessage.ToString(), _EmailSubject, EmailFrom, ToAddress, "");

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ManageSavedReports.aspx.cs > SendEmail:"));
            }
        }
        #endregion
    }
}