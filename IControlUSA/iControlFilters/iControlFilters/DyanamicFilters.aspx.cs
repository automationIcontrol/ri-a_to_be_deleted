﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using iControlGenricFilters.ResourceFiles;
using iControlDataLayer;
using AjaxControlToolkit;
using Telerik.Web.UI;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Script.Serialization;
using iControlGenricFilters.LIB;
using System.Runtime.InteropServices;
using System.Configuration;
using iControlGenricFilters.Controls;

namespace iControlGenricFilters
{
    public partial class DyanamicFilters : System.Web.UI.Page
    {
       
        #region Page Level Properties
        IControlHelper helper = new IControlHelper();

        //Get report Id from Query string
        private int ReportId
        {
            get
            {
                if (!String.IsNullOrEmpty(Request.QueryString["mid"]))
                {
                    return Convert.ToInt16(Request.QueryString["mid"]);
                }
                else
                {
                    return 0;
                }

            }
        }

        //Get report Name
        private string ReportName
        {
            get
            {
                if (ReportId > 0)
                {
                    DataTable dtFilters = dataAccessLayer.GetFiltersDataTable(ReportId);
                    string reportName = dtFilters.Rows[0]["REPORT_NAME"].ToString();
                    return reportName.Replace(" ", "").Replace("-", "");
                }
                else
                {
                    return "";
                }

            }
        }

        //Get Saved Report Id from Query string
        private int SavedReportId
        {
            get
            {
                if (!String.IsNullOrEmpty(Request.QueryString["repid"]))
                {
                    return Convert.ToInt16(Request.QueryString["repid"]);
                }
                else
                {
                    return 0;
                }

            }
        }

        //Get Login User Name from session
        public string LoginUserName
        {
            get
            {
                return Convert.ToString(this.Session["UserName"]);
            }
        }

        //Get Login User Profile Name 
        public string LoginUserProfileName
        {
            get
            {
                return Convert.ToString(this.LoginUserProfileDetails[0].Trim());
            }
        }

        //Get Login User Vendor Number
        public string LoginUserVendorNumber_FilterUser
        {
            get
            {
                return Convert.ToString(this.LoginUserProfileDetails[1].Trim());
            }
        }

        //Get Login User Profile Details
        public string[] LoginUserProfileDetails
        {
            get
            {
                return DataManager.GetUserProfileAndVendorNumber(LoginUserName);
            }
        }

        //Get Product Corporate Hierarchy Details datatable
        protected DataTable dtProductDetailsCorporateHierarchy
        {
            get
            {
                return Session["ProductDetailsCorporateHierarchy"] != null ? Session["ProductDetailsCorporateHierarchy"] as DataTable : new DataTable();


            }
            set
            {
                Session["ProductDetailsCorporateHierarchy"] = value;
            }
        }

        //Get Product Custom Hierarchy Details datatable
        protected DataTable dtProductDetailsCustomHierarchy
        {
            get
            {

                return Session["dtProductDetailsFiltered"] != null ? Session["dtProductDetailsFiltered"] as DataTable : new DataTable();
            }
            set
            {
                Session["dtProductDetailsFiltered"] = value;
            }
        }

        //Get Location Custom Hierarchy Details datatable
        protected DataTable dtLocationDetailsCustomHierarchy
        {
            get
            {
                return Session["locationHierarchyDetailsCorporateFiltered"] != null ? Session["locationHierarchyDetailsCorporateFiltered"] as DataTable : new DataTable();
            }
            set
            {
                Session["locationHierarchyDetailsCorporateFiltered"] = value;
            }
        }

        private DataLayer dataAccessLayer;
        private RadComboBox ddlBanner;
        private RadComboBox ddlSubBanner;

        private RadComboBox ddlLoctionSourceBanner;
        private RadComboBox ddlLoctionSourceSubBanner;
        private RadComboBox ddlLoctionSourceStore;

        private RadComboBox ddlLoctionDestinationBanner;
        private RadComboBox ddlLoctionDestinationSubBanner;
        private RadComboBox ddlLoctionDestinationStore;

        private RadComboBox ddlYear;
        private RadComboBox ddlFiscalYearWeekFrom;
        private RadComboBox ddlFiscalYearWeekTo;

        private RadComboBox ddlDepartment;
        private RadComboBox ddlCategory;
        private RadComboBox ddlSubCategory;
        private RadComboBox ddlSegment = null;
        private RadComboBox ddlBrand = null;
        private RadComboBox ddlProductSize = null;
        private RadComboBox ddlHierarchy;
        private RadComboBox ddlLocationStore;

        private RadComboBox ddlCustomHierarchyLevel1_NewItem;
        private RadComboBox ddlCustomHierarchyLevel1_DiscontinuedItem;
        private RadComboBox ddlCustomHierarchyLevel1;
        private RadComboBox ddlCustomHierarchyLevel2;
        private RadComboBox ddlCustomHierarchyLevel3;


        private RadComboBox ddlBrandType;
        private RadComboBox ddlViewBy;
        private RadComboBox ddlNoOffWeeks;
        private RadComboBox ddlRecentTimeFrame;

        private HtmlGenericControl trlblHeading;
        private HtmlGenericControl tdlblHeading;
        private Label lblHeading;
        private HtmlGenericControl trlbl;
        private HtmlGenericControl trddl;
        private HtmlGenericControl tdddl;
        private RadComboBox ddlHierarchyType;
        private RequiredFieldValidator rfvTexbox;
        private RequiredFieldValidator rfvddlMultiSelection;
        private RequiredFieldValidator rfvddlSingleSelection;
        private CalendarExtender calExenderTextbox;


        private RadComboBox ddlHoliday;
        private RadComboBox ddlPog;

        private DataTable dtYearDetails;
        private DataTable dtBannerAndSubBannerDetails;
        private DataTable dtHolidayAndYearDetails;
        private dynamic pageControlsDetails;
        private bool AnchorTagApplied = false;
        private Dictionary<string, dynamic> selectedFilterValues;
        private static string urlName;
        private string mendatoryParams;
        private bool isEncodingRequired = false;
        #endregion

        //Page load Event
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.dataAccessLayer = new DataLayer();
                if (!Page.IsPostBack)
                {
                    AppManager.FillShareUsers(RdShareUsers, DataManager.IsCorporateUser(this.LoginUserName), this.LoginUserName);
                    //clear existing session and set values for current navaigation menu/report
                    Int32 ReportidShare = 0;
                    if (Request.QueryString["repid"] != null)
                    {
                        ReportidShare = Convert.ToInt32(Request.QueryString["repid"]);
                    }
                    DataManager.CheckShareMessage(ReportidShare, ltrMessage, ltrSharedBy, divMessage);

                    this.dtProductDetailsCorporateHierarchy = null;
                    this.dtProductDetailsCustomHierarchy = null;
                    this.dtLocationDetailsCustomHierarchy = null;
                    if (DataManager.IsCorporateUser(this.LoginUserName))
                    {
                        this.dtProductDetailsCorporateHierarchy = this.dataAccessLayer.GetProductDetailsCorporateHierarchy(ReportId, this.LoginUserName);
                    }
                    else
                    {
                        this.dtProductDetailsCorporateHierarchy = this.dataAccessLayer.GetProductDetailsCorporateHierarchy(ReportId, this.LoginUserName, this.LoginUserProfileName, this.LoginUserProfileDetails[1].ToString());
                    }
                    this.dtProductDetailsCustomHierarchy = this.dataAccessLayer.GetProductHierarchyDetails(this.LoginUserName);
                    this.dtLocationDetailsCustomHierarchy = this.dataAccessLayer.GetLocationHierarchyDetails(this.LoginUserName);

                }
                if (ReportId > 0)
                {
                    //creating dynamic filters controls according to ReportId
                    DataTable dtFilters = dataAccessLayer.GetFiltersDataTable(ReportId);
                    CreateFilterControl(dtFilters);


                    urlName = dtFilters.Rows[0]["REPORT_NAME"].ToString();
                    urlName = urlName.Replace(" ", "").Replace("-", "");
                    mendatoryParams = Convert.ToString(dtFilters.Rows[0]["EXTRA_PARAMETERS"]);

                    //Assiging validation group to button show report for validation
                    BtnShowReport.ValidationGroup = IcontrolFilterResources.ValidationGroup;

                    Button btnFilterForVendorNumber = new Button();
                    btnFilterForVendorNumber.ID = "BtnFilterForVendorNumber";
                    btnFilterForVendorNumber.ClientIDMode = ClientIDMode.Static;
                    btnFilterForVendorNumber.Style.Add("display", "none");
                    btnFilterForVendorNumber.Click += new EventHandler(BtnFilterForVendorNumber_Click);
                    this.ControlsParent.Controls.Add(btnFilterForVendorNumber);

                }
                if (!Page.IsPostBack)
                {
                    RadioButtonList rblDailyWeekly = (RadioButtonList)ControlsParent.FindControl("DailyWeekly");
                    if (rblDailyWeekly != null)
                        rblDailyWeekly_SelectedIndexChanged(rblDailyWeekly, null);

                    AppManager.FillRelativeDateListforWeeks(DrpDateVal);

                    Session["ShowRepforSave"] = null;
                    if (Request.QueryString["RepType"] != null)
                    {
                        Session["ShowRepforSave"] = "1";
                        if (ViewState["OnceIn"] == null)
                        {
                            GetPageControls(false, false, true);
                            ViewState["OnceIn"] = "1";
                            ShowReport();
                        }
                    }
                    else
                    {
                        ClearInstances();
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Page_Load:"));
            }
        }

        #region Bind Input Controls

        #region  Location Dropdown - Corporate Hierarchy
        /// <summary>
        /// Binds the banner list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateBannerList()
        {
            ddlBanner = new RadComboBox();
            try
            {
                dataAccessLayer = new DataLayer();
                this.dtBannerAndSubBannerDetails = null;
                this.dtBannerAndSubBannerDetails = dataAccessLayer.GetBannerAndSubBannerDetails();
                if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                {
                    this.dtBannerAndSubBannerDetails.DefaultView.Sort = AppConstants.BANNER_COLUMN + " ASC";
                    using (DataTable dtBanners = this.dtBannerAndSubBannerDetails.DefaultView.ToTable(true, AppConstants.BANNER_COLUMN))
                    {
                        ddlBanner.DataSource = dtBanners;
                        ddlBanner.DataTextField = AppConstants.BANNER_COLUMN;
                        ddlBanner.DataValueField = AppConstants.BANNER_COLUMN;
                        ddlBanner.EnableViewState = true;
                        ddlBanner.EmptyMessage = "None";
                        ddlBanner.DataBind();
                    }

                    ddlBanner.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlBanner_SelectedIndexChanged);
                    ddlBanner.AutoPostBack = true;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateBannerList: "));
            }

            return ddlBanner;
        }

        /// <summary>
        /// Binds the Sub banner list.
        /// </summary>
        /// <param name="isSingleSelectionList"></param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateSubBannerList()
        {
            ddlSubBanner = new RadComboBox();
            try
            {
                ddlBanner = (RadComboBox)this.ControlsParent.FindControl("LocBanner");
                if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                {
                    if (ddlBanner == null)
                    {
                        this.dtBannerAndSubBannerDetails.DefaultView.Sort = AppConstants.SUBBANNER_COLUMN + " ASC";
                        using (DataTable dtSubBanner = this.dtBannerAndSubBannerDetails.DefaultView.ToTable(true, new string[] { AppConstants.SUBBANNER_ID_COLUMN, AppConstants.SUBBANNER_COLUMN }))
                        {
                            ddlSubBanner.DataSource = dtSubBanner;
                            ddlSubBanner.DataValueField = AppConstants.SUBBANNER_ID_COLUMN;
                            ddlSubBanner.DataTextField = AppConstants.SUBBANNER_COLUMN;
                            ddlSubBanner.DataBind();
                        }
                    }
                    ddlSubBanner.EnableViewState = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateSubBannerList: "));
            }
            return ddlSubBanner;
        }

        #endregion

        #region  Location Dropdown - Custom Hierarchy

        /// <summary>
        /// Creates the location store list for location Custom Hierarchy.
        /// </summary>
        /// <param name="isSingleSelectionList">if set to <c>true</c> [is single selection list].</param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateLocationStoreList()
        {
            ddlLocationStore = new RadComboBox();
            try
            {
                if (this.dtLocationDetailsCustomHierarchy != null && this.dtLocationDetailsCustomHierarchy.Rows.Count > 0)
                {
                    this.dtLocationDetailsCustomHierarchy.DefaultView.Sort = AppConstants.CUSTOM_STORE_NAME_COLUMN + " ASC";
                    using (DataTable dtLocationHierarchy = this.dtLocationDetailsCustomHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CUSTOM_STORE_GROUP_ID_COLUMN, AppConstants.CUSTOM_STORE_NAME_COLUMN }))
                    {
                        ddlLocationStore.DataSource = dtLocationHierarchy;
                        ddlLocationStore.DataTextField = AppConstants.CUSTOM_STORE_NAME_COLUMN;
                        ddlLocationStore.DataValueField = AppConstants.CUSTOM_STORE_GROUP_ID_COLUMN;
                        ddlLocationStore.EmptyMessage = "None";
                        ddlLocationStore.DataBind();
                        ddlLocationStore.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlLocationStore_SelectedIndexChanged);
                        ddlLocationStore.AutoPostBack = true;
                        ddlLocationStore.EnableViewState = true;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateLocationStoreList: "));
            }
            return ddlLocationStore;
        }

        /// <summary>
        /// Creates the custom level1 list for location Custom Hierarchy.
        /// </summary>
        /// <param name="isSingleSelectionList">if set to <c>true</c> [is single selection list].</param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateLocationCustomLevel1List()
        {
            ddlCustomHierarchyLevel1 = new RadComboBox();
            try
            {
                ddlLocationStore = (RadComboBox)this.ControlsParent.FindControl("LocationStore");
                if (this.dtLocationDetailsCustomHierarchy != null && this.dtLocationDetailsCustomHierarchy.Rows.Count > 0)
                {
                    if (ddlLocationStore == null)
                    {
                        this.dtLocationDetailsCustomHierarchy.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_1_COLUMN + " ASC";
                        using (DataTable dtCustomLevel1 = this.dtLocationDetailsCustomHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CUSTOM_STORE_GROUP_ID_COLUMN, AppConstants.CUSTOM_LEVEL_1_COLUMN }))
                        {
                            ddlCustomHierarchyLevel1.DataSource = dtCustomLevel1;
                            ddlCustomHierarchyLevel1.DataValueField = AppConstants.CUSTOM_STORE_GROUP_ID_COLUMN;
                            ddlCustomHierarchyLevel1.DataTextField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            ddlCustomHierarchyLevel1.DataBind();
                        }
                    }
                    ddlCustomHierarchyLevel1.EnableViewState = true;
                    ddlCustomHierarchyLevel1.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlLocationCustomHierarchyLevel1_SelectedIndexChanged);
                    ddlCustomHierarchyLevel1.AutoPostBack = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateLocationCustomLevel1List: "));
            }
            return ddlCustomHierarchyLevel1;
        }


        /// <summary>
        /// Creates the custom level2 list for location Custom Hierarchy.
        /// </summary>
        /// <param name="isSingleSelectionList">if set to <c>true</c> [is single selection list].</param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateLocationCustomLevel2List()
        {
            ddlCustomHierarchyLevel2 = new RadComboBox();
            try
            {
                ddlCustomHierarchyLevel1 = (RadComboBox)this.ControlsParent.FindControl("LocationCustom1");
                if (this.dtLocationDetailsCustomHierarchy != null && this.dtLocationDetailsCustomHierarchy.Rows.Count > 0)
                {
                    if (ddlCustomHierarchyLevel1 == null)
                    {
                        this.dtLocationDetailsCustomHierarchy.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_2_COLUMN + " ASC";
                        using (DataTable dtCustomLevel2 = this.dtLocationDetailsCustomHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CUSTOM_STORE_GROUP_ID_COLUMN, AppConstants.CUSTOM_LEVEL_2_COLUMN }))
                        {
                            ddlCustomHierarchyLevel2.DataSource = dtCustomLevel2;
                            ddlCustomHierarchyLevel2.DataValueField = AppConstants.CUSTOM_STORE_GROUP_ID_COLUMN;
                            ddlCustomHierarchyLevel2.DataTextField = AppConstants.CUSTOM_LEVEL_2_COLUMN;
                            ddlCustomHierarchyLevel2.DataBind();
                        }
                    }
                    ddlCustomHierarchyLevel2.EnableViewState = true;
                    ddlCustomHierarchyLevel2.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlLocationCustomHierarchyLevel2_SelectedIndexChanged);
                    ddlCustomHierarchyLevel2.AutoPostBack = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateLocationCustomLevel2List: "));
            }
            return ddlCustomHierarchyLevel2;
        }

        /// <summary>
        /// Creates the custom level3 list for location Custom Hierarchy.
        /// </summary>
        /// <param name="isSingleSelectionList">if set to <c>true</c> [is single selection list].</param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateLocationCustomLevel3List()
        {
            ddlCustomHierarchyLevel3 = new RadComboBox();
            try
            {
                ddlCustomHierarchyLevel2 = (RadComboBox)this.ControlsParent.FindControl("LocationCustom2");
                if (this.dtLocationDetailsCustomHierarchy != null && this.dtLocationDetailsCustomHierarchy.Rows.Count > 0)
                {
                    if (ddlCustomHierarchyLevel2 == null)
                    {
                        this.dtLocationDetailsCustomHierarchy.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_3_COLUMN + " ASC";
                        using (DataTable dtCustomLevel3 = this.dtLocationDetailsCustomHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CUSTOM_STORE_GROUP_ID_COLUMN, AppConstants.CUSTOM_LEVEL_3_COLUMN }))
                        {
                            ddlCustomHierarchyLevel3.DataSource = dtCustomLevel3;
                            ddlCustomHierarchyLevel3.DataValueField = AppConstants.CUSTOM_STORE_GROUP_ID_COLUMN;
                            ddlCustomHierarchyLevel3.DataTextField = AppConstants.CUSTOM_LEVEL_3_COLUMN;
                            ddlCustomHierarchyLevel3.DataBind();
                        }
                    }
                    ddlCustomHierarchyLevel3.EnableViewState = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateLocationCustomLevel3List: "));
            }
            return ddlCustomHierarchyLevel3;
        }


        #endregion

        #region Products Dropdown - Corporate Hierarchy
        /// <summary>
        /// Creates the department list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateDepartmentList()
        {
            ddlDepartment = new RadComboBox();
            try
            {
                if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
                {
                    this.dtProductDetailsCorporateHierarchy.DefaultView.Sort = AppConstants.DEPARTMENT_COLUMN + " ASC";
                    using (DataTable dtDepartments = this.dtProductDetailsCorporateHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.DEPARTMENT_COLUMN }))
                    {
                        ddlDepartment.DataSource = dtDepartments;
                        ddlDepartment.DataTextField = AppConstants.DEPARTMENT_COLUMN;
                        ddlDepartment.DataValueField = AppConstants.DEPARTMENT_COLUMN;
                        ddlDepartment.DataBind();
                        ddlDepartment.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlDepartment_SelectedIndexChanged);
                        ddlDepartment.AutoPostBack = true;
                        ddlDepartment.EnableViewState = true;
                        ddlDepartment.EmptyMessage = "None";
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateDepartmentList: "));
            }
            return ddlDepartment;
        }

        /// <summary>
        /// Binds the category list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateCategoryList(bool isSingleSelectionList = false)
        {
            ddlCategory = new RadComboBox();
            try
            {
                ddlDepartment = (RadComboBox)this.ControlsParent.FindControl("Department");
                if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
                {
                    if (ddlDepartment == null)
                    {
                        this.dtProductDetailsCorporateHierarchy.DefaultView.Sort = AppConstants.CATEGORY_COLUMN + " ASC";
                        using (DataTable dtcategory = this.dtProductDetailsCorporateHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CATEGORY_COLUMN }))
                        {
                            ddlCategory.DataSource = dtcategory;
                            ddlCategory.DataValueField = AppConstants.CATEGORY_COLUMN;
                            ddlCategory.DataTextField = AppConstants.CATEGORY_COLUMN;
                            ddlCategory.DataBind();
                        }
                    }
                    ddlCategory.EnableViewState = true;
                    ddlCategory.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlCategory_SelectedIndexChanged);
                    ddlCategory.AutoPostBack = true;

                    if (isSingleSelectionList)
                    {
                        ddlCategory.Items.Insert(0, new RadComboBoxItem { Text = IcontrolFilterResources.ddl_Select, Value = IcontrolFilterResources.ddl_Select });
                    }
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateCategoryList: "));
            }
            return ddlCategory;
        }

        /// <summary>
        /// Creates the sub category list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateSubCategoryList()
        {
            ddlSubCategory = new RadComboBox();
            try
            {
                ddlCategory = (RadComboBox)this.ControlsParent.FindControl("Category");
                if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
                {
                    if (ddlCategory == null)
                    {
                        this.dtProductDetailsCorporateHierarchy.DefaultView.Sort = AppConstants.SUBCATEGORY_COLUMN + " ASC";
                        using (DataTable dtSubCategory = this.dtProductDetailsCorporateHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.SUBCATEGORY_COLUMN }))
                        {
                            ddlSubCategory.DataSource = dtSubCategory;
                            ddlSubCategory.DataValueField = AppConstants.SUBCATEGORY_COLUMN;
                            ddlSubCategory.DataTextField = AppConstants.SUBCATEGORY_COLUMN;
                            ddlSubCategory.DataBind();
                        }
                    }
                    ddlSubCategory.EnableViewState = true;
                    ddlSubCategory.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlSubCategory_SelectedIndexChanged);
                    ddlSubCategory.AutoPostBack = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateSubCategoryList: "));
            }
            return ddlSubCategory;
        }

        /// <summary>
        /// Creates the segment list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateSegmentList()
        {
            ddlSegment = new RadComboBox();
            try
            {
                ddlSubCategory = (RadComboBox)this.ControlsParent.FindControl("SubCategory");
                if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
                {
                    if (ddlSubCategory == null)
                    {
                        this.dtProductDetailsCorporateHierarchy.DefaultView.Sort = AppConstants.SEGMENT_COLUMN + " ASC";
                        using (DataTable dtSegment = this.dtProductDetailsCorporateHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.SEGMENT_COLUMN }))
                        {
                            ddlSegment.DataSource = dtSegment;
                            ddlSegment.DataValueField = AppConstants.SEGMENT_COLUMN;
                            ddlSegment.DataTextField = AppConstants.SEGMENT_COLUMN;
                            ddlSegment.DataBind();
                        }
                    }
                    ddlSegment.EnableViewState = true;
                    ddlSegment.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlSegment_SelectedIndexChanged);
                    ddlSegment.AutoPostBack = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateSegmentList: "));
            }
            return ddlSegment;
        }

        /// <summary>
        /// Creates the corporate brand list.
        /// </summary>
        /// <param name="isSingleSelectionList">if set to <c>true</c> [is single selection list].</param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateCorporateBrandList()
        {
            ddlBrand = new RadComboBox();
            try
            {
                ddlSegment = (RadComboBox)this.ControlsParent.FindControl("Segment");
                if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
                {
                    if (ddlSegment == null)
                    {
                        this.dtProductDetailsCorporateHierarchy.DefaultView.Sort = AppConstants.BRAND_COLUMN + " ASC";
                        using (DataTable dtBrand = this.dtProductDetailsCorporateHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.BRAND_COLUMN }))
                        {
                            ddlBrand.DataSource = dtBrand;
                            ddlBrand.DataValueField = AppConstants.BRAND_COLUMN;
                            ddlBrand.DataTextField = AppConstants.BRAND_COLUMN;
                            ddlBrand.DataBind();
                        }
                    }
                    ddlBrand.EnableViewState = true;
                    ddlBrand.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlCorporateBrand_SelectedIndexChanged);
                    ddlBrand.AutoPostBack = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateCorporateBrandList: "));
            }
            return ddlBrand;
        }

        /// <summary>
        /// Creates the product size list.
        /// </summary>
        /// <param name="isSingleSelectionList">if set to <c>true</c> [is single selection list].</param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateCorporateProductSizeList()
        {
            ddlProductSize = new RadComboBox();
            try
            {
                ddlBrand = (RadComboBox)this.ControlsParent.FindControl("ProductBrand");
                if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
                {
                    if (ddlBrand == null)
                    {
                        this.dtProductDetailsCorporateHierarchy.DefaultView.Sort = AppConstants.PRODUCT_SIZE_COLUMN + " ASC";
                        using (DataTable dtProductSize = this.dtProductDetailsCorporateHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.PRODUCT_SIZE_COLUMN }))
                        {
                            ddlProductSize.DataSource = dtProductSize;
                            ddlProductSize.DataValueField = AppConstants.PRODUCT_SIZE_COLUMN;
                            ddlProductSize.DataTextField = AppConstants.PRODUCT_SIZE_COLUMN;
                            ddlProductSize.DataBind();
                        }
                    }
                    ddlProductSize.EnableViewState = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateCorporateProductSizeList: "));
            }
            return ddlProductSize;
        }

        #endregion

        #region Products Dropdown - Custom Hierarchy

        /// <summary>
        /// Creates the hierarchy list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateHierarchyList(bool isSingleSelectionList = false)
        {
            ddlHierarchy = new RadComboBox();
            try
            {
                if (this.dtProductDetailsCustomHierarchy != null && this.dtProductDetailsCustomHierarchy.Rows.Count > 0)
                {
                    this.dtProductDetailsCustomHierarchy.DefaultView.Sort = AppConstants.CUSTOM_HIERARCHY_COLUMN + " ASC";
                    using (DataTable dtProductHierarchy = this.dtProductDetailsCustomHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CUSTOM_HIERARCHY_ID_COLUMN, AppConstants.CUSTOM_HIERARCHY_COLUMN }))
                    {
                        ddlHierarchy.DataSource = dtProductHierarchy;
                        ddlHierarchy.DataTextField = AppConstants.CUSTOM_HIERARCHY_COLUMN;
                        ddlHierarchy.DataValueField = AppConstants.CUSTOM_HIERARCHY_ID_COLUMN;
                        ddlHierarchy.DataBind();
                        ddlHierarchy.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlHierarchy_SelectedIndexChanged);
                        ddlHierarchy.AutoPostBack = true;
                        ddlHierarchy.EnableViewState = true;
                        if (isSingleSelectionList)
                        {
                            ddlHierarchy.Items.Insert(0, new RadComboBoxItem { Text = IcontrolFilterResources.ddl_Select, Value = IcontrolFilterResources.ddl_Select });
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateHierarchyList: "));
            }
            return ddlHierarchy;
        }

        /// <summary>
        /// Creates the custom level1 list.
        /// </summary>
        /// <param name="isSingleSelectionList">if set to <c>true</c> [is single selection list].</param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateCustomLevel1List()
        {
            ddlCustomHierarchyLevel1 = new RadComboBox();
            try
            {
                ddlHierarchy = (RadComboBox)this.ControlsParent.FindControl("HierarchyName");
                if (this.dtProductDetailsCustomHierarchy != null && this.dtProductDetailsCustomHierarchy.Rows.Count > 0)
                {
                    if (ddlHierarchy == null)
                    {
                        this.dtProductDetailsCustomHierarchy.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_1_COLUMN + " ASC";
                        using (DataTable dtCustomLevel1 = this.dtProductDetailsCustomHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CUSTOM_HIERARCHY_ID_COLUMN, AppConstants.CUSTOM_LEVEL_1_COLUMN }))
                        {
                            ddlCustomHierarchyLevel1.DataSource = dtCustomLevel1;
                            ddlCustomHierarchyLevel1.DataValueField = AppConstants.CUSTOM_HIERARCHY_ID_COLUMN;
                            ddlCustomHierarchyLevel1.DataTextField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            ddlCustomHierarchyLevel1.DataBind();
                        }
                    }
                    ddlCustomHierarchyLevel1.EnableViewState = true;
                    ddlCustomHierarchyLevel1.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlCustomHierarchyLevel1_SelectedIndexChanged);
                    ddlCustomHierarchyLevel1.AutoPostBack = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateCustomLevel1List: "));
            }
            return ddlCustomHierarchyLevel1;
        }

        /// <summary>
        /// Creates the custom level1NewItems list.
        /// </summary>
        /// <param name="isSingleSelectionList">if set to <c>true</c> [is single selection list].</param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateCustomLevel1NewItemsList()
        {
            ddlCustomHierarchyLevel1_NewItem = new RadComboBox();
            try
            {
                ddlHierarchy = (RadComboBox)this.ControlsParent.FindControl("HierarchyName");
                if (this.dtProductDetailsCustomHierarchy != null && this.dtProductDetailsCustomHierarchy.Rows.Count > 0)
                {
                    if (ddlHierarchy == null)
                    {
                        this.dtProductDetailsCustomHierarchy.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_1_COLUMN + " ASC";
                        using (DataTable dtCustomLevel1 = this.dtProductDetailsCustomHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CUSTOM_HIERARCHY_ID_COLUMN, AppConstants.CUSTOM_LEVEL_1_COLUMN }))
                        {
                            ddlCustomHierarchyLevel1_NewItem.DataSource = dtCustomLevel1;
                            ddlCustomHierarchyLevel1_NewItem.DataValueField = AppConstants.CUSTOM_HIERARCHY_ID_COLUMN;
                            ddlCustomHierarchyLevel1_NewItem.DataTextField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            ddlCustomHierarchyLevel1_NewItem.DataBind();
                        }
                    }
                    ddlCustomHierarchyLevel1_NewItem.EnableViewState = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateCustomLevel1NewItemsList: "));
            }
            return ddlCustomHierarchyLevel1_NewItem;
        }

        /// <summary>
        /// Creates the custom level1DiscontinuedItems list.
        /// </summary>
        /// <param name="isSingleSelectionList">if set to <c>true</c> [is single selection list].</param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateCustomLevel1DiscontinuedItemsList()
        {
            ddlCustomHierarchyLevel1_DiscontinuedItem = new RadComboBox();
            try
            {
                ddlHierarchy = (RadComboBox)this.ControlsParent.FindControl("HierarchyName");
                if (this.dtProductDetailsCustomHierarchy != null && this.dtProductDetailsCustomHierarchy.Rows.Count > 0)
                {
                    if (ddlHierarchy == null)
                    {
                        this.dtProductDetailsCustomHierarchy.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_1_COLUMN + " ASC";
                        using (DataTable dtCustomLevel1 = this.dtProductDetailsCustomHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CUSTOM_HIERARCHY_ID_COLUMN, AppConstants.CUSTOM_LEVEL_1_COLUMN }))
                        {
                            ddlCustomHierarchyLevel1_DiscontinuedItem.DataSource = dtCustomLevel1;
                            ddlCustomHierarchyLevel1_DiscontinuedItem.DataValueField = AppConstants.CUSTOM_HIERARCHY_ID_COLUMN;
                            ddlCustomHierarchyLevel1_DiscontinuedItem.DataTextField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            ddlCustomHierarchyLevel1_DiscontinuedItem.DataBind();
                        }
                    }
                    ddlCustomHierarchyLevel1_DiscontinuedItem.EnableViewState = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateCustomLevel1DiscontinuedItemsList: "));
            }
            return ddlCustomHierarchyLevel1_DiscontinuedItem;
        }

        /// <summary>
        /// Creates the custom level2 list.
        /// </summary>
        /// <param name="isSingleSelectionList">if set to <c>true</c> [is single selection list].</param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateCustomLevel2List()
        {
            ddlCustomHierarchyLevel2 = new RadComboBox();
            try
            {
                ddlCustomHierarchyLevel1 = (RadComboBox)this.ControlsParent.FindControl("CustomLevel1");
                if (ddlCustomHierarchyLevel1 == null)
                {
                    ddlCustomHierarchyLevel1 = (RadComboBox)this.ControlsParent.FindControl("CustomLevel1NewItems");
                }
                if (this.dtProductDetailsCustomHierarchy != null && this.dtProductDetailsCustomHierarchy.Rows.Count > 0)
                {
                    if (ddlCustomHierarchyLevel1 == null)
                    {
                        this.dtProductDetailsCustomHierarchy.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_2_COLUMN + " ASC";
                        using (DataTable dtCustomLevel2 = this.dtProductDetailsCustomHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CUSTOM_HIERARCHY_ID_COLUMN, AppConstants.CUSTOM_LEVEL_2_COLUMN }))
                        {
                            ddlCustomHierarchyLevel2.DataSource = dtCustomLevel2;
                            ddlCustomHierarchyLevel2.DataValueField = AppConstants.CUSTOM_HIERARCHY_ID_COLUMN;
                            ddlCustomHierarchyLevel2.DataTextField = AppConstants.CUSTOM_LEVEL_2_COLUMN;
                            ddlCustomHierarchyLevel2.DataBind();
                        }
                    }
                    ddlCustomHierarchyLevel2.EnableViewState = true;
                    ddlCustomHierarchyLevel2.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlCustomHierarchyLevel2_SelectedIndexChanged);
                    ddlCustomHierarchyLevel2.AutoPostBack = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateCustomLevel2List: "));
            }
            return ddlCustomHierarchyLevel2;
        }

        /// <summary>
        /// Creates the custom level3 list.
        /// </summary>
        /// <param name="isSingleSelectionList">if set to <c>true</c> [is single selection list].</param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateCustomLevel3List()
        {
            ddlCustomHierarchyLevel3 = new RadComboBox();
            try
            {
                ddlCustomHierarchyLevel2 = (RadComboBox)this.ControlsParent.FindControl("CustomLevel2");
                if (this.dtProductDetailsCustomHierarchy != null && this.dtProductDetailsCustomHierarchy.Rows.Count > 0)
                {
                    if (ddlCustomHierarchyLevel2 == null)
                    {
                        this.dtProductDetailsCustomHierarchy.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_3_COLUMN + " ASC";
                        using (DataTable dtCustomLevel3 = this.dtProductDetailsCustomHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CUSTOM_HIERARCHY_ID_COLUMN, AppConstants.CUSTOM_LEVEL_3_COLUMN }))
                        {
                            ddlCustomHierarchyLevel3.DataSource = dtCustomLevel3;
                            ddlCustomHierarchyLevel3.DataValueField = AppConstants.CUSTOM_HIERARCHY_ID_COLUMN;
                            ddlCustomHierarchyLevel3.DataTextField = AppConstants.CUSTOM_LEVEL_3_COLUMN;
                            ddlCustomHierarchyLevel3.DataBind();
                        }
                    }
                    ddlCustomHierarchyLevel3.EnableViewState = true;
                    ddlCustomHierarchyLevel3.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlCustomHierarchyLevel3_SelectedIndexChanged);
                    ddlCustomHierarchyLevel3.AutoPostBack = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateCustomLevel3List: "));
            }
            return ddlCustomHierarchyLevel3;
        }

        /// <summary>
        /// Creates the custom brand list.
        /// </summary>
        /// <param name="isSingleSelectionList">if set to <c>true</c> [is single selection list].</param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateCustomBrandList()
        {
            ddlBrand = new RadComboBox();
            try
            {
                ddlCustomHierarchyLevel3 = (RadComboBox)this.ControlsParent.FindControl("CustomLevel3");
                if (this.dtProductDetailsCustomHierarchy != null && this.dtProductDetailsCustomHierarchy.Rows.Count > 0)
                {
                    if (ddlCustomHierarchyLevel3 == null)
                    {
                        this.dtProductDetailsCustomHierarchy.DefaultView.Sort = AppConstants.CUSTOM_BRAND_COLUMN + " ASC";
                        using (DataTable dtBrand = this.dtProductDetailsCustomHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CUSTOM_HIERARCHY_ID_COLUMN, AppConstants.CUSTOM_BRAND_COLUMN }))
                        {
                            ddlBrand.DataSource = dtBrand;
                            ddlBrand.DataValueField = AppConstants.CUSTOM_HIERARCHY_ID_COLUMN;
                            ddlBrand.DataTextField = AppConstants.CUSTOM_BRAND_COLUMN;
                            ddlBrand.DataBind();
                        }
                    }
                    ddlBrand.EnableViewState = true;
                    ddlBrand.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlCustomBrand_SelectedIndexChanged);
                    ddlBrand.AutoPostBack = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateCustomBrandList: "));
            }
            return ddlBrand;
        }

        /// <summary>
        /// Creates the custom product size list.
        /// </summary>
        /// <param name="isSingleSelectionList">if set to <c>true</c> [is single selection list].</param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateCustomProductSizeList()
        {
            ddlProductSize = new RadComboBox();
            try
            {
                ddlBrand = (RadComboBox)this.ControlsParent.FindControl("CustomBrand");
                if (this.dtProductDetailsCustomHierarchy != null && this.dtProductDetailsCustomHierarchy.Rows.Count > 0)
                {
                    if (ddlBrand == null)
                    {
                        this.dtProductDetailsCustomHierarchy.DefaultView.Sort = AppConstants.CUSTOM_PRODUCT_SIZE_COLUMN + " ASC";
                        using (DataTable dtProductSize = this.dtProductDetailsCustomHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CUSTOM_HIERARCHY_ID_COLUMN, AppConstants.CUSTOM_PRODUCT_SIZE_COLUMN }))
                        {
                            ddlProductSize.DataSource = dtProductSize;
                            ddlProductSize.DataValueField = AppConstants.CUSTOM_HIERARCHY_ID_COLUMN;
                            ddlProductSize.DataTextField = AppConstants.CUSTOM_PRODUCT_SIZE_COLUMN;
                            ddlProductSize.DataBind();
                        }
                    }
                    ddlProductSize.EnableViewState = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateCustomProductSizeList: "));
            }
            return ddlProductSize;
        }

        #endregion

        /// <summary>
        /// Binds the banner list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateLoctionHierarchySourceBannerList()
        {
            ddlLoctionSourceBanner = new RadComboBox();
            try
            {
                dataAccessLayer = new DataLayer();
                this.dtBannerAndSubBannerDetails = null;
                this.dtBannerAndSubBannerDetails = dataAccessLayer.GetBannerAndSubBannerDetails();
                if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                {
                    this.dtBannerAndSubBannerDetails.DefaultView.Sort = AppConstants.BANNER_COLUMN + " ASC";
                    using (DataTable dtLoctionSourceBanners = this.dtBannerAndSubBannerDetails.DefaultView.ToTable(true, AppConstants.BANNER_COLUMN))
                    {
                        ddlLoctionSourceBanner.DataSource = dtLoctionSourceBanners;
                        ddlLoctionSourceBanner.DataTextField = AppConstants.BANNER_COLUMN;
                        ddlLoctionSourceBanner.DataValueField = AppConstants.BANNER_COLUMN;
                        ddlLoctionSourceBanner.AutoPostBack = true;
                        ddlLoctionSourceBanner.EnableViewState = true;
                        ddlLoctionSourceBanner.EmptyMessage = "None";
                        ddlLoctionSourceBanner.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlLoctionHierarchySourceBanner_SelectedIndexChanged);
                        ddlLoctionSourceBanner.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateLoctionHierarchySourceBannerList: "));
            }

            return ddlLoctionSourceBanner;
        }

        /// <summary>
        /// Binds the location source Sub banner list.
        /// </summary>
        /// <param name="isSingleSelectionList"></param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateLoctionHierarchySourceSubBannerList()
        {
            ddlLoctionSourceSubBanner = new RadComboBox();
            try
            {
                ddlLoctionSourceBanner = (RadComboBox)this.ControlsParent.FindControl("LocHierSrcBanner");
                if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                {
                    if (ddlLoctionSourceBanner == null)
                    {
                        this.dtBannerAndSubBannerDetails.DefaultView.Sort = AppConstants.SUBBANNER_COLUMN + " ASC";
                        using (DataTable dtLoctionSourceSubBanner = this.dtBannerAndSubBannerDetails.DefaultView.ToTable(true, AppConstants.SUBBANNER_COLUMN))
                        {
                            ddlLoctionSourceSubBanner.DataSource = dtLoctionSourceSubBanner;
                            ddlLoctionSourceSubBanner.DataValueField = AppConstants.SUBBANNER_COLUMN;
                            ddlLoctionSourceSubBanner.DataTextField = AppConstants.SUBBANNER_COLUMN;
                            ddlLoctionSourceSubBanner.DataBind();
                        }
                    }
                    ddlLoctionSourceSubBanner.EnableViewState = true;
                    ddlLoctionSourceSubBanner.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlLoctionSourceSubBanner_SelectedIndexChanged);
                    ddlLoctionSourceSubBanner.AutoPostBack = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateLoctionHierarchySourceSubBannerList: "));
            }
            return ddlLoctionSourceSubBanner;
        }

        /// <summary>
        /// Binds the location source Store list.
        /// </summary>
        /// <param name="isSingleSelectionList"></param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateLoctionHierarchySourceStoreList()
        {
            ddlLoctionSourceStore = new RadComboBox();
            try
            {
                ddlLoctionSourceSubBanner = (RadComboBox)this.ControlsParent.FindControl("LocHierSrcSubBanner");
                if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                {
                    if (ddlLoctionSourceSubBanner == null)
                    {
                        this.dtBannerAndSubBannerDetails.DefaultView.Sort = AppConstants.LOC_SOURCE_STORE_COLUMN + " ASC";
                        using (DataTable dtLoctionSourceSubBanner = this.dtBannerAndSubBannerDetails.DefaultView.ToTable(true, AppConstants.LOC_SOURCE_STORE_COLUMN))
                        {
                            ddlLoctionSourceStore.DataSource = dtLoctionSourceSubBanner;
                            ddlLoctionSourceStore.DataValueField = AppConstants.LOC_SOURCE_STORE_COLUMN;
                            ddlLoctionSourceStore.DataTextField = AppConstants.LOC_SOURCE_STORE_COLUMN;
                            ddlLoctionSourceStore.DataBind();
                        }
                    }
                    ddlLoctionSourceStore.EnableViewState = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateLoctionHierarchySourceStoreList: "));
            }
            return ddlLoctionSourceStore;
        }

        /// <summary>
        /// Binds the banner list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateLoctionHierarchyDestinationBannerList()
        {
            ddlLoctionDestinationBanner = new RadComboBox();
            try
            {
                dataAccessLayer = new DataLayer();
                this.dtBannerAndSubBannerDetails = null;
                this.dtBannerAndSubBannerDetails = dataAccessLayer.GetBannerAndSubBannerDetails();
                if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                {
                    this.dtBannerAndSubBannerDetails.DefaultView.Sort = AppConstants.BANNER_COLUMN + " ASC";
                    using (DataTable dtLoctionDestinationBanners = this.dtBannerAndSubBannerDetails.DefaultView.ToTable(true, AppConstants.BANNER_COLUMN))
                    {
                        ddlLoctionDestinationBanner.DataSource = dtLoctionDestinationBanners;
                        ddlLoctionDestinationBanner.DataTextField = AppConstants.BANNER_COLUMN;
                        ddlLoctionDestinationBanner.DataValueField = AppConstants.BANNER_COLUMN;
                        ddlLoctionDestinationBanner.AutoPostBack = true;
                        ddlLoctionDestinationBanner.EnableViewState = true;
                        ddlLoctionDestinationBanner.EmptyMessage = "None";
                        ddlLoctionDestinationBanner.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlLoctionHierarchyDestinationBanner_SelectedIndexChanged);
                        ddlLoctionDestinationBanner.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateLoctionHierarchyDestinationBannerList: "));
            }

            return ddlLoctionDestinationBanner;
        }

        /// <summary>
        /// Binds the location Destination Sub banner list.
        /// </summary>
        /// <param name="isSingleSelectionList"></param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateLoctionHierarchyDestinationSubBannerList()
        {
            ddlLoctionDestinationSubBanner = new RadComboBox();
            try
            {
                ddlLoctionDestinationBanner = (RadComboBox)this.ControlsParent.FindControl("LocHierDesBanner");
                if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                {
                    if (ddlLoctionDestinationBanner == null)
                    {
                        this.dtBannerAndSubBannerDetails.DefaultView.Sort = AppConstants.SUBBANNER_COLUMN + " ASC";
                        using (DataTable dtLoctionDestinationSubBanner = this.dtBannerAndSubBannerDetails.DefaultView.ToTable(true, AppConstants.SUBBANNER_COLUMN))
                        {
                            ddlLoctionDestinationSubBanner.DataSource = dtLoctionDestinationSubBanner;
                            ddlLoctionDestinationSubBanner.DataValueField = AppConstants.SUBBANNER_COLUMN;
                            ddlLoctionDestinationSubBanner.DataTextField = AppConstants.SUBBANNER_COLUMN;
                            ddlLoctionDestinationSubBanner.DataBind();
                        }
                    }
                    ddlLoctionDestinationSubBanner.EnableViewState = true;
                    ddlLoctionDestinationSubBanner.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlLoctionDestinationSubBanner_SelectedIndexChanged);
                    ddlLoctionDestinationSubBanner.AutoPostBack = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateLoctionHierarchyDestinationSubBannerList: "));
            }
            return ddlLoctionDestinationSubBanner;
        }

        /// <summary>
        /// Binds the location Destination Store list.
        /// </summary>
        /// <param name="isSingleSelectionList"></param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateLoctionHierarchyDestinationStoreList()
        {
            ddlLoctionDestinationStore = new RadComboBox();
            try
            {
                ddlLoctionDestinationSubBanner = (RadComboBox)this.ControlsParent.FindControl("LocHierDesSubBanner");
                if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                {
                    if (ddlLoctionDestinationSubBanner == null)
                    {
                        this.dtBannerAndSubBannerDetails.DefaultView.Sort = AppConstants.LOC_SOURCE_STORE_COLUMN + " ASC";
                        using (DataTable dtLoctionSourceSubBanner = this.dtBannerAndSubBannerDetails.DefaultView.ToTable(true, AppConstants.LOC_SOURCE_STORE_COLUMN))
                        {
                            ddlLoctionDestinationStore.DataSource = dtLoctionSourceSubBanner;
                            ddlLoctionDestinationStore.DataValueField = AppConstants.LOC_SOURCE_STORE_COLUMN;
                            ddlLoctionDestinationStore.DataTextField = AppConstants.LOC_SOURCE_STORE_COLUMN;
                            ddlLoctionDestinationStore.DataBind();
                        }
                    }
                    ddlLoctionDestinationStore.EnableViewState = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateLoctionHierarchyDestinationStoreList: "));
            }
            return ddlLoctionDestinationStore;
        }

        /// <summary>
        /// Binds the fiscal year week from list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateFiscalYearWeekFromList()
        {
            ddlFiscalYearWeekFrom = new RadComboBox();
            try
            {
                dataAccessLayer = new DataLayer();
                ddlFiscalYearWeekFrom.DataSource = dataAccessLayer.GetFiscalYearWeekDetails();
                ddlFiscalYearWeekFrom.DataValueField = "FISCAL_YEAR_WEEK";
                ddlFiscalYearWeekFrom.DataTextField = "FISCAL_YEAR_WEEK";
                ddlFiscalYearWeekFrom.EnableViewState = true;
                ddlFiscalYearWeekFrom.DataBind();
                ddlFiscalYearWeekFrom.SelectedIndex = 3;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateFiscalYearWeekFromList: "));
            }
            return ddlFiscalYearWeekFrom;
        }

        /// <summary>
        /// Binds the fiscal year week to list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateFiscalYearWeekToList()
        {
            ddlFiscalYearWeekTo = new RadComboBox();
            try
            {
                dataAccessLayer = new DataLayer();
                ddlFiscalYearWeekTo.DataSource = dataAccessLayer.GetFiscalYearWeekDetails();
                ddlFiscalYearWeekTo.DataValueField = "FISCAL_YEAR_WEEK";
                ddlFiscalYearWeekTo.DataTextField = "FISCAL_YEAR_WEEK";
                ddlFiscalYearWeekTo.EnableViewState = true;
                ddlFiscalYearWeekTo.DataBind();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateFiscalYearWeekToList: "));
            }

            return ddlFiscalYearWeekTo;
        }

        /// <summary>
        /// Binds the brand list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateBrandTypeList()
        {
            ddlBrandType = new RadComboBox();
            try
            {

                ddlBrandType.EnableViewState = true;
                ddlBrandType.Items.Add(new RadComboBoxItem() { Text = IcontrolFilterResources.BrandType_All, Value = IcontrolFilterResources.BrandType_All_Value });
                ddlBrandType.Items.Add(new RadComboBoxItem() { Text = IcontrolFilterResources.BrandType_PrivateLabel, Value = IcontrolFilterResources.BrandType_PrivateLabel_Value });
                ddlBrandType.Items.Add(new RadComboBoxItem() { Text = IcontrolFilterResources.BrandType_NationalBrand, Value = IcontrolFilterResources.BrandType_NationalBrand_Value });
                ddlBrandType.DataBind();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateBrandTypeList: "));
            }
            return ddlBrandType;

        }

        /// <summary>
        /// Binds the view by list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateViewByList()
        {
            ddlViewBy = new RadComboBox();
            try
            {
                ddlViewBy.Items.Add(new RadComboBoxItem() { Text = IcontrolFilterResources.ViewBy_Sales, Value = IcontrolFilterResources.ViewBy_Sales });
                ddlViewBy.Items.Add(new RadComboBoxItem() { Text = IcontrolFilterResources.ViewBy_Units, Value = IcontrolFilterResources.ViewBy_Units });
                ddlViewBy.EnableViewState = true;
                ddlViewBy.DataBind();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateViewByList: "));
            }
            return ddlViewBy;
        }

        /// <summary>
        /// Creates the recent timeframe list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateTimeFrameList()
        {
            ddlRecentTimeFrame = new RadComboBox();
            try
            {
                ddlRecentTimeFrame.Items.Add(new RadComboBoxItem() { Text = IcontrolFilterResources.TimeFrame_Period, Value = IcontrolFilterResources.TimeFrame_Period });
                ddlRecentTimeFrame.Items.Add(new RadComboBoxItem() { Text = IcontrolFilterResources.TimeFrame_Quarter, Value = IcontrolFilterResources.TimeFrame_Quarter });
                ddlRecentTimeFrame.EnableViewState = true;
                ddlRecentTimeFrame.DataBind();
                ddlRecentTimeFrame.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateTimeFrameList: "));
            }
            return ddlRecentTimeFrame;
        }

        /// <summary>
        /// Creates the location hierarchy type list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateLocationHierarchyTypeList()
        {
            ddlHierarchyType = null;
            ddlHierarchyType = new RadComboBox();
            try
            {
                ddlHierarchyType.Items.Add(new RadComboBoxItem { Text = IcontrolFilterResources.CorporateHierarchy, Value = "1" });
                ddlHierarchyType.Items.Add(new RadComboBoxItem { Text = IcontrolFilterResources.CustomStoreGroupHierarchy, Value = "2" });
                ddlHierarchyType.EnableViewState = true;
                ddlHierarchyType.EnableViewState = true;
                ddlHierarchyType.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlHierarchyType_SelectedIndexChanged);
                ddlHierarchyType.AutoPostBack = true;
                ddlHierarchyType.DataBind();

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateLocationHierarchyTypeList: "));
            }

            return ddlHierarchyType;
        }

        /// <summary>
        /// Creates the product hierarchy type list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateProductHierarchyTypeList()
        {
            ddlHierarchyType = null;
            ddlHierarchyType = new RadComboBox();
            try
            {
                ddlHierarchyType.Items.Add(new RadComboBoxItem { Text = IcontrolFilterResources.CorporateHierarchy, Value = "1" });
                ddlHierarchyType.Items.Add(new RadComboBoxItem { Text = IcontrolFilterResources.CustomHierarchy, Value = "2" });
                ddlHierarchyType.EnableViewState = true;
                ddlHierarchyType.EnableViewState = true;
                ddlHierarchyType.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlHierarchyType_SelectedIndexChanged);
                ddlHierarchyType.AutoPostBack = true;
                ddlHierarchyType.DataBind();


            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateProductHierarchyTypeList: "));
            }

            return ddlHierarchyType;
        }

        /// <summary>
        /// Creates the no off weeks list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateNoOffWeeksList()
        {
            ddlNoOffWeeks = new RadComboBox();
            try
            {
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_4, Value = AppConstants.DDL_NO_OFF_WEEKS_4 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_5, Value = AppConstants.DDL_NO_OFF_WEEKS_5 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_6, Value = AppConstants.DDL_NO_OFF_WEEKS_6 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_7, Value = AppConstants.DDL_NO_OFF_WEEKS_7 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_8, Value = AppConstants.DDL_NO_OFF_WEEKS_8 });
                ddlNoOffWeeks.EnableViewState = true;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateNoOffWeeksList: "));
            }
            return ddlNoOffWeeks;
        }

        /// <summary>
        /// Creates the no off weeks list prior.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateNoOffWeeksPriorList()
        {
            ddlNoOffWeeks = new RadComboBox();
            try
            {
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_0, Value = AppConstants.DDL_NO_OFF_WEEKS_0 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_1, Value = AppConstants.DDL_NO_OFF_WEEKS_1 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_2, Value = AppConstants.DDL_NO_OFF_WEEKS_2 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_3, Value = AppConstants.DDL_NO_OFF_WEEKS_3 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_4, Value = AppConstants.DDL_NO_OFF_WEEKS_4 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_5, Value = AppConstants.DDL_NO_OFF_WEEKS_5 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_6, Value = AppConstants.DDL_NO_OFF_WEEKS_6 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_7, Value = AppConstants.DDL_NO_OFF_WEEKS_7 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_8, Value = AppConstants.DDL_NO_OFF_WEEKS_8 });
                ddlNoOffWeeks.EnableViewState = true;
                ddlNoOffWeeks.Items.Insert(0, new RadComboBoxItem { Text = IcontrolFilterResources.ddl_Select, Value = IcontrolFilterResources.ddl_Select });

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateNoOffWeeksPriorList: "));
            }
            return ddlNoOffWeeks;
        }

        /// <summary>
        /// Creates the no off weeks list post.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateNoOffWeeksPostList()
        {
            ddlNoOffWeeks = new RadComboBox();
            try
            {
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_0, Value = AppConstants.DDL_NO_OFF_WEEKS_0 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_1, Value = AppConstants.DDL_NO_OFF_WEEKS_1 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_2, Value = AppConstants.DDL_NO_OFF_WEEKS_2 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_3, Value = AppConstants.DDL_NO_OFF_WEEKS_3 });
                ddlNoOffWeeks.Items.Add(new RadComboBoxItem() { Text = AppConstants.DDL_NO_OFF_WEEKS_4, Value = AppConstants.DDL_NO_OFF_WEEKS_4 });
                ddlNoOffWeeks.EnableViewState = true;
                ddlNoOffWeeks.Items.Insert(0, new RadComboBoxItem { Text = IcontrolFilterResources.ddl_Select, Value = IcontrolFilterResources.ddl_Select });
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateNoOffWeeksPostList: "));
            }
            return ddlNoOffWeeks;
        }

        /// <summary>
        /// Creates the holiday list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateHolidayList(bool isSingleSelectionList = false)
        {
            ddlHoliday = new RadComboBox();
            try
            {
                dataAccessLayer = new DataLayer();
                this.dtHolidayAndYearDetails = null;
                this.dtHolidayAndYearDetails = dataAccessLayer.GetHolidayAndYearDetails();
                if (this.dtHolidayAndYearDetails != null && this.dtHolidayAndYearDetails.Rows.Count > 0)
                {
                    this.dtHolidayAndYearDetails.DefaultView.Sort = AppConstants.HOLIDAY_COLUMN + " ASC";
                    ddlHoliday.DataSource = this.dtHolidayAndYearDetails.DefaultView.ToTable(true, new string[] { AppConstants.HOLIDAY_ID, AppConstants.HOLIDAY_COLUMN });
                    ddlHoliday.DataValueField = AppConstants.HOLIDAY_ID;
                    ddlHoliday.DataTextField = AppConstants.HOLIDAY_COLUMN;
                    ddlHoliday.EnableViewState = true;
                    ddlHoliday.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ddlHoliday_SelectedIndexChanged);
                    ddlHoliday.AutoPostBack = true;
                    ddlHoliday.DataBind();
                }
                if (isSingleSelectionList)
                {
                    ddlHoliday.Items.Insert(0, new RadComboBoxItem { Text = IcontrolFilterResources.ddl_Select, Value = IcontrolFilterResources.ddl_Select });
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateHolidayList: "));
            }
            return ddlHoliday;
        }

        /// <summary>
        /// Binds the Year list.
        /// </summary>
        /// <param name="isSingleSelectionList"></param>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreateYearList()
        {
            ddlYear = new RadComboBox();
            try
            {
                ddlHoliday = (RadComboBox)this.ControlsParent.FindControl("Holiday");
                if (this.dtHolidayAndYearDetails != null && this.dtHolidayAndYearDetails.Rows.Count > 0)
                {
                    if (ddlHoliday == null)
                    {
                        this.dtHolidayAndYearDetails.DefaultView.Sort = AppConstants.HOLIDAY_YEAR_COLUMN + " ASC";
                        using (DataTable dtYear = this.dtHolidayAndYearDetails.DefaultView.ToTable(true, new string[] { AppConstants.HOLIDAY_YEAR_COLUMN }))
                        {
                            ddlYear.DataSource = dtYear;
                            ddlYear.DataValueField = AppConstants.HOLIDAY_YEAR_COLUMN;
                            ddlYear.DataTextField = AppConstants.HOLIDAY_YEAR_COLUMN;
                            ddlYear.DataBind();
                        }
                    }
                    ddlYear.EnableViewState = true;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateYearList: "));
            }
            return ddlYear;
        }

        /// <summary>
        /// Creates the pog list.
        /// </summary>
        /// <returns>RadComboBox.</returns>
        private RadComboBox CreatePogList()
        {
            ddlPog = new RadComboBox();
            try
            {
                ddlPog.Items.Add(new RadComboBoxItem() { Text = IcontrolFilterResources.ddl_PogAll, Value = "0" });
                ddlPog.Items.Add(new RadComboBoxItem() { Text = IcontrolFilterResources.ddl_PogYes, Value = "Y" });
                ddlPog.Items.Add(new RadComboBoxItem() { Text = IcontrolFilterResources.ddl_PogNo, Value = "N" });
                ddlPog.EnableViewState = true;
                ddlPog.DataBind();
                ddlPog.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreatePogList: "));
            }
            return ddlPog;
        }

        #endregion

        #region Private Page Methods

        /// <summary>
        /// Creates the filter control.
        /// </summary>
        /// <param name="dt">The dt.</param>
        private void CreateFilterControl(DataTable FilterDetails)
        {
            this.ControlsParent.Controls.Clear();
            try
            {
                this.dataAccessLayer = new DataLayer();

                if (FilterDetails.Rows.Count > 0)
                {

                    var result = FilterDetails.AsEnumerable().ToList().GroupBy(x => x.Field<string>("FILTERGROUP_NAME")).Select(g => new
                    {
                        Heading = g.Key,
                        HeadingId = g.Select(c => c.Field<string>("FILTERGROUP_ID")),
                        Items = g.Select(c => new
                        {
                            FilterGroupName = c.Field<string>("FILTERGROUP_NAME"),
                            FilterName = c.Field<string>("FILTER_NAME"),
                            FilterLabelName = c.Field<string>("FILTER_LABEL_NAME"),
                            ControlName = c.Field<string>("controlName"),
                            IsMultiselectORCalenderRequired = c.Field<bool>("IS_MULTISELECT_DATECONTROL"),
                            IdFilterDependency = c.Field<int>("DEPENDENCY"),
                            EventName = c.Field<string>("EVENT_NAME"),
                            IsRequired = c.Field<bool>("IS_REQUIRED"),
                            CssClassName = c.Field<string>("CSSCLASS_NAME"),
                            ParameterName = c.Field<string>("PARAMETER_NAME"),
                            BreadCrumbs = c.Field<string>("REPORT_HEADER") + " -> " + c.Field<string>("BREAD_CRUMB"),
                            ReportName = c.Field<string>("REPORT_NAME"),
                            ExtraParameters = c.Field<string>("EXTRA_PARAMETERS"),
                        })
                    });
                    if (result != null)
                    {
                        this.pageControlsDetails = result;
                        lblBreadCrumbs.Text = result.FirstOrDefault().Items.FirstOrDefault().BreadCrumbs.ToString();
                    }
                    foreach (var row in result)
                    {

                        if (row != null)
                        {
                            trlblHeading = new HtmlGenericControl("tr");
                            tdlblHeading = new HtmlGenericControl("td");

                            trlblHeading.Controls.Add(tdlblHeading);
                            ControlsParent.Controls.Add(trlblHeading);
                            lblHeading = new Label();
                            lblHeading.Text = row.Heading;
                            lblHeading.Attributes.Add(IcontrolFilterResources.Class, IcontrolFilterResources.heading);
                            tdlblHeading.Controls.Add(lblHeading);

                        }

                        foreach (var filter in row.Items)
                        {
                            //Create control for dropdown Control.
                            if (!string.IsNullOrEmpty(filter.ControlName) && (Convert.ToString(filter.ControlName).ToUpper() == AppConstants.INPUTCONTROLS_DROPDOWN.ToUpper()))
                            {
                                string MethodName = Convert.ToString(filter.EventName);
                                trlbl = new HtmlGenericControl("tr");
                                HtmlGenericControl tdlbl = new HtmlGenericControl("td");

                                Label lbl = new Label();
                                lbl.Text = Convert.ToString(filter.FilterLabelName);
                                lbl.Attributes.Add(IcontrolFilterResources.Class, IcontrolFilterResources.Label);
                                tdlbl.Controls.Add(lbl);
                                if (filter.IsRequired == true)
                                {
                                    Literal litrals = new Literal();
                                    litrals.Text = IcontrolFilterResources.literals;
                                    tdlbl.Controls.Add(litrals);
                                }
                                trlbl.Controls.Add(tdlbl);
                                if (!string.IsNullOrEmpty(filter.FilterLabelName))
                                {
                                    ControlsParent.Controls.Add(trlbl);
                                }
                                trddl = new HtmlGenericControl("tr");
                                tdddl = new HtmlGenericControl("td");
                                trddl.Controls.Add(tdddl);
                                this.ControlsParent.Controls.Add(trddl);

                                //Create control for multiselect dropdown.
                                if (filter.IsMultiselectORCalenderRequired)
                                {

                                    if (!string.IsNullOrEmpty(MethodName))
                                    {
                                        RadComboBox ddlMultiSelection = (RadComboBox)BindControlsDynamicaly(MethodName);
                                        ddlMultiSelection.ID = Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty));
                                        ddlMultiSelection.CheckBoxes = true;
                                        ddlMultiSelection.EnableLoadOnDemand = true;
                                        ddlMultiSelection.EnableCheckAllItemsCheckBox = true;
                                        ddlMultiSelection.Filter = RadComboBoxFilter.StartsWith;
                                        ddlMultiSelection.Localization.AllItemsCheckedString = "All";
                                        ddlMultiSelection.Localization.CheckAllString = "All";
                                        tdddl.Controls.Add(ddlMultiSelection);

                                        if (filter.IsRequired)
                                        {
                                            rfvddlMultiSelection = new RequiredFieldValidator();
                                            rfvddlMultiSelection.ID = "rfv" + Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty));
                                            rfvddlMultiSelection.ControlToValidate = Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty));
                                            rfvddlMultiSelection.ErrorMessage = filter.FilterLabelName + IcontrolFilterResources.RequiredMsgTextBox;
                                            rfvddlMultiSelection.ForeColor = System.Drawing.Color.Red;
                                            rfvddlMultiSelection.ValidationGroup = IcontrolFilterResources.ValidationGroup;
                                            rfvddlMultiSelection.Display = ValidatorDisplay.Dynamic;
                                            rfvddlMultiSelection.EnableClientScript = false;
                                            rfvddlMultiSelection.Enabled = true;
                                            rfvddlMultiSelection.Visible = true;
                                            tdddl.Controls.Add(rfvddlMultiSelection);
                                        }
                                    }

                                }
                                else
                                {
                                    if ((Convert.ToString(filter.ControlName).ToUpper() == AppConstants.INPUTCONTROLS_DROPDOWN.ToUpper()))
                                    {
                                        //Create dropdown control with single select  

                                        RadComboBox ddlSingleSelection = (RadComboBox)BindControlsDynamicaly(MethodName, true);
                                        ddlSingleSelection.ID = Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty));

                                        tdddl.Controls.Add(ddlSingleSelection);
                                        if (filter.IsRequired)
                                        {
                                            rfvddlSingleSelection = new RequiredFieldValidator();
                                            rfvddlSingleSelection.ID = "rfv" + Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty));
                                            rfvddlSingleSelection.ControlToValidate = Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty));
                                            rfvddlSingleSelection.ErrorMessage = filter.FilterLabelName + IcontrolFilterResources.RequiredMsgTextBox;
                                            rfvddlSingleSelection.ForeColor = System.Drawing.Color.Red;
                                            rfvddlSingleSelection.ValidationGroup = IcontrolFilterResources.ValidationGroup;
                                            rfvddlSingleSelection.Display = ValidatorDisplay.Dynamic;
                                            rfvddlSingleSelection.EnableClientScript = false;
                                            rfvddlSingleSelection.Enabled = true;
                                            rfvddlSingleSelection.Visible = true;
                                            if (ddlSingleSelection.Items.Count > 0)
                                            {
                                                if (ddlSingleSelection.Items[0].Text == IcontrolFilterResources.ddl_Select)
                                                {
                                                    rfvddlSingleSelection.InitialValue = IcontrolFilterResources.ddl_Select;
                                                }
                                            }

                                            tdddl.Controls.Add(rfvddlSingleSelection);
                                        }

                                    }
                                }
                                if (!string.IsNullOrEmpty(filter.CssClassName) && AnchorTagApplied == false)
                                {
                                    trddl.Attributes.Add(IcontrolFilterResources.Class, filter.CssClassName);
                                    trlbl.Attributes.Add(IcontrolFilterResources.Class, filter.CssClassName);
                                }
                                else if (!string.IsNullOrEmpty(filter.CssClassName) && AnchorTagApplied == true)
                                {
                                    trddl.Attributes.Add(IcontrolFilterResources.Class, filter.CssClassName + IcontrolFilterResources.Anchor);
                                    trlbl.Attributes.Add(IcontrolFilterResources.Class, filter.CssClassName + IcontrolFilterResources.Anchor);
                                }
                                else if (AnchorTagApplied == true)
                                {
                                    trddl.Attributes.Add(IcontrolFilterResources.Class, IcontrolFilterResources.Anchor);
                                    trlbl.Attributes.Add(IcontrolFilterResources.Class, IcontrolFilterResources.Anchor);
                                }
                            }
                            else if (!string.IsNullOrEmpty(filter.ControlName) && Convert.ToString(filter.ControlName).ToUpper() == AppConstants.INPUTCONTROLS_TEXTBOX.ToUpper())
                            {
                                HtmlGenericControl trlbl = new HtmlGenericControl("tr");
                                HtmlGenericControl tdlbl = new HtmlGenericControl("td");
                                trlbl.Controls.Add(tdlbl);

                                Label lblFilter = new Label();
                                lblFilter.Text = Convert.ToString(filter.FilterLabelName);
                                lblFilter.Attributes.Add(IcontrolFilterResources.Class, IcontrolFilterResources.Label);
                                tdlbl.Controls.Add(lblFilter);
                                if (filter.IsRequired == true)
                                {
                                    Literal litralsFilter = new Literal();
                                    litralsFilter.Text = IcontrolFilterResources.literals;
                                    tdlbl.Controls.Add(litralsFilter);

                                }
                                this.ControlsParent.Controls.Add(trlbl);

                                HtmlGenericControl trtxt = new HtmlGenericControl("tr");
                                HtmlGenericControl tdtxt = new HtmlGenericControl("td");

                                //if (filter.FilterName.Trim() == "From")
                                //{
                                //   MyDate ucMyDate =LoadControl("~/Controls/MyDate.ascx") as MyDate;
                                //   // ucMyDate.ID = filter.FilterName.Trim();                                 
                                //    tdtxt.Controls.Add(ucMyDate);
                                //    trtxt.Controls.Add(tdtxt);
                                //}
                                //else
                                //{
                                    TextBox txtBoxFilter = new TextBox();
                                    txtBoxFilter.ID = Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty));
                                    txtBoxFilter.ValidationGroup = IcontrolFilterResources.ValidationGroup;

                                    if (filter.IsMultiselectORCalenderRequired)
                                    {
                                        txtBoxFilter.Attributes.Add("onblur", "CheckEndDate(this)");
                                        txtBoxFilter.Attributes.Add("onfocus", "CheckEndDate(this)");

                                    }
                                    tdtxt.Controls.Add(txtBoxFilter);
                                    if (filter.IsMultiselectORCalenderRequired)
                                    {
                                        calExenderTextbox = new CalendarExtender();
                                        calExenderTextbox.ID = "cal_" + Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty));
                                        calExenderTextbox.PopupButtonID = Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty));
                                        calExenderTextbox.TargetControlID = Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty));
                                        calExenderTextbox.Format = "MM/dd/yyyy";
                                        tdtxt.Controls.Add(calExenderTextbox);
                                    }
                                    if (filter.IsRequired)
                                    {
                                        rfvTexbox = new RequiredFieldValidator();
                                        rfvTexbox.ID = "rfv" + Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty));
                                        rfvTexbox.ControlToValidate = Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty));
                                        rfvTexbox.ErrorMessage = filter.FilterLabelName + IcontrolFilterResources.RequiredMsgTextBox;
                                        rfvTexbox.ForeColor = System.Drawing.Color.Red;
                                        rfvTexbox.ValidationGroup = IcontrolFilterResources.ValidationGroup;
                                        rfvTexbox.Display = ValidatorDisplay.Dynamic;
                                        rfvTexbox.EnableClientScript = false;
                                        rfvTexbox.Enabled = true;
                                        rfvTexbox.Visible = true;
                                        tdtxt.Controls.Add(rfvTexbox);
                                    }

                                    bool is_enabled_vendorNo = true;
                                    if (!DataManager.IsCorporateUser(this.LoginUserName) && Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty)) == "VendorNumber")
                                    {
                                        if (this.LoginUserProfileDetails != null && (this.LoginUserProfileDetails != null && this.LoginUserProfileDetails.Length > 0))
                                        {
                                            if (this.LoginUserProfileName == IcontrolFilterResources.UserProfileVendor)
                                            {
                                                txtBoxFilter.Text = Convert.ToString(this.LoginUserProfileDetails[1]);
                                            }
                                            is_enabled_vendorNo = false;
                                        }
                                    }
                                    txtBoxFilter.Enabled = is_enabled_vendorNo;
                                    trtxt.Controls.Add(tdtxt);
                                    if (!string.IsNullOrEmpty(filter.CssClassName))
                                    {
                                        trlbl.Attributes.Add(IcontrolFilterResources.Class, filter.CssClassName);
                                        trtxt.Attributes.Add(IcontrolFilterResources.Class, filter.CssClassName);
                                    }
                                //}
                                this.ControlsParent.Controls.Add(trtxt);
                            }
                            else if (!string.IsNullOrEmpty(filter.ControlName) && Convert.ToString(filter.ControlName).ToUpper() == AppConstants.INPUTCONTROLS_ANCHOR.ToUpper())
                            {
                                HtmlGenericControl trAnchor = new HtmlGenericControl("tr");
                                HtmlGenericControl tdAnchor = new HtmlGenericControl("td");
                                tdAnchor.Style.Add("text-align", "right");
                                tdAnchor.Style.Add("padding-top", "10px");
                                trAnchor.Controls.Add(tdAnchor);

                                HtmlAnchor anchorFilters = new HtmlAnchor();
                                anchorFilters.InnerText = "";
                                anchorFilters.HRef = "#";
                                anchorFilters.ID = Convert.ToString(filter.FilterName);
                                anchorFilters.ClientIDMode = ClientIDMode.Static;
                                anchorFilters.Attributes.Add("runat", "sever");
                                tdAnchor.Controls.Add(anchorFilters);
                                this.ControlsParent.Controls.Add(trAnchor);
                                AnchorTagApplied = true;
                            }
                            else if (!string.IsNullOrEmpty(filter.ControlName) && Convert.ToString(filter.ControlName).ToUpper() == AppConstants.INPUTCONTROLS_RADIO.ToUpper())
                            {
                                HtmlGenericControl trRadio = new HtmlGenericControl("tr");
                                HtmlGenericControl tdRadio = new HtmlGenericControl("td");
                                trRadio.Controls.Add(tdRadio);
                                RadioButtonList rblDailyWeekly = new RadioButtonList();
                                rblDailyWeekly.ID = Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty));
                                rblDailyWeekly.ClientIDMode = ClientIDMode.Static;
                                rblDailyWeekly.Items.Add(new ListItem("Daily", "1"));
                                rblDailyWeekly.Items.Add(new ListItem("Weekly", "2"));
                                rblDailyWeekly.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
                                rblDailyWeekly.AutoPostBack = true;
                                rblDailyWeekly.SelectedIndexChanged += new EventHandler(rblDailyWeekly_SelectedIndexChanged);
                                rblDailyWeekly.SelectedValue = "2";
                                tdRadio.Controls.Add(rblDailyWeekly);
                                this.ControlsParent.Controls.Add(trRadio);
                            }
                        }
                    }
                }
                rfvValidorEnabled_Disabled();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateFilterControl: "));
            }
        }

        #region Required field Enable/Disable
        //Required field Enable/Disable based on hierarchy selected
        private void rfvValidorEnabled_Disabled()
        {
            RequiredFieldValidator rfvBanner = (RequiredFieldValidator)ControlsParent.FindControl("rfvLocBanner");
            RequiredFieldValidator rfvLocationStore = (RequiredFieldValidator)ControlsParent.FindControl("rfvLocationStore");
            RequiredFieldValidator rfvDepartment = (RequiredFieldValidator)ControlsParent.FindControl("rfvDepartment");
            RequiredFieldValidator rfvCategory = (RequiredFieldValidator)ControlsParent.FindControl("rfvCategory");
            RequiredFieldValidator rfvHierarchyName = (RequiredFieldValidator)ControlsParent.FindControl("rfvHierarchyName");

            RadComboBox ddllocationHierarchyType = (RadComboBox)ControlsParent.FindControl("HierarchyLocation");
            RadComboBox ddlProductHierarchyType = (RadComboBox)ControlsParent.FindControl("Hierarchy");

            if (ddllocationHierarchyType != null)
            {
                if (ddllocationHierarchyType.SelectedItem.Text == IcontrolFilterResources.CustomStoreGroupHierarchy)
                {
                    if (rfvBanner != null && rfvLocationStore != null)
                    {
                        rfvBanner.Enabled = false;
                        rfvLocationStore.Enabled = true;
                    }
                }
                else
                {
                    if (rfvBanner != null && rfvLocationStore != null)
                    {
                        rfvBanner.Enabled = true;
                        rfvLocationStore.Enabled = false;
                    }
                }
            }
            if (ddlProductHierarchyType != null)
            {

                if (ddlProductHierarchyType.SelectedItem.Text == IcontrolFilterResources.CustomHierarchy)
                {
                    if (rfvDepartment != null)
                    {
                        rfvDepartment.Enabled = false;
                    }
                    else if (rfvCategory != null)
                    {
                        rfvCategory.Enabled = false;
                    }
                    if (rfvHierarchyName != null)
                    {
                        rfvHierarchyName.Enabled = true;
                    }
                }
                else
                {

                    if (rfvDepartment != null)
                    {
                        rfvDepartment.Enabled = true;
                    }
                    else if (rfvCategory != null)
                    {
                        rfvCategory.Enabled = true;
                    }
                    if (rfvHierarchyName != null)
                    {
                        rfvHierarchyName.Enabled = false;
                    }
                }
            }
        }
        #endregion

        #region INDEX CHANGE EVENTS

        #region  INDEX CHANGE EVENTS LOCATION
        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlHierarchyType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlHierarchyType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox ddlLocationHierarchy = (RadComboBox)sender;
            TextBox txtVendorNumber = (TextBox)ControlsParent.FindControl("VendorNumber");

            if (txtVendorNumber != null)
            {
                txtVendorNumber.Enabled = true;
                if (ddlLocationHierarchy.SelectedItem.Text.Trim().Replace(" ", "") == "CustomStoreGroup" || ddlLocationHierarchy.SelectedItem.Text.Trim().Replace(" ", "") == "CustomHierarchy")
                {
                    txtVendorNumber.Enabled = false;
                    txtVendorNumber.Text = "";
                }
            }

            rfvValidorEnabled_Disabled();
        }

        #region Location Banner Change Event
        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlBanner control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlBanner_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            //Ddl select index change event for binding dependent Dropdownlist.
            try
            {

                ddlBanner = (RadComboBox)sender;
                //For Binding category based on Department
                ddlSubBanner = (RadComboBox)ControlsParent.FindControl("LocSubBanner");

                if (ddlBanner.CheckedItems.Count() == 0)
                {
                    ddlSubBanner.Items.Clear();
                }

                foreach (RadComboBoxItem item in ddlBanner.CheckedItems)
                {
                    dataAccessLayer = new DataLayer();
                    if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                    {
                        string selectedBanner = AppManager.GetSelectedItemsWithQuotes(ddlBanner);
                        DataRow[] subBannerDetails = AppManager.GetNextLocationFilterItemsList_Corporate(this.dtBannerAndSubBannerDetails, selectedBanner);
                        if (subBannerDetails != null && subBannerDetails.Count() > 0)
                        {
                            DataTable dtSubBanners = subBannerDetails.CopyToDataTable();
                            dtSubBanners.DefaultView.Sort = AppConstants.SUBBANNER_COLUMN + " ASC";

                            if (ddlSubBanner != null)
                            {
                                ddlSubBanner.DataSource = dtSubBanners.DefaultView.ToTable(true, AppConstants.SUBBANNER_COLUMN);
                                ddlSubBanner.DataTextField = AppConstants.SUBBANNER_COLUMN;
                                ddlSubBanner.DataValueField = AppConstants.SUBBANNER_COLUMN;
                                ddlSubBanner.Filter = RadComboBoxFilter.StartsWith;
                                ddlSubBanner.AllowCustomText = true;
                                ddlSubBanner.CheckBoxes = true;
                                ddlSubBanner.EnableCheckAllItemsCheckBox = true;
                                ddlSubBanner.DataBind();

                                foreach (RadComboBoxItem SubBanner in ddlSubBanner.Items)
                                {
                                    SubBanner.Checked = true;
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlBanner_SelectedIndexChanged: "));
            }
        }
        #endregion

        #region Location Source Banner and Subbanner Change Event

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlLoctionHierarchySource control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlLoctionHierarchySourceBanner_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //Ddl select index change event for binding dependent Dropdownlist.
            try
            {

                string selectedBanners = string.Empty;
                string selectedLoctionSourceSubBanners = string.Empty;
                ddlLoctionSourceBanner = (RadComboBox)sender;

                ddlLoctionSourceSubBanner = (RadComboBox)ControlsParent.FindControl("LocHierSrcSubBanner");

                ddlLoctionSourceStore = (RadComboBox)ControlsParent.FindControl("SrcStore");

                if (ddlLoctionSourceBanner.CheckedItems.Count() == 0)
                {
                    ddlLoctionSourceSubBanner.Items.Clear();
                }
                else
                {
                    dataAccessLayer = new DataLayer();
                    if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                    {
                        selectedBanners = AppManager.GetSelectedItemsWithQuotes(ddlLoctionSourceBanner);
                        if (!string.IsNullOrEmpty(selectedBanners))
                        {
                            DataRow[] subBannerDetails = AppManager.GetNextLocationFilterItemsList_Corporate(this.dtBannerAndSubBannerDetails, selectedBanners);
                            if (subBannerDetails != null && subBannerDetails.Count() > 0)
                            {
                                DataTable dtSubBanners = subBannerDetails.CopyToDataTable();
                                dtSubBanners.DefaultView.Sort = AppConstants.SUBBANNER_COLUMN + " ASC";

                                if (ddlLoctionSourceSubBanner != null)
                                {
                                    ddlLoctionSourceSubBanner.DataSource = dtSubBanners.DefaultView.ToTable(true, AppConstants.SUBBANNER_COLUMN);
                                    ddlLoctionSourceSubBanner.DataTextField = AppConstants.SUBBANNER_COLUMN;
                                    ddlLoctionSourceSubBanner.DataValueField = AppConstants.SUBBANNER_COLUMN;
                                    ddlLoctionSourceSubBanner.Filter = RadComboBoxFilter.StartsWith;
                                    ddlLoctionSourceSubBanner.AllowCustomText = true;
                                    ddlLoctionSourceSubBanner.CheckBoxes = true;
                                    ddlLoctionSourceSubBanner.EnableCheckAllItemsCheckBox = true;
                                    ddlLoctionSourceSubBanner.DataBind();

                                    foreach (RadComboBoxItem SubBanner in ddlLoctionSourceSubBanner.Items)
                                    {
                                        SubBanner.Checked = true;
                                    }
                                    selectedLoctionSourceSubBanners = AppManager.GetSelectedItemsWithQuotes(ddlLoctionSourceSubBanner);
                                }
                            }
                        }
                    }
                }
                if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                {

                    if (!string.IsNullOrEmpty(selectedLoctionSourceSubBanners))
                    {
                        DataRow[] storeDetails = AppManager.GetNextLocationFilterItemsList_Corporate(this.dtBannerAndSubBannerDetails, selectedBanners, selectedLoctionSourceSubBanners);
                        if (storeDetails != null && storeDetails.Count() > 0)
                        {
                            DataTable dtStore = storeDetails.CopyToDataTable();
                            dtStore.DefaultView.Sort = AppConstants.LOC_SOURCE_STORE_COLUMN + " ASC";
                            this.ddlLoctionSourceStore.DataSource = dtStore.DefaultView.ToTable(true, AppConstants.LOC_SOURCE_STORE_COLUMN);
                            this.ddlLoctionSourceStore.DataTextField = AppConstants.LOC_SOURCE_STORE_COLUMN;
                            this.ddlLoctionSourceStore.DataValueField = AppConstants.LOC_SOURCE_STORE_COLUMN;
                            this.ddlLoctionSourceStore.DataBind();

                            if (this.ddlLoctionSourceStore.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemLoctionSourceStore in this.ddlLoctionSourceStore.Items)
                                {
                                    itemLoctionSourceStore.Checked = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (this.ddlLoctionSourceStore != null)
                        {
                            this.ddlLoctionSourceStore.Items.Clear();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlLoctionHierarchySourceBanner_SelectedIndexChanged: "));
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlLoctionSourceSubBanner control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlLoctionSourceSubBanner_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //Ddl select index change event for binding dependent Dropdownlist.
            try
            {
                ddlLoctionSourceSubBanner = (RadComboBox)sender;
                ddlLoctionSourceBanner = (RadComboBox)ControlsParent.FindControl("LocHierSrcBanner");
                ddlLoctionSourceStore = (RadComboBox)ControlsParent.FindControl("SrcStore");

                if (ddlLoctionSourceSubBanner.CheckedItems.Count() == 0)
                {
                    ddlLoctionSourceStore.Items.Clear();
                }
                else
                {
                    dataAccessLayer = new DataLayer();
                    if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                    {
                        string selectedBanners = AppManager.GetSelectedItemsWithQuotes(ddlLoctionSourceBanner);
                        string selectedSubBanners = AppManager.GetSelectedItemsWithQuotes(ddlLoctionSourceSubBanner);
                        DataRow[] storeDetails = AppManager.GetNextLocationFilterItemsList_Corporate(this.dtBannerAndSubBannerDetails, selectedBanners, selectedSubBanners);
                        if (storeDetails != null && storeDetails.Count() > 0)
                        {
                            DataTable dtSourceStore = storeDetails.CopyToDataTable();
                            dtSourceStore.DefaultView.Sort = AppConstants.LOC_SOURCE_STORE_COLUMN + " ASC";
                            if (ddlLoctionSourceStore != null)
                            {
                                ddlLoctionSourceStore.DataSource = dtSourceStore.DefaultView.ToTable(true, AppConstants.LOC_SOURCE_STORE_COLUMN);
                                ddlLoctionSourceStore.DataTextField = AppConstants.LOC_SOURCE_STORE_COLUMN;
                                ddlLoctionSourceStore.DataValueField = AppConstants.LOC_SOURCE_STORE_COLUMN;
                                ddlLoctionSourceStore.Filter = RadComboBoxFilter.StartsWith;
                                ddlLoctionSourceStore.AllowCustomText = true;
                                ddlLoctionSourceStore.CheckBoxes = true;
                                ddlLoctionSourceStore.EnableCheckAllItemsCheckBox = true;
                                ddlLoctionSourceStore.DataBind();

                                foreach (RadComboBoxItem SubBanner in ddlLoctionSourceStore.Items)
                                {
                                    SubBanner.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlLoctionSourceSubBanner_SelectedIndexChanged: "));
            }
        }
        #endregion

        #region Location Diestination Banner and Subbanner Change Event
        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlLoctionHierarchyDestination control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlLoctionHierarchyDestinationBanner_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //Ddl select index change event for binding dependent Dropdownlist.
            try
            {
                string selectedLoctionDestinationSubBanners = string.Empty;
                ddlLoctionDestinationBanner = (RadComboBox)sender;

                ddlLoctionDestinationBanner = (RadComboBox)ControlsParent.FindControl("LocHierDesBanner");
                ddlLoctionDestinationSubBanner = (RadComboBox)ControlsParent.FindControl("LocHierDesSubBanner");

                ddlLoctionDestinationStore = (RadComboBox)ControlsParent.FindControl("DesStore");

                if (ddlLoctionDestinationBanner.CheckedItems.Count() == 0)
                {
                    ddlLoctionDestinationSubBanner.Items.Clear();
                }

                foreach (RadComboBoxItem item in ddlLoctionDestinationBanner.CheckedItems)
                {
                    dataAccessLayer = new DataLayer();
                    if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                    {
                        string selectedBanners = AppManager.GetSelectedItemsWithQuotes(ddlLoctionDestinationBanner);

                        DataRow[] subBannerDetails = AppManager.GetNextLocationFilterItemsList_Corporate(this.dtBannerAndSubBannerDetails, selectedBanners);
                        if (subBannerDetails != null && subBannerDetails.Count() > 0)
                        {
                            DataTable dtSubBanner = subBannerDetails.CopyToDataTable();
                            dtSubBanner.DefaultView.Sort = AppConstants.SUBBANNER_COLUMN + " ASC";

                            if (ddlLoctionDestinationSubBanner != null)
                            {
                                ddlLoctionDestinationSubBanner.DataSource = dtSubBanner.DefaultView.ToTable(true, AppConstants.SUBBANNER_COLUMN);
                                ddlLoctionDestinationSubBanner.DataTextField = AppConstants.SUBBANNER_COLUMN;
                                ddlLoctionDestinationSubBanner.DataValueField = AppConstants.SUBBANNER_COLUMN;
                                ddlLoctionDestinationSubBanner.Filter = RadComboBoxFilter.StartsWith;
                                ddlLoctionDestinationSubBanner.AllowCustomText = true;
                                ddlLoctionDestinationSubBanner.CheckBoxes = true;
                                ddlLoctionDestinationSubBanner.EnableCheckAllItemsCheckBox = true;
                                ddlLoctionDestinationSubBanner.DataBind();

                                foreach (RadComboBoxItem SubBanner in ddlLoctionDestinationSubBanner.Items)
                                {
                                    SubBanner.Checked = true;
                                }
                                selectedLoctionDestinationSubBanners = AppManager.GetSelectedItemsWithQuotes(ddlLoctionDestinationSubBanner);
                            }
                        }
                    }
                }

                if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                {

                    if (!string.IsNullOrEmpty(selectedLoctionDestinationSubBanners))
                    {
                        string selectedBanners = AppManager.GetSelectedItemsWithQuotes(ddlLoctionDestinationBanner);
                        DataRow[] storeDetails = AppManager.GetNextLocationFilterItemsList_Corporate(this.dtBannerAndSubBannerDetails, selectedBanners, selectedLoctionDestinationSubBanners);
                        if (storeDetails != null && storeDetails.Count() > 0)
                        {
                            DataTable dtDestinationStore = storeDetails.CopyToDataTable();
                            dtDestinationStore.DefaultView.Sort = AppConstants.LOC_DESTINATION_STORE_COLUMN + " ASC";

                            this.ddlLoctionDestinationStore.DataSource = dtDestinationStore.DefaultView.ToTable(true, AppConstants.LOC_DESTINATION_STORE_COLUMN);
                            this.ddlLoctionDestinationStore.DataTextField = AppConstants.LOC_DESTINATION_STORE_COLUMN;
                            this.ddlLoctionDestinationStore.DataValueField = AppConstants.LOC_DESTINATION_STORE_COLUMN;
                            this.ddlLoctionDestinationStore.DataBind();

                            if (this.ddlLoctionDestinationStore.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemLoctionDestinationStore in this.ddlLoctionDestinationStore.Items)
                                {
                                    itemLoctionDestinationStore.Checked = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (this.ddlLoctionDestinationStore != null)
                        {
                            this.ddlLoctionDestinationStore.Items.Clear();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlLoctionHierarchyDestinationBanner_SelectedIndexChanged: "));
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlLoctionDestinationSubBanner control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlLoctionDestinationSubBanner_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //Ddl select index change event for binding dependent Dropdownlist.
            try
            {
                ddlLoctionDestinationSubBanner = (RadComboBox)sender;

                ddlLoctionDestinationBanner = (RadComboBox)ControlsParent.FindControl("LocHierDesBanner");
                ddlLoctionDestinationSubBanner = (RadComboBox)ControlsParent.FindControl("LocHierDesSubBanner");
                ddlLoctionDestinationStore = (RadComboBox)ControlsParent.FindControl("DesStore");

                if (ddlLoctionDestinationSubBanner.CheckedItems.Count() == 0)
                {
                    ddlLoctionDestinationStore.Items.Clear();
                }

                foreach (RadComboBoxItem item in ddlLoctionDestinationSubBanner.CheckedItems)
                {
                    dataAccessLayer = new DataLayer();
                    if (this.dtBannerAndSubBannerDetails != null && this.dtBannerAndSubBannerDetails.Rows.Count > 0)
                    {
                        string selectedSubBanners = AppManager.GetSelectedItemsWithQuotes(ddlLoctionDestinationSubBanner);
                        string selectedBanners = AppManager.GetSelectedItemsWithQuotes(ddlLoctionDestinationBanner);
                        DataRow[] storeDetails = AppManager.GetNextLocationFilterItemsList_Corporate(this.dtBannerAndSubBannerDetails, selectedBanners, selectedSubBanners);
                        if (storeDetails != null && storeDetails.Count() > 0)
                        {
                            DataTable dtDestinationStore = storeDetails.CopyToDataTable();
                            dtDestinationStore.DefaultView.Sort = AppConstants.LOC_DESTINATION_STORE_COLUMN + " ASC";

                            if (ddlLoctionDestinationStore != null)
                            {
                                ddlLoctionDestinationStore.DataSource = dtDestinationStore.DefaultView.ToTable(true, AppConstants.LOC_DESTINATION_STORE_COLUMN);
                                ddlLoctionDestinationStore.DataTextField = AppConstants.LOC_DESTINATION_STORE_COLUMN;
                                ddlLoctionDestinationStore.DataValueField = AppConstants.LOC_DESTINATION_STORE_COLUMN;
                                ddlLoctionDestinationStore.Filter = RadComboBoxFilter.StartsWith;
                                ddlLoctionDestinationStore.AllowCustomText = true;
                                ddlLoctionDestinationStore.CheckBoxes = true;
                                ddlLoctionDestinationStore.EnableCheckAllItemsCheckBox = true;
                                ddlLoctionDestinationStore.DataBind();

                                foreach (RadComboBoxItem LoctionDestinationStore in ddlLoctionDestinationStore.Items)
                                {
                                    LoctionDestinationStore.Checked = true;
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlLoctionDestinationSubBanner_SelectedIndexChanged: "));
            }
        }

        #endregion

        #region Location Custom Store Select Index change

        /// <summary>
        /// Handles the SelectedIndexChanged event of the LocationStore control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlLocationStore_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                ddlCustomHierarchyLevel1 = null;
                ddlCustomHierarchyLevel2 = null;
                ddlCustomHierarchyLevel3 = null;

                ddlLocationStore = (RadComboBox)sender;

                //For Binding CustomHierarchyLevel1 based on store
                ddlCustomHierarchyLevel1 = (RadComboBox)ControlsParent.FindControl("LocationCustom1");

                //For Binding CustomHierarchyLevel2 based on store
                ddlCustomHierarchyLevel2 = (RadComboBox)ControlsParent.FindControl("LocationCustom2");

                //For Binding CustomHierarchyLevel3 based on store
                ddlCustomHierarchyLevel3 = (RadComboBox)ControlsParent.FindControl("LocationCustom3");


                string selectedStores = string.Empty;

                if (ddlLocationStore.CheckBoxes)
                {
                    foreach (RadComboBoxItem itemLocationStore in ddlLocationStore.CheckedItems)
                    {
                        itemLocationStore.Checked = true;
                    }
                }
                selectedStores = AppManager.GetSelectedItemsWithQuotes(this.ddlLocationStore);
                this.BindLocationCustomLevel1Details(selectedStores);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlLocationStore_SelectedIndexChanged: "));
            }
        }

        /// <summary>
        ///  Handles the SelectedIndexChanged event of the location Custom Hierarchy Level1 control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlLocationCustomHierarchyLevel1_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                ddlCustomHierarchyLevel2 = null;
                ddlCustomHierarchyLevel3 = null;

                ddlCustomHierarchyLevel1 = (RadComboBox)sender;

                //For Binding CustomHierarchyLevel2 based on store
                ddlCustomHierarchyLevel2 = (RadComboBox)ControlsParent.FindControl("LocationCustom2");

                //For Binding CustomHierarchyLevel3 based on store
                ddlCustomHierarchyLevel3 = (RadComboBox)ControlsParent.FindControl("LocationCustom3");


                string selecteditemLocationCustomHierarchyLevel1 = string.Empty;

                //if blocks handeles store location multiple selection list
                if (ddlCustomHierarchyLevel1.CheckBoxes)
                {
                    foreach (RadComboBoxItem itemCustomHierarchyLevel1 in ddlCustomHierarchyLevel1.CheckedItems)
                    {
                        itemCustomHierarchyLevel1.Checked = true;
                    }
                }
                selecteditemLocationCustomHierarchyLevel1 = AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel1);
                this.GetLocationCustomLevel2DetailsUsingLocationCustomLevel1(AppManager.GetSelectedItemsWithQuotes(ddlLocationStore), selecteditemLocationCustomHierarchyLevel1);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlLocationCustomHierarchyLevel1_SelectedIndexChanged: "));
            }
        }

        /// <summary>
        ///  Handles the SelectedIndexChanged event of the location Custom Hierarchy Level2 control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlLocationCustomHierarchyLevel2_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {

                ddlCustomHierarchyLevel3 = null;

                ddlCustomHierarchyLevel2 = (RadComboBox)sender;

                //For Binding CustomHierarchyLevel3 based on store
                ddlCustomHierarchyLevel3 = (RadComboBox)ControlsParent.FindControl("LocationCustom3");

                string selecteditemLocationCustomHierarchyLevel2 = string.Empty;

                //if blocks handeles store location multiple selection list
                if (ddlCustomHierarchyLevel2.CheckBoxes)
                {
                    foreach (RadComboBoxItem itemCustomHierarchyLevel2 in ddlCustomHierarchyLevel2.CheckedItems)
                    {
                        itemCustomHierarchyLevel2.Checked = true;
                    }
                }
                selecteditemLocationCustomHierarchyLevel2 = AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel2);
                this.GetLocationCustomLevel3DetailsUsingLocationCustomLevel2(AppManager.GetSelectedItemsWithQuotes(ddlLocationStore), AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel1), selecteditemLocationCustomHierarchyLevel2);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlLocationCustomHierarchyLevel2_SelectedIndexChanged: "));
            }
        }

        #endregion

        /// <summary>
        /// Handles the SelectedIndexChanged event of the Holiday control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlHoliday_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //Ddl select index change event for binding dependent Dropdownlist.
            try
            {
                dtYearDetails = null;
                ddlHoliday = (RadComboBox)sender;

                //For Binding category based on Department
                ddlYear = (RadComboBox)ControlsParent.FindControl("FiscalYear");

                dtYearDetails = new DataTable();
                dtYearDetails.Columns.Add(AppConstants.HOLIDAY_ID, typeof(int));
                dtYearDetails.Columns.Add(AppConstants.HOLIDAY_YEAR_COLUMN, typeof(string));


                if (ddlHoliday.SelectedIndex > 0)
                {
                    dataAccessLayer = new DataLayer();
                    DataRow[] YearDetails = this.dtHolidayAndYearDetails.Select(AppConstants.HOLIDAY_ID + "=" + Convert.ToInt16(ddlHoliday.SelectedValue));
                    if (YearDetails != null && YearDetails.Count() > 0)
                    {
                        for (int i = 0; i < YearDetails.Count(); i++)
                        {
                            dtYearDetails.Rows.Add(YearDetails[i][AppConstants.HOLIDAY_ID], YearDetails[i][AppConstants.HOLIDAY_YEAR_COLUMN]);
                        }
                    }
                }

                dtYearDetails.DefaultView.Sort = AppConstants.HOLIDAY_YEAR_COLUMN + " ASC";
                this.dtYearDetails = dtYearDetails.DefaultView.ToTable(true, new string[] { AppConstants.HOLIDAY_YEAR_COLUMN, AppConstants.HOLIDAY_ID });
                if (ddlYear != null && dtYearDetails != null && dtYearDetails.Rows.Count > 0)
                {
                    ddlYear.DataSource = dtYearDetails;
                    ddlYear.DataTextField = AppConstants.HOLIDAY_YEAR_COLUMN;
                    ddlYear.DataValueField = AppConstants.HOLIDAY_ID;
                    ddlYear.DataBind();
                    ddlYear.Items.Insert(0, new RadComboBoxItem { Text = IcontrolFilterResources.ddl_Select, Value = IcontrolFilterResources.ddl_Select });
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlHoliday_SelectedIndexChanged: "));
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the Dailyweekly control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">instance containing the event data.</param>
        private void rblDailyWeekly_SelectedIndexChanged(object sender, EventArgs e)
        {

            RadioButtonList rblDailyWeekly = (RadioButtonList)sender;
            //for location & Product section
            RadComboBox ddlLocationHeirerchy = (RadComboBox)ControlsParent.FindControl("HierarchyLocation");
            RadComboBox ddlProductHierarchy = (RadComboBox)this.ControlsParent.FindControl("Hierarchy");
            TextBox txtVendorNumber = (TextBox)this.ControlsParent.FindControl("VendorNumber");
            if (!DataManager.IsCorporateUser(this.LoginUserName))
            {
                if (txtVendorNumber != null)
                {
                    txtVendorNumber.Enabled = false;
                }
            }
            if (rblDailyWeekly.SelectedValue == "2")
            {
                if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_NO_SCAN_LAST_X_WEEKS)
                {
                    if (ddlLocationHeirerchy != null)
                    {
                        ddlLocationHeirerchy.SelectedIndex = 0;
                        ddlLocationHeirerchy.Enabled = false;

                    }
                    if (ddlProductHierarchy != null)
                    {
                        ddlProductHierarchy.SelectedIndex = 0;
                        ddlProductHierarchy.Enabled = true;
                    }
                }
                else
                {
                    if (ddlLocationHeirerchy != null)
                    {
                        ddlLocationHeirerchy.SelectedIndex = 0;
                        ddlLocationHeirerchy.Enabled = false;

                    }
                    if (ddlProductHierarchy != null)
                    {
                        ddlProductHierarchy.SelectedIndex = 0;
                        ddlProductHierarchy.Enabled = false;
                    }
                }
            }
            else
            {
                if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_NO_SCAN_LAST_X_WEEKS)
                {
                    if (ddlLocationHeirerchy != null)
                    {
                        ddlLocationHeirerchy.SelectedIndex = 0;
                        ddlLocationHeirerchy.Enabled = true;
                    }
                    if (ddlProductHierarchy != null)
                    {
                        ddlProductHierarchy.SelectedIndex = 0;
                        ddlProductHierarchy.Enabled = false;
                    }
                }
                else
                {
                    if (ddlLocationHeirerchy != null)
                    {
                        ddlLocationHeirerchy.SelectedIndex = 0;
                        ddlLocationHeirerchy.Enabled = true;
                    }
                    if (ddlProductHierarchy != null)
                    {
                        ddlProductHierarchy.SelectedIndex = 0;
                        ddlProductHierarchy.Enabled = true;
                    }
                }
            }

        }
        #endregion

        #region INDEX CHANGE EVENTS PRODUCT

        #region Index Change Events Product Corporate

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlDepartment control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlDepartment_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            try
            {
                ddlDepartment = null;
                ddlSubCategory = null;
                ddlSegment = null;
                ddlBrand = null;
                ddlProductSize = null;

                ddlDepartment = (RadComboBox)sender;


                //For Binding category based on Department
                ddlCategory = (RadComboBox)ControlsParent.FindControl("Category");

                //For Binding subcategory based on category
                ddlSubCategory = (RadComboBox)ControlsParent.FindControl("SubCategory");

                //For Binding Segment
                ddlSegment = (RadComboBox)ControlsParent.FindControl("Segment");

                //For Binding Product Brand
                ddlBrand = (RadComboBox)ControlsParent.FindControl("ProductBrand");

                //For Binding Product Size
                ddlProductSize = (RadComboBox)ControlsParent.FindControl("ProProductSizeDesc");

                //if blocks handeles Department multiple selection list
                if (ddlDepartment.CheckBoxes)
                {
                    foreach (RadComboBoxItem itemDepartment in ddlDepartment.CheckedItems)
                    {
                        itemDepartment.Checked = true;
                    }
                }

                string selectedDepartments = AppManager.GetSelectedItemsWithQuotes(ddlDepartment);

                this.BindCategoryDetails(selectedDepartments);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlDepartment_SelectedIndexChanged: "));
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlCategory control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlCategory_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //Ddl select index change event for binding dependent Dropdownlist on Category.
            try
            {
                ddlSubCategory = null;
                ddlSegment = null;
                ddlBrand = null;
                ddlProductSize = null;
                ddlCategory = (RadComboBox)sender;

                //For Binding subcategory based on category
                ddlSubCategory = (RadComboBox)ControlsParent.FindControl("SubCategory");

                //For Binding Segment
                ddlSegment = (RadComboBox)ControlsParent.FindControl("Segment");

                //For Binding Product Brand
                ddlBrand = (RadComboBox)ControlsParent.FindControl("ProductBrand");

                //For Binding Product Size
                ddlProductSize = (RadComboBox)ControlsParent.FindControl("ProProductSizeDesc");

                //if blocks handeles category multiple selection list
                if (ddlCategory.CheckBoxes)
                {
                    foreach (RadComboBoxItem itemCategory in ddlCategory.CheckedItems)
                    {
                        itemCategory.Checked = true;
                    }
                }

                string selectedDepartments = AppManager.GetSelectedItemsWithQuotes(ddlDepartment);
                string selectedCategories = AppManager.GetSelectedItemsWithQuotes(ddlCategory);
                this.GetSubCategoryDetailsUsingCategory(selectedDepartments, selectedCategories);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlCategory_SelectedIndexChanged: "));
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the SubCategory control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlSubCategory_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //Ddl select index change event for binding dependent Dropdownlist on SubCategory.
            try
            {
                ddlSegment = null;
                ddlBrand = null;

                ddlSubCategory = (RadComboBox)sender;

                ddlDepartment = (RadComboBox)ControlsParent.FindControl("Department");

                //For Binding category based on Department
                ddlCategory = (RadComboBox)ControlsParent.FindControl("Category");

                //For Binding Segment
                ddlSegment = (RadComboBox)ControlsParent.FindControl("Segment");

                //For Binding Brand
                ddlBrand = (RadComboBox)ControlsParent.FindControl("ProductBrand");

                //For Binding Product Size
                ddlProductSize = (RadComboBox)ControlsParent.FindControl("ProProductSizeDesc");

                //if blocks handeles category multiple selection list
                if (ddlSubCategory.CheckBoxes)
                {
                    foreach (RadComboBoxItem itemSubCategory in ddlSubCategory.CheckedItems)
                    {
                        itemSubCategory.Checked = true;
                    }
                }
                string selectedCategories = AppManager.GetSelectedItemsWithQuotes(ddlCategory);
                string selectedSubCategory = AppManager.GetSelectedItemsWithQuotes(ddlSubCategory);
                this.GetSegmentDetailsUsingSubCategory(AppManager.GetSelectedItemsWithQuotes(ddlDepartment), selectedCategories, selectedSubCategory);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlSubCategory_SelectedIndexChanged: "));
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the Segment control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlSegment_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //Ddl select index change event for binding dependent Dropdownlist on Category.
            try
            {
                ddlSegment = null;
                ddlBrand = null;

                ddlSegment = (RadComboBox)sender;

                ddlDepartment = (RadComboBox)ControlsParent.FindControl("Department");

                //For Binding category based on Department
                ddlCategory = (RadComboBox)ControlsParent.FindControl("Category");

                //For Binding sub category based on Category
                ddlSubCategory = (RadComboBox)ControlsParent.FindControl("SubCategory");

                //For Binding Segment
                ddlBrand = (RadComboBox)ControlsParent.FindControl("ProductBrand");

                //For Binding Product Size
                ddlProductSize = (RadComboBox)ControlsParent.FindControl("ProProductSizeDesc");

                //if blocks handeles category multiple selection list
                if (ddlSegment.CheckBoxes)
                {
                    foreach (RadComboBoxItem itemSegment in ddlSegment.CheckedItems)
                    {
                        itemSegment.Checked = true;
                    }
                }

                string selectedsegment = AppManager.GetSelectedItemsWithQuotes(ddlSegment);
                this.GetBrandDetailsUsingSegment(AppManager.GetSelectedItemsWithQuotes(ddlDepartment), AppManager.GetSelectedItemsWithQuotes(ddlCategory), AppManager.GetSelectedItemsWithQuotes(ddlSubCategory), selectedsegment);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlSegment_SelectedIndexChanged: "));
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlCorporateBrand control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlCorporateBrand_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //Ddl select index change event for binding dependent Dropdownlist on Category.
            try
            {

                ddlBrand = null;
                ddlProductSize = null;
                ddlBrand = (RadComboBox)sender;

                ddlDepartment = (RadComboBox)ControlsParent.FindControl("Department");

                //For Binding category based on Department
                ddlCategory = (RadComboBox)ControlsParent.FindControl("Category");

                //For Binding sub category based on Category
                ddlSubCategory = (RadComboBox)ControlsParent.FindControl("SubCategory");

                //For Binding Product Size
                ddlProductSize = (RadComboBox)ControlsParent.FindControl("ProProductSizeDesc");

                ddlSegment = (RadComboBox)ControlsParent.FindControl("Segment");

                //if blocks handeles category multiple selection list
                if (ddlBrand.CheckBoxes)
                {
                    foreach (RadComboBoxItem itemBrand in ddlBrand.CheckedItems)
                    {
                        itemBrand.Checked = true;
                    }
                }

                this.GetProductSizeDetailsUsingBrand(AppManager.GetSelectedItemsWithQuotes(ddlDepartment), AppManager.GetSelectedItemsWithQuotes(ddlCategory), AppManager.GetSelectedItemsWithQuotes(ddlSubCategory), AppManager.GetSelectedItemsWithQuotes(ddlSegment), AppManager.GetSelectedItemsWithQuotes(ddlBrand));

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlCorporateBrand_SelectedIndexChanged: "));
            }

        }

        #endregion

        #region Index Change Events Product Custom

        /// <summary>
        /// Handles the SelectedIndexChanged event of the Hierarchy control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlHierarchy_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                ddlCustomHierarchyLevel1_NewItem = null;
                ddlCustomHierarchyLevel1_DiscontinuedItem = null;
                ddlCustomHierarchyLevel1 = null;
                ddlCustomHierarchyLevel2 = null;
                ddlCustomHierarchyLevel3 = null;
                ddlBrand = null;
                ddlProductSize = null;

                ddlHierarchy = (RadComboBox)sender;


                //For Binding CustomHierarchyLevel1 New Item based on Hierarchy
                ddlCustomHierarchyLevel1_NewItem = (RadComboBox)ControlsParent.FindControl("CustomLevel1NewItems");

                //For Binding CustomHierarchyLevel1 Discontinued Item based on Hierarchy
                ddlCustomHierarchyLevel1_DiscontinuedItem = (RadComboBox)ControlsParent.FindControl("CustomLevel1DiscontinuedItems");

                if (ddlCustomHierarchyLevel1_NewItem == null && ddlCustomHierarchyLevel1_DiscontinuedItem == null)
                {
                    ddlCustomHierarchyLevel1 = (RadComboBox)ControlsParent.FindControl("CustomLevel1");
                }

                //For Binding CustomHierarchyLevel2 based on Hierarchy
                ddlCustomHierarchyLevel2 = (RadComboBox)ControlsParent.FindControl("CustomLevel2");

                //For Binding CustomHierarchyLevel3 based on Hierarchy
                ddlCustomHierarchyLevel3 = (RadComboBox)ControlsParent.FindControl("CustomLevel3");

                //For Binding Custom Product Brand
                ddlBrand = (RadComboBox)ControlsParent.FindControl("CustomBrand");

                //For Binding Custom Product Size
                ddlProductSize = (RadComboBox)ControlsParent.FindControl("CustomProductSizeDesc");

                //Handeles Hierarchy single selection list
                this.BindCustomLevel1Details(AppManager.GetSelectedItemsWithQuotes(ddlHierarchy));

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlHierarchy_SelectedIndexChanged: "));
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the CustomHierarchyLevel1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlCustomHierarchyLevel1_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                ddlCustomHierarchyLevel2 = null;
                ddlCustomHierarchyLevel3 = null;
                ddlBrand = null;
                ddlProductSize = null;

                ddlCustomHierarchyLevel1 = (RadComboBox)sender;

                //For Binding CustomHierarchyLevel2 based on Hierarchy
                ddlCustomHierarchyLevel2 = (RadComboBox)ControlsParent.FindControl("CustomLevel2");

                //For Binding CustomHierarchyLevel3 based on Hierarchy
                ddlCustomHierarchyLevel3 = (RadComboBox)ControlsParent.FindControl("CustomLevel3");

                //For Binding Custom Product Brand
                ddlBrand = (RadComboBox)ControlsParent.FindControl("CustomBrand");

                //For Binding Custom Product Size
                ddlProductSize = (RadComboBox)ControlsParent.FindControl("CustomProductSizeDesc");


                //if blocks handeles category multiple selection list
                if (ddlCustomHierarchyLevel1.CheckBoxes)
                {
                    foreach (RadComboBoxItem itemCustomHierarchyLevel1 in ddlCustomHierarchyLevel1.CheckedItems)
                    {
                        itemCustomHierarchyLevel1.Checked = true;
                    }
                }

                string selecteditemCustomHierarchyLevel1 = AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel1);
                this.GetCustomLevel2DetailsUsingCustomLevel1(AppManager.GetSelectedItemsWithQuotes(ddlHierarchy), selecteditemCustomHierarchyLevel1);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlCustomHierarchyLevel1_SelectedIndexChanged: "));
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the CustomHierarchyLevel2 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlCustomHierarchyLevel2_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                ddlCustomHierarchyLevel3 = null;
                ddlBrand = null;
                ddlProductSize = null;

                ddlCustomHierarchyLevel2 = (RadComboBox)sender;


                //For Binding CustomHierarchyLevel3 based on Hierarchy
                ddlCustomHierarchyLevel3 = (RadComboBox)ControlsParent.FindControl("CustomLevel3");

                //For Binding Custom Product Brand
                ddlBrand = (RadComboBox)ControlsParent.FindControl("CustomBrand");

                //For Binding Custom Product Size
                ddlProductSize = (RadComboBox)ControlsParent.FindControl("CustomProductSizeDesc");


                //if blocks handeles category multiple selection list
                if (ddlCustomHierarchyLevel2.CheckBoxes)
                {
                    foreach (RadComboBoxItem itemCustomHierarchyLevel2 in ddlCustomHierarchyLevel2.CheckedItems)
                    {
                        itemCustomHierarchyLevel2.Checked = true;
                    }
                }
                string selecteditemCustomHierarchyLevel2 = AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel2);
                this.GetCustomLevel3DetailsUsingCustomLevel2(AppManager.GetSelectedItemsWithQuotes(ddlHierarchy), AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel1), selecteditemCustomHierarchyLevel2);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlCustomHierarchyLevel2_SelectedIndexChanged: "));
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the CustomHierarchyLevel3 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        private void ddlCustomHierarchyLevel3_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                ddlBrand = null;
                ddlProductSize = null;

                ddlCustomHierarchyLevel3 = (RadComboBox)sender;

                //For Binding Custom Product Brand
                ddlBrand = (RadComboBox)ControlsParent.FindControl("CustomBrand");

                //For Binding Custom Product Size
                ddlProductSize = (RadComboBox)ControlsParent.FindControl("CustomProductSizeDesc");

                //if blocks handeles category multiple selection list
                if (ddlCustomHierarchyLevel3.CheckBoxes)
                {
                    foreach (RadComboBoxItem itemCustomHierarchyLevel3 in ddlCustomHierarchyLevel3.CheckedItems)
                    {
                        itemCustomHierarchyLevel3.Checked = true;
                    }
                }

                string selecteditemCustomHierarchyLevel3 = AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel3);
                this.GetBrandDetailsUsingCustomLevel3(AppManager.GetSelectedItemsWithQuotes(ddlHierarchy), AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel1), AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel2), selecteditemCustomHierarchyLevel3);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlCustomHierarchyLevel3_SelectedIndexChanged: "));
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlCustomBrand control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadComboBoxSelectedIndexChangedEventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void ddlCustomBrand_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                ddlProductSize = null;

                ddlBrand = (RadComboBox)sender;

                //For Binding Custom Product Size
                ddlProductSize = (RadComboBox)ControlsParent.FindControl("CustomProductSizeDesc");


                //if blocks handeles category multiple selection list
                if (ddlBrand.CheckBoxes)
                {
                    foreach (RadComboBoxItem itemBrand in ddlBrand.CheckedItems)
                    {
                        itemBrand.Checked = true;
                    }

                }
                string selecteditemBrand = AppManager.GetSelectedItemsWithQuotes(ddlBrand, true);
                this.GetProductSizeDetailsUsingCustomBrand(AppManager.GetSelectedItemsWithQuotes(ddlHierarchy), AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel1), AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel2), AppManager.GetSelectedItemsWithQuotes(ddlCustomHierarchyLevel2), selecteditemBrand);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "ddlCustomBrand_SelectedIndexChanged: "));
            }
        }

        #endregion

        #endregion

        #endregion

        #region Product Details Process Controls Bindings for corporate Hierarchy

        /// <summary>
        /// Binds the category details.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <param name="departmentId">The department identifier.</param>
        private void BindCategoryDetails(string departments = null)
        {

            if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
            {
                string selectedCategory = string.Empty;
                if (!string.IsNullOrEmpty(departments))
                {
                    DataRow[] categoryDetails = null;
                    categoryDetails = AppManager.GetCategoryDetails(this.dtProductDetailsCorporateHierarchy, departments);
                    if (categoryDetails != null && categoryDetails.Count() > 0)
                    {
                        DataTable dtCategory = categoryDetails.CopyToDataTable();
                        dtCategory.DefaultView.Sort = AppConstants.CATEGORY_COLUMN + " ASC";
                        this.ddlCategory.DataSource = dtCategory.DefaultView.ToTable(true, AppConstants.CATEGORY_COLUMN);
                        this.ddlCategory.DataTextField = AppConstants.CATEGORY_COLUMN;
                        this.ddlCategory.DataValueField = AppConstants.CATEGORY_COLUMN;
                        this.ddlCategory.DataBind();

                        if (this.ddlCategory.Items.Count > 0 && this.ddlCategory.CheckBoxes)
                        {
                            foreach (RadComboBoxItem itemCategory in this.ddlCategory.Items)
                            {
                                itemCategory.Checked = true;
                            }

                        }

                        selectedCategory = AppManager.GetSelectedItemsWithQuotes(this.ddlCategory);
                    }

                }
                else
                {
                    if (this.ddlCategory != null)
                    {
                        this.ddlCategory.Items.Clear();
                    }
                }
                //binding all subcategories as per category
                this.GetSubCategoryDetailsUsingCategory(departments, selectedCategory);
            }

        }

        /// <summary>
        /// Gets the sub category details using category.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        private void GetSubCategoryDetailsUsingCategory(string departments, string category)
        {
            if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
            {
                string selectedSubCategories = string.Empty;
                if (!string.IsNullOrEmpty(category))
                {
                    DataRow[] subCategoryDetails = AppManager.GetSubCategoryDetails(this.dtProductDetailsCorporateHierarchy, departments, category);
                    if (subCategoryDetails != null && subCategoryDetails.Count() > 0)
                    {
                        DataTable dtSubCategory = subCategoryDetails.CopyToDataTable();
                        dtSubCategory.DefaultView.Sort = AppConstants.SUBCATEGORY_COLUMN + " ASC";
                        if (this.ddlSubCategory != null)
                        {
                            this.ddlSubCategory.DataSource = dtSubCategory.DefaultView.ToTable(true, AppConstants.SUBCATEGORY_COLUMN);
                            this.ddlSubCategory.DataTextField = AppConstants.SUBCATEGORY_COLUMN;
                            this.ddlSubCategory.DataValueField = AppConstants.SUBCATEGORY_COLUMN;
                            this.ddlSubCategory.DataBind();
                            if (this.ddlSubCategory.Items.Count > 0 && this.ddlSubCategory.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemSubCategory in this.ddlSubCategory.Items)
                                {
                                    itemSubCategory.Checked = true;
                                }
                            }
                            selectedSubCategories = AppManager.GetSelectedItemsWithQuotes(ddlSubCategory);
                        }
                    }
                }
                else
                {
                    if (this.ddlSubCategory != null)
                    {
                        this.ddlSubCategory.Items.Clear();
                    }
                }
                this.GetSegmentDetailsUsingSubCategory(departments, category, selectedSubCategories);
            }
        }

        /// <summary>
        /// Gets the segment details using sub category.
        /// </summary>
        /// <param name="subCategoryId">The sub category identifier.</param>
        private void GetSegmentDetailsUsingSubCategory(string departments, string categories, string subCategories)
        {
            if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
            {
                string selectedsegments = string.Empty;
                if (!string.IsNullOrEmpty(subCategories))
                {
                    DataRow[] segmentDetails = AppManager.GetSegmentDetails(this.dtProductDetailsCorporateHierarchy, departments, categories, subCategories);
                    if (segmentDetails != null && segmentDetails.Count() > 0)
                    {
                        DataTable dtSegment = segmentDetails.CopyToDataTable();
                        dtSegment.DefaultView.Sort = AppConstants.SEGMENT_COLUMN + " ASC";
                        if (this.ddlSegment != null)
                        {
                            this.ddlSegment.DataSource = dtSegment.DefaultView.ToTable(true, AppConstants.SEGMENT_COLUMN);
                            this.ddlSegment.DataTextField = AppConstants.SEGMENT_COLUMN;
                            this.ddlSegment.DataValueField = AppConstants.SEGMENT_COLUMN;
                            this.ddlSegment.DataBind();

                            if (this.ddlSegment.Items.Count > 0 && this.ddlSegment.CheckBoxes)
                            {
                                foreach (RadComboBoxItem segment in this.ddlSegment.Items)
                                {
                                    segment.Checked = true;
                                }
                            }
                            selectedsegments = AppManager.GetSelectedItemsWithQuotes(ddlSegment);
                        }
                    }
                }
                else if (this.ddlSegment != null)
                {
                    this.ddlSegment.Items.Clear();
                }
                this.GetBrandDetailsUsingSegment(departments, categories, subCategories, selectedsegments);
            }

        }

        /// <summary>
        /// Gets the brand details using segment.
        /// </summary>
        /// <param name="segmentId">The segment identifier.</param>
        private void GetBrandDetailsUsingSegment(string departments, string categories, string subCategories, string segments)
        {
            if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
            {
                string selectedbrand = string.Empty;
                if (!string.IsNullOrEmpty(segments))
                {
                    DataRow[] brandDetails = AppManager.GetBrandDetails(this.dtProductDetailsCorporateHierarchy, departments, categories, subCategories, segments);
                    if (brandDetails != null && brandDetails.Count() > 0)
                    {
                        DataTable dtBrand = brandDetails.CopyToDataTable();
                        dtBrand.DefaultView.Sort = AppConstants.BRAND_COLUMN + " ASC";
                        if (this.ddlBrand != null)
                        {
                            this.ddlBrand.DataSource = dtBrand.DefaultView.ToTable(true, AppConstants.BRAND_COLUMN);
                            this.ddlBrand.DataTextField = AppConstants.BRAND_COLUMN;
                            this.ddlBrand.DataValueField = AppConstants.BRAND_COLUMN;
                            this.ddlBrand.DataBind();
                            if (this.ddlBrand.Items.Count > 0 && ddlBrand.CheckBoxes)
                            {
                                foreach (RadComboBoxItem brand in this.ddlBrand.Items)
                                {
                                    brand.Checked = true;
                                }
                            }
                            selectedbrand = AppManager.GetSelectedItemsWithQuotes(ddlBrand);
                        }
                    }
                }
                else if (this.ddlBrand != null)
                {
                    this.ddlBrand.Items.Clear();
                }
                this.GetProductSizeDetailsUsingBrand(departments, categories, subCategories, segments, selectedbrand);
            }
        }

        /// <summary>
        /// Gets the product size details using brand.
        /// </summary>
        /// <param name="BrandId">The brand identifier.</param>
        private void GetProductSizeDetailsUsingBrand(string departments, string categories, string subCategories, string segments, string brands)
        {
            if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(brands))
                {
                    DataRow[] ProductSizeDetails = AppManager.GetProductSizeDescDetails(this.dtProductDetailsCorporateHierarchy, departments, categories, subCategories, segments, brands);
                    if (ProductSizeDetails != null && ProductSizeDetails.Count() > 0)
                    {
                        DataTable dtProductSize = ProductSizeDetails.CopyToDataTable();
                        dtProductSize.DefaultView.Sort = AppConstants.PRODUCT_SIZE_COLUMN + " ASC";
                        if (this.ddlProductSize != null)
                        {
                            this.ddlProductSize.DataSource = dtProductSize.DefaultView.ToTable(true, AppConstants.PRODUCT_SIZE_COLUMN);
                            this.ddlProductSize.DataTextField = AppConstants.PRODUCT_SIZE_COLUMN;
                            this.ddlProductSize.DataValueField = AppConstants.PRODUCT_SIZE_COLUMN;
                            this.ddlProductSize.DataBind();
                            if (this.ddlProductSize.Items.Count > 0 && this.ddlProductSize.CheckBoxes)
                            {
                                foreach (RadComboBoxItem ProductSize in this.ddlProductSize.Items)
                                {
                                    ProductSize.Checked = true;
                                }
                            }
                        }
                    }
                }
                else if (this.ddlProductSize != null)
                {
                    this.ddlProductSize.Items.Clear();
                }
            }
        }

        #endregion

        #region  Product Details Process Control Bindings for Custom Hierarchy

        /// <summary>
        /// Binds the custom level1 details.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <param name="HierarchyId">The hierarchy identifier.</param>
        private void BindCustomLevel1Details(string hierarchyItem)
        {
            if (this.dtProductDetailsCustomHierarchy != null && this.dtProductDetailsCustomHierarchy.Rows.Count > 0)
            {
                string selectedCustomeLevel1Text = string.Empty;
                if (!string.IsNullOrEmpty(hierarchyItem))
                {
                    DataRow[] customLevel1Details = this.dtProductDetailsCustomHierarchy.Select(AppConstants.CUSTOM_HIERARCHY_COLUMN + "=" + hierarchyItem);
                    DataTable dtCustomLevel1 = customLevel1Details.CopyToDataTable();
                    if (customLevel1Details != null && customLevel1Details.Count() > 0)
                    {

                        dtCustomLevel1.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_1_COLUMN + " ASC";
                        if (this.ddlCustomHierarchyLevel1_NewItem != null)
                        {
                            this.ddlCustomHierarchyLevel1_NewItem.DataSource = dtCustomLevel1.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_1_COLUMN);
                            this.ddlCustomHierarchyLevel1_NewItem.DataTextField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            this.ddlCustomHierarchyLevel1_NewItem.DataValueField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            this.ddlCustomHierarchyLevel1_NewItem.DataBind();
                            this.ddlCustomHierarchyLevel1_NewItem.Items.Insert(0, new RadComboBoxItem { Text = IcontrolFilterResources.ddl_Select, Value = IcontrolFilterResources.ddl_Select });
                            if (this.ddlCustomHierarchyLevel1_NewItem.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemCustomHierarchyLevel1New in this.ddlCustomHierarchyLevel1_NewItem.Items)
                                {
                                    itemCustomHierarchyLevel1New.Checked = true;
                                }
                            }
                            else
                            {
                                this.ddlCustomHierarchyLevel1_NewItem.SelectedIndex = 1;
                            }

                            selectedCustomeLevel1Text = AppManager.GetSelectedItemsWithQuotes(this.ddlCustomHierarchyLevel1_NewItem);

                        }
                        if (this.ddlCustomHierarchyLevel1_DiscontinuedItem != null)
                        {
                            this.ddlCustomHierarchyLevel1_DiscontinuedItem.DataSource = dtCustomLevel1.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_1_COLUMN);
                            this.ddlCustomHierarchyLevel1_DiscontinuedItem.DataTextField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            this.ddlCustomHierarchyLevel1_DiscontinuedItem.DataValueField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            this.ddlCustomHierarchyLevel1_DiscontinuedItem.DataBind();
                            this.ddlCustomHierarchyLevel1_DiscontinuedItem.Items.Insert(0, new RadComboBoxItem { Text = IcontrolFilterResources.ddl_Select, Value = IcontrolFilterResources.ddl_Select });
                            if (this.ddlCustomHierarchyLevel1_DiscontinuedItem.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemCustomHierarchyLevel1dis in this.ddlCustomHierarchyLevel1_DiscontinuedItem.Items)
                                {
                                    itemCustomHierarchyLevel1dis.Checked = true;
                                }
                            }
                            else
                            {
                                this.ddlCustomHierarchyLevel1_DiscontinuedItem.SelectedIndex = 1;
                            }

                            selectedCustomeLevel1Text = AppManager.GetSelectedItemsWithQuotes(this.ddlCustomHierarchyLevel1_DiscontinuedItem);



                        }
                        if (this.ddlCustomHierarchyLevel1 != null)
                        {
                            this.ddlCustomHierarchyLevel1.DataSource = dtCustomLevel1.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_1_COLUMN);
                            this.ddlCustomHierarchyLevel1.DataTextField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            this.ddlCustomHierarchyLevel1.DataValueField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            this.ddlCustomHierarchyLevel1.DataBind();

                            if (this.ddlCustomHierarchyLevel1.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemCustomHierarchyLevel1 in this.ddlCustomHierarchyLevel1.Items)
                                {
                                    itemCustomHierarchyLevel1.Checked = true;
                                }
                            }
                            else
                            {
                                this.ddlCustomHierarchyLevel1.SelectedIndex = 1;
                            }
                            selectedCustomeLevel1Text = AppManager.GetSelectedItemsWithQuotes(this.ddlCustomHierarchyLevel1);
                        }
                    }
                }
                else
                {
                    if (this.ddlCustomHierarchyLevel1_NewItem != null)
                    {
                        this.ddlCustomHierarchyLevel1_NewItem.Items.Clear();
                        this.ddlCustomHierarchyLevel1_DiscontinuedItem.Items.Clear();
                    }
                    if (this.ddlCustomHierarchyLevel1 != null)
                    {
                        this.ddlCustomHierarchyLevel1.Items.Clear();
                    }
                }
                this.GetCustomLevel2DetailsUsingCustomLevel1(hierarchyItem, selectedCustomeLevel1Text);
            }
        }

        /// <summary>
        /// Gets the custom level2 details using custom level1.
        /// </summary>
        /// <param name="HierarchyId">The hierarchy identifier.</param>
        private void GetCustomLevel2DetailsUsingCustomLevel1(string hierarchyItem, string selectedCustomeLevel1Text)
        {
            string selectedCustomeLevel2Text = string.Empty;
            if (!string.IsNullOrEmpty(selectedCustomeLevel1Text))
            {
                DataRow[] customLevel2Details = AppManager.GetNextProductFilterItemsList_Custom(this.dtProductDetailsCustomHierarchy, hierarchyItem, selectedCustomeLevel1Text);

                if (customLevel2Details != null && customLevel2Details.Count() > 0)
                {
                    DataTable dtCustomLevel2 = customLevel2Details.CopyToDataTable();
                    dtCustomLevel2.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_2_COLUMN + " ASC";
                    if (this.ddlCustomHierarchyLevel2 != null)
                    {
                        this.ddlCustomHierarchyLevel2.DataSource = dtCustomLevel2.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_2_COLUMN);
                        this.ddlCustomHierarchyLevel2.DataTextField = AppConstants.CUSTOM_LEVEL_2_COLUMN;
                        this.ddlCustomHierarchyLevel2.DataValueField = AppConstants.CUSTOM_LEVEL_2_COLUMN;
                        this.ddlCustomHierarchyLevel2.DataBind();

                        if (this.ddlCustomHierarchyLevel2.CheckBoxes)
                        {
                            foreach (RadComboBoxItem itemCustomHierarchyLevel2 in this.ddlCustomHierarchyLevel2.Items)
                            {
                                itemCustomHierarchyLevel2.Checked = true;
                            }

                        }
                        selectedCustomeLevel2Text = AppManager.GetSelectedItemsWithQuotes(this.ddlCustomHierarchyLevel2, true);

                    }

                }
            }
            else
            {
                if (this.ddlCustomHierarchyLevel2 != null)
                {
                    this.ddlCustomHierarchyLevel2.Items.Clear();
                }

            }
            this.GetCustomLevel3DetailsUsingCustomLevel2(hierarchyItem, selectedCustomeLevel1Text, selectedCustomeLevel2Text);
        }

        /// <summary>
        /// Gets the custom level3 details using custom level2.
        /// </summary>
        /// <param name="HierarchyId">The hierarchy identifier.</param>
        private void GetCustomLevel3DetailsUsingCustomLevel2(string hierarchyItem, string selectedCustomeLevel1Text, string customLevel2Text)
        {
            string customLevel3Text = string.Empty;
            if (!string.IsNullOrEmpty(customLevel2Text))
            {
                DataRow[] CustomLevel3Details = AppManager.GetNextProductFilterItemsList_Custom(this.dtProductDetailsCustomHierarchy, hierarchyItem, selectedCustomeLevel1Text, customLevel2Text);

                if (CustomLevel3Details != null && CustomLevel3Details.Count() > 0)
                {
                    DataTable dtCustomLevel3 = CustomLevel3Details.CopyToDataTable();
                    dtCustomLevel3.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_3_COLUMN + " ASC";

                    if (this.ddlCustomHierarchyLevel3 != null)
                    {
                        this.ddlCustomHierarchyLevel3.DataSource = dtCustomLevel3.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_3_COLUMN); ;
                        this.ddlCustomHierarchyLevel3.DataTextField = AppConstants.CUSTOM_LEVEL_3_COLUMN;
                        this.ddlCustomHierarchyLevel3.DataValueField = AppConstants.CUSTOM_LEVEL_3_COLUMN;
                        this.ddlCustomHierarchyLevel3.DataBind();

                        if (this.ddlCustomHierarchyLevel3.CheckBoxes)
                        {
                            foreach (RadComboBoxItem itemCustomHierarchyLevel3 in this.ddlCustomHierarchyLevel3.Items)
                            {
                                itemCustomHierarchyLevel3.Checked = true;
                            }
                        }

                        customLevel3Text = AppManager.GetSelectedItemsWithQuotes(this.ddlCustomHierarchyLevel3);

                    }
                }
            }
            else
            {
                if (this.ddlCustomHierarchyLevel3 != null)
                {
                    this.ddlCustomHierarchyLevel3.Items.Clear();
                }
            }
            this.GetBrandDetailsUsingCustomLevel3(hierarchyItem, selectedCustomeLevel1Text, customLevel2Text, customLevel3Text);
        }

        /// <summary>
        /// Gets the brand details using segment.
        /// </summary>
        /// <param name="segmentId">The segment identifier.</param>
        private void GetBrandDetailsUsingCustomLevel3(string hierarchyItem, string selectedCustomeLevel1Text, string customLevel2Text, string customLevel3Text)
        {
            string selectedBrandText = string.Empty;
            if (!string.IsNullOrEmpty(customLevel3Text))
            {
                DataRow[] brandDetails = AppManager.GetNextProductFilterItemsList_Custom(this.dtProductDetailsCustomHierarchy, hierarchyItem, selectedCustomeLevel1Text, customLevel2Text, customLevel3Text);
                if (brandDetails != null && brandDetails.Count() > 0)
                {
                    DataTable dtBrand = brandDetails.CopyToDataTable();
                    dtBrand.DefaultView.Sort = AppConstants.CUSTOM_BRAND_COLUMN + " ASC";

                    if (this.ddlBrand != null)
                    {
                        this.ddlBrand.DataSource = dtBrand.DefaultView.ToTable(true, AppConstants.CUSTOM_BRAND_COLUMN);
                        this.ddlBrand.DataTextField = AppConstants.CUSTOM_BRAND_COLUMN;
                        this.ddlBrand.DataValueField = AppConstants.CUSTOM_BRAND_COLUMN;
                        this.ddlBrand.DataBind();

                        if (this.ddlBrand.CheckBoxes)
                        {
                            foreach (RadComboBoxItem brand in this.ddlBrand.Items)
                            {
                                brand.Checked = true;
                            }

                        }
                        selectedBrandText = AppManager.GetSelectedItemsWithQuotes(this.ddlBrand);

                    }

                }
            }
            else
            {
                if (this.ddlBrand != null)
                {
                    this.ddlBrand.Items.Clear();
                }
            }
            this.GetProductSizeDetailsUsingCustomBrand(hierarchyItem, selectedCustomeLevel1Text, customLevel2Text, customLevel3Text, selectedBrandText);
        }

        /// <summary>
        /// Gets the product size details using brand.
        /// </summary>
        /// <param name="BrandId">The brand identifier.</param>
        private void GetProductSizeDetailsUsingCustomBrand(string hierarchyItem, string selectedCustomeLevel1Text, string customLevel2Text, string customLevel3Text, string selectedBrandText)
        {
            if (!string.IsNullOrEmpty(selectedBrandText))
            {
                DataRow[] productSizeDetails = AppManager.GetNextProductFilterItemsList_Custom(this.dtProductDetailsCustomHierarchy, hierarchyItem, selectedCustomeLevel1Text, customLevel2Text, customLevel3Text, selectedBrandText);

                if (productSizeDetails != null && productSizeDetails.Count() > 0)
                {
                    DataTable dtProductSize = productSizeDetails.CopyToDataTable();
                    dtProductSize.DefaultView.Sort = AppConstants.CUSTOM_PRODUCT_SIZE_COLUMN + " ASC";
                    if (this.ddlProductSize != null)
                    {
                        this.ddlProductSize.DataSource = dtProductSize.DefaultView.ToTable(true, AppConstants.CUSTOM_PRODUCT_SIZE_COLUMN);
                        this.ddlProductSize.DataTextField = AppConstants.CUSTOM_PRODUCT_SIZE_COLUMN;
                        this.ddlProductSize.DataValueField = AppConstants.CUSTOM_PRODUCT_SIZE_COLUMN;
                        this.ddlProductSize.DataBind();

                        if (this.ddlProductSize != null && this.ddlProductSize.CheckBoxes)
                        {
                            foreach (RadComboBoxItem ProductSize in this.ddlProductSize.Items)
                            {
                                ProductSize.Checked = true;
                            }
                        }
                    }
                }
            }
            else
            {
                if (this.ddlProductSize != null)
                {
                    this.ddlProductSize.Items.Clear();
                }
            }
        }

        #endregion

        #region  Location Details Process Control Bindings for Custom Hierarchy
        /// <summary>
        /// Binds the custom level1 details.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <param name="HierarchyId">The hierarchy identifier.</param>
        private void BindLocationCustomLevel1Details(string LocationStores = null)
        {

            if (this.dtLocationDetailsCustomHierarchy != null && this.dtLocationDetailsCustomHierarchy.Rows.Count > 0)
            {
                string selectedCustomeLevel1 = string.Empty;
                if (!string.IsNullOrEmpty(LocationStores))
                {
                    DataRow[] CustomLevel1Details = AppManager.GetNextLocationFilterItemsList_Custom(this.dtLocationDetailsCustomHierarchy, LocationStores);
                    if (CustomLevel1Details != null && CustomLevel1Details.Count() > 0)
                    {
                        DataTable dtCustomLevel1 = CustomLevel1Details.CopyToDataTable();
                        dtCustomLevel1.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_1_COLUMN + " ASC";

                        if (this.ddlCustomHierarchyLevel1 != null)
                        {
                            this.ddlCustomHierarchyLevel1.DataSource = dtCustomLevel1.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_1_COLUMN);
                            this.ddlCustomHierarchyLevel1.DataTextField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            this.ddlCustomHierarchyLevel1.DataValueField = AppConstants.CUSTOM_LEVEL_1_COLUMN;
                            this.ddlCustomHierarchyLevel1.DataBind();
                            if (this.ddlCustomHierarchyLevel1.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemCustomHierarchyLevel1 in this.ddlCustomHierarchyLevel1.Items)
                                {
                                    itemCustomHierarchyLevel1.Checked = true;
                                }
                            }
                            selectedCustomeLevel1 = AppManager.GetSelectedItemsWithQuotes(this.ddlCustomHierarchyLevel1);
                        }
                    }
                }
                else
                {
                    if (this.ddlCustomHierarchyLevel1 != null)
                    {
                        this.ddlCustomHierarchyLevel1.Items.Clear();
                    }
                }
                this.GetLocationCustomLevel2DetailsUsingLocationCustomLevel1(LocationStores, selectedCustomeLevel1);
            }

        }

        /// <summary>
        /// Gets the custom level2 details using custom level1.
        /// </summary>
        /// <param name="HierarchyId">The hierarchy identifier.</param>
        private void GetLocationCustomLevel2DetailsUsingLocationCustomLevel1(string LocationStores, string selectedCustomeLevel1Text)
        {
            if (this.dtLocationDetailsCustomHierarchy != null && this.dtLocationDetailsCustomHierarchy.Rows.Count > 0)
            {
                string selectedCustomeLevel2 = string.Empty;
                if (!string.IsNullOrEmpty(selectedCustomeLevel1Text))
                {
                    DataRow[] CustomLevel2Details = AppManager.GetNextLocationFilterItemsList_Custom(this.dtLocationDetailsCustomHierarchy, LocationStores, selectedCustomeLevel1Text);
                    if (CustomLevel2Details != null && CustomLevel2Details.Count() > 0)
                    {
                        DataTable dtCustomLevel2 = CustomLevel2Details.CopyToDataTable();
                        dtCustomLevel2.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_2_COLUMN + " ASC";

                        if (this.ddlCustomHierarchyLevel2 != null)
                        {
                            this.ddlCustomHierarchyLevel2.DataSource = dtCustomLevel2.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_2_COLUMN);
                            this.ddlCustomHierarchyLevel2.DataTextField = AppConstants.CUSTOM_LEVEL_2_COLUMN;
                            this.ddlCustomHierarchyLevel2.DataValueField = AppConstants.CUSTOM_LEVEL_2_COLUMN;
                            this.ddlCustomHierarchyLevel2.DataBind();

                            if (this.ddlCustomHierarchyLevel2.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemCustomHierarchyLevel2 in this.ddlCustomHierarchyLevel2.Items)
                                {
                                    itemCustomHierarchyLevel2.Checked = true;
                                }
                            }
                            selectedCustomeLevel2 = AppManager.GetSelectedItemsWithQuotes(this.ddlCustomHierarchyLevel2);
                        }
                    }
                }
                else
                {
                    if (this.ddlCustomHierarchyLevel2 != null)
                    {
                        this.ddlCustomHierarchyLevel2.Items.Clear();
                    }
                }
                this.GetLocationCustomLevel3DetailsUsingLocationCustomLevel2(LocationStores, selectedCustomeLevel1Text, selectedCustomeLevel2);
            }
        }

        /// <summary>
        /// Gets the custom level3 details using custom level2.
        /// </summary>
        /// <param name="HierarchyId">The hierarchy identifier.</param>
        private void GetLocationCustomLevel3DetailsUsingLocationCustomLevel2(string LocationStores, string selectedCustomeLevel1Text, string selectedCustomeLevel2Text)
        {
            if (this.dtLocationDetailsCustomHierarchy != null && this.dtLocationDetailsCustomHierarchy.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(selectedCustomeLevel2Text))
                {
                    DataRow[] CustomLevel3Details = AppManager.GetNextLocationFilterItemsList_Custom(this.dtLocationDetailsCustomHierarchy, LocationStores, selectedCustomeLevel1Text, selectedCustomeLevel2Text);
                    if (CustomLevel3Details != null && CustomLevel3Details.Count() > 0)
                    {
                        DataTable dtCustomLevel3 = CustomLevel3Details.CopyToDataTable();
                        dtCustomLevel3.DefaultView.Sort = AppConstants.CUSTOM_LEVEL_3_COLUMN + " ASC";

                        if (this.ddlCustomHierarchyLevel3 != null)
                        {
                            this.ddlCustomHierarchyLevel3.DataSource = dtCustomLevel3.DefaultView.ToTable(true, AppConstants.CUSTOM_LEVEL_3_COLUMN);
                            this.ddlCustomHierarchyLevel3.DataTextField = AppConstants.CUSTOM_LEVEL_3_COLUMN;
                            this.ddlCustomHierarchyLevel3.DataValueField = AppConstants.CUSTOM_LEVEL_3_COLUMN;
                            this.ddlCustomHierarchyLevel3.DataBind();

                            if (this.ddlCustomHierarchyLevel3.CheckBoxes)
                            {
                                foreach (RadComboBoxItem itemCustomHierarchyLevel3 in this.ddlCustomHierarchyLevel3.Items)
                                {
                                    itemCustomHierarchyLevel3.Checked = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (this.ddlCustomHierarchyLevel3 != null)
                    {
                        this.ddlCustomHierarchyLevel3.Items.Clear();
                    }

                }
            }
        }

        #endregion

        /// <summary>
        /// Binds the controls dynamicaly.
        /// </summary>
        /// <param name="MethodName">Name of the method.</param>
        /// <returns>dynamic.</returns>
        private dynamic BindControlsDynamicaly(string MethodName, bool isSingleSelectionList = false)
        {
            switch (MethodName)
            {
                case AppConstants.EVENT_CREATE_BANNER_LIST:
                    return CreateBannerList();

                case AppConstants.EVENT_CREATE_SUB_BANNER_LIST:
                    return CreateSubBannerList();

                case AppConstants.EVENT_CREATE_BANNER_LOCATION_SOURCE_LIST:
                    return CreateLoctionHierarchySourceBannerList();

                case AppConstants.EVENT_CREATE_SUB_BANNER_LOCATION_SOURCE_LIST:
                    return CreateLoctionHierarchySourceSubBannerList();

                case AppConstants.EVENT_CREATE_STORE_LOCATION_SOURCE_LIST:
                    return CreateLoctionHierarchySourceStoreList();

                case AppConstants.EVENT_CREATE_BANNER_LOCATION_DESTINATION_LIST:
                    return CreateLoctionHierarchyDestinationBannerList();

                case AppConstants.EVENT_CREATE_SUB_BANNER_LOCATION_DESTINATION_LIST:
                    return CreateLoctionHierarchyDestinationSubBannerList();

                case AppConstants.EVENT_CREATE_STORE_LOCATION_DESTINATION_LIST:
                    return CreateLoctionHierarchyDestinationStoreList();

                case AppConstants.EVENT_CREATE_LOCATION_STORE_LIST:
                    return CreateLocationStoreList();

                case AppConstants.EVENT_CREATE_LOCATION_CUSTOM_LEVEL_1_LIST:
                    return CreateLocationCustomLevel1List();

                case AppConstants.EVENT_CREATE_LOCATION_CUSTOM_LEVEL_2_LIST:
                    return CreateLocationCustomLevel2List();

                case AppConstants.EVENT_CREATE_LOCATION_CUSTOM_LEVEL_3_LIST:
                    return CreateLocationCustomLevel3List();

                case AppConstants.EVENT_CREATE_FISCAL_YEAR_WEEK_FROM_LIST:
                    return CreateFiscalYearWeekFromList();

                case AppConstants.EVENT_CREATE_FISCAL_YEAR_WEEK_TO_LIST:
                    return CreateFiscalYearWeekToList();

                case AppConstants.EVENT_CREATE_DEPARTMENT_LIST:
                    return CreateDepartmentList();

                case AppConstants.EVENT_CREATE_CATEGORY_LIST:
                    return CreateCategoryList(isSingleSelectionList);

                case AppConstants.EVENT_CREATE_SUB_CATEGORY_LIST:
                    return CreateSubCategoryList();

                case AppConstants.EVENT_CREATE_SEGMENT_LIST:
                    return CreateSegmentList();

                case AppConstants.EVENT_CREATE_CORPORATE_BRAND_LIST:
                    return CreateCorporateBrandList();

                case AppConstants.EVENT_CREATE_CORPORATE_PRODUCT_SIZE_LIST:
                    return CreateCorporateProductSizeList();

                case AppConstants.EVENT_CREATE_CUSTOM_LEVEL_1_LIST:
                    return CreateCustomLevel1List();

                case AppConstants.EVENT_CREATE_CUSTOM_LEVEL_1_NEW_ITEMS_LIST:
                    return CreateCustomLevel1NewItemsList();

                case AppConstants.EVENT_CREATE_CUSTOM_LEVEL_1_DISCONTINUED_ITEMS_LIST:
                    return CreateCustomLevel1DiscontinuedItemsList();

                case AppConstants.EVENT_CREATE_CUSTOM_LEVEL_2_LIST:
                    return CreateCustomLevel2List();

                case AppConstants.EVENT_CREATE_CUSTOM_LEVEL_3_LIST:
                    return CreateCustomLevel3List();

                case AppConstants.EVENT_CREATE_CUSTOM_BRAND_LIST:
                    return CreateCustomBrandList();

                case AppConstants.EVENT_CREATE_CUSTOM_PRODUCT_SIZE_LIST:
                    return CreateCustomProductSizeList();

                case AppConstants.EVENT_CREATE_BRANDTYPE_LIST:
                    return CreateBrandTypeList();

                case AppConstants.EVENT_CREATE_VIEWBY_LIST:
                    return CreateViewByList();

                case AppConstants.EVENT_CREATE_LOCATION_HIERARCHY_TYPE_LIST:
                    return CreateLocationHierarchyTypeList();

                case AppConstants.EVENT_CREATE_PRODUCT_HIERARCHY_TYPE_LIST:
                    return CreateProductHierarchyTypeList();

                case AppConstants.EVENT_CREATE_TIMEFRAME_LIST:
                    return CreateTimeFrameList();

                case AppConstants.EVENT_CREATE_HIERARCHY_LIST:
                    return CreateHierarchyList(isSingleSelectionList);

                case AppConstants.EVENT_CREATE_NO_OF_WEEKS_LIST:
                    return CreateNoOffWeeksList();

                case AppConstants.EVENT_CREATE_NO_OF_WEEKS_PRIOR_LIST:
                    return CreateNoOffWeeksPriorList();

                case AppConstants.EVENT_CREATE_NO_OF_WEEKS_POST_LIST:
                    return CreateNoOffWeeksPostList();

                case AppConstants.EVENT_CREATE_HOLIDAY_LIST:
                    return CreateHolidayList(isSingleSelectionList);

                case AppConstants.EVENT_CREATE_YEAR_LIST:
                    return CreateYearList();

                case AppConstants.EVENT_CREATE_POG_LIST:
                    return CreatePogList();
                default:
                    return null;

            }
        }

        #endregion

        #region ShowReport Event
        /// <summary>
        /// Handles the Click event of the BtnShowReport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void BtnShowReport_Click(object sender, EventArgs e)
        {
            try
            {

                this.Page.Validate();
                if (this.Page.IsValid)
                {
                    PnlshareButtons.Visible = true;
                    UpdatePanelToolbar.Update();
                    this.dataAccessLayer = new DataLayer();
                    this.selectedFilterValues = new Dictionary<string, dynamic>();

                    //handeling location and product Hierarchies (Custom or Corporate)
                    RadComboBox locationHierarcgy = (RadComboBox)this.ControlsParent.FindControl("HierarchyLocation");
                    RadComboBox productHierarchy = (RadComboBox)this.ControlsParent.FindControl("Hierarchy");
                    string result = "";
                    if (productHierarchy == null)
                    {
                        RadComboBox category = (RadComboBox)this.ControlsParent.FindControl("Category");
                        RadComboBox hierarchyName = (RadComboBox)this.ControlsParent.FindControl("HierarchyName");
                        result = category != null ? "Corporate" : hierarchyName != null ? "Custom" : string.Empty;
                    }

                    foreach (var row in this.pageControlsDetails)
                    {
                        foreach (var filter in row.Items)
                        {
                            if (!string.IsNullOrEmpty(filter.ParameterName))
                            {
                                dynamic control = ControlsParent.FindControl(Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty)));
                                if (!string.IsNullOrEmpty(filter.ControlName) && (Convert.ToString(filter.ControlName).ToUpper() == AppConstants.INPUTCONTROLS_DROPDOWN.ToUpper()))
                                {
                                    string commmaSeperatedValues = string.Empty;
                                    control = control as RadComboBox;
                                    string commmaSeperatedTexts = string.Empty;
                                    if (control.CheckBoxes)
                                    {
                                        //Location Corporate Hierarchy
                                        if (filter.ParameterName == AppConstants.PARAM_BANNER_ID || filter.ParameterName == AppConstants.PARAM_SUB_BANNER_ID || filter.ParameterName == AppConstants.PARAM_STORE_ID || filter.ParameterName == AppConstants.PARAM_OPP_BANNER_ID || filter.ParameterName == AppConstants.PARAM_OPP_SUB_BANNER_ID || filter.ParameterName == AppConstants.PARAM_OPP_STORE_ID)
                                        {
                                            if (filter.ParameterName == AppConstants.PARAM_BANNER_ID && control.CheckedItems.Count != control.Items.Count)
                                            {
                                                commmaSeperatedValues = AppManager.GetControlValuesByText(control as RadComboBox, this.dtBannerAndSubBannerDetails, AppConstants.BANNER_COLUMN, AppConstants.BANNER_ID_COLUMN);
                                            }
                                            else if (filter.ParameterName == AppConstants.PARAM_SUB_BANNER_ID && control.CheckedItems.Count != control.Items.Count)
                                            {
                                                commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtBannerAndSubBannerDetails, AppConstants.SUBBANNER_COLUMN, AppConstants.SUBBANNER_ID_COLUMN);
                                            }
                                            else if (filter.ParameterName == AppConstants.PARAM_STORE_ID && control.CheckedItems.Count != control.Items.Count)
                                            {
                                                commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtBannerAndSubBannerDetails, AppConstants.LOC_SOURCE_STORE_COLUMN, AppConstants.LOC_STORE_NUMBER_COLUMN);
                                            }
                                            else if (filter.ParameterName == AppConstants.PARAM_OPP_BANNER_ID && control.CheckedItems.Count != control.Items.Count)
                                            {
                                                commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtBannerAndSubBannerDetails, AppConstants.BANNER_COLUMN, AppConstants.BANNER_ID_COLUMN);
                                            }
                                            else if (filter.ParameterName == AppConstants.PARAM_OPP_SUB_BANNER_ID && control.CheckedItems.Count != control.Items.Count)
                                            {
                                                commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtBannerAndSubBannerDetails, AppConstants.SUBBANNER_COLUMN, AppConstants.SUBBANNER_ID_COLUMN);
                                            }
                                            else if (filter.ParameterName == AppConstants.PARAM_OPP_STORE_ID && control.CheckedItems.Count != control.Items.Count)
                                            {
                                                commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtBannerAndSubBannerDetails, AppConstants.LOC_DESTINATION_STORE_COLUMN, AppConstants.LOC_STORE_NUMBER_COLUMN);
                                            }


                                        }
                                        //Location Custom Hierarchy
                                        else if (filter.ParameterName == AppConstants.PARAM_CUSTOM_STORE_ID && control.CheckedItems.Count != control.Items.Count)
                                        {
                                            commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtLocationDetailsCustomHierarchy, AppConstants.CUSTOM_STORE_NAME_COLUMN, AppConstants.CUSTOM_STORE_GROUP_ID_COLUMN);
                                        }
                                        else if (filter.ParameterName == AppConstants.PARAM_LOCCUSTOM_LEVEL1 && control.CheckedItems.Count != control.Items.Count)
                                        {
                                            commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtLocationDetailsCustomHierarchy, AppConstants.CUSTOM_LEVEL_1_COLUMN, AppConstants.CUSTOM_LEVEL_1_COLUMN);
                                        }
                                        else if (filter.ParameterName == AppConstants.PARAM_LOCCUSTOM_LEVEL2 && control.CheckedItems.Count != control.Items.Count)
                                        {
                                            commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtLocationDetailsCustomHierarchy, AppConstants.CUSTOM_LEVEL_2_COLUMN, AppConstants.CUSTOM_LEVEL_2_COLUMN);
                                        }
                                        else if (filter.ParameterName == AppConstants.PARAM_LOCCUSTOM_LEVEL3 && control.CheckedItems.Count != control.Items.Count)
                                        {
                                            commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtLocationDetailsCustomHierarchy, AppConstants.CUSTOM_LEVEL_3_COLUMN, AppConstants.CUSTOM_LEVEL_3_COLUMN);
                                        }

                                        //Product Corporate Hierarchy
                                        else if (filter.ParameterName == AppConstants.PARAM_DEPARTMENT_ID || filter.ParameterName == AppConstants.PARAM_CATEGORY_ID || filter.ParameterName == AppConstants.PARAM_SUBCATEGORY_ID || filter.ParameterName == AppConstants.PARAM_SEGMENT_ID || Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty)) == AppConstants.PARAM_CORPORATE_BRAND_FILTER_NAME || Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty)) == AppConstants.PARAM_CORPORATE_PRODUCTSIZE_BRAND_FILTER_NAME)
                                        {
                                            if (control.CheckedItems.Count != control.Items.Count)
                                            {
                                                if (filter.ParameterName == AppConstants.PARAM_DEPARTMENT_ID)
                                                {
                                                    commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtProductDetailsCorporateHierarchy, AppConstants.DEPARTMENT_COLUMN, AppConstants.DEPARTMENT_ID_COLUMN);
                                                }
                                                else if (filter.ParameterName == AppConstants.PARAM_CATEGORY_ID)
                                                {
                                                    commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtProductDetailsCorporateHierarchy, AppConstants.CATEGORY_COLUMN, AppConstants.CATEGORY_ID_COLUMN);
                                                }
                                                else if (filter.ParameterName == AppConstants.PARAM_SUBCATEGORY_ID)
                                                {
                                                    commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtProductDetailsCorporateHierarchy, AppConstants.SUBCATEGORY_COLUMN, AppConstants.SUBCATEGORY_ID_COLUMN);
                                                }
                                                else if (filter.ParameterName == AppConstants.PARAM_SEGMENT_ID)
                                                {
                                                    commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtProductDetailsCorporateHierarchy, AppConstants.SEGMENT_COLUMN, AppConstants.SEGMENT_ID_COLUMN);
                                                }
                                                else if (filter.ParameterName == AppConstants.PARAM_BRAND)
                                                {
                                                    commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtProductDetailsCorporateHierarchy, AppConstants.BRAND_COLUMN, AppConstants.BRAND_ID_COLUMN);
                                                }
                                                else if (filter.ParameterName == AppConstants.PARAM_PRODUCT_SIZE)
                                                {
                                                    commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtProductDetailsCorporateHierarchy, AppConstants.PRODUCT_SIZE_COLUMN, AppConstants.PRODUCT_SIZE_COLUMN);
                                                }
                                            }

                                        }
                                        //Product Custom Hierarchy
                                        else if (filter.ParameterName == AppConstants.PARAM_HIERARCHY_ID || filter.ParameterName == AppConstants.PARAM_CUSTOM_LEVEL1 || filter.ParameterName == AppConstants.PARAM_CUSTOM_LEVEL2 || filter.ParameterName == AppConstants.PARAM_CUSTOM_LEVEL3 || Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty)) == AppConstants.PARAM_CUSTOM_BRAND_FILTER_NAME || Convert.ToString(filter.FilterName.Trim().Replace(" ", string.Empty)) == AppConstants.PARAM_CUSTOM_PRODUCTSIZE_FILTER_NAME)
                                        {
                                            if (filter.ParameterName == AppConstants.PARAM_HIERARCHY_ID && control.CheckedItems.Count != control.Items.Count)
                                            {
                                                commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtProductDetailsCustomHierarchy, AppConstants.CUSTOM_HIERARCHY_COLUMN, AppConstants.CUSTOM_HIERARCHY_ID_COLUMN);
                                            }
                                            else if (filter.ParameterName == AppConstants.PARAM_CUSTOM_LEVEL1 && control.CheckedItems.Count != control.Items.Count)
                                            {
                                                commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtProductDetailsCustomHierarchy, AppConstants.CUSTOM_LEVEL_1_COLUMN, AppConstants.CUSTOM_LEVEL_1_COLUMN);
                                            }
                                            else if (filter.ParameterName == AppConstants.PARAM_CUSTOM_LEVEL2 && control.CheckedItems.Count != control.Items.Count)
                                            {
                                                commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtProductDetailsCustomHierarchy, AppConstants.CUSTOM_LEVEL_2_COLUMN, AppConstants.CUSTOM_LEVEL_2_COLUMN);
                                            }
                                            else if (filter.ParameterName == AppConstants.PARAM_CUSTOM_LEVEL3 && control.CheckedItems.Count != control.Items.Count)
                                            {
                                                commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtProductDetailsCustomHierarchy, AppConstants.CUSTOM_LEVEL_3_COLUMN, AppConstants.CUSTOM_LEVEL_3_COLUMN);
                                            }
                                            else if (filter.ParameterName == AppConstants.PARAM_BRAND)
                                            {
                                                commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtProductDetailsCustomHierarchy, AppConstants.CUSTOM_BRAND_COLUMN, AppConstants.CUSTOM_BRAND_COLUMN);
                                            }
                                            else if (filter.ParameterName == AppConstants.PARAM_PRODUCT_SIZE)
                                            {
                                                commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtProductDetailsCustomHierarchy, AppConstants.PRODUCT_SIZE_COLUMN, AppConstants.PRODUCT_SIZE_COLUMN);
                                            }
                                        }

                                        if (control.CheckedItems.Count == control.Items.Count && control.Items.Count != 0)
                                        {
                                            commmaSeperatedTexts = "All";
                                        }
                                        else if (control.CheckedItems.Count == 0 && control.Items.Count != 0)
                                        {
                                            commmaSeperatedTexts = "None";
                                        }
                                        else
                                        {
                                            commmaSeperatedTexts = AppManager.GetSelectedItemsWithOutQuotes(control);
                                        }

                                    }
                                    else if (control.SelectedIndex > 0 || control.SelectedItem.Text != IcontrolFilterResources.ddl_Select)
                                    {
                                        if (filter.ParameterName == AppConstants.PARAM_CATEGORY_ID)
                                        {
                                            commmaSeperatedValues = AppManager.GetControlValuesByText(control, this.dtProductDetailsCorporateHierarchy, AppConstants.CATEGORY_COLUMN, AppConstants.CATEGORY_ID_COLUMN);
                                            commmaSeperatedTexts = control.SelectedItem.Text;
                                        }
                                        else if (filter.ParameterName == AppConstants.PARAM_HIERARCHY_ID || filter.ParameterName == AppConstants.PARAM_HOLIDAY_ID || filter.ParameterName == AppConstants.PARAM_FISCAL_YEAR || filter.ParameterName == AppConstants.PARAM_PRIOR_WEEK || filter.ParameterName == AppConstants.PARAM_POST_WEEK)
                                        {
                                            commmaSeperatedValues = control.SelectedValue;
                                            commmaSeperatedTexts = control.SelectedItem.Text;
                                        }

                                        else if (filter.ParameterName == AppConstants.PARAM_BRAND_TYPE)
                                        {
                                            if (control.SelectedValue != "0")
                                            {
                                                commmaSeperatedValues = control.SelectedValue != "Yes" ? "N" : "Y";
                                                commmaSeperatedTexts = control.SelectedItem.Text;
                                            }
                                        }
                                        else if (filter.ParameterName == AppConstants.PARAM_POG_FLAG)
                                        {
                                            commmaSeperatedValues = control.SelectedValue != "0" ? control.SelectedValue : " ";
                                            commmaSeperatedTexts = control.SelectedItem.Text;
                                        }

                                    }
                                    if ((!String.IsNullOrEmpty(commmaSeperatedTexts) && String.IsNullOrEmpty(commmaSeperatedValues)) || !String.IsNullOrEmpty(commmaSeperatedValues))
                                    {
                                        string selectedValues = string.Empty;

                                        if (!string.IsNullOrEmpty(filter.CssClassName) && (filter.CssClassName == AppConstants.PARAM_CSS_CORPORATE_ITEM || filter.CssClassName == AppConstants.PARAM_CSS_CUSTOM_ITEM || filter.CssClassName == AppConstants.PARAM_CSS_LOCCORPORATE_ITEM || filter.CssClassName == AppConstants.PARAM_CSS_LOCCUSTOM_ITEM))
                                        {
                                            //work related to location hiererchy
                                            string filterLocationHeirerchy = ""; ;
                                            if (filter.CssClassName == AppConstants.PARAM_CSS_LOCCORPORATE_ITEM)
                                            {
                                                filterLocationHeirerchy = IcontrolFilterResources.CorporateHierarchy;

                                            }
                                            else if (filter.CssClassName == AppConstants.PARAM_CSS_LOCCUSTOM_ITEM)
                                            {
                                                filterLocationHeirerchy = IcontrolFilterResources.CustomStoreGroupHierarchy;
                                            }
                                            if ((locationHierarcgy != null && locationHierarcgy.SelectedItem.Text == filterLocationHeirerchy)
                                                || locationHierarcgy == null)
                                            {
                                                if (commmaSeperatedValues.Length > 0)
                                                {
                                                    selectedValues = commmaSeperatedValues;
                                                }
                                            }

                                            //work related to product hiererchy
                                            string filterProductHeirerchy = "";
                                            if (filter.CssClassName == AppConstants.PARAM_CSS_CORPORATE_ITEM)
                                            {
                                                filterProductHeirerchy = IcontrolFilterResources.CorporateHierarchy;
                                            }
                                            else if (filter.CssClassName == AppConstants.PARAM_CSS_CUSTOM_ITEM)
                                            {
                                                filterProductHeirerchy = IcontrolFilterResources.CustomHierarchy;
                                            }
                                            if ((productHierarchy != null && productHierarchy.SelectedItem.Text == filterProductHeirerchy)
                                                || productHierarchy == null)
                                            {
                                                if (commmaSeperatedValues.Length > 0)
                                                {
                                                    selectedValues = commmaSeperatedValues;
                                                }
                                            }

                                            if (!string.IsNullOrEmpty(selectedValues))
                                            {
                                                this.selectedFilterValues.Add(filter.ParameterName, selectedValues);
                                            }
                                            else
                                            {
                                                if (commmaSeperatedValues.Length > 0)
                                                {
                                                    this.selectedFilterValues.Add(filter.ParameterName, commmaSeperatedValues);
                                                }

                                            }
                                        }
                                        else
                                        {
                                            if (commmaSeperatedValues.Length > 0)
                                            {
                                                if (filter.ParameterName == AppConstants.PARAM_HOLIDAY_ID)
                                                {
                                                    this.selectedFilterValues.Add(filter.ParameterName, commmaSeperatedValues);
                                                    this.selectedFilterValues.Add(AppConstants.PARAM_HOLIDAY_NAME, commmaSeperatedTexts);
                                                }
                                                else
                                                {
                                                    this.selectedFilterValues.Add(filter.ParameterName, commmaSeperatedValues);
                                                }
                                            }

                                        }
                                    }
                                    if (!string.IsNullOrEmpty(commmaSeperatedTexts) && (filter.FilterLabelName.Trim() == "Banner" || filter.FilterLabelName == "Sub-Banner" || filter.FilterLabelName.Trim() == "Department" || filter.FilterLabelName.Trim() == "Category" || filter.FilterLabelName.Trim() == "CustomStoreGroup" || filter.FilterLabelName.Trim() == "HierarchyName"))
                                    {

                                        if ((locationHierarcgy != null && locationHierarcgy.SelectedItem.Text == IcontrolFilterResources.CorporateHierarchy)
                                            || (locationHierarcgy == null))
                                        {
                                            if (this.ReportId != 1319)
                                            {
                                                if (filter.FilterName.Trim().Replace(" ", "") == "LocBanner")
                                                {
                                                    this.selectedFilterValues.Add(AppConstants.PARAM_HEADER_BANNERS, commmaSeperatedTexts.Replace(",", "\\\\,"));
                                                }
                                                if (filter.FilterName.Trim().Replace(" ", "") == "LocSubBanner")
                                                {
                                                    this.selectedFilterValues.Add(AppConstants.PARAM_HEADER_SUB_BANNERS, commmaSeperatedTexts.Replace(",", "\\\\,"));
                                                }
                                            }
                                            else
                                            {
                                                if (filter.FilterName.Trim() == "LocHierDesBanner")
                                                {
                                                    this.selectedFilterValues.Add(AppConstants.PARAM_HEADER_BANNERS, commmaSeperatedTexts.Replace(",", "\\\\,"));
                                                }
                                                if (filter.FilterName.Trim().Replace(" ", "") == "LocHierDesSubBanner")
                                                {
                                                    this.selectedFilterValues.Add(AppConstants.PARAM_HEADER_SUB_BANNERS, commmaSeperatedTexts.Replace(",", "\\\\,"));
                                                }
                                            }

                                        }
                                        else if (locationHierarcgy != null && locationHierarcgy.SelectedItem.Text == IcontrolFilterResources.CustomStoreGroupHierarchy)
                                        {
                                            if (filter.FilterLabelName.Trim().Replace(" ", "") == "CustomStoreGroup")
                                            {
                                                this.selectedFilterValues.Add("CGID", commmaSeperatedTexts);
                                            }
                                        }

                                        if ((productHierarchy != null && productHierarchy.SelectedItem.Text == IcontrolFilterResources.CorporateHierarchy)
                                           || (productHierarchy == null && result == "Corporate"))
                                        {

                                            if (filter.FilterLabelName.Trim().Replace(" ", "") == "Department")
                                            {
                                                this.selectedFilterValues.Add(AppConstants.PARAM_HEADER_DEPARTMENT, commmaSeperatedTexts);
                                            }
                                            if (filter.FilterLabelName.Trim() == "Category")
                                            {
                                                this.selectedFilterValues.Add(AppConstants.PARAM_HEADER_CATEGORY, commmaSeperatedTexts);
                                            }
                                        }
                                        else if ((productHierarchy != null && productHierarchy.SelectedItem.Text == IcontrolFilterResources.CustomHierarchy)
                                            || (productHierarchy == null && result == "Custom"))
                                        {
                                            if (filter.FilterLabelName.Trim().Replace(" ", "") == "HierarchyName")
                                            {
                                                this.selectedFilterValues.Add("Hname", commmaSeperatedTexts);
                                            }
                                        }

                                    }
                                }
                                else if (!string.IsNullOrEmpty(filter.ControlName) && Convert.ToString(filter.ControlName).ToUpper() == AppConstants.INPUTCONTROLS_TEXTBOX.ToUpper())
                                {
                                    control = control as TextBox;
                                    if (!string.IsNullOrEmpty(control.Text))
                                    {
                                        if (filter.IsMultiselectORCalenderRequired)
                                        {
                                            DateTime date = DateTime.ParseExact(control.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                                            this.selectedFilterValues.Add(filter.ParameterName, date.ToString(AppConstants.GLOBAL_DATE_FORMAT));
                                        }
                                        else
                                        {
                                            this.selectedFilterValues.Add(filter.ParameterName, control.Text);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    TextBox txtVendorNumber = (TextBox)this.ControlsParent.FindControl("VendorNumber");
                    if (!DataManager.IsCorporateUser(this.LoginUserName))
                    {
                        bool is_vendor = false;
                        if (this.LoginUserProfileName == "VENDOR" && txtVendorNumber != null && !string.IsNullOrEmpty(txtVendorNumber.Text))
                        {
                            is_vendor = true;
                        }
                        this.selectedFilterValues.Add("is_vendor", is_vendor);
                    }

                    //add filters based on various report names     
                    if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_DISTRIBUTION_GAP_ANALYSIS)
                    {
                        this.selectedFilterValues.Add(AppConstants.PARAM_USERNAME, this.LoginUserName);
                    }

                    else if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_DIGITAL_ID_CUSTOMER_DASHBOARD)
                    {
                        this.selectedFilterValues.Add(AppConstants.START_DATE, this.dataAccessLayer.StartDate().ToString(AppConstants.GLOBAL_DATE_FORMAT));
                    }
                    //Genereate Execution id for Product purchase multiple.
                    else if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_PRODUCT_PURCHASE_MULTIPLES)
                    {
                        RadComboBox ddlCustomHierarchy = (RadComboBox)this.ControlsParent.FindControl("HierarchyName");
                        if (ddlCustomHierarchy != null && ddlCustomHierarchy.SelectedIndex > 0)
                        {
                            this.selectedFilterValues.Add(AppConstants.CUSTOM_HIERARCHY_NAME, this.dataAccessLayer.EncodeSpecialCharacters(ddlCustomHierarchy.Text, this.isEncodingRequired));
                        }
                        string userType = GetLoginUserType();
                        if (!string.IsNullOrEmpty(userType))
                        {

                            string Exec_Identifier = AppManager.CreateDynamicSql_ReportProductPurchaseMultiple(ReportId.ToString(), userType, this.LoginUserProfileName, this.LoginUserName, this.selectedFilterValues);

                            if (!string.IsNullOrEmpty(Exec_Identifier))
                            {
                                ViewState["Ex_Id"] = Exec_Identifier;
                                this.selectedFilterValues.Add(AppConstants.E_ID, Exec_Identifier);
                            }
                        }
                    }
                    //Genereate Execution id for Distribution gap analysis.
                    else if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_DISTRIBUTION_GAP_SALES_OPPORTUNITY)
                    {
                        string userType = GetLoginUserType();
                        if (!string.IsNullOrEmpty(userType))
                        {
                            string Exec_Identifier = AppManager.CreateDynamicSql_ReportDistributionGapSalesOopportunity(ReportId.ToString(), userType, this.LoginUserProfileName, this.LoginUserName, this.selectedFilterValues);

                            if (!string.IsNullOrEmpty(Exec_Identifier))
                            {
                                ViewState["Ex_Id"] = Exec_Identifier;
                                this.selectedFilterValues.Add(AppConstants.E_ID, Exec_Identifier);
                            }
                        }
                    }
                    else if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_PRICE_EXECUTION_AUDIT)
                    {
                        this.selectedFilterValues.Add(this.dataAccessLayer.EncodeSpecialCharacters(AppConstants.WEEK_VIEW, this.isEncodingRequired), "false");
                    }
                    else if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_DIGITAL_ID_CUSTOMER_DEMOGRAPHICS || this.ReportName.Trim().ToUpper() == AppConstants.REPORT_DIGITAL_ID_PERFORMANCE_METRICS)
                    {
                        this.selectedFilterValues.Add(this.dataAccessLayer.EncodeSpecialCharacters(AppConstants.WEEK_VIEW, this.isEncodingRequired), "true");
                    }

                    else if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_DIGITAL_ID_NEW_ITEMS)
                    {
                        TextBox txtLaunchDate = (TextBox)this.ControlsParent.FindControl("LaunchDate");
                        if (txtLaunchDate != null && !string.IsNullOrEmpty(txtLaunchDate.Text))
                        {
                            DateTime date = DateTime.ParseExact(txtLaunchDate.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                            this.selectedFilterValues.Add(AppConstants.L_DATE, Convert.ToDateTime(date).ToString(AppConstants.GLOBAL_DATE_FORMAT));
                        }

                        RadComboBox ddlCustomHierarchy = (RadComboBox)this.ControlsParent.FindControl("HierarchyName");
                        RadComboBox ddlCustomLevel1 = (RadComboBox)this.ControlsParent.FindControl("CustomLevel1NewItems");
                        if (ddlCustomHierarchy != null && ddlCustomHierarchy.SelectedIndex > 0)
                        {
                            this.selectedFilterValues.Add(AppConstants.CUSTOM_HIERARCHY_ID, this.dataAccessLayer.EncodeSpecialCharacters(ddlCustomHierarchy.Text, this.isEncodingRequired));
                            this.selectedFilterValues.Add(AppConstants.CUSTOM_HIERARCHY_NAME, this.dataAccessLayer.EncodeSpecialCharacters(ddlCustomHierarchy.Text, this.isEncodingRequired));
                        }
                        if (ddlCustomLevel1 != null && ddlCustomLevel1.SelectedIndex > 0)
                        {
                            this.selectedFilterValues.Add(AppConstants.NEW_ITEM_VALUE, this.dataAccessLayer.EncodeSpecialCharacters(ddlCustomLevel1.Text, this.isEncodingRequired));
                        }
                        RadComboBox ddlCategories = (RadComboBox)this.ControlsParent.FindControl("Category");
                        if (ddlCategories != null && ddlCategories.SelectedIndex > 0)
                        {
                            if (ddlCategories.CheckBoxes)
                            {
                                string selectedTexts = AppManager.GetSelectedItemsWithOutQuotes(ddlCategories);
                                this.selectedFilterValues.Add(AppConstants.CATEGORY, this.dataAccessLayer.EncodeSpecialCharacters(selectedTexts, this.isEncodingRequired));
                            }
                            else
                            {
                                this.selectedFilterValues.Add(AppConstants.CATEGORY, this.dataAccessLayer.EncodeSpecialCharacters(ddlCategories.SelectedItem.Text, this.isEncodingRequired));
                            }
                        }
                        this.selectedFilterValues.Add(AppConstants.PARAM_USERNAME, this.LoginUserName);
                    }

                    if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_DIGITAL_ID_DISCONTINUED_ITEMS)
                    {
                        TextBox txtDiscoDate = (TextBox)this.ControlsParent.FindControl("DiscoDate");
                        if (txtDiscoDate != null && !string.IsNullOrEmpty(txtDiscoDate.Text))
                        {
                            DateTime date = DateTime.ParseExact(txtDiscoDate.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                            this.selectedFilterValues.Add(AppConstants.D_DATE, Convert.ToDateTime(date).ToString(AppConstants.GLOBAL_DATE_FORMAT));
                        }
                        RadComboBox ddlCustomHierarchy = (RadComboBox)this.ControlsParent.FindControl("HierarchyName");
                        RadComboBox ddlCustomLevel1 = (RadComboBox)this.ControlsParent.FindControl("CustomLevel1DiscontinuedItems");
                        if (ddlCustomHierarchy != null && ddlCustomHierarchy.SelectedIndex > 0)
                        {
                            this.selectedFilterValues.Add(AppConstants.CUSTOM_HIERARCHY_ID, this.dataAccessLayer.EncodeSpecialCharacters(ddlCustomHierarchy.Text, this.isEncodingRequired));
                            this.selectedFilterValues.Add(AppConstants.CUSTOM_HIERARCHY_NAME, this.dataAccessLayer.EncodeSpecialCharacters(ddlCustomHierarchy.Text, this.isEncodingRequired));
                        }
                        if (ddlCustomLevel1 != null && ddlCustomLevel1.SelectedIndex > 0)
                        {
                            this.selectedFilterValues.Add(AppConstants.DISCO_ITEM_VALUE, this.dataAccessLayer.EncodeSpecialCharacters(ddlCustomLevel1.Text, this.isEncodingRequired));
                        }
                        this.selectedFilterValues.Add(AppConstants.PARAM_USERNAME, this.LoginUserName);

                        RadComboBox ddlCategories = (RadComboBox)this.ControlsParent.FindControl("Category");
                        if (ddlCategories != null && ddlCategories.SelectedItem != null)
                        {
                            if (ddlCategories.CheckBoxes)
                            {
                                string selectedTexts = AppManager.GetSelectedItemsWithOutQuotes(ddlCategories);
                                this.selectedFilterValues.Add(AppConstants.CATEGORY, this.dataAccessLayer.EncodeSpecialCharacters(selectedTexts, this.isEncodingRequired));
                            }
                            else
                            {
                                this.selectedFilterValues.Add(AppConstants.CATEGORY, this.dataAccessLayer.EncodeSpecialCharacters(ddlCategories.SelectedItem.Text.ToString(), this.isEncodingRequired));
                            }
                        }
                    }

                    if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_NEW_ITEM_DASHBOARD)
                    {
                        TextBox txtLaunchDate = (TextBox)this.ControlsParent.FindControl("LaunchDate");
                        if (txtLaunchDate != null)
                        {
                            DateTime date = DateTime.ParseExact(txtLaunchDate.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                            RadComboBox ddlCategories = (RadComboBox)this.ControlsParent.FindControl("Category");
                            this.selectedFilterValues.Add(AppConstants.DISCODATE, Convert.ToDateTime(this.dataAccessLayer.GetSunday(date)).ToString(AppConstants.GLOBAL_DATE_FORMAT));
                            this.selectedFilterValues.Add(AppConstants.START_DATE, Convert.ToDateTime(date).AddDays(-182).ToString(AppConstants.GLOBAL_DATE_FORMAT));
                            this.selectedFilterValues.Add(AppConstants.END_DATE, Convert.ToDateTime(date).AddDays(+181).ToString(AppConstants.GLOBAL_DATE_FORMAT));

                            if (ddlCategories != null && ddlCategories.SelectedIndex > 0)
                            {
                                if (ddlCategories.CheckBoxes)
                                {
                                    string selectedTexts = AppManager.GetSelectedItemsWithOutQuotes(ddlCategories);
                                    this.selectedFilterValues.Add(AppConstants.CATEGORY, this.dataAccessLayer.EncodeSpecialCharacters(selectedTexts, this.isEncodingRequired));
                                }
                                else
                                {
                                    this.selectedFilterValues.Add(AppConstants.CATEGORY, this.dataAccessLayer.EncodeSpecialCharacters(ddlCategories.SelectedItem.Text, this.isEncodingRequired));
                                }
                            }
                        }
                    }

                    if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_AD_HOC_HOLIDAY_SHIFT)
                    {
                        RadComboBox ddlHoliday = (RadComboBox)this.ControlsParent.FindControl("Holiday");
                        RadComboBox ddlYear = (RadComboBox)this.ControlsParent.FindControl("FiscalYear");
                        RadComboBox ddlWeekPrior = (RadComboBox)this.ControlsParent.FindControl("NumberofWeeksPrior");
                        RadComboBox ddlWeekPost = (RadComboBox)this.ControlsParent.FindControl("NumberofWeeksPost");
                        DataTable holidayStartDate = this.dataAccessLayer.GetHolidayStartDates(ddlHoliday.SelectedItem.Text, ddlWeekPrior.SelectedItem.Text.ToString(), ddlYear.SelectedItem.Text, TableName: AppConstants.DIM_CALENDAR_HOLIDAYS_CORE_WITH_WEEK_0);
                        DataTable holidayEndDate = this.dataAccessLayer.GetHolidayEndDates(ddlHoliday.SelectedItem.Text, ddlWeekPost.SelectedItem.Text, ddlYear.SelectedItem.Text, TableName: AppConstants.DIM_CALENDAR_HOLIDAYS_CORE_WITH_WEEK_0);
                        if (holidayStartDate.Rows.Count > 0)
                        {
                            this.selectedFilterValues.Add(this.dataAccessLayer.EncodeSpecialCharacters(AppConstants.START_DATE_TY, this.isEncodingRequired), Convert.ToDateTime(holidayStartDate.Rows[0]["StartDateTY"]).ToString(AppConstants.GLOBAL_DATE_FORMAT));
                            this.selectedFilterValues.Add(this.dataAccessLayer.EncodeSpecialCharacters(AppConstants.START_DATE_LY, this.isEncodingRequired), Convert.ToDateTime(holidayStartDate.Rows[0]["StartDateLY"]).ToString(AppConstants.GLOBAL_DATE_FORMAT));
                        }
                        if (holidayEndDate.Rows.Count > 0)
                        {
                            this.selectedFilterValues.Add(this.dataAccessLayer.EncodeSpecialCharacters(AppConstants.END_DATE_TY, this.isEncodingRequired), Convert.ToDateTime(holidayEndDate.Rows[0]["EndDateTY"]).ToString(AppConstants.GLOBAL_DATE_FORMAT));
                            this.selectedFilterValues.Add(this.dataAccessLayer.EncodeSpecialCharacters(AppConstants.END_DATE_LY, this.isEncodingRequired), Convert.ToDateTime(holidayEndDate.Rows[0]["EndDateLY"]).ToString(AppConstants.GLOBAL_DATE_FORMAT));
                        }

                    }
                    if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_SALES_DRIVER_BASKET)
                    {
                        RadComboBox fromFiscalWeek = (RadComboBox)this.ControlsParent.FindControl("FromFiscalWeek");
                        RadComboBox toFiscalWeek = (RadComboBox)this.ControlsParent.FindControl("ToFiscalWeek");
                        RadComboBox ddlCustomHierarchy = (RadComboBox)this.ControlsParent.FindControl("HierarchyName");
                        if (ddlCustomHierarchy != null && ddlCustomHierarchy.SelectedIndex > 0)
                        {
                            this.selectedFilterValues.Add(AppConstants.CUSTOM_HIERARCHY_NAME, this.dataAccessLayer.EncodeSpecialCharacters(ddlCustomHierarchy.Text, this.isEncodingRequired));
                        }
                        if (toFiscalWeek != null && fromFiscalWeek != null)
                        {
                            this.selectedFilterValues.Add(AppConstants.PARAM_START_DATE, this.dataAccessLayer.GetParamDate(fromFiscalWeek.SelectedValue));
                            this.selectedFilterValues.Add(AppConstants.PARAM_START_DATE_LY, this.dataAccessLayer.GetParamLYDate(fromFiscalWeek.SelectedValue));

                            this.selectedFilterValues.Add(AppConstants.PARAM_END_DATE, this.dataAccessLayer.GetParamDate(toFiscalWeek.SelectedValue, "End"));
                            this.selectedFilterValues.Add(AppConstants.PARAM_END_DATE_LY, this.dataAccessLayer.GetParamLYDate(toFiscalWeek.SelectedValue, "End"));

                            this.selectedFilterValues.Add(AppConstants.Dates, fromFiscalWeek.SelectedItem.Text + this.dataAccessLayer.EncodeSpecialCharacters(" to ", this.isEncodingRequired) + toFiscalWeek.SelectedItem.Text);

                        }
                        string userType = GetLoginUserType();
                        if (!string.IsNullOrEmpty(userType))
                        {
                            string Exec_Identifier = AppManager.CreateDynamicSql_ReportSalesDriverBasket(userType, this.LoginUserProfileName, this.LoginUserName, this.selectedFilterValues);

                            if (!string.IsNullOrEmpty(Exec_Identifier))
                            {
                                ViewState["Ex_Id"] = Exec_Identifier;
                                this.selectedFilterValues.Add(AppConstants.E_ID, Exec_Identifier);
                            }
                        }
                    }

                    if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_BUSINESS_REVIEW_DASHBOARDS)
                    {
                        RadComboBox ddlRC = (RadComboBox)this.ControlsParent.FindControl("RecentTimeframe");
                        this.selectedFilterValues.Add(AppConstants.START_DATE, this.dataAccessLayer.StartDate().ToString(AppConstants.GLOBAL_DATE_FORMAT));
                        if (ddlRC != null)
                        {
                            this.selectedFilterValues.Add(AppConstants.RC, ddlRC.SelectedItem.Text);
                            this.selectedFilterValues.Add(AppConstants.RC_DATE, this.dataAccessLayer.RCDate(ddlRC.SelectedItem.Text).ToString(AppConstants.GLOBAL_DATE_FORMAT));
                        }
                    }
                    RadioButtonList rdoDailyWeekly = (RadioButtonList)this.ControlsParent.FindControl("DailyWeekly");
                    if (rdoDailyWeekly != null)
                    {
                        bool WeeklyView = rdoDailyWeekly.SelectedValue == "1" ? false : true;
                        this.selectedFilterValues.Add(AppConstants.PARAM_WEEKLY_VIEW, WeeklyView);
                    }

                    string completeUrl = CreateTableauURL(urlName);
                    if (!string.IsNullOrEmpty(urlName) && this.selectedFilterValues.Count > 0)
                    {
                        string ChartPath = "";
                        Dictionary<string, dynamic> items = new Dictionary<string, dynamic>();
                        items = this.selectedFilterValues;
                        foreach (KeyValuePair<string, dynamic> item in items)
                        {
                            string value = "";
                            if (item.Value is bool)
                            {
                                if (item.Value)
                                    value = "True";
                                else value = "False";
                            }
                            else
                            {
                                value = item.Value.Replace(" ", "%20");
                            }

                            ChartPath += "&" + item.Key.Replace(" ", "%20") + "=" + value;

                        }

                        string sharedHierarchyType = GetSharedHierarchyType() == SharedHierarchyTypeEnum.COMBO.ToString() ? "share_Combo" : GetSharedHierarchyType() == SharedHierarchyTypeEnum.STORE.ToString() ? "PnlCustomStore" : GetSharedHierarchyType() == SharedHierarchyTypeEnum.CUSTOM.ToString() ? "SharedHierarchy" : "";

                        if (!String.IsNullOrEmpty(sharedHierarchyType))
                        {
                            ChartPath += "&" + sharedHierarchyType;
                        }

                        ViewState["Param"] = ChartPath;
                        ViewState["RepName"] = AppConstants.HTTP + AppConstants.TableauServerInstanceURL;
                        this.selectedFilterValues.Add("WorkbookUrl", completeUrl);
                        JavaScriptSerializer objSerialize = new JavaScriptSerializer();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "TeblauScript", "<script type='text/javascript'>initViz('" + objSerialize.Serialize(this.selectedFilterValues) + "');</script>", false);
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "BtnShowReport_Click: "));
            }
        }

        #endregion

        #region Show Saved Reports
        //Show saved reports
        private void ShowReport()
        {
            try
            {
                PnlshareButtons.Visible = true;
                UpdatePanelToolbar.Update();
                DataLayer dal = new DataLayer();
                String finalTebleauUrl = "";
                DataTable dt = DataManager.GetReportInfoByReportId(this.SavedReportId.ToString());
                if (dt.Rows.Count > 0)
                {
                    string tebleauUrl = dal.CreateCompleteTableauReportURL(this.LoginUserName, Convert.ToString(dt.Rows[0]["ParamName"]), mendatoryParams + "&:embed=y&:display_count=no&:showVizHome=no");
                    string queryString = Convert.ToString(dt.Rows[0]["ReportParameters"]);
                    finalTebleauUrl = tebleauUrl + queryString;
                }

                ReportContainer.InnerHtml = AppManager.GetFrameContent(finalTebleauUrl);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DyanamicFilters.cs > :ShowReport "));
            }
            finally
            {
                ClearInstances();
            }
        }

        //Clear view state and session related to saved report.
        private void ClearInstances()
        {
            ViewState["SaveRep"] = null;
            ViewState["ShowRep"] = null;
            Session["RepParam"] = null;
            ViewState["AlreadyExecuted"] = null;
        }

        #endregion

        #region Vendor Number Changed event
        //Vendor number click event when text of vendor number changed
        protected void BtnFilterForVendorNumber_Click(object sender, EventArgs e)
        {

            TextBox txtVendorNumber = (TextBox)this.ControlsParent.FindControl("VendorNumber");
            ddlDepartment = (RadComboBox)this.ControlsParent.FindControl("Department");
            ddlCategory = (RadComboBox)this.ControlsParent.FindControl("Category");
            ddlSubCategory = (RadComboBox)ControlsParent.FindControl("SubCategory");
            ddlSegment = (RadComboBox)ControlsParent.FindControl("Segment");
            ddlBrand = (RadComboBox)ControlsParent.FindControl("ProductBrand");
            ddlProductSize = (RadComboBox)ControlsParent.FindControl("ProProductSizeDesc");
            BtnShowReport.Enabled = true;
            if (txtVendorNumber != null && !string.IsNullOrEmpty(txtVendorNumber.Text))
            {
                bool isValidVendorNumber = dataAccessLayer.VenoderNumberExist(txtVendorNumber.Text);

                if (isValidVendorNumber)
                {
                    if (ddlDepartment != null)
                    {
                        ddlDepartment.Items.Clear();
                    }
                    if (ddlCategory != null)
                    {
                        ddlCategory.Items.Clear();
                    }
                    if (ddlSubCategory != null)
                    {
                        ddlSubCategory.Items.Clear();
                    }
                    if (ddlSegment != null)
                    {
                        ddlSegment.Items.Clear();
                    }
                    if (ddlBrand != null)
                    {
                        ddlBrand.Items.Clear();
                    }
                    if (ddlProductSize != null)
                    {
                        ddlProductSize.Items.Clear();
                    }
                    this.dtProductDetailsCorporateHierarchy = null;
                    //write logic of binding details according to vendor number 
                    if (DataManager.IsCorporateUser(this.LoginUserName))
                    {
                        this.dtProductDetailsCorporateHierarchy = this.dataAccessLayer.GetProductDetailsCorporateHierarchy(ReportId, this.LoginUserName, null, txtVendorNumber.Text);
                    }
                    else
                    {
                        this.dtProductDetailsCorporateHierarchy = this.dataAccessLayer.GetProductDetailsCorporateHierarchy(ReportId, this.LoginUserName, this.LoginUserProfileName, txtVendorNumber.Text);
                    }
                    string department = string.Empty;
                    if (ddlDepartment != null)
                    {
                        if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
                        {
                            DataTable dtDepartments = this.dtProductDetailsCorporateHierarchy.DefaultView.ToTable(true, AppConstants.DEPARTMENT_COLUMN);
                            ddlDepartment.DataSource = dtDepartments;
                            ddlDepartment.DataTextField = AppConstants.DEPARTMENT_COLUMN;
                            ddlDepartment.DataValueField = AppConstants.DEPARTMENT_COLUMN;
                            ddlDepartment.DataBind();

                            if (ddlDepartment.CheckBoxes)
                            {
                                ddlDepartment.Items[0].Checked = true;
                            }
                            else
                            {
                                ddlDepartment.SelectedIndex = 1;
                            }
                            department = AppManager.GetSelectedItemsWithQuotes(this.ddlDepartment);
                            this.BindCategoryDetails(department);
                        }
                    }
                    else if (ddlCategory != null)
                    {
                        if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
                        {
                            DataTable dtcategory = this.dtProductDetailsCorporateHierarchy.DefaultView.ToTable(true, AppConstants.CATEGORY_COLUMN);
                            ddlCategory.DataSource = dtcategory;
                            ddlCategory.DataValueField = AppConstants.CATEGORY_COLUMN;
                            ddlCategory.DataTextField = AppConstants.CATEGORY_COLUMN;
                            ddlCategory.DataBind();

                            string category = string.Empty;
                            if (ddlCategory.CheckBoxes)
                            {
                                ddlCategory.Items[0].Checked = true;
                            }
                            else
                            {
                                ddlCategory.SelectedIndex = 1;
                            }
                            category = AppManager.GetSelectedItemsWithQuotes(this.ddlCategory);
                            this.GetSubCategoryDetailsUsingCategory(department, category);
                        }
                    }
                }
                else
                {
                    BtnShowReport.Enabled = false;
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", @"alert('Invalid Vendor Number')", true);
                }
            }
            else
            {
                this.dtProductDetailsCorporateHierarchy = null;
                if (DataManager.IsCorporateUser(this.LoginUserName))
                {
                    this.dtProductDetailsCorporateHierarchy = this.dataAccessLayer.GetProductDetailsCorporateHierarchy(ReportId, this.LoginUserName);
                }
                else
                {
                    this.dtProductDetailsCorporateHierarchy = this.dataAccessLayer.GetProductDetailsCorporateHierarchy(ReportId, this.LoginUserName, this.LoginUserProfileName);
                }

                if (ddlDepartment != null)
                {
                    if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
                    {
                        DataTable dtDepartments = this.dtProductDetailsCorporateHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.DEPARTMENT_ID_COLUMN, AppConstants.DEPARTMENT_COLUMN });
                        ddlDepartment.DataSource = dtDepartments;
                        ddlDepartment.DataTextField = AppConstants.DEPARTMENT_COLUMN;
                        ddlDepartment.DataValueField = AppConstants.DEPARTMENT_ID_COLUMN;
                        ddlDepartment.DataBind();

                    }
                }
                else if (ddlCategory != null)
                {
                    if (this.dtProductDetailsCorporateHierarchy != null && this.dtProductDetailsCorporateHierarchy.Rows.Count > 0)
                    {
                        DataTable dtcategory = this.dtProductDetailsCorporateHierarchy.DefaultView.ToTable(true, new string[] { AppConstants.CATEGORY_ID_COLUMN, AppConstants.CATEGORY_COLUMN });
                        ddlCategory.DataSource = dtcategory;
                        ddlCategory.DataValueField = AppConstants.CATEGORY_ID_COLUMN;
                        ddlCategory.DataTextField = AppConstants.CATEGORY_COLUMN;
                        ddlCategory.DataBind();
                    }

                }

            }

        }
        #endregion

        #region Create Tableau URL

        /// <summary>
        /// Create complete URL of tableau reports
        /// </summary>
        /// <param name="urlName"></param>
        /// <returns></returns>
        /// 
        private string CreateTableauURL(string urlName)
        {
            string reportUrl = "";
            DataLayer dataLayer = new DataLayer();
            try
            {
                if (urlName.Trim().ToUpper() != AppConstants.REPORT_PRODUCT_PURCHASE_MULTIPLES)
                {
                    //check if there is radio button of daily/weekly
                    RadioButtonList rblDailyWeekly = (RadioButtonList)this.ControlsParent.FindControl("DailyWeekly");
                    bool isWeeklyExists = rblDailyWeekly != null && rblDailyWeekly.SelectedItem.Text == "Weekly" ? true : false;

                    //checking vendor number filter
                    TextBox txtVendorNumber = (TextBox)this.ControlsParent.FindControl("VendorNumber");

                    //handeling location and product Hierarchies (Custom or Corporate)
                    RadComboBox locationHierarcgy = (RadComboBox)this.ControlsParent.FindControl("HierarchyLocation");
                    bool isLocationHierarchyExists = locationHierarcgy != null ? true : false;


                    RadComboBox productHierarchy = (RadComboBox)this.ControlsParent.FindControl("Hierarchy");
                    bool isProductHierarchyExists = productHierarchy != null ? true : false;
                    string result = string.Empty;
                    if (productHierarchy == null)
                    {
                        RadComboBox category = (RadComboBox)this.ControlsParent.FindControl("Category");
                        RadComboBox hierarchyName = (RadComboBox)this.ControlsParent.FindControl("HierarchyName");
                        result = category != null ? "Corporate" : hierarchyName != null ? "Custom" : string.Empty;
                    }


                    if (rblDailyWeekly != null && !string.IsNullOrEmpty(rblDailyWeekly.Text))
                    {
                        urlName = urlName + rblDailyWeekly.SelectedItem.Text;
                    }
                    string hiererchyText = string.Empty;
                    if ((isLocationHierarchyExists && isProductHierarchyExists && locationHierarcgy.SelectedItem.Text == "Corporate Hierarchy" && productHierarchy.SelectedItem.Text == "Corporate Hierarchy")
                        || (!isLocationHierarchyExists && isProductHierarchyExists && productHierarchy.SelectedItem.Text == "Corporate Hierarchy")
                          || (!isLocationHierarchyExists && !isProductHierarchyExists && result == "Corporate"))
                    {
                        hiererchyText = "Corporate";
                    }
                    else if ((isLocationHierarchyExists && isProductHierarchyExists && locationHierarcgy.SelectedItem.Text == "Corporate Hierarchy" && productHierarchy.SelectedItem.Text == "Custom Hierarchy")
                         || (!isLocationHierarchyExists && isProductHierarchyExists && productHierarchy.SelectedItem.Text == "Custom Hierarchy")
                         || (!isLocationHierarchyExists && !isProductHierarchyExists && result == "Custom"))
                    {
                        hiererchyText = "CustomHierarchy";
                    }
                    else if (isLocationHierarchyExists && isProductHierarchyExists && locationHierarcgy.SelectedItem.Text == "Custom Store Group" && productHierarchy.SelectedItem.Text == "Corporate Hierarchy")
                    {
                        hiererchyText = "CustomStoreGroup";
                    }
                    else if (isLocationHierarchyExists && isProductHierarchyExists && locationHierarcgy.SelectedItem.Text == "Custom Store Group" && productHierarchy.SelectedItem.Text == "Custom Hierarchy")
                    {
                        hiererchyText = "Combo";
                    }
                    urlName = !string.IsNullOrEmpty(hiererchyText) ? urlName + hiererchyText : urlName;


                    if ((!string.IsNullOrEmpty(hiererchyText) && (rblDailyWeekly != null && isWeeklyExists))
                        || (rblDailyWeekly != null && !isWeeklyExists && hiererchyText == "Corporate")
                        || (!isWeeklyExists && !isLocationHierarchyExists)
                        )
                    {
                        if ((txtVendorNumber != null && string.IsNullOrEmpty(txtVendorNumber.Text)) && DataManager.IsCorporateUser(this.LoginUserName))
                        {
                            urlName = urlName + "WithoutVendorNumber";
                        }
                    }

                    RadComboBox unitSales = (RadComboBox)this.ControlsParent.FindControl("ViewBy");

                    if (unitSales != null)
                    {
                        urlName = urlName + unitSales.SelectedItem.Text;
                    }


                    if (DataManager.IsCorporateUser(this.LoginUserName))
                    {
                        urlName = urlName + "_corporate";
                    }
                    else
                    {
                        urlName = urlName + "_UAM";
                    }
                }
                ViewState["ParamName"] = urlName + AppConstants.MAIN;
                reportUrl = dataLayer.CreateCompleteTableauReportURL(this.LoginUserName, urlName, mendatoryParams);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "CreateTableauURL: "));
            }
            return reportUrl;
        }

        #endregion

        /// <summary>
        /// Get login User Type
        /// </summary>
        private string GetLoginUserType()
        {
            string UserType = "CORP_NO_VND";

            if (!DataManager.IsCorporateUser(this.LoginUserName))
            {
                if (this.LoginUserVendorNumber_FilterUser.ToLower().Contains("Select Vendor (If Relevant)".ToLower()))
                {
                    if (DataManager.GetUserRoleBasedOnProfile(Session["UserName"].ToString(), this.LoginUserProfileName) == "3")
                    {
                        UserType = "VND";
                    }
                    else
                    {
                        UserType = "PROD";
                    }
                }
                else
                {
                    UserType = "VND";
                }
            }
            else
            {
                TextBox txtVendorNumber = (TextBox)this.ControlsParent.FindControl("VendorNumber");
                if (String.IsNullOrEmpty(txtVendorNumber.Text))
                    UserType = "CORP_NO_VND";
                else
                    UserType = "CORP";
            }
            return UserType;
        }

        #region SAVE, SHARE, EXPORT

        #region SAVE REPORT
        /// <summary>
        /// Schedule Radio button selected Index Changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The instance containing the event data.</param>
        protected void rdSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdSchedule.SelectedValue == "Y")
                {
                    btnsendandsave.Visible = true;
                    PnlReportTime.Visible = true;
                    btnsend.Visible = true;
                    txtemail.Text = DataManager.GetUserEmailId(this.LoginUserName);
                    txtrepdate.Text = DateTime.UtcNow.AddDays(7 * 52).ToString("MM/dd/yyyy");
                    CalendarExtender11.Enabled = true;
                    txtrepdate.Enabled = true;
                }
                else
                {
                    btnsendandsave.Visible = true;
                    btnsend.Visible = true;
                    PnlReportTime.Visible = false;
                    txtrepdate.Text = "__/__/____";
                    txtrepdate.Enabled = false;
                    CalendarExtender11.Enabled = false;
                }
                mpeRequest.Show();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > rdSchedule_SelectedIndexChanged:"));
            }
        }

        /// <summary>
        /// Button save Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsave_click(object sender, EventArgs e)
        {
            try
            {
                ViewState["SaveSpCall"] = "";
                ViewState["DbConnection"] = "";
                SaveReportInfo();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > btnsave_click:"));
            }
        }

        /// <summary>
        /// Button save Send Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsSavSend_click(object sender, EventArgs e)
        {
            try
            {
                ViewState["sendReport"] = "1";
                ViewState["SaveSpCall"] = "";
                ViewState["DbConnection"] = "";

                if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_SALES_DRIVER_BASKET || this.ReportName.Trim().ToUpper() == AppConstants.REPORT_BUSINESS_REVIEW_DASHBOARDS)
                {
                    GetPageControls(false, false, false, true);
                }

                SaveReportInfo();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > btnsSavSend_click:"));
            }
        }

        /// <summary>
        /// Button send Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsSend_click(object sender, EventArgs e)
        {
            try
            {
                ViewState["sendReport"] = "1";
                //just do the work for batching and show the message accordingly
                SaveReportForSendNow();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > btnsSend_click:"));
            }
        }

        /// <summary>
        /// Button save Report Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnsaveRep_click(object sender, EventArgs e)
        {
            try
            {
                clearControl();
                AppManager.FillSchedule(DrpReportTime);
                FillSaveReportsValues();
                RadComboBox RecentTimeframe = (RadComboBox)this.ControlsParent.FindControl("RecentTimeframe");
                if (RecentTimeframe != null)
                {
                    pnldates.Visible = false;
                }
                upRequest.Update();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > BtnsaveRep_click:"));
            }
        }

        /// <summary>
        /// Fill Saved Report Values
        /// </summary>
        protected void FillSaveReportsValues()
        {
            try
            {
                string savedfilename = "";
                DataTable dt1 = DataManager.GetMenuName(this.ReportId.ToString());
                if (dt1.Rows.Count > 0)
                {
                    savedfilename = dt1.Rows[0]["MenuDescription"].ToString() + System.DateTime.Now.ToString("ddMMyyyy_HHmmss");
                }
                ViewState["savedfilename"] = savedfilename;
                ViewState["MenuName"] = dt1.Rows[0]["MenuName"].ToString();
                txtReportName.Text = "";
                txtReportDescr.Text = "";
                if (ViewState["TempName"] != null)
                {
                    txtReportName.Text = ViewState["TempName"].ToString();
                    ViewState["TempName"] = "";
                }

                if (ViewState["TempDescr"] != null)
                {
                    txtReportDescr.Text = ViewState["TempDescr"].ToString();
                    ViewState["TempDescr"] = "";
                }
                ViewState["SaveRep"] = "1";
                if (Request.QueryString["repid"] != null)
                {
                    DataTable ds = DataManager.GetSavedReportDetails(Request.QueryString["repid"].ToString());

                    txtReportName.Text = ds.Rows[0]["ReportName"] + "";
                    txtReportDescr.Text = ds.Rows[0]["ReportDescription"] + "";
                    if (!String.IsNullOrEmpty(Convert.ToString(ds.Rows[0]["DateRange"])))
                    {
                        pnldates.Visible = true;
                        DrpDateVal.Enabled = true;
                        string[] StrReports = ds.Rows[0]["DateRange"].ToString().Split('-');
                        int i = 1;
                        DataTable dt = new DataTable(); ;
                        dt.Columns.Add("TextName");
                        dt.Columns.Add("TextValue");
                        for (i = 1; i <= 52; i++)
                        {
                            DataRow dr = dt.NewRow();
                            dr["TextName"] = i;
                            dr["TextValue"] = i;
                            dt.Rows.Add(dr);
                        }
                        DrpDateVal.DataSource = dt;
                        DrpDateVal.DataTextField = "TextName";
                        DrpDateVal.DataValueField = "TextValue";
                        DrpDateVal.DataBind();
                        DrpDateVal.SelectedValue = StrReports[1];
                    }

                    if (!String.IsNullOrEmpty(Convert.ToString(ds.Rows[0]["Emailid"])))
                    {
                        btnsendandsave.Visible = true;
                        btnsend.Visible = true;
                        AppManager.FillSchedule(DrpReportTime);
                        PnlReportTime.Visible = true;
                        rdSchedule.SelectedValue = "Y";
                        if (!string.IsNullOrEmpty(Convert.ToString(ds.Rows[0]["ScheduleId"])))
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(ds.Rows[0]["ScheduleId"])))
                            {
                                if (DrpReportTime.Items.FindByValue(Convert.ToString(ds.Rows[0]["ScheduleId"])) != null)
                                {
                                    DrpReportTime.SelectedValue = Convert.ToString(ds.Rows[0]["ScheduleId"]);
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ds.Rows[0]["EmailID"])))
                        {
                            txtemail.Text = Convert.ToString(ds.Rows[0]["EmailID"]);
                        }
                        else
                        {
                            txtemail.Text = DataManager.GetUserEmailId(this.LoginUserName);
                        }
                        if (ds.Rows[0]["EndDate"] != null && ds.Rows[0]["EndDate"] != DBNull.Value)
                        {
                            txtrepdate.Text = Convert.ToDateTime(ds.Rows[0]["EndDate"]).ToString("MM/dd/yyyy");
                            CalendarExtender11.Enabled = true;
                            txtrepdate.Enabled = true;
                        }
                    }
                    else
                    {
                        btnsendandsave.Visible = true;
                        btnsend.Visible = true;
                        DrpDateVal.Enabled = true;
                    }
                }
                else
                {
                    txtemail.Text = DataManager.GetUserEmailId(this.LoginUserName);
                    txtrepdate.Text = DateTime.UtcNow.AddDays(7 * 52).ToString("MM/dd/yyyy");
                    //comment below code if we dont want to edit date
                    CalendarExtender11.Enabled = true;
                    txtrepdate.Enabled = true;
                    btnsendandsave.Visible = true;
                    btnsend.Visible = true;
                }

                if (ViewState["SaveClick"] != null)
                {
                    mpeRequest.Show();
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > FillSaveReportsValues:"));
            }
        }

        /// <summary>
        /// Saved Report Information
        /// </summary>
        private void SaveReportInfo()
        {
            try
            {
                string dateVal = null;
                RadComboBox RecentTimeframe = (RadComboBox)this.ControlsParent.FindControl("RecentTimeframe");
                if (RecentTimeframe == null)
                {
                    if (Convert.ToInt32(DrpDateVal.SelectedValue) > 0)
                    {
                        dateVal = "W-" + DrpDateVal.SelectedValue;
                    }
                }
                string SelectedParameters = string.Empty;

                DataTable dtFilters = dataAccessLayer.GetFiltersDataTable(ReportId);
                string reportName = dtFilters.Rows[0]["REPORT_NAME"].ToString();
                reportName = reportName.Replace(" ", "").Replace("-", "");

                SelectedParameters = GetPageControls(false, true);

                if (txtReportName.Text.Length > 0)
                {
                    if (rdSchedule.SelectedValue == "Y")
                    {
                        btnsend.Visible = true;
                        btnsendandsave.Visible = true;
                        if (string.IsNullOrEmpty(txtemail.Text.Trim()))
                        {
                            ViewState["TempName"] = txtReportName.Text.Replace("'", "''").Trim();
                            ViewState["TempDescr"] = txtReportDescr.Text.Replace("'", "''");
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                "err_msg",
                                "alert('Please enter Email Id');",
                                true);
                            ViewState["sendReport"] = null;
                            FillSaveReportsValues();
                            mpeRequest.Show();
                            return;
                        }
                        if (txtrepdate.Text == "__/__/____" || String.IsNullOrEmpty(txtrepdate.Text))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                                                                            "err_msg",
                                                                                            "alert('Please enter end date');",
                                                                                            true);
                            mpeRequest.Show();
                            return;
                        }
                    }
 

                    DataTable dt = DataManager.GetSavedReportInfoByReportNadUsername(txtReportName.Text.Replace("'", "''").Trim(), this.LoginUserName);
                    if (dt.Rows.Count > 0)
                    {
                        if (Request.QueryString["Repid"] == null)
                        {
                            mpeRequest.Show();
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('" + IcontrolFilterResources.ReportNameAlradyExist + "');", true);
                        }
                        else
                        {
                            if (Request.QueryString["Repid"].ToString() == dt.Rows[0]["Reportid"].ToString())
                            {
                                DateTime? enddate = null;
                                if (txtrepdate.Text == "__/__/____" || String.IsNullOrEmpty(txtrepdate.Text))
                                {

                                }
                                else
                                {
                                    enddate = Convert.ToDateTime(txtrepdate.Text);
                                }
                                string Emailid = "";
                                string ScheduleId = "";
                                if (rdSchedule.SelectedValue == "Y")
                                {
                                    Emailid = txtemail.Text.Replace("'", "''");
                                    ScheduleId = DrpReportTime.SelectedValue;
                                }
                                else
                                {
                                    Emailid = "";
                                    ScheduleId = "";
                                    enddate = null;
                                }

                                int sendReportStatus1 = 0;
                                if (ViewState["sendReport"] == null)
                                {
                                    sendReportStatus1 = 0;
                                }
                                else
                                {
                                    sendReportStatus1 = 1;
                                }

                                DataManager.UpdateReport(Request.QueryString["repid"].ToString(), txtReportDescr.Text.Replace("'", "''").Trim(), dateVal, ViewState["RepName"].ToString(),
                                SelectedParameters, ViewState["Param"].ToString(), ViewState["ParamName"].ToString(), enddate, Emailid, ScheduleId, sendReportStatus1);

                                if (ViewState["SaveInitial"] == null && ViewState["sendReport"] == null)
                                {
                                    clearControl();
                                    upRequest.Update();
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Confirmation Message", "alert('Report has been updated successfully.');", true);
                                    mpeRequest.Hide();
                                }

                                if (ViewState["sendReport"] != null)
                                {
                                    clearControl();
                                    upRequest.Update();
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Report has been updated successfully. Confirmation email will be send to the User(s) shortly.');", true);
                                    ViewState["sendReport"] = null;
                                    mpeRequest.Hide();
                                }
                                else
                                {
                                    mpeRequest.Hide();
                                }
                            }
                            else
                            {
                                mpeRequest.Show();
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Report Name Already Exist');", true);
                            }
                        }
                    }
                    else
                    {
                        DateTime? enddate = null;
                        if (txtrepdate.Text == "__/__/____" || String.IsNullOrEmpty(txtrepdate.Text))
                        {

                        }
                        else
                        {
                            enddate = Convert.ToDateTime(txtrepdate.Text);
                        }

                        string Emailid = "";
                        string ScheduleId = "";
                        if (rdSchedule.SelectedValue == "Y")
                        {
                            Emailid = txtemail.Text.Replace("'", "''");
                            ScheduleId = DrpReportTime.SelectedValue;
                        }
                        else
                        {
                            Emailid = "";
                            ScheduleId = "";
                            enddate = null;
                        }

                        int sendReportStatus = 0;
                        if (ViewState["sendReport"] == null)
                        {
                            sendReportStatus = 0;
                        }
                        else
                        {
                            sendReportStatus = 1;
                        }
                        string UserProfile = "";
                        if (DataManager.IsCorporateUser(this.LoginUserName))
                        {
                            UserProfile = DataManager.GetUserRoleName(this.LoginUserName).ToLower();
                        }
                        else
                        {
                            string ProfileName, VendorNumber;
                            if (Session["UserProfile"] == null || Session["VendorNumber"] == null)
                            {
                                string[] arrValues = new string[2];
                                arrValues = DataManager.GetUserProfileAndVendorNumber(this.LoginUserName);
                                ProfileName = arrValues[0].Trim();
                                VendorNumber = arrValues[1].Trim();
                            }
                            else
                            {
                                ProfileName = Session["UserProfile"].ToString().Trim();
                                VendorNumber = Session["VendorNumber"].ToString().Trim();
                            }
                            if (VendorNumber.ToLower().Contains("Select Vendor (If Relevant)".ToLower()))
                            {
                                if (DataManager.GetUserRoleBasedOnProfile(this.LoginUserName, ProfileName) == "3")
                                    UserProfile = "vendors";
                                else
                                    UserProfile = "products";
                            }
                            else
                            {
                                UserProfile = "vendors";
                            }
                        }

                        DataManager.SaveReport(txtReportName.Text.Replace("'", "''").Trim(), txtReportDescr.Text.Replace("'", "''").Trim(), this.LoginUserName,
                                                 dateVal, this.ReportId.ToString(), ViewState["RepName"].ToString(), ViewState["Param"].ToString(), ViewState["MenuName"].ToString(),
                                                  ViewState["ParamName"].ToString(), SelectedParameters, enddate, Emailid, ScheduleId, sendReportStatus, UserProfile, ViewState["SaveSpCall"].ToString().Replace("'", "%27"), ViewState["DbConnection"].ToString());

                        if (ViewState["SaveInitial"] == null && sendReportStatus == 0)
                        {
                            clearControl();
                            upRequest.Update();
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Report has been saved successfully.');", true);
                        }
                        if (ViewState["sendReport"] != null)
                        {
                            clearControl();
                            upRequest.Update();
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Report has been saved successfully. Confirmation email will be send to the User(s) shortly.');", true);
                            ViewState["sendReport"] = null;
                        }
                        mpeRequest.Hide();
                    }
                }
                else
                {
                    mpeRequest.Show();
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Please enter report name');", true);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > SaveReportInfo:"));
            }

        }

        /// <summary>
        /// Saved the Report for Send now
        /// </summary>
        private void SaveReportForSendNow()
        {
            try
            {
                string SelectedParameters = string.Empty;

                DataTable dtFilters = dataAccessLayer.GetFiltersDataTable(ReportId);
                string reportName = dtFilters.Rows[0]["REPORT_NAME"].ToString();
                reportName = reportName.Replace(" ", "").Replace("-", "");

                SelectedParameters = GetPageControls(false, true);


                if (txtReportName.Text.Length > 0)
                {
                    if (rdSchedule.SelectedValue == "Y")
                    {
                        btnsend.Visible = true;
                        btnsendandsave.Visible = true;
                        if (string.IsNullOrEmpty(txtemail.Text.Trim()))
                        {
                            ViewState["TempName"] = txtReportName.Text.Replace("'", "''").Trim();
                            ViewState["TempDescr"] = txtReportDescr.Text.Replace("'", "''");
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                "err_msg",
                                "alert('Please enter Email Id');",
                                true);
                            ViewState["sendReport"] = null;
                            FillSaveReportsValues();
                            mpeRequest.Show();
                            return;
                        }
                    }
                    else
                    {
                        //btnsend.Visible = false;
                        //btnsendandsave.Visible = false;
                    }
                    if (txtrepdate.Text == "__/__/____" || String.IsNullOrEmpty(txtrepdate.Text))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                                                                        "err_msg",
                                                                                        "alert('Please enter end date');",
                                                                                        true);
                        mpeRequest.Show();
                        return;
                    }
                    string Emailid = "";
                    if (rdSchedule.SelectedValue == "Y")
                    {
                        Emailid = txtemail.Text.Replace("'", "''");
                    }
                    else
                    {
                        Emailid = "";
                    }
                    int sendReportStatus = 0;
                    if (ViewState["sendReport"] == null)
                    {
                        sendReportStatus = 0;
                    }
                    else
                    {
                        sendReportStatus = 1;
                    }
                    string UserProfile = "";
                    if (DataManager.IsCorporateUser(this.LoginUserName))
                    {
                        UserProfile = DataManager.GetUserRoleName(this.LoginUserName).ToLower();
                    }
                    else
                    {
                        string ProfileName, VendorNumber;
                        if (Session["UserProfile"] == null || Session["VendorNumber"] == null)
                        {
                            string[] arrValues = new string[2];
                            arrValues = DataManager.GetUserProfileAndVendorNumber(this.LoginUserName);
                            ProfileName = arrValues[0].Trim();
                            VendorNumber = arrValues[1].Trim();
                        }
                        else
                        {
                            ProfileName = Session["UserProfile"].ToString().Trim();
                            VendorNumber = Session["VendorNumber"].ToString().Trim();
                        }
                        if (VendorNumber.ToLower().Contains("Select Vendor (If Relevant)".ToLower()))
                        {
                            if (DataManager.GetUserRoleBasedOnProfile(this.LoginUserName, ProfileName) == "3")
                                UserProfile = "vendors";
                            else
                                UserProfile = "products";
                        }
                        else
                        {
                            UserProfile = "vendors";
                        }
                    }

                    DataManager.SaveReportForBatching(txtReportName.Text.Replace("'", "''").Trim(), this.LoginUserName,
                                              this.ReportId.ToString(), ViewState["RepName"].ToString(), ViewState["Param"].ToString(),
                                              ViewState["ParamName"].ToString(), SelectedParameters, Emailid, sendReportStatus, UserProfile);


                    if (ViewState["sendReport"] != null)
                    {
                        clearControl();
                        upRequest.Update();
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Email will be send to the User(s) shortly.');", true);
                        ViewState["sendReport"] = null;
                    }
                    mpeRequest.Hide();
                }

                else
                {
                    mpeRequest.Show();
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Please enter report name');", true);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > SaveReportForSendNow:"));
            }

        }

        /// <summary>
        /// Clear saved report controls
        /// </summary>
        protected void clearControl()
        {
            try
            {
                rdSchedule.SelectedValue = "N";
                DrpDateVal.ClearSelection();
                txtrepdate.Enabled = false;
                txtrepdate.Text = "__/__/____";
                CalendarExtender11.Enabled = false;
                btnsendandsave.Visible = false;
                FillSaveReportsValues();
                PnlReportTime.Visible = false;
                ViewState["SaveClick"] = "1";
                ViewState["SaveRep"] = "1";
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > clearControl:"));
            }
        }

        #endregion

        #region EXPORT FILTERS
        /// <summary>
        /// Handles the export button Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_click(object sender, EventArgs e)
        {
            try
            {
                ExportFilters();
                PnlshareButtons.Visible = true;
                UpdatePanelToolbar.Update();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > btnExport_click:"));
            }
        }

        /// <summary>
        /// Export All selected filters
        /// </summary>
        public void ExportFilters()
        {
            try
            {
                GetPageControls(true);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > btnExport_click:"));
            }
        }
        #endregion

        #region Share Report

        /// <summary>
        /// Handles the share Report button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Btnsharerep_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["SaveRep"] = "1";
                string HierarchyType = GetSharedHierarchyType();
                if (HierarchyType == SharedHierarchyTypeEnum.CUSTOM.ToString())
                {
                    PnlHierarchy.Visible = true;
                }
                else
                {
                    PnlHierarchy.Visible = false;
                }
                if (HierarchyType.ToUpper() == SharedHierarchyTypeEnum.STORE.ToString())
                {
                    PnlCustomStore.Visible = true;
                }
                else
                {
                    PnlCustomStore.Visible = false;
                }

                if (HierarchyType.ToUpper() == SharedHierarchyTypeEnum.COMBO.ToString())
                    PnlComboBoth.Visible = true;
                else
                    PnlComboBoth.Visible = false;
                mpeShare.Show();
                upShare.Update();
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > Btnsharerep_Click:"));
            }
        }

        /// <summary>
        /// Handles the save share report event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsaveShare_Click(object sender, EventArgs e)
        {
            try
            {
                string reportExistedFor = string.Empty;
                string SelectedParameters = GetPageControls(false, true);

                if (RdShareUsers.CheckedItems.Count < 1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                                              "err_msg", "alert('Please select User');", true);
                    mpeShare.Show();
                    return;
                }

                foreach (RadComboBoxItem itm in RdShareUsers.CheckedItems)
                {
                    bool isReportExisted = DataManager.SharedReportNameExists(txtSharedReportName.Text.Trim(), itm.Value.ToString());
                    if (isReportExisted)
                        reportExistedFor += itm.Value.ToString() + ",";
                }

                if (!string.IsNullOrEmpty(reportExistedFor))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                                          "err_msg", "alert('Report Name Already Exist.');", true);
                    mpeShare.Show();
                    return;
                }
                string UserProfile = "";

                if (DataManager.IsCorporateUser(this.LoginUserName))
                {
                    UserProfile = DataManager.GetUserRoleName(this.LoginUserName).ToLower();
                }
                else
                {
                    string ProfileName, VendorNumber;
                    if (Session["UserProfile"] == null || Session["VendorNumber"] == null)
                    {
                        string[] arrValues = new string[2];
                        arrValues = DataManager.GetUserProfileAndVendorNumber(this.LoginUserName);
                        ProfileName = arrValues[0].Trim();
                        VendorNumber = arrValues[1].Trim();
                    }
                    else
                    {
                        ProfileName = Session["UserProfile"].ToString().Trim();
                        VendorNumber = Session["VendorNumber"].ToString().Trim();
                    }
                    if (VendorNumber.ToLower().Contains("Select Vendor (If Relevant)".ToLower()))
                    {
                        if (DataManager.GetUserRoleBasedOnProfile(this.LoginUserName, ProfileName) == "3")
                            UserProfile = "vendors";
                        else
                            UserProfile = "products";
                    }
                    else
                    {
                        UserProfile = "vendors";
                    }
                }
                string ReportName = "";
                DataTable dt1 = DataManager.GetMenuName(Convert.ToString(Request.QueryString["mid"]));
                if (dt1.Rows.Count > 0)
                    ViewState["MenuName"] = dt1.Rows[0]["MenuName"].ToString();

                if (String.IsNullOrEmpty(txtSharedReportName.Text.Trim()))
                    ReportName = dt1.Rows[0]["MenuName"].ToString();
                else
                    ReportName = txtSharedReportName.Text.Trim().Replace("'", "''");

                if (PnlHierarchy.Visible == true)
                {
                    string strMessage = ShareReport(UserProfile, SelectedParameters, ReportName);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Confirmation Message", "alert('" + strMessage + "');", true);
                }

                else if (PnlCustomStore.Visible == true)
                {
                    string storemessage = ShareReportforStore(UserProfile, SelectedParameters, ReportName);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Confirmation Message", "alert('" + storemessage + "');", true);
                }
                else if (PnlComboBoth.Visible == true)
                {
                    string message = ShareComboReport(UserProfile, SelectedParameters, ReportName);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Confirmation Message", "alert('" + message + "');", true);
                }
                // for without hierarchy selection
                else
                {
                    foreach (RadComboBoxItem ltm in RdShareUsers.CheckedItems)
                    {
                        if (ltm.Checked && !String.IsNullOrEmpty(ltm.Value.ToString()))
                        {
                            DataManager.ShareSaveReport(ReportName.Trim(), ltm.Value, this.ReportId.ToString(), ViewState["RepName"].ToString(), ViewState["Param"].ToString(), ViewState["MenuName"].ToString(), 0,
                           UserProfile, this.LoginUserName, txtMessage.Text.Replace("'", "''").Trim(), ViewState["ParamName"].ToString(), SelectedParameters);

                            string UserEmail = "";
                            UserEmail = DataManager.GetUserEmailId(ltm.Value.ToString());

                            if (!string.IsNullOrEmpty(UserEmail))
                                SendEmail(UserEmail, this.LoginUserName, ltm.Value, ReportName, txtMessage.Text.Replace("'", "''"));
                        }
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Confirmation Message", "alert('Report has been shared successfully');", true);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > btnsaveShare_Click"));
            }
            clearShareControl();
        }

        /// <summary>
        /// Cleaer Share Controls
        /// </summary>
        protected void clearShareControl()
        {
            try
            {
                txtSharedReportName.Text = "";
                txtMessage.Text = "";

                foreach (var item in RdShareUsers.CheckedItems)
                    item.Checked = false;

                RdShareUsers.SelectedIndex = -1;
                rdShareStore.SelectedIndex = -1;
                rdComboStoreHierarchy.SelectedIndex = -1;
                rdShareHierarchy.SelectedIndex = -1;

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > clearShareControl"));
            }
        }
        /// <summary>
        /// Share report method
        /// </summary>
        /// <param name="UserProfile"></param>
        /// <param name="SelectedParameters"></param>
        /// <param name="ReportName"></param>
        /// <returns></returns>
        protected string ShareReport(string UserProfile, string SelectedParameters, string ReportName)
        {
            string Message = "";
            try
            {
                int UserCount = 0;
                int ShareCount = 0;
                foreach (RadComboBoxItem ltm in RdShareUsers.CheckedItems)
                {
                    if (ltm.Checked && !String.IsNullOrEmpty(ltm.Value.ToString()))
                    {
                        UserCount += 1;

                        RadComboBox ddlCustomHierarchy = (RadComboBox)this.ControlsParent.FindControl("HierarchyName");
                        if (rdShareHierarchy.SelectedValue == "1")
                        {
                            if (DataManager.IsHierarchyShared(ddlCustomHierarchy.SelectedValue, ltm.Value.ToString()) == false)
                            {
                                DataManager.ShareHierarchy_Owned(ddlCustomHierarchy.SelectedValue, ltm.Value);
                            }
                        }
                        if (DataManager.IsHierarchyShared(ddlCustomHierarchy.SelectedValue, ltm.Value.ToString()))
                        {
                            DataManager.ShareSaveReport(ReportName.Trim(), ltm.Value, this.ReportId.ToString(), ViewState["RepName"].ToString(), ViewState["Param"].ToString(), ViewState["MenuName"].ToString(), 0,
                                UserProfile, this.LoginUserName, txtMessage.Text.Replace("'", "''").Trim(), ViewState["ParamName"].ToString(), SelectedParameters);

                            ShareCount += 1;

                            string UserEmail = "";
                            UserEmail = DataManager.GetUserEmailId(ltm.Value.ToString());

                            if (!string.IsNullOrEmpty(UserEmail))
                                SendEmail(UserEmail, this.LoginUserName, ltm.Value, ReportName, txtMessage.Text.Replace("'", "''"));

                        }
                    }
                }
                if (ShareCount == UserCount)
                {
                    if (RdShareUsers.CheckedItems.Count > 1)
                    {
                        Message = "Report has been shared with all the selected users.";
                    }
                    else
                    {
                        Message = "Report has been shared with the selected user.";
                    }
                }
                else if (ShareCount > 0)
                    Message = "Report has been shared with " + ShareCount + " user(s) out of " + UserCount;
                else
                    Message = "Report could not be shared with any user(s) as hierarchy is not shared.";
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > ShareReport"));
            }
            return Message;
        }

        /// <summary>
        /// Share report method for custom store
        /// </summary>
        /// <param name="UserProfile"></param>
        /// <param name="SelectedParameters"></param>
        /// <param name="ReportName"></param>
        /// <returns></returns>
        protected string ShareReportforStore(string UserProfile, string SelectedParameters, string ReportName)
        {
            string Message = "";
            try
            {
                int UserCount = 0;
                int ShareCount = 0;
                bool _isShareStore = false;
                int NotAuth_UsersCount = 0;
                String NotAuth_SharedUsers = string.Empty;
                foreach (RadComboBoxItem ltm in RdShareUsers.CheckedItems)
                {
                    if (ltm.Checked && !String.IsNullOrEmpty(ltm.Value.ToString()))
                    {
                        UserCount += 1;
                        RadComboBox CustomStoreGroup = (RadComboBox)this.ControlsParent.FindControl("LocationStore");

                        foreach (RadComboBoxItem store in CustomStoreGroup.CheckedItems)
                        {
                            if (rdShareStore.SelectedValue == "1")
                            {
                                if (DataManager.IsStoreShared(store.Value, ltm.Value.ToString()) == false)
                                {
                                    if (DataManager.IsCorporateUser(this.LoginUserName))
                                    {
                                        DataManager.SharedCustomStore_Owned(store.Value, ltm.Value);
                                    }
                                    else
                                    {
                                        if (DataManager.IsOwnerOfStoreGroup(store.Value))
                                        {
                                            Int32 UnauthorizedStoreNumber = DataManager.OnShareValidateStoreNumber(store.Value);
                                            if (UnauthorizedStoreNumber == 0)
                                            {
                                                DataManager.SharedCustomStore_Owned(store.Value, ltm.Value);
                                            }
                                            else
                                            {
                                                NotAuth_UsersCount += 1;
                                                String _NAME = DataManager.GetUsername(ltm.Value.ToString());
                                                NotAuth_SharedUsers += " " + NotAuth_UsersCount + ") " + ltm.Value.ToString() + " (" + _NAME + ")" + " : " + UnauthorizedStoreNumber + " Store(s)\\n";
                                            }
                                        }
                                    }
                                }
                            }
                            if (DataManager.IsStoreShared(store.Value, ltm.Value.ToString()))
                                _isShareStore = true;
                        }

                        if (_isShareStore)
                        {
                            DataManager.ShareSaveReport(ReportName.Trim(), ltm.Value, this.ReportId.ToString(), ViewState["RepName"].ToString(), ViewState["Param"].ToString(), ViewState["MenuName"].ToString(), 0,
                                   UserProfile, this.LoginUserName, txtMessage.Text.Replace("'", "''").Trim(), ViewState["ParamName"].ToString(), SelectedParameters);

                            string UserEmail = "";
                            UserEmail = DataManager.GetUserEmailId(ltm.Value.ToString());

                            if (!string.IsNullOrEmpty(UserEmail))
                                SendEmail(UserEmail, this.LoginUserName, ltm.Value, ReportName, txtMessage.Text.Replace("'", "''"));

                            ShareCount += 1;
                        }
                    }
                }

                if (ShareCount == UserCount)
                {
                    if (RdShareUsers.CheckedItems.Count > 1)
                        Message = "Report has been shared with all the selected users.";
                    else
                        Message = "Report has been shared with the selected user.";
                }
                else if (ShareCount > 0)
                    Message = "Report has been shared with " + ShareCount + " user(s) out of " + UserCount;
                else
                {
                    if (rdShareStore.SelectedValue == "1")
                        Message = "This report can not be shared because it uses a shared custom store.";
                    else
                        Message = "Report could not be shared with user(s) as Store does not exist for the user.";
                }

                if (!String.IsNullOrEmpty(NotAuth_SharedUsers))
                {
                    Message = "Following Users are not authorized to view some of the Store(s) in this custom store group:\\n" + NotAuth_SharedUsers;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > ShareReport"));
            }
            return Message;
        }

        /// <summary>
        /// Share report method for combo
        /// </summary>
        /// <param name="UserProfile"></param>
        /// <param name="SelectedParameters"></param>
        /// <param name="ReportName"></param>
        /// <returns></returns>
        protected string ShareComboReport(string UserProfile, string SelectedParameters, string ReportName)
        {
            string Message = "";
            try
            {
                int UserCount = 0;
                int ShareCount = 0;
                RadComboBox ddlCustomStoreGroup = (RadComboBox)this.ControlsParent.FindControl("LocationStore");
                RadComboBox ddlCustomHierarchy = (RadComboBox)this.ControlsParent.FindControl("HierarchyName");

                foreach (RadComboBoxItem ltm in RdShareUsers.CheckedItems)
                {
                    int StoreShared = 0;
                    bool isHierarchyShared = false;
                    if (ltm.Checked && !String.IsNullOrEmpty(ltm.Value.ToString()))
                    {
                        UserCount += 1;
                        foreach (RadComboBoxItem ltm1 in ddlCustomStoreGroup.CheckedItems)
                        {
                            if (rdComboStoreHierarchy.SelectedValue == "1")
                            {
                                if (DataManager.IsStoreShared(ltm1.Value, ltm.Value.ToString()) == false)
                                {
                                    DataManager.SharedCustomStore_Owned(ltm1.Value, ltm.Value);
                                    StoreShared++;
                                }
                            }
                            //for 2nd option
                            if (DataManager.IsStoreShared(ltm1.Value, ltm.Value.ToString()))
                                StoreShared++;
                        }
                        if (rdComboStoreHierarchy.SelectedValue == "1")
                        {
                            if (DataManager.IsHierarchyShared(ddlCustomHierarchy.SelectedValue, ltm.Value.ToString()) == false)
                            {
                                DataManager.ShareHierarchy_Owned(ddlCustomHierarchy.SelectedValue, ltm.Value);
                                isHierarchyShared = true;
                            }
                        }
                        //for 2nd option
                        if (DataManager.IsHierarchyShared(ddlCustomHierarchy.SelectedValue, ltm.Value.ToString()))
                            isHierarchyShared = true;
                        else
                            isHierarchyShared = false;

                        if (isHierarchyShared && StoreShared > 0)
                        {
                            DataManager.ShareSaveReport(ReportName.Trim(), ltm.Value, this.ReportId.ToString(), ViewState["RepName"].ToString(), ViewState["Param"].ToString(), ViewState["MenuName"].ToString(), 0,
                                UserProfile, this.LoginUserName, txtMessage.Text.Replace("'", "''").Trim(), ViewState["ParamName"].ToString(), SelectedParameters);

                            ShareCount += 1;

                            string UserEmail = "";
                            UserEmail = DataManager.GetUserEmailId(ltm.Value.ToString());

                            if (!string.IsNullOrEmpty(UserEmail))
                                SendEmail(UserEmail, this.LoginUserName, ltm.Value, ReportName, txtMessage.Text.Replace("'", "''"));
                        }
                    }
                }
                if (ShareCount == UserCount)
                {
                    if (RdShareUsers.CheckedItems.Count > 1)
                        Message = "Report has been shared with all the selected users.";
                    else
                        Message = "Report has been shared with the selected user.";
                }
                else if (ShareCount > 0)
                    Message = "Report has been shared with " + ShareCount + " user(s) out of " + UserCount;
                else
                    Message = "Report could not be shared with any user(s) as either Hierarchy or Store is not shared.";
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > ShareComboReport"));
            }

            return Message;
        }

        /// <summary>
        /// Send email method
        /// </summary>
        /// <param name="ToAddress"></param>
        /// <param name="SharedBy"></param>
        /// <param name="ShareTo"></param>
        /// <param name="ReportName"></param>
        /// <param name="ReportMessage"></param>
        public static void SendEmail(string ToAddress, string SharedBy, string ShareTo, string ReportName, string ReportMessage)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                String _EmailSubject = SharedBy + " has shared a report with you.";
                string sbMessage = "<body>  <br/><br/> ";
                sbMessage += "<table>";
                sbMessage += "<tr>";
                sbMessage += "<td>Dear " + ShareTo + ",</td><td></td>";
                sbMessage += "</tr>";
                sbMessage += "<tr style='padding-top:20px;'>";
                sbMessage += "<td colspan='2'>" + SharedBy + " has shared a report with you with the following details.</td>";
                sbMessage += "</tr>";
                sbMessage += "<tr style='padding-top:10px;'><td>Report Name:</td><td> " + ReportName + "</td></tr>";
                sbMessage += "<tr><td>Message:</td><td>" + ReportMessage + "</td></tr>";
                sbMessage += "<tr style='padding-top:20px;'>";
                sbMessage += "<td> Thanks</td><td></td>";
                sbMessage += "</tr>";
                sbMessage += "<tr>";
                sbMessage += "<td>" + SharedBy + "</td><td></td>";
                sbMessage += "</tr>";
                sbMessage += "</table> </body> ";
                string ccEmail = Convert.ToString(ConfigurationManager.AppSettings["EmailCc"]);
                AppManager.SendEmail(sbMessage.ToString(), _EmailSubject, "info@icontroldsd.com", ToAddress, "", ccEmail);// ToAddress

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DynamicFilters.aspx.cs > SendEmail"));

            }
        }
        #endregion

        #endregion

        #region Get Page Controls and Shared Hieararchy type

        /// <summary>
        /// Get page controls
        /// </summary>
        /// <param value='IsExportFilters'>check for exportfilter</param>
        /// <param value='FillSavedFilters'>check for SavedFilters</param>
        ///  <param value='InserParameters'>check for InserParameters</param>>
        private string GetPageControls(bool IsExportFilters = false, bool IsSavedFilters = false, bool FillSavedFilters = false, bool InserParameters = false)
        {

            RadComboBox FiscalYear = (RadComboBox)this.ControlsParent.FindControl("FiscalYear");
            RadComboBox FromAdWeek = (RadComboBox)this.ControlsParent.FindControl("FromAdWeek");
            RadComboBox ToAdWeek = (RadComboBox)this.ControlsParent.FindControl("ToAdWeek");
            TextBox VendorNumber = (TextBox)this.ControlsParent.FindControl("VendorNumber");
            RadComboBox LocBanner = (RadComboBox)this.ControlsParent.FindControl("LocBanner");
            RadComboBox LocSubBanner = (RadComboBox)this.ControlsParent.FindControl("LocSubBanner");
            RadComboBox Department = (RadComboBox)this.ControlsParent.FindControl("Department");
            RadComboBox Category = (RadComboBox)this.ControlsParent.FindControl("Category");
            RadComboBox SubCategory = (RadComboBox)this.ControlsParent.FindControl("SubCategory");
            RadComboBox Segment = (RadComboBox)this.ControlsParent.FindControl("Segment");
            RadComboBox ProductBrand = (RadComboBox)this.ControlsParent.FindControl("ProductBrand");
            RadComboBox ProProductSizeDesc = (RadComboBox)this.ControlsParent.FindControl("ProProductSizeDesc");
            RadComboBox BrandType = (RadComboBox)this.ControlsParent.FindControl("BrandType");
            RadComboBox RecentTimeframe = (RadComboBox)this.ControlsParent.FindControl("RecentTimeframe");
            RadComboBox CustomLevel1 = (RadComboBox)this.ControlsParent.FindControl("CustomLevel1");
            RadComboBox CustomLevel2 = (RadComboBox)this.ControlsParent.FindControl("CustomLevel2");
            RadComboBox CustomLevel3 = (RadComboBox)this.ControlsParent.FindControl("CustomLevel3");
            RadComboBox CustomBrand = (RadComboBox)this.ControlsParent.FindControl("CustomBrand");
            RadComboBox CustomProductSizeDesc = (RadComboBox)this.ControlsParent.FindControl("CustomProductSizeDesc");
            RadComboBox ViewBy = (RadComboBox)this.ControlsParent.FindControl("ViewBy");
            RadComboBox Hierarchy = (RadComboBox)this.ControlsParent.FindControl("Hierarchy");
            TextBox From = (TextBox)this.ControlsParent.FindControl("From");
            TextBox To = (TextBox)this.ControlsParent.FindControl("To");
            RadComboBox HierarchyName = (RadComboBox)this.ControlsParent.FindControl("HierarchyName");
            RadComboBox Holiday = (RadComboBox)this.ControlsParent.FindControl("Holiday");
            RadComboBox NumberofWeeksPrior = (RadComboBox)this.ControlsParent.FindControl("NumberofWeeksPrior");
            RadComboBox NumberofWeeksPost = (RadComboBox)this.ControlsParent.FindControl("NumberofWeeksPost");
            RadComboBox FromFiscalWeek = (RadComboBox)this.ControlsParent.FindControl("FromFiscalWeek");
            RadComboBox ToFiscalWeek = (RadComboBox)this.ControlsParent.FindControl("ToFiscalWeek");
            RadComboBox LocHierSrcBanner = (RadComboBox)this.ControlsParent.FindControl("LocHierSrcBanner");
            RadComboBox LocHierSrcSubBanner = (RadComboBox)this.ControlsParent.FindControl("LocHierSrcSubBanner");
            RadComboBox SrcStore = (RadComboBox)this.ControlsParent.FindControl("SrcStore");
            RadComboBox LocHierDesBanner = (RadComboBox)this.ControlsParent.FindControl("LocHierDesBanner");
            RadComboBox LocHierDesSubBanner = (RadComboBox)this.ControlsParent.FindControl("LocHierDesSubBanner");
            RadComboBox DesStore = (RadComboBox)this.ControlsParent.FindControl("DesStore");
            RadComboBox POG = (RadComboBox)this.ControlsParent.FindControl("POG");
            TextBox LaunchDate = (TextBox)this.ControlsParent.FindControl("LaunchDate");
            RadComboBox NumberofWeeks = (RadComboBox)this.ControlsParent.FindControl("NumberofWeeks");
            RadComboBox CustomLevel1NewItems = (RadComboBox)this.ControlsParent.FindControl("CustomLevel1NewItems");
            RadComboBox CustomLevel1DiscontinuedItems = (RadComboBox)this.ControlsParent.FindControl("CustomLevel1DiscontinuedItems");
            TextBox DiscontinuedDate = (TextBox)this.ControlsParent.FindControl("DiscontinuedDate");
            RadComboBox HierarchyLocation = (RadComboBox)this.ControlsParent.FindControl("HierarchyLocation");
            RadComboBox LocationStore = (RadComboBox)this.ControlsParent.FindControl("LocationStore");
            RadComboBox LocationCustom1 = (RadComboBox)this.ControlsParent.FindControl("LocationCustom1");
            RadComboBox LocationCustom2 = (RadComboBox)this.ControlsParent.FindControl("LocationCustom2");
            RadComboBox LocationCustom3 = (RadComboBox)this.ControlsParent.FindControl("LocationCustom3");
            RadioButtonList DailyWeekly = (RadioButtonList)this.ControlsParent.FindControl("DailyWeekly");

            string selectedParametres = "";

            #region EXPORT FILTERS LOGIC
            if (IsExportFilters)
            {

                PDFExport.ExportFilters(this.ReportId.ToString(), FiscalYear, FromAdWeek, ToAdWeek, VendorNumber, LocBanner, LocSubBanner, Department, Category, SubCategory, Segment, ProductBrand, ProProductSizeDesc,
                    BrandType, RecentTimeframe, CustomLevel1, CustomLevel2, CustomLevel3, CustomBrand, CustomProductSizeDesc, ViewBy, From, To, HierarchyName, Holiday, NumberofWeeksPrior, NumberofWeeksPost,
                    FromFiscalWeek, ToFiscalWeek, LocHierSrcBanner, LocHierSrcSubBanner, SrcStore, LocHierDesBanner, LocHierDesSubBanner, DesStore, POG, LaunchDate,
                    NumberofWeeks, CustomLevel1NewItems, CustomLevel1DiscontinuedItems, DiscontinuedDate, HierarchyLocation, LocationStore, LocationCustom1, LocationCustom2, LocationCustom3,
                    DailyWeekly, Hierarchy);
            }
            #endregion  EXPORT FILTERS LOGIC

            #region RETREIVE SELECTED PAGE FILTERS(STRING)
            else if (IsSavedFilters)
            {

                string ExecutionId = "";

                if (ViewState["Ex_Id"] != null)
                {
                    ExecutionId = Convert.ToString(ViewState["Ex_Id"]);
                }

                selectedParametres = PDFExport.SaveSelectedFilters(FiscalYear, FromAdWeek, ToAdWeek, VendorNumber, LocBanner, LocSubBanner, Department, Category, SubCategory, Segment, ProductBrand, ProProductSizeDesc,
                    BrandType, RecentTimeframe, CustomLevel1, CustomLevel2, CustomLevel3, CustomBrand, CustomProductSizeDesc, ViewBy, From, To, HierarchyName, Holiday, NumberofWeeksPrior, NumberofWeeksPost,
                    FromFiscalWeek, ToFiscalWeek, LocHierSrcBanner, LocHierSrcSubBanner, SrcStore, LocHierDesBanner, LocHierDesSubBanner, DesStore, POG, LaunchDate,
                    NumberofWeeks, CustomLevel1NewItems, CustomLevel1DiscontinuedItems, DiscontinuedDate, HierarchyLocation, LocationStore, LocationCustom1, LocationCustom2, LocationCustom3,
                    DailyWeekly, Hierarchy, ExecutionId);
            }
            #endregion  RETREIVE SELECTED PAGE FILTERS(STRING)

            #region BIND PAGE FILTERS CONTROLS,FOR SAVED REPORTS
            else if (FillSavedFilters)
            {

                PDFExport.FillSavedFilters(this.SavedReportId.ToString(), this.ReportId.ToString(), this.LoginUserName, this.LoginUserProfileName, FiscalYear, FromAdWeek, ToAdWeek, VendorNumber, LocBanner, LocSubBanner, Department, Category, SubCategory, Segment, ProductBrand, ProProductSizeDesc,
                    BrandType, RecentTimeframe, CustomLevel1, CustomLevel2, CustomLevel3, CustomBrand, CustomProductSizeDesc, ViewBy, From, To, HierarchyName, Holiday, NumberofWeeksPrior, NumberofWeeksPost,
                    FromFiscalWeek, ToFiscalWeek, LocHierSrcBanner, LocHierSrcSubBanner, SrcStore, LocHierDesBanner, LocHierDesSubBanner, DesStore, POG, LaunchDate,
                    NumberofWeeks, CustomLevel1NewItems, CustomLevel1DiscontinuedItems, DiscontinuedDate, HierarchyLocation, LocationStore, LocationCustom1, LocationCustom2, LocationCustom3,
                    DailyWeekly, Hierarchy, this.dtBannerAndSubBannerDetails, this.dtProductDetailsCustomHierarchy, dtLocationDetailsCustomHierarchy);
            }
            #endregion  BIND PAGE FILTERS CONTROLS,FOR SAVED REPORTS

            #region INSERT StoreProceedures ANd DbConnection Detials for Saome reports
            else if (InserParameters)
            {

                if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_SALES_DRIVER_BASKET)
                {

                    string ExecutionId = "";
                    string Start_date = null;
                    string End_Date = null;
                    string BANNER_ID_LIST = null;
                    string SUB_BANNER_ID_LIST = null;
                    string CATEGORY_ID_LIST = null;
                    string SUB_CATEGORY_ID_LIST = null;
                    string SEGMENT_ID_LIST = null;
                    string BRAND_ID_LIST = null;
                    string PRODUCT_SIZE_LIST = null;
                    string PRIVATE_LABEL = null;
                    string UPC_LIST = null;
                    string HIERARCHY_NAME = null;
                    string HIERARCHY_LEVEL1 = null;
                    string HIERARCHY_LEVEL2 = null;
                    string HIERARCHY_LEVEL3 = null;
                    string SalesBy = null;
                    Int64 Vendorno = 0;

                    Start_date = AppManager.GetParamDate(FromFiscalWeek.SelectedValue.ToString());
                    End_Date = AppManager.GetParamDate(ToFiscalWeek.SelectedValue.ToString(), "End");
                    if (LocBanner.CheckedItems.Count > 0)
                    {
                        BANNER_ID_LIST = AppManager.Banner(LocBanner);
                    }

                    if (LocSubBanner.CheckedItems.Count > 0 && AppManager.SubBanner(LocSubBanner) != "empty")
                    {
                        SUB_BANNER_ID_LIST = AppManager.SubBanner(LocSubBanner);
                    }
                    if (Hierarchy.SelectedItem.Text == IcontrolFilterResources.CorporateHierarchy)
                    {
                        if (Category.SelectedItem != null)
                        {
                            if (Category.SelectedIndex > 0)
                                CATEGORY_ID_LIST = AppManager.SingleCategory(Category);

                        }
                        if (SubCategory.CheckedItems.Count > 0 && AppManager.SubCategory(SubCategory) != "empty")
                            SUB_CATEGORY_ID_LIST = AppManager.SubCategory(SubCategory);

                        if (Segment.CheckedItems.Count > 0 && AppManager.Segment(Segment) != "empty")
                            SEGMENT_ID_LIST = AppManager.Segment(Segment);

                        if (BrandType.SelectedValue != "0")
                        {
                            if (BrandType.SelectedValue == "Yes")
                                PRIVATE_LABEL = "''Y''";
                            if (BrandType.SelectedValue == "No,NO,~~null~")
                                PRIVATE_LABEL = "''N''";
                        }
                    }
                    else if (Hierarchy.SelectedItem.Text == IcontrolFilterResources.CustomHierarchy)
                    {

                        if (HierarchyName.SelectedIndex > 0)
                        {
                            HIERARCHY_NAME = HierarchyName.SelectedItem.Text;
                        }

                        if (CustomLevel1.CheckedItems.Count > 0 && AppManager.CustomLevel1(CustomLevel1) != "empty")
                        {
                            HIERARCHY_LEVEL1 = AppManager.GetSelectedItemsWithDoubleQuotes(CustomLevel1, true);
                        }

                        if (CustomLevel2.CheckedItems.Count > 0 && AppManager.CustomLevel2(CustomLevel2) != "empty")
                        {
                            HIERARCHY_LEVEL2 = AppManager.GetSelectedItemsWithDoubleQuotes(CustomLevel2, true);
                        }

                        if (CustomLevel3.CheckedItems.Count > 0 && AppManager.CustomLevel3(CustomLevel3) != "empty")
                        {
                            HIERARCHY_LEVEL3 = AppManager.GetSelectedItemsWithDoubleQuotes(CustomLevel3, true);
                        }
                    }
                    if (ProductBrand.CheckedItems.Count > 0 && AppManager.Brand(ProductBrand) != "empty")
                    {
                        BRAND_ID_LIST = AppManager.GetBrandIds(ProductBrand);
                    }



                    if (ProProductSizeDesc.CheckedItems.Count > 0 && AppManager.ProductSizeDesc(ProProductSizeDesc) != "empty")
                    {
                        PRODUCT_SIZE_LIST = AppManager.GetSelectedItemsWithDoubleQuotes(ProProductSizeDesc, true);
                    }

                    if (ViewBy.SelectedItem.Text == "Sales")
                        SalesBy = "1";


                    string connectionString = "";
                    string spName = "";

                    if (ViewState["Ex_Id"] != null)
                    {
                        ExecutionId = Convert.ToString(ViewState["Ex_Id"]);
                    }
                    string ProfileName_Param = "";
                    if (DataManager.GetUserRoleBasedOnProfile(Session["UserName"].ToString(), this.LoginUserProfileName) != "3")
                    {
                        ProfileName_Param = LoginUserProfileName;
                    }

                    bool result = DataManager.InsertParameters_SalesDriverBasket(ExecutionId, Start_date, End_Date, BANNER_ID_LIST, SUB_BANNER_ID_LIST, CATEGORY_ID_LIST,
                        SUB_CATEGORY_ID_LIST, SEGMENT_ID_LIST, BRAND_ID_LIST, PRODUCT_SIZE_LIST, PRIVATE_LABEL, UPC_LIST, HIERARCHY_NAME, HIERARCHY_LEVEL1,
                        HIERARCHY_LEVEL2, HIERARCHY_LEVEL3, Vendorno, GetLoginUserType(), this.LoginUserName, ProfileName_Param, SalesBy, ref connectionString, ref spName);
                    if (result)
                    {
                        ViewState["SaveSpCall"] = spName;
                        ViewState["DbConnection"] = connectionString;
                        ViewState["AlreadyExecuted"] = "1";
                    }
                }
                else if (this.ReportName.Trim().ToUpper() == AppConstants.REPORT_BUSINESS_REVIEW_DASHBOARDS)
                {
                    string Exec_Identifier = "";
                    Int64 Vendorno = 0;
                    if (!String.IsNullOrEmpty(VendorNumber.Text.Trim()))
                        Vendorno = Convert.ToInt32(VendorNumber.Text.Trim());
                    else
                    {
                        if (ViewState["vendorno"] != null)
                        {
                            Vendorno = Convert.ToInt64(ViewState["vendorno"]);
                        }
                    }

                    string UserName_Param = this.LoginUserName;

                    string Start_date = null;
                    string BANNER_ID_LIST = null;
                    string SUB_BANNER_ID_LIST = null;
                    string CATEGORY_ID_LIST = null;
                    string SUB_CATEGORY_ID_LIST = null;
                    string SEGMENT_ID_LIST = null;
                    string PRIVATE_LABEL = null;
                    string SalesBy = null;
                    string TimeFrame = null;
                    int IsBurst = 0;

                    if (RecentTimeframe.SelectedItem.Text == "Period")
                        TimeFrame = "P";
                    else
                        TimeFrame = "Q";


                    Start_date = DateTime.Now.ToString("yyyy-MM-dd");
                    if (LocBanner.CheckedItems.Count > 0)
                    {
                        BANNER_ID_LIST = AppManager.Banner(LocBanner);
                    }

                    if (LocSubBanner.CheckedItems.Count > 0 && AppManager.SubBanner(LocSubBanner) != "empty")
                    {
                        SUB_BANNER_ID_LIST = AppManager.SubBanner(LocSubBanner);
                    }

                    if (Category.CheckedItems.Count > 0)
                        CATEGORY_ID_LIST = AppManager.GetCategoryIds(Category).Replace(" ", "%20");

                    if (SubCategory.CheckedItems.Count > 0 && AppManager.SubCategory(SubCategory) != "empty")
                        SUB_CATEGORY_ID_LIST = AppManager.SubCategory(SubCategory);

                    if (Segment.CheckedItems.Count > 0 && AppManager.Segment(Segment) != "empty")
                        SEGMENT_ID_LIST = AppManager.Segment(Segment);

                    if (BrandType.SelectedValue != "0")
                    {
                        if (BrandType.SelectedValue == "Yes")
                            PRIVATE_LABEL = "''Y''";
                        if (BrandType.SelectedValue == "No,NO,~~null~")
                            PRIVATE_LABEL = "''N''";
                    }

                    if (ViewBy != null && ViewBy.SelectedItem.Text == "Sales")
                        SalesBy = "1";

                    if (ViewState["Ex_Id"] != null)
                    {
                        Exec_Identifier = Convert.ToString(ViewState["Ex_Id"]);
                    }
                    string connectionString = "";
                    string spName = "";
                    string ProfileName_Param = "";
                    if (DataManager.GetUserRoleBasedOnProfile(Session["UserName"].ToString(), this.LoginUserProfileName) != "3")
                    {
                        ProfileName_Param = LoginUserProfileName;
                    }
                    bool result = DataManager.InsertParameters_BusinessReviewDashboard(Exec_Identifier, Start_date, TimeFrame, BANNER_ID_LIST, SUB_BANNER_ID_LIST,
                         CATEGORY_ID_LIST, SUB_CATEGORY_ID_LIST, SEGMENT_ID_LIST, PRIVATE_LABEL, GetLoginUserType(), UserName_Param, ProfileName_Param,
                         SalesBy, Vendorno, IsBurst, ref connectionString, ref spName);
                    if (result)
                    {

                        ViewState["SaveSpCall"] = spName;
                        ViewState["DbConnection"] = connectionString;
                    }

                }
                #endregion INSERT StoreProceedures ANd DbConnection Detials for Saome reports

            }
            return selectedParametres;

        }

        /// <summary>
        /// Get Shared Hierarchy Type.
        /// </summary>
        /// <returns></returns>
        private string GetSharedHierarchyType()
        {
            string hierarchyType = SharedHierarchyTypeEnum.CORPORATE.ToString();
            RadComboBox HierarchyLocation = (RadComboBox)this.ControlsParent.FindControl("HierarchyLocation");
            RadComboBox ProductHierarchy = (RadComboBox)this.ControlsParent.FindControl("Hierarchy");
            RadComboBox HierarchyName = (RadComboBox)this.ControlsParent.FindControl("HierarchyName");

            if ((HierarchyLocation != null && HierarchyLocation.SelectedItem.Text == IcontrolFilterResources.CustomStoreGroupHierarchy)
                && (ProductHierarchy != null && ProductHierarchy.SelectedItem.Text == IcontrolFilterResources.CustomHierarchy))
            {
                hierarchyType = SharedHierarchyTypeEnum.COMBO.ToString();
            }
            else if ((HierarchyLocation != null && HierarchyLocation.SelectedItem.Text == IcontrolFilterResources.CustomStoreGroupHierarchy)
               && (ProductHierarchy != null && ProductHierarchy.SelectedItem.Text == IcontrolFilterResources.CorporateHierarchy))
            {
                hierarchyType = SharedHierarchyTypeEnum.STORE.ToString();
            }

            else if (((HierarchyLocation != null && HierarchyLocation.SelectedItem.Text == IcontrolFilterResources.CorporateHierarchy)
                && (ProductHierarchy != null && ProductHierarchy.SelectedItem.Text == IcontrolFilterResources.CustomHierarchy)
                || (HierarchyLocation == null && ProductHierarchy != null && ProductHierarchy.SelectedItem.Text == IcontrolFilterResources.CustomHierarchy)
                || (HierarchyLocation == null && ProductHierarchy == null && HierarchyName != null))
                )
            {
                hierarchyType = SharedHierarchyTypeEnum.CUSTOM.ToString();
            }

            return hierarchyType;

        }

        #endregion
    }
    /// <summary>
    /// Shared Hieararchy type Enum
    /// </summary>
    public enum SharedHierarchyTypeEnum
    {
        CUSTOM,
        COMBO,
        STORE,
        CORPORATE
    }
}
