﻿using iControlDataLayer.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Text;

namespace iControlDataLayer
{
    public class IControlHelper
    {
        private string DatabaseType { get; set; }
        public string MainDynamicConnectionString { get; set; }

        public string SQLConnectionString { get; set; }
        private static ResourceManager _oracleResource = new ResourceManager("iControlDAL.Resources.OracleResources", Assembly.GetExecutingAssembly());
        private static ResourceManager _mysqlResource = new ResourceManager("iControlDAL.Resources.MySqlResources", Assembly.GetExecutingAssembly());
        private static ResourceManager _mssqlResource = new ResourceManager("iControlDAL.Resources.MsSqlResources", Assembly.GetExecutingAssembly());
        private static ResourceManager _nzOledbResource = new ResourceManager("iControlDAL.Resources.NzOledbResources", Assembly.GetExecutingAssembly());

        public IControlHelper()
        {

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["PrimaryDataBaseType"]))
            {
                this.DatabaseType = ConfigurationManager.AppSettings["PrimaryDataBaseType"];
                this.MainDynamicConnectionString = ConfigurationManager.ConnectionStrings[this.DatabaseType].ConnectionString;
                this.SQLConnectionString = ConfigurationManager.ConnectionStrings["SQLServerConnection"].ConnectionString;
            }
        }

        #region(Database Types and Resource)
        /// <summary>
        /// Gets the resource string.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>System.String.</returns>
        public string GetResourceString(string key, string OptionalResource = "")
        {
            if (!String.IsNullOrEmpty(OptionalResource))
            {
                switch (OptionalResource)
                {
                    case "OracleConnection":
                        return _oracleResource.GetString(key);

                    case "MySqlConnection":
                        return _mysqlResource.GetString(key);

                    case "NZOLEDBConnection":
                        return _nzOledbResource.GetString(key);
                    case "SQLServerConnection":
                        return _mssqlResource.GetString(key);
                    default:
                        return _mssqlResource.GetString(key); ;
                }

            }
            else {
                switch (this.DatabaseType)
                {
                    case "OracleConnection":
                        return _oracleResource.GetString(key);

                    case "MySqlConnection":
                        return _mysqlResource.GetString(key);

                    case "NZOLEDBConnection":
                        return _nzOledbResource.GetString(key);
                    case "SQLServerConnection":
                        return _mssqlResource.GetString(key);
                    default:
                        return _mssqlResource.GetString(key); ;
                }

            }


           

        }
        /// <summary>
        /// Gets the type of the database.
        /// </summary>
        /// <returns>DatabaseType.</returns>
        public DatabaseType GetDbType()
        {
            DatabaseType dbtype = iControlDataLayer.Helper.DatabaseType.Unknown;
            if (this.DatabaseType == DataSourceConnection.MySqlConnection.ToString())
                dbtype = iControlDataLayer.Helper.DatabaseType.MySql;
            else if (this.DatabaseType == DataSourceConnection.SQLServerConnection.ToString())
                dbtype = iControlDataLayer.Helper.DatabaseType.MSSql;
            else if (this.DatabaseType == DataSourceConnection.OleDbConnection.ToString())
                dbtype = iControlDataLayer.Helper.DatabaseType.Oledb;
            else if (this.DatabaseType == DataSourceConnection.OracleConnection.ToString())
                dbtype = iControlDataLayer.Helper.DatabaseType.Oracle;
            else if (this.DatabaseType == DataSourceConnection.NZOLEDBConnection.ToString())
                dbtype = iControlDataLayer.Helper.DatabaseType.Oledb;
            return dbtype;
        }
        #endregion

        #region(Exception Log)
        public void WriteLog(string message)
        {
            string file = LogFilePathDetail();
            StringBuilder sbLog = new StringBuilder();
            sbLog.AppendLine(message);
            System.IO.File.AppendAllText(file, sbLog.ToString());
        }

        private string LogFilePathDetail()
        {
            var defaultDir = System.Web.HttpContext.Current.Server.MapPath("~/Log/");
            string fileName = string.Format("{0}_TraceLog.txt", DateTime.Now.ToString("yyyyMMdd"));
            if (!Directory.Exists(defaultDir))
            {
                Directory.CreateDirectory(defaultDir);
            }
            string newName = defaultDir + fileName;
            return newName;
        }
        #endregion

    }

    public enum DataSourceConnection
    {
        MySqlConnection,
        SQLServerConnection,
        OleDbConnection,
        OracleConnection,
        NZOLEDBConnection
    }

}
