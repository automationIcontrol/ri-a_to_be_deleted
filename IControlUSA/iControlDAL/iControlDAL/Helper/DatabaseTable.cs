﻿using System;

namespace iControlDataLayer.Helper
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class DatabaseTableAttribute : Attribute
    {
        public string TableName { get; set; }
    }
}
