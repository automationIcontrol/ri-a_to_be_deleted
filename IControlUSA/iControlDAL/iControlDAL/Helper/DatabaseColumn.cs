﻿using System;

namespace iControlDataLayer.Helper
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class DatabaseColumnAttribute : Attribute
    {
        public string ColumnName { get; set; }
        public bool IsIdentifier { get; set; }
        public bool IsNullable { get; set; }
        public bool IsAutoValue { get; set; }
    }
}