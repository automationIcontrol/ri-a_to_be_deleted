﻿using System.Data;

namespace iControlDataLayer.Helper
{
    public class ColumnDefinition
    {
        public string ColumnName { get; set; }
        public string AssociatedPropertyName { get; set; }
        public DbType DbType { get; set; }
        public bool IsAutoValue { get; set; }
        public bool IsIdentifier { get; set; }
        public bool IsNullable { get; set; }

        public ColumnDefinition() 
        {
            IsAutoValue = false;
            IsIdentifier = false;
            IsNullable = true;
        }

        public ColumnDefinition(string columnName, string associatedPropertyName, DbType dbType, bool isAutoValue, bool isIdentifier, bool isNullable)
        {
            ColumnName = columnName;
            AssociatedPropertyName = associatedPropertyName;
            DbType = dbType;
            IsAutoValue = isAutoValue;
            IsIdentifier = isIdentifier;
            IsNullable = isNullable;
        }
    }
}
