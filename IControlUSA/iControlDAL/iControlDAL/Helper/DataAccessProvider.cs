﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Data.OracleClient;

namespace iControlDataLayer.Helper
{
    public enum DatabaseType
    {
        MSSql,
        MySql,
        Oracle,
        SqlCompact,
        ODBC,
        Oledb,
        NZOLEDB,
        Unknown
    }

    public class DataAccessProvider : IDisposable
    {
        #region(Public Readonly Properties)
        public DatabaseType Database { get; private set; }
        public string ConnectionString { get; private set; }
        IDbConnection connection { get; set; }
        #endregion

        #region(Database Connection Establishment)
        public DataAccessProvider(string connectionString, DatabaseType databaseType)
        {
            Database = databaseType;
            ConnectionString = connectionString;

            using (var conn = CreateConnection())
            {
                conn.Open();
            }
        }
        #endregion

        #region(Public Methods)
        public IDbConnection CreateConnection()
        {
            return CreateConnection(ConnectionString, Database);
        }

        public IDbConnection CreateConnection(string connectionString)
        {
            return CreateConnection(connectionString, Database);
        }

        public IDbConnection CreateConnection(string connectionString, DatabaseType database)
        {
            //IDbConnection connection;
            switch (database)
            {
                case DatabaseType.MSSql:
                    connection = new SqlConnection();
                    break;
                case DatabaseType.MySql:
                    connection = new MySqlConnection();
                    break;
                case DatabaseType.Oracle:
                    connection = new OracleConnection();
                    break;
                case DatabaseType.SqlCompact:
                    connection = new SqlCeConnection();
                    break;
                case DatabaseType.Oledb:
                    connection = new System.Data.OleDb.OleDbConnection();
                    break;
                case DatabaseType.ODBC:
                    connection = new OdbcConnection();
                    break;
                default:
                    connection = new SqlConnection();
                    break;
            }
            connection.ConnectionString = connectionString;
            return connection;
        }

        public void CloseConnection()
        {
            if (this.connection.State == ConnectionState.Open)
                this.connection.Close();
        }

        public IDbCommand CreateCommand()
        {
            return CreateCommand("", null);
        }

        public IDbCommand CreateCommand(IDbConnection connection)
        {
            return CreateCommand("", connection);
        }

        public IDbCommand CreateCommand(string commandText, IDbConnection connection)
        {
            IDbCommand command;
            DatabaseType database = connection == null ? Database : GetDatabaseType(connection);
            switch (database)
            {
                case DatabaseType.MSSql:
                    command = new SqlCommand(commandText, (SqlConnection)connection);
                    break;
                case DatabaseType.MySql:
                    command = new MySqlCommand(commandText, (MySqlConnection)connection);
                    break;
                case DatabaseType.Oracle:
                    command = new OracleCommand(commandText, (OracleConnection)connection);
                    break;
                case DatabaseType.SqlCompact:
                    command = new SqlCeCommand(commandText, (SqlCeConnection)connection);
                    break;
                case DatabaseType.ODBC:
                    command = new OdbcCommand(commandText, (OdbcConnection)connection);
                    break;
                case DatabaseType.NZOLEDB:
                    command = new OleDbCommand(commandText, (OleDbConnection)connection);
                    break;
                case DatabaseType.Oledb:
                    command = new OleDbCommand(commandText, (OleDbConnection)connection);
                    break;
                default:
                    command = new SqlCommand(commandText, (SqlConnection)connection);
                    break;
            }
            return command;
        }

        public IDataParameter CreateParameter()
        {
            return CreateParameter("", DbType.String, null, Database);
        }

        public IDataParameter CreateParameter(string parameterName, DbType dbType, object value)
        {
            return CreateParameter(parameterName, dbType, value, Database);
        }

        public IDataParameter CreateParameter(string parameterName, DbType dbType, object value, DatabaseType database)
        {
            IDataParameter parameter = null;
            switch (database)
            {
                case DatabaseType.MSSql:
                    parameter = new SqlParameter() { ParameterName = parameterName, DbType = dbType, Value = value };
                    break;
                case DatabaseType.MySql:
                    parameter = new MySqlParameter() { ParameterName = parameterName, DbType = dbType, Value = value };
                    break;
                case DatabaseType.Oracle:
                    parameter = new OracleParameter() { ParameterName = parameterName, DbType = dbType, Value = value };
                    break;
                case DatabaseType.SqlCompact:
                    parameter = new SqlCeParameter() { ParameterName = parameterName, DbType = dbType, Value = value };
                    break;
                case DatabaseType.Unknown:
                    break;
                default:
                    parameter = new SqlParameter() { ParameterName = parameterName, DbType = dbType, Value = value };
                    break;
            }
            return parameter;
        }

        public IDataReader ExecuteReader(string query)
        {
            return ExecuteReader(query, new List<IDataParameter>());
        }

        public IDataReader ExecuteReader(string query, List<IDataParameter> parameters)
        {
            IDbConnection connection = CreateConnection();
            connection.Open();
            IDbCommand command = CreateCommand(query, connection);
            foreach (var param in parameters)
            {
                command.Parameters.Add(param);
            }
            return command.ExecuteReader(CommandBehavior.CloseConnection);

        }

        public object ExecuteScaler(string query, bool isProc = false)
        {
            return ExecuteScaler(query, new List<IDataParameter>(), isProc);
        }

        public object ExecuteScaler(string query, List<IDataParameter> parameters, bool isProc = false)
        {
            IDbConnection connection = CreateConnection();

            connection.Open();
            IDbCommand command = CreateCommand(query, connection);
            command.CommandTimeout = 1800;
            if (isProc)
            {
                command.CommandType = CommandType.StoredProcedure;
            }

            foreach (var param in parameters)
            {
                command.Parameters.Add(param);
            }
            return command.ExecuteScalar();

        }

        public DataTable ExecuteProcedure(string strProcName, bool isProc = true)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dt = new DataTable();
            try
            {

                using (IDbConnection connection = CreateConnection())
                {
                    connection.Open();
                    IDbCommand command = CreateCommand(strProcName, connection);
                    if (isProc)
                    {
                        command.CommandType = CommandType.StoredProcedure;
                    }
                    IDataReader result = command.ExecuteReader();
                    dt.Load(result);
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataAccessProvider.cs=>ExecuteProcedure: "));
            }
            return dt;
        }
        public DataTable ExecuteProcedure(string strProcName, List<IDataParameter> parameters, bool isProc = true)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dt = new DataTable();
            try
            {
               
                using (IDbConnection connection = CreateConnection())
                {
                    connection.Open();
                    IDbCommand command = CreateCommand(strProcName, connection);
                    if (isProc)
                    {
                        command.CommandType = CommandType.StoredProcedure;
                    }
                    foreach (var param in parameters)
                    {
                        command.Parameters.Add(param);
                    }
                    IDataReader result = command.ExecuteReader();
                    dt.Load(result);
                }
                return dt;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataAccessProvider.cs=>ExecuteProcedure: "));
            }
            return dt;
        }

        public int ExecuteNonQuery(string query)
        {
            return ExecuteNonQuery(query, new List<IDataParameter>());
        }

        public int ExecuteNonQuery(string query, List<IDataParameter> parameters, bool isProc = false)
        {
            using (IDbConnection connection = CreateConnection())
            {
                connection.Open();
                IDbCommand command = CreateCommand(query, connection);
                if (isProc)
                {
                    command.CommandType = CommandType.StoredProcedure;
                }
                foreach (var param in parameters)
                {
                    command.Parameters.Add(param);
                }
                return command.ExecuteNonQuery();
            }
        }

        public List<dynamic> SelectAll(string tableName)
        {
            List<dynamic> result = new List<dynamic>();
            using (IDataReader reader = ExecuteReader(tableName))
            {
                while (reader.Read())
                {
                    dynamic expando = new ExpandoObject();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        string columnName = reader.GetName(i);
                        ((IDictionary<String, Object>)expando).Add(columnName, reader[columnName]);
                    }
                    result.Add(expando);
                }
            }

            return result;
        }

        public List<T> SelectAll<T>() where T : new()
        {
            return SelectAll<T>("");
        }

        public List<T> SelectAll<T>(string whereClause) where T : new()
        {
            List<T> result = new List<T>();
            Type type = typeof(T);
            TableScheme dbTable = GetTableSchemeFromType(type);
            var query = "Select * From " + dbTable.TableName + " " + whereClause;
            IDataReader reader = ExecuteReader(query);
            result = ParseDataReaderToEntityList<T>(reader, dbTable);
            return result;
        }

        public List<T> SelectAll<T>(string selectQuery, List<IDataParameter> parameters) where T : new()
        {
            List<T> result = new List<T>();
            TableScheme dbTable = GetTableSchemeFromType(typeof(T));
            IDataReader reader = ExecuteReader(selectQuery, parameters);
            result = ParseDataReaderToEntityList<T>(reader, dbTable);
            reader.Close();
            return result;
        }

        public List<dynamic> Select(string tableName, Func<dynamic, bool> predicate)
        {
            return SelectAll(tableName).Where(predicate).ToList();
        }

        public List<dynamic> Select(string tableName, Func<dynamic, bool> predicate, Func<dynamic, dynamic> orderBy)
        {
            return SelectAll(tableName).Where(predicate).OrderBy(orderBy).ToList();
        }

        public List<dynamic> Select(string tableName, Func<dynamic, bool> predicate, Func<dynamic, dynamic> orderBy, SortOrder orderType)
        {
            switch (orderType)
            {
                default:
                case SortOrder.Unspecified:
                case SortOrder.Ascending:
                    return SelectAll(tableName).Where(predicate).OrderBy(orderBy).ToList();
                case SortOrder.Descending:
                    return SelectAll(tableName).Where(predicate).OrderByDescending(orderBy).ToList();
            }
        }

        public List<T> Select<T>(Func<T, bool> predicate) where T : new()
        {
            return SelectAll<T>().Where(predicate).ToList<T>();
        }

        public List<T> Select<T>(Func<T, bool> predicate, Func<T, object> orderBy) where T : new()
        {
            return SelectAll<T>().Where(predicate).OrderBy(orderBy).ToList<T>();
        }

        public List<T> Select<T>(Func<T, bool> predicate, Func<T, object> orderBy, SortOrder orderType) where T : new()
        {
            switch (orderType)
            {
                default:
                case SortOrder.Unspecified:
                case SortOrder.Ascending:
                    return SelectAll<T>().Where(predicate).OrderBy(orderBy).ToList<T>();
                case SortOrder.Descending:
                    return SelectAll<T>().Where(predicate).OrderByDescending(orderBy).ToList<T>();
            }
        }

        public List<T> Select<T>(string queryBuilder) where T : new()
        {
            List<T> result = new List<T>();
            TableScheme dbTable = GetTableSchemeFromType(typeof(T));
            IDataReader reader = ExecuteReader(queryBuilder);
            result = ParseDataReaderToEntityList<T>(reader, dbTable);
            reader.Close();
            return result;
        }

        public T SelectSingleTuple<T>(string queryBuilder) where T : new()
        {
            T result = new T();
            TableScheme dbTable = GetTableSchemeFromType(typeof(T));
            IDataReader reader = ExecuteReader(queryBuilder);
            result = ParseDataReaderToEntity<T>(reader, dbTable);
            reader.Close();
            return result;
        }

        public object SelectSingleItem(string queryBuilder)
        {
            object reader = ExecuteScaler(queryBuilder);
            return reader;
        }

        public DataTable Select(string queryBuilder)
        {
            IDataReader reader = ExecuteReader(queryBuilder);
            using (DataTable dt = new DataTable())
            {
                dt.Load(reader);
                reader.Close();
                return dt;
            }
        }
        public int Insert<T>(T databaseObject) where T : new()
        {
            int result = new int();
            Type type = typeof(T);
            List<IDataParameter> parameters = new List<IDataParameter>();
            TableScheme dbTable = GetTableSchemeFromType(type);

            StringBuilder queryBuilder = new StringBuilder("Insert Into " + dbTable.TableName);
            StringBuilder valuesBuilder = new StringBuilder();
            queryBuilder.Append(" (");
            valuesBuilder.Append("values (");

            int i = 0;
            foreach (var column in dbTable.Columns)
            {
                i++;
                if (!column.IsAutoValue)
                {
                    valuesBuilder.Append("@" + column.ColumnName);
                    valuesBuilder.Append(i == dbTable.Columns.Count ? "" : ",");
                    queryBuilder.Append("" + column.ColumnName);
                    queryBuilder.Append(i == dbTable.Columns.Count ? "" : ",");

                    parameters.Add(CreateParameter("@" + column.ColumnName,
                        column.DbType,
                        type.GetProperty(column.AssociatedPropertyName).GetValue(databaseObject, null)));
                }
            }
            valuesBuilder.Append(")");
            queryBuilder.Append(") ");
            queryBuilder.Append(valuesBuilder);

            result = ExecuteNonQuery(queryBuilder.ToString(), parameters);

            return result;
        }

        public int Insert(string tableName, object databaseObject)
        {
            int result = new int();

            Type type = databaseObject.GetType();
            List<IDataParameter> parameters = new List<IDataParameter>();
            TableScheme dbTable = GetTableSchemeFromType(databaseObject.GetType());
            dbTable.TableName = tableName;
            StringBuilder queryBuilder = new StringBuilder("Insert Into " + dbTable.TableName);
            StringBuilder valuesBuilder = new StringBuilder();
            queryBuilder.Append(" (");
            valuesBuilder.Append("values (");

            int i = 0;
            foreach (var column in dbTable.Columns)
            {
                i++;
                if (!column.IsAutoValue)
                {
                    valuesBuilder.Append("@" + column.ColumnName);
                    valuesBuilder.Append(i == dbTable.Columns.Count ? "" : ",");
                    queryBuilder.Append("" + column.ColumnName);
                    queryBuilder.Append(i == dbTable.Columns.Count ? "" : ",");

                    parameters.Add(CreateParameter("@" + column.ColumnName,
                        column.DbType,
                        type.GetProperty(column.AssociatedPropertyName).GetValue(databaseObject, null)));
                }
            }
            valuesBuilder.Append(")");
            queryBuilder.Append(") ");
            queryBuilder.Append(valuesBuilder);

            result = ExecuteNonQuery(queryBuilder.ToString(), parameters);

            return result;
        }

        public int Insert(string queryBuilder)
        {
            int result = new int();
            List<IDataParameter> parameters = new List<IDataParameter>();
            result = ExecuteNonQuery(queryBuilder.ToString(), parameters);
            return result;
        }

        public int Update<T>(T databaseObject) where T : new()
        {
            int result = new int();
            Type type = typeof(T);
            TableScheme dbTable = GetTableSchemeFromType(type);
            List<IDataParameter> parameters = new List<IDataParameter>();
            StringBuilder queryBuilder = new StringBuilder("Update " + dbTable.TableName + " Set ");
            StringBuilder whereClauseBuilder = new StringBuilder(" where ");

            bool appendWhereClause = false;

            foreach (var column in dbTable.Columns)
            {
                if (column.IsIdentifier)
                {
                    whereClauseBuilder.Append(column.ColumnName);
                    whereClauseBuilder.Append(" = @");
                    whereClauseBuilder.Append(column.ColumnName);
                    whereClauseBuilder.Append(" and ");

                    parameters.Add(CreateParameter("@" + column.ColumnName,
                        column.DbType,
                        type.GetProperty(column.AssociatedPropertyName).GetValue(databaseObject, null)));

                    appendWhereClause = true;
                }
                else if (!column.IsAutoValue)
                {
                    queryBuilder.Append(column.ColumnName);
                    queryBuilder.Append(" = @");
                    queryBuilder.Append(column.ColumnName);
                    queryBuilder.Append(",");

                    parameters.Add(CreateParameter("@" + column.ColumnName,
                            column.DbType,
                            type.GetProperty(column.AssociatedPropertyName).GetValue(databaseObject, null)));
                }
            }
            queryBuilder.Append("|end");
            queryBuilder.Replace(",|end", "");
            whereClauseBuilder.Append("|end");
            whereClauseBuilder.Replace(" and |end", "");

            queryBuilder.Append(appendWhereClause ? whereClauseBuilder.ToString() : "");
            result = ExecuteNonQuery(queryBuilder.ToString(), parameters);

            return result;
        }

        public int Update<T>(Func<T, bool> selectionPredicate, Action<T> updatePredicate) where T : new()
        {
            int result = new int();

            foreach (var item in Select(selectionPredicate))
            {
                updatePredicate(item);
                result += Update<T>(item);
            }

            return result;
        }

        public int Update(string queryBuilder)
        {
            int result = new int();
            result = ExecuteNonQuery(queryBuilder.ToString());
            return result;
        }

        public int Delete<T>(T databaseObject) where T : new()
        {
            int result = new int();
            Type type = typeof(T);
            TableScheme dbTable = GetTableSchemeFromType(type);
            List<IDataParameter> parameters = new List<IDataParameter>();

            bool canExecute = false;
            bool hasIdentifier = dbTable.Columns.Where((x) => { return x.IsIdentifier; }).Count() > 0;

            StringBuilder queryBuilder = new StringBuilder("Delete From " + dbTable.TableName + " Where ");

            if (hasIdentifier)
            {
                ColumnDefinition column = dbTable.Columns.Where((x) => { return x.IsIdentifier; }).First();
                queryBuilder.Append(column.ColumnName + "= @" + column.ColumnName);
                parameters.Add(CreateParameter("@" + column.ColumnName,
                    column.DbType,
                    type.GetProperty(column.AssociatedPropertyName).GetValue(databaseObject, null)));
                canExecute = true;
            }
            else
            {
                foreach (var column in dbTable.Columns)
                {
                    queryBuilder.Append(column.ColumnName + "= @" + column.ColumnName + " And ");
                    parameters.Add(CreateParameter("@" + column.ColumnName,
                        column.DbType,
                        type.GetProperty(column.AssociatedPropertyName).GetValue(databaseObject, null)));
                    if (!canExecute)
                        canExecute = true;
                }
                queryBuilder.Append("_END_");
                queryBuilder = queryBuilder.Replace(" And _END_", "");
            }

            if (canExecute)
            {
                result = ExecuteNonQuery(queryBuilder.ToString(), parameters);
            }

            return result;
        }

        public int Delete<T>(Func<T, bool> predicate) where T : new()
        {
            int result = new int();

            foreach (var item in Select<T>(predicate))
            {
                result += Delete<T>(item);
            }

            return result;
        }

        public int Delete(string queryBuilder)
        {
            int result = ExecuteNonQuery(queryBuilder.ToString());
            return result;
        }
        #endregion

        #region(Private Methods)
        private DatabaseType GetDatabaseType(IDbConnection connection)
        {
            DatabaseType database = DatabaseType.Unknown;
            if (connection is SqlConnection)
            {
                database = DatabaseType.MSSql;
            }
            else if (connection is MySqlConnection)
            {
                database = DatabaseType.MySql;
            }
            else if (connection is OracleConnection)
            {
                database = DatabaseType.Oracle;
            }
            else if (connection is SqlCeConnection)
            {
                database = DatabaseType.SqlCompact;
            }
            else if (connection is OdbcConnection)
            {
                database = DatabaseType.ODBC;
            }
            else if (connection is OleDbConnection)
            {
                database = DatabaseType.Oledb;
            }
            return database;
        }

        private TableScheme GetTableSchemeFromType(Type type)
        {
            TableScheme dbTable = new TableScheme();

            string tableName;
            List<ColumnDefinition> columns = new List<ColumnDefinition>();

            var attributes = type.GetCustomAttributes(typeof(DatabaseTableAttribute), false);
            //If type uses DatabaseTableAttribute, then map it using specified fields like table name.
            if (attributes.Length > 0)
            {
                var attr = attributes[0] as DatabaseTableAttribute;
                //If TableName isn't specified, then use the name of type as table name.
                tableName = string.IsNullOrEmpty(attr.TableName) ? type.Name : attr.TableName;
                //Loop properties of the type.
                foreach (var prop in type.GetProperties())
                {
                    var propAttributes = prop.GetCustomAttributes(typeof(DatabaseColumnAttribute), false);
                    //If the current property uses DatabaseColumnAttribute, then map it using specified fields like column name, nullable etc.
                    if (propAttributes.Length > 0)
                    {
                        var propAttr = propAttributes[0] as DatabaseColumnAttribute;
                        //If ColumnName isn't specified, then use the name of property as column name.
                        string columnName = string.IsNullOrEmpty(propAttr.ColumnName) ? prop.Name : propAttr.ColumnName;
                        //Get System.Data.DbType of property using the method below.
                        DbType dbType = Helper.DbTypeConverter.ConvertFromSystemType(prop.PropertyType);
                        bool isAutoValue = propAttr.IsAutoValue;
                        bool isIdenifier = propAttr.IsIdentifier;
                        bool isNullable = propAttr.IsNullable;
                        columns.Add(new ColumnDefinition(columnName, prop.Name, dbType, isAutoValue, isIdenifier, isNullable));
                    }
                    //If the curent property doesn't use DatabaseColumnAttribute, then behave as default.
                    else
                    {
                        //If property name is "ID" or "[TABLENAME]ID", then set it as identifier.
                        bool isIdentifier = prop.Name.ToUpper() == "ID" || prop.Name.ToUpper() == tableName.ToUpper() + "ID";
                        columns.Add(new ColumnDefinition(prop.Name, prop.Name, Helper.DbTypeConverter.ConvertFromSystemType(prop.PropertyType),
                            isIdentifier, isIdentifier, true));
                    }
                }
            }
            //If type doesn't use DatabaseTableAttribute, then behave as default.
            else
            {
                tableName = type.Name;
                foreach (var prop in type.GetProperties())
                {
                    bool isIdentifier = prop.Name.ToUpper() == "ID" || prop.Name.ToUpper() == tableName.ToUpper() + "ID";
                    columns.Add(new ColumnDefinition(prop.Name, prop.Name, Helper.DbTypeConverter.ConvertFromSystemType(prop.PropertyType),
                        isIdentifier, isIdentifier, true));
                }
            }

            dbTable.TableName = tableName;
            dbTable.Columns = columns;

            return dbTable;
        }

        private List<T> ParseDataReaderToEntityList<T>(IDataReader reader, TableScheme dbTable) where T : new()
        {
            Type type = typeof(T);
            List<T> result = new List<T>();
            while (reader.Read())
            {
                T t = new T();
                foreach (var column in dbTable.Columns)
                {
                    type.GetProperty(column.AssociatedPropertyName).SetValue(t, reader[column.ColumnName], null);
                }
                result.Add(t);
            }
            return result;
        }

        private T ParseDataReaderToEntity<T>(IDataReader reader, TableScheme dbTable) where T : new()
        {
            Type type = typeof(T);
            T result = new T();
            while (reader.Read())
            {
                T t = new T();
                foreach (var column in dbTable.Columns)
                {
                    type.GetProperty(column.AssociatedPropertyName).SetValue(t, reader[column.ColumnName], null);
                }
                result = t;
            }
            return result;
        }

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            CloseConnection();
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}

