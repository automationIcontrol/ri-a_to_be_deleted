﻿using System.Collections.Generic;

namespace iControlDataLayer.Helper
{
    public class TableScheme
    {
        public string TableName { get; set; }
        public List<ColumnDefinition> Columns { get; set; }
    }
}
