﻿using iControlDataLayer.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace iControlDataLayer
{
    /// <summary>
    /// PARTIAL CLASS USED FOR DATABASE MIGRATION
    /// </summary>
    public partial class DataLayer
    {
        #region(Page Level Properties)

        IControlHelper helper = new IControlHelper();
        DataAccessProvider dataAccessProvider;
        #endregion

        #region(P_MARKET_BASKET_REQUEST_TEXTVALUES)
        /// <summary>
        /// ps the market basket request textvalues.
        /// </summary>
        /// <param name="p_USERNAME">The p username.</param>
        /// <param name="p_BASKET_DESCRIPTION">The p basket description.</param>
        /// <param name="p_ETNICITY">The p etnicity.</param>
        /// <param name="p_INCOME">The p income.</param>
        /// <param name="p_NOOFCHILDREN">The p noofchildren.</param>
        /// <param name="p_NOOFADULTS">The p noofadults.</param>
        /// <param name="p_GENDER">The p gender.</param>
        /// <param name="p_AGEHH">The p agehh.</param>
        /// <param name="p_GENERATIONGROUPING">The p generationgrouping.</param>
        /// <param name="p_GOURMETENTHUSIASTS">The p gourmetenthusiasts.</param>
        /// <param name="p_DOGENTHUSIASTS">The p dogenthusiasts.</param>
        /// <param name="p_CATENTHUSIASTS">The p catenthusiasts.</param>
        /// <param name="p_SIZEOfHH">The p size of hh.</param>
        /// <param name="p_DEPARTMENTS">The p departments.</param>
        /// <param name="p_CATEGORIES">The p categories.</param>
        /// <param name="p_SUBCATEGORIES">The p subcategories.</param>
        /// <param name="p_SEGMENTS">The p segments.</param>
        /// <param name="p_BRANDS">The p brands.</param>
        /// <param name="p_UPCS">The p upcs.</param>
        /// <param name="p_ACTIVEFILTERS">The p activefilters.</param>
        /// <returns>System.Int32.</returns>
        public int P_MARKET_BASKET_REQUEST_TEXTVALUES(string p_USERNAME, string p_BASKET_DESCRIPTION, string p_ETNICITY, string p_INCOME, string p_NOOFCHILDREN, string p_NOOFADULTS, string p_GENDER, string p_AGEHH, string p_GENERATIONGROUPING, string p_GOURMETENTHUSIASTS, string p_DOGENTHUSIASTS, string p_CATENTHUSIASTS, string p_SIZEOfHH, string p_DEPARTMENTS, string p_CATEGORIES, string p_SUBCATEGORIES, string p_SEGMENTS, string p_BRANDS, string p_UPCS, string p_ACTIVEFILTERS)
        {
            int result = 0;
            try
            {
                string EXECIDENTIFIER = P_MARKET_BASKET_REQUEST_TEXTVALUES_SELECT(p_USERNAME, p_BASKET_DESCRIPTION);
                result = P_MARKET_BASKET_REQUEST_TEXTVALUES_INSERT(EXECIDENTIFIER, p_ETNICITY, p_INCOME, p_NOOFCHILDREN, p_NOOFADULTS, p_GENDER, p_AGEHH, p_GENERATIONGROUPING, p_GOURMETENTHUSIASTS, p_DOGENTHUSIASTS, p_CATENTHUSIASTS, p_SIZEOfHH, p_DEPARTMENTS, p_CATEGORIES, p_SUBCATEGORIES, p_SEGMENTS, p_BRANDS, p_UPCS, p_ACTIVEFILTERS);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "P_MARKET_BASKET_REQUEST_TEXTVALUES: "));
            }
            return result;
        }

        /// <summary>
        /// Select record from MARKET_BASKET_REQUESTS  based on filters
        /// </summary>
        /// <param name="p_USERNAME">The p username.</param>
        /// <param name="p_BASKET_DESCRIPTION">The p basket description.</param>
        /// <returns>System.String.</returns>
        private string P_MARKET_BASKET_REQUEST_TEXTVALUES_SELECT(string p_USERNAME, string p_BASKET_DESCRIPTION)
        {
            dynamic EXECIDENTIFIER = "";
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string selectQuery = helper.GetResourceString(AppConstants.P_MARKET_BASKET_REQUEST_TEXTVALUES_SELECT);
                selectQuery = selectQuery.Replace("@p_USERNAME", string.Format("'{0}'", p_USERNAME)).Replace("@p_BASKET_DESCRIPTION", string.Format("'{0}'", p_BASKET_DESCRIPTION));
                EXECIDENTIFIER = dataAccessProvider.SelectSingleItem(selectQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "P_MARKET_BASKET_REQUEST_TEXTVALUES_SELECT: "));
            }
            dataAccessProvider.CloseConnection();
            return EXECIDENTIFIER.ToString();
        }

        /// <summary>
        /// Insert items into MARKET_BASKET_REQUEST_TEXTVALUES 
        /// </summary>
        /// <param name="EXECIDENTIFIER">The execidentifier.</param>
        /// <param name="p_ETNICITY">The p etnicity.</param>
        /// <param name="p_INCOME">The p income.</param>
        /// <param name="p_NOOFCHILDREN">The p noofchildren.</param>
        /// <param name="p_NOOFADULTS">The p noofadults.</param>
        /// <param name="p_GENDER">The p gender.</param>
        /// <param name="p_AGEHH">The p agehh.</param>
        /// <param name="p_GENERATIONGROUPING">The p generationgrouping.</param>
        /// <param name="p_GOURMETENTHUSIASTS">The p gourmetenthusiasts.</param>
        /// <param name="p_DOGENTHUSIASTS">The p dogenthusiasts.</param>
        /// <param name="p_CATENTHUSIASTS">The p catenthusiasts.</param>
        /// <param name="p_SIZEOfHH">The p size of hh.</param>
        /// <param name="p_DEPARTMENTS">The p departments.</param>
        /// <param name="p_CATEGORIES">The p categories.</param>
        /// <param name="p_SUBCATEGORIES">The p subcategories.</param>
        /// <param name="p_SEGMENTS">The p segments.</param>
        /// <param name="p_BRANDS">The p brands.</param>
        /// <param name="p_UPCS">The p upcs.</param>
        /// <param name="p_ACTIVEFILTERS">The p activefilters.</param>
        /// <returns>System.Int32.</returns>
        private int P_MARKET_BASKET_REQUEST_TEXTVALUES_INSERT(string EXECIDENTIFIER, string p_ETNICITY, string p_INCOME, string p_NOOFCHILDREN, string p_NOOFADULTS, string p_GENDER, string p_AGEHH, string p_GENERATIONGROUPING, string p_GOURMETENTHUSIASTS, string p_DOGENTHUSIASTS, string p_CATENTHUSIASTS, string p_SIZEOfHH, string p_DEPARTMENTS, string p_CATEGORIES, string p_SUBCATEGORIES, string p_SEGMENTS, string p_BRANDS, string p_UPCS, string p_ACTIVEFILTERS)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.P_MARKET_BASKET_REQUEST_TEXTVALUES_INSERT);
                insertQuery = insertQuery.Replace("@EXECIDENTIFIER", string.Format("'{0}'", EXECIDENTIFIER)).Replace("@p_ETNICITY", string.Format("'{0}'", p_ETNICITY)).Replace("@p_INCOME", string.Format("'{0}'", p_INCOME)).Replace("@p_NOOFCHILDREN", string.Format("'{0}'", p_NOOFCHILDREN)).Replace("@p_NOOFADULTS", string.Format("'{0}'", p_NOOFADULTS)).Replace("@p_GENDER", string.Format("'{0}'", p_GENDER)).Replace("@p_AGEHH", string.Format("'{0}'", p_AGEHH)).Replace("@p_GENERATIONGROUPING", string.Format("'{0}'", p_GENERATIONGROUPING)).Replace("@p_GOURMETENTHUSIASTS", string.Format("'{0}'", p_GOURMETENTHUSIASTS)).Replace("@p_DOGENTHUSIASTS", string.Format("'{0}'", p_DOGENTHUSIASTS)).Replace("@p_CATENTHUSIASTS", string.Format("'{0}'", p_CATENTHUSIASTS)).Replace("@p_SIZEOfHH", string.Format("'{0}'", p_SIZEOfHH)).Replace("@p_DEPARTMENTS", string.Format("'{0}'", p_DEPARTMENTS)).Replace("@p_CATEGORIES", string.Format("'{0}'", p_CATEGORIES)).Replace("@p_SUBCATEGORIES", string.Format("'{0}'", p_SUBCATEGORIES)).Replace("@p_SEGMENTS", string.Format("'{0}'", p_SEGMENTS)).Replace("@p_BRANDS", string.Format("'{0}'", p_BRANDS)).Replace("@p_UPCS", string.Format("'{0}'", p_UPCS)).Replace("@p_ACTIVEFILTERS", string.Format("'{0}'", p_ACTIVEFILTERS));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "P_MARKET_BASKET_REQUEST_TEXTVALUES_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_CUSTOM_HIERARCHY_REPLACE_BLACKLIST_CHARACTERS)
        /// <summary>
        /// Replacing blacklist characters.
        /// </summary>
        public void SP_CUSTOM_HIERARCHY_REPLACE_BLACKLIST_CHARACTERS()
        {
            try
            {
                REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_HIERARCHY_MASTER", "HIERARCHY_NAME");
                REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_HIERARCHY_MASTER", "HIERARCHY_DESCRIPTION");
                REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_HIERARCHY_PRODUCTS", "CUSTOM_LEVEL_1");
                REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_HIERARCHY_PRODUCTS", "CUSTOM_LEVEL_2");
                REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_HIERARCHY_PRODUCTS", "CUSTOM_LEVEL_3");
                REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_HIERARCHY_EXTENDED", "HIERARCHY_NAME");
                REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_HIERARCHY_EXTENDED", "HIERARCHY_DESCRIPTION");
                REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_HIERARCHY_EXTENDED", "CUSTOM_LEVEL_1");
                REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_HIERARCHY_EXTENDED", "CUSTOM_LEVEL_2");
                REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_HIERARCHY_EXTENDED", "CUSTOM_LEVEL_3");
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_CUSTOM_HIERARCHY_REPLACE_BLACKLIST_CHARACTERS: "));
            }
        }

        /// <summary>
        /// Updates given column in table by replacing blacklist characters.
        /// </summary>
        /// <param name="p_TABLENAME">The p tablename.</param>
        /// <param name="p_COLUMN">The p column.</param>
        /// <returns>System.Int32.</returns>
        private int REPLACE_BLACKLIST_CHARACTERS(string p_TABLENAME, string p_COLUMN)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string updateQuery = helper.GetResourceString(AppConstants.REPLACE_BLACKLIST_CHARACTERS_UPDATE);
                updateQuery = updateQuery.Replace("@p_TABLENAME", string.Format("{0}", p_TABLENAME)).Replace("@p_COLUMN", string.Format("{0}", p_COLUMN));
                result = dataAccessProvider.Update(updateQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "REPLACE_BLACKLIST_CHARACTERS: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_CUSTOM_STORE_REPLACE_BLACKLIST_CHARACTERS)
        /// <summary>
        /// Replacing blacklist characters.
        /// </summary>
        public void SP_CUSTOM_STORE_REPLACE_BLACKLIST_CHARACTERS()
        {
            REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_STORES_MASTER", "CUSTOM_STORE_NAME");
            REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_STORES_MASTER", "CUSTOM_STORE_DESCRIPTION");
            REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_STORES_DETAILS", "CUSTOM_LEVEL_1");
            REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_STORES_DETAILS", "CUSTOM_LEVEL_2");
            REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_STORES_DETAILS", "CUSTOM_LEVEL_3");
            REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_STORES_EXTENDED", "CUSTOM_STORE_NAME");
            REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_STORES_EXTENDED", "CUSTOM_STORE_DESCRIPTION");
            REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_STORES_EXTENDED", "CUSTOM_LEVEL_1");
            REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_STORES_EXTENDED", "CUSTOM_LEVEL_2");
            REPLACE_BLACKLIST_CHARACTERS("DIM_CUSTOM_STORES_EXTENDED", "CUSTOM_LEVEL_3");
        }
        #endregion

        #region(SP_DELETECUSTOMHIERARCHY_NEW)
        /// <summary>
        /// Sps the deletecustomhierarchy new.
        /// </summary>
        /// <param name="p_CustomHierarchyId">The p custom hierarchy identifier.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool SP_DELETECUSTOMHIERARCHY_NEW(long p_CustomHierarchyId)
        {
            int result1 = 0, result2 = 0, result3 = 0;
            bool result = false;
            try
            {
                result1 = SP_DELETECUSTOMHIERARCHY_NEW_DELETE1(p_CustomHierarchyId);
                if (result1 > 0)
                {
                    result2 = SP_DELETECUSTOMHIERARCHY_NEW_DELETE2(p_CustomHierarchyId);
                    if (result2 > 0)
                    {
                        result3 = SP_DELETECUSTOMHIERARCHY_NEW_DELETE3(p_CustomHierarchyId);
                        if (result3 > 0)
                            result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETECUSTOMHIERARCHY_NEW: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }

        /// <summary>
        /// Delete record from DIM_CUSTOM_HIERARCHY_MASTER 
        /// </summary>
        /// <param name="p_CustomHierarchyId">The p custom hierarchy identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_DELETECUSTOMHIERARCHY_NEW_DELETE1(long p_CustomHierarchyId)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETECUSTOMHIERARCHY_NEW_DELETE1);
                deleteQuery = deleteQuery.Replace("@p_CustomHierarchyId", string.Format("'{0}'", p_CustomHierarchyId));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETECUSTOMHIERARCHY_NEW_DELETE1: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }

        /// <summary>
        /// Delete record from DIM_CUSTOM_HIERARCHY_PRODUCTS
        /// </summary>
        /// <param name="p_CustomHierarchyId">The p custom hierarchy identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_DELETECUSTOMHIERARCHY_NEW_DELETE2(long p_CustomHierarchyId)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETECUSTOMHIERARCHY_NEW_DELETE2);
                deleteQuery = deleteQuery.Replace("@p_CustomHierarchyId", string.Format("'{0}'", p_CustomHierarchyId));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETECUSTOMHIERARCHY_NEW_DELETE2: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }

        /// <summary>
        /// Delete record from DIM_CUSTOM_HIERARCHIES_SHARED.
        /// </summary>
        /// <param name="p_CustomHierarchyId">The p custom hierarchy identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_DELETECUSTOMHIERARCHY_NEW_DELETE3(long p_CustomHierarchyId)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETECUSTOMHIERARCHY_NEW_DELETE3);
                deleteQuery = deleteQuery.Replace("@p_CustomHierarchyId", string.Format("'{0}'", p_CustomHierarchyId));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETECUSTOMHIERARCHY_NEW_DELETE3: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_DELETECUSTOMHIERARCHY_PRODUCT_NEW)

        /// <summary>
        /// Deleting row from DIM_CUSTOM_HIERARCHY_PRODUCTS based on criteria
        /// </summary>
        /// <param name="p_CustomHierarchyId">The p custom hierarchy identifier.</param>
        /// <param name="p_ProductId">The p product identifier.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool SP_DELETECUSTOMHIERARCHY_PRODUCT_NEW(long p_CustomHierarchyId, long p_ProductId)
        {
            bool result = false;
            try
            {
                result = SP_DELETECUSTOMHIERARCHY_PRODUCT_NEW_DELETE(p_CustomHierarchyId, p_ProductId);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETECUSTOMHIERARCHY_PRODUCT_NEW: "));
            }
            return result;
        }

        /// <summary>
        /// Delete records from DIM_CUSTOM_HIERARCHY_PRODUCTS.
        /// </summary>
        /// <param name="p_CustomHierarchyId">The p custom hierarchy identifier.</param>
        /// <param name="p_ProductId">The p product identifier.</param>
        /// <returns>System.Int32.</returns>
        private bool SP_DELETECUSTOMHIERARCHY_PRODUCT_NEW_DELETE(long p_CustomHierarchyId, long p_ProductId)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETECUSTOMHIERARCHY_PRODUCT_NEW_DELETE);
                deleteQuery = deleteQuery.Replace("@p_CustomHierarchyId", string.Format("'{0}'", p_CustomHierarchyId)).Replace("@p_ProductId", string.Format("'{0}'", p_ProductId));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETECUSTOMHIERARCHY_PRODUCT_NEW_DELETE: "));
            }
            dataAccessProvider.CloseConnection();
            if (result >= 1)
                return true;
            else return false;
        }

        #endregion

        #region(SP_DELETECUSTOMSTOREGROUP_NEW)
        /// <summary>
        /// Deleting from DIM_CUSTOM_STORES_MASTER.
        /// </summary>
        /// <param name="p_StoreGroupId">The p store group identifier.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool SP_DELETECUSTOMSTOREGROUP_NEW(long p_StoreGroupId)
        {
            int result1 = 0, result2 = 0, result3 = 0;
            bool result = false;
            try
            {
                result1 = SP_DELETECUSTOMSTOREGROUP_NEW_DELETE1(p_StoreGroupId);
                if (result1 > 0)
                {
                    result2 = SP_DELETECUSTOMSTOREGROUP_NEW_DELETE2(p_StoreGroupId);
                    if (result2 > 0)
                    {
                        result3 = SP_DELETECUSTOMSTOREGROUP_NEW_DELETE3(p_StoreGroupId);
                        if (result3 > 0)
                            result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > SP_DELETECUSTOMSTOREGROUP_NEW"));
            }
            return result;
        }

        /// <summary>
        /// Delete records from DIM_CUSTOM_STORES_MASTER .
        /// </summary>
        /// <param name="p_StoreGroupId">The p store group identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_DELETECUSTOMSTOREGROUP_NEW_DELETE1(long p_StoreGroupId)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETECUSTOMSTOREGROUP_NEW_DELETE1);
                deleteQuery = deleteQuery.Replace("@p_StoreGroupId", string.Format("'{0}'", p_StoreGroupId));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETECUSTOMSTOREGROUP_NEW_DELETE1: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }

        /// <summary>
        ///Delete records from DIM_CUSTOM_STORES_DETAILS 
        /// </summary>
        /// <param name="p_StoreGroupId">The p store group identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_DELETECUSTOMSTOREGROUP_NEW_DELETE2(long p_StoreGroupId)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETECUSTOMSTOREGROUP_NEW_DELETE2);
                deleteQuery = deleteQuery.Replace("@p_StoreGroupId", string.Format("'{0}'", p_StoreGroupId));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETECUSTOMSTOREGROUP_NEW_DELETE2: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }

        /// <summary>
        /// Delete records from DIM_CUSTOM_STORES_SHARED 
        /// </summary>
        /// <param name="p_StoreGroupId">The p store group identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_DELETECUSTOMSTOREGROUP_NEW_DELETE3(long p_StoreGroupId)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETECUSTOMSTOREGROUP_NEW_DELETE3);
                deleteQuery = deleteQuery.Replace("@p_StoreGroupId", string.Format("'{0}'", p_StoreGroupId));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETECUSTOMSTOREGROUP_NEW_DELETE3: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_DELETECUSTOMSTORE_NEW)
        /// <summary>
        /// Delete items from DIM_CUSTOM_STORES_DETAILS based on condition
        /// </summary>
        /// <param name="StoreId">The store identifier.</param>
        /// <param name="StoreGroupId">The store group identifier.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool SP_DELETECUSTOMSTORE_NEW(long StoreId, long StoreGroupId)
        {
            int result1 = 0;
            bool result = false;
            try
            {
                result1 = SP_DELETECUSTOMSTORE_NEW_DELETE(StoreId, StoreGroupId);
                if (result1 > 0)
                    result = true;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETECUSTOMSTORE_NEW: "));
            }
            return result;
        }

        /// <summary>
        /// Delete from DIM_CUSTOM_STORES_DETAILS 
        /// </summary>
        /// <param name="StoreId">The store identifier.</param>
        /// <param name="StoreGroupId">The store group identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_DELETECUSTOMSTORE_NEW_DELETE(long StoreId, long StoreGroupId)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETECUSTOMSTORE_NEW_DELETE);
                deleteQuery = deleteQuery.Replace("@storeId", string.Format("'{0}'", StoreId)).Replace("@storeGroupId", string.Format("'{0}'", StoreGroupId));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETECUSTOMSTORE_NEW_DELETE: "));
            }
            return result;
        }
        #endregion

        #region(SP_DELETECUSTOMSTORE)
        private int SP_DELETECUSTOMSTORE(long StoreId, long StoreGroupId)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETE_CUSTOM_STORES);
                deleteQuery = deleteQuery.Replace("@StoreId", string.Format("'{0}'", StoreId)).Replace("@StoreGroupId", string.Format("'{0}'", StoreGroupId));
                result = dataAccessProvider.Delete(deleteQuery);

                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETECUSTOMSTORE: "));
            }
            return result;
        }

        public bool SP_DELETECUSTOMSTORES(long StoreId, long StoreGroupId)
        {
            int result1 = 0;
            bool result = false;
            try
            {
                result1 = SP_DELETECUSTOMSTORE(StoreId, StoreGroupId);
                if (result1 > 0)
                    result = true;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETECUSTOMSTORE_NEW: "));
            }
            return result;
        }
        #endregion
        #region(SP_DELETE_MARKET_BASKET)
        /// <summary>
        /// Delete from various tables.
        /// </summary>
        /// <param name="p_EXEC_IDENTIFIER">The p execute identifier.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool SP_DELETE_MARKET_BASKET(string p_EXEC_IDENTIFIER)
        {
            bool result = false;
            int result1 = 0, result2 = 0, result3 = 0, result4 = 0;
            try
            {
                result1 = SP_DELETE_MARKET_BASKET_DELETE1(p_EXEC_IDENTIFIER);
                if (result1 > 0)
                {
                    result2 = SP_DELETE_MARKET_BASKET_DELETE2(p_EXEC_IDENTIFIER);
                    if (result2 > 0)
                    {
                        result3 = SP_DELETE_MARKET_BASKET_DELETE3(p_EXEC_IDENTIFIER);
                        if (result3 > 0)
                        {
                            result4 = SP_DELETE_MARKET_BASKET_DELETE4(p_EXEC_IDENTIFIER);
                            if (result4 > 0)
                                result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETE_MARKET_BASKET: "));
            }

            return result;
        }

        /// <summary>
        /// Delete records from MARKET_BASKET_REQUESTS 
        /// </summary>
        /// <param name="p_EXEC_IDENTIFIER">The p execute identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_DELETE_MARKET_BASKET_DELETE1(string p_EXEC_IDENTIFIER)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETE_MARKET_BASKET_DELETE1);
                deleteQuery = deleteQuery.Replace("@p_EXEC_IDENTIFIER", string.Format("'{0}'", p_EXEC_IDENTIFIER));
                result = dataAccessProvider.Delete(deleteQuery);


                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETE_MARKET_BASKET_DELETE1: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }

        /// <summary>
        /// Delete records from MARKET_BASKET_REQUEST_TEXTVALUES 
        /// </summary>
        /// <param name="p_EXEC_IDENTIFIER">The p execute identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_DELETE_MARKET_BASKET_DELETE2(string p_EXEC_IDENTIFIER)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETE_MARKET_BASKET_DELETE2);
                deleteQuery = deleteQuery.Replace("@p_EXEC_IDENTIFIER", string.Format("'{0}'", p_EXEC_IDENTIFIER));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETE_MARKET_BASKET_DELETE2: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }

        /// <summary>
        /// Delete records from BASKET_AFFINITY_SETUP 
        /// </summary>
        /// <param name="p_EXEC_IDENTIFIER">The p execute identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_DELETE_MARKET_BASKET_DELETE3(string p_EXEC_IDENTIFIER)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETE_MARKET_BASKET_DELETE3);
                deleteQuery = deleteQuery.Replace("@p_EXEC_IDENTIFIER", string.Format("'{0}'", p_EXEC_IDENTIFIER));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETE_MARKET_BASKET_DELETE3: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }

        /// <summary>
        /// Delete records from BASKET_AFFINITY 
        /// </summary>
        /// <param name="p_EXEC_IDENTIFIER">The p execute identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_DELETE_MARKET_BASKET_DELETE4(string p_EXEC_IDENTIFIER)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETE_MARKET_BASKET_DELETE4);
                deleteQuery = deleteQuery.Replace("@p_EXEC_IDENTIFIER", string.Format("'{0}'", p_EXEC_IDENTIFIER));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETE_MARKET_BASKET_DELETE4: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_DELETE_PROMOTIONS)
        /// <summary>
        /// SP to delete from DIM_CUSTOM_PROMOTIONS .
        /// </summary>
        /// <param name="promoid">The promoid.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool SP_DELETE_PROMOTIONS(long promoid)
        {
            bool success = false;
            int result = 0;
            try
            {
                result = SP_DELETE_PROMOTIONS_DELETE(promoid);
                if (result > 0)
                    success = true;
            }
            catch (Exception ex)
            { helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETE_PROMOTIONS: ")); }
            return success;
        }

        /// <summary>
        /// Delete records from DIM_CUSTOM_PROMOTIONS 
        /// </summary>
        /// <param name="promoid">The promoid.</param>
        /// <returns>System.Int32.</returns>
        private int SP_DELETE_PROMOTIONS_DELETE(long promoid)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_DELETE_PROMOTIONS_DELETE);
                deleteQuery = deleteQuery.Replace("@promoid", string.Format("'{0}'", promoid));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_DELETE_PROMOTIONS_DELETE: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERTREPORT_PARAMETERS)
        /// <summary>
        /// Insert to REPORT_DATA_DEFINITION table.
        /// </summary>
        /// <param name="p_ExecId">The p execute identifier.</param>
        /// <param name="p_Parameter_id">The p parameter identifier.</param>
        /// <param name="p_Parameter_Value">The p parameter value.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERTREPORT_PARAMETERS(string p_ExecId, int p_Parameter_id, string p_Parameter_Value)
        {
            int result = 0;
            try
            {
                result = SP_INSERTREPORT_PARAMETERS_INSERT(p_ExecId, p_Parameter_id, p_Parameter_Value);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERTREPORT_PARAMETERS: "));
            }
            return result;
        }

        /// <summary>
        /// Insert records into REPORT_DATA_DEFINITION 
        /// </summary>
        /// <param name="p_ExecId">The p execute identifier.</param>
        /// <param name="p_Parameter_id">The p parameter identifier.</param>
        /// <param name="p_Parameter_Value">The p parameter value.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERTREPORT_PARAMETERS_INSERT(string p_ExecId, int p_Parameter_id, string p_Parameter_Value)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERTREPORT_PARAMETERS_INSERT);
                insertQuery = insertQuery.Replace("@p_ExecId", string.Format("'{0}'", p_ExecId)).Replace("@p_Parameter_id", string.Format("'{0}'", p_Parameter_id)).Replace("@p_Parameter_Value", string.Format("'{0}'", p_Parameter_Value));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERTREPORT_PARAMETERS_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_CREATEUSERPROFILE)
        /// <summary>
        /// Sps the insert createuserprofile.
        /// </summary>
        /// <param name="p_ProfileName">Name of the p profile.</param>
        /// <param name="p_ProfileDesc">The p profile desc.</param>
        /// <param name="p_CreatedDate">The p created date.</param>
        /// <param name="p_CreatedBy">The p created by.</param>
        /// <param name="p_SuperUser">The p super user.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_CREATEUSERPROFILE(string p_ProfileName, string p_ProfileDesc, DateTime p_CreatedDate, string p_CreatedBy, bool p_SuperUser)
        {
            int p_ProfileId = 0;
            try
            {
                p_ProfileId = SP_INSERT_CREATEUSERPROFILE_SELECT();
                if (p_ProfileId != 0)
                    p_ProfileId = p_ProfileId + 1;
                else
                    p_ProfileId = 1;
                SP_INSERT_CREATEUSERPROFILE_INSERT(p_ProfileId, p_ProfileName, p_ProfileDesc, p_CreatedDate, p_CreatedBy, p_SuperUser);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CREATEUSERPROFILE: "));
            }
            return p_ProfileId;
        }

        /// <summary>
        /// Select records from CreateUserProfile .
        /// </summary>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_CREATEUSERPROFILE_SELECT()
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string selectQuery = helper.GetResourceString(AppConstants.SP_INSERT_CREATEUSERPROFILE_SELECT);
                result = dataAccessProvider.SelectSingleItem(selectQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CREATEUSERPROFILE_SELECT: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }

        /// <summary>
        /// Insert records into CreateUserProfile .
        /// </summary>
        /// <param name="p_ProfileId">The p profile identifier.</param>
        /// <param name="p_ProfileName">Name of the p profile.</param>
        /// <param name="p_ProfileDesc">The p profile desc.</param>
        /// <param name="p_CreatedDate">The p created date.</param>
        /// <param name="p_CreatedBy">The p created by.</param>
        /// <param name="p_SuperUser">The p super user.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_CREATEUSERPROFILE_INSERT(int p_ProfileId, string p_ProfileName, string p_ProfileDesc, DateTime p_CreatedDate, string p_CreatedBy, bool p_SuperUser)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_CREATEUSERPROFILE_INSERT);
                insertQuery = insertQuery.Replace("@p_ProfileId", p_ProfileId.ToString()).Replace("@p_ProfileName", string.Format("'{0}'", p_ProfileName)).Replace("@p_ProfileDesc", string.Format("'{0}'", p_ProfileDesc)).Replace("@p_CreatedDate", string.Format("'{0}'", p_CreatedDate)).Replace("@p_CreatedBy", string.Format("'{0}'", p_CreatedBy)).Replace("@p_SuperUser", p_SuperUser.ToString());
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CREATEUSERPROFILE_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_CUSTOMHIERARCHY_DETAIL)
        /// <summary>
        /// Inserting records into DIM_CUSTOM_HIERARCHY_PRODUCTS after getting from DIM_Products table
        /// </summary>
        /// <param name="p_Custom_Hierarchy_Id">The p custom hierarchy identifier.</param>
        /// <param name="p_CustomLevel1">The p custom level1.</param>
        /// <param name="p_CustomLevel2">The p custom level2.</param>
        /// <param name="p_CustomLevel3">The p custom level3.</param>
        /// <param name="p_UPC">The p upc.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_CUSTOMHIERARCHY_DETAIL(long p_Custom_Hierarchy_Id, string p_CustomLevel1, string p_CustomLevel2, string p_CustomLevel3, string p_UPC)
        {
            int result = 0;
            try
            {
                result = SP_INSERT_CUSTOMHIERARCHY_DETAIL_INSERT(p_Custom_Hierarchy_Id, p_CustomLevel1, p_CustomLevel2, p_CustomLevel3, p_UPC);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMHIERARCHY_DETAIL: "));
            }
            return result;
        }

        /// <summary>
        /// Insert records into DIM_CUSTOM_HIERARCHY_PRODUCTS
        /// </summary>
        /// <param name="p_Custom_Hierarchy_Id">The p custom hierarchy identifier.</param>
        /// <param name="p_CustomLevel1">The p custom level1.</param>
        /// <param name="p_CustomLevel2">The p custom level2.</param>
        /// <param name="p_CustomLevel3">The p custom level3.</param>
        /// <param name="p_UPC">The p upc.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_CUSTOMHIERARCHY_DETAIL_INSERT(long p_Custom_Hierarchy_Id, string p_CustomLevel1, string p_CustomLevel2, string p_CustomLevel3, string p_UPC)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_CUSTOMHIERARCHY_DETAIL_INSERT);
                insertQuery = insertQuery.Replace("@p_Custom_Hierarchy_Id", string.Format("'{0}'", p_Custom_Hierarchy_Id)).Replace("@p_CustomLevel1", string.Format("'{0}'", @p_CustomLevel1)).Replace("@p_CustomLevel2", string.Format("'{0}'", @p_CustomLevel2)).Replace("@p_CustomLevel3", string.Format("'{0}'", p_CustomLevel3)).Replace("@p_UPC", string.Format("'{0}'", p_UPC));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMHIERARCHY_DETAIL_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_CUSTOMHIERARCHY_MASTER)
        /// <summary>
        /// Insert record into DIM_CUSTOM_HIERARCHY_MASTER with help of submethod.
        /// </summary>
        /// <param name="p_Custom_Hierarchy_Id">The p custom hierarchy identifier.</param>
        /// <param name="p_UserName">Name of the p user.</param>
        /// <param name="p_HierarchyName">Name of the p hierarchy.</param>
        /// <param name="p_HierarchyDesc">The p hierarchy desc.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_CUSTOMHIERARCHY_MASTER(long p_Custom_Hierarchy_Id, string p_UserName, string p_HierarchyName, string p_HierarchyDesc)
        {
            int result = 0;
            try
            {
                result = SP_INSERT_CUSTOMHIERARCHY_MASTER_INSERT(p_Custom_Hierarchy_Id, p_UserName, p_HierarchyName, p_HierarchyDesc);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMHIERARCHY_MASTER: "));
            }
            return result;
        }

        /// <summary>
        /// Insert record into DIM_CUSTOM_HIERARCHY_MASTER .
        /// </summary>
        /// <param name="p_Custom_Hierarchy_Id">The p custom hierarchy identifier.</param>
        /// <param name="p_UserName">Name of the p user.</param>
        /// <param name="p_HierarchyName">Name of the p hierarchy.</param>
        /// <param name="p_HierarchyDesc">The p hierarchy desc.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_CUSTOMHIERARCHY_MASTER_INSERT(long p_Custom_Hierarchy_Id, string p_UserName, string p_HierarchyName, string p_HierarchyDesc)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_CUSTOMHIERARCHY_MASTER_INSERT);
                insertQuery = insertQuery.Replace("@p_Custom_Hierarchy_Id", p_Custom_Hierarchy_Id.ToString()).Replace("@p_UserName", string.Format("'{0}'", p_UserName)).Replace("@p_HierarchyName", string.Format("'{0}'", p_HierarchyName)).Replace("@p_HierarchyDesc", string.Format("'{0}'", p_HierarchyDesc));
                result = dataAccessProvider.Insert(insertQuery);

            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMHIERARCHY_MASTER_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_CUSTOMHIERARCHY_MASTER_WITHPARENT_HIERARCHY)
        /// <summary>
        /// Insert records into DIM_CUSTOM_HIERARCHY_MASTER using sub function
        /// </summary>
        /// <param name="p_Custom_Hierarchy_Id">The p custom hierarchy identifier.</param>
        /// <param name="p_UserName">Name of the p user.</param>
        /// <param name="p_HierarchyName">Name of the p hierarchy.</param>
        /// <param name="p_HierarchyDesc">The p hierarchy desc.</param>
        /// <param name="p_ParentCustomHierarchy_id">The p parent custom hierarchy identifier.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_CUSTOMHIERARCHY_MASTER_WITHPARENT_HIERARCHY(long p_Custom_Hierarchy_Id, string p_UserName, string p_HierarchyName, string p_HierarchyDesc, long p_ParentCustomHierarchy_id)
        {
            int result = 0;
            try
            {
                result = SP_INSERT_CUSTOMHIERARCHY_MASTER_WITHPARENT_HIERARCHY_INSERT(p_Custom_Hierarchy_Id, p_UserName, p_HierarchyName, p_HierarchyDesc, p_ParentCustomHierarchy_id);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMHIERARCHY_MASTER_WITHPARENT_HIERARCHY: "));
            }
            return result;
        }

        /// <summary>
        /// Insert records into DIM_CUSTOM_HIERARCHY_MASTER 
        /// </summary>
        /// <param name="p_Custom_Hierarchy_Id">The p custom hierarchy identifier.</param>
        /// <param name="p_UserName">Name of the p user.</param>
        /// <param name="p_HierarchyName">Name of the p hierarchy.</param>
        /// <param name="p_HierarchyDesc">The p hierarchy desc.</param>
        /// <param name="p_ParentCustomHierarchy_id">The p parent custom hierarchy identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_CUSTOMHIERARCHY_MASTER_WITHPARENT_HIERARCHY_INSERT(long p_Custom_Hierarchy_Id, string p_UserName, string p_HierarchyName, string p_HierarchyDesc, long p_ParentCustomHierarchy_id)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_CUSTOMHIERARCHY_MASTER_WITHPARENT_HIERARCHY_INSERT);
                insertQuery = insertQuery.Replace("@p_Custom_Hierarchy_Id", p_Custom_Hierarchy_Id.ToString()).Replace("@p_UserName", string.Format("'{0}'", p_UserName)).Replace("@p_HierarchyName", string.Format("'{0}'", p_HierarchyName)).Replace("@p_HierarchyDesc", string.Format("'{0}'", p_HierarchyDesc)).Replace("@p_ParentCustomHierarchy_id", p_ParentCustomHierarchy_id.ToString());
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMHIERARCHY_MASTER_WITHPARENT_HIERARCHY_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_CUSTOMHIERARCHY_PRODUCTS)
        /// <summary>
        /// Insert records into DIM_CUSTOM_HIERARCHY_PRODUCTS using sub method
        /// </summary>
        /// <param name="p_Custom_Hierarchy_Id">The p custom hierarchy identifier.</param>
        /// <param name="p_CustomLevel1">The p custom level1.</param>
        /// <param name="p_CustomLevel2">The p custom level2.</param>
        /// <param name="p_CustomLevel3">The p custom level3.</param>
        /// <param name="p_UPC">The p upc.</param>
        /// <param name="p_ProductID">The p product identifier.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_CUSTOMHIERARCHY_PRODUCTS(long p_Custom_Hierarchy_Id, string p_CustomLevel1, string p_CustomLevel2, string p_CustomLevel3, string p_UPC, long p_ProductID)
        {
            int result = 0;
            try
            {
                result = SP_INSERT_CUSTOMHIERARCHY_PRODUCTS_INSERT(p_Custom_Hierarchy_Id, p_CustomLevel1, p_CustomLevel2, p_CustomLevel3, p_UPC, p_ProductID);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMHIERARCHY_PRODUCTS: "));
            }
            return result;
        }

        /// <summary>
        /// Insert records into DIM_CUSTOM_HIERARCHY_PRODUCTS
        /// </summary>
        /// <param name="p_Custom_Hierarchy_Id">The p custom hierarchy identifier.</param>
        /// <param name="p_CustomLevel1">The p custom level1.</param>
        /// <param name="p_CustomLevel2">The p custom level2.</param>
        /// <param name="p_CustomLevel3">The p custom level3.</param>
        /// <param name="p_UPC">The p upc.</param>
        /// <param name="p_ProductID">The p product identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_CUSTOMHIERARCHY_PRODUCTS_INSERT(long p_Custom_Hierarchy_Id, string p_CustomLevel1, string p_CustomLevel2, string p_CustomLevel3, string p_UPC, long p_ProductID)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_CUSTOMHIERARCHY_PRODUCTS_INSERT);
                insertQuery = insertQuery.Replace("@p_Custom_Hierarchy_Id", p_Custom_Hierarchy_Id.ToString()).Replace("@p_CustomLevel1", string.Format("'{0}'", p_CustomLevel1)).Replace("@p_CustomLevel2", string.Format("'{0}'", p_CustomLevel2)).Replace("@p_CustomLevel3", string.Format("'{0}'", p_CustomLevel3)).Replace("@p_UPC", string.Format("'{0}'", p_UPC)).Replace("@p_ProductID", p_ProductID.ToString());
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMHIERARCHY_PRODUCTS_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_DIM_CUSTOM_STORES)
        /// <summary>
        /// Insert records into DIM_CUSTOM_STORES_DETAILS using sub function
        /// </summary>
        /// <param name="p_CustomStoreGroupId">The p custom store group identifier.</param>
        /// <param name="p_CustomLevel1">The p custom level1.</param>
        /// <param name="p_CustomLevel2">The p custom level2.</param>
        /// <param name="p_CustomLevel3">The p custom level3.</param>
        /// <param name="p_StoreNo">The p store no.</param>
        /// <param name="p_STORE_ID">The p store identifier.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_DIM_CUSTOM_STORES(long p_CustomStoreGroupId, string p_CustomLevel1, string p_CustomLevel2, string p_CustomLevel3, string p_StoreNo, string p_STORE_ID)
        {
            int result = 0;
            try
            {
                result = SP_INSERT_DIM_CUSTOM_STORES_INSERTSELECT(p_CustomStoreGroupId, p_CustomLevel1, p_CustomLevel2, p_CustomLevel3, p_StoreNo, p_STORE_ID);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_DIM_CUSTOM_STORES: "));
            }
            return result;
        }

        /// <summary>
        ///Insert records into DIM_CUSTOM_STORES_DETAILS after selecting record
        /// </summary>
        /// <param name="p_CustomStoreGroupId">The p custom store group identifier.</param>
        /// <param name="p_CustomLevel1">The p custom level1.</param>
        /// <param name="p_CustomLevel2">The p custom level2.</param>
        /// <param name="p_CustomLevel3">The p custom level3.</param>
        /// <param name="p_StoreNo">The p store no.</param>
        /// <param name="p_STORE_ID">The p store identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_DIM_CUSTOM_STORES_INSERTSELECT(long p_CustomStoreGroupId, string p_CustomLevel1, string p_CustomLevel2, string p_CustomLevel3, string p_StoreNo, string p_STORE_ID)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_DIM_CUSTOM_STORES_INSERTSELECT);
                insertQuery = insertQuery.Replace("@p_CustomStoreGroupId", p_CustomStoreGroupId.ToString()).Replace("@p_CustomLevel1", string.Format("'{0}'", p_CustomLevel1)).Replace("@p_CustomLevel2", string.Format("'{0}'", p_CustomLevel2)).Replace("@p_CustomLevel3", string.Format("'{0}'", p_CustomLevel3)).Replace("@p_StoreNo", string.Format("'{0}'", p_StoreNo)).Replace("@p_STORE_ID", string.Format("'{0}'", p_STORE_ID));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_DIM_CUSTOM_STORES_INSERTSELECT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_DIM_CUSTOM_STORES_DETAILS)
        /// <summary>
        ///Insert into DIM_CUSTOM_STORES_DETAILS using sub function
        /// </summary>
        /// <param name="p_CustomStoreGroupId">The p custom store group identifier.</param>
        /// <param name="p_CustomLevel1">The p custom level1.</param>
        /// <param name="p_CustomLevel2">The p custom level2.</param>
        /// <param name="p_CustomLevel3">The p custom level3.</param>
        /// <param name="p_StoreNo">The p store no.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_DIM_CUSTOM_STORES_DETAILS(long p_CustomStoreGroupId, string p_CustomLevel1, string p_CustomLevel2, string p_CustomLevel3, string p_StoreNo)
        {
            int result = 0;
            try
            {
                result = SP_INSERT_DIM_CUSTOM_STORES_DETAILS_INSERT(p_CustomStoreGroupId, p_CustomLevel1, p_CustomLevel2, p_CustomLevel3, p_StoreNo);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_DIM_CUSTOM_STORES_DETAILS: "));
            }
            return result;
        }

        /// <summary>
        /// Insert into DIM_CUSTOM_STORES_DETAILS after selecting record from DIM_STORES
        /// </summary>
        /// <param name="p_CustomStoreGroupId">The p custom store group identifier.</param>
        /// <param name="p_CustomLevel1">The p custom level1.</param>
        /// <param name="p_CustomLevel2">The p custom level2.</param>
        /// <param name="p_CustomLevel3">The p custom level3.</param>
        /// <param name="p_StoreNo">The p store no.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_DIM_CUSTOM_STORES_DETAILS_INSERT(long p_CustomStoreGroupId, string p_CustomLevel1, string p_CustomLevel2, string p_CustomLevel3, string p_StoreNo)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_DIM_CUSTOM_STORES_DETAILS_INSERT);
                insertQuery = insertQuery.Replace("@p_CustomStoreGroupId", p_CustomStoreGroupId.ToString()).Replace("@p_CustomLevel1", string.Format("'{0}'", p_CustomLevel1)).Replace("@p_CustomLevel2", string.Format("'{0}'", p_CustomLevel2)).Replace("@p_CustomLevel3", string.Format("'{0}'", p_CustomLevel3)).Replace("@p_StoreNo", string.Format("'{0}'", p_StoreNo));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_DIM_CUSTOM_STORES_DETAILS_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_DIM_CUSTOM_STORES_MASTER)
        /// <summary>
        /// Insert into DIM_CUSTOM_STORES_MASTER  using sub function
        /// </summary>
        /// <param name="p_CUSTOM_STORE_GROUP_ID">The p custom store group identifier.</param>
        /// <param name="p_USERNAME">The p username.</param>
        /// <param name="p_CUSTOM_STORE_NAME">Name of the p custom store.</param>
        /// <param name="p_CUSTOM_STORE_DESCRIPTION">The p custom store description.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_DIM_CUSTOM_STORES_MASTER(long p_CUSTOM_STORE_GROUP_ID, string p_USERNAME, string p_CUSTOM_STORE_NAME, string p_CUSTOM_STORE_DESCRIPTION)
        {
            int result = 0;
            try
            {
                result = SP_INSERT_DIM_CUSTOM_STORES_MASTER_INSERT(p_CUSTOM_STORE_GROUP_ID, p_USERNAME, p_CUSTOM_STORE_NAME, p_CUSTOM_STORE_DESCRIPTION);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_DIM_CUSTOM_STORES_MASTER: "));
            }
            return result;
        }

        /// <summary>
        /// Insert into DIM_CUSTOM_STORES_MASTER
        /// </summary>
        /// <param name="p_CUSTOM_STORE_GROUP_ID">The p custom store group identifier.</param>
        /// <param name="p_USERNAME">The p username.</param>
        /// <param name="p_CUSTOM_STORE_NAME">Name of the p custom store.</param>
        /// <param name="p_CUSTOM_STORE_DESCRIPTION">The p custom store description.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_DIM_CUSTOM_STORES_MASTER_INSERT(long p_CUSTOM_STORE_GROUP_ID, string p_USERNAME, string p_CUSTOM_STORE_NAME, string p_CUSTOM_STORE_DESCRIPTION)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_DIM_CUSTOM_STORES_MASTER_INSERT);
                insertQuery = insertQuery.Replace("@p_CUSTOM_STORE_GROUP_ID", p_CUSTOM_STORE_GROUP_ID.ToString()).Replace("@p_USERNAME", string.Format("'{0}'", p_USERNAME)).Replace("@p_CUSTOM_STORE_NAME", string.Format("'{0}'", p_CUSTOM_STORE_NAME)).Replace("@p_CUSTOM_STORE_DESCRIPTION", string.Format("'{0}'", p_CUSTOM_STORE_DESCRIPTION));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_DIM_CUSTOM_STORES_MASTER_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_DUPLICATECUSTOMHIERARCHY)
        /// <summary>
        /// Insert records into two tables DIM_CUSTOM_HIERARCHY_MASTER  and DIM_CUSTOM_HIERARCHY_PRODUCTS using sub function.
        /// </summary>
        /// <param name="p_Custom_Hierarchy_Id">The p custom hierarchy identifier.</param>
        /// <param name="p_New_Custom_Hierarchy_Id">The p new custom hierarchy identifier.</param>
        /// <param name="p_UserName">Name of the p user.</param>
        /// <param name="p_New_HierarchyName">Name of the p new hierarchy.</param>
        /// <param name="p_New_HierarchyDesc">The p new hierarchy desc.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_DUPLICATECUSTOMHIERARCHY(long p_Custom_Hierarchy_Id, long p_New_Custom_Hierarchy_Id, string p_UserName, string p_New_HierarchyName, string p_New_HierarchyDesc)
        {
            int result1 = 0, result2 = 0;
            try
            {
                result1 = SP_INSERT_DUPLICATECUSTOMHIERARCHY_INSERT(p_New_Custom_Hierarchy_Id, p_UserName, p_New_HierarchyName, p_New_HierarchyDesc);
                if (result1 > 0)
                    result2 = SP_INSERT_DUPLICATECUSTOMHIERARCHY_INSERTSELECT(p_Custom_Hierarchy_Id, p_New_Custom_Hierarchy_Id);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_DUPLICATECUSTOMHIERARCHY: "));
            }
            return result2;
        }

        /// <summary>
        /// Insert records into DIM_CUSTOM_HIERARCHY_MASTER.
        /// </summary>
        /// <param name="p_New_Custom_Hierarchy_Id">The p new custom hierarchy identifier.</param>
        /// <param name="p_UserName">Name of the p user.</param>
        /// <param name="p_New_HierarchyName">Name of the p new hierarchy.</param>
        /// <param name="p_New_HierarchyDesc">The p new hierarchy desc.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_DUPLICATECUSTOMHIERARCHY_INSERT(long p_New_Custom_Hierarchy_Id, string p_UserName, string p_New_HierarchyName, string p_New_HierarchyDesc)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_DUPLICATECUSTOMHIERARCHY_INSERT);
                insertQuery = insertQuery.Replace("@p_New_Custom_Hierarchy_Id", p_New_Custom_Hierarchy_Id.ToString()).Replace("@p_UserName", string.Format("'{0}'", p_UserName)).Replace("@p_New_HierarchyName", string.Format("'{0}'", p_New_HierarchyName)).Replace("@p_New_HierarchyDesc", string.Format("'{0}'", p_New_HierarchyDesc));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_DUPLICATECUSTOMHIERARCHY_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }

        /// <summary>
        /// Insert records into DIM_CUSTOM_HIERARCHY_PRODUCTS after selecting record from DIM_CUSTOM_HIERARCHY_PRODUCTS.
        /// </summary>
        /// <param name="p_Custom_Hierarchy_Id">The p custom hierarchy identifier.</param>
        /// <param name="p_New_Custom_Hierarchy_Id">The p new custom hierarchy identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_DUPLICATECUSTOMHIERARCHY_INSERTSELECT(long p_Custom_Hierarchy_Id, long p_New_Custom_Hierarchy_Id)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_DUPLICATECUSTOMHIERARCHY_INSERTSELECT);
                insertQuery = insertQuery.Replace("@p_Custom_Hierarchy_Id", p_Custom_Hierarchy_Id.ToString()).Replace("@p_New_Custom_Hierarchy_Id", p_New_Custom_Hierarchy_Id.ToString());
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_DUPLICATECUSTOMHIERARCHY_INSERTSELECT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_DUPLICATECUSTOM_STORE)
        /// <summary>
        /// Insert records into two table DIM_CUSTOM_STORES_MASTER and DIM_CUSTOM_STORES_DETAILS using sub functions
        /// </summary>
        /// <param name="p_Custom_Store_Id">The p custom store identifier.</param>
        /// <param name="p_New_Custom_Store_Id">The p new custom store identifier.</param>
        /// <param name="p_UserName">Name of the p user.</param>
        /// <param name="p_New_StoreName">Name of the p new store.</param>
        /// <param name="p_New_StoreDesc">The p new store desc.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_DUPLICATECUSTOM_STORE(long p_Custom_Store_Id, long p_New_Custom_Store_Id, string p_UserName, string p_New_StoreName, string p_New_StoreDesc)
        {
            int result1 = 0, result2 = 0;
            try
            {
                result1 = SP_INSERT_DUPLICATECUSTOM_STORE_INSERT(p_New_Custom_Store_Id, p_UserName, p_New_StoreName, p_New_StoreDesc);
                if (result1 > 0)
                    result2 = SP_INSERT_DUPLICATECUSTOM_STORE_INSERTSELECT(p_Custom_Store_Id, p_New_Custom_Store_Id);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_DUPLICATECUSTOM_STORE: "));
            }
            return result2;
        }

        /// <summary>
        /// Insert records into DIM_CUSTOM_STORES_MASTER.
        /// </summary>
        /// <param name="p_New_Custom_Store_Id">The p new custom store identifier.</param>
        /// <param name="p_UserName">Name of the p user.</param>
        /// <param name="p_New_StoreName">Name of the p new store.</param>
        /// <param name="p_New_StoreDesc">The p new store desc.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_DUPLICATECUSTOM_STORE_INSERT(long p_New_Custom_Store_Id, string p_UserName, string p_New_StoreName, string p_New_StoreDesc)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_DUPLICATECUSTOM_STORE_INSERT);
                insertQuery = insertQuery.Replace("@p_New_Custom_Store_Id", p_New_Custom_Store_Id.ToString()).Replace("@p_UserName", string.Format("'{0}'", p_UserName)).Replace("@p_New_StoreName", string.Format("'{0}'", p_New_StoreName)).Replace("@p_New_StoreDesc", string.Format("'{0}'", p_New_StoreDesc));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_DUPLICATECUSTOM_STORE_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }

        /// <summary>
        /// Insert records  DIM_CUSTOM_STORES_DETAILS after getting records from DIM_CUSTOM_STORES_DETAILS .
        /// </summary>
        /// <param name="p_Custom_Store_Id">The p custom store identifier.</param>
        /// <param name="p_New_Custom_Store_Id">The p new custom store identifier.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_DUPLICATECUSTOM_STORE_INSERTSELECT(long p_Custom_Store_Id, long p_New_Custom_Store_Id)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_DUPLICATECUSTOM_STORE_INSERTSELECT);
                insertQuery = insertQuery.Replace("@p_Custom_Store_Id", p_Custom_Store_Id.ToString()).Replace("@p_New_Custom_Store_Id", p_New_Custom_Store_Id.ToString());
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_DUPLICATECUSTOM_STORE_INSERTSELECT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_FEEDBACKDETAILS)
        /// <summary>
        /// This stored procedure function is based on three sub functions.
        /// </summary>
        /// <param name="p_Name">Name of the p.</param>
        /// <param name="p_Email">The p email.</param>
        /// <param name="p_Category">The p category.</param>
        /// <param name="p_Subject">The p subject.</param>
        /// <param name="p_Description">The p description.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_FEEDBACKDETAILS(string p_Name, string p_Email, string p_Category, string p_Subject, string p_Description)
        {
            int LastRowId, TotalRows;
            int result = 0;
            try
            {
                TotalRows = SP_INSERT_FEEDBACKDETAILS_SELECTCOUNT();
                if (TotalRows == 0)
                    LastRowId = 1;
                else
                {
                    LastRowId = SP_INSERT_FEEDBACKDETAILS_SELECT();
                    LastRowId = LastRowId + 1;
                }
                result = SP_INSERT_FEEDBACKDETAILS_INSERT(LastRowId, p_Name, p_Email, p_Category, p_Subject, p_Description);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_FEEDBACKDETAILS: "));
            }
            return result;
        }

        /// <summary>
        /// Selecting total count of records from ICONTROL.DIM_FEEDBACK.
        /// </summary>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_FEEDBACKDETAILS_SELECTCOUNT()
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string selectQuery = helper.GetResourceString(AppConstants.SP_INSERT_FEEDBACKDETAILS_SELECTCOUNT);
                result = dataAccessProvider.SelectSingleItem(selectQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_FEEDBACKDETAILS_SELECTCOUNT: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }

        /// <summary>
        /// Select EntryId from ICONTROL.DIM_FEEDBACK .
        /// </summary>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_FEEDBACKDETAILS_SELECT()
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string selectQuery = helper.GetResourceString(AppConstants.SP_INSERT_FEEDBACKDETAILS_SELECT);
                result = dataAccessProvider.SelectSingleItem(selectQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_FEEDBACKDETAILS_SELECT: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }

        /// <summary>
        /// Insert records into ICONTROL.DIM_FEEDBACK.
        /// </summary>
        /// <param name="LastRowId">The last row identifier.</param>
        /// <param name="p_Name">Name of the p.</param>
        /// <param name="p_Email">The p email.</param>
        /// <param name="p_Category">The p category.</param>
        /// <param name="p_Subject">The p subject.</param>
        /// <param name="p_Description">The p description.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_FEEDBACKDETAILS_INSERT(int LastRowId, string p_Name, string p_Email, string p_Category, string p_Subject, string p_Description)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_FEEDBACKDETAILS_INSERT);
                insertQuery = insertQuery.Replace("@LastRowId", LastRowId.ToString()).Replace("@p_Name", string.Format("'{0}'", p_Name)).Replace("@p_Email", string.Format("'{0}'", p_Email)).Replace("@p_Category", string.Format("'{0}'", p_Category)).Replace("@p_Subject", string.Format("'{0}'", p_Subject)).Replace("@p_Description", string.Format("'{0}'", p_Description));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_FEEDBACKDETAILS_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_PRODUCT_HISTORY)
        /// <summary>
        /// Insert records into Dim_Products_History  using sub function. Another function with same name but with p_UPC data type as string
        /// </summary>
        /// <param name="p_NewProductHierarchyId">The p new product hierarchy identifier.</param>
        /// <param name="p_UPC">The p upc.</param>
        /// <param name="p_BRAND">The p brand.</param>
        /// <param name="p_PRODUCTSIZE">The p productsize.</param>
        /// <param name="p_UPDATEDON">The p updatedon.</param>
        /// <param name="p_USERNAME">The p username.</param>
        /// <param name="p_NEWBRAND">The p newbrand.</param>
        /// <param name="p_NEWPRODUCTSIZE">The p newproductsize.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_PRODUCT_HISTORY(long p_NewProductHierarchyId, long p_UPC, string p_BRAND, string p_PRODUCTSIZE, DateTime p_UPDATEDON, string p_USERNAME, string p_NEWBRAND, string p_NEWPRODUCTSIZE)
        {
            int result = 0;
            try
            {
                result = SP_INSERT_PRODUCT_HISTORY_INSERT(p_NewProductHierarchyId, p_UPC, p_BRAND, p_PRODUCTSIZE, p_UPDATEDON, p_USERNAME, p_NEWBRAND, p_NEWPRODUCTSIZE);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_PRODUCT_HISTORY: "));
            }
            return result;
        }

        /// <summary>
        /// Insert records into Dim_Products_History 
        /// </summary>
        /// <param name="p_NewProductHierarchyId">The p new product hierarchy identifier.</param>
        /// <param name="p_UPC">The p upc.</param>
        /// <param name="p_BRAND">The p brand.</param>
        /// <param name="p_PRODUCTSIZE">The p productsize.</param>
        /// <param name="p_UPDATEDON">The p updatedon.</param>
        /// <param name="p_USERNAME">The p username.</param>
        /// <param name="p_NEWBRAND">The p newbrand.</param>
        /// <param name="p_NEWPRODUCTSIZE">The p newproductsize.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_PRODUCT_HISTORY_INSERT(long p_NewProductHierarchyId, long p_UPC, string p_BRAND, string p_PRODUCTSIZE, DateTime p_UPDATEDON, string p_USERNAME, string p_NEWBRAND, string p_NEWPRODUCTSIZE)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_PRODUCT_HISTORY_INSERT);
                insertQuery = insertQuery.Replace("@p_NewProductHierarchyId", p_NewProductHierarchyId.ToString()).Replace("@p_UPC", p_UPC.ToString()).Replace("@p_BRAND", string.Format("'{0}'", p_BRAND)).Replace("@p_PRODUCTSIZE", string.Format("'{0}'", p_PRODUCTSIZE)).Replace("@p_UPDATEDON", string.Format("'{0}'", p_UPDATEDON)).Replace("@p_USERNAME", string.Format("'{0}'", p_USERNAME)).Replace("@p_NEWBRAND", string.Format("'{0}'", p_NEWBRAND)).Replace("@p_NEWPRODUCTSIZE", string.Format("'{0}'", p_NEWPRODUCTSIZE));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_PRODUCT_HISTORY_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_PRODUCT_HISTORY)
        /// <summary>
        /// Insert records into Dim_Products_History using sub function.  Another function with same name but with parameter p_UPC data type as long
        /// </summary>
        /// <param name="p_NewProductHierarchyId">The p new product hierarchy identifier.</param>
        /// <param name="p_UPC">The p upc.</param>
        /// <param name="p_BRAND">The p brand.</param>
        /// <param name="p_PRODUCTSIZE">The p productsize.</param>
        /// <param name="p_UPDATEDON">The p updatedon.</param>
        /// <param name="p_USERNAME">The p username.</param>
        /// <param name="p_NEWBRAND">The p newbrand.</param>
        /// <param name="p_NEWPRODUCTSIZE">The p newproductsize.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_PRODUCT_HISTORY(long p_NewProductHierarchyId, string p_UPC, string p_BRAND, string p_PRODUCTSIZE, DateTime p_UPDATEDON, string p_USERNAME, string p_NEWBRAND, string p_NEWPRODUCTSIZE)
        {
            int result = 0;
            try
            {
                result = SP_INSERT_PRODUCT_HISTORY_INSERT(p_NewProductHierarchyId, p_UPC, p_BRAND, p_PRODUCTSIZE, p_UPDATEDON, p_USERNAME, p_NEWBRAND, p_NEWPRODUCTSIZE);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_PRODUCT_HISTORY: "));
            }
            return result;
        }

        /// <summary>
        /// Insert records into Dim_Products_History 
        /// </summary>
        /// <param name="p_NewProductHierarchyId">The p new product hierarchy identifier.</param>
        /// <param name="p_UPC">The p upc.</param>
        /// <param name="p_BRAND">The p brand.</param>
        /// <param name="p_PRODUCTSIZE">The p productsize.</param>
        /// <param name="p_UPDATEDON">The p updatedon.</param>
        /// <param name="p_USERNAME">The p username.</param>
        /// <param name="p_NEWBRAND">The p newbrand.</param>
        /// <param name="p_NEWPRODUCTSIZE">The p newproductsize.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_PRODUCT_HISTORY_INSERT(long p_NewProductHierarchyId, string p_UPC, string p_BRAND, string p_PRODUCTSIZE, DateTime p_UPDATEDON, string p_USERNAME, string p_NEWBRAND, string p_NEWPRODUCTSIZE)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_PRODUCT_HISTORY_INSERT);
                insertQuery = insertQuery.Replace("@p_NewProductHierarchyId", p_NewProductHierarchyId.ToString()).Replace("@p_UPC", string.Format("'{0}'", p_UPC)).Replace("@p_BRAND", string.Format("'{0}'", p_BRAND)).Replace("@p_PRODUCTSIZE", string.Format("'{0}'", p_PRODUCTSIZE)).Replace("@p_UPDATEDON", string.Format("'{0}'", p_UPDATEDON)).Replace("@p_USERNAME", string.Format("'{0}'", p_USERNAME)).Replace("@p_NEWBRAND", string.Format("'{0}'", p_NEWBRAND)).Replace("@p_NEWPRODUCTSIZE", string.Format("'{0}'", p_NEWPRODUCTSIZE));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_PRODUCT_HISTORY_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_UAM_ONLINETEMPLATE)
        /// <summary>
        /// Various opeartion of select, insert or update using sub functions.
        /// </summary>
        /// <param name="pTEMPLATE_ID">The p template identifier.</param>
        /// <param name="pDATEREQUESTED">The p daterequested.</param>
        /// <param name="pNAMEOFREQUESTOR">The p nameofrequestor.</param>
        /// <param name="pORGANIZATIONNAME">The p organizationname.</param>
        /// <param name="pORGANIZATIONADDRESS">The p organizationaddress.</param>
        /// <param name="pPRIMARYCONTACT">The p primarycontact.</param>
        /// <param name="pCONTACTPHONE">The p contactphone.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_UAM_ONLINETEMPLATE(int pTEMPLATE_ID, string pDATEREQUESTED, string pNAMEOFREQUESTOR, string pORGANIZATIONNAME, string pORGANIZATIONADDRESS, string pPRIMARYCONTACT, string pCONTACTPHONE)
        {
            int LastRowId, TotalRows;
            int result = 0;
            try
            {
                TotalRows = SP_INSERT_UAM_ONLINETEMPLATE_SELECTCOUNT();
                if (TotalRows == 1)
                    LastRowId = 1;
                else
                {
                    LastRowId = SP_INSERT_UAM_ONLINETEMPLATE_SELECT();
                    LastRowId = LastRowId + 1;
                }
                if (pTEMPLATE_ID == 0)
                    result = SP_INSERT_UAM_ONLINETEMPLATE_INSERT(LastRowId, pDATEREQUESTED, pNAMEOFREQUESTOR, pORGANIZATIONNAME, pORGANIZATIONADDRESS, pPRIMARYCONTACT, pCONTACTPHONE);
                else
                    result = SP_INSERT_UAM_ONLINETEMPLATE_UPDATE(pTEMPLATE_ID, pDATEREQUESTED, pNAMEOFREQUESTOR, pORGANIZATIONNAME, pORGANIZATIONADDRESS, pPRIMARYCONTACT, pCONTACTPHONE);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_UAM_ONLINETEMPLATE_SELECTCOUNT: "));
            }
            return result;
        }

        /// <summary>
        /// Select count of total records from ICONTROL.UAM_ONLINETEMPLATE
        /// </summary>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_UAM_ONLINETEMPLATE_SELECTCOUNT()
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string selectQuery = helper.GetResourceString(AppConstants.SP_INSERT_UAM_ONLINETEMPLATE_SELECTCOUNT);
                result = dataAccessProvider.SelectSingleItem(selectQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_UAM_ONLINETEMPLATE_SELECTCOUNT: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }

        /// <summary>
        /// Select Template_Id from ICONTROL.UAM_ONLINETEMPLATE.
        /// </summary>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_UAM_ONLINETEMPLATE_SELECT()
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string selectQuery = helper.GetResourceString(AppConstants.SP_INSERT_UAM_ONLINETEMPLATE_SELECT);
                result = dataAccessProvider.SelectSingleItem(selectQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_UAM_ONLINETEMPLATE_SELECT: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }

        /// <summary>
        /// Insert records into ICONTROL.UAM_ONLINETEMPLATE .
        /// </summary>
        /// <param name="LastRowId">The last row identifier.</param>
        /// <param name="pDATEREQUESTED">The p daterequested.</param>
        /// <param name="pNAMEOFREQUESTOR">The p nameofrequestor.</param>
        /// <param name="pORGANIZATIONNAME">The p organizationname.</param>
        /// <param name="pORGANIZATIONADDRESS">The p organizationaddress.</param>
        /// <param name="pPRIMARYCONTACT">The p primarycontact.</param>
        /// <param name="pCONTACTPHONE">The p contactphone.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_UAM_ONLINETEMPLATE_INSERT(int LastRowId, string pDATEREQUESTED, string pNAMEOFREQUESTOR, string pORGANIZATIONNAME, string pORGANIZATIONADDRESS, string pPRIMARYCONTACT, string pCONTACTPHONE)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_UAM_ONLINETEMPLATE_INSERT);
                insertQuery = insertQuery.Replace("@LastRowId", LastRowId.ToString()).Replace("@pDATEREQUESTED", string.Format("'{0}'", pDATEREQUESTED)).Replace("@pNAMEOFREQUESTOR", string.Format("'{0}'", pNAMEOFREQUESTOR)).Replace("@pORGANIZATIONNAME", string.Format("'{0}'", pORGANIZATIONNAME)).Replace("@pORGANIZATIONADDRESS", string.Format("'{0}'", pORGANIZATIONADDRESS)).Replace("@pPRIMARYCONTACT", string.Format("'{0}'", pPRIMARYCONTACT)).Replace("@pCONTACTPHONE", string.Format("'{0}'", pCONTACTPHONE));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_UAM_ONLINETEMPLATE_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }

        /// <summary>
        /// Update record in table ICONTROL.UAM_ONLINETEMPLATE 
        /// </summary>
        /// <param name="pTEMPLATE_ID">The p template identifier.</param>
        /// <param name="pDATEREQUESTED">The p daterequested.</param>
        /// <param name="pNAMEOFREQUESTOR">The p nameofrequestor.</param>
        /// <param name="pORGANIZATIONNAME">The p organizationname.</param>
        /// <param name="pORGANIZATIONADDRESS">The p organizationaddress.</param>
        /// <param name="pPRIMARYCONTACT">The p primarycontact.</param>
        /// <param name="pCONTACTPHONE">The p contactphone.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_UAM_ONLINETEMPLATE_UPDATE(int pTEMPLATE_ID, string pDATEREQUESTED, string pNAMEOFREQUESTOR, string pORGANIZATIONNAME, string pORGANIZATIONADDRESS, string pPRIMARYCONTACT, string pCONTACTPHONE)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string updateQuery = helper.GetResourceString(AppConstants.SP_INSERT_UAM_ONLINETEMPLATE_UPDATE);
                updateQuery = updateQuery.Replace("@pTEMPLATE_ID", pTEMPLATE_ID.ToString()).Replace("@pDATEREQUESTED", string.Format("'{0}'", pDATEREQUESTED)).Replace("@pNAMEOFREQUESTOR", string.Format("'{0}'", pNAMEOFREQUESTOR)).Replace("@pORGANIZATIONNAME", string.Format("'{0}'", pORGANIZATIONNAME)).Replace("@pORGANIZATIONADDRESS", string.Format("'{0}'", pORGANIZATIONADDRESS)).Replace("@pPRIMARYCONTACT", string.Format("'{0}'", pPRIMARYCONTACT)).Replace("@pCONTACTPHONE", string.Format("'{0}'", pCONTACTPHONE));
                result = dataAccessProvider.Update(updateQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_UAM_ONLINETEMPLATE_UPDATE: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_INSERT_USERDATAACCESS)
        /// <summary>
        /// Getting UserId from UserDataAccess and then saving records into UserDataAccess using sub functions
        /// </summary>
        /// <param name="p_ProfileId">The p profile identifier.</param>
        /// <param name="p_Username">The p username.</param>
        /// <param name="p_iControlUserId">The p i control user identifier.</param>
        /// <param name="p_Name">Name of the p.</param>
        /// <param name="p_Email">The p email.</param>
        /// <param name="p_Role">The p role.</param>
        /// <param name="p_AccessDescription">The p access description.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_USERDATAACCESS(int p_ProfileId, string p_Username, int p_iControlUserId, string p_Name, string p_Email, string p_Role, string p_AccessDescription)
        {
            string p_UserId;
            int result = 0;
            try
            {
                p_UserId = SP_INSERT_USERDATAACCESS_SELECT();
                if (!string.IsNullOrEmpty(p_UserId))
                {
                    p_UserId = Convert.ToString(Convert.ToInt32(p_UserId) + 1);
                }
                else
                    p_UserId = "1";
                result = SP_INSERT_USERDATAACCESS_INSERT(p_UserId, p_ProfileId, p_Username, p_iControlUserId, p_Name, p_Email, p_Role, p_AccessDescription);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_USERDATAACCESS: "));
            }
            return result;
        }

        /// <summary>
        /// Getting UserId from UserDataAccess.
        /// </summary>
        /// <returns>System.String.</returns>
        private string SP_INSERT_USERDATAACCESS_SELECT()
        {
            dynamic result = string.Empty;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string selectQuery = helper.GetResourceString(AppConstants.SP_INSERT_USERDATAACCESS_SELECT);
                result = dataAccessProvider.SelectSingleItem(selectQuery);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_USERDATAACCESS_SELECT: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToString(result);
        }

        /// <summary>
        /// Inserting records into UserDataAccess.
        /// </summary>
        /// <param name="p_UserId">The p user identifier.</param>
        /// <param name="p_ProfileId">The p profile identifier.</param>
        /// <param name="p_Username">The p username.</param>
        /// <param name="p_iControlUserId">The p i control user identifier.</param>
        /// <param name="p_Name">Name of the p.</param>
        /// <param name="p_Email">The p email.</param>
        /// <param name="p_Role">The p role.</param>
        /// <param name="p_AccessDescription">The p access description.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_USERDATAACCESS_INSERT(string p_UserId, int p_ProfileId, string p_Username, int p_iControlUserId, string p_Name, string p_Email, string p_Role, string p_AccessDescription)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string inserteQuery = helper.GetResourceString(AppConstants.SP_INSERT_USERDATAACCESS_INSERT);
                inserteQuery = inserteQuery.Replace("@p_UserId", string.Format("'{0}'", p_UserId)).Replace("@p_ProfileId", p_ProfileId.ToString()).Replace("@p_Username", string.Format("'{0}'", p_Username)).Replace("@p_iControlUserId", p_iControlUserId.ToString()).Replace("@p_Name", string.Format("'{0}'", p_Name)).Replace("@p_Email", string.Format("'{0}'", p_Email)).Replace("@p_Role", string.Format("'{0}'", p_Role)).Replace("@p_AccessDescription", string.Format("'{0}'", p_AccessDescription));
                result = dataAccessProvider.Insert(inserteQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_USERDATAACCESS_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_UPDATE_MARKET_BASKET_REQUEST_TEXTVALUES)
        /// <summary>
        /// Update records in table MARKET_BASKET_REQUEST_TEXTVALUES using sub function.
        /// </summary>
        /// <param name="p_EXEC_IDENTIFIER">The p execute identifier.</param>
        /// <param name="p_USERNAME">The p username.</param>
        /// <param name="p_BASKET_DESCRIPTION">The p basket description.</param>
        /// <param name="p_ETNICITY">The p etnicity.</param>
        /// <param name="p_INCOME">The p income.</param>
        /// <param name="p_NOOFCHILDREN">The p noofchildren.</param>
        /// <param name="p_NOOFADULTS">The p noofadults.</param>
        /// <param name="p_GENDER">The p gender.</param>
        /// <param name="p_AGEHH">The p agehh.</param>
        /// <param name="p_GENERATIONGROUPING">The p generationgrouping.</param>
        /// <param name="p_GOURMETENTHUSIASTS">The p gourmetenthusiasts.</param>
        /// <param name="p_DOGENTHUSIASTS">The p dogenthusiasts.</param>
        /// <param name="p_CATENTHUSIASTS">The p catenthusiasts.</param>
        /// <param name="p_SIZEOfHH">The p size of hh.</param>
        /// <param name="p_DEPARTMENTS">The p departments.</param>
        /// <param name="p_CATEGORIES">The p categories.</param>
        /// <param name="p_SUBCATEGORIES">The p subcategories.</param>
        /// <param name="p_SEGMENTS">The p segments.</param>
        /// <param name="p_BRANDS">The p brands.</param>
        /// <param name="p_UPCS">The p upcs.</param>
        /// <param name="p_ACTIVEFILTERS">The p activefilters.</param>
        /// <returns>System.Int32.</returns>
        public int SP_UPDATE_MARKET_BASKET_REQUEST_TEXTVALUES(string p_EXEC_IDENTIFIER, string p_ETNICITY, string p_INCOME, string p_NOOFCHILDREN, string p_NOOFADULTS, string p_GENDER, string p_AGEHH, string p_GENERATIONGROUPING, string p_GOURMETENTHUSIASTS, string p_DOGENTHUSIASTS, string p_CATENTHUSIASTS, string p_SIZEOfHH, string p_DEPARTMENTS, string p_CATEGORIES, string p_SUBCATEGORIES, string p_SEGMENTS, string p_BRANDS, string p_UPCS, string p_ACTIVEFILTERS)
        {
            int result = 0;
            try
            {
                result = SP_UPDATE_MARKET_BASKET_REQUEST_TEXTVALUES_UPDATE(p_EXEC_IDENTIFIER, p_ETNICITY, p_INCOME, p_NOOFCHILDREN, p_NOOFADULTS, p_GENDER, p_AGEHH, p_GENERATIONGROUPING, p_GOURMETENTHUSIASTS, p_DOGENTHUSIASTS, p_CATENTHUSIASTS, p_SIZEOfHH, p_DEPARTMENTS, p_CATEGORIES, p_SUBCATEGORIES, p_SEGMENTS, p_BRANDS, p_UPCS, p_ACTIVEFILTERS);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_UAM_ONLINETEMPLATE_SELECTCOUNT: "));
            }
            return result;
        }

        /// <summary>
        /// Update records in table MARKET_BASKET_REQUEST_TEXTVALUES.
        /// </summary>
        /// <param name="p_EXEC_IDENTIFIER">The p execute identifier.</param>
        /// <param name="p_ETNICITY">The p etnicity.</param>
        /// <param name="p_INCOME">The p income.</param>
        /// <param name="p_NOOFCHILDREN">The p noofchildren.</param>
        /// <param name="p_NOOFADULTS">The p noofadults.</param>
        /// <param name="p_GENDER">The p gender.</param>
        /// <param name="p_AGEHH">The p agehh.</param>
        /// <param name="p_GENERATIONGROUPING">The p generationgrouping.</param>
        /// <param name="p_GOURMETENTHUSIASTS">The p gourmetenthusiasts.</param>
        /// <param name="p_DOGENTHUSIASTS">The p dogenthusiasts.</param>
        /// <param name="p_CATENTHUSIASTS">The p catenthusiasts.</param>
        /// <param name="p_SIZEOfHH">The p size of hh.</param>
        /// <param name="p_DEPARTMENTS">The p departments.</param>
        /// <param name="p_CATEGORIES">The p categories.</param>
        /// <param name="p_SUBCATEGORIES">The p subcategories.</param>
        /// <param name="p_SEGMENTS">The p segments.</param>
        /// <param name="p_BRANDS">The p brands.</param>
        /// <param name="p_UPCS">The p upcs.</param>
        /// <param name="p_ACTIVEFILTERS">The p activefilters.</param>
        /// <returns>System.Int32.</returns>
        private int SP_UPDATE_MARKET_BASKET_REQUEST_TEXTVALUES_UPDATE(string p_EXEC_IDENTIFIER, string p_ETNICITY, string p_INCOME, string p_NOOFCHILDREN, string p_NOOFADULTS, string p_GENDER, string p_AGEHH, string p_GENERATIONGROUPING, string p_GOURMETENTHUSIASTS, string p_DOGENTHUSIASTS, string p_CATENTHUSIASTS, string p_SIZEOfHH, string p_DEPARTMENTS, string p_CATEGORIES, string p_SUBCATEGORIES, string p_SEGMENTS, string p_BRANDS, string p_UPCS, string p_ACTIVEFILTERS)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string updateQuery = helper.GetResourceString(AppConstants.SP_UPDATE_MARKET_BASKET_REQUEST_TEXTVALUES_UPDATE);
                updateQuery = updateQuery
                    .Replace("@p_EXEC_IDENTIFIER", string.Format("'{0}'", p_EXEC_IDENTIFIER))
                    .Replace("@p_ETNICITY", string.Format("'{0}'", p_ETNICITY))
                    .Replace("@p_INCOME", string.Format("'{0}'", p_INCOME))
                    .Replace("@p_NOOFCHILDREN", string.Format("'{0}'", p_NOOFCHILDREN))
                    .Replace("@p_NOOFADULTS", string.Format("'{0}'", p_NOOFADULTS))
                    .Replace("@p_GENDER", string.Format("'{0}'", p_GENDER))
                    .Replace("@p_AGEHH", string.Format("'{0}'", p_AGEHH))
                    .Replace("@p_GENERATIONGROUPING", string.Format("'{0}'", p_GENERATIONGROUPING))
                    .Replace("@p_DOGENTHUSIASTS", string.Format("'{0}'", p_DOGENTHUSIASTS))
                    .Replace("@p_GOURMETENTHUSIASTS", string.Format("'{0}'", p_GOURMETENTHUSIASTS))
                    .Replace("@p_CATENTHUSIASTS", string.Format("'{0}'", p_CATENTHUSIASTS))
                    .Replace("@p_SIZEOfHH", string.Format("'{0}'", p_SIZEOfHH))
                    .Replace("@p_DEPARTMENTS", string.Format("'{0}'", p_DEPARTMENTS))
                    .Replace("@p_CATEGORIES", string.Format("'{0}'", p_CATEGORIES))
                    .Replace("@p_SUBCATEGORIES", string.Format("'{0}'", p_SUBCATEGORIES))
                    .Replace("@p_SEGMENTS", string.Format("'{0}'", p_SEGMENTS))
                    .Replace("@p_BRANDS", string.Format("'{0}'", p_BRANDS))
                    .Replace("@p_UPCS", string.Format("'{0}'", p_UPCS))
                    .Replace("@p_ACTIVEFILTERS", string.Format("'{0}'", p_ACTIVEFILTERS));
                result = dataAccessProvider.Update(updateQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_UPDATE_MARKET_BASKET_REQUEST_TEXTVALUES_UPDATE: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_UPDATE_MARKET_REQUEST)
        /// <summary>
        /// Update record in table MARKET_BASKET_REQUESTS using sub function.
        /// </summary>
        /// <param name="p_EXEC_IDENTIFIER">The p execute identifier.</param>
        /// <param name="p_USERNAME">The p username.</param>
        /// <param name="p_VENDOR_LIST">The p vendor list.</param>
        /// <param name="p_SUBMISSION_DATETIME">The p submission datetime.</param>
        /// <param name="p_FROM_DATE">The p from date.</param>
        /// <param name="p_TO_DATE">The p to date.</param>
        /// <param name="p_BASKET_DESCRIPTION">The p basket description.</param>
        /// <param name="p_BANNER_LIST">The p banner list.</param>
        /// <param name="p_DRIVER_LEVEL">The p driver level.</param>
        /// <param name="p_DRIVER_VALUE_LIST">The p driver value list.</param>
        /// <param name="p_OUTPUT_LEVEL">The p output level.</param>
        /// <param name="p_MIN_BASKET_SIZE">Size of the p minimum basket.</param>
        /// <param name="p_MAX_BASKET_SIZE">Size of the p maximum basket.</param>
        /// <param name="p_REWARD_TYPE">Type of the p reward.</param>
        /// <param name="p_CENSUS_FILTER">The p census filter.</param>
        /// <param name="p_All_UPC">if set to <c>true</c> [p all upc].</param>
        /// <returns>System.Int32.</returns>
        public int SP_UPDATE_MARKET_REQUEST(string p_EXEC_IDENTIFIER, string p_USERNAME, string p_VENDOR_LIST, string p_SUBMISSION_DATETIME, string p_FROM_DATE, string p_TO_DATE, string p_BASKET_DESCRIPTION, string p_BANNER_LIST, string p_DRIVER_LEVEL, string p_DRIVER_VALUE_LIST, string p_OUTPUT_LEVEL, string p_MIN_BASKET_SIZE, string p_MAX_BASKET_SIZE, string p_REWARD_TYPE, string p_CENSUS_FILTER, bool p_All_UPC)
        {
            int result = 0;
            try
            {
                result = SP_UPDATE_MARKET_REQUEST_UPDATE(p_EXEC_IDENTIFIER, p_USERNAME, p_VENDOR_LIST, p_SUBMISSION_DATETIME, p_FROM_DATE, p_TO_DATE, p_BASKET_DESCRIPTION, p_BANNER_LIST, p_DRIVER_LEVEL, p_DRIVER_VALUE_LIST, p_OUTPUT_LEVEL, p_MIN_BASKET_SIZE, p_MAX_BASKET_SIZE, p_REWARD_TYPE, p_CENSUS_FILTER, p_All_UPC);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_UPDATE_MARKET_REQUEST: "));
            }
            return result;
        }

        /// <summary>
        ///Update record in table MARKET_BASKET_REQUESTS.
        /// </summary>
        /// <param name="p_EXEC_IDENTIFIER">The p execute identifier.</param>
        /// <param name="p_USERNAME">The p username.</param>
        /// <param name="p_VENDOR_LIST">The p vendor list.</param>
        /// <param name="p_SUBMISSION_DATETIME">The p submission datetime.</param>
        /// <param name="p_FROM_DATE">The p from date.</param>
        /// <param name="p_TO_DATE">The p to date.</param>
        /// <param name="p_BASKET_DESCRIPTION">The p basket description.</param>
        /// <param name="p_BANNER_LIST">The p banner list.</param>
        /// <param name="p_DRIVER_LEVEL">The p driver level.</param>
        /// <param name="p_DRIVER_VALUE_LIST">The p driver value list.</param>
        /// <param name="p_OUTPUT_LEVEL">The p output level.</param>
        /// <param name="p_MIN_BASKET_SIZE">Size of the p minimum basket.</param>
        /// <param name="p_MAX_BASKET_SIZE">Size of the p maximum basket.</param>
        /// <param name="p_REWARD_TYPE">Type of the p reward.</param>
        /// <param name="p_CENSUS_FILTER">The p census filter.</param>
        /// <param name="p_All_UPC">if set to <c>true</c> [p all upc].</param>
        /// <returns>System.Int32.</returns>
        private int SP_UPDATE_MARKET_REQUEST_UPDATE(string p_EXEC_IDENTIFIER, string p_USERNAME, string p_VENDOR_LIST, string p_SUBMISSION_DATETIME, string p_FROM_DATE, string p_TO_DATE, string p_BASKET_DESCRIPTION, string p_BANNER_LIST, string p_DRIVER_LEVEL, string p_DRIVER_VALUE_LIST, string p_OUTPUT_LEVEL, string p_MIN_BASKET_SIZE, string p_MAX_BASKET_SIZE, string p_REWARD_TYPE, string p_CENSUS_FILTER, bool p_All_UPC)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string updateQuery = helper.GetResourceString(AppConstants.SP_UPDATE_MARKET_REQUEST_UPDATE);
                updateQuery = updateQuery
                    .Replace("@p_EXEC_IDENTIFIER", string.Format("'{0}'", p_EXEC_IDENTIFIER))
                    .Replace("@p_USERNAME", string.Format("'{0}'", p_USERNAME))
                    .Replace("@p_VENDOR_LIST", string.Format("'{0}'", p_VENDOR_LIST))
                    .Replace("@p_SUBMISSION_DATETIME", string.Format("'{0}'", p_SUBMISSION_DATETIME))
                    .Replace("@p_FROM_DATE", string.Format("'{0}'", p_FROM_DATE))
                    .Replace("@p_TO_DATE", string.Format("'{0}'", p_TO_DATE))
                    .Replace("@p_BASKET_DESCRIPTION", string.Format("'{0}'", p_BASKET_DESCRIPTION))
                    .Replace("@p_BANNER_LIST", string.Format("{0}", p_BANNER_LIST.Replace("','",",")))
                    .Replace("@p_DRIVER_LEVEL", string.Format("'{0}'", p_DRIVER_LEVEL))
                    .Replace("@p_DRIVER_VALUE_LIST", string.Format("'{0}'", p_DRIVER_VALUE_LIST))
                    .Replace("@p_OUTPUT_LEVEL", string.Format("'{0}'", p_OUTPUT_LEVEL))
                    .Replace("@p_MIN_BASKET_SIZE", string.Format("'{0}'", p_MIN_BASKET_SIZE))
                    .Replace("@p_MAX_BASKET_SIZE", string.Format("'{0}'", p_MAX_BASKET_SIZE))
                    .Replace("@p_REWARD_TYPE", string.Format("'{0}'", p_REWARD_TYPE))
                    .Replace("@p_CENSUS_FILTER", string.Format("'{0}'", p_CENSUS_FILTER))
                    .Replace("@p_All_UPC", string.Format("'{0}'", p_All_UPC));
                result = dataAccessProvider.Update(updateQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_UPDATE_MARKET_REQUEST_UPDATE: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion

        #region(SP_VALIDATEHIERARCHYNAME)
        /// <summary>
        /// Returns count of records from DIM_CUSTOM_HIERARCHY_MASTER/DIM_CUSTOM_STORES_MASTER based on condition using sub functions
        /// </summary>
        /// <param name="p_UserName">Name of the p user.</param>
        /// <param name="p_HierarchyName">Name of the p hierarchy.</param>
        /// <param name="p_HierarchyType">Type of the p hierarchy.</param>
        /// <param name="p_HierarchyId">The p hierarchy identifier.</param>
        /// <returns>System.Int64.</returns>
        public bool SP_VALIDATEHIERARCHYNAME(string p_UserName, string p_HierarchyName, string p_HierarchyType, string p_HierarchyId)
        {
            long v_ROW_COUNT = 0;
            bool Result = false;
            try
            {
                if (p_HierarchyType.ToLower() == "product")
                    v_ROW_COUNT = SP_VALIDATEHIERARCHYNAME_SELECTCOUNT1(p_UserName, p_HierarchyName, p_HierarchyId);
                if (p_HierarchyType.ToLower() == "store")
                    v_ROW_COUNT = SP_VALIDATEHIERARCHYNAME_SELECTCOUNT2(p_UserName, p_HierarchyName, p_HierarchyId);
                if (v_ROW_COUNT > 0)
                {
                    Result = true;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_VALIDATEHIERARCHYNAME: "));
            }
            return Result;
        }

        /// <summary>
        /// Select count of record from DIM_CUSTOM_HIERARCHY_MASTER.
        /// </summary>
        /// <param name="p_UserName">Name of the p user.</param>
        /// <param name="p_HierarchyName">Name of the p hierarchy.</param>
        /// <param name="p_HierarchyId">The p hierarchy identifier.</param>
        /// <returns>System.Int64.</returns>
        private long SP_VALIDATEHIERARCHYNAME_SELECTCOUNT1(string p_UserName, string p_HierarchyName, string p_HierarchyId)
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string selectQuery = helper.GetResourceString(AppConstants.SP_VALIDATEHIERARCHYNAME_SELECTCOUNT1);
                selectQuery = selectQuery.Replace("@p_UserName", string.Format("'{0}'", p_UserName)).Replace("@p_HierarchyName", string.Format("'{0}'", p_HierarchyName)).Replace("@p_HierarchyId", string.Format("'{0}'", p_HierarchyId));
                result = dataAccessProvider.SelectSingleItem(selectQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_VALIDATEHIERARCHYNAME_SELECTCOUNT1: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt64(result);
        }

        /// <summary>
        /// Select count of record from DIM_CUSTOM_STORES_MASTER.
        /// </summary>
        /// <param name="p_UserName">Name of the p user.</param>
        /// <param name="p_HierarchyName">Name of the p hierarchy.</param>
        /// <param name="p_HierarchyId">The p hierarchy identifier.</param>
        /// <returns>System.Int64.</returns>
        private long SP_VALIDATEHIERARCHYNAME_SELECTCOUNT2(string p_UserName, string p_HierarchyName, string p_HierarchyId)
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string selectQuery = helper.GetResourceString(AppConstants.SP_VALIDATEHIERARCHYNAME_SELECTCOUNT2);
                selectQuery = selectQuery.Replace("@p_UserName", string.Format("'{0}'", p_UserName)).Replace("@p_HierarchyName", string.Format("'{0}'", p_HierarchyName)).Replace("@p_HierarchyId", string.Format("'{0}'", p_HierarchyId));
                result = dataAccessProvider.SelectSingleItem(selectQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_VALIDATEHIERARCHYNAME_SELECTCOUNT2: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt64(result);
        }
        #endregion

        #region(SP_REPLICATE_CUSTOMHIERARCHY)
        /// <summary>
        /// Various operations of insert and delete using sub functionssssss
        /// </summary>
        /// <param name="p_Custom_Hierarchy_Id"></param>
        /// <param name="p_UserName"></param>
        /// <param name="p_HierarchyName"></param>
        /// <param name="p_HierarchyDesc"></param>
        /// <param name="p_ParentCustomHierarchy"></param>
        /// <param name="p_CustomLevel1"></param>
        /// <param name="p_CustomLevel2"></param>
        /// <param name="p_CustomLevel3"></param>
        /// <returns></returns>
        public long SP_REPLICATE_CUSTOMHIERARCHY(long p_Custom_Hierarchy_Id, string p_UserName, string p_HierarchyName, string p_HierarchyDesc, long p_ParentCustomHierarchy, string p_CustomLevel1, string p_CustomLevel2, string p_CustomLevel3)
        {
            long New_Custom_Hierarchy_Id = 0;
            try
            {
                if (p_Custom_Hierarchy_Id > 0)
                {
                    SP_REPLICATE_CUSTOMHIERARCHY_DELETE1(p_Custom_Hierarchy_Id);
                    SP_REPLICATE_CUSTOMHIERARCHY_DELETE2(p_Custom_Hierarchy_Id);
                    New_Custom_Hierarchy_Id = p_Custom_Hierarchy_Id;
                }
                else
                {
                    New_Custom_Hierarchy_Id = SP_REPLICATE_CUSTOMHIERARCHY_SELECT();
                }

                SP_REPLICATE_CUSTOMHIERARCHY_INSERT1(New_Custom_Hierarchy_Id, p_UserName, p_HierarchyName, p_HierarchyDesc, p_ParentCustomHierarchy);
                SP_REPLICATE_CUSTOMHIERARCHY_INSERT2(New_Custom_Hierarchy_Id, p_ParentCustomHierarchy, p_CustomLevel1, p_CustomLevel2, p_CustomLevel3);
            }
            catch (Exception ex)
            {
                New_Custom_Hierarchy_Id = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_REPLICATE_CUSTOMHIERARCHY: "));
            }
            return New_Custom_Hierarchy_Id;
        }

        /// <summary>
        /// Delete records from DIM_CUSTOM_HIERARCHY_PRODUCTS
        /// </summary>
        /// <param name="p_Custom_Hierarchy_Id"></param>
        /// <returns>System.Int32</returns>
        private int SP_REPLICATE_CUSTOMHIERARCHY_DELETE1(long p_Custom_Hierarchy_Id)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_REPLICATE_CUSTOMHIERARCHY_DELETE1);
                deleteQuery = deleteQuery.Replace("@p_Custom_Hierarchy_Id", string.Format("'{0}'", p_Custom_Hierarchy_Id));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_REPLICATE_CUSTOMHIERARCHY_DELETE1: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }

        /// <summary>
        /// Delete records from DIM_CUSTOM_HIERARCHY_MASTER
        /// </summary>
        /// <param name="p_Custom_Hierarchy_Id"></param>
        /// <returns>System.Int32</returns>
        private int SP_REPLICATE_CUSTOMHIERARCHY_DELETE2(long p_Custom_Hierarchy_Id)
        {
            int result = 1;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.SP_REPLICATE_CUSTOMHIERARCHY_DELETE2);
                deleteQuery = deleteQuery.Replace("@p_Custom_Hierarchy_Id", string.Format("'{0}'", p_Custom_Hierarchy_Id));
                result = dataAccessProvider.Delete(deleteQuery);

                //Whether deleted any record or not, we need to return result as 1 always. 0 only in case of exception
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_REPLICATE_CUSTOMHIERARCHY_DELETE2: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }

        /// <summary>
        /// Returns next sequence
        /// </summary>
        /// <returns>System.Int64</returns>
        private long SP_REPLICATE_CUSTOMHIERARCHY_SELECT()
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string selectQuery = helper.GetResourceString(AppConstants.SP_REPLICATE_CUSTOMHIERARCHY_SELECT);
                result = dataAccessProvider.SelectSingleItem(selectQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_REPLICATE_CUSTOMHIERARCHY_SELECT: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt64(result);
        }

        /// <summary>
        /// Insert records into DIM_CUSTOM_HIERARCHY_MASTER
        /// </summary>
        /// <param name="New_Custom_Hierarchy_Id"></param>
        /// <param name="p_UserName"></param>
        /// <param name="p_HierarchyName"></param>
        /// <param name="p_HierarchyDesc"></param>
        /// <param name="p_ParentCustomHierarchy"></param>
        /// <returns>System.Int32</returns>
        private int SP_REPLICATE_CUSTOMHIERARCHY_INSERT1(long New_Custom_Hierarchy_Id, string p_UserName, string p_HierarchyName, string p_HierarchyDesc, long p_ParentCustomHierarchy)
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_REPLICATE_CUSTOMHIERARCHY_INSERT1);
                insertQuery = insertQuery.Replace("@New_Custom_Hierarchy_Id", New_Custom_Hierarchy_Id.ToString()).Replace("@p_UserName", string.Format("'{0}'", p_UserName)).Replace("@p_HierarchyName", string.Format("'{0}'", p_HierarchyName)).Replace("@p_HierarchyDesc", string.Format("'{0}'", p_HierarchyDesc)).Replace("@p_ParentCustomHierarchy", p_ParentCustomHierarchy.ToString());
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_REPLICATE_CUSTOMHIERARCHY_INSERT1: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }

        /// <summary>
        /// Insert records into DIM_CUSTOM_HIERARCHY_PRODUCTS after selecting records from PRODUCT_FULL_DESC
        /// </summary>
        /// <param name="New_Custom_Hierarchy_Id"></param>
        /// <param name="p_ParentCustomHierarchy"></param>
        /// <param name="p_CustomLevel1"></param>
        /// <param name="p_CustomLevel2"></param>
        /// <param name="p_CustomLevel3"></param>
        /// <returns>System.Int32</returns>
        private int SP_REPLICATE_CUSTOMHIERARCHY_INSERT2(long New_Custom_Hierarchy_Id, long p_ParentCustomHierarchy, string p_CustomLevel1, string p_CustomLevel2, string p_CustomLevel3)
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_REPLICATE_CUSTOMHIERARCHY_INSERT2);
                insertQuery = insertQuery.Replace("@New_Custom_Hierarchy_Id", New_Custom_Hierarchy_Id.ToString()).Replace("@p_ParentCustomHierarchy", p_ParentCustomHierarchy.ToString()).Replace("@p_CustomLevel1", string.Format("'{0}'", p_CustomLevel1)).Replace("@p_CustomLevel2", string.Format("'{0}'", p_CustomLevel2)).Replace("@p_CustomLevel3", string.Format("'{0}'", p_CustomLevel3));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_REPLICATE_CUSTOMHIERARCHY_INSERT2: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }
        #endregion

        #region(SP_INSERT_CUSTOMPROMOTION)
        /// <summary>
        /// Insert and Update records using sub functions
        /// </summary>
        /// <param name="p_UserName"></param>
        /// <param name="p_promoid"></param>
        /// <param name="p_Hierarchyid"></param>
        /// <param name="p_Promoname"></param>
        /// <param name="p_descr"></param>
        /// <param name="p_startdate"></param>
        /// <param name="p_enddate"></param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_CUSTOMPROMOTION(string p_UserName, long p_promoid, int p_Hierarchyid, string p_Promoname, string p_descr, DateTime p_startdate, DateTime p_enddate)
        {
            long Seq = 0;
            int Result = 0;
            try
            {
                if (p_promoid == 0)
                {
                    Seq = SP_INSERT_CUSTOMPROMOTION_SELECT();
                    Result = SP_INSERT_CUSTOMPROMOTION_INSERT(p_UserName, Seq, p_Hierarchyid, p_Promoname, p_descr, p_startdate, p_enddate);
                }
                else
                {
                    Result = SP_INSERT_CUSTOMPROMOTION_UPDATE(p_promoid, p_Hierarchyid, p_Promoname, p_descr, p_startdate, p_enddate);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMPROMOTION: "));
            }
            return Result;
        }

        /// <summary>
        /// Returns next sequence
        /// </summary>
        /// <returns>System.Int64.</returns>
        private long SP_INSERT_CUSTOMPROMOTION_SELECT()
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string selectQuery = helper.GetResourceString(AppConstants.SP_INSERT_CUSTOMPROMOTION_SELECT);
                result = dataAccessProvider.SelectSingleItem(selectQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMPROMOTION_SELECT: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt64(result);
        }
        /// <summary>
        /// Insert records into DIM_CUSTOM_PROMOTIONS
        /// </summary>
        /// <param name="p_UserName"></param>
        /// <param name="p_seq"></param>
        /// <param name="p_Hierarchyid"></param>
        /// <param name="p_Promoname"></param>
        /// <param name="p_descr"></param>
        /// <param name="p_startdate"></param>
        /// <param name="p_enddate"></param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_CUSTOMPROMOTION_INSERT(string p_UserName, long p_seq, int p_Hierarchyid, string p_Promoname, string p_descr, DateTime p_startdate, DateTime p_enddate)
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_CUSTOMPROMOTION_INSERT);
                insertQuery = insertQuery.Replace("@p_UserName", string.Format("'{0}'", p_UserName)).Replace("@p_Seq", p_seq.ToString()).Replace("@p_Hierarchyid", p_Hierarchyid.ToString()).Replace("@p_Promoname", string.Format("'{0}'", p_Promoname)).Replace("@p_descr", string.Format("'{0}'", p_descr)).Replace("@p_startdate", string.Format("'{0}'", p_startdate)).Replace("@p_enddate", string.Format("'{0}'", p_enddate));
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMPROMOTION_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }

        /// <summary>
        /// Update table DIM_CUSTOM_PROMOTIONS
        /// </summary>
        /// <param name="p_promoid"></param>
        /// <param name="p_Hierarchyid"></param>
        /// <param name="p_Promoname"></param>
        /// <param name="p_descr"></param>
        /// <param name="p_startdate"></param>
        /// <param name="p_enddate"></param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_CUSTOMPROMOTION_UPDATE(long p_promoid, int p_Hierarchyid, string p_Promoname, string p_descr, DateTime p_startdate, DateTime p_enddate)
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string updateQuery = helper.GetResourceString(AppConstants.SP_INSERT_CUSTOMPROMOTION_UPDATE);
                updateQuery = updateQuery.Replace("@p_promoid", p_promoid.ToString()).Replace("@p_Hierarchyid", p_Hierarchyid.ToString()).Replace("@p_Promoname", string.Format("'{0}'", p_Promoname)).Replace("@p_descr", string.Format("'{0}'", p_descr)).Replace("@p_startdate", string.Format("'{0}'", p_startdate)).Replace("@p_enddate", string.Format("'{0}'", p_enddate));
                result = dataAccessProvider.Update(updateQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMPROMOTION_UPDATE: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }
        #endregion

        #region(SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE)
        /// <summary>
        /// Insert and Updates records using subfunctions
        /// </summary>
        /// <param name="p_UserName"></param>
        /// <param name="p_promoid"></param>
        /// <param name="p_Hierarchyid"></param>
        /// <param name="p_Promoname"></param>
        /// <param name="p_descr"></param>
        /// <param name="p_startdate"></param>
        /// <param name="p_enddate"></param>
        /// <param name="p_ChType"></param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE(string p_UserName, long p_promoid, int p_Hierarchyid, string p_Promoname, string p_descr, DateTime p_startdate, DateTime p_enddate, int p_ChType)
        {
            long Seq = 0;
            int Result = 0;
            try
            {
                if (p_promoid == 0)
                {
                    Seq = SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_SELECT();
                    Result = SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_INSERT(p_UserName, Seq, p_Hierarchyid, p_Promoname, p_descr, p_startdate, p_enddate, p_ChType);

                }
                else
                {
                    Result = SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_UPDATE(p_promoid, p_Hierarchyid, p_Promoname, p_descr, p_startdate, p_enddate, p_ChType);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE: "));
            }
            return Result;
        }
        /// <summary>
        /// Returns next sequence
        /// </summary>
        /// <returns></returns>
        private long SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_SELECT()
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string selectQuery = helper.GetResourceString(AppConstants.SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_SELECT);
                result = dataAccessProvider.SelectSingleItem(selectQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_SELECT: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt64(result);
        }
        /// <summary>
        /// Insert records into DIM_CUSTOM_PROMOTIONS
        /// </summary>
        /// <param name="p_UserName"></param>
        /// <param name="p_seq"></param>
        /// <param name="p_Hierarchyid"></param>
        /// <param name="p_Promoname"></param>
        /// <param name="p_descr"></param>
        /// <param name="p_startdate"></param>
        /// <param name="p_enddate"></param>
        /// <param name="p_ChType"></param>
        /// <returns>System.Int64</returns>
        private int SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_INSERT(string p_UserName, long p_seq, int p_Hierarchyid, string p_Promoname, string p_descr, DateTime p_startdate, DateTime p_enddate, int p_ChType)
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_INSERT);
                insertQuery = insertQuery.Replace("@p_UserName", string.Format("'{0}'", p_UserName)).Replace("@p_Seq", p_seq.ToString()).Replace("@p_Hierarchyid", p_Hierarchyid.ToString()).Replace("@p_Promoname", string.Format("'{0}'", p_Promoname)).Replace("@p_descr", string.Format("'{0}'", p_descr)).Replace("@p_startdate", string.Format("'{0}'", p_startdate)).Replace("@p_enddate", string.Format("'{0}'", p_enddate)).Replace("@p_ChType", p_ChType.ToString());
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_INSERT: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }
        /// <summary>
        /// Update table DIM_CUSTOM_PROMOTIONS
        /// </summary>
        /// <param name="p_promoid"></param>
        /// <param name="p_Hierarchyid"></param>
        /// <param name="p_Promoname"></param>
        /// <param name="p_descr"></param>
        /// <param name="p_startdate"></param>
        /// <param name="p_enddate"></param>
        /// <param name="p_ChType"></param>
        /// <returns>System.Int64</returns>
        private int SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_UPDATE(long p_promoid, int p_Hierarchyid, string p_Promoname, string p_descr, DateTime p_startdate, DateTime p_enddate, int p_ChType)
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string updateQuery = helper.GetResourceString(AppConstants.SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_UPDATE);
                updateQuery = updateQuery.Replace("@p_promoid", p_promoid.ToString()).Replace("@p_Hierarchyid", p_Hierarchyid.ToString()).Replace("@p_Promoname", string.Format("'{0}'", p_Promoname)).Replace("@p_descr", string.Format("'{0}'", p_descr)).Replace("@p_startdate", string.Format("'{0}'", p_startdate)).Replace("@p_enddate", string.Format("'{0}'", p_enddate)).Replace("@p_ChType", p_ChType.ToString());
                result = dataAccessProvider.Update(updateQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_UPDATE: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }
        #endregion


        #region(P_CUSTOM_HIERARCHY_EXTENDED_UPDATE)
        /// <summary>
        /// P_CUSTOM_HIERARCHY_EXTENDED_UPDATEle.
        /// </summary>
        /// <param name="p_CUSTOM_HIERARCHY_ID">Name of the p custom hierarchy id.</param>
        /// <returns>System.Int32.</returns>
        public int P_CUSTOM_HIERARCHY_EXTENDED_UPDATE(string p_CUSTOM_HIERARCHY_ID)
        {

            try
            {
                SP_INSERT_LOG_CUSTOMHIERARCHY_EXTENDED(p_CUSTOM_HIERARCHY_ID, "Processing ended within SP", "", "");
                //DELETE_FROM_DIM_CUSTOM_HIERARCHY_EXTENDED
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.DELETE_FROM_DIM_CUSTOM_HIERARCHY_EXTENDED).Replace("@p_CUSTOM_HIERARCHY_ID", p_CUSTOM_HIERARCHY_ID.ToString());
                dataAccessProvider.Delete(deleteQuery);

                //SP_INSERT_DIM_CUSTOM_HIERARCHY_EXTENDED
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string Query = helper.GetResourceString(AppConstants.SP_INSERT_DIM_CUSTOM_HIERARCHY_EXTENDED).Replace("@p_CUSTOM_HIERARCHY_ID", p_CUSTOM_HIERARCHY_ID.ToString()); ;
                dataAccessProvider.Insert(Query);

                //DELETE_FROM_DIM_CUSTOM_HIERARCHIES_SHARED
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery2 = helper.GetResourceString(AppConstants.DELETE_FROM_DIM_CUSTOM_HIERARCHIES_SHARED).Replace("@p_CUSTOM_HIERARCHY_ID", p_CUSTOM_HIERARCHY_ID.ToString());
                dataAccessProvider.Delete(deleteQuery2);

                SP_CUSTOM_HIERARCHY_REPLACE_BLACKLIST_CHARACTERS();

                SP_INSERT_LOG_CUSTOMHIERARCHY_EXTENDED(p_CUSTOM_HIERARCHY_ID, "Processing ended within SP", "", "");

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "P_CUSTOM_HIERARCHY_EXTENDED_UPDATE: "));
            }
            return 1;
        }

        /// <summary>
        /// insert log for custom hierarchy
        /// <param name="p_CustomHierarchyId">custom hierarchy id</param>
        /// <param name="p_Message">The p msg.</param>
        /// <param name="p_Username">The p user name.</param>
        /// <param name="p_Pagename">The p page name.</param>
        /// </summary>
        private int SP_INSERT_LOG_CUSTOMHIERARCHY_EXTENDED(string p_CustomHierarchyId, string p_Message, string p_Username, string p_Pagename)
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());

                string LogIDQuery = helper.GetResourceString(AppConstants.SP_GET_LOGID_SEQ_CUSTOMHIERARCHIES_EXTENDED_LOG);
                int LogID = Convert.ToInt32(dataAccessProvider.SelectSingleItem(LogIDQuery));

                string selectQuery = helper.GetResourceString(AppConstants.SP_INSERT_LOG_CUSTOMHIERARCHY_EXTENDED);
                selectQuery = selectQuery.Replace("@LogID", LogID.ToString()).Replace("@p_CustomHierarchyId", p_CustomHierarchyId.ToString()).Replace("@p_Message", "'" + p_Message.ToString() + "'").Replace("@p_Username", "'" + p_Username.ToString() + "'").Replace("@p_Pagename", "'" + p_Pagename.ToString() + "'");
                result = dataAccessProvider.Insert(selectQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_LOG_CUSTOMHIERARCHY_EXTENDED: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }

        #endregion

        #region  P_CUSTOM_STORE_EXTENDED_UPDATE   
        /// <summary>
        /// P_CUSTOM_STORE_EXTENDED_UPDATE.
        /// </summary>
        /// <param name="p_CUSTOM_STORE_GROUP_ID">Name of the p custom store group id.</param>
        /// <returns>System.Int32.</returns>
        public int P_CUSTOM_STORE_EXTENDED_UPDATE(string p_CUSTOM_STORE_GROUP_ID)
        {

            try
            {
                SP_INSERT_LOG_CUSTOMSTORE_EXTENDED(p_CUSTOM_STORE_GROUP_ID, "Processing ended within SP", "", "");

                //DELETE_FROM_DIM_CUSTOM_STORES_SHARED
                DELETE_FROM_DIM_CUSTOM_HIERARCHIES_SHARED(p_CUSTOM_STORE_GROUP_ID);

                //DELETE_FROM_DIM_CUSTOM_STORES_EXTENDED
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQueryCustomStore = helper.GetResourceString(AppConstants.DELETE_FROM_DIM_CUSTOM_STORES_EXTENDED).Replace("@p_CUSTOM_STORE_GROUP_ID", p_CUSTOM_STORE_GROUP_ID.ToString());
                dataAccessProvider.Delete(deleteQueryCustomStore);

                //SP_INSERT_DIM_CUSTOM_HIERARCHY_EXTENDED
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string Query = helper.GetResourceString(AppConstants.SP_INSERT_DIM_CUSTOM_STORES_EXTENDED).Replace("@p_CUSTOM_STORE_GROUP_ID", p_CUSTOM_STORE_GROUP_ID.ToString()); ;
                dataAccessProvider.Insert(Query);

                //call  SP_CUSTOM_STORE_REPLACE_BLACKLIST_CHARACTERS
                SP_CUSTOM_STORE_REPLACE_BLACKLIST_CHARACTERS();

                //DELETE_FROM_DIM_CUSTOM_HIERARCHIES_SHARED
                DELETE_FROM_DIM_CUSTOM_HIERARCHIES_SHARED(p_CUSTOM_STORE_GROUP_ID);

                //call  SP_CUSTOM_STORE_REPLACE_BLACKLIST_CHARACTERS()
                SP_CUSTOM_STORE_REPLACE_BLACKLIST_CHARACTERS();

                SP_INSERT_LOG_CUSTOMSTORE_EXTENDED(p_CUSTOM_STORE_GROUP_ID, "Processing ended within SP", "", "");

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "P_CUSTOM_STORE_EXTENDED_UPDATE: "));
            }
            return 1;
        }
        /// <summary>
        /// DELETE_FROM_DIM_CUSTOM_HIERARCHIES_SHARED
        /// <param name="p_CustomHierarchyId">custom hierarchy id</param>
        /// </summary>
        private int DELETE_FROM_DIM_CUSTOM_HIERARCHIES_SHARED(string p_CUSTOM_STORE_GROUP_ID)
        {
            dynamic result = 0;
            try
            {
                //DELETE_FROM_DIM_CUSTOM_HIERARCHIES_SHARED
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string deleteQuery = helper.GetResourceString(AppConstants.DELETE_FROM_DIM_CUSTOM_HIERARCHIES_SHARED).Replace("@p_CUSTOM_STORE_GROUP_ID", p_CUSTOM_STORE_GROUP_ID.ToString());
                result = dataAccessProvider.Delete(deleteQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "DELETE_FROM_DIM_CUSTOM_HIERARCHIES_SHARED: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }

        /// <summary>
        /// insert log for custom hierarchy
        /// <param name="p_CustomStoreId">custom store group id</param>
        /// <param name="p_Message">The p msg.</param>
        /// <param name="p_Username">The p user name.</param>
        /// <param name="p_Pagename">The p page name.</param>
        /// </summary>
        private int SP_INSERT_LOG_CUSTOMSTORE_EXTENDED(string p_CustomStoreId, string p_Message, string p_Username, string p_Pagename)
        {
            dynamic result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());

                string LogIDQuery = helper.GetResourceString(AppConstants.SP_GET_LOGID_SEQ_CUSTOMSTORES_EXTENDED_LOG);
                int LogID = Convert.ToInt32(dataAccessProvider.SelectSingleItem(LogIDQuery));

                string selectQuery = helper.GetResourceString(AppConstants.SP_INSERT_LOG_CUSTOMSTORE_EXTENDED);
                selectQuery = selectQuery.Replace("@LogID", LogID.ToString()).Replace("@p_CustomStoreId,", p_CustomStoreId.ToString()).Replace("@p_Message", "'" + p_Message.ToString() + "'").Replace("@p_Username", "'" + p_Username.ToString() + "'").Replace("@p_Pagename", "'" + p_Pagename.ToString() + "'");
                result = dataAccessProvider.Insert(selectQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_INSERT_LOG_CUSTOMSTORE_EXTENDED: "));
            }
            dataAccessProvider.CloseConnection();
            return Convert.ToInt32(result);
        }
        #endregion


        #region(SP_INSERT_CUSTOMSTOREGROUP)
        /// <summary>
        /// Insert into DIM_Custom_Stores  using sub function
        /// </summary>
        /// <param name="p_UserName">The p username.</param>
        /// <param name="p_StoreGroupName">The p Store Group Name.</param>
        /// <param name="p_StoreGroupDesc">The P Store Group Desc</param>
        /// <param name="p_CustomLevel1">The p custom Level 1.</param>
        /// <param name="p_CustomLevel2">The p custom Level 2.</param>
        /// <param name="p_CustomLevel3">The p custom Level 3.</param>
        /// <param name="p_StoreNo">The p store number.</param>
        /// <param name="p_CustomStoreGroupId">The p custom store groupId.</param>
        /// <param name="p_DateTimeCreated">The p created date.</param>
        /// <param name="StoreID">The storeID.</param>
        /// <param name="StoreName">The StoreName.</param>
        /// <param name="StoreLegacyNumber">The StoreLegacyNumber.</param>
        /// <returns>System.Int32.</returns>
        public int SP_INSERT_CUSTOMSTOREGROUP_NEW(string p_UserName,string p_StoreGroupName,string p_StoreGroupDesc,string p_CustomLevel1,string p_CustomLevel2,string p_CustomLevel3,string p_StoreNo, Int64 p_CustomStoreGroupId,DateTime p_DateTimeCreated)
        {
            int result = 0;
            Int64 StoreID, StoreLegacyNumber;
            string StoreName = string.Empty;
            try
            {
                DataTable DT = DataManager.ExecuteQueryForNetezza(helper.GetResourceString(AppConstants.GET_ALL_FROM_STORES).Replace("@StoreNumber", p_StoreNo));
                StoreID = Convert.ToInt64(DT.Rows[0]["STORE_ID"]);
                StoreName = Convert.ToString(DT.Rows[0]["STORE_NAME"]);
                StoreLegacyNumber = Convert.ToInt64(DT.Rows[0]["STORE_LEGACY_NUMBER"]);
                result = SP_INSERT_CUSTOMSTOREGROUP(p_UserName, p_StoreGroupName, p_StoreGroupDesc, p_CustomLevel1, p_CustomLevel2, p_CustomLevel3, p_StoreNo,  p_CustomStoreGroupId,  p_DateTimeCreated, StoreID, StoreName, StoreLegacyNumber);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_Insert_CustomStoreGroup_NEW: "));
            }
            return result;
        }

        /// <summary>
        /// Insert into DIM_Custom_Stores  using sub function
        /// </summary>
        /// <param name="p_UserName">The p username.</param>
        /// <param name="p_StoreGroupName">The p Store Group Name.</param>
        /// <param name="p_StoreGroupDesc">The P Store Group Desc</param>
        /// <param name="p_CustomLevel1">The p custom Level 1.</param>
        /// <param name="p_CustomLevel2">The p custom Level 2.</param>
        /// <param name="p_CustomLevel3">The p custom Level 3.</param>
        /// <param name="p_StoreNo">The p store number.</param>
        /// <param name="p_CustomStoreGroupId">The p custom store groupId.</param>
        /// <param name="p_DateTimeCreated">The p created date.</param>
        /// <param name="StoreID">The storeID.</param>
        /// <param name="StoreName">The StoreName.</param>
        /// <param name="StoreLegacyNumber">The StoreLegacyNumber.</param>
        /// <returns>System.Int32.</returns>
        private int SP_INSERT_CUSTOMSTOREGROUP(string p_UserName, string p_StoreGroupName, string p_StoreGroupDesc, string p_CustomLevel1, string p_CustomLevel2, string p_CustomLevel3, string p_StoreNo, Int64 p_CustomStoreGroupId, DateTime p_DateTimeCreated,Int64 StoreID,string StoreName,Int64 StoreLegacyNumber)
        {
            int result = 0;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string insertQuery = helper.GetResourceString(AppConstants.SP_INSERT_INTO_CUSTOM_STORES);
                insertQuery = insertQuery.Replace("@p_UserName", p_UserName).Replace("@p_StoreGroupName", p_StoreGroupName).Replace("@p_StoreGroupDesc", p_StoreGroupDesc).Replace("@p_CustomLevel1", p_CustomLevel1).Replace("@p_CustomLevel2", p_CustomLevel2).Replace("@p_CustomLevel3", p_CustomLevel3).Replace("@p_StoreNo", p_StoreNo.ToString()).Replace("@p_CustomStoreGroupId", p_CustomStoreGroupId.ToString()).Replace("@p_DateTimeCreated", p_DateTimeCreated.ToString()).Replace("@StoreID", StoreID.ToString()).Replace("@StoreName", StoreName).Replace("@StoreLegacyNumber", StoreLegacyNumber.ToString());
                result = dataAccessProvider.Insert(insertQuery);
            }
            catch (Exception ex)
            {
                result = 0;
                helper.WriteLog(string.Format("{0}" + ex.Message, "SP_Insert_CustomStoreGroup: "));
            }
            dataAccessProvider.CloseConnection();
            return result;
        }
        #endregion
    }

    /// <summary>
    /// PARTIAL CLASS USED FOR GENERIC ASPX PAGE
    /// </summary>
    public partial class DataLayer
    {
        #region Private Page Level Properties
        private DataTable dtFilters;
        private DataTable dtFiscalYearWeekDetails;
        private DataTable dtHolidayAndYearDetails;
        private DataTable dtBannerAndSubBannerDetails;
        private DataTable dtProductDetails;
        private DataTable dtHierarchyDetails;
        private DataTable dtVendors;
        private DataTable dtDates;
        private DataTable dtMenu;
        public static String strConnNetezzaEDWGrocery = ConfigurationManager.ConnectionStrings["DataTrue_EDWGrocery_Production"].ConnectionString.ToString();
        public static String PortalName = ConfigurationManager.AppSettings["ApplicationGroup"].ToString();
        #endregion


        /// <summary>
        /// Gets the filters data table.
        /// </summary>
        /// <param name="reportid">The reportid.</param>
        /// <returns>DataTable.</returns>
        public DataTable GetFiltersDataTable(int reportid)
        {
            dtFilters = new DataTable();
            try
            {

                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = helper.GetResourceString(AppConstants.GENERIC_GET_FILTER_DETAILS_BY_REPORTID).Replace("@ReportId", string.Format("{0}", reportid));

                dtFilters = dataAccessProvider.Select(strQuery);
                // return Utility.ToDataTable(result, "dtFilters");
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "GetFiltersDataTable:"));
            }
            dataAccessProvider.CloseConnection();
            return dtFilters;
        }

        /// <summary>
        /// Gets the Venoder Number Exist
        /// </summary>
        /// <param name="reportid">The VendorNumber.</param>
        /// <returns>bool.</returns>
        public bool VenoderNumberExist(string VendorNumber)
        {
            bool VendorExist = false;
            dtVendors = new DataTable();
            try
            {

                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = helper.GetResourceString(AppConstants.GENERIC_GET_VENDOR_EXIST).Replace("@VendorNo", string.Format("{0}", VendorNumber));
                dtVendors = dataAccessProvider.Select(strQuery);
                if (dtVendors != null && dtVendors.Rows.Count > 0)
                {
                    VendorExist = true;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "GetFiltersDataTable:"));
            }
            dataAccessProvider.CloseConnection();
            return VendorExist;
        }
        #region dropdownList Data Source

        /// <summary>
        /// Gets the fiscal year week data table.
        /// </summary>
        /// <returns>DataTable.</returns>
        public DataTable GetFiscalYearWeekDetails()
        {
            DateTime StartOfWeek = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek);
            DateTime LastSaturday = StartOfWeek.AddDays(-1);
            dtFiscalYearWeekDetails = new DataTable();
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());

                string select = helper.GetResourceString(AppConstants.GENERIC_GET_FISCAL_YEAR_WEEK).Replace("@LASTSATURDAY", "'" + LastSaturday.ToString("yyyy-MM-dd") + "'");

                dtFiscalYearWeekDetails = dataAccessProvider.Select(select);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "GetFiscalYearWeekDetails: "));
            }
            dataAccessProvider.CloseConnection();
            return dtFiscalYearWeekDetails;
        }

        /// <summary>
        /// Gets the Holiday and year data table.
        /// </summary>
        /// <returns>DataTable.</returns>
        public DataTable GetHolidayAndYearDetails()
        {
            dtHolidayAndYearDetails = new DataTable();
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());

                string select = helper.GetResourceString(AppConstants.GENERIC_GET_HOLIDAY_AND_YEAR);

                dtHolidayAndYearDetails = dataAccessProvider.Select(select);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "GetHolidayAndYearDetails: "));
            }
            dataAccessProvider.CloseConnection();
            return dtHolidayAndYearDetails;
        }

        /// <summary>
        /// Gets the banner and sub banner details.
        /// </summary>
        /// <returns>DataTable.</returns>
        public DataTable GetBannerAndSubBannerDetails()
        {
            dtBannerAndSubBannerDetails = new DataTable();
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());

                string select = helper.GetResourceString(AppConstants.GENERIC_GET_BANNER_DETAILS);

                dtBannerAndSubBannerDetails = dataAccessProvider.Select(select);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "GetBannerAndSubBannerDetails: "));
            }
            dataAccessProvider.CloseConnection();
            return dtBannerAndSubBannerDetails;
        }


        public DataTable GetProductDetailsCorporateHierarchy(int ReportId, string username, string profileName = null, string vendornumber = null)
        {
            dtProductDetails = new DataTable();
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = string.Empty;
                //for Report Add hoc fresh support 
                if (ReportId != 1322)
                {
                    if (DataManager.IsCorporateUser(username))
                    {
                        if (!string.IsNullOrEmpty(vendornumber))
                        {
                            strQuery = helper.GetResourceString(AppConstants.GENERIC_GET_PRODUCT_DETAILS_CORPORATEUSER).Replace("@VendorNumber", vendornumber);
                        }
                        else
                        {
                            strQuery = helper.GetResourceString(AppConstants.GENERIC_GET_PRODUCT_DETAILS_CORPORATEUSER).Replace("@VendorNumber", "v.vendor_number");

                        }

                    }
                    else
                    {

                        if (!string.IsNullOrEmpty(vendornumber))
                        {
                            if (vendornumber.ToString().ToLower() != "Select Vendor (If Relevant)".ToLower())
                                strQuery = helper.GetResourceString(AppConstants.GENERIC_GET_PRODUCT_DETAILS_FILTEREDUSER).Replace("@VENDORNUMBER", vendornumber).Replace("@CURRENTUSER", "'" + username + "'").Replace("@CURRENTPROFILE", "'" + profileName + "'");
                            else
                            {
                                strQuery = helper.GetResourceString(AppConstants.GENERIC_GET_PRODUCT_DETAILS_FILTEREDUSER).Replace("@VENDORNUMBER", "su.vendor_num").Replace("@CURRENTUSER", "'" + username + "'").Replace("@CURRENTPROFILE", "'" + profileName + "'");
                            }
                        }
                        else
                        {
                            strQuery = helper.GetResourceString(AppConstants.GENERIC_GET_PRODUCT_DETAILS_FILTEREDUSER).Replace("@VENDORNUMBER", "su.vendor_num").Replace("@CURRENTUSER", "'" + username + "'").Replace("@CURRENTPROFILE", "'" + profileName + "'");

                        }
                    }
                }
                else
                {
                    strQuery = helper.GetResourceString(AppConstants.GENERIC_GET_FRESH_SUPPORT_PRODUCT_HIERARCHY_DETAILS);
                }


                dtProductDetails = dataAccessProvider.Select(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "GetProductDetials:"));
            }
            dataAccessProvider.CloseConnection();
            return dtProductDetails;
        }

        /// <summary>
        /// Gets the product hierarchy details.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="userType">Type of the user.</param>
        /// <returns>DataTable.</returns>
        public DataTable GetProductHierarchyDetails(string userName)
        {
            dtHierarchyDetails = new DataTable();
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = null;
                if (DataManager.IsCorporateUser(userName))
                {
                    strQuery = helper.GetResourceString(AppConstants.GENERIC_GET_CUSTOM_PRODUCT_HIERARCHY_DETAILS_CORPORATE_USER).Replace("@USERNAME", "'" + userName + "'");
                }
                else
                {
                    strQuery = helper.GetResourceString(AppConstants.GENERIC_GET_CUSTOM_PRODUCT_HIERARCHY_DETAILS_UAM_USER).Replace("@USERNAME", "'" + userName + "'");
                }
                dtHierarchyDetails = dataAccessProvider.Select(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "GetProductHierarchyDetails:"));
            }
            dataAccessProvider.CloseConnection();
            return dtHierarchyDetails;
        }

        /// <summary>
        /// Gets the location hierarchy details.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="userType">Type of the user.</param>
        /// <returns>DataTable.</returns>
        public DataTable GetLocationHierarchyDetails(string userName)
        {
            dtHierarchyDetails = new DataTable();
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = null;
                if (DataManager.IsCorporateUser(userName))
                {
                    strQuery = helper.GetResourceString(AppConstants.GENERIC_GET_CUSTOM_LOCATION_HIERARCHY_DETAILS_CORPORATE_USER).Replace("@USERNAME", "'" + userName + "'");
                }
                else
                {
                    strQuery = helper.GetResourceString(AppConstants.GENERIC_GET_CUSTOM_LOCATION_HIERARCHY_DETAILS_UAM_USER).Replace("@USERNAME", "'" + userName + "'");
                }
                dtHierarchyDetails = dataAccessProvider.Select(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "GetLocationHierarchyDetails:"));
            }
            dataAccessProvider.CloseConnection();
            return dtHierarchyDetails;
        }
        #endregion

        /// <summary>
        /// Get hidden field values like Param_Start_Date
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="StartOrEnd"></param>
        /// <returns></returns>
        public string GetParamDate(string StartDate, string StartOrEnd = "Start")
        {
            string ParamDate = "";
            try
            {
                dtDates = new DataTable();
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = helper.GetResourceString(AppConstants.GETPARAMDATE).Replace("@StartDate", string.Format("'{0}'", StartDate));

                dtDates = dataAccessProvider.Select(strQuery);
                foreach (DataRow dr in dtDates.Rows)
                {
                    if (StartOrEnd == "Start")
                        ParamDate = Convert.ToDateTime(dr[0].ToString()).ToString("yyyy-MM-dd");
                    else
                        ParamDate = Convert.ToDateTime(dr[1].ToString()).ToString("yyyy-MM-dd");
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "GetParamDate:"));
            }
            return ParamDate;
        }

        public string GetParamLYDate(string StartDate, string StartOrEnd = "Start")
        {
            string ParamDate = "";
            try
            {
                dtDates = new DataTable();
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = helper.GetResourceString(AppConstants.GETPARAMLYDATE).Replace("@StartDate", string.Format("'{0}'", StartDate));
                dtDates = dataAccessProvider.Select(strQuery);
                foreach (DataRow dr in dtDates.Rows)
                {
                    if (StartOrEnd == "Start")
                        ParamDate = Convert.ToDateTime(dr[0].ToString()).ToString("yyyy-MM-dd");
                    else
                        ParamDate = Convert.ToDateTime(dr[1].ToString()).ToString("yyyy-MM-dd");
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > GetParamLYDate:"));
            }
            return ParamDate;
        }
        /// <summary>
        /// Get next sunday date
        /// </summary>
        /// <returns>Datetime.</returns>
        public DateTime GetSunday(DateTime Launchdate)
        {
            int daysUntilSunday = ((int)DayOfWeek.Sunday - (int)Launchdate.DayOfWeek + 7) % 7;
            DateTime nextSunday = Launchdate.AddDays(daysUntilSunday);
            return nextSunday;
        }
        /// <summary>
        /// Get Holiday start date
        /// </summary>
        /// <returns>DataTable.</returns>
        public DataTable GetHolidayStartDates(string ddlHoliday, string ddlPreWeek, string ddlFiscalYear, String TableName = "Dim_Calendar_Holidays_Core")
        {
            DataTable dt = new DataTable();
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = helper.GetResourceString(AppConstants.SELECT_HOLIDAYSTARTDATE).Replace("@TableName", string.Format("{0}", TableName)).Replace("@ddlHoliday", string.Format("'{0}'", ddlHoliday)).Replace("@ddlFiscalYear", string.Format("'{0}'", ddlFiscalYear)).Replace("@ddlPreWeek", string.Format("{0}", ddlPreWeek));
                dt = dataAccessProvider.Select(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > GetHolidayStartDates:"));
            }
            return dt;
        }
        /// <summary>
        /// Get Holiday End date
        /// </summary>
        /// <returns>DataTable.</returns>
        public DataTable GetHolidayEndDates(string ddlHoliday, string ddlPostWeek, string ddlFiscalYear, String TableName = "Dim_Calendar_Holidays_Core")
        {
            DataTable dt = new DataTable();
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = helper.GetResourceString(AppConstants.SELECT_HOLIDAYENDDATE).Replace("@TableName", string.Format("{0}", TableName)).Replace("@ddlHoliday", string.Format("'{0}'", ddlHoliday)).Replace("@ddlFiscalYear", string.Format("'{0}'", ddlFiscalYear)).Replace("@ddlPostWeek", string.Format("{0}", ddlPostWeek));
                dt = dataAccessProvider.Select(strQuery);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > GetHolidayEndDates:"));
            }
            return dt;
        }
        /// <summary>
        /// Get Imsert parameters list for distribution gap analysis report
        /// </summary>
        public void InsertParametersDistributionGapAnalysis(string EXEC_IDENTIFIER)
        {
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = helper.GetResourceString(AppConstants.GET_SALES_OPPORTUNITY_SP_EXCUTE).Replace("@EXEC_IDENTIFIER", "'" + EXEC_IDENTIFIER + "'");
                dataAccessProvider.ExecuteNonQuery(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertParametersDistributionGapAnalysis:"));
            }
        }
        /// <summary>
        /// Get Imsert parameters list for product purchase multiple report
        /// </summary>
        public void InsertParametersProductPurchaseMultiple(string EXEC_IDENTIFIER)
        {
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = helper.GetResourceString(AppConstants.GET_PRODUCT_PURCHASE_MULTIPLE_SP_EXCUTE).Replace("@EXEC_IDENTIFIER", "'" + EXEC_IDENTIFIER + "'");
                dataAccessProvider.ExecuteNonQuery(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertParametersProductPurchaseMultiple:"));
            }
        }
        /// <summary>
        /// Get menu list
        /// </summary>
        /// <returns>DataTable.</returns>
        public DataTable GetMenuName(string MenuId)
        {

            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = helper.GetResourceString(AppConstants.GET_MENUNAME).Replace("@MenuId", "'" + MenuId + "'");
                dtMenu = dataAccessProvider.Select(strQuery);

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > GetMenuName:"));
            }
            return dtMenu;
        }
        /// <summary>
        /// insert report parameters call
        /// </summary>
        /// <param name="Exec_id">execution id</param>
        /// <param name="Parameter_Id"></param>
        /// <param name="Filters"></param>
        public void InsertReportParameters_Call(string Exec_id, int Parameter_Id, string Filters)
        {
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = helper.GetResourceString(AppConstants.GET_INSERTREPORT_PARAMETERS).Replace("@p_ExecId", "'" + Exec_id + "'").Replace("@p_Parameter_id", Parameter_Id.ToString()).Replace("@p_Parameter_Value", "'" + Filters + "'");
                dataAccessProvider.ExecuteNonQuery(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertReportParameters_Call:"));
            }
        }
        /// <summary>
        /// insert dynamic sql for sales report driver basket
        /// </summary>
        /// <param name="value">value</param>
        public void InsertDynamicSql_ReportSalesDriverBasket(string Value)
        {
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                string strQuery = helper.GetResourceString(AppConstants.GET_SALES_DRIVER_BASKET_SP_EXECUTE).Replace("@value", Value);
                dataAccessProvider.ExecuteNonQuery(strQuery);
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertDynamicSql_ReportSalesDriverBasket:"));
            }
        }
    

    }

    /// <summary>
    /// PARTIAL CLASS FOR HANDELING TABLEAU WORK
    /// </summary>
    public partial class DataLayer
    {
        #region TABLEAU CODE

        /// <summary>
        /// Creating tableau url by several parameters
        /// </summary>
        /// <param name="loginUserName"></param>
        /// <param name="reportUrl"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public string CreateCompleteTableauReportURL(string loginUserName, string reportUrl, string parameters)
        {
            string ticket = string.Empty;
            string url = string.Empty;
            try
            {
                ticket = GetTableauTicket(loginUserName);
                if (!string.IsNullOrEmpty(ticket))
                {
                    if (ticket == "-1")
                    {
                        bool reportWithMain = reportUrl.Contains(AppConstants.MAIN);
                        if (!reportWithMain)
                        {
                            url = AppConstants.HTTP + AppConstants.TableauServerInstanceURL + AppConstants.HASH + AppConstants.VIEWS + reportUrl + AppConstants.MAIN;
                        }
                        else
                        {
                            url = AppConstants.HTTP + AppConstants.TableauServerInstanceURL + AppConstants.HASH + AppConstants.VIEWS + reportUrl;
                        }
                    }
                    else
                    {
                        var reportWithMain = reportUrl.Contains(AppConstants.MAIN);
                        if (!reportWithMain)
                        {
                            url = AppConstants.HTTP + AppConstants.TableauServerInstanceURL + AppConstants.TRUSTED + ticket + AppConstants.VIEWS + reportUrl + AppConstants.MAIN;
                        }
                        else
                        {
                            url = AppConstants.HTTP + AppConstants.TableauServerInstanceURL + AppConstants.TRUSTED + ticket + AppConstants.VIEWS + reportUrl;
                        }
                        url = !string.IsNullOrEmpty(parameters) ? url + "?" + parameters : url;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > CreateCompleteTableauReportURL:"));
            }

            return url;
        }

        /// <summary>
        /// Creating ticket to authenticate tableau request
        /// </summary>
        /// <param name="loginUserName"></param>
        /// <returns></returns>
        private string GetTableauTicket(string loginUserName)
        {
            string resString = string.Empty;
            ASCIIEncoding enc = new ASCIIEncoding();
            string postData = "username=" + loginUserName.ToUpper();

            byte[] data = enc.GetBytes(postData);
            try
            {
                string serverURL = AppConstants.HTTP + AppConstants.TableauServerInstanceURL + "/trusted";
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(serverURL);
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = data.Length;
                // Write the request
                Stream outStream = req.GetRequestStream();
                outStream.Write(data, 0, data.Length);
                outStream.Close();
                // Do the request to get the response
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader inStream = new StreamReader(res.GetResponseStream(), enc);
                resString = inStream.ReadToEnd();
                inStream.Close();
                helper.WriteLog(string.Format("{0}" + resString, "Ticket:"));
            }

            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > GetTableauTicket:"));
            }
            return resString;
        }

        public string EncodeSpecialCharacters(string strOriginal, bool isEncodingRequired)
        {
            string strEncodedString = strOriginal;
            if (isEncodingRequired)
            {
                // Replacing % with %25
                strEncodedString = strEncodedString.Replace("%", "%25");

                // Replacing Space with %20
                strEncodedString = strEncodedString.Replace(" ", "%20");

                // Replacing Tab with %09
                strEncodedString = strEncodedString.Replace("\t", "%09");

                // Replacing ! with %21
                strEncodedString = strEncodedString.Replace("!", "%21");

                // Replacing # with %23
                strEncodedString = strEncodedString.Replace("#", "%23");

                // Replacing $ with %24
                strEncodedString = strEncodedString.Replace("$", "%24");

                // Replacing & with %26
                strEncodedString = strEncodedString.Replace("&", "%26");

                // Replacing ' with %27
                strEncodedString = strEncodedString.Replace("'", "%27");

                // Replacing * with %2A
                strEncodedString = strEncodedString.Replace("*", "%2A");

                // Replacing , with %2C
                strEncodedString = strEncodedString.Replace(",", "%2C");

                // Replacing - with %2D
                strEncodedString = strEncodedString.Replace("-", "%2D");

                // Replacing . with %2E
                strEncodedString = strEncodedString.Replace(".", "%2E");

                // Replacing / with %2F
                strEncodedString = strEncodedString.Replace("/", "%2F");

                // Replacing : with %3A
                strEncodedString = strEncodedString.Replace(":", "%3A");

                // Replacing ; with %3B
                strEncodedString = strEncodedString.Replace(";", "%3B");

                // Replacing < with %3C
                strEncodedString = strEncodedString.Replace("<", "%3C");

                // Replacing = with %3D
                strEncodedString = strEncodedString.Replace("=", "%3D");

                // Replacing > with %3E
                strEncodedString = strEncodedString.Replace(">", "%3E");

                // Replacing ? with %3F

                strEncodedString = strEncodedString.Replace("?", "%3F");

                // Replacing @ with %40

                strEncodedString = strEncodedString.Replace("@", "%40");

                // Replacing [ with %5B

                strEncodedString = strEncodedString.Replace("[", "%5B");

                // Replacing \ with %5C

                strEncodedString = strEncodedString.Replace(@"\", "%5C");

                // Replacing ] with %5D

                strEncodedString = strEncodedString.Replace("]", "%5D");

                // Replacing ^ with %5E

                strEncodedString = strEncodedString.Replace("^", "%5E");

                // Replacing _ with %5F

                strEncodedString = strEncodedString.Replace("_", "%5F");

                // Replacing ` with %60

                strEncodedString = strEncodedString.Replace("`", "%60");

                // Replacing { with %7B

                strEncodedString = strEncodedString.Replace("{", "%7B");

                // Replacing | with %7C

                strEncodedString = strEncodedString.Replace("|", "%7C");

                // Replacing } with %7D

                strEncodedString = strEncodedString.Replace("}", "%7D");

                // Replacing ~ with %7E

                strEncodedString = strEncodedString.Replace("~", "%7E");

                // Replacing ( with %28

                strEncodedString = strEncodedString.Replace("(", "%28");

                // Replacing ) with %29

                strEncodedString = strEncodedString.Replace(")", "%29");

                // Replacing + with %2B

                strEncodedString = strEncodedString.Replace("+", "%2B");

                // Replacing " with %22

                strEncodedString = strEncodedString.Replace("\"", "%22");
            }

            return strEncodedString;
        }

        public DateTime StartDate()
        {
            DateTime SundayBeforeLast = EndDate().AddDays(-6);
            return SundayBeforeLast;
        }
        public DateTime EndDate()
        {
            DateTime StartOfWeek = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek);
            DateTime LastSaturday = StartOfWeek.AddDays(-1);
            return LastSaturday;
        }

        public DateTime RCDate(string ddlRCSelectedText)
        {
            DataTable dt = new DataTable();
            string strQuery = "";
            DateTime RCDate = DateTime.Now;
            try
            {
                dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType());
                if (ddlRCSelectedText == "true")
                    strQuery = helper.GetResourceString(AppConstants.RCDATE1);
                else
                    strQuery = helper.GetResourceString(AppConstants.RCDATE2);
                using (dt = dataAccessProvider.Select(strQuery))
                {
                    if (dt.Rows.Count > 0)
                    {
                        RCDate = Convert.ToDateTime(dt.Rows[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs >  RCDate:"));
            }
            return RCDate;
        }

        #endregion
    }

    public static partial class DataManager
    {
        public static bool _AlcoholLogin;

        public static String PortalName = ConfigurationManager.AppSettings["ApplicationGroup"].ToString();
        public static bool CheckUnderMaintenanceUsers(string UserId)
        {
            bool blResult = false;
            IControlHelper helper = new IControlHelper();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    List<IDataParameter> parameters = new List<IDataParameter>();
                    IDataParameter param11 = new SqlParameter("@UserName", SqlDbType.VarChar);
                    param11.Value = UserId;
                    parameters.Add(param11);
                    using (DataTable dtResult = dataAccessProvider.ExecuteProcedure("usp_CheckUnderMaintenanceUsers", parameters))
                    {

                        if (dtResult != null && dtResult.Rows.Count > 0)
                            blResult = true;
                    }
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > CheckUnderMaintenanceUsers:"));
            }
            return blResult;
        }
        /// <summary>
        /// save user login activity
        /// </summary>
        public static void SaveUserActivity(string userId, string strPageId, string strPageName, string strSessionId)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                string strMyIP = GetIPAddress();

                List<IDataParameter> parameters = new List<IDataParameter>();

                IDataParameter paramUserID = new SqlParameter("@UserID", userId);
                parameters.Add(paramUserID);

                IDataParameter paramPageId = new SqlParameter("@PageID", strPageId);
                parameters.Add(paramPageId);

                IDataParameter paramPageName = new SqlParameter("@PageName", strPageName);
                parameters.Add(paramPageName);

                IDataParameter paramPageIP = new SqlParameter("@SystemIP", strMyIP);
                parameters.Add(paramPageIP);

                IDataParameter paramSessionId = new SqlParameter("@SessionID", strSessionId);
                parameters.Add(paramSessionId);

                IDataParameter paramPortalId = new SqlParameter("@PortalName", PortalName);
                parameters.Add(paramPortalId);

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dataAccessProvider.ExecuteNonQuery("usp_SaveUserActivityDetails", parameters, true);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > SaveUserActivity:"));
            }
        }
        /// <summary>
        /// Get IP Address
        /// </summary>
        public static string GetIPAddress()
        {
            IControlHelper helper = new IControlHelper();
            string ipaddress = string.Empty;
            try
            {
                ipaddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (string.IsNullOrEmpty(ipaddress))
                    ipaddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetIPAddress:"));
            }
            return ipaddress;
        }
        /// <summary>
        /// Check if user is in locked state
        /// </summary>
        public static bool IsUserLocked(string userName, string strPassword)
        {
            bool blResult = false;
            IControlHelper helper = new IControlHelper();
            try
            {
                List<IDataParameter> parameters = new List<IDataParameter>();

                IDataParameter paramUserName = new SqlParameter("@UserName", SqlDbType.VarChar);
                paramUserName.Value = userName;
                parameters.Add(paramUserName);

                IDataParameter paramPwd = new SqlParameter("@Password", SqlDbType.VarChar);
                paramPwd.Value = strPassword;
                parameters.Add(paramPwd);

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    using (DataTable dtResult = dataAccessProvider.ExecuteProcedure("usp_UserAccountLockStatus", parameters))
                    {
                        if (dtResult != null && dtResult.Rows.Count > 0)
                            blResult = true;
                    }
                }


            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > IsUserLocked"));
            }
            return blResult;
        }
        /// <summary>
        /// Check portal exist for user or not
        /// </summary>
        public static bool IsPortalExistForUser(string UserName)
        {
            bool portalExist = false;
            IControlHelper helper = new IControlHelper();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string sql = helper.GetResourceString(AppConstants.LOGIN_CHECK_PORTAL_EXISTANCE, DataSourceConnection.SQLServerConnection.ToString()).Replace("@UserName", "'" + UserName + "'").Replace("@PortalName", "'" + PortalName + "'");
                    using (DataTable dt = dataAccessProvider.Select(sql))
                    {
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            portalExist = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > IsPortalExistForUser:"));
            }
            return portalExist;
        }
        /// <summary>
        /// Check user is valid user or not
        /// </summary>
        public static bool ValidateUser(string UserName, string Pwd)
        {
            IControlHelper helper = new IControlHelper();

            List<dynamic> result = null;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string query = helper.GetResourceString(AppConstants.LOGIN_GET_LOGIN_SESSION_USER, DataSourceConnection.SQLServerConnection.ToString()).Replace("@USERNAME", "'" + UserName + "'").Replace("@PWD", "'" + Pwd + "'");
                    result = dataAccessProvider.SelectAll(query);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > ValidateUser"));
            }
            return result.Count > 0 ? true : false;
        }
        /// <summary>
        /// Get home url for login
        /// </summary>
        public static string GetHomeUrl_Login(string UserName, string defaultPage, string initialPath)
        {
            IControlHelper helper = new IControlHelper();
            string url = "";
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    UserName = UserName.ToLower();
                    string strPersonId = DataManager.GetFieldValue(helper, dataAccessProvider, "Logins where lower(Login)=lower('" + UserName + "')", "OwnerEntityId");

                    string sql = helper.GetResourceString(AppConstants.LOGIN_GET_HOMEURL, DataSourceConnection.SQLServerConnection.ToString()).Replace("@UserId", "'" + strPersonId + "'").Replace("@PortalName", "'" + PortalName + "'"); ;
                    using (DataTable dt = dataAccessProvider.Select(sql))
                    {

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["DefaultPageIcon"])))
                            {
                                if (dt.Rows[0]["PageURL"].ToString().ToLower().Contains("default"))
                                    url = initialPath + dt.Rows[0]["PageURL"].ToString();
                                else
                                    url = initialPath + dt.Rows[0]["PageURL"].ToString() + "?vid=11&mid=" + dt.Rows[0]["MenuID"].ToString();
                            }
                            else
                                url = initialPath + defaultPage + "? vid=11&mid=" + dt.Rows[0]["MenuID"].ToString() + "&DefaultPageIcon=show";
                        }
                        else
                            url = defaultPage + "?username=" + UserName;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetHomeUrl"));
            }
            return url;
        }
        /// <summary>
        /// Check home url using master page
        /// </summary>
        public static string GetHomeUrl_SiteMaster(string PersonId, string defaultPage, string initialPath)
        {
            IControlHelper helper = new IControlHelper();
            string url = "";
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {

                    string sql = helper.GetResourceString(AppConstants.LOGIN_GET_HOMEURL, DataSourceConnection.SQLServerConnection.ToString()).Replace("@UserId", "'" + PersonId + "'").Replace("@PortalName", "'" + PortalName + "'"); ;
                    using (DataTable dt = dataAccessProvider.Select(sql))
                    {
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["DefaultPageIcon"])))
                            {
                                if (dt.Rows[0]["PageURL"].ToString().ToLower().Contains("default"))
                                    url = initialPath + dt.Rows[0]["PageURL"].ToString();
                                else
                                    url = initialPath + dt.Rows[0]["PageURL"].ToString() + "?vid=11&mid=" + dt.Rows[0]["MenuID"].ToString();
                            }
                            else
                                url = initialPath + defaultPage + "? vid=11&mid=" + dt.Rows[0]["MenuID"].ToString() + "&DefaultPageIcon=show";
                        }
                        else
                            url = defaultPage;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetHomeUrl"));
            }
            return url;
        }
        /// <summary>
        /// get field value
        /// </summary>
        private static String GetFieldValue(IControlHelper helper, DataAccessProvider dataAccessProvider, string strTableName, string strFldName)
        {
            string ret = "";
            string strQuery = "Select " + strFldName + " as fld from " + strTableName;

            try
            {
                using (DataTable dt = dataAccessProvider.Select(strQuery))
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        ret = dr["fld"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetHomeUrl"));
            }
            return ret;

        }
        /// <summary>
        /// get field value
        /// </summary>
        public static String GetFieldValue(string strTableName, string strFldName)
        {
            IControlHelper helper = new IControlHelper();
            string ret = "";
            string strQuery = "Select " + strFldName + " as fld from " + strTableName;

            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    using (DataTable dt = dataAccessProvider.Select(strQuery))
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ret = dr["fld"].ToString();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetFieldValue  version2"));
            }
            return ret;

        }
        /// <summary>
        /// get field value for netezza
        /// </summary>
        public static String GetFieldValueNetezza(string strTableName, string strFldName)
        {
            IControlHelper helper = new IControlHelper();
            string ret = "";
            string strQuery = "Select " + strFldName + " as fld from " + strTableName;

            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    using (DataTable dt = dataAccessProvider.Select(strQuery))
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ret = dr["fld"].ToString();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetFieldValueNetezza "));
            }
            return ret;

        }
        /// <summary>
        /// get MAC address
        /// </summary>
        public static string GetMACAddress()
        {
            IControlHelper helper = new IControlHelper();
            string MACaddress = "";
            try
            {
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (nic.OperationalStatus == OperationalStatus.Up)
                    {
                        MACaddress += nic.GetPhysicalAddress().ToString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetMACAddress"));
            }
            return MACaddress;
        }
        /// <summary>
        /// Get user login details
        /// </summary>
        public static DataTable GetUserLogInDetails(string UserName, string GUID, bool IsMACAddress = false)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dt = new DataTable();
            string strMACAddress = "";
            try
            {
                if (IsMACAddress != false)
                {
                    strMACAddress = GetMACAddress();
                }
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    List<IDataParameter> parameters = new List<IDataParameter>();

                    IDataParameter paramUserName = new SqlParameter("@UserID", UserName);
                    IDataParameter paramMacAddress = new SqlParameter("@MACAddress", strMACAddress);
                    IDataParameter paramGuId = new SqlParameter("@GUID", GUID);
                    parameters.Add(paramUserName);
                    parameters.Add(paramMacAddress);
                    parameters.Add(paramGuId);

                    dt = dataAccessProvider.ExecuteProcedure("usp_GetUserLogInActivity_Details", parameters);

                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetUserLogInInfo"));
            }
            return dt != null && dt.Rows.Count > 0 ? dt : new DataTable();
        }
        /// <summary>
        /// Update user log in activity
        /// </summary>
        public static bool UpdateUserLogInActivity(string UserID, string GUID, int IsDifference)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                string strMACAddress = GetMACAddress();
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    List<IDataParameter> parameters = new List<IDataParameter>();

                    IDataParameter paramUserName = new SqlParameter("@UserID", UserID);
                    IDataParameter paramMacAddress = new SqlParameter("@MACAddress", strMACAddress);
                    IDataParameter paramGuId = new SqlParameter("@GUID", GUID);
                    IDataParameter paramIsDifference = new SqlParameter("@IsDifference", IsDifference);
                    IDataParameter paramPortalName = new SqlParameter("@PortalName", PortalName);
                    parameters.Add(paramUserName);
                    parameters.Add(paramMacAddress);
                    parameters.Add(paramGuId);
                    parameters.Add(paramIsDifference);
                    parameters.Add(paramPortalName);

                    dataAccessProvider.ExecuteNonQuery("usp_UpdateUserLogInActivity", parameters, true);
                    return true;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > UpdateUserLogInActivity"));
                return false;
            }

        }
        /// <summary>
        /// save user log in activity
        /// </summary>
        public static string SaveUserLogInActivity(string UserID)
        {
            IControlHelper helper = new IControlHelper();
            string strGUID = "";
            try
            {
                string strMACAddress = GetMACAddress();
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    List<IDataParameter> parameters = new List<IDataParameter>();

                    IDataParameter paramUserName = new SqlParameter("@UserID", UserID);
                    IDataParameter paramMacAddress = new SqlParameter("@MACAddress", strMACAddress);
                    IDataParameter paramPortalName = new SqlParameter("@PortalName", PortalName);
                    parameters.Add(paramUserName);
                    parameters.Add(paramMacAddress);
                    parameters.Add(paramPortalName);

                    using (DataTable dt = dataAccessProvider.ExecuteProcedure("usp_SaveUserLogInActivity", parameters))
                    {
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            strGUID = dt.Rows[0]["GUID"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > SaveUserLogInActivity"));
            }
            return strGUID;
        }
        /// <summary>
        /// check user id shared shrink
        /// </summary>
        public static bool IsSharedShrinkUser(string SupplierId)
        {
            IControlHelper helper = new IControlHelper();
            bool blResult = false;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    List<IDataParameter> parameters = new List<IDataParameter>();

                    IDataParameter paramSupplierId = new SqlParameter("@SupplierId", SupplierId);
                    parameters.Add(paramSupplierId);

                    using (DataTable dtResult = dataAccessProvider.ExecuteProcedure("usp_NonSharedShrinkUsers", parameters, true))
                    {

                        if (dtResult.Rows.Count > 0)
                            blResult = true;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > IsSharedShrinkUser"));
            }
            return blResult;
        }
        /// <summary>
        /// Get notification list
        /// </summary>
        public static DataTable GetNotifications(string UserName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string query = helper.GetResourceString(AppConstants.SP_LOGIN_GETNOTIFICATIONS, DataSourceConnection.SQLServerConnection.ToString()).Replace("@USERNAME", "'" + UserName + "'");
                    dtResult = dataAccessProvider.Select(query);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > IsSharedShrinkUser"));
            }
            return dtResult;
        }
        /// <summary>
        /// Get Upcoming messgaes list
        /// </summary>
        public static DataTable GetUpcomingMessage()
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dtResult = dataAccessProvider.ExecuteProcedure("usp_MaintenanceSchedule_ShowUpComingMessage", true);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetUpcomingMessage"));
            }
            return dtResult;
        }
        /// <summary>
        /// Get during message
        /// </summary>
        public static DataTable GetDuringMessage()
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dtResult = dataAccessProvider.ExecuteProcedure("usp_MaintenanceSchedule_ShowDuringMessage", true);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetDuringMessage"));
            }
            return dtResult;
        }
        /// <summary>
        /// Get during site active message
        /// </summary>
        public static DataTable GetDuringSiteActiveMessage()
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dtResult = dataAccessProvider.ExecuteProcedure("usp_MaintenanceSchedule_ShowDuringMessage_SiteActive", true);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetDuringSiteActiveMessage"));
            }
            return dtResult;
        }
        /// <summary>
        /// Get user role name
        /// </summary>
        public static string GetUserRoleName(string UserName)
        {
            IControlHelper helper = new IControlHelper();
            String RoleName = "filtered";
            try
            {

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    string strQuery = helper.GetResourceString(AppConstants.GET_USERROLE_SV).Replace("@UserName", "'" + UserName + "'");

                    using (DataTable dt = dataAccessProvider.Select(strQuery))
                    {
                        if (dt.Rows.Count > 0)
                        {
                            RoleName = dt.Rows[0]["RoleSV"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetUserRoleName"));
            }
            return RoleName;
        }
        /// <summary>
        /// Get Admin message details
        /// </summary>
        public static DataTable GetAdminMessageDetails(string UserId)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string query = helper.GetResourceString(AppConstants.SP_GET_ADMIN_MESSAGE, DataSourceConnection.SQLServerConnection.ToString()).Replace("@UserId", "'" + UserId + "'");
                    dtResult = dataAccessProvider.Select(query);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetAdminMessageDetails"));
            }
            return dtResult;
        }
        /// <summary>
        /// Save AdminMessage Activity Details
        /// </summary>
        /// <param name="UserId">User Id</param>
        /// <param name="AdminMessageId">Admin message id</param>
        public static void SaveAdminMessageActivityDetails(string UserId, String AdminMessageId)
        {
            IControlHelper helper = new IControlHelper();
            try
            {

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string query = helper.GetResourceString(AppConstants.SP_INSERT_ADMIN_MESSAGE_ACTIVITY, DataSourceConnection.SQLServerConnection.ToString()).Replace("@UserId", "'" + UserId + "'").Replace("@AdminMessageId", "'" + AdminMessageId + "'");
                    dataAccessProvider.ExecuteNonQuery(query);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > SaveAdminMessageActivityDetails"));
            }
        }
        /// <summary>
        /// Get current user role id
        /// </summary>
        /// <param name="UserId">User Id</param>
        public static string GetCurrentUserRoleId(string UserId)
        {
            string strResult = "";
            IControlHelper helper = new IControlHelper();
            try
            {
                string PortalName = ConfigurationManager.AppSettings["ApplicationGroup"] != null ? ConfigurationManager.AppSettings["ApplicationGroup"].ToString() : "General";
                strResult = GetFieldValue("AssignUserGroup A inner join UserGroup R on R.Groupid=A.Groupid where (R.PortalName='" + PortalName + "' or R.PortalName='General') and UserID=" + UserId, "R.Groupid");
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetCurrentUserRoleId"));
            }
            return strResult;
        }
        /// <summary>
        /// Get current vendor name by vendor number
        /// </summary>
        /// <param name="vendorNo">vendor number</param>
        public static string GetVendorNameByVendorNo(string VendorNo)
        {
            IControlHelper helper = new IControlHelper();
            string VendorName = "";

            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {

                    string strQuery = helper.GetResourceString(AppConstants.GET_VENDORNAME_BY_VENDORNO).Replace("@VENDOR_NUMBER", VendorNo);
                    using (DataTable dt = dataAccessProvider.Select(strQuery))
                    {

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            VendorName = dt.Rows[0][0].ToString();
                        }
                    }

                }
            }

            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetVendorNameByVendorNo:"));
            }
            return VendorName;
        }
        /// <summary>
        /// check for corporate user
        /// </summary>
        /// <param name="username">user name</param>
        public static bool IsCorporateUser(string UserName)
        {
            IControlHelper helper = new IControlHelper();
            bool IsCorporate = true;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    string strQuery = helper.GetResourceString(AppConstants.GET_ROLEBY_USERNAME).Replace("@UserName", UserName);

                    using (DataTable dt = dataAccessProvider.Select(strQuery))
                    {

                        if (dt.Rows.Count > 0)
                        {
                            if (Convert.ToInt32(dt.Rows[0]["Role"]) == 0)
                                IsCorporate = true;
                            else if (Convert.ToInt32(dt.Rows[0]["Role"]) > 0)
                                IsCorporate = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManger.cs > IsCorporateUser:"));
            }
            return IsCorporate;
        }
        /// <summary>
        /// get user role based on profile
        /// </summary>
        /// <param name="username">user name</param>
        /// <param name="ProfileName">profile name</param>
        public static string GetUserRoleBasedOnProfile(string UserName, string ProfileName)
        {
            IControlHelper helper = new IControlHelper();
            String Role = "";
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    string strQuery = helper.GetResourceString(AppConstants.GET_USERROLE_BYPROFILE).Replace("@UserName ", "'" + UserName + "'").Replace("@ProfileName", "'" + ProfileName + "'");
                    var Result = dataAccessProvider.ExecuteScaler(strQuery);
                    if (Result != null)
                    {
                        Role = Result.ToString();
                    }
                }
            }

            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManger.cs > GetUserRoleBasedOnProfile:"));

            }
            return Role;
        }
        /// <summary>
        /// check is site down
        /// </summary>
        public static bool IsSiteDown()
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    using (DataTable dtUpcoming = dataAccessProvider.ExecuteProcedure("usp_MaintenanceSchedule_ShowDuringMessage", true))
                    {
                        if (dtUpcoming != null && dtUpcoming.Rows.Count > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, " DataManger.cs >  IsSiteDown:"));
                return false;
            }
        }
        /// <summary>
        /// get user menues
        /// </summary>
        public static DataTable GetUserMenues(string LoginID, string VerticalID = "")
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    List<IDataParameter> parameters = new List<IDataParameter>();
                    PortalName = String.IsNullOrEmpty(PortalName) ? "General" : PortalName;

                    IDataParameter paramLoginId = new SqlParameter("@LoginID", LoginID);
                    IDataParameter paramVerticalId = new SqlParameter("@VerticalID", VerticalID);
                    IDataParameter paramPortalName = new SqlParameter("@PortalName", PortalName);
                    parameters.Add(paramLoginId);
                    parameters.Add(paramVerticalId);
                    parameters.Add(paramPortalName);

                    dtResult = dataAccessProvider.ExecuteProcedure("usp_SetUserMenus", parameters, true);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, " DataManger.cs >  GetUserMenues:"));
            }
            return dtResult;
        }
        /// <summary>
        /// get user parent menues
        /// </summary>
        /// <param name="LoginID">login id</param>
        public static DataTable GetParentMenues(string LoginID)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    List<IDataParameter> parameters = new List<IDataParameter>();
                    PortalName = String.IsNullOrEmpty(PortalName) ? "General" : PortalName;

                    IDataParameter paramLoginId = new SqlParameter("@LoginID", LoginID);
                    IDataParameter paramPortalName = new SqlParameter("@PortalName", PortalName);
                    parameters.Add(paramLoginId);
                    parameters.Add(paramPortalName);

                    dtResult = dataAccessProvider.ExecuteProcedure("usp_SetTopMenus", parameters, true);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, " DataManger.cs >  GetParentMenues:"));
            }
            return dtResult;
        }

        /// <summary>
        /// Gets The Profile Vendor Number table
        /// </summary>
        /// <param name="userName"> The userName</param>
        /// <returns>DataTable.</returns>
        public static string[] GetUserProfileAndVendorNumber(string userName)
        {
            string[] arr = new string[2];
            IControlHelper helper = new IControlHelper();

            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    string strQuery = helper.GetResourceString(AppConstants.GENERIC_GET_PROFILE_VENDOR_NUMBER).Replace("@USERNAME", "'" + userName + "'");
                    using (DataTable dtProfileVendorDetails = dataAccessProvider.Select(strQuery))
                    {
                        if (dtProfileVendorDetails.Rows.Count > 0)
                        {
                            arr[0] = dtProfileVendorDetails.Rows[0]["ProfileName"].ToString();
                            arr[1] = dtProfileVendorDetails.Rows[0]["Vendor_Num"].ToString();
                        }
                        else
                        if (dtProfileVendorDetails.Rows.Count == 0)
                        {
                            string qryProfile = "select distinct ProfileName from USERS_PROFILES_ROLES where lower(UserName)=lower('" + userName + "') order by ProfileName limit 1";
                            using (DataTable dtUserProfiles = dataAccessProvider.Select(qryProfile))
                            {
                                arr[0] = dtUserProfiles.Rows[0]["ProfileName"].ToString();
                            }

                            string qryVendorNumber = "select distinct Vendor_Num from USERS_PROFILES_ROLES where lower(ProfileName)=lower('" + arr[0].ToString() + "') and lower(UserName)=lower('" + userName + "') order by Vendor_Num limit 1";
                            using (DataTable dtVendors = dataAccessProvider.Select(qryVendorNumber))
                            {
                                arr[1] = dtVendors.Rows[0]["Vendor_Num"].ToString();
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetProfileVendorNumberDetails:"));
            }
            return arr;
        }
        /// <summary>
        /// Get user profile list
        /// </summary>
        /// <param name="userName">user name</param>
        public static DataTable GetUserProfileList(string userName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    string strQuery = helper.GetResourceString(AppConstants.GET_USER_PROFILE_LIST).Replace("@USERNAME", "'" + userName + "'");
                    dtResult = dataAccessProvider.Select(strQuery);
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > FillUserProfileList:"));
            }
            return dtResult;

        }
        /// <summary>
        /// Get vendor number list
        /// </summary>
        /// <param name="userName">user name</param>
        /// <param name="profileName">profile name</param>
        public static DataTable GetVendorNumberList(string userName, string profileName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    string strQuery = helper.GetResourceString(AppConstants.GET_VENDERNUMBER_LIST).Replace("@USERNAME", "'" + userName + "'").Replace("@PROFILENAME", "'" + profileName + "'");
                    dtResult = dataAccessProvider.Select(strQuery);
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetUserProfileList:"));
            }
            return dtResult;

        }
        /// <summary>
        /// Delete user profile by user name
        /// </summary>
        /// <param name="userName">user name</param>
        public static bool DeleteUserDefaultProfile_ByUserName(string userName)
        {
            IControlHelper helper = new IControlHelper();
            bool isDeleted = false; ;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    string strQuery = helper.GetResourceString(AppConstants.DELETE_USER_DEFAULTPROFILE_BY_USERNAME).Replace("@USERNAME", "'" + userName + "'");
                    dataAccessProvider.ExecuteNonQuery(strQuery);
                    isDeleted = true;
                }

            }
            catch (Exception ex)
            {
                isDeleted = false;
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetUserProfileList:"));
            }
            return isDeleted;

        }
        /// <summary>
        /// inser user default profile vendor
        /// </summary>
        /// <param name="userName">user name</param>
        /// <param name="profileName">profile name</param>
        /// <param name="vendorNumber">vendor number</param>
        public static void InsertUserDefaultProfileVendor(string userName, string profileName, string vendorNumber)
        {
            IControlHelper helper = new IControlHelper();

            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    string strQuery = helper.GetResourceString(AppConstants.INSERT_USER_DEFAULT_PROFILE_VENDOR).Replace("@USERNAME", "'" + userName + "'").Replace("@PROFILENAME", "'" + profileName + "'").Replace("@VENDERNUMBER", "'" + vendorNumber + "'");
                    dataAccessProvider.ExecuteNonQuery(strQuery);
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > InsertUserDefaultProfileVendor:"));
            }


        }
        /// <summary>
        /// get user roleid by user name and profile name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <param name="profileName">profile name</param>
        public static string GetUserRoleIdByUserNameAndProfileName(string UserName, string ProfileName)
        {
            IControlHelper helper = new IControlHelper();
            string RoleId = "0";
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    string strQuery = helper.GetResourceString(AppConstants.GET_ROLEBY_USERNAME).Replace("@UserName", "'" + UserName + "'");

                    if (!string.IsNullOrEmpty(ProfileName))
                        strQuery += " and lower(ProfileName) = lower('" + ProfileName + "') ";

                    strQuery += " Order by ProfileName Limit 1;";

                    using (DataTable dt = dataAccessProvider.Select(strQuery))
                    {

                        if (dt.Rows.Count > 0)
                        {
                            RoleId = dt.Rows[0]["Role"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetUserRoleIdByUserNameAndProfileName:"));
            }
            return RoleId;
        }
        /// <summary>
        /// get user attributes with login id
        /// </summary>
        /// <param name="loginid">login id</param>
        /// <param name="userIsAuthenticated">is user authenticated</param>
        public static string GetUserAttributesWithLoginID(Boolean userIsAuthenticated, string loginid)
        {
            IControlHelper helper = new IControlHelper();
            string returnString = string.Empty;
            try
            {
                if (!userIsAuthenticated)
                {
                    return "";
                }
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    List<IDataParameter> parameters = new List<IDataParameter>();
                    IDataParameter paramlogin = new SqlParameter("@loginid", SqlDbType.NVarChar);
                    paramlogin.Value = loginid;
                    parameters.Add(paramlogin);
                    using (DataTable dtResult = dataAccessProvider.ExecuteProcedure("prPerson_Attributes_Get", parameters, true))
                    {

                        if (dtResult != null && dtResult.Rows.Count > 0)
                            returnString = dtResult.Rows[0][0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetUserAttributesWithLoginID:"));
            }

            return returnString;

        }
        /// <summary>
        /// get search result
        /// </summary>
        public static DataTable GetSearchResults()
        {
            IControlHelper helper = new IControlHelper();
            DataTable dt = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dt = dataAccessProvider.ExecuteProcedure("usp_GetNewsUpdatesList", true);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Datamanger > GetSearchResults: "));
            }
            return dt;
        }
        /// <summary>
        /// Get Search Results Quick Links
        /// </summary>
        public static DataTable GetSearchResultsQuickLinks()
        {
            IControlHelper helper = new IControlHelper();
            DataTable dt = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dt = dataAccessProvider.ExecuteProcedure("usp_GetQuickLinksList", true);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, " Datamanger > GetSearchResultsQuickLinks: "));
            }
            return dt;
        }
        /// <summary>
        /// Get Page title
        /// </summary>
        /// <param name="menuIDQS">menuIDQS</param>
        ///  <param name="menuIDSession">menuIDSession</param>
        public static string GetPageTitle(string menuIDQS, string menuIDSession)
        {
            IControlHelper helper = new IControlHelper();
            string pageTitle = "";
            try
            {
                if (!string.IsNullOrEmpty(menuIDQS))
                {
                    pageTitle = GetFieldValue("webmenus where menuid=" + menuIDQS, "menuname");
                }
                else if (!string.IsNullOrEmpty(menuIDSession))
                {
                    pageTitle = GetFieldValue("webmenus where menuid=" + menuIDSession, "menuname");
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, " Datamanger.cs > GetPageTitle: "));
            }
            return pageTitle;
        }
        /// <summary>
        /// Get VerticalId And Default PageUrl
        /// </summary>
        public static DataTable GetVerticalIdAndDefaultPageUrl()
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string query = helper.GetResourceString(AppConstants.GET_HOMEPAGE_URL_ON_PROFILE_CHANGE, DataSourceConnection.SQLServerConnection.ToString());
                    dtResult = dataAccessProvider.Select(query);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetVerticalIdAndDefaultPageUrl"));
            }
            return dtResult;
        }
        /// <summary>
        /// Get SavedReport Info By Report and Username
        /// </summary>
        /// <param name="ReportName">Report name</param>
        ///  <param name="UserName">UserName</param>
        public static DataTable GetSavedReportInfoByReportNadUsername(string reportName, string userName)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string query = helper.GetResourceString(AppConstants.GENRIC_GETREPORTINFO_BY_REPORT_AND_USERNAME, DataSourceConnection.SQLServerConnection.ToString()).Replace("@USERNAME", "'" + userName + "'").Replace("@REPORTNAME", "'" + reportName + "'"); ;
                    dtResult = dataAccessProvider.Select(query);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetSavedReportInfoByReportNadUsername"));
            }
            return dtResult;
        }
        /// <summary>
        /// Get user email id
        /// </summary>
        /// <param name="UserName">UserName</param>
        public static string GetUserEmailId(string UserName)
        {
            IControlHelper helper = new IControlHelper();
            string UserEmailId = "";
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    string query = helper.GetResourceString(AppConstants.GENERIC_GET_USEREMAIL_BY_USERNAME).Replace("@USERNAME", "'" + UserName + "'");
                    using (DataTable dt = dataAccessProvider.Select(query))
                    {
                        if (dt.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["EMAIL"])))
                            {
                                UserEmailId = dt.Rows[0]["EMAIL"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetUserEmailId"));
            }
            return UserEmailId;
        }
        /// <summary>
        /// Get Saved Report Details
        /// </summary>
        /// <param name="reportId">reportId</param>
        public static DataTable GetSavedReportDetails(string reportId)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dt = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string query = helper.GetResourceString(AppConstants.GET_SAVED_REPORT_DETAILS, DataSourceConnection.SQLServerConnection.ToString()).Replace("@REPORTID", "'" + reportId.ToString() + "'");
                    dt = dataAccessProvider.Select(query);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetSavedReportDetails"));
            }
            return dt;
        }
        /// <summary>
        /// Get menu name
        /// </summary>
        /// <param name="MenuID">Menu id</param>
        public static DataTable GetMenuName(string MenuId)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dt1 = new DataTable();
            try
            {

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string query = helper.GetResourceString(AppConstants.GENERIC_GET_MENUNAME, DataSourceConnection.SQLServerConnection.ToString()).Replace("@MENUID", "'" + MenuId.ToString() + "'");
                    dt1 = dataAccessProvider.Select(query);
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetMenuName"));
            }
            return dt1;
        }
        /// <summary>
        /// Save report
        /// </summary>
        /// <param value='RepName'>report name</param>
        /// <param value='RepDesc'>report description</param>
        /// <param value='Username'>user name</param>
        /// <param value='dateVal'>date val</param>
        /// <param value='mid'>menu id</param>
        /// <param value='tableauUrl'>tableau Url</param>
        /// <param value='repParam'>report parameters</param>
        /// <param value='MenuName'>menu name</param>
        /// <param value='SelectedParameters'>Selected Parameters</param>
        /// <param value='enddate'>end date</param>
        /// <param value='Emailid'>Emailid</param>
        /// <param value='ScheduleId'>ScheduleId</param>
        /// <param value='sendReportStatus'>sendReportStatus</param>
        /// <param value='UserProfile'>UserProfile</param>
        /// <param value='SaveSpCall'>SaveSpCall</param>
        /// <param value='DbConnection'>DbConnection</param>
        public static void SaveReport(string RepName, string RepDesc, string Username, string dateVal, string mid, string tableauUrl,
                                      string repParam, string MenuName, string ParamName, string SelectedParameters, DateTime? enddate,
                                      string Emailid, string ScheduleId, int sendReportStatus, string UserProfile, string SaveSpCall = "", string DbConnection = "")
        {
            IControlHelper helper = new IControlHelper();

            try
            {

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    List<IDataParameter> parameters = new List<IDataParameter>();

                    IDataParameter paramRepName = new SqlParameter("@ReportName", RepName);
                    parameters.Add(paramRepName);
                    IDataParameter paramRepDesc = new SqlParameter("@ReportDescription", RepDesc);
                    parameters.Add(paramRepDesc);
                    IDataParameter paramUsername = new SqlParameter("@Username", Username);
                    parameters.Add(paramUsername);
                    IDataParameter paramMid = new SqlParameter("@MenuId", mid);
                    parameters.Add(paramMid);
                    IDataParameter paramtableauUrl = new SqlParameter("@TableauURL", tableauUrl);
                    parameters.Add(paramtableauUrl);
                    IDataParameter paramrepParam = new SqlParameter("@ReportParameters", repParam);
                    parameters.Add(paramrepParam);
                    if (dateVal == null)
                    {
                        IDataParameter paramDateVal = new SqlParameter("@DateRange", DBNull.Value);
                        parameters.Add(paramDateVal);
                    }
                    else
                    {
                        IDataParameter paramDateVal = new SqlParameter("@DateRange", dateVal);
                        parameters.Add(paramDateVal);

                    }
                    parameters.Add(new SqlParameter("@EmailId", Emailid));
                    parameters.Add(new SqlParameter("@ScheduleId", ScheduleId));
                    if (enddate == null)
                        parameters.Add(new SqlParameter("@EndDate", DBNull.Value));
                    else
                    {
                        parameters.Add(new SqlParameter("@EndDate", enddate));
                    }
                    parameters.Add(new SqlParameter("@OriginalReportName", MenuName));
                    parameters.Add(new SqlParameter("@SendReportNow", sendReportStatus));
                    parameters.Add(new SqlParameter("@UserProfile", UserProfile));
                    parameters.Add(new SqlParameter("@paramname", ParamName));
                    parameters.Add(new SqlParameter("@selectedFilters", SelectedParameters));
                    parameters.Add(new SqlParameter("@Spcall", SaveSpCall));
                    parameters.Add(new SqlParameter("@DBConnection", DbConnection));

                    string query = helper.GetResourceString(AppConstants.GENERIC_SAVE_REPORT, DataSourceConnection.SQLServerConnection.ToString());
                    dataAccessProvider.ExecuteNonQuery(query, parameters, false);

                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > SaveReport"));

            }
        }
        /// <summary>
        /// Share save report
        /// </summary>
        /// <param value='RepName'>report name</param>
        /// <param value='Username'>user name</param>
        /// <param value='mid'>menu id</param>
        /// <param value='tableauUrl'>tableau Url</param>
        /// <param value='repParam'>report parameters</param>
        /// <param value='MenuName'>menu name</param>
        /// <param value='sendReportStatus'>sendReportStatus</param>
        /// <param value='UserProfile'>UserProfile</param>
        /// <param value='ShareBy'>ShareBy</param>
        /// <param value='ShareMessage'>ShareMessage</param>
        /// <param value='ParamName'>ParamName</param>
        /// <param value='SelectedParameters'>SelectedParameters</param>
        public static void ShareSaveReport(string RepName, string Username, string mid, string tableauUrl,
                                     string repParam, string MenuName, int sendReportNow, string UserProfile, string ShareBy, string ShareMessage,
                                     string ParamName, string SelectedParameters)
        {
            IControlHelper helper = new IControlHelper();

            try
            {

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    List<IDataParameter> parameters = new List<IDataParameter>();

                    IDataParameter paramRepName = new SqlParameter("@ReportName", RepName);
                    parameters.Add(paramRepName);
                    IDataParameter paramUsername = new SqlParameter("@Username", Username);
                    parameters.Add(paramUsername);
                    IDataParameter paramMid = new SqlParameter("@MenuId", mid);
                    parameters.Add(paramMid);
                    IDataParameter paramtableauUrl = new SqlParameter("@TableauURL", tableauUrl);
                    parameters.Add(paramtableauUrl);
                    IDataParameter paramrepParam = new SqlParameter("@ReportParameters", repParam);
                    parameters.Add(paramrepParam);
                    parameters.Add(new SqlParameter("@OriginalReportName", MenuName));
                    parameters.Add(new SqlParameter("@SendReportNow", sendReportNow));
                    parameters.Add(new SqlParameter("@UserProfile", UserProfile));
                    parameters.Add(new SqlParameter("@shareby", ShareBy));
                    parameters.Add(new SqlParameter("@ShareMessage", ShareMessage));
                    parameters.Add(new SqlParameter("@paramname", ParamName));
                    parameters.Add(new SqlParameter("@selectedFilters", SelectedParameters));

                    string query = helper.GetResourceString(AppConstants.GENERIC_SHARESAVE_REPORT, DataSourceConnection.SQLServerConnection.ToString());
                    dataAccessProvider.ExecuteNonQuery(query, parameters, false);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > ShareSaveReport"));
            }
        }
        /// <summary>
        /// Update report
        /// </summary>
        /// <param value='RepId'>report id</param>
        /// <param value='RepName'>report name</param>
        /// <param value='RepDesc'>report description</param>
        /// <param value='dateVal'>date val</param>
        /// <param value='tableauUrl'>tableau Url</param>
        /// <param value='SelectedParameters'>Selected Parameters</param>
        /// <param value='repParam'>report parameters</param>
        /// <param value='ParamName'>Param Name </param>
        /// <param value='enddate'>end date</param>
        /// <param value='Emailid'>Emailid</param>
        /// <param value='ScheduleId'>ScheduleId</param>
        /// <param value='sendReportStatus'>sendReportStatus</param>
        public static void UpdateReport(string RepID, string RepDesc, string dateVal, string tableauUrl, string SelectedParameters, string repParam,
                                        string ParamName, DateTime? enddate, string Emailid, string ScheduleId, int sendReportStatus)
        {
            IControlHelper helper = new IControlHelper();

            try
            {

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    List<IDataParameter> parameters = new List<IDataParameter>();

                    IDataParameter paramRepDesc = new SqlParameter("@ReportDescription", RepDesc);
                    parameters.Add(paramRepDesc);
                    IDataParameter paramtableauUrl = new SqlParameter("@TableauURL", tableauUrl);
                    parameters.Add(paramtableauUrl);
                    IDataParameter paramrepParam = new SqlParameter("@ReportParameters", repParam);
                    parameters.Add(paramrepParam);
                    if (dateVal == null)
                    {
                        IDataParameter paramDateVal = new SqlParameter("@DateRange", DBNull.Value);
                        parameters.Add(paramDateVal);
                    }
                    else
                    {
                        IDataParameter paramDateVal = new SqlParameter("@DateRange", dateVal);
                        parameters.Add(paramDateVal);

                    }
                    parameters.Add(new SqlParameter("@EmailId", Emailid));
                    parameters.Add(new SqlParameter("@ScheduleId", ScheduleId));
                    if (enddate == null)
                        parameters.Add(new SqlParameter("@EndDate", DBNull.Value));
                    else
                    {
                        parameters.Add(new SqlParameter("@EndDate", enddate));
                    }
                    parameters.Add(new SqlParameter("@SendReportNow", sendReportStatus));
                    parameters.Add(new SqlParameter("@paramname", ParamName));
                    parameters.Add(new SqlParameter("@selectedFilters", SelectedParameters));

                    string query = "Update SavedReports set ReportDescription = @ReportDescription,TableauURL = @TableauURL,ReportParameters = @ReportParameters,DateRange = @DateRange,EmailId = @EmailId,ScheduleId = @ScheduleId,EndDate = @EndDate,SendReportNow = @SendReportNow,SelectedFilters = @SelectedFilters,paramname = @paramname  where reportid = '" + RepID + "'";
                    dataAccessProvider.ExecuteNonQuery(query, parameters, false);

                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > UpdateReport"));
            }
        }
        /// <summary>
        ///Save report for batching
        /// </summary>
        /// <param value='RepName'>report name</param>
        /// <param value='Username'>User name</param>
        /// <param value='mid'>Menu id</param>
        /// <param value='tableauUrl'>tableau Url</param>
        /// <param value='repParam'>report parameters</param>
        /// <param value='ParamName'>Param Name </param> 
        /// <param value='SelectedParameters'>Selected Parameters</param>
        /// <param value='Emailid'>Emailid</param>
        /// <param value='sendReportStatus'>sendReportStatus</param>
        /// <param value='UserProfile'>UserProfile</param>
        public static bool SaveReportForBatching(string RepName, string Username, string mid, string tableauUrl,
                                      string repParam, string ParamName, string SelectedParameters,
                                      string Emailid, int sendReportStatus, string UserProfile)
        {
            IControlHelper helper = new IControlHelper();
            bool result = false;
            try
            {

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    List<IDataParameter> parameters = new List<IDataParameter>();

                    parameters.Add(new SqlParameter("@ReportName", RepName));
                    parameters.Add(new SqlParameter("@Username", Username));
                    parameters.Add(new SqlParameter("@MenuId", mid));
                    parameters.Add(new SqlParameter("@TableauURL", tableauUrl));
                    parameters.Add(new SqlParameter("@ReportParameters", repParam));
                    parameters.Add(new SqlParameter("@EmailId", Emailid));
                    parameters.Add(new SqlParameter("@SendReportNow", sendReportStatus));
                    parameters.Add(new SqlParameter("@UserProfile", UserProfile));
                    parameters.Add(new SqlParameter("@paramname", ParamName));
                    parameters.Add(new SqlParameter("@selectedFilters", SelectedParameters));

                    string query = helper.GetResourceString(AppConstants.GENERIC_SAVE_REPORT_FOR_BATCHING, DataSourceConnection.SQLServerConnection.ToString());
                    dataAccessProvider.ExecuteNonQuery(query, parameters, false);
                    result = true;
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > SaveReport"));

            }
            return result;
        }

        /// <summary>
        /// Gets The Profile Vendor Number table
        /// </summary>
        /// <param name="reportId"> repor tId</param>
        /// <returns>bool</returns>
        public static bool SaveMessage(string reportId)
        {
            bool result = false;
            IControlHelper helper = new IControlHelper();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string sql = "update SavedReports set Is_Deleted=0 where reportid=" + reportId;
                    dataAccessProvider.ExecuteNonQuery(sql);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > SaveMessage"));
            }
            return result;
        }
        
        /// <summary>
        /// Get report details list
        /// </summary>
        public static DataTable GetReportInfo(string reportId, string reportName, string userName)
        {
            DataTable dt = new DataTable();
            IControlHelper helper = new IControlHelper();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string sql = helper.GetResourceString(AppConstants.GET_SAVED_REPORTS).Replace("@ReportName", reportName).Replace("@ReportID", reportId).Replace("@UserName", userName);
                    dt = dataAccessProvider.Select(sql);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetReportInfo"));
            }
            return dt;
        }
        /// <summary>
        /// Get single report details
        /// </summary>
        public static DataTable GetReportInfoByReportId(string reportId)
        {
            DataTable dt = new DataTable();
            IControlHelper helper = new IControlHelper();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string sql = "select * from SavedReports where ReportId='" + reportId + "'";
                    dt = dataAccessProvider.Select(sql);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetReportInfoByReportId"));
            }
            return dt;
        }
        /// <summary>
        /// Gets schedule report
        /// </summary>
        public static DataTable GetReportSchedule()
        {
            DataTable dt = new DataTable();
            IControlHelper helper = new IControlHelper();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string sql = "select * from SavedReportsSchedule order by ScheduleId";
                    dt = dataAccessProvider.Select(sql);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetReportSchedule"));
            }
            return dt;
        }
        /// <summary>
        /// Delete report
        /// </summary>
        /// <param name="ReportId">ReportId</param>
        public static void DeleteReports(int ReportId)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string str = "delete from SavedReports where reportid=" + ReportId;
                    dataAccessProvider.ExecuteNonQuery(str);

                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > DeleteReports"));
            }
        }
        /// <summary>
        /// check shared report name exist or not
        /// </summary>
        /// <param name="SharedReportName">SharedReportName</param>
        /// <param name="UserNames"> user name</param>
        public static bool SharedReportNameExists(string SharedReportName, string UserName)
        {
            IControlHelper helper = new IControlHelper();
            bool blResult = false;
            try
            {
                List<IDataParameter> parameters = new List<IDataParameter>();
                parameters.Add(new SqlParameter("p_UserName", UserName));
                parameters.Add(new SqlParameter("p_SharedReportName", SharedReportName.Replace(";", "|")));
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    using (DataTable dtResult = dataAccessProvider.ExecuteProcedure("usp_ValidateSharedReportName", parameters, true))
                    {
                        if (dtResult.Rows.Count > 0)
                            blResult = true;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > SharedReportNameExists"));
            }
            return blResult;
        }
        /// <summary>
        /// check Is Hierarchy shared or not
        /// </summary>
        /// <param name="CustomHierarchyId"> CustomHierarchyId</param>
        /// <param name="UserNames"> user name</param>
        public static bool IsHierarchyShared(string CustomHierarchyId, string UserNames)
        {
            IControlHelper helper = new IControlHelper();
            bool blnStatus = false;
            try
            {

                string strQuery = "Select distinct USERNAME from DIM_CUSTOM_HIERARCHIES_SHARED where CUSTOM_HIERARCHY_ID='" + CustomHierarchyId + "' and USERNAME in ('" + UserNames + "')";
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    using (DataTable dt = dataAccessProvider.Select(strQuery))
                    {
                        if (dt.Rows.Count > 0)
                            blnStatus = true;
                    }

                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > IsHierarchyShared"));
            }

            return blnStatus;
        }
        /// <summary>
        /// check if Is Owner of Hierarchy
        /// </summary>
        /// <param name="CustomHierarchyId"> CustomHierarchyId</param>
        /// <param name="UserNames"> user name</param>
        public static void ShareHierarchy_Owned(string CustomHierarchyId, string UserNames)
        {
            IControlHelper helper = new IControlHelper();
            DataLayer dal = new DataLayer();
            try
            {
                if (IsOwnerofHierarchy(CustomHierarchyId))
                {
                    String CurrDate = DateTime.Now.ToString();
                    string strQuery = "Insert into DIM_CUSTOM_HIERARCHIES_SHARED(CUSTOM_HIERARCHY_ID,USERNAME,DATE_SHARED) values ('" + CustomHierarchyId + "','" + UserNames + "','" + CurrDate + "')";

                    using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                    {
                        dataAccessProvider.ExecuteNonQuery(strQuery);

                        //CUSTOM_HIERARCHY_EXTENDED_UPDATE
                        dal.P_CUSTOM_HIERARCHY_EXTENDED_UPDATE(CustomHierarchyId.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > ShareHierarchy"));
            }
        }
        /// <summary>
        /// check if Is Owner of Hierarchy
        /// </summary>
        /// <param name="CustomHierarchyId"> CustomHierarchyId</param>
        public static bool IsOwnerofHierarchy(string CustomHierarchyId)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                string qry = "select * from Dim_Custom_Hierarchy_Master where CUSTOM_HIERARCHY_ID=" + CustomHierarchyId + " and lower(UserName)=lower('" + HttpContext.Current.Session["Username"].ToString() + "')";
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    using (DataTable dt = dataAccessProvider.Select(qry))
                    {
                        if (dt.Rows.Count > 0)
                            return true;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > IsOwnerofHierarchy"));
            }
            return false;
        }
        /// <summary>
        /// check if store is shared
        /// </summary>
        /// <param name="CustomStoreId"> CustomStoreId</param>
        /// <param name="UserNames"> user name</param>
        public static bool IsStoreShared(string CustomStoreId, string UserNames)
        {
            IControlHelper helper = new IControlHelper();
            bool blnStatus = false;
            try
            {
                string strQuery = "Select distinct USERNAME from DIM_CUSTOM_STORES_SHARED where CUSTOM_STORE_GROUP_ID='" + CustomStoreId + "' and USERNAME in ('" + UserNames + "')";

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {

                    using (DataTable dt = dataAccessProvider.Select(strQuery))
                    {
                        if (dt.Rows.Count > 0)
                            blnStatus = true;
                    }
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > IsStoreShared"));
            }
            return blnStatus;
        }
        /// <summary>
        /// Get User name
        /// </summary>
        /// <param name="UserNames"> user name</param>
        public static String GetUsername(String Username)
        {
            IControlHelper helper = new IControlHelper();
            String _Username = "";
            try
            {
                String strQ = "Select distinct NAME from USERDATAACCESS where  upper(UserName) =upper('" + Username + "')";
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    using (DataTable dtUser_Name = dataAccessProvider.Select(strQ))
                    {
                        if (dtUser_Name.Rows.Count > 0)
                            _Username = Convert.ToString(dtUser_Name.Rows[0]["NAME"]);
                    }

                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetUsername"));
            }
            return _Username;
        }
        /// <summary>
        /// check shared custom store by owner
        /// </summary>
        /// <param name="CustomStoreId"> CustomStoreId</param>
        /// <param name="UserNames"> user name</param>
        public static void SharedCustomStore_Owned(string CustomStoreId, string UserNames)
        {
            IControlHelper helper = new IControlHelper();
            DataLayer dal = new DataLayer();
            try
            {

                if (IsOwnerOfStoreGroup(CustomStoreId))
                {
                    String CurrDate = DateTime.Now.ToString();
                    string strQuery = "Insert into DIM_CUSTOM_STORES_SHARED(CUSTOM_STORE_GROUP_ID,USERNAME,DATE_SHARED) values ('" + CustomStoreId + "','" + UserNames + "','" + CurrDate + "')";

                    using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                    {
                        dataAccessProvider.ExecuteNonQuery(strQuery);

                        //CUSTOM_STORE_EXTENDED_UPDATE
                        dal.P_CUSTOM_STORE_EXTENDED_UPDATE(CustomStoreId.ToString());
                    }


                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > SharedCustomStore_Owned"));
            }
        }
        /// <summary>
        /// check Is Owner Of StoreGroup
        /// </summary>
        /// <param name="CustomStoreId"> CustomStoreId</param>
        /// <returns>bool</returns>
        public static bool IsOwnerOfStoreGroup(string CustomStoreId)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                string qry = "select * from DIM_CUSTOM_STORES_MASTER where CUSTOM_STORE_GROUP_ID = " + CustomStoreId + " and lower(UserName)=lower('" + HttpContext.Current.Session["Username"].ToString() + "')";
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    using (DataTable dt = dataAccessProvider.Select(qry))
                    {
                        if (dt.Rows.Count > 0)
                            return true;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > IsOwnerOfStoreGroup"));
            }
            return false;
        }
        /// <summary>
        /// check On Share Validate Store Number 
        /// </summary>
        /// <param name="CustomStoreId"> CustomStoreId</param>
        /// <returns>int32</returns>
        public static Int32 OnShareValidateStoreNumber(String _CustomStoreId)
        {
            Int32 _countInvalidStores = 0; //0 for all validated stores
            Int32 _countValidStores = 0;
            IControlHelper helper = new IControlHelper();
            try
            {
                String StoreNumbers = GetCustomStoreNumbers(_CustomStoreId);
                String[] CountStores = StoreNumbers.Split(',');

                string sqlQuery = "";
                sqlQuery = "Select count(Distinct STORE_NUMBER) STORE_NUMBER_COUNT  From DIM_STORES S ";

                if (string.IsNullOrEmpty(StoreNumbers))
                    return _countInvalidStores;
                else
                    sqlQuery += " Where STORE_NUMBER  in (" + StoreNumbers + ")";

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    using (DataTable dtCount = dataAccessProvider.Select(sqlQuery))
                    {
                        if (dtCount != null)
                            _countValidStores = Convert.ToInt32(dtCount.Rows[0]["STORE_NUMBER_COUNT"]);

                        _countInvalidStores = CountStores.Length - _countValidStores;
                    }
                }
                return _countInvalidStores;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > OnShareValidateStoreNumber"));
            }
            return _countInvalidStores;
        }
        /// <summary>
        /// Get CustomStore Numbers 
        /// </summary>
        /// <param name="CustomStoreId"> CustomStoreId</param>
        /// <returns>string</returns>
        public static String GetCustomStoreNumbers(String CustomStoreId)
        {
            String _Stores = ""; string _STORE_NUMBER = "";
            IControlHelper helper = new IControlHelper();
            try
            {
                string strQuery = "Select Distinct STORE_NUMBER from DIM_CUSTOM_STORES_DETAILS where CUSTOM_STORE_GROUP_ID = '" + CustomStoreId + "' group by STORE_NUMBER";
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    using (DataTable dtStoreNumbers = dataAccessProvider.Select(strQuery))
                    {
                        if (dtStoreNumbers.Rows.Count == 0)
                            return _Stores;


                        for (int i = 0; i < dtStoreNumbers.Rows.Count; i++)
                        {
                            string STORE_NUMBER = dtStoreNumbers.Rows[i]["STORE_NUMBER"].ToString();
                            _STORE_NUMBER += "'" + STORE_NUMBER + "',";
                        }
                    }

                }
                _Stores = _STORE_NUMBER.TrimEnd(',');

                return _Stores;
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetCustomStoreNumbers"));
            }

            return _Stores;
        }

        public static void CheckShareMessage(Int32 ReportId, Literal ltrMessage, Literal ltrSharedBy, System.Web.UI.HtmlControls.HtmlGenericControl divMessage)
        {
            IControlHelper helper = new IControlHelper();
            try
            {

                if (ReportId != 0)
                {
                    string sql = "Select ShareMessage, ReadStatus, ShareBy, Is_Deleted from SavedReports Where ReportId=" + ReportId;
                    using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                    {
                        using (DataTable dt = dataAccessProvider.Select(sql))
                        {
                            if (dt.Rows.Count > 0)
                            {
                                if (!String.IsNullOrEmpty(dt.Rows[0]["ShareMessage"].ToString()))
                                {
                                    ltrMessage.Text = dt.Rows[0]["ShareMessage"].ToString();
                                    string ShareByFullName = DataManager.GetFieldValueNetezza("SETUP_USERDATAACCESS where lower(username) = lower('" + dt.Rows[0]["ShareBy"].ToString() + "')", "distinct (Name || ' (' || UserName || ')')");
                                    ltrSharedBy.Text = ShareByFullName;
                                    divMessage.Visible = true;
                                }
                                else
                                {
                                    divMessage.Visible = false;
                                }
                                if (!String.IsNullOrEmpty(dt.Rows[0]["Is_Deleted"].ToString()))
                                {
                                    if (dt.Rows[0]["Is_Deleted"].ToString() == "True")
                                    {
                                        divMessage.Visible = false;
                                    }
                                }
                            }
                        }
                    }

                }
                else
                {
                    divMessage.Visible = false;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > CheckShareMessage"));
            }
        }
        /// <summary>
        /// InsertParameters for SalesDriverBasket
        /// </summary>
        public static bool InsertParameters_SalesDriverBasket(string EXEC_IDENTIFIER, string START_DATE, string END_DATE, string BANNER_ID_LIST, string SUB_BANNER_ID_LIST,
            string CATEGORY_ID_LIST, string SUB_CATEGORY_ID_LIST, string SEGMENT_ID_LIST,
            string BRAND_ID_LIST, string PRODUCT_SIZE_LIST, string PRIVATE_LABEL, string UPC_LIST, string HIERARCHY_NAME, string HIERARCHY_LEVEL1,
            string HIERARCHY_LEVEL2, string HIERARCHY_LEVEL3, long VendorNo, string UserType, string UserName, string ProfileName,
            string SalesBy, ref string strConnNetezza, ref string spcall)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                string SpcallSave = "";
                string SpExec = "";
                string qry = "";
                int IsBurst = 0;

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    SpExec = "exec P_SALES_DRIVERS_BASKETS_MASTER(";
                    SpExec += " '" + EXEC_IDENTIFIER + "',";
                    SpExec += " '" + Convert.ToDateTime(START_DATE).ToString("yyyy-MM-dd") + "',";
                    SpExec += " '" + Convert.ToDateTime(END_DATE).ToString("yyyy-MM-dd") + "',";

                    SpcallSave = "exec P_SALES_DRIVERS_BASKETS_MASTER(";
                    SpcallSave += " #~#'" + EXEC_IDENTIFIER + "',";
                    SpcallSave += " #~#'" + Convert.ToDateTime(START_DATE).ToString("yyyy-MM-dd") + "'#~#,";
                    SpcallSave += " #~#'" + Convert.ToDateTime(END_DATE).ToString("yyyy-MM-dd") + "'#~#,";

                    if (BANNER_ID_LIST != null)
                        qry += " '" + BANNER_ID_LIST + "',";
                    else
                        qry += " NULL,";

                    if (SUB_BANNER_ID_LIST != null)
                        qry += " '" + SUB_BANNER_ID_LIST + "',";
                    else
                        qry += " NULL,";

                    if (CATEGORY_ID_LIST != null)
                        qry += " '" + CATEGORY_ID_LIST + "',";
                    else
                        qry += " NULL,";

                    if (SUB_CATEGORY_ID_LIST != null)
                        qry += " '" + SUB_CATEGORY_ID_LIST + "',";
                    else
                        qry += " NULL,";

                    if (SEGMENT_ID_LIST != null)
                        qry += " '" + SEGMENT_ID_LIST + "',";
                    else
                        qry += " NULL,";

                    if (BRAND_ID_LIST != null)
                        qry += " '" + BRAND_ID_LIST + "',";
                    else
                        qry += " NULL,";

                    if (PRODUCT_SIZE_LIST != null)
                        qry += " '" + PRODUCT_SIZE_LIST + "',";
                    else
                        qry += " NULL,";

                    if (PRIVATE_LABEL != null)
                        qry += " '" + PRIVATE_LABEL + "',";
                    else
                        qry += " NULL,";

                    if (UPC_LIST != null)
                        qry += " '" + UPC_LIST + "',";
                    else
                        qry += " NULL,";

                    if (HIERARCHY_NAME != null)
                        qry += " '" + HIERARCHY_NAME.Replace("'", "''") + "',";
                    else
                        qry += " NULL,";

                    if (HIERARCHY_LEVEL1 != null)
                        qry += " '" + HIERARCHY_LEVEL1 + "',";
                    else
                        qry += " NULL,";

                    if (HIERARCHY_LEVEL2 != null)
                        qry += " '" + HIERARCHY_LEVEL2 + "',";
                    else
                        qry += " NULL,";

                    if (HIERARCHY_LEVEL3 != null)
                        qry += " '" + HIERARCHY_LEVEL3 + "',";
                    else
                        qry += " NULL,";

                    if (UserType != null)
                        qry += " '" + UserType + "',";
                    else
                        qry += " NULL,";

                    if (VendorNo != 0)
                        qry += " '" + VendorNo + "',";
                    else
                        qry += " NULL,";

                    if (UserName != null)
                        qry += " '" + UserName + "',";
                    else
                        qry += " NULL,";

                    if (!string.IsNullOrEmpty(ProfileName))
                        qry += " '" + ProfileName + "',";
                    else
                        qry += " NULL,";

                    if (SalesBy != null)
                        qry += " " + SalesBy + ",";
                    else
                        qry += " NULL,";

                    qry += IsBurst;

                    qry += " );";
                    SpExec += qry;
                    SpcallSave += qry;

                    dataAccessProvider.ExecuteScaler(SpExec, true);
                    spcall = SpcallSave;
                    strConnNetezza = helper.MainDynamicConnectionString;
                    return true;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > InsertParameters"));
                return false;
            }
        }
        /// <summary>
        /// InsertParameters for BusinessReviewDashboard
        /// </summary>
        public static bool InsertParameters_BusinessReviewDashboard(string EXEC_IDENTIFIER, string START_DATE, string TIMEFRAME, string BANNER_ID_LIST, string SUB_BANNER_ID_LIST,
            string CATEGORY_ID_LIST, string SUB_CATEGORY_ID_LIST, string SEGMENT_ID_LIST, string PRIVATE_LABEL, string UserType, string UserName, string ProfileName,
            string SalesBy, Int64 VendorNo, int IsBurst, ref string strConnNetezza, ref string spcall)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                string SpcallSave = "";
                string SpExec = "";
                string qry = "";
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    SpExec = "exec P_BUSINESS_REVIEW_DASHBOARD(";
                    SpcallSave = "exec P_BUSINESS_REVIEW_DASHBOARD(";

                    SpExec += " '" + EXEC_IDENTIFIER + "',";
                    SpcallSave += " #~#'" + EXEC_IDENTIFIER + "'#~#,";

                    SpExec += " '" + Convert.ToDateTime(START_DATE).ToString("yyyy-MM-dd") + "',";
                    SpcallSave += " #~#'" + Convert.ToDateTime(START_DATE).ToString("yyyy-MM-dd") + "'#~#,";

                    qry = " '" + TIMEFRAME + "',";

                    if (BANNER_ID_LIST != null)
                        qry += " '" + BANNER_ID_LIST + "',";
                    else
                        qry += " NULL,";

                    if (SUB_BANNER_ID_LIST != null)
                        qry += " '" + SUB_BANNER_ID_LIST + "',";
                    else
                        qry += " NULL,";

                    if (CATEGORY_ID_LIST != null)
                        qry += " '" + CATEGORY_ID_LIST + "',";
                    else
                        qry += " NULL,";

                    if (SUB_CATEGORY_ID_LIST != null)
                        qry += " '" + SUB_CATEGORY_ID_LIST + "',";
                    else
                        qry += " NULL,";

                    if (SEGMENT_ID_LIST != null)
                        qry += " '" + SEGMENT_ID_LIST + "',";
                    else
                        qry += " NULL,";

                    if (PRIVATE_LABEL != null)
                        qry += " '" + PRIVATE_LABEL + "',";
                    else
                        qry += " NULL,";

                    if (VendorNo != 0)
                        qry += " '" + VendorNo + "',";
                    else
                        qry += " NULL,";

                    if (UserType != null)
                        qry += " '" + UserType + "',";
                    else
                        qry += " NULL,";

                    if (UserName != null)
                        qry += " '" + UserName + "',";
                    else
                        qry += " NULL,";

                    if (!string.IsNullOrEmpty(ProfileName))
                        qry += " '" + ProfileName + "',";
                    else
                        qry += " NULL,";

                    if (SalesBy != null)
                        qry += " " + SalesBy + ",";
                    else
                        qry += " NULL,";

                    qry += IsBurst;

                    qry += " );";
                    SpExec += qry;
                    SpcallSave += qry;

                    dataAccessProvider.ExecuteScaler(SpExec, true);
                    spcall = SpcallSave;
                    strConnNetezza = helper.MainDynamicConnectionString;
                    return true;
                }


            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > InsertParameters_BusinessSalesReport"));
                return false;
            }
        }


        /// <summary>
        /// get application report search result 
        /// </summary>
        /// <param name="userid"> userid</param>
        /// <param name="portalName"> portalName</param>
        /// <param name="searchText"> searchText</param>
        /// <returns>DataTable.</returns>
        public static DataTable GetAppReportSearch(string userid, string portalName, string searchText)
        {
            IControlHelper helper = new IControlHelper();
            DataTable DtAll = new DataTable();
            try
            {
                List<IDataParameter> parameters = new List<IDataParameter>();

                IDataParameter paramuserid = new SqlParameter("@LoginID", SqlDbType.VarChar);
                paramuserid.Value = userid;
                parameters.Add(paramuserid);

                IDataParameter paramportalName = new SqlParameter("@PortalName", SqlDbType.VarChar);
                paramportalName.Value = portalName;
                parameters.Add(paramportalName);

                IDataParameter paramsearchText = new SqlParameter("@SearchText", SqlDbType.VarChar);
                paramsearchText.Value = searchText;
                parameters.Add(paramsearchText);
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    DtAll = dataAccessProvider.ExecuteProcedure("usp_SearchReports", parameters, true);
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "Datalayer.cs > GetAppReportSearch"));
            }
            return DtAll;
        }
        /// <summary>
        /// get trainning video detail 
        /// </summary>
        /// <param name="videoId"> videoId</param>
        /// <returns>DataTable.</returns>
        public static DataTable GetTrainningVideoDetail(string videoId)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dt = new DataTable();
            try
            {
                string strSql = "select * from Trainingvideos where VideoId=" + videoId.ToString();
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dt = dataAccessProvider.Select(strSql);
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetTrainningVideoDetail"));
            }
            return dt;
        }
        /// <summary>
        /// get trainning video list 
        /// </summary>
        /// <returns>DataTable.</returns>
        public static DataTable GetTrainningVideoList()
        {
            IControlHelper helper = new IControlHelper();
            DataTable dt = new DataTable();
            try
            {
                string strSql = "select * from Trainingvideos";
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dt = dataAccessProvider.Select(strSql);
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GetTrainningVideoList"));
            }
            return dt;
        }
        /// <summary>
        /// check current user account 
        /// </summary>
        /// <param name="UserId"> UserId</param>
        /// <param name="UserType"> UserType</param>
        /// <returns>bool.</returns>
        public static bool CheckCurrentUserAccount(string UserId)
        {
            bool blResult = false;
            IControlHelper helper = new IControlHelper();
            try
            {
                string strUserRoleId = GetCurrentUserRoleId(UserId);

                if (strUserRoleId == "8" || strUserRoleId == "9")
                {
                    blResult = true;
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > CheckCurrentUserAccount:"));
            }
            return blResult;
        }


        //NEW IMPLEMENTATION
        public static string GroupNameByGroupId(int GroupId)
        {
            IControlHelper helper = new IControlHelper();
            string GroupName = "";
            try
            {
                string strQuery = helper.GetResourceString(AppConstants.GET_GROUPNAME_ID_PORTAL_BY_GROUPID, DataSourceConnection.SQLServerConnection.ToString()).Replace("@GroupId", Convert.ToString(GroupId));
                GroupName = GetFieldValue(strQuery, "GroupName");
            }

            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GroupNameByGroupId"));
            }
            return GroupName;

        }
        public static string GroupTypeIdByGroupId(int GroupId)
        {
            IControlHelper helper = new IControlHelper();
            string GroupTypeId = "";
            try
            {
                string strQuery = helper.GetResourceString(AppConstants.GET_GROUPNAME_ID_PORTAL_BY_GROUPID, DataSourceConnection.SQLServerConnection.ToString()).Replace("@GroupId", Convert.ToString(GroupId));
                GroupTypeId = GetFieldValue(strQuery, "GroupTypeId");
            }

            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > GroupTypeIdByGroupId"));
            }
            return GroupTypeId;

        }
        public static string PortalNameByGroupId(int GroupId)
        {
            IControlHelper helper = new IControlHelper();
            string PortalName = "";
            try
            {
                string strQuery = helper.GetResourceString(AppConstants.GET_GROUPNAME_ID_PORTAL_BY_GROUPID, DataSourceConnection.SQLServerConnection.ToString()).Replace("@GroupId", Convert.ToString(GroupId));
                PortalName = GetFieldValue(strQuery, "PortalName");
            }

            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > PortalNameByGroupId"));
            }
            return PortalName;

        }

        public static DataTable FillUserGroups()
        {
            IControlHelper helper = new IControlHelper();
            DataTable dt = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string strQuery = helper.GetResourceString(AppConstants.GET_GROUP_DETAILS, DataSourceConnection.SQLServerConnection.ToString());
                    dt = dataAccessProvider.Select(strQuery);
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > FillUserRoles"));
            }
            return dt;
        }

        public static DataTable MenuList(string Group, string Vertical)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dt = new DataTable();
            try
            {
                List<IDataParameter> parameters = new List<IDataParameter>();
                IDataParameter paramGroupId = new SqlParameter("@GroupId", SqlDbType.NVarChar);
                paramGroupId.Value = Group;
                parameters.Add(paramGroupId);


                IDataParameter paramVerticalId = new SqlParameter("@VerticalId", SqlDbType.VarChar);
                paramVerticalId.Value = Vertical;
                parameters.Add(paramVerticalId);

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dt = dataAccessProvider.ExecuteProcedure("usp_GetMenueList", parameters);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > MenuList"));
            }
            return dt;
        }

        public static DataTable AssignMenuList(string Group, string Vertical)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                List<IDataParameter> parameters = new List<IDataParameter>();
                IDataParameter paramGroupId = new SqlParameter("@GroupId", SqlDbType.NVarChar);
                paramGroupId.Value = Group;
                parameters.Add(paramGroupId);


                IDataParameter paramVerticalId = new SqlParameter("@VerticalId", SqlDbType.VarChar);
                paramVerticalId.Value = Vertical;
                parameters.Add(paramVerticalId);

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dtResult = dataAccessProvider.ExecuteProcedure("usp_GetAssignMenus", parameters);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > AssignMenuList"));
            }
            return dtResult;
        }

        public static int GetParentID(int MenuId)
        {
            IControlHelper helper = new IControlHelper();
            //get the parent id from table for given menuid
            int strParentID = 0;
            DataTable dt = new DataTable();

            using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
            {
                string strQuery = helper.GetResourceString(AppConstants.GET_PARENT_ID_BY_MENUID, DataSourceConnection.SQLServerConnection.ToString()).Replace("@MenuId", Convert.ToString(MenuId));
                dt = dataAccessProvider.Select(strQuery);
            }
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["ParentMenuId"].ToString() == "" || dt.Rows[0]["ParentMenuId"].ToString() == null)
                {
                    strParentID = 0;
                }
                else
                {
                    strParentID = Convert.ToInt32(dt.Rows[0]["ParentMenuId"].ToString());
                }
            }
            return strParentID;
        }

        public static void DeleteListItem(string GroupId, string VarticalId)
        {
            if (GroupId != "" && GroupId != null && VarticalId != "" && VarticalId != null)
            {
                IControlHelper helper = new IControlHelper();
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string strQuery = helper.GetResourceString(AppConstants.DELETE_GROUPMENUS, DataSourceConnection.SQLServerConnection.ToString()).Replace("@GroupId", GroupId).Replace("@VerticalId", VarticalId);
                    dataAccessProvider.ExecuteNonQuery(strQuery);
                }
            }
        }

        public static void AssignMenusToGroup(int GroupId, int MenuId, int ParentId, int Order)
        {
            IControlHelper helper = new IControlHelper();
            try
            {

                List<IDataParameter> parameters = new List<IDataParameter>();
                IDataParameter paramGroupId = new SqlParameter("@GroupId", SqlDbType.NVarChar);
                paramGroupId.Value = GroupId;
                parameters.Add(paramGroupId);

                IDataParameter paramMenuId = new SqlParameter("@MenuID", SqlDbType.NVarChar);
                paramMenuId.Value = MenuId;
                parameters.Add(paramMenuId);

                IDataParameter paramParentId = new SqlParameter("@ParentID", SqlDbType.NVarChar);
                paramParentId.Value = ParentId;
                parameters.Add(paramParentId);

                IDataParameter paramOrder = new SqlParameter("@OrderID", SqlDbType.NVarChar);
                paramOrder.Value = Order;
                parameters.Add(paramOrder);

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dataAccessProvider.ExecuteProcedure("usp_AssignMenusToGroup", parameters);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > AssignMenusToGroup"));
            }
        }

        public static bool CheckAlreadyAssignMenusToGroup(int GroupId, int MenuId)
        {
            bool alreadyExist = false;
            IControlHelper helper = new IControlHelper();
            DataTable dt = new DataTable();
            try
            {                
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    string strQuery = helper.GetResourceString(AppConstants.GET_ALREADY_ASSIGNED_MENU_TO_GROUP, DataSourceConnection.SQLServerConnection.ToString()).Replace("@MenuId", Convert.ToString(MenuId)).Replace("@GroupId", Convert.ToString(GroupId));
                    dt=dataAccessProvider.Select(strQuery);
                    if (dt.Rows.Count > 0)
                    {
                        alreadyExist = true;
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > CheckAlreadyAssignMenusToGroup"));
            }
            return alreadyExist;
        }

        public static void AssignMultipleUserGroups(String UserId, int GroupId)
        {
            IControlHelper helper = new IControlHelper();

            try
            {
                List<IDataParameter> parameters = new List<IDataParameter>();
                IDataParameter paramUserId = new SqlParameter("@UserId", SqlDbType.NVarChar);
                paramUserId.Value = UserId;
                parameters.Add(paramUserId);

                IDataParameter paramGroupId = new SqlParameter("@GroupId", SqlDbType.VarChar);
                paramGroupId.Value = GroupId;
                parameters.Add(paramGroupId);

                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dataAccessProvider.ExecuteProcedure("usp_SaveAssignUserGroups", parameters);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > AssignMultipleUserGroups"));
            }
        }
        public static DataTable ExecuteQuery(string strQuery)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dt = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dt = dataAccessProvider.Select(strQuery);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > ExecuteQuery"));
            }
            return dt;
        }

        public static void ExecuteNonQuery(string query)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dataAccessProvider.ExecuteNonQuery(query);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > ExecuteNonQuery"));
            }
        }

        public static Boolean AddGroup(string GroupId, string GroupName, string GroupTypeId, string ActiveStatus, string UserId, string PortalName)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                List<IDataParameter> parameters = new List<IDataParameter>();
                IDataParameter paramGroupId = new SqlParameter("@GroupId", SqlDbType.NVarChar);
                paramGroupId.Value = GroupId;
                parameters.Add(paramGroupId);

                IDataParameter paramGroupName = new SqlParameter("@GroupName", SqlDbType.NVarChar);
                paramGroupName.Value = GroupName;
                parameters.Add(paramGroupName);

                IDataParameter paramGroupTypeId = new SqlParameter("@GroupTypeId", SqlDbType.NVarChar);
                paramGroupTypeId.Value = GroupTypeId;
                parameters.Add(paramGroupTypeId);

                IDataParameter paramActiveStatus = new SqlParameter("@ActiveStatus", SqlDbType.NVarChar);
                paramActiveStatus.Value = ActiveStatus;
                parameters.Add(paramActiveStatus);

                IDataParameter paramUserId = new SqlParameter("@UserId", SqlDbType.NVarChar);
                paramUserId.Value = UserId;
                parameters.Add(paramUserId);

                IDataParameter paramPortalName = new SqlParameter("@PortalName", SqlDbType.NVarChar);
                paramPortalName.Value = PortalName;
                parameters.Add(paramPortalName);
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                {
                    dataAccessProvider.ExecuteProcedure("usp_AddUserGroup", parameters);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > AddGroup"));
            }
            return true;
        }
        #region ExecuteQueryForNetezza
        public static DataTable ExecuteQueryForNetezza(string strQuery)
        {
            IControlHelper helper = new IControlHelper();
            DataTable dtResult = new DataTable();
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    dtResult = dataAccessProvider.Select(strQuery);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > ExecuteQueryForNetezza"));
            }
            return dtResult;
        }
        public static int ExecuteDeleteQueryForNetezza(string strQuery)
        {
            IControlHelper helper = new IControlHelper();
            int RowsAffected = 0;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    RowsAffected = dataAccessProvider.Delete(strQuery);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > ExecuteDeleteQueryForNetezza"));
            }
            return RowsAffected;
        }
        public static int ExecuteInsertQueryForNetezza(string strQuery)
        {
            IControlHelper helper = new IControlHelper();
            int RowsAffected = 0;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    RowsAffected = dataAccessProvider.Delete(strQuery);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > ExecuteInsertQueryForNetezza"));
            }
            return RowsAffected;
        }

        public static int ExecuteUpdateQueryForNetezza(string strQuery)
        {
            IControlHelper helper = new IControlHelper();
            int RowsAffected = 0;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    RowsAffected = dataAccessProvider.Update(strQuery);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManager.cs > ExecuteInsertQueryForNetezza"));
            }
            return RowsAffected;
        }
        #endregion


        public static string GetSegmentIdBySegmentCategorySubCategory(string Segment, string Category, string SubCategory)
        {
            IControlHelper helper = new IControlHelper();
            String segementId = "";
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    string strQuery = helper.GetResourceString(AppConstants.GET_SEGMENTID_BY_SEGMENT_CATEGORY).Replace("@Segment", Segment).Replace("@Category", Category).Replace("@SubCategory", SubCategory);
                    var Result = dataAccessProvider.ExecuteScaler(strQuery);
                    if (Result != null)
                    {
                        segementId = Result.ToString();
                    }
                }
            }

            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataManger.cs > GetSegmentIdBySegmentCategorySubCategory:"));

            }
            return segementId;
        }

        #region ImportUser Methods
        public static string EncryptPassword(string strPassowrd)
        {
            string strEncyptedPassword = "";

            //EncPwd.SHA EncPassword = new EncPwd.SHA();
            //strEncyptedPassword = EncPassword.SHA1(strPassowrd);
            return strEncyptedPassword;
        }
        public static string CreateUserLogin(string UserName, string Name, string Role)
        {
            IControlHelper helper = new IControlHelper();
            string OwnerEntityId = "";
            try
            {
                OwnerEntityId = DataManager.GetFieldValue("Logins where Login='" + UserName.Replace("'", "''") + "'", "OwnerEntityId");

                string FirstName = Name.Split(' ')[0].ToString();
                string LastName = "";

                if (Name.Split(' ').Length > 1)
                    LastName = Name.Split(' ')[1].ToString();

                if (Name.Split(' ').Length > 2)
                    LastName = LastName + ' ' + Name.Split(' ')[2].ToString();

                if (OwnerEntityId == "")
                {
                    using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.SQLConnectionString, DatabaseType.MSSql))
                    {
                        string NewUserPassword = UserName.Substring(0, 4) + "!@#";
                        List<IDataParameter> parameters = new List<IDataParameter>();
                        IDataParameter CurrentUserId = new SqlParameter("@CurrentUserId", SqlDbType.VarChar);
                        CurrentUserId.Value = 62464;
                        parameters.Add(CurrentUserId);

                        IDataParameter paramFirstName = new SqlParameter("@FirstName", SqlDbType.VarChar);
                        paramFirstName.Value = FirstName;
                        parameters.Add(paramFirstName);

                        IDataParameter paramLastName = new SqlParameter("@LastName", SqlDbType.VarChar);
                        paramLastName.Value = LastName;
                        parameters.Add(paramLastName);

                        IDataParameter paramUserName = new SqlParameter("@LoginName", SqlDbType.VarChar);
                        paramUserName.Value = UserName;
                        parameters.Add(paramUserName);

                        IDataParameter paramNewUserPassword = new SqlParameter("@LoginPassword", SqlDbType.VarChar);
                        paramNewUserPassword.Value = NewUserPassword;
                        parameters.Add(paramNewUserPassword);

                        IDataParameter paramPasswordSalt = new SqlParameter("@PasswordSalt", SqlDbType.VarChar);
                        paramPasswordSalt.Value = EncryptPassword(NewUserPassword);
                        parameters.Add(paramPasswordSalt);

                        IDataParameter paramAttributeValue = new SqlParameter("@AttributeValue", SqlDbType.VarChar);
                        paramAttributeValue.Value = 40393;
                        parameters.Add(paramAttributeValue);

                        IDataParameter paramAttributeId = new SqlParameter("@AttributeId", SqlDbType.VarChar);
                        paramAttributeId.Value = 17;
                        parameters.Add(paramAttributeId);

                        IDataParameter paramBannerAccess = new SqlParameter("@BannerAccess", SqlDbType.VarChar);
                        paramBannerAccess.Value = "FULL";
                        parameters.Add(paramBannerAccess);

                        IDataParameter paramEditRights = new SqlParameter("@EditRights", SqlDbType.VarChar);
                        paramEditRights.Value = "EDIT";
                        parameters.Add(paramEditRights);

                        IDataParameter paramentitytypeid = new SqlParameter("@entitytypeid", SqlDbType.VarChar);
                        paramentitytypeid.Value = Role;
                        parameters.Add(paramentitytypeid);

                        dataAccessProvider.ExecuteProcedure("usp_AddNewUser", parameters);

                        string strQuery = helper.GetResourceString(AppConstants.GET_LAST_OWNER_ENTITYID, DataSourceConnection.SQLServerConnection.ToString());
                        OwnerEntityId = dataAccessProvider.ExecuteScaler(strQuery).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > CreateUserLogin:"));
            }
            return OwnerEntityId;
        }

        public static void ClearExistingProfiles(string UserId)
        {
            IControlHelper helper = new IControlHelper();
            try
            {
                string SQLQuery = "";
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    SQLQuery = helper.GetResourceString(AppConstants.DELETE_USER_PROFILE).Replace("@UserId", UserId);
                    dataAccessProvider.ExecuteNonQuery(SQLQuery);

                    SQLQuery = helper.GetResourceString(AppConstants.DELETE_USER_PROFILE_VENDOR_ACCESS);
                    dataAccessProvider.ExecuteNonQuery(SQLQuery);

                    SQLQuery = helper.GetResourceString(AppConstants.DELETE_USER_PROFILE_BANNER_ACCESS);
                    dataAccessProvider.ExecuteNonQuery(SQLQuery);

                    SQLQuery = helper.GetResourceString(AppConstants.DELETE_USER_PROFILE_STORE_ACCESS);
                    dataAccessProvider.ExecuteNonQuery(SQLQuery);

                    SQLQuery = helper.GetResourceString(AppConstants.DELETE_USER_PROFILE_DEPARTMENT_ACCESS);
                    dataAccessProvider.ExecuteNonQuery(SQLQuery);

                    SQLQuery = helper.GetResourceString(AppConstants.DELETE_USER_PROFILE_SUBDEPARTMENT_ACCESS);
                    dataAccessProvider.ExecuteNonQuery(SQLQuery);

                    SQLQuery = helper.GetResourceString(AppConstants.DELETE_USER_PROFILE_CATEGORY_ACCESS);
                    dataAccessProvider.ExecuteNonQuery(SQLQuery);

                    SQLQuery = helper.GetResourceString(AppConstants.DELETE_USER_PROFILE_SUBCATEGORY_ACCESS);
                    dataAccessProvider.ExecuteNonQuery(SQLQuery);

                    SQLQuery = helper.GetResourceString(AppConstants.DELETE_USER_PROFILE_SEGMENT_ACCESS);
                    dataAccessProvider.ExecuteNonQuery(SQLQuery);

                    SQLQuery = helper.GetResourceString(AppConstants.DELETE_USER_PROFILE_PRODUCT_ACCESS);
                    dataAccessProvider.ExecuteNonQuery(SQLQuery);

                    SQLQuery = helper.GetResourceString(AppConstants.DELETE_USER_PROFILE_CHAIN_ACCESS);
                    dataAccessProvider.ExecuteNonQuery(SQLQuery);

                    SQLQuery = helper.GetResourceString(AppConstants.DELETE_USER_DATA_ACCESS).Replace("@UserId", UserId);
                    dataAccessProvider.ExecuteNonQuery(SQLQuery);
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > ClearExistingProfiles:"));
            }
        }

        public static void InsertVendorsData(string VendorNames, string ProfileId)
        {
            IControlHelper helper = new IControlHelper();
            string strQuery;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    if (VendorNames != "")
                    {
                        string[] VendorsList = VendorNames.Trim().Split(',');
                        string RoleName, AccessDescription;

                        strQuery = helper.GetResourceString(AppConstants.GET_ROLE_BY_PROFILE).Replace("@ProfileId", ProfileId.ToString());
                        RoleName = dataAccessProvider.Select(strQuery).Rows[0][0].ToString();

                        strQuery = helper.GetResourceString(AppConstants.GET_ACCESS_DESCRIPTION_BY_PROFILE).Replace("@ProfileId", ProfileId.ToString());
                        AccessDescription = dataAccessProvider.Select(strQuery).Rows[0][0].ToString();

                        if (RoleName.ToUpper() == "PurePartner".ToUpper() || AccessDescription.ToUpper() == "Banner Level".ToUpper())
                        {
                            strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_VENDOR_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@VendorIdSource", "-1");
                            dataAccessProvider.Insert(strQuery);
                        }
                        else
                        {
                            foreach (string v in VendorsList)
                            {
                                strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_VENDOR_ACCESS_WITH_VENDOR).Replace("@ProfileId", ProfileId.ToString()).Replace("@VendorIdSrc", v.Trim().ToUpper());
                                dataAccessProvider.Insert(strQuery);

                            }
                        }
                    }
                    else
                    {
                        strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_VENDOR_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@VendorIdSource", "-1");
                        dataAccessProvider.Insert(strQuery);
                    }
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertVendorsData:"));
            }
        }

        public static void InsertBannersData(string BannerNames, string ProfileId)
        {
            IControlHelper helper = new IControlHelper();
            string strQuery;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    if (BannerNames != "")
                    {
                        string[] BannersList = BannerNames.Trim().Split(',');
                        foreach (string b in BannersList)
                        {
                            strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_BANNER_ACCESS_WITH_BANNER).Replace("@ProfileId", ProfileId.ToString()).Replace("@TopBanner", b.Trim().ToUpper());
                            dataAccessProvider.Insert(strQuery);
                        }
                    }
                    else
                    {
                        strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_BANNER_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@Banner", "-1");
                        dataAccessProvider.Insert(strQuery);
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertBannersData:"));
            }
        }

        public static void InsertStoresData(string StoreNumbers, string ProfileId)
        {
            IControlHelper helper = new IControlHelper();
            string strQuery;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    if (StoreNumbers != "")
                    {
                        string[] StoresList = StoreNumbers.Trim().Split(',');
                        string RoleName, AccessDescription;
                        RoleName = DataManager.GetFieldValueNetezza("USERDATAACCESS Where ProfileID=" + ProfileId + " limit 1", "Role");
                        AccessDescription = DataManager.GetFieldValueNetezza("USERDATAACCESS Where ProfileID=" + ProfileId + " limit 1", "AccessDescription");

                        if (RoleName.ToUpper() == "PurePartner".ToUpper() || AccessDescription.ToUpper() == "Vendor".ToUpper())
                        {
                            strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_STORE_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@StoreId", "-1");
                            dataAccessProvider.Insert(strQuery);
                        }
                        else
                        {
                            foreach (string s in StoresList)
                            {
                                strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_STORE_ACCESS_WITH_STORE).Replace("@ProfileId", ProfileId.ToString()).Replace("@StoreNumber", s.Trim().ToUpper());
                                dataAccessProvider.Insert(strQuery);
                            }
                        }
                    }
                    else
                    {
                        strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_STORE_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@StoreId", "-1");
                        dataAccessProvider.Insert(strQuery);
                    }
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertStoresData:"));
            }
        }

        public static void InsertDepartmentsData(string deptNames, string ProfileId)
        {
            IControlHelper helper = new IControlHelper();
            string strQuery;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    if (deptNames != "")
                    {
                        string[] DeptList = deptNames.Trim().Split('|');
                        foreach (string d in DeptList)
                        {
                            strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_DEPARTMENT_ACCESS_WITH_DEPARTMENT).Replace("@ProfileId", ProfileId.ToString()).Replace("@Department", d.Trim().ToUpper());
                            dataAccessProvider.Insert(strQuery);
                        }
                    }
                    else
                    {
                        strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_DEPARTMENT_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@DepartmentId", "-1");
                        dataAccessProvider.Insert(strQuery);
                    }
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertDepartmentsData:"));
            }
        }

        public static void InsertSubDepartmentsData(string SubDeptNames, string ProfileId)
        {
            IControlHelper helper = new IControlHelper();
            string strQuery;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    if (SubDeptNames != "")
                    {
                        string[] SubDeptList = SubDeptNames.Trim().Split('|');
                        foreach (string d in SubDeptList)
                        {
                            strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_SUBDEPARTMENT_ACCESS_WITH_SUBDEPARTMENT).Replace("@ProfileId", ProfileId.ToString()).Replace("@SubDepartment", d.Trim().ToUpper());
                            dataAccessProvider.Insert(strQuery);
                        }
                    }
                    else
                    {
                        strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_SUBDEPARTMENT_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@SubDepartmentId", "-1");
                        dataAccessProvider.Insert(strQuery);
                    }
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertSubDepartmentsData:"));
            }
        }

        public static void InsertCategoriesData(string CatNames, string ProfileId)
        {
            IControlHelper helper = new IControlHelper();
            string strQuery;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    if (CatNames != "")
                    {
                        string[] CatList = CatNames.Trim().Split('|');
                        foreach (string c in CatList)
                        {
                            strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_CATEGORY_ACCESS_WITH_CATEGORY).Replace("@ProfileId", ProfileId.ToString()).Replace("@Category", c.Trim().ToUpper());
                            dataAccessProvider.Insert(strQuery);
                        }
                    }
                    else
                    {
                        strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_CATEGORY_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@Category", "-1");
                        dataAccessProvider.Insert(strQuery);
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertCategoriesData:"));
            }
        }

        public static void InsertSubCategoriesData(string SubCatNames, string ProfileId)
        {
            IControlHelper helper = new IControlHelper();
            string strQuery;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    if (SubCatNames != "")
                    {
                        string[] SubCatList = SubCatNames.Trim().Split('|');
                        foreach (string c in SubCatList)
                        {
                            strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_SUBCATEGORY_ACCESS_WITH_SUBCATEGORY).Replace("@ProfileId", ProfileId.ToString()).Replace("@SubCategory", c.Trim().ToUpper());
                            dataAccessProvider.Insert(strQuery);
                        }
                    }
                    else
                    {
                        strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_SUBCATEGORY_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@SubCategory", "-1");
                        dataAccessProvider.Insert(strQuery);
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertSubCategoriesData:"));
            }
        }

        public static void InsertSegmentsData(string SegmentNames, string CategoryName, string SubCategoryName, string ProfileId)
        {
            IControlHelper helper = new IControlHelper();
            string strQuery;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    if (SegmentNames != "")
                    {
                        string[] SegmentList = SegmentNames.Trim().Split('|');
                        foreach (string c in SegmentList)
                        {
                            strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_SEGMENT_ACCESS_WITH_CATEGORY_AND_SUBCATEGORY).Replace("@ProfileId", ProfileId.ToString()).Replace("@Segment", c.Trim().ToUpper()).Replace("@CategoryName", CategoryName.Trim().ToUpper()).Replace("@SubCategoryName", SubCategoryName.Trim().ToUpper());
                            dataAccessProvider.Insert(strQuery);
                        }
                    }
                    else
                    {
                        strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_SEGMENT_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@SegmentId", "-1");
                        dataAccessProvider.Insert(strQuery);
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertSegmentsData:"));
            }
        }

        public static void InsertBrandsData(string BrandNames, string ProfileId)
        {
            IControlHelper helper = new IControlHelper();
            string strQuery;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    if (BrandNames != "")
                    {
                        string[] BrandNamesList = BrandNames.Trim().Split('|');
                        foreach (string c in BrandNamesList)
                        {
                            strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_BRAND_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@BrandId", c.Trim().ToUpper().Replace("'", "''"));
                            dataAccessProvider.Insert(strQuery);
                        }
                    }
                    else
                    {
                        strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_BRAND_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@BrandId", "-1");
                        dataAccessProvider.Insert(strQuery);
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertBrandsData:"));
            }
        }

        public static void InsertUPCsData(string UPCs, string ProfileId)
        {
            IControlHelper helper = new IControlHelper();
            string strQuery;
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    if (UPCs != "")
                    {
                        string[] UPCsList = UPCs.Trim().Split(',');

                        string RoleName, AccessDescription;
                        RoleName = DataManager.GetFieldValueNetezza("USERDATAACCESS Where ProfileID=" + ProfileId + " limit 1", "Role");
                        AccessDescription = DataManager.GetFieldValueNetezza("USERDATAACCESS Where ProfileID=" + ProfileId + " limit 1", "AccessDescription");

                        if (RoleName.ToUpper() == "Banner Level".ToUpper() || AccessDescription.ToUpper() == "Vendor".ToUpper())
                        {
                            strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_PRODUCT_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@ProductId", "-1");
                            dataAccessProvider.Insert(strQuery);
                        }
                        else
                        {
                            foreach (string p in UPCsList)
                            {
                                strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_PRODUCT_ACCESS_WITH_UPC).Replace("@ProfileId", ProfileId.ToString()).Replace("@Upc", p.Trim().ToUpper());
                                dataAccessProvider.Insert(strQuery);
                            }
                        }
                    }
                    else
                    {
                        strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_PRODUCT_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@ProductId", "-1");
                        dataAccessProvider.Insert(strQuery);
                    }
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertUPCsData:"));
            }
        }

        public static void InsertChainsData(string ChainName, string ProfileId)
        {
            IControlHelper helper = new IControlHelper();
            string strQuery;
            try
            {
                if (ChainName != "")
                {
                    string[] ChainsList = ChainName.Trim().Split(',');
                    foreach (string v in ChainsList)
                    {
                        using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                        {
                            strQuery = helper.GetResourceString(AppConstants.INSERT_USERPROFILE_CHAIN_ACCESS).Replace("@ProfileId", ProfileId.ToString()).Replace("@ChainId", v.Trim());
                            dataAccessProvider.Insert(strQuery);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > InsertVendorsData:"));
            }
        }

        public static string GetRecordId(string ValidateFor, string strVal)
        {
            IControlHelper helper = new IControlHelper();
            string recordId = null;
            string strQuery = "";
            try
            {
                using (DataAccessProvider dataAccessProvider = new DataAccessProvider(helper.MainDynamicConnectionString, helper.GetDbType()))
                {
                    switch (ValidateFor)
                    {
                        case "Vendors":
                            strQuery = helper.GetResourceString(AppConstants.GET_RECORD_ID).Replace("@FieldName", "Vendor_Id_Src").Replace("@tableName", "SETUP_VENDORS_COMBINED").Replace("@ParamCondition", "Vendor_Id_Src").Replace("@strValue", strVal);
                            break;
                        case "Banners":
                            strQuery = helper.GetResourceString(AppConstants.GET_RECORD_ID).Replace("@FieldName", "TOP_BANNER").Replace("@tableName", "DIM_Stores").Replace("@ParamCondition", "TOP_BANNER").Replace("@strValue", strVal);
                            break;
                        case "Stores":
                            strQuery = helper.GetResourceString(AppConstants.GET_RECORD_ID).Replace("@FieldName", "Store_ID").Replace("@tableName", "DIM_Stores").Replace("@ParamCondition", "Store_Number").Replace("@strValue", strVal);
                            break;
                        case "Departments":
                            strQuery = helper.GetResourceString(AppConstants.GET_RECORD_ID).Replace("@FieldName", "Department_ID").Replace("@tableName", "DIM_PRODUCTS").Replace("@ParamCondition", "Department").Replace("@strValue", strVal);
                            break;
                        case "SubDepartments":
                            strQuery = helper.GetResourceString(AppConstants.GET_RECORD_ID).Replace("@FieldName", "Sub_Department_ID").Replace("@tableName", "DIM_PRODUCTS").Replace("@ParamCondition", "Sub_Department").Replace("@strValue", strVal);
                            break;
                        case "Categories":
                            strQuery = helper.GetResourceString(AppConstants.GET_RECORD_ID).Replace("@FieldName", "Category_ID").Replace("@tableName", "DIM_PRODUCTS").Replace("@ParamCondition", "Category").Replace("@strValue", strVal);
                            break;
                        case "SubCategories":
                            strQuery = helper.GetResourceString(AppConstants.GET_RECORD_ID).Replace("@FieldName", "Sub_Category_ID").Replace("@tableName", "DIM_PRODUCTS").Replace("@ParamCondition", "Sub_Category").Replace("@strValue", strVal);
                            break;
                        case "Segments":
                            strQuery = helper.GetResourceString(AppConstants.GET_RECORD_ID).Replace("@FieldName", "Segment_ID").Replace("@tableName", "DIM_PRODUCTS").Replace("@ParamCondition", "Segment").Replace("@strValue", strVal);
                            break;
                        case "Brands":
                            strQuery = helper.GetResourceString(AppConstants.GET_RECORD_ID).Replace("@FieldName", "MFR_Brand_ID").Replace("@tableName", "DIM_PRODUCTS").Replace("@ParamCondition", "Brand_Name").Replace("@strValue", strVal);
                            break;
                        case "Upc":
                            strQuery = helper.GetResourceString(AppConstants.GET_RECORD_ID).Replace("@FieldName", "Product_ID").Replace("@tableName", "DIM_PRODUCTS").Replace("@ParamCondition", "UPC").Replace("@strValue", strVal);
                            break;
                    }
                    recordId = dataAccessProvider.ExecuteScaler(strQuery).ToString();
                }
            }
            catch (Exception ex)
            {
                helper.WriteLog(string.Format("{0}" + ex.Message, "DataLayer.cs > GetRecordId:"));
            }
            return recordId;
        }
        #endregion
    }
}