﻿using System.Configuration;

namespace iControlDataLayer
{
    /// <summary>
    /// PARTIAL CLASS USED FOR DATABASE MIGRATION
    /// </summary>
    public partial class AppConstants
    {
        /// <summary>
        /// This file contains all const. with name of all database objects => Strore proceedures
        /// </summary>
        public const string P_MARKET_BASKET_REQUEST_TEXTVALUES_INSERT = "P_MARKET_BASKET_REQUEST_TEXTVALUES_INSERT";
        public const string P_MARKET_BASKET_REQUEST_TEXTVALUES_SELECT = "P_MARKET_BASKET_REQUEST_TEXTVALUES_SELECT";

        public const string REPLACE_BLACKLIST_CHARACTERS_UPDATE = "REPLACE_BLACKLIST_CHARACTERS_UPDATE";

        public const string SP_CUSTOM_HIERARCHY_REPLACE_BLACKLIST_CHARACTERS_Query1 = "SP_CUSTOM_HIERARCHY_REPLACE_BLACKLIST_CHARACTERS_Query1";
        public const string SP_CUSTOM_STORE_REPLACE_BLACKLIST_CHARACTERS_Query1 = "SP_CUSTOM_STORE_REPLACE_BLACKLIST_CHARACTERS_Query1";

        public const string SP_DELETE_MARKET_BASKET_DELETE1 = "SP_DELETE_MARKET_BASKET_DELETE1";
        public const string SP_DELETE_MARKET_BASKET_DELETE2 = "SP_DELETE_MARKET_BASKET_DELETE2";
        public const string SP_DELETE_MARKET_BASKET_DELETE3 = "SP_DELETE_MARKET_BASKET_DELETE3";
        public const string SP_DELETE_MARKET_BASKET_DELETE4 = "SP_DELETE_MARKET_BASKET_DELETE4";

        public const string SP_DELETE_PROMOTIONS_DELETE = "SP_DELETE_PROMOTIONS_DELETE";

        public const string SP_DELETECUSTOMHIERARCHY_NEW_DELETE1 = "SP_DELETECUSTOMHIERARCHY_NEW_DELETE1";
        public const string SP_DELETECUSTOMHIERARCHY_NEW_DELETE2 = "SP_DELETECUSTOMHIERARCHY_NEW_DELETE2";
        public const string SP_DELETECUSTOMHIERARCHY_NEW_DELETE3 = "SP_DELETECUSTOMHIERARCHY_NEW_DELETE3";

        public const string SP_DELETECUSTOMHIERARCHY_PRODUCT_NEW_DELETE = "SP_DELETECUSTOMHIERARCHY_PRODUCT_NEW_DELETE";

        public const string SP_DELETECUSTOMSTORE_NEW_DELETE = "SP_DELETECUSTOMSTORE_NEW_DELETE";

        public const string SP_DELETECUSTOMSTOREGROUP_NEW_DELETE1 = "SP_DELETECUSTOMSTOREGROUP_NEW_DELETE1";
        public const string SP_DELETECUSTOMSTOREGROUP_NEW_DELETE2 = "SP_DELETECUSTOMSTOREGROUP_NEW_DELETE2";
        public const string SP_DELETECUSTOMSTOREGROUP_NEW_DELETE3 = "SP_DELETECUSTOMSTOREGROUP_NEW_DELETE3";

        public const string SP_INSERT_CREATEUSERPROFILE_INSERT = "SP_INSERT_CREATEUSERPROFILE_INSERT";
        public const string SP_INSERT_CREATEUSERPROFILE_SELECT = "SP_INSERT_CREATEUSERPROFILE_SELECT";

        public const string SP_INSERT_CUSTOMHIERARCHY_DETAIL_INSERT = "SP_INSERT_CUSTOMHIERARCHY_DETAIL_INSERT";
        public const string SP_INSERT_CUSTOMHIERARCHY_MASTER_INSERT = "SP_INSERT_CUSTOMHIERARCHY_MASTER_INSERT";

        public const string SP_INSERT_CUSTOMHIERARCHY_MASTER_WITHPARENT_HIERARCHY_INSERT = "SP_INSERT_CUSTOMHIERARCHY_MASTER_WITHPARENT_HIERARCHY_INSERT";

        public const string SP_INSERT_CUSTOMHIERARCHY_PRODUCTS_INSERT = "SP_INSERT_CUSTOMHIERARCHY_PRODUCTS_INSERT";

        public const string SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_SELECT = "SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_SELECT";
        public const string SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_INSERT = "SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_INSERT";
        public const string SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_UPDATE = "SP_INSERT_CUSTOMPROMOTION_HIERARCHYTYPE_UPDATE";

        public const string SP_INSERT_CUSTOMPROMOTION_SELECT = "SP_INSERT_CUSTOMPROMOTION_SELECT";
        public const string SP_INSERT_CUSTOMPROMOTION_INSERT = "SP_INSERT_CUSTOMPROMOTION_INSERT";
        public const string SP_INSERT_CUSTOMPROMOTION_UPDATE = "SP_INSERT_CUSTOMPROMOTION_UPDATE";

        public const string SP_INSERT_DIM_CUSTOM_STORES_DETAILS_INSERT = "SP_INSERT_DIM_CUSTOM_STORES_DETAILS_INSERT";

        public const string SP_INSERT_DIM_CUSTOM_STORES_INSERTSELECT = "SP_INSERT_DIM_CUSTOM_STORES_INSERTSELECT";

        public const string SP_INSERT_DIM_CUSTOM_STORES_MASTER_INSERT = "SP_INSERT_DIM_CUSTOM_STORES_MASTER_INSERT";

        public const string SP_INSERT_DUPLICATECUSTOM_STORE_INSERT = "SP_INSERT_DUPLICATECUSTOM_STORE_INSERT";
        public const string SP_INSERT_DUPLICATECUSTOM_STORE_INSERTSELECT = "SP_INSERT_DUPLICATECUSTOM_STORE_INSERTSELECT";

        public const string SP_INSERT_DUPLICATECUSTOMHIERARCHY_INSERT = "SP_INSERT_DUPLICATECUSTOMHIERARCHY_INSERT";
        public const string SP_INSERT_DUPLICATECUSTOMHIERARCHY_INSERTSELECT = "SP_INSERT_DUPLICATECUSTOMHIERARCHY_INSERTSELECT";

        public const string SP_INSERT_FEEDBACKDETAILS_INSERT = "SP_INSERT_FEEDBACKDETAILS_INSERT";
        public const string SP_INSERT_FEEDBACKDETAILS_SELECT = "SP_INSERT_FEEDBACKDETAILS_SELECT";
        public const string SP_INSERT_FEEDBACKDETAILS_SELECTCOUNT = "SP_INSERT_FEEDBACKDETAILS_SELECTCOUNT";

        public const string SP_INSERT_PRODUCT_HISTORY_INSERT = "SP_INSERT_PRODUCT_HISTORY_INSERT";
        public const string SP_INSERT_PRODUCT_HISTORY_INSERT2 = "SP_INSERT_PRODUCT_HISTORY_INSERT2";

        public const string SP_INSERT_UAM_ONLINETEMPLATE_INSERT = "SP_INSERT_UAM_ONLINETEMPLATE_INSERT";
        public const string SP_INSERT_UAM_ONLINETEMPLATE_SELECT = "SP_INSERT_UAM_ONLINETEMPLATE_SELECT";
        public const string SP_INSERT_UAM_ONLINETEMPLATE_SELECTCOUNT = "SP_INSERT_UAM_ONLINETEMPLATE_SELECTCOUNT";
        public const string SP_INSERT_UAM_ONLINETEMPLATE_UPDATE = "SP_INSERT_UAM_ONLINETEMPLATE_UPDATE";
        public const string SP_INSERT_INTO_CUSTOM_STORES = "SP_INSERT_INTO_CUSTOM_STORES";
        public const string SP_INSERT_USERDATAACCESS_INSERT = "SP_INSERT_USERDATAACCESS_INSERT";
        public const string SP_INSERT_USERDATAACCESS_SELECT = "SP_INSERT_USERDATAACCESS_SELECT";

        public const string SP_INSERTREPORT_PARAMETERS_INSERT = "SP_INSERTREPORT_PARAMETERS_INSERT";

        public const string SP_UPDATE_MARKET_BASKET_REQUEST_TEXTVALUES_UPDATE = "SP_UPDATE_MARKET_BASKET_REQUEST_TEXTVALUES_UPDATE";

        public const string SP_UPDATE_MARKET_REQUEST_UPDATE = "SP_UPDATE_MARKET_REQUEST_UPDATE";

        public const string SP_VALIDATEHIERARCHYNAME_SELECTCOUNT1 = "SP_VALIDATEHIERARCHYNAME_SELECTCOUNT1";

        public const string SP_VALIDATEHIERARCHYNAME_SELECTCOUNT2 = "SP_VALIDATEHIERARCHYNAME_SELECTCOUNT2";

        public const string SP_REPLICATE_CUSTOMHIERARCHY_DELETE1 = "SP_REPLICATE_CUSTOMHIERARCHY_DELETE1";
        public const string SP_REPLICATE_CUSTOMHIERARCHY_DELETE2 = "SP_REPLICATE_CUSTOMHIERARCHY_DELETE2";
        public const string SP_REPLICATE_CUSTOMHIERARCHY_SELECT = "SP_REPLICATE_CUSTOMHIERARCHY_SELECT";
        public const string SP_REPLICATE_CUSTOMHIERARCHY_INSERT1 = "SP_REPLICATE_CUSTOMHIERARCHY_INSERT1";
        public const string SP_REPLICATE_CUSTOMHIERARCHY_INSERT2 = "SP_REPLICATE_CUSTOMHIERARCHY_INSERT2";

        //Get Resource constant for generic aspx page.
        public const string GENERIC_GET_BANNER_DETAILS = "GENERIC_GET_BANNER_DETAILS";
        public const string GENERIC_GET_PRODUCT_DETAILS_CORPORATEUSER = "GENERIC_GET_PRODUCT_DETAILS_CORPORATEUSER";
        public const string GENERIC_GET_PRODUCT_DETAILS_FILTEREDUSER = "GENERIC_GET_PRODUCT_DETAILS_FILTEREDUSER";
        public const string GENERIC_GET_FRESH_SUPPORT_PRODUCT_HIERARCHY_DETAILS = "GENERIC_GET_FRESH_SUPPORT_PRODUCT_HIERARCHY_DETAILS";
        public const string GENERIC_GET_CUSTOM_PRODUCT_HIERARCHY_DETAILS_CORPORATE_USER = "GENERIC_GET_CUSTOM_PRODUCT_HIERARCHY_DETAILS_CORPORATE_USER";
        public const string GENERIC_GET_CUSTOM_PRODUCT_HIERARCHY_DETAILS_UAM_USER = "GENERIC_GET_CUSTOM_PRODUCT_HIERARCHY_DETAILS_UAM_USER";
        public const string GENERIC_GET_CUSTOM_LOCATION_HIERARCHY_DETAILS_CORPORATE_USER = "GENERIC_GET_CUSTOM_LOCATION_HIERARCHY_DETAILS_CORPORATE_USER";
        public const string GENERIC_GET_CUSTOM_LOCATION_HIERARCHY_DETAILS_UAM_USER = "GENERIC_GET_CUSTOM_LOCATION_HIERARCHY_DETAILS_UAM_USER";
        public const string GENERIC_GET_FILTER_DETAILS_BY_REPORTID = "GENERIC_GET_FILTER_DETAILS_BY_REPORTID";
        public const string GENERIC_GET_FISCAL_YEAR_WEEK = "GENERIC_GET_FISCAL_YEAR_WEEK";
        public const string GENERIC_GET_HOLIDAY_AND_YEAR = "GENERIC_GET_HOLIDAY_AND_YEAR";
        public const string GENERIC_GET_VENDOR_EXIST = "GENERIC_GET_VENDOR_EXIST";
        public const string GENERIC_GET_PROFILE_VENDOR_NUMBER = "GENERIC_GET_PROFILE_VENDOR_NUMBER";
        public const string GENERIC_GET_MENU_DETAILS = "GENERIC_GET_MENU_DETAILS";
        public const string GENERIC_SAVE_REPORT = "GENERIC_SAVE_REPORT";
        public const string GENERIC_SAVE_REPORT_FOR_BATCHING = "GENERIC_SAVE_REPORT_FOR_BATCHING";
        public const string GENERIC_SHARESAVE_REPORT = "GENERIC_SHARESAVE_REPORT"; 
        public const string GENRIC_GETREPORTINFO_BY_REPORT_AND_USERNAME = "GENRIC_GETREPORTINFO_BY_REPORT_AND_USERNAME";
        public const string GENERIC_GET_USEREMAIL_BY_USERNAME = "GENERIC_GET_USEREMAIL_BY_USERNAME";
        
        public const string GET_USERROLE_BYPROFILE = "GET_USERROLE_BYPROFILE";
        public const string GET_HOMEPAGE_URL_ON_PROFILE_CHANGE = "GET_HOMEPAGE_URL_ON_PROFILE_CHANGE";
       
        public const string GET_USERROLE_SV = "GET_USERROLE_SV";
        public const string GET_USER_PROFILE_LIST = "GET_USER_PROFILE_LIST";
        public const string GET_VENDERNUMBER_LIST = "GET_VENDERNUMBER_LIST";
        public const string DELETE_USER_DEFAULTPROFILE_BY_USERNAME = "DELETE_USER_DEFAULTPROFILE_BY_USERNAME";
        public const string INSERT_USER_DEFAULT_PROFILE_VENDOR = "INSERT_USER_DEFAULT_PROFILE_VENDOR";
        public const string GET_SAVED_REPORT_DETAILS = "SP_GET_SAVED_REPORT_DETAILS";
        public const string GENERIC_GET_MENUNAME = "GENERIC_GET_MENUNAME";

        public const string DELETE_FROM_DIM_CUSTOM_HIERARCHY_EXTENDED = "DELETE_FROM_DIM_CUSTOM_HIERARCHY_EXTENDED";
        public const string SP_INSERT_DIM_CUSTOM_HIERARCHY_EXTENDED = "SP_INSERT_DIM_CUSTOM_HIERARCHY_EXTENDED";
        public const string DELETE_FROM_DIM_CUSTOM_HIERARCHIES_SHARED = "DELETE_FROM_DIM_CUSTOM_HIERARCHIES_SHARED";
        public const string SP_INSERT_LOG_CUSTOMHIERARCHY_EXTENDED = "SP_INSERT_LOG_CUSTOMHIERARCHY_EXTENDED";
        public const string SP_GET_LOGID_SEQ_CUSTOMHIERARCHIES_EXTENDED_LOG = "SP_GET_LOGID_SEQ_CUSTOMHIERARCHIES_EXTENDED_LOG";
        public const string DELETE_FROM_DIM_CUSTOM_STORES_SHARED = "DELETE_FROM_DIM_CUSTOM_STORES_SHARED";
        public const string DELETE_FROM_DIM_CUSTOM_STORES_EXTENDED = "DELETE_FROM_DIM_CUSTOM_STORES_EXTENDED";
        public const string SP_INSERT_DIM_CUSTOM_STORES_EXTENDED = "SP_INSERT_DIM_CUSTOM_STORES_EXTENDED";
        public const string SP_GET_LOGID_SEQ_CUSTOMSTORES_EXTENDED_LOG = "SP_GET_LOGID_SEQ_CUSTOMSTORES_EXTENDED_LOG";
        public const string SP_INSERT_LOG_CUSTOMSTORE_EXTENDED = "SP_INSERT_LOG_CUSTOMSTORE_EXTENDED";


        public const string LOGIN_CHECK_PORTAL_EXISTANCE = "LOGIN_CHECK_PORTAL_EXISTANCE";
        public const string LOGIN_GET_HOMEURL = "LOGIN_GET_HOMEURL";
        public const string LOGIN_GET_LOGIN_SESSION_USER = "LOGIN_GET_LOGIN_SESSION_USER";
        public const string SP_LOGIN_GETNOTIFICATIONS = "SP_LOGIN_GETNOTIFICATIONS";
        public const string SP_GET_ADMIN_MESSAGE = "SP_GET_ADMIN_MESSAGE";
        public const string SP_INSERT_ADMIN_MESSAGE_ACTIVITY = "SP_INSERT_ADMIN_MESSAGE_ACTIVITY";
        


    }

    /// <summary>
    /// PARTIAL CLASS USED FOR GENERIC ASPX PAGE
    /// Class AppConstants.
    /// This file will contains all constants,which we will be using in our web application.
    /// </summary>
    public partial class AppConstants
    {
        //constants related to events,required to create dynamic user/input controls,mapped with database table tblGroupDetails
        public const string EVENT_CREATE_BANNER_LIST = "CreateBannerList";
        public const string EVENT_CREATE_SUB_BANNER_LIST = "CreateSubBannerList";

        public const string EVENT_CREATE_BANNER_LOCATION_SOURCE_LIST = "CreateLoctionHierarchySourceBannerList";
        public const string EVENT_CREATE_SUB_BANNER_LOCATION_SOURCE_LIST = "CreateLoctionHierarchySourceSubBannerList";
        public const string EVENT_CREATE_STORE_LOCATION_SOURCE_LIST = "CreateLoctionHierarchySourceStoreList";

        public const string EVENT_CREATE_BANNER_LOCATION_DESTINATION_LIST = "CreateLoctionHierarchyDestinationBannerList";
        public const string EVENT_CREATE_SUB_BANNER_LOCATION_DESTINATION_LIST = "CreateLoctionHierarchyDestinationSubBannerList";
        public const string EVENT_CREATE_STORE_LOCATION_DESTINATION_LIST = "CreateLoctionHierarchyDestinationStoreList";

        public const string EVENT_CREATE_LOCATION_STORE_LIST = "CreateLocationStoreList";
        public const string EVENT_CREATE_LOCATION_CUSTOM_LEVEL_1_LIST = "CreateLocationCustomLevel1List";
        public const string EVENT_CREATE_LOCATION_CUSTOM_LEVEL_2_LIST = "CreateLocationCustomLevel2List";
        public const string EVENT_CREATE_LOCATION_CUSTOM_LEVEL_3_LIST = "CreateLocationCustomLevel3List";

        public const string EVENT_CREATE_FISCAL_YEAR_WEEK_FROM_LIST = "CreateFiscalYearWeekFromList";
        public const string EVENT_CREATE_FISCAL_YEAR_WEEK_TO_LIST = "CreateFiscalYearWeekToList";
        public const string EVENT_CREATE_DEPARTMENT_LIST = "CreateDepartmentList";
        public const string EVENT_CREATE_CATEGORY_LIST = "CreateCategoryList";
        public const string EVENT_CREATE_SUB_CATEGORY_LIST = "CreateSubCategoryList";
        public const string EVENT_CREATE_SEGMENT_LIST = "CreateSegmentList";
        public const string EVENT_CREATE_CORPORATE_BRAND_LIST = "CreateCorporateBrandList";
        public const string EVENT_CREATE_CORPORATE_PRODUCT_SIZE_LIST = "CreateCorporateProductSizeList";
        public const string EVENT_CREATE_CUSTOM_LEVEL_1_LIST = "CreateCustomLevel1List";
        public const string EVENT_CREATE_CUSTOM_LEVEL_1_NEW_ITEMS_LIST = "CreateCustomLevel1NewItemsList";
        public const string EVENT_CREATE_CUSTOM_LEVEL_1_DISCONTINUED_ITEMS_LIST = "CreateCustomLevel1DiscontinuedItemsList";

        public const string EVENT_CREATE_CUSTOM_LEVEL_2_LIST = "CreateCustomLevel2List";
        public const string EVENT_CREATE_CUSTOM_LEVEL_3_LIST = "CreateCustomLevel3List";
        public const string EVENT_CREATE_CUSTOM_BRAND_LIST = "CreateCustomBrandList";
        public const string EVENT_CREATE_CUSTOM_PRODUCT_SIZE_LIST = "CreateCustomProductSizeList";
        public const string EVENT_CREATE_BRANDTYPE_LIST = "CreateBrandTypeList";
        public const string EVENT_CREATE_VIEWBY_LIST = "CreateViewByList";
        public const string EVENT_CREATE_LOCATION_HIERARCHY_TYPE_LIST = "CreateLocationHierarchyTypeList";
        public const string EVENT_CREATE_PRODUCT_HIERARCHY_TYPE_LIST = "CreateProductHierarchyTypeList";
        public const string EVENT_CREATE_TIMEFRAME_LIST = "CreateTimeFrameList";
        public const string EVENT_CREATE_HIERARCHY_LIST = "CreateHierarchyList";

        public const string EVENT_CREATE_NO_OF_WEEKS_LIST = "CreateNoOffWeeksList";
        public const string EVENT_CREATE_NO_OF_WEEKS_PRIOR_LIST = "CreateNoOffWeeksPriorList";
        public const string EVENT_CREATE_NO_OF_WEEKS_POST_LIST = "CreateNoOffWeeksPostList";
        public const string EVENT_CREATE_HOLIDAY_LIST = "CreateHolidayList";
        public const string EVENT_CREATE_YEAR_LIST = "CreateYearList";
        public const string EVENT_CREATE_POG_LIST = "CreatePogList";

        //List of InputControls which will be created dynamicaly using database mapping
        public const string INPUTCONTROLS_DROPDOWN = "DROPDOWN";
        public const string INPUTCONTROLS_TEXTBOX = "TEXTBOX";
        public const string INPUTCONTROLS_ANCHOR = "ANCHOR";
        public const string INPUTCONTROLS_RADIO = "RADIO";

        //list of database columns name used under Location corporate hiererchy
        public const string BANNER_COLUMN = "BANNER";
        public const string BANNER_ID_COLUMN = "BANNER_ID";
        public const string SUBBANNER_COLUMN = "SUB_BANNER";
        public const string SUBBANNER_ID_COLUMN = "SUB_BANNER_ID";
        public const string LOC_SOURCE_STORE_COLUMN = "STORE_NAME";
        public const string LOC_DESTINATION_STORE_COLUMN = "STORE_NAME";
        public const string LOC_STORE_NUMBER_COLUMN = "STORE_NUMBER";

        //list of database columns name used under Location corporate hiererchy
        public const string CUSTOM_STORE_GROUP_ID_COLUMN = "CUSTOM_STORE_GROUP_ID";
        public const string CUSTOM_STORE_NAME_COLUMN = "CUSTOM_STORE_NAME";


        //list of database columns name used under product corporate hiererchy
        public const string DEPARTMENT_COLUMN = "DEPARTMENT";
        public const string DEPARTMENT_ID_COLUMN = "DEPARTMENT_ID";
        public const string CATEGORY_COLUMN = "CATEGORY";
        public const string CATEGORY_ID_COLUMN = "CATEGORY_ID";
        public const string SUBCATEGORY_COLUMN = "SUB_CATEGORY";
        public const string SUBCATEGORY_ID_COLUMN = "SUB_CATEGORY_ID";
        public const string SEGMENT_COLUMN = "SEGMENT";
        public const string SEGMENT_ID_COLUMN = "SEGMENT_ID";
        public const string BRAND_ID_COLUMN = "BRAND_ID";
        public const string BRAND_COLUMN = "BRAND_NAME";
        public const string PRODUCT_SIZE_COLUMN = "PRODUCT_SIZE_DESC";

        //list of database columns name used under product custom hierarchy
        public const string CUSTOM_HIERARCHY_ID_COLUMN = "CUSTOM_HIERARCHY_ID";
        public const string CUSTOM_HIERARCHY_COLUMN = "HIERARCHY_NAME";
        public const string CUSTOM_LEVEL_1_COLUMN = "CUSTOM_LEVEL_1";
        public const string CUSTOM_LEVEL_2_COLUMN = "CUSTOM_LEVEL_2";
        public const string CUSTOM_LEVEL_3_COLUMN = "CUSTOM_LEVEL_3";
        public const string CUSTOM_BRAND_COLUMN = "BRAND";
        public const string CUSTOM_PRODUCT_SIZE_COLUMN = "PRODUCT_SIZE_DESC";

        //list of database column name used under holiday dropdown
        public const string HOLIDAY_ID = "HOLIDAY_ID";
        public const string HOLIDAY_COLUMN = "HOLIDAY";
        public const string HOLIDAY_YEAR_COLUMN = "CALENDAR_YEAR";

        //list of no of week for post and prior
        public const string DDL_NO_OFF_WEEKS_0 = "0";
        public const string DDL_NO_OFF_WEEKS_1 = "1";
        public const string DDL_NO_OFF_WEEKS_2 = "2";
        public const string DDL_NO_OFF_WEEKS_3 = "3";
        public const string DDL_NO_OFF_WEEKS_4 = "4";
        public const string DDL_NO_OFF_WEEKS_5 = "5";
        public const string DDL_NO_OFF_WEEKS_6 = "6";
        public const string DDL_NO_OFF_WEEKS_7 = "7";
        public const string DDL_NO_OFF_WEEKS_8 = "8";

        //Global Constanats used for Tableau Report URL 
        public const string VIEWS = "/views/";
        public const string MAIN = "/Main";
        public const string TRUSTED = "/trusted/";
        public const string HASH = "/#";
        public const string HTTP = "http://";
        public static string TableauServerInstanceURL
        {
            get
            {
                return !string.IsNullOrEmpty(ConfigurationManager.AppSettings["TableauInstance"]) ? ConfigurationManager.AppSettings["TableauInstance"] : "192.168.100.92:8000";

            }
        }

        //Get Param Date etc from POC_EDW database
        public const string GETPARAMDATE = "GETPARAMDATE";
        public const string GETPARAMLYDATE = "GETPARAMLYDATE";
        public const string SELECT_HOLIDAYSTARTDATE = "SELECT_HOLIDAYSTARTDATE";
        public const string SELECT_HOLIDAYENDDATE = "SELECT_HOLIDAYENDDATE";
        public const string RCDATE1 = "RCDATE1";
        public const string RCDATE2 = "RCDATE2";

        //Hidden fields used for Tableau URL Creating
        public const string PARAM_START_DATE = "PARAM_START_DATE";
        public const string PARAM_START_DATE_LY = "PARAM_START_DATE_LY";
        public const string PARAM_END_DATE = "PARAM_END_DATE";
        public const string PARAM_END_DATE_LY = "PARAM_END_DATE_LY";
        public const string END_DATE_TY = "End_Date TY";//"End_Date%2TY"
        public const string START_DATE_TY = "Start_Date TY";//Start_Date%20TY
        public const string END_DATE_LY = "End_Date LY";//"End_Date%2LY"
        public const string START_DATE_LY = "Start_Date LY";//Start_Date%20LY
        public const string PARAM_USERNAME = "Param_Username";
        public const string END_DATE = "End_Date";
        public const string START_DATE = "Start_Date";
        public const string E_ID = "e_id";
        public const string WEEK_VIEW = "Week View";//Week%20View
        public const string L_DATE = "L_Date";
        public const string NEW_ITEM_VALUE = "new_item_value";
        public const string CUSTOM_HIERARCHY_ID = "CUSTOM_HIERARCHY_ID";
        public const string CUSTOM_HIERARCHY_NAME = "custom_hierarchy_name";
        public const string CATEGORY = "category";
        public const string D_DATE = "D_Date";
        public const string DISCO_ITEM_VALUE = "disco_item_value";
        public const string DISCODATE = "DiscoDate";
        public const string Dates = "Dates";
        public const string RC_DATE = "RC_Date";
        public const string RC = "RC";

        public const string PARAM_VENDOR_NUMBER = "Vendor Number";
        public const string PARAM_BANNER_ID = "b_id";
        public const string PARAM_SUB_BANNER_ID = "sb_id";
        public const string PARAM_DEPARTMENT_ID = "d_id";
        public const string PARAM_CATEGORY_ID = "c_id";
        public const string PARAM_CUSTOM_STORE_ID = "cg_id";
        public const string PARAM_LOCCUSTOM_LEVEL1 = "cgcl_1";
        public const string PARAM_LOCCUSTOM_LEVEL2 = "cgcl_2";
        public const string PARAM_LOCCUSTOM_LEVEL3 = "cgcl_3";
        public const string PARAM_SUBCATEGORY_ID = "sc_id";
        public const string PARAM_SEGMENT_ID = "s_id";
        public const string PARAM_BRAND = "Brand";
        public const string PARAM_BRAND_TYPE = "PL";
        public const string PARAM_PRODUCT_SIZE = "ps";
        public const string PARAM_STORE_ID = "Store Number";
        public const string PARAM_OPP_BANNER_ID = "OpporBanner_id";
        public const string PARAM_OPP_SUB_BANNER_ID = "OpporSubbanner_id";
        public const string PARAM_OPP_STORE_ID = "Opportunity_Store";
        public const string PARAM_POG_FLAG = "POG_FLAG";
        public const string PARAM_POG = "POG";
        public const string PARAM_HEADER_DEPARTMENT = "DPT";
        public const string PARAM_HEADER_CATEGORY = "CAT";
        public const string PARAM_HEADER_SUB_BANNERS = "SB";
        public const string PARAM_HEADER_BANNERS = "TOPBANNER";
        public const string PARAM_HIERARCHY_ID = "ch_id";
        public const string PARAM_CUSTOM_LEVEL1 = "cl_1";
        public const string PARAM_CUSTOM_LEVEL2 = "cl_2";
        public const string PARAM_CUSTOM_LEVEL3 = "cl_3";
        public const string PARAM_CUSTOM_LEVEL1_NEW_ITEM = "NI";             //for new item dashboard
        public const string PARAM_CUSTOM_LEVEL1_DISCONTINUED_ITEM = "DI";
        public const string PARAM_TOADD_WEEK = "ToAdWeek";
        public const string PARAM_FROMADD_WEEK = "FromAdWeek";
        public const string PARAM_FISCAL_YEAR = "Fiscal Year";
        public const string PARAM_VIEW_BY = "ddlviewby";
        public const string PARAM_HOLIDAY_ID = "h_id";
        public const string PARAM_HOLIDAY_NAME = "Holiday Name";
        public const string PARAM_PRIOR_WEEK = "PRW";
        public const string PARAM_POST_WEEK = "POW";
        public const string PARAM_NO_OFF_WEEK = "num_of_weeks";
        
        public const string PARAM_WEEKLY_VIEW = "Weekly View";

        public const string PARAM_CUSTOM_BRAND_FILTER_NAME = "CustomBrand";
        public const string PARAM_CORPORATE_BRAND_FILTER_NAME = "ProductBrand";
        public const string PARAM_CUSTOM_PRODUCTSIZE_FILTER_NAME = "CustomProductSizeDesc";
        public const string PARAM_CORPORATE_PRODUCTSIZE_BRAND_FILTER_NAME = "ProProductSizeDesc";



        public const string PARAM_CSS_CORPORATE_ITEM = "CorporateItem";
        public const string PARAM_CSS_CUSTOM_ITEM = "CustomItem";
        public const string PARAM_CSS_LOCCORPORATE_ITEM = "LocationCorporateItem";
        public const string PARAM_CSS_LOCCUSTOM_ITEM = "LocationCustomItem";


        //reports names
        public const string REPORT_DISTRIBUTION_GAP_ANALYSIS = "DISTRIBUTIONGAPANALYSIS";
        public const string REPORT_DIGITAL_ID_CUSTOMER_DASHBOARD = "DIGITALIDCUSTOMERDASHBOARD";
        public const string REPORT_PRODUCT_PURCHASE_MULTIPLES = "PRODUCTPURCHASEMULTIPLES";
        public const string REPORT_DIGITALIDCUSTOMERDASHBOARD = "DIGITALIDCUSTOMERDASHBOARD";
        public const string REPORT_DISTRIBUTION_GAP_SALES_OPPORTUNITY = "DISTRIBUTIONGAPSALES$OPPORTUNITY";
        public const string REPORT_PRICE_EXECUTION_AUDIT = "PRICEEXECUTIONAUDIT";
        public const string REPORT_DIGITAL_ID_CUSTOMER_DEMOGRAPHICS = "DigitalIDCustomerDemographics";
        public const string REPORT_DIGITAL_ID_PERFORMANCE_METRICS = "DIGITALIDPERFORMANCEMETRICS";
        public const string REPORT_DIGITAL_ID_NEW_ITEMS = "DIGITALIDNEWITEMS";
        public const string REPORT_DIGITAL_ID_DISCONTINUED_ITEMS = "DIGITALIDDISCONTINUEDITEMS";
        public const string REPORT_NEW_ITEM_DASHBOARD = "NEWITEMDASHBOARD";
        public const string REPORT_NO_SCAN_LAST_X_WEEKS = "NOSCANSLASTXWEEKS";


        public const string REPORT_AD_HOC_ADVANCED = "ADHOCADVANCED";
        public const string REPORT_AD_HOC_HOLIDAY_SHIFT = "ADHOCHOLIDAYSHIFT";

        public const string REPORT_SALES_DRIVER_BASKET = "SALESDRIVERBASKET";
        public const string REPORT_BUSINESS_REVIEW_DASHBOARDS = "BUSINESSREVIEWDASHBOARDS";

        //database tables used
        public const string DIM_CALENDAR_HOLIDAYS_CORE_WITH_WEEK_0 = "Dim_Calendar_Holidays_Core_With_Week_0";

        //Global Variables/Constatns
        public const string GLOBAL_DATE_FORMAT = "yyyy-MM-dd";


        public const string GET_SALES_OPPORTUNITY_SP_EXCUTE = "GET_SALES_OPPORTUNITY_SP_EXCUTE";
        public const string GET_PRODUCT_PURCHASE_MULTIPLE_SP_EXCUTE = "GET_PRODUCT_PURCHASE_MULTIPLE_SP_EXCUTE";
        public const string GET_SALES_DRIVER_BASKET_SP_EXECUTE = "GET_SALES_DRIVER_BASKET_SP_EXECUTE";
        

        public const string GET_MENUNAME = "GET_MENUNAME";
        public const string GET_INSERTREPORT_PARAMETERS = "GET_INSERTREPORT_PARAMETERS";
        public const string GET_VENDORNAME_BY_VENDORNO = "GET_VENDORNAME_BY_VENDORNO";
        public const string GET_ROLEBY_USERNAME = "GET_ROLEBY_USERNAME";

        public const string GET_GROUPNAME_ID_PORTAL_BY_GROUPID = "GET_GROUPNAME_ID_PORTAL_BY_GROUPID";
        public const string GET_GROUPLIST = "GET_GROUPLIST";
        public const string GET_VERTICALLIST_GENERAL = "GET_VERTICALLIST_GENERAL";
        public const string GET_VERTICALLIST_OTHER = "GET_GROUPLIST_OTHER";
        public const string DELETE_GROUPMENUS = "DELETE_GROUPMENUS";
        public const string GET_GROUP_DETAILS = "GET_GROUP_DETAILS";
        public const string GET_PARENT_ID_BY_MENUID = "GET_PARENT_ID_BY_MENUID";
        public const string GET_LOGIN_EMAILS = "GET_LOGIN_EMAILS";
        public const string FILL_RAD_CHECKED_CHAIN_LIST = "FILL_RAD_CHECKED_CHAIN_LIST";
        public const string GET_SUPPLIER_BANNER = "GET_SUPPLIER_BANNER";
        public const string GET_SUPPLIER_BANNER_BY_SUPPLIERID = "GET_SUPPLIER_BANNER_BY_SUPPLIERID";
        public const string GET_SUPPLIER_BANNER_WITHOUT_SUPPLIERID = "GET_SUPPLIER_BANNER_WITHOUT_SUPPLIERID";
        public const string GET_SUPPLIER_BANNER_BY_CHAINID = "GET_SUPPLIER_BANNER_BY_CHAINID";
        public const string GET_SUPPLIER_BANNER_BY_WITHOUT_CHAINID = "GET_SUPPLIER_BANNER_BY_WITHOUT_CHAINID";
        public const string GET_MANUFACTURER_NAME_LIST = "GET_MANUFACTURER_NAME_LIST";
        public const string GET_GROUP_NAME_FOR_EDW_IDW_GENERAL = "GET_GROUP_NAME_FOR_EDW_IDW_GENERAL";
        public const string GET_GROUP_NAME_FOR_EDW_IDW_GENERAL_BY_GROUPTYPEID = "GET_GROUP_NAME_FOR_EDW_IDW_GENERAL_BY_GROUPTYPEID";
        public const string GET_ATTRIBUTE_LIST = "GET_ATTRIBUTE_LIST";
        public const string GET_ALL_SUPPLIER_LIST_BY_PERSONID = "GET_ALL_SUPPLIER_LIST_BY_PERSONID";
        public const string GET_SUPPLIER_ID_WITH_EDIT_PERMISSION = "GET_SUPPLIER_ID_WITH_EDIT_PERMISSION";
        public const string GET_ALL_RETAILER_LIST_BY_PERSONID = "GET_ALL_RETAILER_LIST_BY_PERSONID";
        public const string GET_RETAILER_ID_WITH_EDIT_PERMISSION = "GET_RETAILER_ID_WITH_EDIT_PERMISSION";
        public const string GET_GROUPID_PORTAL_BY_USERID = "GET_GROUPID_PORTAL_BY_USERID";
        public const string DELETE_ATTRIBUTE_BY_ATTRIBUTEID = "DELETE_ATTRIBUTE_BY_ATTRIBUTEID";
        public const string DELETE_ASSIGN_USER_GROUP_BY_USERID = "DELETE_ASSIGN_USER_GROUP_BY_USERID";
        public const string GET_ATTRIBUTE_LIST_WITHOUT_ORDER = "GET_ATTRIBUTE_LIST_WITHOUT_ORDER";
        public const string GET_ATTRIBUTEID_BY_OWNERENTITYID = "GET_ATTRIBUTEID_BY_OWNERENTITYID";
        public const string UPDATE_ATTRIBUTEID_BY_OWNERENTITYID = "UPDATE_ATTRIBUTEID_BY_OWNERENTITYID";
        public const string UPDATE_ATTRIBUTE_VALUE_BY_OWNERENTITYID = "UPDATE_ATTRIBUTE_VALUE_BY_OWNERENTITYID";
        public const string INSERT_ATRRIBUTE_VALUE = "INSERT_ATRRIBUTE_VALUE";
        public const string DELETE_RETAILER_ACCESS_BY_PERSONID = "DELETE_RETAILER_ACCESS_BY_PERSONID"; 
        public const string DELETE_SUPPLIER_ACCESS_BY_PERSONID = "DELETE_SUPPLIER_ACCESS_BY_PERSONID";
        public const string INSERT_SUPPLIER_ACCESS = "INSERT_SUPPLIER_ACCESS";
        public const string INSERT_RETAILER_ACCESS = "INSERT_RETAILER_ACCESS";
        public const string GET_PERSON_NAME_BY_PERSONID = "GET_PERSON_NAME_BY_PERSONID";
        public const string GET_PASSWORD_BY_PERSONID = "GET_PASSWORD_BY_PERSONID";

        public const string GET_OWNER_USERS = "GET_OWNER_USERS";
        public const string GET_SHARE_USERS = "GET_SHARE_USERS";
        public const string GET_SHARE_USERS_FOR_CORPORATE = "GET_SHARE_USERS_FOR_CORPORATE";
        public const string GET_SHARE_USERS_FOR_NONCORPORATE = "GET_SHARE_USERS_FOR_NONCORPORATE";
        public const string GET_CUSTOM_STORES_SHARED = "GET_CUSTOM_STORES_SHARED";
        public const string GET_CUSTOM_STORES_MASTER = "GET_CUSTOM_STORES_MASTER";
        public const string GET_CUSTOM_STORES_SHARED_ORDERBY_USERNAME = "GET_CUSTOM_STORES_SHARED_ORDERBY_USERNAME";
        public const string GET_CUSTOM_STORES_UNSHARED_ORDERBY_USERNAME = "GET_CUSTOM_STORES_UNSHARED_ORDERBY_USERNAME";
        public const string GET_CUSTOM_STORES_SHARED_MASTER = "GET_CUSTOM_STORES_SHARED_MASTER";
        public const string GET_CUSTOM_STORES_MASTER_ON_STORENAME = "GET_CUSTOM_STORES_MASTER_ON_STORENAME";
        public const string GET_NEXT_VALUE_FOR_SEQ_CUSTOM_HIERARCHIES_MASTER = "GET_NEXT_VALUE_FOR_SEQ_CUSTOM_HIERARCHIES_MASTER";
        public const string DELETE_CUSTOM_STORES_SHARED = "DELETE_CUSTOM_STORES_SHARED";
        public const string GET_STORE_NUMBER_FROM_STORE_DETAILS = "GET_STORE_NUMBER_FROM_STORE_DETAILS";
        public const string GET_STORE_NUMBER_FROM_STORES = "GET_STORE_NUMBER_FROM_STORES";
        public const string GET_NAME_FROM_USERDATAACCESS = "GET_NAME_FROM_USERDATAACCESS";
        public const string GET_CUSTOM_STORES_AND_USERDATAACCESS = "GET_CUSTOM_STORES_AND_USERDATAACCESS";
        public const string GET_USERDATAACCESS_FOR_EMAIL_NOT_NULL = "GET_USERDATAACCESS_FOR_EMAIL_NOT_NULL";
        public const string GET_CUSTOM_STORES_SHARED_ON_USERNAME = "GET_CUSTOM_STORES_SHARED_ON_USERNAME";
        public const string INSERT_INTO_CUSTOM_STORES_SHARED = "INSERT_INTO_CUSTOM_STORES_SHARED";

        public const string GET_CUSTOM_STORE_MASTER_FOR_RDOWNEDBY = "GET_CUSTOM_STORE_MASTER_FOR_RDOWNEDBY";
        public const string GET_CUSTOM_STORE_MASTER_FOR_NON_RDOWNEDBY = "GET_CUSTOM_STORE_MASTER_FOR_NON_RDOWNEDBY";
        public const string GET_CUSTOM_STORE_MASTER_FOR_SEARCHTEXT = "GET_CUSTOM_STORE_MASTER_FOR_SEARCHTEXT";
        public const string GET_CUSTOM_STORE_MASTER_FOR_DATEBETWEEN = "GET_CUSTOM_STORE_MASTER_FOR_DATEBETWEEN";
        public const string GET_CUSTOM_STORE_MASTER_FOR_FROMDATE = "GET_CUSTOM_STORE_MASTER_FOR_FROMDATE";
        public const string GET_CUSTOM_STORE_MASTER_FOR_TODATE = "GET_CUSTOM_STORE_MASTER_FOR_TODATE";
        public const string GET_CUSTOM_STORE_MASTER_FOR_USERNAME = "GET_CUSTOM_STORE_MASTER_FOR_USERNAME";
        public const string GET_CUSTOM_STORE_MASTER_GROUPBY_CLAUSE = "GET_CUSTOM_STORE_MASTER_GROUPBY_CLAUSE";
        public const string GET_CUSTOM_STORE_MASTER_ORDERBY_CLAUSE = "GET_CUSTOM_STORE_MASTER_ORDERBY_CLAUSE";
        public const string GET_NEXT_VALUE_FROM_CUSTOM_STORE_MASTER = "GET_NEXT_VALUE_FROM_CUSTOM_STORE_MASTER";
        public const string GET_SEARCH_RESULT_FOR_EDIT_CUSTOM_STORE_GROUP = "GET_SEARCH_RESULT_FOR_EDIT_CUSTOM_STORE_GROUP";
        public const string SP_DELETE_CUSTOM_STORES = "SP_DELETE_CUSTOM_STORES";
        public const string GET_ALL_FROM_STORES = "GET_ALL_FROM_STORES";

        //Importuser
        public const string GET_SEGMENTID_BY_SEGMENT_CATEGORY = "GET_SEGMENTID_BY_SEGMENT_CATEGORY";

        public const string INSERT_USERPROFILE_CHAIN_ACCESS = "INSERT_USERPROFILE_CHAIN_ACCESS";
        public const string INSERT_USERPROFILE_PRODUCT_ACCESS = "INSERT_USERPROFILE_PRODUCT_ACCESS";

        public const string INSERT_USERPROFILE_PRODUCT_ACCESS_WITH_UPC = "INSERT_USERPROFILE_PRODUCT_ACCESS_WITH_UPC";
        public const string INSERT_USERPROFILE_BRAND_ACCESS = "INSERT_USERPROFILE_BRAND_ACCESS";
        public const string INSERT_USERPROFILE_SEGMENT_ACCESS = "INSERT_USERPROFILE_SEGMENT_ACCESS";
        public const string INSERT_USERPROFILE_SEGMENT_ACCESS_WITH_CATEGORY_AND_SUBCATEGORY = "INSERT_USERPROFILE_SEGMENT_ACCESS_WITH_CATEGORY_AND_SUBCATEGORY";
        public const string INSERT_USERPROFILE_SUBCATEGORY_ACCESS = "INSERT_USERPROFILE_SUBCATEGORY_ACCESS";
        public const string INSERT_USERPROFILE_SUBCATEGORY_ACCESS_WITH_SUBCATEGORY = "INSERT_USERPROFILE_SUBCATEGORY_ACCESS_WITH_SUBCATEGORY";
        public const string INSERT_USERPROFILE_CATEGORY_ACCESS = "INSERT_USERPROFILE_CATEGORY_ACCESS";
        public const string INSERT_USERPROFILE_CATEGORY_ACCESS_WITH_CATEGORY = "INSERT_USERPROFILE_CATEGORY_ACCESS_WITH_CATEGORY";
        public const string INSERT_USERPROFILE_SUBDEPARTMENT_ACCESS = "INSERT_USERPROFILE_SUBDEPARTMENT_ACCESS";
        public const string INSERT_USERPROFILE_SUBDEPARTMENT_ACCESS_WITH_SUBDEPARTMENT = "INSERT_USERPROFILE_SUBDEPARTMENT_ACCESS_WITH_SUBDEPARTMENT";
        public const string INSERT_USERPROFILE_DEPARTMENT_ACCESS = "INSERT_USERPROFILE_DEPARTMENT_ACCESS";
        public const string INSERT_USERPROFILE_DEPARTMENT_ACCESS_WITH_DEPARTMENT = "INSERT_USERPROFILE_DEPARTMENT_ACCESS_WITH_DEPARTMENT";
        public const string INSERT_USERPROFILE_STORE_ACCESS = "INSERT_USERPROFILE_STORE_ACCESS";
        public const string INSERT_USERPROFILE_STORE_ACCESS_WITH_STORE = "INSERT_USERPROFILE_STORE_ACCESS_WITH_STORE";
        public const string INSERT_USERPROFILE_BANNER_ACCESS = "INSERT_USERPROFILE_BANNER_ACCESS";
        public const string INSERT_USERPROFILE_BANNER_ACCESS_WITH_BANNER = "INSERT_USERPROFILE_BANNER_ACCESS_WITH_BANNER";
        public const string INSERT_USERPROFILE_VENDOR_ACCESS = "INSERT_USERPROFILE_VENDOR_ACCESS";
        public const string INSERT_USERPROFILE_VENDOR_ACCESS_WITH_VENDOR = "INSERT_USERPROFILE_VENDOR_ACCESS_WITH_VENDOR";
        public const string GET_ACCESS_DESCRIPTION_BY_PROFILE = "GET_ACCESS_DESCRIPTION_BY_PROFILE";
        public const string GET_ROLE_BY_PROFILE = "GET_ROLE_BY_PROFILE";
        public const string DELETE_USER_PROFILE = "DELETE_USER_PROFILE";
        public const string DELETE_USER_PROFILE_VENDOR_ACCESS = "DELETE_USER_PROFILE_VENDOR_ACCESS";
        public const string DELETE_USER_PROFILE_BANNER_ACCESS = "DELETE_USER_PROFILE_BANNER_ACCESS";
        public const string DELETE_USER_PROFILE_STORE_ACCESS = "DELETE_USER_PROFILE_STORE_ACCESS";
        public const string DELETE_USER_PROFILE_DEPARTMENT_ACCESS = "DELETE_USER_PROFILE_DEPARTMENT_ACCESS";
        public const string DELETE_USER_PROFILE_SUBDEPARTMENT_ACCESS = "DELETE_USER_PROFILE_SUBDEPARTMENT_ACCESS";
        public const string DELETE_USER_PROFILE_CATEGORY_ACCESS = "DELETE_USER_PROFILE_CATEGORY_ACCESS";
        public const string DELETE_USER_PROFILE_SUBCATEGORY_ACCESS = "DELETE_USER_PROFILE_SUBCATEGORY_ACCESS";
        public const string DELETE_USER_PROFILE_SEGMENT_ACCESS = "DELETE_USER_PROFILE_SEGMENT_ACCESS";
        public const string DELETE_USER_PROFILE_PRODUCT_ACCESS = "DELETE_USER_PROFILE_PRODUCT_ACCESS";
        public const string DELETE_USER_PROFILE_CHAIN_ACCESS = "DELETE_USER_PROFILE_CHAIN_ACCESS";
        public const string DELETE_USER_DATA_ACCESS = "DELETE_USER_DATA_ACCESS";
        public const string GET_LAST_OWNER_ENTITYID = "GET_LAST_OWNER_ENTITYID";
        public const string GET_RECORD_ID = "GET_RECORD_ID";
        public const string GET_ALREADY_ASSIGNED_MENU_TO_GROUP = "GET_ALREADY_ASSIGNED_MENU_TO_GROUP";
//import user end

        public const string UPDATE_CUSTOM_STORES_MASTER = "UPDATE_CUSTOM_STORES_MASTER";
        public const string GET_CUSTOM_STORE_DETAILS = "GET_CUSTOM_STORE_DETAILS";
        public const string UPDATE_CUSTOM_STORES_DETAILS = "UPDATE_CUSTOM_STORES_DETAILS";
        public const string GET_SEARCH_RESULT_FOR_CUSTOM_STORE_GROUP = "GET_SEARCH_RESULT_FOR_CUSTOM_STORE_GROUP";
        public const string SEARCH_STORE_NUMBER = "SEARCH_STORE_NUMBER";

        public const string GET_CUSTOM_HIERARCHY_PRODUCTS = "GET_CUSTOM_HIERARCHY_PRODUCTS";
        public const string GET_CUSTOM_HIERARCHY_SEARCH_PRODUCTS = "GET_CUSTOM_HIERARCHY_SEARCH_PRODUCTS";
        public const string SEARCH_CUSTOM_HIERARCHY_PRODUCTS = "SEARCH_CUSTOM_HIERARCHY_PRODUCTS";
        public const string SEARCH_CUSTOM_HIERARCHY_PRODUCTS_FOR_UPC = "SEARCH_CUSTOM_HIERARCHY_PRODUCTS_FOR_UPC";
        public const string SEARCH_CUSTOM_HIERARCHY_PRODUCTS_GROUP_BY = "SEARCH_CUSTOM_HIERARCHY_PRODUCTS_GROUP_BY";
        public const string UPDATE_CUSTOM_HIERARCHY_MASTER = "UPDATE_CUSTOM_HIERARCHY_MASTER";
        public const string UPDATE_CUSTOM_HIERARCHY_PRODUCTS = "UPDATE_CUSTOM_HIERARCHY_PRODUCTS";
        public const string GET_OWNER_USER_CUSTOM_HIERARCHY_MASTER = "GET_OWNER_USER_CUSTOM_HIERARCHY_MASTER";
        public const string GET_CUSTOM_HIERARCHY_SHARED = "GET_CUSTOM_HIERARCHY_SHARED";
        public const string GET_CUSTOM_PROMOTIONS = "GET_CUSTOM_PROMOTIONS";
        public const string GET_CUSTOM_HIERARCHY_MASTER = "GET_CUSTOM_HIERARCHY_MASTER";
        public const string GET_CUSTOM_HIERARCHY_SHARED_ON_USERDATAACCESS = "GET_CUSTOM_HIERARCHY_SHARED_ON_USERDATAACCESS";
        public const string GET_CUSTOM_HIERARCHY_UNSHARED_ON_USERDATAACCESS = "GET_CUSTOM_HIERARCHY_UNSHARED_ON_USERDATAACCESS";
        public const string GET_CUSTOM_HIERARCHY_MASTER_ON_USERDATAACCESS = "GET_CUSTOM_HIERARCHY_MASTER_ON_USERDATAACCESS";

        public const string GET_CUSTOM_HIERARCHY_SEARCH = "GET_CUSTOM_HIERARCHY_SEARCH";
        public const string GET_CUSTOM_HIERARCHY_SEARCH_NAME = "GET_CUSTOM_HIERARCHY_SEARCH_NAME";
        public const string GET_CUSTOM_HIERARCHY_SEARCH_ON_USERNAME = "GET_CUSTOM_HIERARCHY_SEARCH_ON_USERNAME";
        public const string GET_CUSTOM_HIERARCHY_SEARCH_DATE_BETWEEN = "GET_CUSTOM_HIERARCHY_SEARCH_DATE_BETWEEN";
        public const string GET_CUSTOM_HIERARCHY_SEARCH_FROM_DATE = "GET_CUSTOM_HIERARCHY_SEARCH_FROM_DATE";
        public const string GET_CUSTOM_HIERARCHY_SEARCH_TO_DATE = "GET_CUSTOM_HIERARCHY_SEARCH_TO_DATE";
        public const string GET_CUSTOM_HIERARCHY_SEARCH_USERNAME = "GET_CUSTOM_HIERARCHY_SEARCH_USERNAME";
        public const string GET_CUSTOM_HIERARCHY_SEARCH_GROUPBY = "GET_CUSTOM_HIERARCHY_SEARCH_GROUPBY";
        public const string GET_CUSTOM_HIERARCHY_SEARCH_ORDERBY = "GET_CUSTOM_HIERARCHY_SEARCH_ORDERBY";
        public const string GET_CUSTOM_HIERARCHY_MASTER_ON_HIERARCHYNAME = "GET_CUSTOM_HIERARCHY_MASTER_ON_HIERARCHYNAME";
        public const string DELETE_FROM_CUSTOM_HIERARCHY_SHARED = "DELETE_FROM_CUSTOM_HIERARCHY_SHARED";
        public const string GET_CUSTOM_HIERARCHY_PRODUCTS_ON_HIERARCHY_MASTER = "GET_CUSTOM_HIERARCHY_PRODUCTS_ON_HIERARCHY_MASTER";
        public const string GET_UPC_CUSTOM_HIERARCHY_PRODUCTS = "GET_UPC_CUSTOM_HIERARCHY_PRODUCTS";
        public const string GET_UPC_FROM_PRODUCTS = "GET_UPC_FROM_PRODUCTS";
        public const string GET_CUSTOM_HIERARCHY_TOTAL_UPC = "GET_CUSTOM_HIERARCHY_TOTAL_UPC";
        public const string GET_DISTINCT_CUSTOM_HIERARCHY_MASTER = "GET_DISTINCT_CUSTOM_HIERARCHY_MASTER";
        public const string GET_CUSTOM_HIERARCHY_SHARED_ON_USERNAME = "GET_CUSTOM_HIERARCHY_SHARED_ON_USERNAME";
        public const string INSERT_CUSTOM_HIERARCHY_SHARED = "INSERT_CUSTOM_HIERARCHY_SHARED";
        public const string GET_CUSTOM_PROMOTIONS_ON_CUSTOMHIERARCHYID = "GET_CUSTOM_PROMOTIONS_ON_CUSTOMHIERARCHYID";
        public const string SEARCH_FOR_UPC = "SEARCH_FOR_UPC";
        public const string GET_USER_ACCESS_PRODUCTS = "GET_USER_ACCESS_PRODUCTS";
        public const string GET_STORE_NUMBER = "GET_STORE_NUMBER";
        public const string GET_FROM_USER_ACCESS_PRODUCTS = "GET_FROM_USER_ACCESS_PRODUCTS";
        public const string GET_SAVED_REPORTS = "GET_SAVED_REPORTS";
        public const string UPDATE_SAVED_REPORTS = "UPDATE_SAVED_REPORTS";
        public const string INSERT_SAVED_REPORTS = "INSERT_SAVED_REPORTS";
        public const string GET_SETUP_USER_DATA_ACCESS = "GET_SETUP_USER_DATA_ACCESS";
        public const string GET_UPC_COUNT_FROM_PRODUCTS = "GET_UPC_COUNT_FROM_PRODUCTS";
        public const string GET_WEB_MENUS_ON_REPORTID = "GET_WEB_MENUS_ON_REPORTID";
        public const string GET_MARKET_BASKET_REQUESTS = "GET_MARKET_BASKET_REQUESTS";
        public const string GET_MARKET_BASKET_REQUESTS_DISC_FILTER = "GET_MARKET_BASKET_REQUESTS_DISC_FILTER";
        public const string GET_MARKET_BASKET_REQUESTS_TO_DATE_FILTER = "GET_MARKET_BASKET_REQUESTS_TO_DATE_FILTER";
        public const string GET_MARKET_BASKET_REQUESTS_FROM_DATE_FILTER = "GET_MARKET_BASKET_REQUESTS_FROM_DATE_FILTER";
        public const string GET_MARKET_BASKET_REQUESTS_ORDERBY_FILTER = "GET_MARKET_BASKET_REQUESTS_ORDERBY_FILTER";
        public const string GET_MARKET_BASKET_REQUEST_ON_BASKETID = "GET_MARKET_BASKET_REQUEST_ON_BASKETID";
        public const string GET_TOPBANNER_FROM_STORES = "GET_TOPBANNER_FROM_STORES";
        public const string GET_MARKET_BASKET_USER_ACCESS_PRODUCTS = "GET_MARKET_BASKET_USER_ACCESS_PRODUCTS";
        public const string GET_MARKET_BASKET_USER_ACCESS_PRODUCTS_INNERJOIN = "GET_MARKET_BASKET_USER_ACCESS_PRODUCTS_INNERJOIN";
        public const string GET_MARKET_BASKET_USER_ACCESS_PRODUCTS_UNION = "GET_MARKET_BASKET_USER_ACCESS_PRODUCTS_UNION";
        public const string GET_MARKET_BASKET_USER_ACCESS_PRODUCS_VENDORNUMBER = "GET_MARKET_BASKET_USER_ACCESS_PRODUCS_VENDORNUMBER";
        public const string GET_MARKET_BASKET_USER_ACCESS_PRODUCS_PRODUCTID = "GET_MARKET_BASKET_USER_ACCESS_PRODUCS_PRODUCTID";
        public const string GET_MARKET_BASKET_USER_ACCESS_PRODUCS_ORDERBY = "GET_MARKET_BASKET_USER_ACCESS_PRODUCS_ORDERBY";
        public const string GET_MARKET_BASKET_USER_ACCESS_CATEGORY = "GET_MARKET_BASKET_USER_ACCESS_CATEGORY";
        public const string GET_MARKET_BASKET_USER_ACCESS_CATEGORYLIST = "GET_MARKET_BASKET_USER_ACCESS_CATEGORYLIST";
        public const string GET_MARKET_BASKET_USER_ACCESS_CATEGORY_ORDERBY = "GET_MARKET_BASKET_USER_ACCESS_CATEGORY_ORDERBY";
        public const string GET_MARKET_BASKET_USER_ACCESS_SUB_CATEGORY = "GET_MARKET_BASKET_USER_ACCESS_SUB_CATEGORY";
        public const string GET_MARKET_BASKET_USER_ACCESS_CATEGORYID = "GET_MARKET_BASKET_USER_ACCESS_CATEGORYID";
        public const string GET_MARKET_BASKET_USER_ACCESS_SUB_CATEGORY_ORDERBY = "GET_MARKET_BASKET_USER_ACCESS_SUB_CATEGORY_ORDERBY";
        public const string GET_MARKET_BASKET_USER_ACCESS_SEGMENT = "GET_MARKET_BASKET_USER_ACCESS_SEGMENT";
        public const string GET_MARKET_BASKET_USER_ACCESS_WHERE = "GET_MARKET_BASKET_USER_ACCESS_WHERE";
        public const string GET_MARKET_BASKET_USER_ACCESS_CATEGORYIN = "GET_MARKET_BASKET_USER_ACCESS_CATEGORYIN";
        public const string GET_MARKET_BASKET_USER_ACCESS_SEGMENT_ORDERBY = "GET_MARKET_BASKET_USER_ACCESS_SEGMENT_ORDERBY";
        public const string GET_MARKET_BASKET_USER_ACCESS_BRANDNAME = "GET_MARKET_BASKET_USER_ACCESS_BRANDNAME";
        public const string GET_MARKET_BASKET_USER_ACCESS_SEGMENT_IN = "GET_MARKET_BASKET_USER_ACCESS_SEGMENT_IN";
        public const string GET_MARKET_BASKET_USER_ACCESS_BRAND_NAME_ORDERBY = "GET_MARKET_BASKET_USER_ACCESS_BRAND_NAME_ORDERBY";
        public const string GET_MARKET_BASKET_USER_ACCESS_UPC = "GET_MARKET_BASKET_USER_ACCESS_UPC";
        public const string GET_MARKET_BASKET_USER_ACCESS_BRAND_NAME = "GET_MARKET_BASKET_USER_ACCESS_BRAND_NAME";
        public const string GET_MARKET_BASKET_USER_ACCESS_UPC_ORDERBY = "GET_MARKET_BASKET_USER_ACCESS_UPC_ORDERBY";
        public const string GET_ETHNICITY_CD = "GET_ETHNICITY_CD";
        public const string GET_MARKET_BASKET_DISCRIPTION = "GET_MARKET_BASKET_DISCRIPTION";
        public const string GET_MARKET_BASKET_REQUESTS_ON_DESC = "GET_MARKET_BASKET_REQUESTS_ON_DESC";
        public const string GET_CATEGORYID_FROM_PRODUCTS = "GET_CATEGORYID_FROM_PRODUCTS";
        public const string GET_SUB_CATEGORYID_FROM_PRODUCTS = "GET_SUB_CATEGORYID_FROM_PRODUCTS";
        public const string GET_SEGMENTID_FROM_PRODUCTS = "GET_SEGMENTID_FROM_PRODUCTS";
        public const string GET_MFR_BRANDID_SEGMENTID = "GET_MFR_BRANDID_SEGMENTID";
        public const string GET_PRODUCTID_FROM_PRODUCTS = "GET_PRODUCTID_FROM_PRODUCTS";
        public const string INSERT_INTO_MARKET_BASKET_REQUESTS = "INSERT_INTO_MARKET_BASKET_REQUESTS";
    }

}
